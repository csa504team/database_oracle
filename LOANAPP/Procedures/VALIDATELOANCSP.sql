CREATE OR REPLACE PROCEDURE LOANAPP.VALIDATELOANCSP (p_retval            OUT NUMBER,
                                                     p_loanappnmb     IN     NUMBER DEFAULT NULL,
                                                     p_transind       IN     NUMBER DEFAULT NULL,
                                                     p_cleardataind   IN     VARCHAR DEFAULT 'Y',
                                                     p_selcur1           OUT SYS_REFCURSOR,
                                                     p_selcur2           OUT SYS_REFCURSOR) AS
    /*
     Stored Procedure: LOANAPP.VALIDATELOANCSP
     Database        : LOANAPP
     Purpose         : To check for all validations.
     Date            :
     Author          :
     Authors Note    :
     Parameters      :
     -- --.
    ********************* REVISION HISTORY ****************************************
     Revised By   Date             Description
     ----------  ----------     -----------------------------------------------
    APK          02/08/2011     Modified to add new validation VALIDATEELIGCSP
                                exceptions
    APK         02/07/2012     Modified to add new procedure VALIDATEAPPCACSP
    APK         02/08/2012     Modified to exclude VALIDATEAPPCACSP if process method is
                                not    CAI
    APK         05/22/2012    Please create following two new validations for LAI loan.
                              1.    Credit Score is Mandatory.
                              2.    Credit Score needs to be greater than 140.
                              We already have parameters for the processing methods and
                              credit score limits from ARC loans implementation.
                              They are:CrdScrThrshld:  This holds the lower limit and will
                              have to be set to 140.
    APK         05/22/2012    Modified to change the PrcsMthdCd from SLA to LAI as it was determined that the correct code is LAI.
    SP          05/02/2013    Commented the code for error 1037 to allow poor score loans
    SP          12/26/2013    Added call to LOANAPP.VALIDATEINTDTLCSP
    SP          01/08/2014    Modified error 1036 to check for PrcsMthdReqLqdCr value
    RY          05/16/2016    Added new validation 'ValidateLoanChrgofCsp'--OPSM DEV-835
    RY          14/11/2016 --OPSM DEV 1245   Added new validation 'validateloanpymntcsp'
    SS-         07/25/2018 OPSMDEV 1862 Added Validateagntcsp
    RY-- 08/12/2018--OPSMDEV-1876-- Added new Sp call LOANAPP.VALIDATERURALINITIATVCSP
    SB - 07/19/2020 Added v_PrcsMthdCd IN ('CAI','CRL')
    SL - 01/04/2021 SODSTORY-344.  Added new sp call to loanApp.VALIDATELENRCMNTCSP to validate lender comments
    **************** END OF COMMENTS ******************************************
    */

    v_errseqnmb             NUMBER (10, 0);
    v_loanapprecvdt         DATE;
    v_prcsmthdcd            CHAR (3);
    v_recovind              CHAR (1);
    v_crdscrthrshldmin      NUMBER (10, 0);
    v_lqdcrtotscr           CHAR (3);
    v_imprmtrvalchk         VARCHAR (255);
    v_imprmtrid             VARCHAR2 (255);
    v_loanapprqstamt        NUMBER (19, 4);
    v_prcsmthdreqlqdcramt   NUMBER (19, 4);
    v_loanagntseqnmb        NUMBER (10);
BEGIN
    p_retval := 0;
    v_loanapprecvdt := SYSDATE;
    v_errseqnmb := 1;

    IF    p_cleardataind IS NULL
       OR p_cleardataind = 'Y' THEN
        BEGIN
            DELETE FROM loanerrtbl
            WHERE loanappnmb = p_loanappnmb;
        END;
    END IF;

    BEGIN
        SELECT NVL (MAX (errseqnmb), 0) + 1
        INTO v_errseqnmb
        FROM loanapp.loanerrtbl e
        WHERE e.loanappnmb = p_loanappnmb;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_errseqnmb := 1;
    END;

    BEGIN
        SELECT prcsmthdcd,
               NVL (loanapprecvdt, SYSDATE),
               recovind,
               loanapprqstamt
        INTO v_prcsmthdcd,
             v_loanapprecvdt,
             v_recovind,
             v_loanapprqstamt
        FROM loanapptbl a
        WHERE a.loanappnmb = p_loanappnmb;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_prcsmthdcd := NULL;
    END;

    validateappcsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
    validateappprtcsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
    validateintdtlcsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
    validateprocdcsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
    validatespcpurpscsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
    validateeligcsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind);

    IF v_prcsmthdcd != 'SMP' THEN
		validateappprojcsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validateborrcsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validatebuscsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validatebusprincsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validatepercsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validateprincsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validateprinracecsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
    END IF;
    
    if v_prcsmthdcd NOT IN('PPP','PPS') THEN
    	validatelenrcmntcsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
    	validatebscsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validatecrdtunavrsncsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validatecollatcsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validatecollatliencsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validateiscsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validateguarcsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validatefedempcsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validateindbtnescsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validateinjctncsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validateperfinancsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validateprevfinancsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validatepartlendrcsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
		validateloanchrgofcsp (p_loanappnmb, v_errseqnmb, p_transind);
		validateloanpymntcsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind);
		validateloanexprtcntrycsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind);
		validateagntcsp (p_retval, p_loanappnmb, v_errseqnmb, p_transind);
    END IF;

    IF v_prcsmthdcd IN ('CAI', 'CRL') THEN
        loanapp.validateappcacsp (p_loanappnmb, p_transind, v_loanapprecvdt, v_errseqnmb, p_retval);
    END IF;
    
    if v_prcsmthdcd='504' THEN
        loanapp.validatesubprocdcsp  (p_retval, p_loanappnmb, v_errseqnmb, p_transind, v_loanapprecvdt);
    end if;

    OPEN p_selcur1 FOR SELECT errseqnmb, errcd, errtxt
                       FROM loanerrtbl
                       WHERE     loanappnmb = p_loanappnmb
                             AND errtypind = 'F'
                       -- ORDER BY ErrSeqNmb
                       ORDER BY loansectntypcd;

    OPEN p_selcur2 FOR SELECT errseqnmb, errcd, errtxt
                       FROM loanerrtbl
                       WHERE     loanappnmb = p_loanappnmb
                             AND errtypind = 'W'
                       -- ORDER BY ErrSeqNmb
                       ORDER BY loansectntypcd;

    p_retval := NVL (p_retval, 0);
--dbms_output.put_line(29);
END;
/


GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO CDCONLINEDEVROLE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO CNTRFHINES;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO CNTRILIBERMAN;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO CNTRNNAMILAE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO CNTRNSALATOVA;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO CNTRSJUSTIN;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO CNTRSMANIAM;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO LOANACCTREADALLROLE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO LOANAPPREADALLROLE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO LOANORIGBORROWER;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO LOANREADALLROLE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO LOANUPDROLE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO PARTNERREADALLROLE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO PARTNERUPDCDROLE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO POOLPIMSREADROLE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO POOLPIMSUPDROLE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO POOLSECADMINROLE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO SBACDUPDTROLE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO SBAREFREADALLROLE;

GRANT EXECUTE ON LOANAPP.VALIDATELOANCSP TO SBAREFUPDCDROLE;

DROP PROCEDURE LOANAPP.VALIDATEAGNTCSP;

CREATE OR REPLACE PROCEDURE LOANAPP.VALIDATEAGNTCSP (p_RetVal         OUT NUMBER,
                                                     p_LoanAppNmb  IN     NUMBER DEFAULT NULL,
                                                  
                                                     p_ErrSeqNmb   IN OUT NUMBER,
                                                     p_TransInd    IN     NUMBER DEFAULT NULL)
AS
--SS--07/26/2018--OPSMDEV1862
--BR--10/26/2018--CAFSOPER2199- removed NOT from NULL analysis for both LOANAGNTAPPCNTPAIDAMT and LOANAGNTSBALENDRPAIDAMT because error should only fires if BOTH are null;
--SS--11/15/2018--Added fuction for encryption
--BR--12/12/2018--CAFSOPER2322--Added  'B' to if then else list for v_LOANAGNTBUSPERIND for error code 4173, 4174

   v_LoanAgntSeqNmb          NUMBER (10);
   v_LOANAGNTBUSPERIND       CHAR (1);
   v_LOANAGNTNM              VARCHAR2 (200);
   v_LOANAGNTCNTCTFIRSTNM    VARCHAR2 (40);
   v_LOANAGNTCNTCTMIDNM      CHAR (1);
   v_LOANAGNTCNTCTLASTNM     VARCHAR2 (40);
   v_LOANAGNTCNTCTSFXNM      VARCHAR2 (4);
   v_LOANAGNTADDRSTR1NM      VARCHAR2 (80);
   v_LOANAGNTADDRSTR2NM      VARCHAR2 (80);
   v_LOANAGNTADDRCTYNM       VARCHAR2 (40);
   v_LOANAGNTADDRSTCD        CHAR (2);
   v_LOANAGNTADDRSTNM        VARCHAR2 (60);
   v_LOANAGNTADDRZIPCD       CHAR (5);
   v_LOANAGNTADDRZIP4CD      CHAR (4);
   v_LOANAGNTADDRPOSTCD      VARCHAR2 (20);
   v_LOANAGNTADDRCNTCD       CHAR (2);
   v_LOANAGNTTYPCD           NUMBER (3);
   v_LOANAGNTDOCUPLDIND      CHAR (1);
   v_LOANCDCTPLFEEIND        CHAR (1);
   v_LOANCDCTPLFEEAMT        NUMBER (15, 2);
   v_PRGMCD                  CHAR (2);
   v_LOANAGNTSERVTYPCD       NUMBER (3);
   v_ChkVal                  NUMBER;
   v_LOANAGNTAPPCNTPAIDAMT   NUMBER (15, 2);
   v_LOANAGNTSBALENDRPAIDAMT NUMBER (15, 2);
   v_chkval1                 NUMBER;
   v_chkval2                 NUMBER;
   v_LOANAGNTSERVOTHTYPTXT   VARCHAR2 (80);
   v_feedtlcnt               NUMBER;
   v_LoanAgntSeqNmb1         NUMBER (10);
   v_LOANAGNTINVLVDIND       VARCHAR2(1);


   CURSOR Agnt_Cur
   IS
      SELECT LoanAgntSeqNmb,
             LOANAGNTBUSPERIND,
             LOANAGNTNM,
             ocadataout(LOANAGNTCNTCTFIRSTNM) LOANAGNTCNTCTFIRSTNM, 
             LOANAGNTCNTCTMIDNM,
             ocadataout(LOANAGNTCNTCTLASTNM) LOANAGNTCNTCTLASTNM,
             LOANAGNTCNTCTSFXNM,
             ocadataout(LOANAGNTADDRSTR1NM) LOANAGNTADDRSTR1NM, 
             ocadataout(LOANAGNTADDRSTR2NM) LOANAGNTADDRSTR2NM, 
             LOANAGNTADDRCTYNM,
             LOANAGNTADDRSTCD,
             LOANAGNTADDRSTNM,
             LOANAGNTADDRZIPCD,
             LOANAGNTADDRZIP4CD,
             LOANAGNTADDRPOSTCD,
             LOANAGNTADDRCNTCD,
             LOANAGNTTYPCD,
             LOANAGNTDOCUPLDIND,
             LOANCDCTPLFEEIND,
             LOANCDCTPLFEEAMT
             
      FROM   loanagnttbl 
      WHERE  loanappnmb = p_loanappnmb;
     
    
BEGIN
   SAVEPOINT ValidateAgnt;

   BEGIN
      SELECT PRGMCD
      INTO   v_PRGMCD
      FROM   LoanAppTbl
      WHERE  LoanAppNmb = p_LoanAppNmb;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         v_PRGMCD := NULL;
   END;

   BEGIN
     SELECT LOANAGNTINVLVDIND
     INTO   v_LOANAGNTINVLVDIND
     FROM   LoanApp.LoanAppPrtTbl a
     WHERE  a.LoanappNmb = p_LoanAppNmb;
   EXCEPTION
     WHEN OTHERS THEN
      v_LOANAGNTINVLVDIND:= 'N';
   END;
      
   IF v_LOANAGNTINVLVDIND = 'Y' THEN
   BEGIN

   OPEN Agnt_Cur;

   FETCH Agnt_Cur
      INTO v_LoanAgntSeqNmb,
           v_LOANAGNTBUSPERIND,
           v_LOANAGNTNM,
           v_LOANAGNTCNTCTFIRSTNM,
           v_LOANAGNTCNTCTMIDNM,
           v_LOANAGNTCNTCTLASTNM,
           v_LOANAGNTCNTCTSFXNM,
           v_LOANAGNTADDRSTR1NM,
           v_LOANAGNTADDRSTR2NM,
           v_LOANAGNTADDRCTYNM,
           v_LOANAGNTADDRSTCD,
           v_LOANAGNTADDRSTNM,
           v_LOANAGNTADDRZIPCD,
           v_LOANAGNTADDRZIP4CD,
           v_LOANAGNTADDRPOSTCD,
           v_LOANAGNTADDRCNTCD,
           v_LOANAGNTTYPCD,
           v_LOANAGNTDOCUPLDIND,
           v_LOANCDCTPLFEEIND,
           v_LOANCDCTPLFEEAMT;
         

   BEGIN

      SELECT COUNT (loanappnmb)
      INTO   v_chkval1
      FROM   loanapp.loanappprttbl a
      WHERE      a.loanappnmb = p_loanappnmb
             AND a.LOANAGNTINVLVDIND = 'Y'
             AND (   v_PRGMCD = 'A'
                  OR v_PRGMCD = 'E')
             AND a.loanappnmb NOT IN (SELECT Loanappnmb FROM LOANAPP.LOANAGNTTBL);

      IF v_chkval1 > 0
      THEN
         p_ErrSeqNmb :=
            ADD_ERROR (
                       p_LoanAppNmb,
                       p_ErrSeqNmb,
                       4182,
                       p_TransInd,
                       TO_CHAR (v_LoanAGNTSeqNmb),
                       NULL,
                       NULL,
                       NULL,
                       NULL
                      );
       END IF;
    END;
  END;

   v_feedtlcnt := 0;

   FOR c2 IN (SELECT loanagntservtypcd,
                     LOANAGNTSERVOTHTYPTXT,
                     LOANAGNTAPPCNTPAIDAMT,
                     LOANAGNTSBALENDRPAIDAMT,
                     LoanAgntSeqNmb
              FROM   LoanAgntFeeDtlTbl
              WHERE      Loanappnmb = p_Loanappnmb
                     --  and   LOANAGNTSeqNmb = p_LOANAGNTSeqNmb
                     AND (   LOANAGNTAPPCNTPAIDAMT IS NOT NULL
                          OR LOANAGNTSBALENDRPAIDAMT IS NOT NULL))
   LOOP
      v_feedtlcnt := v_feedtlcnt + 1;

      IF     (c2.LOANAGNTAPPCNTPAIDAMT IS NULL
         AND c2.LOANAGNTSBALENDRPAIDAMT IS NULL)
      THEN
         NULL;
         p_ErrSeqNmb :=
            ADD_ERROR (
                       p_LoanAppNmb,
                       p_ErrSeqNmb,
                       4172,
                       p_TransInd,
                       TO_CHAR (c2.LoanAgntSeqNmb),
                       Null,
                       NULL,
                       NULL,
                       NULL
                      );
      END IF;
      --  END;--add error for both amount is not null
      -- END IF;

      IF     c2.loanagntservtypcd = 5
         AND c2.LOANAGNTSERVOTHTYPTXT IS NULL
      THEN
         p_ErrSeqNmb :=
            ADD_ERROR (
                       p_LoanAppNmb,
                       p_ErrSeqNmb,
                       4181,
                       p_TransInd,
                       TO_CHAR (c2.LoanAgntSeqNmb),
                       NULL,
                       NULL,
                       NULL,
                       NULL
                      );
      END IF;
   --   END IF;
  END LOOP;
  
  
 
   
  



 WHILE (Agnt_Cur%FOUND)
   LOOP
  

      IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND = 'B'
         AND v_LOANAGNTNM IS NULL
      THEN
         p_ErrSeqNmb :=
            ADD_ERROR (
                       p_LoanAppNmb,
                       p_ErrSeqNmb,
                       4170,
                       p_TransInd,
                       TO_CHAR (v_LoanAgntSeqNmb),
                       NULL,
                       NULL,
                       NULL,
                       NULL
                      );
      END IF;
      IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND IN ('B', 'P')
         AND v_LOANAGNTTYPCD IS NULL
      THEN
         p_ErrSeqNmb :=
            ADD_ERROR (
                       p_LoanAppNmb,
                       p_ErrSeqNmb,
                       4171,
                       p_TransInd,
                       TO_CHAR (v_LoanAgntSeqNmb),
                       NULL,
                       NULL,
                       NULL,
                       NULL
                      );
      END IF;

      IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND in('P', 'B')
         AND v_LOANAGNTCNTCTFIRSTNM IS NULL
      THEN
         p_ErrSeqNmb :=
            ADD_ERROR (
                       p_LoanAppNmb,
                       p_ErrSeqNmb,
                       4173,
                       p_TransInd,
                       TO_CHAR (v_LoanAgntSeqNmb),
                       NULL,
                       NULL,
                       NULL,
                       NULL
                      );
      END IF;

      IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND in('P', 'B')
         AND v_LOANAGNTCNTCTLASTNM IS NULL
      THEN
         p_ErrSeqNmb :=
            ADD_ERROR (
                       p_LoanAppNmb,
                       p_ErrSeqNmb,
                       4174,
                       p_TransInd,
                       TO_CHAR (v_LoanAgntSeqNmb),
                       NULL,
                       NULL,
                       NULL,
                       NULL
                      );
      END IF;

      IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND IN ('B', 'P')
         AND v_LOANAGNTADDRSTR1NM IS NULL
      THEN
         p_ErrSeqNmb :=
            ADD_ERROR (
                       p_LoanAppNmb,
                       p_ErrSeqNmb,
                       4175,
                       p_TransInd,
                       TO_CHAR (v_LoanAgntSeqNmb),
                       NULL,
                       NULL,
                       NULL,
                       NULL
                      );
      END IF;
      IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND IN ('B', 'P')
         AND v_LOANAGNTADDRCTYNM IS NULL
      THEN
         p_ErrSeqNmb :=
            ADD_ERROR (
                       p_LoanAppNmb,
                       p_ErrSeqNmb,
                       4176,
                       p_TransInd,
                       TO_CHAR (v_LoanAgntSeqNmb),
                       NULL,
                       NULL,
                       NULL,
                       NULL
                      );
      END IF;
      IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND IN ('B', 'P')
         AND v_LOANAGNTADDRSTCD IS NULL
      THEN
         p_ErrSeqNmb :=
            ADD_ERROR (
                       p_LoanAppNmb,
                       p_ErrSeqNmb,
                       4177,
                       p_TransInd,
                       TO_CHAR (v_LoanAgntSeqNmb),
                       NULL,
                       NULL,
                       NULL,
                       NULL
                      );
      END IF;
      IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND IN ('B', 'P')
         AND v_LOANAGNTADDRZIPCD IS NULL
      THEN
         p_ErrSeqNmb :=
            ADD_ERROR (
                       p_LoanAppNmb,
                       p_ErrSeqNmb,
                       4178,
                       p_TransInd,
                       TO_CHAR (v_LoanAgntSeqNmb),
                       NULL,
                       NULL,
                       NULL,
                       NULL
                      );
      END IF;
      IF     v_PRGMCD IN ('A', 'E')
         AND v_LOANAGNTBUSPERIND IN ('B', 'P')
         AND v_LOANAGNTADDRZIP4CD IS NULL
      THEN
         p_ErrSeqNmb :=
            ADD_ERROR (
                       p_LoanAppNmb,
                       p_ErrSeqNmb,
                       4179,
                       p_TransInd,
                       TO_CHAR (v_LoanAgntSeqNmb),
                       NULL,
                       NULL,
                       NULL,
                       NULL
                      );
      END IF;
      IF     v_PRGMCD IN ('E')
         AND v_LOANAGNTBUSPERIND IN ('B', 'P')
         AND v_LOANCDCTPLFEEIND = 'Y'
      THEN
         IF v_LOANCDCTPLFEEAMT IS NULL
         THEN
            p_ErrSeqNmb :=
               ADD_ERROR (
                          p_LoanAppNmb,
                          p_ErrSeqNmb,
                          4180,
                          p_TransInd,
                          TO_CHAR (v_LoanAgntSeqNmb),
                          NULL,
                          NULL,
                          NULL,
                          NULL
                         );
         END IF;
      END IF;

     
      FETCH Agnt_cur
         INTO v_LoanAgntSeqNmb,
              v_LOANAGNTBUSPERIND,
              v_LOANAGNTNM,
              v_LOANAGNTCNTCTFIRSTNM,
              v_LOANAGNTCNTCTMIDNM,
              v_LOANAGNTCNTCTLASTNM,
              v_LOANAGNTCNTCTSFXNM,
              v_LOANAGNTADDRSTR1NM,
              v_LOANAGNTADDRSTR2NM,
              v_LOANAGNTADDRCTYNM,
              v_LOANAGNTADDRSTCD,
              v_LOANAGNTADDRSTNM,
              v_LOANAGNTADDRZIPCD,
              v_LOANAGNTADDRZIP4CD,
              v_LOANAGNTADDRPOSTCD,
              v_LOANAGNTADDRCNTCD,
              v_LOANAGNTTYPCD,
              v_LOANAGNTDOCUPLDIND,
              v_LOANCDCTPLFEEIND,
              v_LOANCDCTPLFEEAMT;
             
   END LOOP;

   CLOSE Agnt_Cur;

END IF;
   p_RetVal := NVL (p_RetVal, 0);
   p_ErrSeqNmb := NVL (p_ErrSeqNmb, 0);
END VALIDATEAGNTCSP;
/


GRANT EXECUTE ON LOANAPP.VALIDATEAGNTCSP TO CDCONLINEDEVROLE;

GRANT EXECUTE ON LOANAPP.VALIDATEAGNTCSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOANAPP.VALIDATEAGNTCSP TO LOANACCTREADALLROLE;

GRANT EXECUTE ON LOANAPP.VALIDATEAGNTCSP TO LOANAPPREADALLROLE;

GRANT EXECUTE ON LOANAPP.VALIDATEAGNTCSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOANAPP.VALIDATEAGNTCSP TO LOANDEVROLE;

GRANT EXECUTE ON LOANAPP.VALIDATEAGNTCSP TO LOANORIGBORROWER;

GRANT EXECUTE ON LOANAPP.VALIDATEAGNTCSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOANAPP.VALIDATEAGNTCSP TO LOANREADALLROLE;

GRANT EXECUTE ON LOANAPP.VALIDATEAGNTCSP TO LOANUPDROLE;

GRANT EXECUTE ON LOANAPP.VALIDATEAGNTCSP TO POOLSECADMINROLE;

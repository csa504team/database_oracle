CREATE OR REPLACE PROCEDURE LOANAPP.LoanAGNTSELTSP (p_Identifier       IN     NUMBER := 0,
                                                    p_LoanAppNmb       IN     NUMBER := 0,
                                                    p_LoanAgntSeqNmb   IN     NUMBER := 0,
                                                    p_SelCur              OUT SYS_REFCURSOR,
                                                    p_RetVal              OUT NUMBER)
AS
/*SS--OPSMDEV1862--07/16/2018--Collecting Agent data
SS--OPSMDEV 1874 added identifier 13
SS--11/15/2018 added function for encryption

*/

BEGIN
    IF p_Identifier = 0
    THEN
        BEGIN
            OPEN p_SelCur FOR SELECT LOANAGNTID,
                                     LOANAPPNMB,
                                     LoanAgntSeqNmb,
                                     LOANAGNTBUSPERIND,
                                     LOANAGNTNM,
                                     ocadataout (LOANAGNTCNTCTFIRSTNM)     LOANAGNTCNTCTFIRSTNM,
                                     LOANAGNTCNTCTMIDNM,
                                     ocadataout (LOANAGNTCNTCTLASTNM)      LOANAGNTCNTCTLASTNM,
                                     LOANAGNTCNTCTSFXNM,
                                     ocadataout (LOANAGNTADDRSTR1NM)       LOANAGNTADDRSTR1NM,
                                     ocadataout (LOANAGNTADDRSTR2NM)       LOANAGNTADDRSTR2NM,
                                     LOANAGNTADDRCTYNM,
                                     LOANAGNTADDRSTCD,
                                     LOANAGNTADDRSTNM,
                                     LOANAGNTADDRZIPCD,
                                     LOANAGNTADDRZIP4CD,
                                     LOANAGNTADDRPOSTCD,
                                     LOANAGNTADDRCNTCD,
                                     LOANAGNTTYPCD,
                                     LOANAGNTDOCUPLDIND,
                                     LOANCDCTPLFEEIND,
                                     LOANCDCTPLFEEAMT,
                                     LOANAGNTOTHTYPTXT,
                                     CreatUserId,
                                     CreatDt,
                                     LastUpdtUserId,
                                     LastUpdtDt
                                FROM LOANAPP.LoanAgntTbl
                               WHERE LOANAPPNMB = p_LOANAPPNMB;

            p_RetVal := SQL%ROWCOUNT;
        END;
    ELSIF p_Identifier = 2
    THEN
        BEGIN
            OPEN p_SelCur FOR SELECT 1
                                FROM LOANAPP.LoanAgntTbl
                               WHERE LoanAppNmb = p_LoanAppNmb AND LoanAgntSeqNmb = p_LoanAgntSeqNmb;

            p_RetVal := SQL%ROWCOUNT;
        END;
    ELSIF p_Identifier = 11
    THEN
        BEGIN
            OPEN p_SelCur FOR SELECT LOANAGNTID,
                                     LOANAPPNMB,
                                     LoanAgntSeqNmb,
                                     LOANAGNTBUSPERIND,
                                     LOANAGNTNM,
                                     ocadataout (LOANAGNTCNTCTFIRSTNM)     LOANAGNTCNTCTFIRSTNM,
                                     LOANAGNTCNTCTMIDNM,
                                     ocadataout (LOANAGNTCNTCTLASTNM)      LOANAGNTCNTCTLASTNM,
                                     LOANAGNTCNTCTSFXNM,
                                     ocadataout (LOANAGNTADDRSTR1NM)       LOANAGNTADDRSTR1NM,
                                     ocadataout (LOANAGNTADDRSTR2NM)       LOANAGNTADDRSTR2NM,
                                     LOANAGNTADDRCTYNM,
                                     LOANAGNTADDRSTCD,
                                     LOANAGNTADDRSTNM,
                                     LOANAGNTADDRZIPCD,
                                     LOANAGNTADDRZIP4CD,
                                     LOANAGNTADDRPOSTCD,
                                     LOANAGNTADDRCNTCD,
                                     LOANAGNTTYPCD,
                                     LOANAGNTDOCUPLDIND,
                                     LOANCDCTPLFEEIND,
                                     LOANCDCTPLFEEAMT,
                                     LOANAGNTOTHTYPTXT,
                                     CreatUserId,
                                     CreatDt,
                                     LastUpdtUserId,
                                     LastUpdtDt                                     
                                FROM LOANAPP.LoanAgntTbl
                               WHERE LOANAPPNMB = p_LOANAPPNMB AND LoanAgntSeqNmb = p_LoanAgntSeqNmb;

            p_RetVal := SQL%ROWCOUNT;
        END;
    ELSIF p_Identifier = 12
    THEN
        BEGIN
            OPEN p_SelCur FOR SELECT LOANAGNTID,
                                     LOANAPPNMB,
                                     LoanAgntSeqNmb,
                                     LOANAGNTBUSPERIND,
                                     LOANAGNTNM,
                                     ocadataout (LOANAGNTCNTCTFIRSTNM)     LOANAGNTCNTCTFIRSTNM,
                                     LOANAGNTCNTCTMIDNM,
                                     ocadataout (LOANAGNTCNTCTLASTNM)      LOANAGNTCNTCTLASTNM,
                                     LOANAGNTCNTCTSFXNM,
                                     ocadataout (LOANAGNTADDRSTR1NM)       LOANAGNTADDRSTR1NM,
                                     ocadataout (LOANAGNTADDRSTR2NM)       LOANAGNTADDRSTR2NM,
                                     LOANAGNTADDRCTYNM,
                                     LOANAGNTADDRSTCD,
                                     LOANAGNTADDRSTNM,
                                     LOANAGNTADDRZIPCD,
                                     LOANAGNTADDRZIP4CD,
                                     LOANAGNTADDRPOSTCD,
                                     LOANAGNTADDRCNTCD,
                                     LOANAGNTTYPCD,
                                     LOANAGNTDOCUPLDIND,
                                     LOANCDCTPLFEEIND,
                                     LOANCDCTPLFEEAMT,
                                     LOANAGNTOTHTYPTXT,
                                     CreatUserId,
                                     CreatDt,
                                     LastUpdtUserId,
                                     LastUpdtDt
                                    
                                FROM LOANAPP.LoanAgntTbl 
                               WHERE LOANAPPNMB = p_LOANAPPNMB AND LoanAgntSeqNmb > NVL (p_LoanAgntSeqNmb, 0);
                              

            p_RetVal := SQL%ROWCOUNT;
        END;
    ELSIF p_identifier = 13
    THEN
        BEGIN
            OPEN p_SelCur FOR
                SELECT a.LOANAPPNMB,
                       a.LOANAGNTTYPCD,
                       a.LOANAGNTBUSPERIND,
                       a.LOANAGNTNM,
                          ocadataout (a.LOANAGNTCNTCTFIRSTNM)
                       || RTRIM (' ' || a.LOANAGNTCNTCTMidNm)
                       || ' '
                       || ocadataout (a.LOANAGNTCNTCTLastNm)
                       || RTRIM (' ' || a.LOANAGNTCNTCTSfxNm)
                           AS LoanContactName,
                          ocadataout (a.LOANAGNTADDRSTR1NM)
                       || RTRIM (' ' || ocadataout (a.LOANAGNTADDRSTR2NM))
                       || ' '
                       || a.LOANAGNTADDRCTYNM
                       || ' '
                       || RTRIM (' ' || a.LOANAGNTADDRSTCD)
                       || ' '
                       || a.LOANAGNTADDRZIPCD
                           AS LoanContactaddress,
                       a.LOANCDCTPLFEEIND,
                       a.LOANCDCTPLFEEAMT,
                       a.LOANAGNTOTHTYPTXT,
                       b.LOANAGNTSERVTYPCD,
                       b.LOANAGNTSERVOTHTYPTXT,
                       b.LOANAGNTAPPCNTPAIDAMT,
                       b.LOANAGNTSBALENDRPAIDAMT,
                       c.Prgmcd,
                       c.LoanAppNm,
                       c.LoanNmb,
                       d.LoanAppPrtNm,
                       e.loanpartlendrtypcd,
                       e.loanpartlendrnm,
                          e.loanpartlendrstr1nm
                       || RTRIM (' ' || e.loanpartlendrstr2nm)
                       || ' '
                       || e.loanpartlendrctynm
                       || ' '
                       || RTRIM (' ' || e.loanpartlendrstcd)
                       || ' '
                       || e.loanpartlendrcntrycd
                       || ' '
                       || RTRIM (' ' || e.loanpartlendrzip5cd)
                           AS LendrAddress,
                       e.LOCID
                  FROM loanagnttbl        a,
                       loanagntfeedtltbl  b,
                       loanapptbl         c,
                       LoanAppPrtTbl      d,
                       loanpartlendrtbl   e
                 WHERE     a.loanappnmb = b.loanappnmb
                       AND b.loanappnmb = c.loanappnmb
                       AND a.LOANAPPNMB = p_LOANAPPNMB
                       AND a.loanappnmb = d.loanappnmb
                       AND e.LOANAPPNMB(+) = a.LOANAPPNMB
                       AND a.LoanAgntSeqNmb = p_LoanAgntSeqNmb
                       AND a.LoanAgntSeqNmb = b.LoanAgntSeqNmb;
        END;
    ELSIF p_Identifier = 38
    THEN
        BEGIN
            OPEN p_SelCur FOR SELECT LOANAGNTID,
                                     LOANAPPNMB,
                                     LoanAgntSeqNmb,
                                     LOANAGNTBUSPERIND,
                                     LOANAGNTNM,
                                     ocadataout (LOANAGNTCNTCTFIRSTNM)                      LOANAGNTCNTCTFIRSTNM,
                                     LOANAGNTCNTCTMIDNM,
                                     ocadataout (LOANAGNTCNTCTLASTNM)                       LOANAGNTCNTCTLASTNM,
                                     LOANAGNTCNTCTSFXNM,
                                     ocadataout (LOANAGNTADDRSTR1NM)                        LOANAGNTADDRSTR1NM,
                                     ocadataout (LOANAGNTADDRSTR2NM)                        LOANAGNTADDRSTR2NM,
                                     LOANAGNTADDRCTYNM,
                                     LOANAGNTADDRSTCD,
                                     LOANAGNTADDRSTNM,
                                     LOANAGNTADDRZIPCD,
                                     LOANAGNTADDRZIP4CD,
                                     LOANAGNTADDRPOSTCD,
                                     LOANAGNTADDRCNTCD,
                                     LOANAGNTTYPCD,
                                     LOANAGNTDOCUPLDIND,
                                     LOANCDCTPLFEEIND,
                                     TO_CHAR (LOANCDCTPLFEEAMT, 'FM999999999999990.00')     LOANCDCTPLFEEAMT,
                                     LOANAGNTOTHTYPTXT
                                FROM LOANAPP.LoanAgntTbl
                               WHERE LOANAPPNMB = p_LOANAPPNMB;

            p_RetVal := SQL%ROWCOUNT;
        END;
    END IF;
END;
/

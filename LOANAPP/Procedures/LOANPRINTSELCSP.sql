CREATE OR REPLACE PROCEDURE LOANAPP.LOANPRINTSELCSP (
    p_LoanAppNmb   IN     NUMBER DEFAULT NULL,
    p_SelCur1         OUT SYS_REFCURSOR,
    p_SelCur2         OUT SYS_REFCURSOR,
    p_SelCur3         OUT SYS_REFCURSOR,
    p_SelCur4         OUT SYS_REFCURSOR,
    p_SelCur5         OUT SYS_REFCURSOR,
    p_SelCur6         OUT SYS_REFCURSOR,
    p_SelCur7         OUT SYS_REFCURSOR,
    p_SelCur8         OUT SYS_REFCURSOR,
    p_SelCur9         OUT SYS_REFCURSOR,
    p_SelCur10        OUT SYS_REFCURSOR,
    p_SelCur11        OUT SYS_REFCURSOR,
    p_SelCur12        OUT SYS_REFCURSOR,
    p_SelCur13        OUT SYS_REFCURSOR,
    p_SelCur14        OUT SYS_REFCURSOR,
    p_SelCur15        OUT SYS_REFCURSOR,
    p_SelCur16        OUT SYS_REFCURSOR,
    p_SelCur17        OUT SYS_REFCURSOR,
    p_SelCur18        OUT SYS_REFCURSOR,
    p_SelCur19        OUT SYS_REFCURSOR,
    p_SelCur20        OUT SYS_REFCURSOR,
    p_SelCur21        OUT SYS_REFCURSOR,
    p_SelCur22        OUT SYS_REFCURSOR,
    p_SelCur23        OUT SYS_REFCURSOR,
    p_SelCur24        OUT SYS_REFCURSOR,
    p_SelCur25        OUT SYS_REFCURSOR,
    p_SelCur26        OUT SYS_REFCURSOR,
    p_SelCur27        OUT SYS_REFCURSOR,
    p_SelCur28        OUT SYS_REFCURSOR,
    p_SelCur29        OUT SYS_REFCURSOR,
    p_SelCur30        OUT SYS_REFCURSOR,
    p_SelCur31        OUT SYS_REFCURSOR,
    p_SelCur32        OUT SYS_REFCURSOR)
AS
    /*  Date     :
    Programmer   :
    Remarks      :
    ***********************************************************************************
    Revised By      Date        Comments
    ----------     --------    ----------------------------------------------------------
    APK         03/15/2011  Modified to add new result sets 22-25
    APK         03/21/2011  Modified to add proper exception handlers.
    SP          12/7/2011   Modified to remove LOANCOLLATLQDVALAMT.
    APK -- 02/06/2012 -- modified to add new column LOANDISASTRAPPNMB, as its added to the loanapptbl in development.
    SP          06/10/2013  Modified to add loanaffilind to result sets 13,15,16
    RSURAPA -- 10/08/2013 -- Removed references to all the strnmb and strsfxnm columns
    RGG     --  11/05/2013  -- Modified to add new column LOANAPPINTDTLCD
    SP -- 11/27/2013 -- Removed references to columns :IMRtTypCd,LoanAppFixVarInd,
    LoanAppBaseRt,LoanAppInitlIntPct,LoanAppInitlSprdOvrPrimePct,LoanAppOthAdjPrdTxt,LoanAppAdjPrdCd from p_SelCur4  and added cursor p_SelCur27 to return these values from LoanIntDtlTbl
    RGG -- 02/12/2014 -- Added LACGNTYCONDTYPCD to some result sets to fix an issue with Printing
    SP  -- 04/07/2014 -- Modified to get NAICSYRNMB,NAICSCD,LOANAPPFRNCHSCD,LOANAPPFRNCHSNM,LoanAppFrnchsInd from loanappprojtbl instead of loanapptbl
    RY --- 04/04/2016 --- Added LOANAGNTINVLVDIND in p_SelCur2 as it is added in loanappprttbl
    NK --08/05/2016--OPSMDEV1018-- Replaced LOANAFFILIND with LoanCntlIntInd and LoanCntlIntTyp
    RGG -- 09/12/2016 -- OPSMDEV 1103 Added new columns for making Use of Proceeds support Loan Auth LOANPROCDREFDESCTXT, LOANPROCDPURAGRMTDT,
                         LOANPROCDPURINTANGASSETAMT, LOANPROCDPURINTANGASSETDESC, LOANPROCDPURSTKHLDRNM

    ********************************************************************************
    */
    /*SP:9/13/2012: Modified as LoanPrtCmntSeqNmb is removed
    SS -- 09/14/2016 added new columns to p_SelCur7  for OPSMDEV 1111Print functionality in ETRAN to account for new columns added in collateral
    NK -- 09/28/2016-- OPSMDEV-1163 added ,  LoanDisblInsurRqrdInd   ,  LoanInsurDisblDescTxt  ,  LoanLifeInsurRqrdInd    , LoanPrinNoNCAEvdncInd   ,LoanComptitrNm
                           in   p_SelCur16.
    NK--09/28/2016--OPSMDEV 1179-- Added LOANINJCTNFundTermYrNmb p_SelCur5.
    NK-- 10/03/2016 --OPSMDEV-1141 added GntySubCondTypCd  , GntyLmtAmt , GntyLmtPct, LoanCollatSeqNmb , GntyLmtYrNmb in   p_SelCur15, p_SelCur16.
    SS---10/14/2016 OPSMDEV1122 Added LoanPymntSchdlFreq,LoanPymtRepayInstlTypCd,LOANPYMNINSTLMNTFREQCD to cursor 4
    SS---10/18/2016 OPSMDEV-1172 added more columns for Project to cursor 10)
    NK--10/24/2016--OPSMDEV-1174 Added  ,a.LiabInsurRqrdInd,a.ProdLiabInsurRqrdInd,a.LiqLiabInsurRqrdInd,a.MalPrctsInsurRqrdInd ,a.OthInsurRqrdind ,a.OthInsurDescTxt,a.PymtLessCrdtInd ,a.InsurDisblDescTxt
    ,a.RecrdStmtTypCd ,a.RecrdStmtYrEndDayNmb in p_SelCur13
    NK--10/25/2016--OPSMDEV 1171-- added NCAIncldInd,StkPurCorpText in p_SelCur6
    NK--11/08/2016--OPSMDEV 1229-- Added p_SelCur30 for StbyAgrmt
    SS--11/16/2016 --OPSMDEV 1170-- Added  CollatAppOrdDt , LoanCollatValDt,COLLATSTATDESCTXT to Cursor7
    SS--11/16/2016 ---OPSMDEV1172-- Added NAICSDESCTXT to cursor 10
    SS--12/09/2016 --OPSMDEV1289 --Added LOANGNTYMATSTDESCTXT to cursor 4
    NK--01/11/2016 -- OPSMDEV 1287 -- Added VetCertInd in p_SelCur16
    NK-- 01/26/2017--OPSMDEV-1337-- Added LoanAppLspLastNm,LoanAppLspFirstNm, LoanAppLspInitialNm, LoanAppLspSfxNm, LoanAppLspTitlTxt, LoanAppLspPhn, LoanAppLspFax, LoanAppLspEMail in p_SelCur2
    NK--03/28/2017-- OPSMDEV 1385-- added LoanEconDevObjctCd in p_SelCur10
    Nk--05/12/2017-- OPSMDEV 1432--Added LOANINTADJPRDEFFDT  in p_Selcur27
    RY-- 06/07/2017--OPSMDEV 1407 --Added loanpartlendrcntrycd,loanpartlendrpostcd,loanpartlendrstnm to p_Selcur20

    --   BR -- 06/14/2017 -- OPSMDEV - 1359 -- Added p_SelCur31
    RY -- 07/24/2017-- CAFSOPER-805 --Added LoanAppCDCGrossDbentrAmt,LoanAppBalToBorrAmt to p_SelCur4.
    NK--10/11/2017-- OPSMDEV 1548 added DISASTRSTDECLCD, DISASTRDESCTXT,DISASTRSTRTDT to p_selCur10.
    BR --11/14/2017--OPSMDEV 1577 -- Added new fields from Loan.loanintdtltbl
    NK --11/14/2017--OPSMDEV 1551 Added SOP Principal questions to p_SelCur13
    NK--11/14/2017--OPSMDEV 1558 Added SOP Borrower Questions to p_SelCur15
    NK-- 11/14/2017--OPSMDEV 1591 Added LoanPrevFinanDelnqCmntTxt to p_SelCur14
    BR--11/14/2017--opsmdev 1577 Added LoanIntDtlGuarInd p_selCur27
    SS--11/15/2017 OPSMDEV 1573 Added LoanPrtCntctCellPhn, LoanPrtAltCntctFirstNm,LoanPrtAltCntctLastNm,LoanPrtAltCntctInitialNm,LoanPrtAltCntctTitlTxt, LoanPrtAltCntctPrimPhn, LoanPrtAltCntctCellPhn,
   LoanPrtAltCntctemail,LendrAltCntctTypCd
   SS--11/15/2017 OPSMDEV 1566 Added LOANBUSESTDT to p_selcur 10 and BusPrimCntctNm,BusPrimCntctEmail to p_selcur 13
   NK-- 11/16/2017 OPSMDEV 1591 Added BusFedAgncyLoanInd, PerFedAgncyLoanInd to p_selcur 13
     --NK--11/16/2017 ---OPSMDEV 1560 Added BusOutDbtInd to p_selcur 13
   NK--12/14/2017--OPSMDEV 1605 added LOANINTDTLDESCTXT  and LoanGntyNoteDt to p_selcur 27
      NK--01/12/2018--OPSMDEV 1644--Added LoanAppRevlMoIntQty, LoanAppAmortMoIntQty  to p_SelCur4
 RY-- 04/20/2018--OPSMDEV--1786--Added WorkrsCompInsRqrdInd to p_SelCur13
 SS--08/03/2018--OPSMDEV--1882--Added p_SelCur 32
 SS--10/25/2018--OPSMDEV 1965 Added  LoanAppExtraServFeeAMT,LoanAppExtraServFeeInd  for extraordinary servicing fee to p_selcur 4
 SS--11/15/2018--Added function for encryption to p_selcur 32
 SS--11/16/2018--Added function to p_selcur 2 for encryption OPSMDEV 2043
 SS--11/20/2018 OPSMDEV 2043 Added function for encryption to p_selcur 18,20
 SS--11/26/2018 OPSMDEV 2043 Added function for encryption
 RY-01/25/2019--OPSMDEV-2087::Added new column PerOthrCmntTxt to PERTBL call
 SS--06/18/2019 --OPSMDEV 2230 Added RqstSeqNmb to p_selcur 4
 SS--04/3/2020 Added LoanAppMonthPayroll  to cursor 4 CARESACT - 63
 SS--04/26/2020 Added ACHAcctNmb,ACHRtngNmb,ACHAcctTypCd,ACHTinNmb to p_selcur 2
 SL 01/04/2021 SODSTORY-317/318.  Added LOANAPPPROJLMIND and LOANAPPPROJSIZECD to p_selcur 10
 JS--02/10/2021 Loan Authorization Modernization. Added LiabInsurRqrdInd,ProdLiabInsurRqrdInd,LiqLiabInsurRqrdInd,MalPrctsInsurRqrdInd,OthInsurRqrdInd,WorkrsCompInsRqrdInd,OthInsurDescTxt to Guarantor - p_SelCur15
 */
    v_ProcdTypCd   CHAR (1);
BEGIN
    BEGIN
        SELECT ProcdTypCd
          INTO v_ProcdTypCd
          FROM LoanAppTbl a, sbaref.PrgrmValidTbl b
         WHERE     LoanAppNmb = p_LoanAppNmb
               AND a.PrgmCd = b.PrgrmCd
               AND a.PrcsMthdCd = b.PrcsMthdCd
               AND (   (    b.SpcPurpsLoanCd IS NULL
                        AND NOT EXISTS
                                (SELECT 1
                                   FROM LoanSpcPurpsTbl
                                  WHERE LoanAppNmb = p_LoanAppNmb))
                    OR EXISTS
                           (SELECT 1
                              FROM LoanSpcPurpsTbl z
                             WHERE     LoanAppNmb = p_LoanAppNmb
                                   AND z.SpcPurpsLoanCd = b.SpcPurpsLoanCd))
               AND ROWNUM = 1;
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            v_ProcdTypCd := NULL;
    END;

    OPEN p_SelCur1 FOR
        SELECT b.SpcPurpsLoanCd, b.SpcPurpsLoanDesc
          FROM LoanSpcPurpsTbl a, sbaref.SpcPurpsLoanTbl b
         WHERE     a.LoanAppNmb = p_LoanAppNmb
               AND a.SpcPurpsLoanCd = b.SpcPurpsLoanCd;

    OPEN p_SelCur2 FOR
        SELECT LocId,
               LoanAppFIRSNmb,
               PrtId,
               LoanAppPrtAppNmb,
               LoanAppPrtLoanNmb,
               LoanAppPrtNm,
               LoanAppPrtStr1,
               LoanAppPrtStr2,
               LoanAppPrtCtyNm,
               LoanAppPrtStCd,
               LoanAppPrtZipCd,
               LoanAppPrtZip4Cd,
               ocadataout (LoanAppCntctLastNm)
                   AS LoanAppCntctLastNm,
               ocadataout (LoanAppCntctFirstNm)
                   AS LoanAppCntctFirstNm,
               LoanAppCntctInitialNm,
               LoanAppCntctSfxNm,
               LoanAppCntctTitlTxt,
               ocadataout (LoanAppCntctPhn)
                   AS LoanAppCntctPhn,
               LoanAppCntctFax,
               ocadataout (LoanAppCntctemail)
                   AS LoanAppCntctemail,
               b.LoanPckgSourcTypCd,
               b.LoanPckgSourcTypDescTxt,
               LoanAppPckgSourcNm,
               LoanAppPckgSourcStr1Nm,
               LoanAppPckgSourcStr2Nm,
               LoanAppPckgSourcCtyNm,
               LoanAppPckgSourcStCd,
               LoanAppPckgSourcZipCd,
               LoanAppPckgSourcZip4Cd,
               LoanAgntInvlvdInd,
               --  LoanAppLspFirstNm,
               --  LoanAppLspInitialNm,
               LoanAppLspSfxNm,
               --  LoanAppLspTitlTxt,
               --   LoanAppLspPhn,
               LoanAppLspFax,
               --  LoanAppLspEMail,
               ocadataout (LoanPrtCntctCellPhn)
                   AS LoanPrtCntctCellPhn,
               ocadataout (LoanPrtAltCntctFirstNm)
                   AS LoanPrtAltCntctFirstNm,
               ocadataout (LoanPrtAltCntctLastNm)
                   AS LoanPrtAltCntctLastNm,
               LoanPrtAltCntctInitialNm,
               LoanPrtAltCntctTitlTxt,
               ocadataout (LoanPrtAltCntctPrimPhn)
                   AS LoanPrtAltCntctPrimPhn,
               ocadataout (LoanPrtAltCntctCellPhn)
                   AS LoanPrtAltCntctCellPhn,
               ocadataout (LoanPrtAltCntctemail)
                   AS LoanPrtAltCntctemail,
               LendrAltCntctTypCd,
               ACHAcctNmb,
               ACHRtngNmb,
               ACHAcctTypCd,
               ACHTinNmb
          FROM LoanAppPrtTbl a, sbaref.LoanPckgSourcTypTbl b
         WHERE     LoanAppNmb = p_LoanAppNmb
               AND a.LoanPckgSourcTypCd = b.LoanPckgSourcTypCd(+);

    OPEN p_SelCur3 FOR
          SELECT a.LoanCrdtUnavRsnSeqNmb,
                 a.LoanCrdtUnavRsnCd,
                 b.LoanCrdtUnavRsnDescTxt,
                 a.LoanCrdtUnavRsnTxt
            FROM LoanCrdtUnavRsnTbl a, sbaref.LoanCrdtUnavRsnCdTbl b
           WHERE     a.LoanAppNmb = p_LoanAppNmb
                 AND a.LoanCrdtUnavRsnCd = b.LoanCrdtUnavRsnCd
        ORDER BY a.LoanCrdtUnavRsnSeqNmb;

    OPEN p_SelCur4 FOR
        SELECT LoanAppNm,
               a.PrgmCd,
               c.PrgrmDesc,
               a.PrcsMthdCd,
               b.PrcsMthdDesc,
               ROUND (TO_NUMBER (LoanAppRqstAmt), 2)
                   LoanAppRqstNum,
               LoanAppSBAGntyPct,
               LoanAppRqstMatMoQty                       --  ,LoanAppFixVarInd
                                  --  ,a.IMRtTypCd
                                  -- ,d.IMRtTypDescTxt
                                  -- ,LoanAppBaseRt
                                  -- ,LoanAppInitlIntPct
                                  -- ,LoanAppInitlSprdOvrPrimePct
                                  -- ,a.LoanAppAdjPrdCd
                                  -- ,e.CalndrPrdDesc
                                  ,
               LoanAppEPCInd,
               ROUND (TO_NUMBER (LoanAppPymtAmt), 2)
                   LoanAppPymtNum,
               LoanAppFullAmortPymtInd,
               LoanAppMoIntQty,
               LoanAppLifInsurRqmtInd,
               LoanAppRcnsdrtnInd,
               LoanAppInjctnInd,
               LoanAppEligEvalInd,
               LoanAppNewBusCd,
               BusAgeDesc
                   LoanAppNewBusTxt,
               NVL ((SELECT ROUND (TO_NUMBER (SUM (LoanPartLendrAmt)), 2)
                       FROM LOANAPP.LoanPartLendrTbl
                      WHERE LoanAppNmb = p_LoanAppNmb),
                    0)
                   ThirdPartyLoanAmt,
               LoanAppEWCPSnglTransPostInd,
               LoanAppEWCPSnglTransInd,
               LoanCollatInd,
               a.LoanAppCDCGntyAmt,
               a.LoanAppCDCNetDbentrAmt,
               a.LoanAppCDCFundFeeAmt,
               a.LoanAppCDCSeprtPrcsFeeInd,
               a.LoanAppCDCPrcsFeeAmt,
               a.LoanAppCDCClsCostAmt,
               a.LoanAppCDCOthClsCostAmt,
               a.LoanAppCDCUndrwtrFeeAmt,
               a.LoanAppContribPct,
               a.LoanAppContribAmt,
               LoanAppDisasterCntrlNmb,
               LOANDISASTRAPPFEECHARGED,
               LOANDISASTRAPPDCSN,
               LOANASSOCDISASTRAPPNMB,
               LOANASSOCDISASTRLOANNMB,
               LOANDISASTRAPPNMB,
               LOANAPPINTDTLCD,
               LoanPymtRepayInstlTypCd,
               LOANPYMNINSTLMNTFREQCD,
               LoanPymntSchdlFreq,
               (SELECT e.LOAN_GNTY_MAT_ST_DESC_TXT
                  FROM SBAREF.LOANMATSTINDCDTBL e
                 WHERE a.LOANGNTYMATSTIND = e.LOAN_GNTY_MAT_ST_IND)
                   AS LOANGNTYMATSTDESCTXT,
               CASE
                   WHEN a.LOANPYMNINSTLMNTFREQCD = 'M' THEN 'Month'
                   WHEN a.LOANPYMNINSTLMNTFREQCD = 'Q' THEN 'Quarter'
                   WHEN a.LOANPYMNINSTLMNTFREQCD = 'S' THEN 'Semi annual'
                   WHEN a.LOANPYMNINSTLMNTFREQCD = 'A' THEN 'Annual'
                   WHEN a.LOANPYMNINSTLMNTFREQCD = 'O' THEN 'Other'
                   ELSE 'Not selected yet'
               END
                   AS PYMTINTONLYFREQTXT,
               LoanAppCDCGrossDbentrAmt,
               LoanAppBalToBorrAmt,
               a.LoanAppRevlMoIntQty,
               a.LoanAppAmortMoIntQty,
               a.LoanAppExtraServFeeAMT,
               a.LoanAppExtraServFeeInd,
               a.RqstSeqNmb,
               LoanAppMonthPayroll
          FROM LoanAppTbl          a,
               sbaref.PrcsMthdTbl  b,
               sbaref.PrgrmTbl     c                   -- ,sbaref.IMRtTypTbl d
                                    -- ,sbaref.CalndrPrdTbl e
                                    ,
               sbaref.BusAgeCdTbl  f
         WHERE     a.LoanAppNmb = p_LoanAppNmb
               AND a.PrcsMthdCd = b.PrcsMthdCd
               AND a.PrgmCd = c.PrgrmCd(+)
               -- AND a.IMRtTypCd       = d.IMRtTypCd(+)
               --  AND a.PrcsMthdCd      = d.PrcsMthdCd(+)
               -- AND a.LoanAppAdjPrdCd = e.CalndrPrdCd(+)
               AND a.LoanAppNewBusCd = f.BusAgeCd(+);

    OPEN p_SelCur5 FOR
        SELECT d.InjctnTypCd,
               r.InjctnTypTxt,
               CASE WHEN d.InjctnTypCd = 'O' THEN '~' ELSE r.InjctnTypCd END
                   DisplayOrder,
               ROUND (TO_NUMBER (d.LoanInjctnAmt), 2)
                   LoanInjctnNum,
               d.LoanInjctnOthDescTxt,
               d.LOANINJCTNFundTermYrNmb       -- NK--OPSMDEV 1179--09/28/2016
          FROM LoanInjctnTbl d, sbaref.InjctnTypCdTbl r
         WHERE d.LoanAppNmb = p_LoanAppNmb AND d.InjctnTypCd = r.InjctnTypCd
        UNION ALL
        SELECT a.InjctnTypCd,
               a.InjctnTypTxt,
               CASE WHEN a.InjctnTypCd = 'O' THEN '~' ELSE a.InjctnTypCd END
                   DisplayOrder,
               NULL,
               NULL,
               NULL
          FROM sbaref.InjctnTypCdTbl a
         WHERE     a.InjctnTypStrtDt <= SYSDATE
               AND (   a.InjctnTypEndDt IS NULL
                    OR a.InjctnTypEndDt + 1 > SYSDATE)
               AND NOT EXISTS
                       (SELECT 1
                          FROM LoanInjctnTbl z
                         WHERE     z.LoanAppNmb = p_LoanAppNmb
                               AND z.InjctnTypCd = a.InjctnTypCd)
        ORDER BY DisplayOrder;

    OPEN p_SelCur6 FOR
        SELECT a.ProcdTypCd,
               b.LoanProcdTypCd,
               b.LoanProcdTypDescTxt,
               ROUND (TO_NUMBER (a.LoanProcdAmt), 2)     LoanProcdNum,
               a.LoanProcdOthTypTxt,
               a.LoanProcdRefDescTxt,
               a.LoanProcdPurAgrmtDt,
               a.LoanProcdPurIntangAssetAmt,
               a.LoanProcdPurIntangAssetDesc,
               a.LoanProcdPurStkHldrNm,
               a.NCAIncldInd,
               a.StkPurCorpText
          FROM LoanProcdTbl a, sbaref.LoanProcdTypTbl b
         WHERE     a.LoanAppNmb = p_LoanAppNmb
               AND a.ProcdTypCd = b.ProcdTypCd
               AND a.LoanProcdTypCd = b.LoanProcdTypCd
        UNION ALL
        SELECT ProcdTypCd,
               b.LoanProcdTypCd,
               b.LoanProcdTypDescTxt,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL
          FROM sbaref.LoanProcdTypTbl b
         WHERE     ProcdTypCd = v_ProcdTypCd
               AND NOT EXISTS
                       (SELECT 1
                          FROM LoanProcdTbl a
                         WHERE     a.LoanAppNmb = p_LoanAppNmb
                               AND a.ProcdTypCd = b.ProcdTypCd
                               AND a.LoanProcdTypCd = b.LoanProcdTypCd)
               AND LoanProcdTypCdStrtDt <= SYSDATE
               AND (   LoanProcdTypCdEndDt IS NULL
                    OR LoanProcdTypCdEndDt + 1 > SYSDATE)
        ORDER BY LoanProcdTypCd;

    OPEN p_SelCur7 FOR
          SELECT LoanCollatSeqNmb,
                 a.LoanCollatTypCd,
                 b.LoanCollatTypDescTxt,
                 a.LoanCollatDesc,
                 ROUND (TO_NUMBER (LoanCollatMrktValAmt), 2)
                     LoanCollatMrktValNum,
                 LoanCollatOwnrRecrd,
                 LoanCollatSBALienPos,
                 a.LoanCollatValSourcCd,
                 c.LoanCollatValSourcDescTxt,
                 LoanCollatValDt,
                 SecurShrPariPassuInd                                     --SS
                                     ,
                 SecurShrPariPassuNonSBAInd,
                 SecurPariPassuLendrNm,
                 SecurPariPassuAmt,
                 SecurLienLimAmt,
                 SecurInstrmntTypCd,
                 SecurWaterRightInd,
                 SecurRentAsgnInd,
                 SecurSellerNm,
                 SecurPurNm,
                 SecurOwedToSellerAmt,
                 SecurCDCDeedInEscrowInd,
                 SecurSellerIntDtlInd,
                 SecurSellerIntDtlTxt,
                 SecurTitlSubjPriorLienInd,
                 SecurPriorLienTxt,
                 SecurPriorLienLimAmt,
                 SecurSubjPriorAsgnInd,
                 SecurPriorAsgnTxt,
                 SecurPriorAsgnLimAmt,
                 SecurALTATltlInsurInd,
                 SecurLeaseTermOverLoanYrNmb,
                 LandlordIntProtWaiverInd,
                 SecurDt,
                 SecurLessorTermNotcDaysNmb,
                 SecurDescTxt,
                 SecurPropAcqWthLoanInd,
                 SecurPropTypTxt,
                 SecurOthPropTxt,
                 SecurLienOnLqorLicnsInd,
                 SecurMadeYrNmb,
                 SecurLocTxt,
                 SecurOwnerNm,
                 SecurMakeNm,
                 SecurAmt,
                 SecurNoteSecurInd,
                 SecurStkShrNmb,
                 SecurLienHldrVrfyInd,
                 SecurTitlVrfyTypCd,
                 SecurTitlVrfyOthTxt,
                 FloodInsurRqrdInd,
                 REHazardInsurRqrdInd,
                 PerHazardInsurRqrdInd,
                 FullMarInsurRqrdInd,
                 EnvInvstgtSBAAppInd,
                 LeasePrmTypCd,
                 ApprTypCd,
                 LoanCollatSubTypCd,
                 CollatAppOrdDt,
                 LoanCollatValDt,
                 (SELECT d.LAC_COND_TYP_DESC_TXT
                    FROM LOAN.LAC_COND_TYPCD_TBL d
                   WHERE a.LoanCollatSubTypCd = d.LAC_COND_TYP_CD)
                     AS LAC_COND_TYP_DESC_TXT,
                 (SELECT e.LAC_ALTA_PLCY_DESC_TXT
                    FROM LOAN.LAC_ALTA_PLCY_CD_TBL e
                   WHERE a.SecurTitlVrfyTypCd = e.LAC_ALTA_PLCY_CD)
                     AS LAC_ALTA_PLCY_DESC_TXT,
                 (SELECT z.leastypdesctxt
                    FROM sbaref.leastypcdtbl z
                   WHERE a.leaseprmtypcd = z.leastypcd)
                     AS LEASDESCTXT,
                 (SELECT g.Propapprsltypdesctxt
                    FROM sbaref.Propapprsltypcdtbl g
                   WHERE a.ApprTypcd = g.Propapprsltypcd)
                     AS APPRSLDESCTXT,
                 (SELECT f.collatstatdesctxt
                    FROM sbaref.collatstatcdtbl f
                   WHERE a.collatstatcd = f.collatstatcd)
                     AS COLLATSTATDESCTXT
            FROM LoanCollatTbl               a,
                 sbaref.LoanCollatTypCdTbl   b,
                 sbaref.LoanCollatValSourcTbl c
           WHERE     LoanAppNmb = p_LoanAppNmb
                 AND a.LoanCollatTypCd = b.LoanCollatTypCd
                 AND a.LoanCollatValSourcCd = c.LoanCollatValSourcCd(+)
        ORDER BY LoanCollatSeqNmb;

    OPEN p_SelCur8 FOR
        SELECT LoanCollatSeqNmb,
               LoanCollatLienSeqNmb,
               LoanCollatLienHldrNm,
               LoanCollatLienPos,
               ROUND (TO_NUMBER (LoanCollatLienBalAmt), 2)
                   LoanCollatLienBalNum
          FROM LoanCollatLienTbl
         WHERE LoanAppNmb = p_LoanAppNmb;

    OPEN p_SelCur9 FOR SELECT LoanPrtCmntTxt
                         FROM LoanPrtCmntTbl
                        WHERE LoanAppNmb = p_LoanAppNmb;

    OPEN p_SelCur10 FOR
        SELECT a.LoanAppNmb,
               NAICSYRNMB,
               NAICSCD,
               LoanAppFrnchsInd,
               LOANAPPFRNCHSCD,
               LOANAPPFRNCHSNM,
               LoanAppProjStr1Nm,
               LoanAppProjStr2Nm,
               LoanAppProjCtyNm,
               LoanAppProjCntyCd,
               LoanAppProjStCd,
               LoanAppProjZipCd,
               LoanAppProjZip4Cd,
               LoanAppRuralUrbanInd,
               ROUND (TO_NUMBER (LoanAppNetExprtAmt), 2)
                   LoanAppNetExprtAmt,
               LoanAppCurrEmpQty,
               LoanAppJobCreatQty,
               LoanAppJobRtnd,
               LoanAppJobRqmtMetInd,
               LoanAppCDCJobRat,
               LOANAPPPROJLMIND,
               LOANAPPPROJSIZECD,
               e.LoanEconDevObjctCd,
               b.EconDevObjctTxt,
               ADDTNLLOCACQLMTIND                                         --SS
                                 ,
               FIXASSETACQLMTIND,
               FIXASSETACQLMTAMT,
               COMPSLMTIND,
               COMPSLMTAMT,
               BULKSALELAWCOMPLYIND,
               FRNCHSRECRDACCSIND,
               FRNCHSFEEDEFMONNMB,
               FRNCHSFEEDEFIND,
               FRNCHSTERMNOTCIND,
               FRNCHSLENDRSAMEOPPIND,
               LOANBUSESTDT,
               (SELECT z.LqdCrTotScr
                  FROM loanapp.LqdCrWSCTbl z
                 WHERE     z.LoanAppNmb = p_LoanAppNmb
                       AND z.LqdCrSeqNmb =
                           (SELECT MAX (LqdCrSeqNmb)
                              FROM loanapp.LqdCrWSCTbl y
                             WHERE y.LoanAppNmb = p_LoanAppNmb)
                       AND creatdt = (SELECT MAX (creatdt)
                                        FROM loanapp.LqdCrWSCTbl x
                                       WHERE z.LoanAppNmb = x.LoanAppNmb))
                   AS LqdCrTotScr,
               (SELECT g.DESCTXT
                  FROM Sbaref.NAICSTBL g
                 WHERE a.NAICSCD = g.NAICSCD AND a.NAICSYRNMB = g.NAICSYRNMB)
                   AS NAICSDESCTXT,
               (SELECT b.DISASTRSTDECLCD
                  FROM sbaref.DISASTRDECLTBL b, loanapp.loanapptbl a
                 WHERE     A.LOANAPPDISASTERCNTRLNMB = B.SBAECONCNTRLNMB
                       AND A.LOANAPPNMB = p_LOANAPPNMB)
                   AS DISASTRSTDECLCD,
               (SELECT b.DISASTRDESCTXT
                  FROM sbaref.DISASTRDECLTBL b, loanapp.loanapptbl a
                 WHERE     A.LOANAPPDISASTERCNTRLNMB = B.SBAECONCNTRLNMB
                       AND A.LOANAPPNMB = p_LOANAPPNMB)
                   AS DISASTRDESCTXT,
               (SELECT b.DISASTRSTRTDT
                  FROM sbaref.DISASTRDECLTBL b, loanapp.loanapptbl a
                 WHERE     A.LOANAPPDISASTERCNTRLNMB = B.SBAECONCNTRLNMB
                       AND A.LOANAPPNMB = p_LOANAPPNMB)
                   AS DISASTRSTRTDT
          FROM LoanAppProjTbl                   a,
               sbaref.EconDevObjctCdTbl         b,
               LOANAPP.LOANECONDEVOBJCTCHLDTBL  e
         WHERE     a.LoanAppNmb = p_LoanAppNmb
               AND e.LoanEconDevObjctCd = b.EconDevObjctCd(+);

    OPEN p_SelCur11 FOR
        SELECT LoanBSDt,
               ROUND (TO_NUMBER (LoanBSCashEqvlntAmt), 2)
                   LoanBSCashEqvlntAmt,
               ROUND (TO_NUMBER (LoanBSNetTrdRecvAmt), 2)
                   LoanBSNetTrdRecvAmt,
               ROUND (TO_NUMBER (LoanBSTotInvtryAmt), 2)
                   LoanBSTotInvtryAmt,
               ROUND (TO_NUMBER (LoanBSOthCurAssetAmt), 2)
                   LoanBSOthCurAssetAmt,
               ROUND (TO_NUMBER (LoanBSTotCurAssetAmt), 2)
                   LoanBSTotCurAssetAmt,
               ROUND (TO_NUMBER (LoanBSTotFixAssetAmt), 2)
                   LoanBSTotFixAssetAmt,
               ROUND (TO_NUMBER (LoanBSTotOthAssetAmt), 2)
                   LoanBSTotOthAssetAmt,
               ROUND (TO_NUMBER (LoanBSTotAssetAmt), 2)
                   LoanBSTotAssetAmt,
               ROUND (TO_NUMBER (LoanBSAcctsPayblAmt), 2)
                   LoanBSAcctsPayblAmt,
               ROUND (TO_NUMBER (LoanBSCurLTDAmt), 2)
                   LoanBSCurLTDAmt,
               ROUND (TO_NUMBER (LoanBSOthCurLiabAmt), 2)
                   LoanBSOthCurLiabAmt,
               ROUND (TO_NUMBER (LoanBSTotCurLiabAmt), 2)
                   LoanBSTotCurLiabAmt,
               ROUND (TO_NUMBER (LoanBSLTDAmt), 2)
                   LoanBSLTDAmt,
               ROUND (TO_NUMBER (LoanBSOthLTLiabAmt), 2)
                   LoanBSOthLTLiabAmt,
               ROUND (TO_NUMBER (LoanBSStbyDbt), 2)
                   LoanBSStbyDbt,
               ROUND (TO_NUMBER (LoanBSTotLiab), 2)
                   LoanBSTotLiab,
               ROUND (TO_NUMBER (LoanBSBusNetWrth), 2)
                   LoanBSBusNetWrth,
               ROUND (TO_NUMBER (LoanBSTngblNetWrth), 2)
                   LoanBSTngblNetWrth,
               LoanBSActlPrfrmaInd,
               a.LoanFinanclStmtSourcCd,
               b.LoanFinanclStmtSourcDescTxt
          FROM LoanBSTbl a, sbaref.LoanFinanclStmtSourcTbl b
         WHERE     LoanAppNmb = p_LoanAppNmb
               AND a.LoanFinanclStmtSourcCd = b.LoanFinanclStmtSourcCd;

    OPEN p_SelCur12 FOR
          SELECT LoanISSeqNmb,
                 ROUND (TO_NUMBER (LoanISNetSalesRevnuAmt), 2)
                     LoanISNetSalesRevnuAmt,
                 ROUND (TO_NUMBER (LoanISCostSalesAmt), 2)
                     LoanISCostSalesAmt,
                 ROUND (TO_NUMBER (LoanISGrsProftAmt), 2)
                     LoanISGrsProftAmt,
                 ROUND (TO_NUMBER (LoanISOperProftAmt), 2)
                     LoanISOperProftAmt,
                 ROUND (TO_NUMBER (LoanISAnnIntExpnAmt), 2)
                     LoanISAnnIntExpnAmt,
                 ROUND (TO_NUMBER (LoanISDprctAmortAmt), 2)
                     LoanISDprctAmortAmt,
                 ROUND (TO_NUMBER (LoanISNetIncBefTaxWthdrlAmt), 2)
                     LoanISNetIncBefTaxWthdrlAmt,
                 ROUND (TO_NUMBER (LoanISOwnrSalaryAmt), 2)
                     LoanISOwnrSalaryAmt,
                 ROUND (TO_NUMBER (LoanISIncTaxAmt), 2)
                     LoanISIncTaxAmt,
                 ROUND (TO_NUMBER (LoanISNetIncAmt), 2)
                     LoanISNetIncAmt,
                 ROUND (TO_NUMBER (LoanISCshflwAmt), 2)
                     LoanISCshflwAmt,
                 a.LoanFinanclStmtSourcCd,
                 b.LoanFinanclStmtSourcDescTxt,
                 LoanISBegnDt,
                 LoanISEndDt
            FROM LoanISTbl a, sbaref.LoanFinanclStmtSourcTbl b
           WHERE     LoanAppNmb = p_LoanAppNmb
                 AND a.LoanFinanclStmtSourcCd = b.LoanFinanclStmtSourcCd
        ORDER BY LoanISSeqNmb;

    OPEN p_SelCur13 FOR
        SELECT BorrSeqNmb,
               a.TaxId,
               LoanBusPrimBorrInd,
               BorrBusPerInd,
               a.IMEPCOperCd,
               e.IMEPCOperDescTxt,
               c.BusTypCd,
               c.BusTypTxt,
               BusNm,
               BusTrdNm,
               NULL
                   PerFirstNm,
               NULL
                   PerLastNm,
               NULL
                   PerInitialNm,
               NULL
                   PerSfxNm,
               NULL
                   PerTitlTxt,
               NULL
                   PerBrthDt,
               NULL
                   PerBrthCtyNm,
               NULL
                   PerBrthStCd,
               NULL
                   PerBrthCntCd,
               NULL
                   PerBrthCntNm,
               NULL
                   PerUSCitznInd,
               NULL
                   CitznshipTxt,
               NULL
                   PerAlienRgstrtnNmb,
               NULL
                   PerAffilEmpFedInd,
               NULL
                   PerIOBInd,
               NULL
                   PerIndctPrleProbatnInd,
               NULL
                   PerCrmnlOffnsInd,
               NULL
                   PerCnvctInd,
               NULL
                   PerLqdAssetAmt,
               NULL
                   PerBusOwnrshpAmt,
               NULL
                   PerREAmt,
               NULL
                   PerOthAssetAmt,
               NULL
                   PerTotAssetAmt,
               NULL
                   PerRELiabAmt,
               NULL
                   PerCCDbtAmt,
               NULL
                   PerInstlDbtAmt,
               NULL
                   PerOthLiabAmt,
               NULL
                   PerTotLiabAmt,
               NULL
                   PerNetWrthAmt,
               NULL
                   PerAnnSalaryAmt,
               NULL
                   PerOthAnnIncAmt,
               NULL
                   PerSourcOfOthIncTxt,
               NULL
                   PerResOwnRentOthInd,
               NULL
                   PerMoHsngAmt,
               BusExprtInd,
               NULL
                   PerFngrprntWaivDt,
               BusBnkrptInd,
               BusLwsuitInd,
               BusPriorSBALoanInd,
               BusCurBnkNm,
               BusChkngBalAmt,
               BusCurOwnrshpDt,
               b.BusPrimPhnNmb,
               BusPhyAddrStr1Nm,
               BusPhyAddrStr2Nm,
               BusPhyAddrCtyNm,
               BusPhyAddrStCd,
               BusPhyAddrStNm,
               BusPhyAddrCntCd,
               BusPhyAddrZipCd,
               BusPhyAddrZip4Cd,
               BusPhyAddrPostCd,
               BusMailAddrStr1Nm,
               BusMailAddrStr2Nm,
               BusMailAddrCtyNm,
               BusMailAddrStCd,
               BusMailAddrStNm,
               BusMailAddrCntCd,
               BusMailAddrZipCd,
               BusMailAddrZip4Cd,
               BusMailAddrPostCd,
               BusExtrnlCrdtScorInd,
               b.IMCrdtScorSourcCd,
               d.IMCrdtScorSourcDescTxt,
               BusExtrnlCrdtScorNmb,
               BusExtrnlCrdtScorDt,
               b.BusDunsNmb,
               a.LiabInsurRqrdInd,
               a.ProdLiabInsurRqrdInd,
               a.LiqLiabInsurRqrdInd,
               a.MalPrctsInsurRqrdInd,
               a.OthInsurRqrdind,
               a.OthInsurDescTxt,
               a.PymtLessCrdtInd,
               (SELECT r.RecrdStmtTypDescTxt
                  FROM sbaref.RecrdStmtTypCdTbl r
                 WHERE a.RecrdStmtTypCd = r.RecrdStmtTypCd)
                   AS RecrdStmtTypCd,
               a.RecrdStmtYrEndDayNmb                  -- ,a.InsurDisblDescTxt
                                     ,
               a.LoanCntlIntInd
                   AS borrLoanCntlIntInd,
               (SELECT s.CNTLINTTYPTXT
                  FROM sbaref.LOANCNTLINTTYPCDTBL s
                 WHERE a.LoanCntlIntTyp = s.CNTLINTTYPCD)
                   AS borrLoanCntlIntTyp                     -- NK--08/29/2016
                                        ,
               a.WorkrsCompInsRqrdInd,
               NULL
                   AS LACGNTYCONDTYPCD,
               NULL
                   AS GntyCondTypTxt,
               BusAltPhnNmb,
               BusPrimEmailAdr,
               BusAltEmailAdr,
               BusFedAgncySDPIEInd,
               BusSexNatrInd,
               BusNonFmrSBAEmpInd,
               BusNonLegBrnchEmpInd,
               BusNonFedEmpInd,
               BusNonGS13EmpInd,
               BusNonSBACEmpInd,
               b.BusPrimCntctNm,
               --     b.BusPrimCntctEmail,
               b.BusFedAgncyLoanInd,
               b.BusOutDbtInd,
               NULL
                   PerOthrCmntTxt
          FROM LoanBusTbl                 b,
               LoanBorrTbl                a,
               sbaref.BusTypTbl           c,
               sbaref.IMCrdtScorSourcTbl  d,
               sbaref.IMEPCOperCdTbl      e
         WHERE     b.LoanAppNmb = p_LoanAppNmb
               AND a.LoanAppNmb = p_LoanAppNmb
               AND b.TaxId = a.TaxId
               AND BorrBusPerInd = 'B'
               AND b.BusTypCd = c.BusTypCd(+)
               AND b.IMCrdtScorSourcCd = d.IMCrdtScorSourcCd(+)
               AND a.IMEPCOperCd = e.IMEPCOperCd(+)
        UNION ALL
        SELECT BorrSeqNmb,
               a.TaxId,
               LoanBusPrimBorrInd,
               BorrBusPerInd,
               a.IMEPCOperCd,
               e.IMEPCOperDescTxt,
               NULL
                   BusTypCd,
               NULL
                   BusTypTxt,
               NULL
                   BusNm,
               NULL
                   BusTrdNm,
               ocadataout (PerFirstNm)
                   PerFirstNm,
               ocadataout (PerLastNm)
                   PerLastNm,
               PerInitialNm,
               PerSfxNm,
               PerTitlTxt,
               PerBrthDt,
               PerBrthCtyNm,
               PerBrthStCd,
               PerBrthCntCd,
               PerBrthCntNm,
               b.PerUSCitznInd,
               c.CitznshipTxt,
               PerAlienRgstrtnNmb,
               PerAffilEmpFedInd,
               PerIOBInd,
               PerIndctPrleProbatnInd,
               PerCrmnlOffnsInd,
               PerCnvctInd,
               ROUND (TO_NUMBER (PerLqdAssetAmt), 2)
                   PerLqdAssetAmt,
               ROUND (TO_NUMBER (PerBusOwnrshpAmt), 2)
                   PerBusOwnrshpAmt,
               ROUND (TO_NUMBER (PerREAmt), 2)
                   PerREAmt,
               ROUND (TO_NUMBER (PerOthAssetAmt), 2)
                   PerOthAssetAmt,
               ROUND (TO_NUMBER (PerTotAssetAmt), 2)
                   PerTotAssetAmt,
               ROUND (TO_NUMBER (PerRELiabAmt), 2)
                   PerRELiabAmt,
               ROUND (TO_NUMBER (PerCCDbtAmt), 2)
                   PerCCDbtAmt,
               ROUND (TO_NUMBER (PerInstlDbtAmt), 2)
                   PerInstlDbtAmt,
               ROUND (TO_NUMBER (PerOthLiabAmt), 2)
                   PerOthLiabAmt,
               ROUND (TO_NUMBER (PerTotLiabAmt), 2)
                   PerTotLiabAmt,
               ROUND (TO_NUMBER (PerNetWrthAmt), 2)
                   PerNetWrthAmt,
               ROUND (TO_NUMBER (PerAnnSalaryAmt), 2)
                   PerAnnSalaryAmt,
               ROUND (TO_NUMBER (PerOthAnnIncAmt), 2)
                   PerOthAnnIncAmt,
               PerSourcOfOthIncTxt,
               PerResOwnRentOthInd,
               ROUND (TO_NUMBER (PerMoHsngAmt), 2)
                   PerMoHsngAmt,
               NULL
                   BusExprtInd,
               PerFngrprntWaivDt,
               PerBnkrptcyInd,
               PerPndngLwsuitInd,
               NULL
                   BusPriorSBALoanInd,
               NULL
                   BusCurBnkNm,
               NULL
                   BusChkngBalAmt,
               NULL
                   BusCurOwnrshpDt,
               ocadataout (b.PerPrimPhnNmb)
                   PerPrimPhnNmb,
               ocadataout (PerPhyAddrStr1Nm)
                   PerPhyAddrStr1Nm,
               ocadataout (PerPhyAddrStr2Nm)
                   PerPhyAddrStr2Nm,
               PerPhyAddrCtyNm,
               PerPhyAddrStCd,
               PerPhyAddrStNm,
               PerPhyAddrCntCd,
               PerPhyAddrZipCd,
               PerPhyAddrZip4Cd,
               PerPhyAddrPostCd,
               ocadataout (PerMailAddrStr1Nm)
                   PerMailAddrStr1Nm,
               ocadataout (PerMailAddrStr2Nm)
                   PerMailAddrStr2Nm,
               PerMailAddrCtyNm,
               PerMailAddrStCd,
               PerMailAddrStNm,
               PerMailAddrCntCd,
               PerMailAddrZipCd,
               PerMailAddrZip4Cd,
               PerMailAddrPostCd,
               PerExtrnlCrdtScorInd,
               b.IMCrdtScorSourcCd,
               d.IMCrdtScorSourcDescTxt,
               PerExtrnlCrdtScorNmb,
               PerExtrnlCrdtScorDt,
               NULL
                   BusDunsNmb,
               a.LiabInsurRqrdInd,
               a.ProdLiabInsurRqrdInd,
               a.LiqLiabInsurRqrdInd,
               a.MalPrctsInsurRqrdInd,
               a.OthInsurRqrdind,
               a.OthInsurDescTxt,
               a.PymtLessCrdtInd,
               (SELECT r.RecrdStmtTypDescTxt
                  FROM sbaref.RecrdStmtTypCdTbl r
                 WHERE a.RecrdStmtTypCd = r.RecrdStmtTypCd)
                   AS RecrdStmtTypCd,
               a.RecrdStmtYrEndDayNmb                  -- ,a.InsurDisblDescTxt
                                     ,
               a.LoanCntlIntInd
                   AS borrLoanCntlIntInd,
               (SELECT s.CNTLINTTYPTXT
                  FROM sbaref.LOANCNTLINTTYPCDTBL s
                 WHERE a.LoanCntlIntTyp = s.CNTLINTTYPCD)
                   AS borrLoanCntlIntTyp                     -- NK--08/29/2016
                                        ,
               a.WorkrsCompInsRqrdInd,
               NULL
                   AS LACGNTYCONDTYPCD,
               NULL
                   AS GntyCondTypTxt,
               NULL
                   BusAltPhnNmb,
               NULL
                   BusPrimEmailAdr,
               NULL
                   BusAltEmailAdr,
               NULL
                   BusFedAgncySDPIEInd,
               NULL
                   BusSexNatrInd,
               NULL
                   BusNonFmrSBAEmpInd,
               NULL
                   BusNonLegBrnchEmpInd,
               NULL
                   BusNonFedEmpInd,
               NULL
                   BusNonGS13EmpInd,
               NULL
                   BusNonSBACEmpInd,
               NULL
                   BusPrimCntctNm,
               --   NULL BusPrimCntctEmail,
               b.PerFedAgncyLoanInd,
               NULL
                   BusOutDbtInd,
               b.PerOthrCmntTxt
          FROM LoanPerTbl                 b,
               LoanBorrTbl                a,
               sbaref.CitznshipCdTbl      c,
               sbaref.IMCrdtScorSourcTbl  d,
               sbaref.IMEPCOperCdTbl      e,
               LoanPerFinanTbl            f
         WHERE     b.LoanAppNmb = p_LoanAppNmb
               AND a.LoanAppNmb = p_LoanAppNmb
               AND a.TaxId = f.TaxId(+)
               AND a.LoanAppNmb = f.LoanAppNmb(+)
               AND b.TaxId = a.TaxId
               AND BorrBusPerInd = 'P'
               AND b.PerUSCitznInd = c.CitznshipCd(+)
               AND b.IMCrdtScorSourcCd = d.IMCrdtScorSourcCd(+)
               AND a.IMEPCOperCd = e.IMEPCOperCd(+)
        ORDER BY BorrSeqNmb;

    OPEN p_SelCur14 FOR
          SELECT a.BorrSeqNmb,
                 a.LoanPrevFinanSeqNmb,
                 a.LoanPrevFinanAgencyNm,
                 a.LoanPrevFinanLoanNmb,
                 a.LoanPrevFinanAppvDt,
                 ROUND (TO_NUMBER (a.LoanPrevFinanTotAmt), 2)
                     LoanPrevFinanTotNum,
                 ROUND (TO_NUMBER (a.LoanPrevFinanBalAmt), 2)
                     LoanPrevFinanBalNum,
                 a.LoanPrevFinanStatCd,
                 b.LoanPrevFinanStatDescTxt,
                 a.LoanPrevFinanDelnqCmntTxt
            FROM LoanPrevFinanTbl a, sbaref.LoanPrevFinanStatTbl b
           WHERE     a.LoanAppNmb = p_LoanAppNmb
                 AND a.LoanPrevFinanStatCd = b.LoanPrevFinanStatCd(+)
        ORDER BY a.BorrSeqNmb, a.LoanPrevFinanSeqNmb;

    OPEN p_SelCur15 FOR
        SELECT GuarSeqNmb,
               a.TaxId,
               GuarBusPerInd,
               LoanGuarOperCoInd,
               c.BusTypCd,
               c.BusTypTxt,
               BusNm,
               BusTrdNm,
               NULL
                   PerFirstNm,
               NULL
                   PerLastNm,
               NULL
                   PerInitialNm,
               NULL
                   PerSfxNm,
               NULL
                   PerTitlTxt,
               NULL
                   PerBrthDt,
               NULL
                   PerBrthCtyNm,
               NULL
                   PerBrthStCd,
               NULL
                   PerBrthCntCd,
               NULL
                   PerBrthCntNm,
               NULL
                   PerUSCitznInd,
               NULL
                   CitznshipTxt,
               NULL
                   PerAlienRgstrtnNmb,
               NULL
                   PerAffilEmpFedInd,
               NULL
                   PerIOBInd,
               NULL
                   PerIndctPrleProbatnInd,
               NULL
                   PerCrmnlOffnsInd,
               NULL
                   PerCnvctInd,
               NULL
                   PerLqdAssetAmt,
               NULL
                   PerBusOwnrshpAmt,
               NULL
                   PerREAmt,
               NULL
                   PerOthAssetAmt,
               NULL
                   PerTotAssetAmt,
               NULL
                   PerRELiabAmt,
               NULL
                   PerCCDbtAmt,
               NULL
                   PerInstlDbtAmt,
               NULL
                   PerOthLiabAmt,
               NULL
                   PerTotLiabAmt,
               NULL
                   PerNetWrthAmt,
               NULL
                   PerAnnSalaryAmt,
               NULL
                   PerOthAnnIncAmt,
               NULL
                   PerSourcOfOthIncTxt,
               NULL
                   PerResOwnRentOthInd,
               NULL
                   PerMoHsngAmt,
               BusExprtInd,
               NULL
                   PerFngrprntWaivDt,
               BusBnkrptInd,
               BusLwsuitInd,
               BusPriorSBALoanInd,
               BusCurBnkNm,
               BusChkngBalAmt,
               BusCurOwnrshpDt,
               b.BusPrimPhnNmb,
               BusPhyAddrStr1Nm,
               BusPhyAddrStr2Nm,
               BusPhyAddrCtyNm,
               BusPhyAddrStCd,
               BusPhyAddrStNm,
               BusPhyAddrCntCd,
               BusPhyAddrZipCd,
               BusPhyAddrZip4Cd,
               BusPhyAddrPostCd,
               BusMailAddrStr1Nm,
               BusMailAddrStr2Nm,
               BusMailAddrCtyNm,
               BusMailAddrStCd,
               BusMailAddrStNm,
               BusPhyAddrCntCd,
               BusMailAddrZipCd,
               BusMailAddrZip4Cd,
               BusMailAddrPostCd,
               BusExtrnlCrdtScorInd,
               b.IMCrdtScorSourcCd,
               d.IMCrdtScorSourcDescTxt,
               BusExtrnlCrdtScorNmb,
               BusExtrnlCrdtScorDt,
               a.LACGNTYCONDTYPCD,                           -- RGG 02/12/2014
               (SELECT e.LOANGNTYTYPTXT
                  FROM SBAREF.LOANGNTYTYPCDTBL e
                 WHERE e.LOANGNTYTYPCD = a.LACGNTYCONDTYPCD)
                   AS GntyCondTypTxt,
               (SELECT s.LOANGNTYLMTTYPTXT
                  FROM SBAREF.LOANGNTYLMTTYPCDTBL s
                 WHERE s.LOANGNTYLMTTYPCD = a.GntySubCondTypCd)
                   AS GntySubCondTypTxt,
               a.GntyLmtAmt,
               a.GntyLmtPct,
               a.LoanCollatSeqNmb,
               (SELECT i.LOANCOLLATDESC
                  FROM LOANAPP.LOANCOLLATTBL i
                 WHERE     i.LOANCOLLATSEQNMB = a.LoanCollatSeqNmb
                       AND i.LOANAPPNMB = a.LoanAppNmb)
                   AS LOANCOLLATDESC,
               (SELECT I.LOANCOLLATMRKTVALAMT
                  FROM LOANAPP.LOANCOLLATTBL i
                 WHERE     i.LOANCOLLATSEQNMB = a.LoanCollatSeqNmb
                       AND i.LOANAPPNMB = a.LoanAppNmb)
                   AS LOANCOLLATMRKTVALAMT,
               a.GntyLmtYrNmb,
               a.LiabInsurRqrdInd,
               a.ProdLiabInsurRqrdInd,
               a.LiqLiabInsurRqrdInd,
               a.MalPrctsInsurRqrdInd,
               a.OthInsurRqrdInd,
               a.WorkrsCompInsRqrdInd,
               a.OthInsurDescTxt
          FROM LoanBusTbl                 b,
               LoanGuarTbl                a,
               sbaref.BusTypTbl           c,
               sbaref.IMCrdtScorSourcTbl  d
         WHERE     b.LoanAppNmb = p_LoanAppNmb
               AND a.LoanAppNmb = p_LoanAppNmb
               AND b.TaxId = a.TaxId
               AND GuarBusPerInd = 'B'
               AND b.BusTypCd = c.BusTypCd(+)
               AND b.IMCrdtScorSourcCd = d.IMCrdtScorSourcCd(+)
        UNION ALL
        SELECT GuarSeqNmb,
               a.TaxId,
               GuarBusPerInd,
               LoanGuarOperCoInd,
               NULL
                   BusTypCd,
               NULL
                   BusTypTxt,
               NULL
                   BusNm,
               NULL
                   BusTrdNm,
               ocadataout (PerFirstNm)
                   PerFirstNm,
               ocadataout (PerLastNm)
                   PerLastNm,
               PerInitialNm,
               PerSfxNm,
               PerTitlTxt,
               PerBrthDt,
               PerBrthCtyNm,
               PerBrthStCd,
               PerBrthCntCd,
               PerBrthCntNm,
               b.PerUSCitznInd,
               c.CitznshipTxt,
               PerAlienRgstrtnNmb,
               PerAffilEmpFedInd,
               PerIOBInd,
               PerIndctPrleProbatnInd,
               PerCrmnlOffnsInd,
               PerCnvctInd,
               ROUND (TO_NUMBER (PerLqdAssetAmt), 2)
                   PerLqdAssetAmt,
               ROUND (TO_NUMBER (PerBusOwnrshpAmt), 2)
                   PerBusOwnrshpAmt,
               ROUND (TO_NUMBER (PerREAmt), 2)
                   PerREAmt,
               ROUND (TO_NUMBER (PerOthAssetAmt), 2)
                   PerOthAssetAmt,
               ROUND (TO_NUMBER (PerTotAssetAmt), 2)
                   PerTotAssetAmt,
               ROUND (TO_NUMBER (PerRELiabAmt), 2)
                   PerRELiabAmt,
               ROUND (TO_NUMBER (PerCCDbtAmt), 2)
                   PerCCDbtAmt,
               ROUND (TO_NUMBER (PerInstlDbtAmt), 2)
                   PerInstlDbtAmt,
               ROUND (TO_NUMBER (PerOthLiabAmt), 2)
                   PerOthLiabAmt,
               ROUND (TO_NUMBER (PerTotLiabAmt), 2)
                   PerTotLiabAmt,
               ROUND (TO_NUMBER (PerNetWrthAmt), 2)
                   PerNetWrthAmt,
               ROUND (TO_NUMBER (PerAnnSalaryAmt), 2)
                   PerAnnSalaryAmt,
               ROUND (TO_NUMBER (PerOthAnnIncAmt), 2)
                   PerOthAnnIncAmt,
               PerSourcOfOthIncTxt,
               PerResOwnRentOthInd,
               ROUND (TO_NUMBER (PerMoHsngAmt), 2)
                   PerMoHsngAmt,
               NULL
                   BusExprtInd,
               PerFngrprntWaivDt,
               PerBnkrptcyInd,
               PerPndngLwsuitInd,
               NULL
                   BusPriorSBALoanInd,
               NULL
                   BusCurBnkNm,
               NULL
                   BusChkngBalAmt,
               NULL
                   BusCurOwnrshpDt,
               ocadataout (b.PerPrimPhnNmb)
                   PerPrimPhnNmb,
               ocadataout (PerPhyAddrStr1Nm)
                   PerPhyAddrStr1Nm,
               ocadataout (PerPhyAddrStr2Nm)
                   PerPhyAddrStr2Nm,
               PerPhyAddrCtyNm,
               PerPhyAddrStCd,
               PerPhyAddrStNm,
               PerPhyAddrCntCd,
               PerPhyAddrZipCd,
               PerPhyAddrZip4Cd,
               PerPhyAddrPostCd,
               ocadataout (PerMailAddrStr1Nm)
                   AS PerMailAddrStr1Nm,
               ocadataout (PerMailAddrStr2Nm)
                   AS PerMailAddrStr2Nm,
               PerMailAddrCtyNm,
               PerMailAddrStCd,
               PerMailAddrStNm,
               PerMailAddrCntCd,
               PerMailAddrZipCd,
               PerMailAddrZip4Cd,
               PerMailAddrPostCd,
               PerExtrnlCrdtScorInd,
               b.IMCrdtScorSourcCd,
               d.IMCrdtScorSourcDescTxt,
               PerExtrnlCrdtScorNmb,
               PerExtrnlCrdtScorDt,
               a.LACGNTYCONDTYPCD,                           -- RGG 02/12/2014
               (SELECT e.LOANGNTYTYPTXT
                  FROM SBAREF.LOANGNTYTYPCDTBL e
                 WHERE e.LOANGNTYTYPCD = a.LACGNTYCONDTYPCD)
                   AS GntyCondTypTxt,
               (SELECT s.LOANGNTYLMTTYPTXT
                  FROM SBAREF.LOANGNTYLMTTYPCDTBL s
                 WHERE s.LOANGNTYLMTTYPCD = a.GntySubCondTypCd)
                   AS GntySubCondTypTxt,
               a.GntyLmtAmt,
               a.GntyLmtPct,
               a.LoanCollatSeqNmb,
               (SELECT i.LOANCOLLATDESC
                  FROM LOANAPP.LOANCOLLATTBL i
                 WHERE     i.LOANCOLLATSEQNMB = a.LoanCollatSeqNmb
                       AND i.LOANAPPNMB = a.LoanAppNmb)
                   AS LOANCOLLATDESC,
               (SELECT I.LOANCOLLATMRKTVALAMT
                  FROM LOANAPP.LOANCOLLATTBL i
                 WHERE     i.LOANCOLLATSEQNMB = a.LoanCollatSeqNmb
                       AND i.LOANAPPNMB = a.LoanAppNmb)
                   AS LOANCOLLATMRKTVALAMT,
               a.GntyLmtYrNmb,
               a.LiabInsurRqrdInd,
               a.ProdLiabInsurRqrdInd,
               a.LiqLiabInsurRqrdInd,
               a.MalPrctsInsurRqrdInd,
               a.OthInsurRqrdInd,
               a.WorkrsCompInsRqrdInd,
               a.OthInsurDescTxt
          FROM LoanPerTbl                 b,
               LoanGuarTbl                a,
               sbaref.CitznshipCdTbl      c,
               sbaref.IMCrdtScorSourcTbl  d,
               LoanPerFinanTbl            f
         WHERE     b.LoanAppNmb = p_LoanAppNmb
               AND a.LoanAppNmb = p_LoanAppNmb
               AND a.TaxId = f.TaxId(+)
               AND a.LoanAppNmb = f.LoanAppNmb(+)
               AND b.TaxId = a.TaxId
               AND GuarBusPerInd = 'P'
               AND b.PerUSCitznInd = c.CitznshipCd(+)
               AND b.IMCrdtScorSourcCd = d.IMCrdtScorSourcCd(+)
        ORDER BY GuarSeqNmb;

    OPEN p_SelCur16 FOR
        SELECT j.BorrSeqNmb,
               j.TaxId
                   BorrTaxId,
               p.PrinSeqNmb,
               p.TaxId
                   PrinTaxId,
               PrinBusPerInd,
               p.VetCd,
               VetTxt,
               b.VetCertInd,
               p.GndrCd,
               GndrDesc,
               p.EthnicCd,
               EthnicDesc,
               LoanPrinPrimBusExprnceYrNmb,
               LoanPrinInsurNm,
               LoanPrinInsurAmt,
               LoanPrinGntyInd,
               NVL (LoanBusPrinPctOwnrshpBus, 0)
                   LoanBusPrinPctOwnrshpBus,
               c.BusTypCd,
               c.BusTypTxt,
               BusNm,
               BusTrdNm,
               NULL
                   PerFirstNm,
               NULL
                   PerLastNm,
               NULL
                   PerInitialNm,
               NULL
                   PerSfxNm,
               NULL
                   PerTitlTxt,
               NULL
                   PerBrthDt,
               NULL
                   PerBrthCtyNm,
               NULL
                   PerBrthStCd,
               NULL
                   PerBrthCntCd,
               NULL
                   PerBrthCntNm,
               NULL
                   PerUSCitznInd,
               NULL
                   CitznshipTxt,
               NULL
                   PerAlienRgstrtnNmb,
               NULL
                   PerAffilEmpFedInd,
               NULL
                   PerIOBInd,
               NULL
                   PerIndctPrleProbatnInd,
               NULL
                   PerCrmnlOffnsInd,
               NULL
                   PerCnvctInd,
               NULL
                   PerLqdAssetAmt,
               NULL
                   PerBusOwnrshpAmt,
               NULL
                   PerREAmt,
               NULL
                   PerOthAssetAmt,
               NULL
                   PerTotAssetAmt,
               NULL
                   PerRELiabAmt,
               NULL
                   PerCCDbtAmt,
               NULL
                   PerInstlDbtAmt,
               NULL
                   PerOthLiabAmt,
               NULL
                   PerTotLiabAmt,
               NULL
                   PerNetWrthAmt,
               NULL
                   PerAnnSalaryAmt,
               NULL
                   PerOthAnnIncAmt,
               NULL
                   PerSourcOfOthIncTxt,
               NULL
                   PerResOwnRentOthInd,
               NULL
                   PerMoHsngAmt,
               BusExprtInd,
               NULL
                   PerFngrprntWaivDt,
               BusBnkrptInd,
               BusLwsuitInd,
               BusPriorSBALoanInd,
               BusCurBnkNm,
               BusChkngBalAmt,
               BusCurOwnrshpDt,
               BusPhyAddrStr1Nm,
               BusPhyAddrStr2Nm,
               BusPhyAddrCtyNm,
               BusPhyAddrStCd,
               BusPhyAddrStNm,
               BusPhyAddrCntCd,
               BusPhyAddrZipCd,
               BusPhyAddrZip4Cd,
               BusPhyAddrPostCd,
               BusMailAddrStr1Nm,
               BusMailAddrStr2Nm,
               BusMailAddrCtyNm,
               BusMailAddrStCd,
               BusMailAddrStNm,
               BusMailAddrCntCd,
               BusMailAddrZipCd,
               BusMailAddrZip4Cd,
               BusMailAddrPostCd,
               BusExtrnlCrdtScorInd,
               b.IMCrdtScorSourcCd,
               d.IMCrdtScorSourcDescTxt,
               BusExtrnlCrdtScorNmb,
               BusExtrnlCrdtScorDt,
               j.LoanCntlIntInd
                   AS borrLoanCntlIntInd,
               bp.LoanCntlIntInd
                   AS busprinLoanCntlIntInd,
               (SELECT a.CNTLINTTYPTXT
                  FROM sbaref.LOANCNTLINTTYPCDTBL a
                 WHERE bp.LoanCntlIntTyp = a.CNTLINTTYPCD)
                   AS busprintLoanCntlIntTyp,                -- NK--08/15/2016
               p.LoanDisblInsurRqrdInd,
               p.LoanInsurDisblDescTxt,
               p.LoanLifeInsurRqrdInd,
               p.LoanPrinNoNCAEvdncInd,
               p.LoanComptitrNm,
               (SELECT e.LOANGNTYTYPTXT
                  FROM SBAREF.LOANGNTYTYPCDTBL e
                 WHERE e.LOANGNTYTYPCD = p.LACGNTYCONDTYPCD)
                   AS GntyCondTypTxt,
               (SELECT s.LOANGNTYLMTTYPTXT
                  FROM SBAREF.LOANGNTYLMTTYPCDTBL s
                 WHERE s.LOANGNTYLMTTYPCD = p.GntySubCondTypCd)
                   AS GntySubCondTypTxt,
               p.GntyLmtAmt,
               p.GntyLmtPct,
               p.LoanCollatSeqNmb,
               (SELECT i.LOANCOLLATDESC
                  FROM LOANAPP.LOANCOLLATTBL i
                 WHERE     i.LOANCOLLATSEQNMB = p.LoanCollatSeqNmb
                       AND i.LOANAPPNMB = p.LoanAppNmb)
                   AS LOANCOLLATDESC,
               (SELECT I.LOANCOLLATMRKTVALAMT
                  FROM LOANAPP.LOANCOLLATTBL i
                 WHERE     i.LOANCOLLATSEQNMB = p.LoanCollatSeqNmb
                       AND i.LOANAPPNMB = p.LoanAppNmb)
                   AS LOANCOLLATMRKTVALAMT,
               p.GntyLmtYrNmb,
               b.BusPrimPhnNmb,
               b.BusAltPhnNmb,
               b.BusPrimEmailAdr,
               b.BusAltEmailAdr,
               b.BusFedAgncySDPIEInd,
               b.BusCSP60DayDelnqInd,
               b.BusLglActnInd,
               NULL
                   PerCitznShpCntNm
          FROM LoanBusTbl                 b,
               LoanBorrTbl                j,
               LoanPrinTbl                p,
               LoanBusPrinTbl             bp,
               sbaref.BusTypTbl           c,
               sbaref.IMCrdtScorSourcTbl  d,
               sbaref.VetTbl              e,
               sbaref.GndrCdTbl           f,
               sbaref.EthnicCdTbl         g
         WHERE     b.LoanAppNmb = p_LoanAppNmb
               AND j.LoanAppNmb = p_LoanAppNmb
               AND p.LoanAppNmb = p_LoanAppNmb
               AND bp.LoanAppNmb = p_LoanAppNmb
               AND j.BorrSeqNmb = bp.BorrSeqNmb
               AND p.PrinSeqNmb = bp.PrinSeqNmb
               AND b.TaxId = p.TaxId
               AND p.PrinBusPerInd = 'B'
               AND b.BusTypCd = c.BusTypCd(+)
               AND b.IMCrdtScorSourcCd = d.IMCrdtScorSourcCd(+)
               AND p.VetCd = e.VetCd(+)
               AND p.GndrCd = f.GndrCd(+)
               AND p.EthnicCd = g.EthnicCd(+)
        UNION ALL
        SELECT j.BorrSeqNmb,
               j.TaxId
                   BorrTaxId,
               p.PrinSeqNmb,
               p.TaxId
                   PrinTaxId,
               PrinBusPerInd,
               p.VetCd,
               VetTxt,
               b.VetCertInd,
               p.GndrCd,
               GndrDesc,
               p.EthnicCd,
               EthnicDesc,
               LoanPrinPrimBusExprnceYrNmb,
               LoanPrinInsurNm,
               LoanPrinInsurAmt,
               LoanPrinGntyInd,
               NVL (LoanBusPrinPctOwnrshpBus, 0)
                   LoanBusPrinPctOwnrshpBus,
               NULL
                   BusTypCd,
               NULL
                   BusTypTxt,
               NULL
                   BusNm,
               NULL
                   BusTrdNm,
               ocadataout (PerFirstNm)
                   AS PerFirstNm,
               ocadataout (PerLastNm)
                   AS PerLastNm,
               PerInitialNm,
               PerSfxNm,
               PerTitlTxt,
               PerBrthDt,
               PerBrthCtyNm,
               PerBrthStCd,
               PerBrthCntCd,
               PerBrthCntNm,
               b.PerUSCitznInd,
               c.CitznshipTxt,
               PerAlienRgstrtnNmb,
               PerAffilEmpFedInd,
               PerIOBInd,
               PerIndctPrleProbatnInd,
               PerCrmnlOffnsInd,
               PerCnvctInd,
               ROUND (TO_NUMBER (PerLqdAssetAmt), 2)
                   PerLqdAssetAmt,
               ROUND (TO_NUMBER (PerBusOwnrshpAmt), 2)
                   PerBusOwnrshpAmt,
               ROUND (TO_NUMBER (PerREAmt), 2)
                   PerREAmt,
               ROUND (TO_NUMBER (PerOthAssetAmt), 2)
                   PerOthAssetAmt,
               ROUND (TO_NUMBER (PerTotAssetAmt), 2)
                   PerTotAssetAmt,
               ROUND (TO_NUMBER (PerRELiabAmt), 2)
                   PerRELiabAmt,
               ROUND (TO_NUMBER (PerCCDbtAmt), 2)
                   PerCCDbtAmt,
               ROUND (TO_NUMBER (PerInstlDbtAmt), 2)
                   PerInstlDbtAmt,
               ROUND (TO_NUMBER (PerOthLiabAmt), 2)
                   PerOthLiabAmt,
               ROUND (TO_NUMBER (PerTotLiabAmt), 2)
                   PerTotLiabAmt,
               ROUND (TO_NUMBER (PerNetWrthAmt), 2)
                   PerNetWrthAmt,
               ROUND (TO_NUMBER (PerAnnSalaryAmt), 2)
                   PerAnnSalaryAmt,
               ROUND (TO_NUMBER (PerOthAnnIncAmt), 2)
                   PerOthAnnIncAmt,
               PerSourcOfOthIncTxt,
               PerResOwnRentOthInd,
               ROUND (TO_NUMBER (PerMoHsngAmt), 2)
                   PerMoHsngAmt,
               NULL
                   BusExprtInd,
               PerFngrprntWaivDt,
               PerBnkrptcyInd,
               PerPndngLwsuitInd,
               NULL
                   BusPriorSBALoanInd,
               NULL
                   BusCurBnkNm,
               NULL
                   BusChkngBalAmt,
               NULL
                   BusCurOwnrshpDt,
               ocadataout (PerPhyAddrStr1Nm)
                   AS PerPhyAddrStr1Nm,
               ocadataout (PerPhyAddrStr2Nm)
                   AS PerPhyAddrStr2Nm,
               PerPhyAddrCtyNm,
               PerPhyAddrStCd,
               PerPhyAddrStNm,
               PerPhyAddrCntCd,
               PerPhyAddrZipCd,
               PerPhyAddrZip4Cd,
               PerPhyAddrPostCd,
               ocadataout (PerMailAddrStr1Nm)
                   AS PerMailAddrStr1Nm,
               ocadataout (PerMailAddrStr2Nm)
                   AS PerMailAddrStr2Nm,
               PerMailAddrCtyNm,
               PerMailAddrStCd,
               PerMailAddrStNm,
               PerMailAddrCntCd,
               PerMailAddrZipCd,
               PerMailAddrZip4Cd,
               PerMailAddrPostCd,
               PerExtrnlCrdtScorInd,
               b.IMCrdtScorSourcCd,
               d.IMCrdtScorSourcDescTxt,
               PerExtrnlCrdtScorNmb,
               PerExtrnlCrdtScorDt,
               j.LoanCntlIntInd
                   AS borrLoanCntlIntInd,
               bp.LoanCntlIntInd
                   AS busprinLoanCntlIntInd,
               (SELECT a.CNTLINTTYPTXT
                  FROM sbaref.LOANCNTLINTTYPCDTBL a
                 WHERE bp.LoanCntlIntTyp = a.CNTLINTTYPCD)
                   AS busprintLoanCntlIntTyp,                -- NK--08/15/2016
               p.LoanDisblInsurRqrdInd,
               p.LoanInsurDisblDescTxt,
               p.LoanLifeInsurRqrdInd,
               p.LoanPrinNoNCAEvdncInd,
               p.LoanComptitrNm,
               (SELECT e.LOANGNTYTYPTXT
                  FROM SBAREF.LOANGNTYTYPCDTBL e
                 WHERE e.LOANGNTYTYPCD = p.LACGNTYCONDTYPCD)
                   AS GntyCondTypTxt,
               (SELECT s.LOANGNTYLMTTYPTXT
                  FROM SBAREF.LOANGNTYLMTTYPCDTBL s
                 WHERE s.LOANGNTYLMTTYPCD = p.GntySubCondTypCd)
                   AS GntySubCondTypTxt,
               p.GntyLmtAmt,
               p.GntyLmtPct,
               p.LoanCollatSeqNmb,
               (SELECT i.LOANCOLLATDESC
                  FROM LOANAPP.LOANCOLLATTBL i
                 WHERE     i.LOANCOLLATSEQNMB = p.LoanCollatSeqNmb
                       AND i.LOANAPPNMB = p.LoanAppNmb)
                   AS LOANCOLLATDESC,
               (SELECT I.LOANCOLLATMRKTVALAMT
                  FROM LOANAPP.LOANCOLLATTBL i
                 WHERE     i.LOANCOLLATSEQNMB = p.LoanCollatSeqNmb
                       AND i.LOANAPPNMB = p.LoanAppNmb)
                   AS LOANCOLLATMRKTVALAMT,
               p.GntyLmtYrNmb,
               ocadataout (b.PerPrimPhnNmb)
                   PerPrimPhnNmb,
               ocadataout (b.PerAltPhnNmb)
                   AS PerAltPhnNmb,
               ocadataout (b.PerPrimEmailAdr)
                   AS PerPrimEmailAdr,
               ocadataout (b.PerAltEmailAdr)
                   AS PerAltEmailAdr,
               b.PerFedAgncySDPIEInd,
               b.perCSP60DayDelnqInd,
               b.PerLglActnInd,
               (SELECT z.IMCNTRYNM
                  FROM SBAREF.IMCNTRYCDTBL z
                 WHERE z.IMCNTRYCD = b.PerCitznShpCntNm)
                   AS PerCitznShpCntNm
          FROM LoanPerTbl                 b,
               LoanBorrTbl                j,
               LoanPrinTbl                p,
               LoanBusPrinTbl             bp,
               LoanPerFinanTbl            i,
               sbaref.CitznshipCdTbl      c,
               sbaref.IMCrdtScorSourcTbl  d,
               sbaref.VetTbl              e,
               sbaref.GndrCdTbl           f,
               sbaref.EthnicCdTbl         g
         WHERE     b.LoanAppNmb = p_LoanAppNmb
               AND j.LoanAppNmb = p_LoanAppNmb
               AND p.LoanAppNmb = p_LoanAppNmb
               AND bp.LoanAppNmb = p_LoanAppNmb
               AND j.BorrSeqNmb = bp.BorrSeqNmb
               AND p.PrinSeqNmb = bp.PrinSeqNmb
               AND b.TaxId = p.TaxId
               AND b.TaxId = i.TaxId(+)
               AND b.LoanAppNmb = i.LoanAppNmb(+)
               AND PrinBusPerInd = 'P'
               AND b.PerUSCitznInd = c.CitznshipCd(+)
               AND b.IMCrdtScorSourcCd = d.IMCrdtScorSourcCd(+)
               AND p.VetCd = e.VetCd(+)
               AND p.GndrCd = f.GndrCd(+)
               AND p.EthnicCd = g.EthnicCd(+)
        ORDER BY PrinSeqNmb;

    OPEN p_SelCur17 FOR
        SELECT PrinSeqNmb, a.RaceCd, b.RaceTxt
          FROM LoanPrinRaceTbl a, sbaref.RaceCdTbl b
         WHERE LoanAppNmb = p_LoanAppNmb AND a.RaceCd = b.RaceCd;

    OPEN p_SelCur18 FOR
          SELECT TaxId,
                 LoanFedEmpSeqNmb,
                 ocadataout (FedEmpFirstNm)        AS FedEmpFirstNm,
                 FedEmpInitialNm,
                 ocadataout (FedEmpLastNm)         AS FedEmpLastNm,
                 FedEmpSfxNm,
                 ocadataout (FedEmpAddrStr1Nm)     AS FedEmpAddrStr1Nm,
                 ocadataout (FedEmpAddrStr2Nm)     AS FedEmpAddrStr2Nm,
                 FedEmpAddrCtyNm,
                 FedEmpAddrStCd,
                 FedEmpAddrZipCd,
                 FedEmpAddrZip4Cd,
                 FedEmpAgncyOfc
            FROM LoanFedEmpTbl
           WHERE LoanAppNmb = p_LoanAppNmb
        ORDER BY TaxId, LoanFedEmpSeqNmb;

    OPEN p_SelCur19 FOR
          SELECT d.TaxId,
                 d.LoanIndbtnesSeqNmb,
                 d.LoanIndbtnesPayblToNm,
                 d.LoanIndbtnesPurpsTxt,
                 d.LoanIndbtnesOrglDt,
                 ROUND (TO_NUMBER (d.LoanIndbtnesCurBalAmt), 2)
                     LoanIndbtnesCurBalNum,
                 d.LoanIndbtnesIntPct,
                 d.LoanIndbtnesMatDt,
                 ROUND (TO_NUMBER (d.LoanIndbtnesPymtAmt), 2)
                     LoanIndbtnesPymtNum,
                 d.LoanIndbtnesCollatDescTxt,
                 freq.CalndrPrdDesc,
                 stat.LoanPrevFinanStatDescTxt
            FROM LoanIndbtnesTbl            d,
                 sbaref.CalndrPrdTbl        freq,
                 sbaref.LoanPrevFinanStatTbl stat
           WHERE     d.LoanAppNmb = p_LoanAppNmb
                 AND d.LoanIndbtnesPymtFreqCd = freq.CalndrPrdCd(+)
                 AND d.LoanIndbtnesPrevFinanStatCd =
                     stat.LoanPrevFinanStatCd(+)
        ORDER BY d.TaxId, d.LoanIndbtnesSeqNmb;

    OPEN p_SelCur20 FOR
        SELECT d.LoanPartLendrNm,
               d.LocId,
               d.LoanPartLendrStr1Nm,
               d.LoanPartLendrStr2Nm,
               d.LoanPartLendrCtyNm,
               d.LoanPartLendrStCd,
               d.LoanPartLendrZip5Cd,
               d.LoanPartLendrZip4Cd,
               d.LoanLienPosCd,
               ROUND (TO_NUMBER (d.LoanPartLendrAmt), 2)
                   LoanPartLendrNum,
               ocadataout (d.LoanPartLendrOfcrLastNm)
                   AS LoanPartLendrOfcrLastNm,
               ocadataout (d.LoanPartLendrOfcrFirstNm)
                   AS LoanPartLendrOfcrFirstNm,
               d.LoanPartLendrOfcrInitialNm,
               d.LoanPartLendrOfcrSfxNm,
               d.LoanPartLendrOfcrTitlTxt,
               ocadataout (d.LoanPartLendrOfcrPhnNmb)
                   AS LoanPartLendrOfcrPhnNmb,
               d.loanpartlendrcntrycd,
               d.loanpartlendrpostcd,
               d.loanpartlendrstnm,
               r.LoanPartLendrTypDescTxt,
               LOANLENDRTAXID,
               LOANLENDRGROSSINTPCT,
               LOANLENDRSERVFEEPCT,
               LOANLENDRPARTPCT,
               LOANORIGPARTPCT,
               LOANSBAGNTYBALPCT,
               LOANLENDRPARTAMT,
               LOANORIGPARTAMT,
               LOANLENDRGROSSAMT,
               LOANSBAGNTYBALAMT,
               LOAN504PARTLENDRSEQNMB,
               LOANPOOLAPPNMB
          FROM LoanPartLendrTbl d, sbaref.LoanPartLendrTypTbl r
         WHERE     d.LoanAppNmb = p_LoanAppNmb
               AND d.LoanPartLendrTypCd = r.LoanPartLendrTypCd(+);

    OPEN p_SelCur21 FOR
        SELECT b.ARCLoanRsnCd, b.ARCLoanRsnDescTxt, a.ARCLoanRsnOthDescTxt
          FROM loanapp.LoanAppARCRsnTbl a, sbaref.ARCLoanRsnTbl b
         WHERE     a.LoanAppNmb = p_LoanAppNmb
               AND a.ARCLoanRsnCd = b.ARCLoanRsnCd;

    OPEN p_SelCur22 FOR SELECT LOANAPPNMB,
                               IMASSTAREACD,
                               IMASSTOTHAREATXT,
                               CREATUSERID,
                               CREATDT
                          FROM LOANAPP.LOANAPPASSTAREATBL
                         WHERE LOANAPPNMB = p_LoanAppNmb;

    OPEN p_SelCur23 FOR SELECT LOANAPPNMB,
                               BUSEXISTIND,
                               GROSSREVNUAMT,
                               ASSTRECVIND,
                               CREATUSERID,
                               CREATDT,
                               LASTUPDTUSERID,
                               LASTUPDTDT
                          FROM LOANAPP.LOANAPPCAINFOTBL
                         WHERE LOANAPPNMB = p_LoanAppNmb;

    OPEN p_SelCur24 FOR SELECT LOANAPPNMB,
                               IMCNSELNGSRCTYPCD,
                               IMCNSELNGOTHSRCTXT,
                               CREATUSERID,
                               CREATDT
                          FROM LOANAPP.LOANAPPCNSELNGSRCTBL
                         WHERE LOANAPPNMB = p_LoanAppNmb;

    OPEN p_SelCur25 FOR SELECT LOANAPPNMB,
                               IMCNSELNGTYPCD,
                               IMCNSELNGHRCD,
                               CREATUSERID,
                               CREATDT
                          FROM LOANAPP.LOANAPPCNSELNGTBL
                         WHERE LOANAPPNMB = p_LoanAppNmb;

    OPEN p_SelCur26 FOR SELECT LOANAPPNMB,
                               TOTPYMTAMT,
                               LOAN7APYMTAMT,
                               SELLERFINANFULLSTBYAMT,
                               SELLERFINANNONFULLSTBYAMT,
                               BUYEREQTYCASHAMT,
                               BUYEREQTYCASHDESCTXT,
                               BUYEREQTYBORRAMT,
                               BUYEREQTYOTHAMT,
                               BUYEREQTYOTHDESCTXT,
                               TOTASSETAMT,
                               ACCTRECVASSETAMT,
                               INVTRYASSETAMT,
                               REASSETAMT,
                               REVALTYPIND,
                               EQUIPASSETAMT,
                               EQUIPVALTYPIND,
                               FIXASSETAMT,
                               INTANGASSETAMT,
                               OTHASSETAMT,
                               COVNTASSETAMT,
                               CUSTASSETAMT,
                               LICNSASSETAMT,
                               FRNCHSASSETAMT,
                               GOODWILLASSETAMT,
                               OTHINTANGASSETAMT,
                               OTHINTANGASSETDESCTXT,
                               TOTAPPRAMT,
                               BUSAPPRNM,
                               BUSAPPRFEEAMT,
                               BUSAPPRASAIND,
                               BUSAPPRCBAIND,
                               BUSAPPRABVIND,
                               BUSAPPRCVAIND,
                               BUSAPPRAVAIND,
                               BUSAPPRCPAIND,
                               BUSBRKRCOMISNIND,
                               BUSBRKRNM,
                               BUSBRKRCOMISNAMT,
                               BUSBRKRADR,
                               CREATUSERID,
                               CREATDT,
                               LASTUPDTUSERID,
                               LASTUPDTDT,
                               OTHASSETDESCTXT
                          FROM LOANAPPCHNGBUSOWNRSHPTBL
                         WHERE LoanAppNmb = p_LoanAppNmb;

    OPEN p_SelCur27 FOR
        SELECT i.LOANAPPNMB,
               i.LOANINTDTLSEQNMB,
               i.LOANINTLOANPARTPCT,
               i.LOANINTLOANPARTMONMB,
               i.LOANINTFIXVARIND,
               i.LOANINTRT,
               i.IMRTTYPCD,
               d.IMRTTYPDESCTXT,
               i.LOANINTBASERT,
               i.LOANINTSPRDOVRPCT,
               CASE
                   WHEN i.LOANINTADJPRDCD IS NULL
                   THEN
                       ' '
                   ELSE
                       (SELECT CALNDRPRDDESC
                          FROM sbaref.CalndrPrdTbl e
                         WHERE e.CALNDRPRDCD = i.LOANINTADJPRDCD)
               END
                   CALNDRPRDDESC,
               i.LOANINTADJPRDMONMB,
               i.LOANINTADJPRDEFFDT,
               LoanIntDtlGuarInd,
               (SELECT s.LOANINTDTLDESCTXT
                  FROM Sbaref.LOANINTDTLCDTBL s
                 WHERE s.LOANINTDTLCD = i.LOANINTFIXVARIND)
                   AS LOANINTDTLDESCTXT,
               (TRUNC (LoanGntyNoteDt))
                   LoanGntyNoteDt
          FROM LOANAPP.LOANINTDTLTBL  i,
               LOANAPP.LOANAPPTBL     a,
               sbaref.IMRtTypTbl      d
         WHERE     i.LOANAPPNMB = p_LOANAPPNMB
               AND i.LOANAPPNMB = a.LOANAPPNMB
               AND i.IMRTTYPCD = d.IMRTTYPCD
               AND a.PrcsMthdCd = d.PrcsMthdCd;

    OPEN p_SelCur28 FOR
        SELECT LOANAPPPYMTAMT,
               CASE
                   WHEN LOANPYMNINSTLMNTFREQCD = 'M' THEN 'month'
                   WHEN LOANPYMNINSTLMNTFREQCD = 'Q' THEN 'quarter'
                   WHEN LOANPYMNINSTLMNTFREQCD = 'S' THEN 'semi annual'
                   WHEN LOANPYMNINSTLMNTFREQCD = 'A' THEN 'annual'
                   ELSE 'Not Selected Yet'
               END
                   AS PYMTFREQTXT,
               PYMNTDAYNMB,
               -- PYMNTDAYSAMEIND ,
               PYMNTBEGNMONMB,
               ESCROWACCTRQRDIND,
               NETEARNCLAUSEIND,
               ARMMARTYPIND,
               CASE
                   WHEN ARMMARTYPIND = 'N' THEN 'None'
                   WHEN ARMMARTYPIND = 'F' THEN 'Ceiling and Floor Fluctuate'
                   WHEN ARMMARTYPIND = 'X' THEN 'Ceiling and Floor are Fixed'
                   ELSE NULL
               END
                   AS ARMMARTYPTXT,
               ARMMARCEILRT,
               ARMMARFLOORRT,
               STINTRTREDUCTNIND,
               LATECHGIND,
               LATECHGAFTDAYNMB,
               LATECHGFEEPCT,
               -- LOANAPPMOINTQTY ,
               PYMNTINTONLYBEGNMONMB,
               PYMNTINTONLYFREQCD,
               CASE
                   WHEN PYMNTINTONLYFREQCD = 'M' THEN 'month'
                   WHEN PYMNTINTONLYFREQCD = 'Q' THEN 'quarter'
                   WHEN PYMNTINTONLYFREQCD = 'S' THEN 'semi annual'
                   WHEN PYMNTINTONLYFREQCD = 'A' THEN 'annual'
                   ELSE 'Not Selected Yet'
               END
                   AS PYMTINTONLYFREQTXT,
               PYMNTINTONLYDAYNMB,
               --PYMNTINTONLYDAYSAMEIND,
               NetEarnPymntPct,
               NetEarnPymntOverAmt,
               StIntRtReductnPrgmNm,
               a.LOANAPPMOINTQTY,
               a.LoanPymtRepayInstlTypCd
          FROM LOANAPP.LOANPYMNTTBL p, LOANAPP.LoanAppTbl a
         WHERE a.LoanAppNmb = p_LoanAppNmb AND a.LoanAppNmb = p.LoanAppNmb(+);

    OPEN p_SelCur29 FOR
        SELECT LoanCollatSeqNmb,
               TaxId,
               BUSPERIND,
               CASE
                   WHEN BUSPERIND = 'B'
                   THEN
                       (SELECT BusNm
                          FROM loanapp.LOANbustbl z
                         WHERE     z.loanappnmb = p_loanappnmb
                               AND z.taxid = c.taxid)
                   ELSE
                       (SELECT    ocadataout (PerFirstNm)
                               || LTRIM (
                                         ' '
                                      || CASE
                                             WHEN PerInitialNm IS NOT NULL
                                             THEN
                                                 PerInitialNm || '.'
                                             ELSE
                                                 NULL
                                         END)
                               || LTRIM (' ' || ocadataout (PerLastNm))
                               || LTRIM (' ' || PerSfxNm)
                          FROM loanapp.loanpertbl z
                         WHERE     z.loanappnmb = p_loanappnmb
                               AND z.taxid = c.taxid)
               END
                   AS BUSPERNm
          FROM loanapp.LoanLmtGntyCollatTbl c
         WHERE LoanAppNmb = p_LoanAppNmb;

    --StbyAgrmt
    OPEN p_SelCur30 FOR
        SELECT a.LOANSTBYCRDTRNM,
               a.LOANSTBYAMT,
               (SELECT s.STBYPYMTTYPDESCTXT
                  FROM SBAREF.STBYPYMTTYPCDTBL s
                 WHERE a.LOANSTBYREPAYTYPCD = S.STBYPYMTTYPCD)
                   AS LOANSTBYPYMTTYPDESCTXT,
               a.LOANSTBYPYMTINTRT,
               a.LOANSTBYPYMTOTHDESCTXT,
               a.LOANSTBYPYMTAMT,
               a.LOANSTBYREPAYTYPCD,
               a.LOANSTBYPYMTSTRTDT
          FROM LOANAPP.LOANStbyAgrmtDtlTBL a
         WHERE a.LOANAPPNMB = p_LOANAPPNMB;

    --Export Country Codes---
    OPEN p_SelCur31 FOR
          SELECT a.LoanAppNmb,
                 a.LoanExprtCntryCd,
                 b.imcntrynm,
                 a.LoanExprtCntrySeqNmb
            FROM LOANAPP.LOANEXPRTCNTRYTBL a, SBAREF.IMCNTRYCDTBL b
           WHERE     a.LOANAPPNMB = p_loanappnmb
                 AND a.LoanExprtCntryCd = b.ImCntryCd
        ORDER BY LOANEXPRTCNTRYSEQNMB ASC;

    OPEN p_SelCur32 FOR
          SELECT a.LOANAGNTSEQNMB,
                 a.LOANAGNTID,
                 a.LOANAGNTBUSPERIND,
                 a.LOANAGNTNM,
                    ocadataout (a.LOANAGNTCNTCTFIRSTNM)
                 || RTRIM (' ' || a.LOANAGNTCNTCTMidNm)
                 || ' '
                 || ocadataout (a.LOANAGNTCNTCTLastNm)
                 || RTRIM (' ' || a.LOANAGNTCNTCTSfxNm)
                     AS LoanContactName,
                    ocadataout (a.LOANAGNTADDRSTR1NM)
                 || RTRIM (' ' || ocadataout (a.LOANAGNTADDRSTR2NM))
                 || ' '
                 || a.LOANAGNTADDRCTYNM
                 || ' '
                 || RTRIM (' ' || a.LOANAGNTADDRSTCD)
                 || ' '
                 || RTRIM (' ' || a.LOANAGNTADDRSTNM)
                 || ' '
                 || RTRIM (' ' || a.LOANAGNTADDRZIPCD)
                 || ' '
                 || RTRIM (' ' || a.LOANAGNTADDRZIP4CD)
                 || ' '
                 || RTRIM (' ' || a.LOANAGNTADDRPOSTCD)
                 || ' '
                 || a.LOANAGNTADDRCNTCD
                     AS LoanContactaddress,
                 a.LOANAGNTTYPCD,
                 a.LOANAGNTDOCUPLDIND,
                 a.LOANCDCTPLFEEIND,
                 a.LOANCDCTPLFEEAMT,
                 b.LOANAGNTSERVTYPCD,
                 b.LOANAGNTSERVOTHTYPTXT,
                 b.LOANAGNTAPPCNTPAIDAMT,
                 b.LOANAGNTSBALENDRPAIDAMT
            FROM LOANAPP.LoanAgntTbl a, loanagntfeedtltbl b
           WHERE     a.LOANAPPNMB = p_LOANAPPNMB
                 AND a.loanappnmb = b.loanappnmb
                 AND a.loanagntseqnmb = b.loanagntseqnmb
        ORDER BY loanagntseqnmb ASC;
END LOANPRINTSELCSP;
/

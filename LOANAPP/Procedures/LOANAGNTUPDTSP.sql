CREATE OR REPLACE PROCEDURE LOANAPP.LoanAgntUPDTSP (p_identifier             IN     NUMBER := 0,
                                                    p_retval                    OUT NUMBER,
                                                    p_LOANAPPNMB                    NUMBER := NULL,
                                                    p_LOANAGNTSEQNMB                NUMBER := NULL,
                                                    p_LOANAGNTBUSPERIND             CHAR := NULL,
                                                    p_LOANAGNTNM                    VARCHAR2 := NULL,
                                                    p_LOANAGNTCNTCTFIRSTNM          VARCHAR2 := NULL,
                                                    p_LOANAGNTCNTCTMIDNM            CHAR := NULL,
                                                    p_LOANAGNTCNTCTLASTNM           VARCHAR2 := NULL,
                                                    p_LOANAGNTCNTCTSFXNM            VARCHAR2 := NULL,
                                                    p_LOANAGNTADDRSTR1NM            VARCHAR2 := NULL,
                                                    p_LOANAGNTADDRSTR2NM            VARCHAR2 := NULL,
                                                    p_LOANAGNTADDRCTYNM             VARCHAR2 := NULL,
                                                    p_LOANAGNTADDRSTCD              CHAR := NULL,
                                                    p_LOANAGNTADDRSTNM              VARCHAR2 := NULL,
                                                    p_LOANAGNTADDRZIPCD             CHAR := NULL,
                                                    p_LOANAGNTADDRZIP4CD            CHAR := NULL,
                                                    p_LOANAGNTADDRPOSTCD            VARCHAR2 := NULL,
                                                    p_LOANAGNTADDRCNTCD             CHAR := NULL,
                                                    p_LOANAGNTTYPCD                 NUMBER := NULL,
                                                    p_LOANAGNTDOCUPLDIND            CHAR := NULL,
                                                    p_LOANCDCTPLFEEIND              CHAR := NULL,
                                                    p_LOANCDCTPLFEEAMT              NUMBER := NULL,
                                                    p_LOANAGNTID                    NUMBER := NULL,
                                                    p_LOANAGENTTYPEOTHER            VARCHAR2 := NULL,
                                                    p_Lastupdtuserid                VARCHAR2 := NULL)
AS
--SS--7/16/2018--OPSMDEV 1862
--SS--11/15/2018 Added function for encryption
BEGIN
    SAVEPOINT LoanAgntUPDTSP;

    IF p_identifier = 0
    THEN
        BEGIN
            UPDATE LOANAPP.LoanAgntTBL
               SET LOANAGNTBUSPERIND = p_LOANAGNTBUSPERIND,
                   LOANAGNTNM = p_LOANAGNTNM,
                   LOANAGNTCNTCTFIRSTNM = ocadatain (p_LOANAGNTCNTCTFIRSTNM),
                   LOANAGNTCNTCTMIDNM = p_LOANAGNTCNTCTMIDNM,
                   LOANAGNTCNTCTLASTNM = ocadatain (p_LOANAGNTCNTCTLASTNM),
                   LOANAGNTCNTCTSFXNM = p_LOANAGNTCNTCTSFXNM,
                   LOANAGNTADDRSTR1NM = ocadatain (p_LOANAGNTADDRSTR1NM),
                   LOANAGNTADDRSTR2NM = ocadatain (p_LOANAGNTADDRSTR2NM),
                   LOANAGNTADDRCTYNM = p_LOANAGNTADDRCTYNM,
                   LOANAGNTADDRSTCD = p_LOANAGNTADDRSTCD,
                   LOANAGNTADDRSTNM = p_LOANAGNTADDRSTNM,
                   LOANAGNTADDRZIPCD = p_LOANAGNTADDRZIPCD,
                   LOANAGNTADDRZIP4CD = p_LOANAGNTADDRZIP4CD,
                   LOANAGNTADDRPOSTCD = p_LOANAGNTADDRPOSTCD,
                   LOANAGNTADDRCNTCD = p_LOANAGNTADDRCNTCD,
                   LOANAGNTTYPCD = p_LOANAGNTTYPCD,
                   LOANAGNTDOCUPLDIND = p_LOANAGNTDOCUPLDIND,
                   LOANCDCTPLFEEIND = p_LOANCDCTPLFEEIND,
                   LOANCDCTPLFEEAMT = p_LOANCDCTPLFEEAMT,
                   LOANAGNTID = p_LOANAGNTID,
                   LOANAGNTOTHTYPTXT = p_LOANAGENTTYPEOTHER,
                   Lastupdtuserid = p_Lastupdtuserid,
                   LastupdtDt = SYSDATE
             WHERE LOANAPPNMB = p_LOANAPPNMB AND LOANAGNTSEQNMB = p_LOANAGNTSEQNMB;


            p_retval := SQL%ROWCOUNT;
        END;
    END IF;
EXCEPTION
    WHEN OTHERS
    THEN
        ROLLBACK TO LoanAgntUPDTSP;
        RAISE;
END;
/

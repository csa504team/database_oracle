CREATE OR REPLACE PROCEDURE LOANAPP.LOANCDCCNTCTINSTSP (p_Identifier           IN     NUMBER DEFAULT 0,
                                                        P_Loanappnmb           IN     NUMBER DEFAULT NULL,
                                                        P_CDCCntctFullNm       IN     VARCHAR2 DEFAULT NULL,
                                                        P_Cdccntctphone        IN     VARCHAR2 DEFAULT NULL,
                                                        P_Cdccntctemailadr     IN     VARCHAR2 DEFAULT NULL,
                                                        P_Cdccntctfax          IN     VARCHAR2 DEFAULT NULL,
                                                        P_CLSAttyFullNm        IN     VARCHAR2 DEFAULT NULL,
                                                        P_Clsattyphone         IN     VARCHAR2 DEFAULT NULL,
                                                        P_Clsattyemailadr      IN     VARCHAR2 DEFAULT NULL,
                                                        P_Clsattyfax           IN     VARCHAR2 DEFAULT NULL,
                                                        P_Clsattysbadesigind   IN     VARCHAR2 DEFAULT NULL,
                                                        P_Debauthdt            IN     DATE DEFAULT NULL,
                                                        P_Fileorderdt          IN     DATE DEFAULT NULL,
                                                        P_Creatuserid          IN     VARCHAR2 DEFAULT USER,
                                                        P_Creatdt              IN     DATE DEFAULT SYSDATE,
                                                        p_RetVal                  OUT NUMBER)
AS
/* Date         : June 01, 2019
   Programmer   : Jose Paul
*********************************************************************************
Revised By      Date        Commments
*********************************************************************************
*/
BEGIN
    SAVEPOINT LOANCDCCNTCTINSTSP;

    /* Insert into LOANCDCCNTCTTBL */
    IF p_Identifier = 0 THEN
        BEGIN
            INSERT INTO LOANAPP.LOANCDCCNTCTTBL (LoanAppNmb,
                                                 CDCCntctFullNm,
                                                 CdccntctPhone,
                                                 CdccntctEmailadr,
                                                 CdccntctFax,
                                                 CLSAttyFullNm,
                                                 ClsattyPhone,
                                                 ClsattyEmailadr,
                                                 ClsattyFax,
                                                 ClsattySBAdesigind,
                                                 DebAuthdt,
                                                 FileOrderdt,
                                                 CreatUserid,
                                                 CreatDt                                                   --,
                                                        -- Lastupdtuserid,
                                                        --  Lastupdtdt
                                                        )
                 VALUES (P_Loanappnmb,
                         P_CDCCntctFullNm,
                         P_Cdccntctphone,
                         P_Cdccntctemailadr,
                         P_Cdccntctfax,
                         P_CLSAttyFullNm,
                         P_Clsattyphone,
                         P_Clsattyemailadr,
                         P_Clsattyfax,
                         P_Clsattysbadesigind,
                         P_Debauthdt,
                         P_Fileorderdt,
                         P_Creatuserid,
                         P_Creatdt);
        END;
    END IF;

    p_RetVal := SQL%ROWCOUNT;
EXCEPTION
    WHEN OTHERS THEN
        RAISE;
        ROLLBACK TO LOANCDCCNTCTINSTSP;
END LOANCDCCNTCTINSTSP;
/
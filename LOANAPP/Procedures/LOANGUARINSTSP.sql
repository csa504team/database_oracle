CREATE OR REPLACE PROCEDURE LOANAPP.LOANGUARINSTSP(
p_Identifier         NUMBER:=0 ,
p_RetVal         OUT NUMBER,
p_LoanAppNmb         NUMBER:=0 ,
p_GuarSeqNmb     OUT NUMBER,
p_TaxId              CHAR:=NULL,
p_GuarBusPerInd      CHAR:=NULL,
p_LoanGuarOperCoInd  CHAR:=NULL,
p_GuarCreatUserId    CHAR:=NULL,
p_LACGntyCondTypCd   NUMBER := NULL, 
p_GntySubCondTypCd   NUMBER := NULL, 
p_GntyLmtAmt         NUMBER := NULL, 
p_GntyLmtPct         NUMBER := NULL, 
p_LoanCollatSeqNmb   NUMBER := NULL, 
p_GntyLmtYrNmb       NUMBER := NULL,
p_LIABINSURRQRDIND               CHAR:=NULL,
p_PRODLIABINSURRQRDIND           CHAR:=NULL,
p_LIQLIABINSURRQRDIND            CHAR:=NULL,
p_MALPRCTSINSURRQRDIND           CHAR:=NULL,
p_OTHINSURRQRDIND                CHAR:=NULL,
p_WORKRSCOMPINSRQRDIND           CHAR:=NULL,
p_OTHINSURDESCTXT                VARCHAR2:=NULL
)
AS
/*  Date         : October 1, 2000
    Programmer   : Sheri Stine
    Remarks      :
    Parameters   :  No.   Name        Type     Description
                    1     p_Identifier  int     Identifies which batch
                                               to execute.
                    2     p_RetVal      int     No. of rows returned/
                                               affected
                    3    table columns to be inserted
*********
* *********************************************************************************
Revised By      Date        Commments
SP              9/26/2012   Modified as column LoanAffilInd is added to the table 
                            for determining companion relationship
SP              02/04/2014  Modified to add column LACGntyCondTypCd to support LADS
*********************************************************************************
*/
--NK--09/09/2016--OPSMDEV1018-- Replaced LOANAFFILIND with LoanCntlIntInd 
--BR--09/30/2016--OPSMDEV    -- Added new columns GntySubCondTypCd, GntyLmtAmt, GntyLmtPct, LoanCollatSeqNmb, GntyLmtYrNmb  

   BEGIN
        
            SAVEPOINT LOANGUARINSTSP;
        
/* Insert into LoanGuar Table */
        IF  p_Identifier = 0 THEN
        BEGIN
        
            
    INSERT INTO LOANAPP.LoanGuarTbl(LoanAppNmb, 
                                    GuarSeqNmb,
                                    TaxId, 
                                    GuarBusPerInd,
                                    LoanGuarOperCoInd,
                                    GuarCreatUserId,
                                    GuarCreatDt,
                                    LastUpdtUserId,
                                    LastUpdtDt,
                                    LACGntyCondTypCd,
                                    GntySubCondTypCd,
                                    GntyLmtAmt,
                                    GntyLmtPct,
                                    LoanCollatSeqNmb,
                                    GntyLmtYrNmb,
                                    LIABINSURRQRDIND,
                                    PRODLIABINSURRQRDIND,
                                    LIQLIABINSURRQRDIND,
                                    MALPRCTSINSURRQRDIND,
                                    OTHINSURRQRDIND,
                                    WORKRSCOMPINSRQRDIND,
                                    OTHINSURDESCTXT
)
                            SELECT  p_LoanAppNmb, 
                                    NVL((select MAX(GuarSeqNmb)
                                         from loanapp.LoanGuarTbl z
                                         where z.LoanAppNmb = p_LoanAppNmb), 0) + 1, 
                                    p_TaxId, 
                                    p_GuarBusPerInd,
                                    p_LoanGuarOperCoInd, 
                                    nvl(p_GuarCreatUserId,user),
                                    SYSDATE,
                                    nvl(p_GuarCreatUserId,user),
                                    sysdate,
                                    p_LACGntyCondTypCd,
                                    p_GntySubCondTypCd,
                                    p_GntyLmtAmt,
                                    p_GntyLmtPct,
                                    p_LoanCollatSeqNmb,
                                    p_GntyLmtYrNmb,
                                    p_LIABINSURRQRDIND,
                                    p_PRODLIABINSURRQRDIND,
                                    p_LIQLIABINSURRQRDIND,
                                    p_MALPRCTSINSURRQRDIND,
                                    p_OTHINSURRQRDIND,
                                    p_WORKRSCOMPINSRQRDIND,
                                    p_OTHINSURDESCTXT
                           FROM DUAL;
                
               /* LoanGuarTbl
            WHERE LoanAppNmb = p_LoanAppNmb;*/
            
            p_RetVal  := SQL%ROWCOUNT;
            SELECT   MAX(GuarSeqNmb)INTO p_GuarSeqNmb
             FROM LoanGuarTbl
            WHERE LoanAppNmb = p_LoanAppNmb;
            END;
            END IF;
p_RetVal := nvl(p_RetVal,0);    
p_GuarSeqNmb := nvl(p_GuarSeqNmb,0);
  EXCEPTION
    WHEN OTHERS THEN
    BEGIN
    ROLLBACK TO LOANGUARINSTSP;
    RAISE;
    END ;
    END;
/


GRANT EXECUTE ON LOANAPP.LOANGUARINSTSP TO CDCONLINEDEVROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARINSTSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARINSTSP TO LOANACCTREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARINSTSP TO LOANAPPDEVROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARINSTSP TO LOANAPPREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARINSTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARINSTSP TO LOANDEVROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARINSTSP TO LOANORIGBORROWER;

GRANT EXECUTE ON LOANAPP.LOANGUARINSTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARINSTSP TO LOANREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARINSTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARINSTSP TO POOLSECADMINROLE;

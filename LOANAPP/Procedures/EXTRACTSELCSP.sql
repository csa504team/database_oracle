CREATE OR REPLACE PROCEDURE LOANAPP.EXTRACTSELCSP (p_LoanAppNmb       IN     NUMBER := NULL,
                                                   p_PrtId                   NUMBER := 0,
                                                   p_RetVal              OUT NUMBER,
                                                   p_SelCur1             OUT SYS_REFCURSOR,
                                                   p_SelCur2             OUT SYS_REFCURSOR,
                                                   p_SelCur3             OUT SYS_REFCURSOR,
                                                   p_SelCur4             OUT SYS_REFCURSOR,
                                                   p_SelCur5             OUT SYS_REFCURSOR,
                                                   p_SelCur6             OUT SYS_REFCURSOR,
                                                   p_SelCur7             OUT SYS_REFCURSOR,
                                                   p_SelCur8             OUT SYS_REFCURSOR,
                                                   p_SelCur9             OUT SYS_REFCURSOR,
                                                   p_SelCur10            OUT SYS_REFCURSOR,
                                                   p_SelCur11            OUT SYS_REFCURSOR,
                                                   p_SelCur12            OUT SYS_REFCURSOR,
                                                   p_SelCur13            OUT SYS_REFCURSOR,
                                                   p_SelCur14            OUT SYS_REFCURSOR,
                                                   p_SelCur15            OUT SYS_REFCURSOR,
                                                   p_SelCur16            OUT SYS_REFCURSOR,
                                                   p_SelCur17            OUT SYS_REFCURSOR,
                                                   p_SelCur18            OUT SYS_REFCURSOR,
                                                   p_SelCur19            OUT SYS_REFCURSOR,
                                                   p_SelCur20            OUT SYS_REFCURSOR,
                                                   p_SelCur21            OUT SYS_REFCURSOR,
                                                   p_SelCur22            OUT SYS_REFCURSOR,
                                                   p_SelCur23            OUT SYS_REFCURSOR,
                                                   p_SelCur24            OUT SYS_REFCURSOR,
                                                   p_SelCur25            OUT SYS_REFCURSOR,
                                                   p_SelCur26            OUT SYS_REFCURSOR,
                                                   p_SelCur27            OUT SYS_REFCURSOR,
                                                   p_SelCur28            OUT SYS_REFCURSOR,
                                                   p_SelCur29            OUT SYS_REFCURSOR,
                                                   p_SelCur30            OUT SYS_REFCURSOR,
                                                   p_SelCur31            OUT SYS_REFCURSOR,
                                                   p_SelCur32            OUT SYS_REFCURSOR,
                                                   p_SelCur33            OUT SYS_REFCURSOR,
                                                   p_SelCur34            OUT SYS_REFCURSOR,
                                                   p_SelCur35            OUT SYS_REFCURSOR,
                                                   p_SelCur36            OUT SYS_REFCURSOR,
                                                   p_SelCur37            OUT SYS_REFCURSOR,
                                                   p_SelCur38            OUT SYS_REFCURSOR,
                                                   p_SelCur39            OUT SYS_REFCURSOR,
                                                   p_SelCur40            OUT SYS_REFCURSOR,
                                                   p_SelCur41            OUT SYS_REFCURSOR,
                                                   p_SelCur42            OUT SYS_REFCURSOR,
                                                   p_SelCur43            OUT SYS_REFCURSOR,
                                                   p_SelCur44            OUT SYS_REFCURSOR)
AS
   /*SB -- 07/07/2014 This procedure created for Extract XML */
   /*SB -- 02/24/2015 Added p_selcur35  */
   /*SB -- 03/27/2015 Added p_selcur36  */
   /*NK -- 03/17/2016 Added cursor initialization code when conditions are not met*/
   /*SB -- 04/07/2016 Added p_selcur37  */
   /*BR -- 10/21/2016 Added p_selcur38, p_selcur39 */
   /*Nk -- 03/10/2017 OPSMDEV 1385 Added p_selcur40*/
   --SS--07/12/2018 OPSMDEV 1860 Added p_selcur 42 and 43
   -- JP 08/06/2019 OPSMDEV 2255 modified call to LoanAppSelTSP 
   v_RetVal   NUMBER;
   v_PrtId    NUMBER (7);
BEGIN
   -- verify that the count of LoanGntySelTSP identifier 38 is greater than 0
   SELECT COUNT (*)
     INTO p_RetVal
     FROM loanapp.LoanAppTbl
    WHERE LoanAppNmb = p_LoanAppNmb;

   IF NVL (p_PrtId, 0) = 0
   THEN
      BEGIN
         SELECT PrtId
           INTO v_PrtId
           FROM loanapp.LoanAppPrtTbl
          WHERE LoanAppNmb = p_LoanAppNmb;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_PrtId := NULL;
      END;
   END IF;

   IF p_RetVal = 0
   THEN
      BEGIN
         OPEN p_SelCur1 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur2 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur3 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur4 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur5 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur6 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur7 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur8 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur9 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur10 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur11 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur12 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur13 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur14 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur15 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur16 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur17 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur18 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur19 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur20 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur21 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur22 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur23 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur24 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur25 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur26 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur27 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur28 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur29 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur30 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur31 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur32 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur33 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur34 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur35 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur36 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur37 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur38 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur39 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur40 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur41 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur42 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;

         OPEN p_SelCur43 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;
             
        OPEN p_SelCur44 FOR
            SELECT v_RetVal
              FROM DUAL
             WHERE 1 = 0;
             
      END;
   ELSE
      BEGIN
         --LoanAppSelTSP (38, p_LoanAppNmb, NULL, NULL, NULL, NULL, v_RetVal, p_SelCur1);
         -- JP Modifed LoanAppSelTSP for 2255 
        LoanAppSelTSP (p_identifier => 38, 
                       p_loanappnmb => p_LoanAppNmb, 
                       p_retval => v_RetVal, 
                       p_selcur => p_SelCur1);
         LoanAppARCRsnSelTSP (38, v_RetVal, p_LoanAppNmb, NULL, p_SelCur2);
         LoanAppAsstAreaSelTSP (38, v_RetVal, p_LoanAppNmb, p_SelCur3);
         LoanAppCAInfoSelTSP (38, v_RetVal, p_LoanAppNmb, p_SelCur4);
         LoanAppChngBusOwnrshpSelTSP (38, v_RetVal, p_LoanAppNmb, p_SelCur5);
         LoanAppCnselngSelTSP (38, v_RetVal, p_LoanAppNmb, p_SelCur6);
         LoanAppCnselngSrcSelTSP (38, v_RetVal, p_LoanAppNmb, p_SelCur7);
         LoanAppEligSelTSP (38, p_LoanAppNmb, NULL, p_SelCur8);
         LoanAppPrtSelTSP (38, p_LoanAppNmb, v_RetVal, p_SelCur9);
         LoanBorrSelTSP (38, p_LoanAppNmb, NULL, NULL, NULL, v_RetVal, p_SelCur10);
         LoanBSSelTSP (38, p_LoanAppNmb, v_RetVal, p_SelCur11);
         LoanCollatSelTSP (38, p_LoanAppNmb, NULL, p_SelCur12);
         LoanCollatLienSelTSP (38, p_LoanAppNmb, NULL, NULL, p_SelCur13);
         LoanCrdtUnavRsnSelTSP (38, p_LoanAppNmb, NULL, v_RetVal, p_SelCur14);
         LoanFedEmpSelTSP (38, p_LoanAppNmb, NULL, NULL, NULL, NULL, v_RetVal, p_SelCur15);
         LoanGuarSelTSP (38, p_LoanAppNmb, NULL, NULL, NULL, v_RetVal, p_SelCur16);
         LoanIndbtnesSelTSP (38, p_LoanAppNmb, NULL, NULL, NULL, v_RetVal, p_SelCur17);
         LoanInjctnSelTSP (38, p_LoanAppNmb, NULL, v_RetVal, p_SelCur18);
         LoanIntDtlSelTSP (38, p_LoanAppNmb, p_SelCur19);
         LoanISSelTSP (38, p_LoanAppNmb, NULL, v_RetVal, p_SelCur20);
         LoanPartLendrSelTSP (38, p_LoanAppNmb, NULL, v_RetVal, p_SelCur21);
         LoanPerFinanSelTSP (38, p_LoanAppNmb, NULL, v_RetVal, p_SelCur22);
         LoanPrevFinanSelTSP (38, p_LoanAppNmb, NULL, NULL, v_RetVal, p_SelCur23);
         LoanPrinSelTSP (38, p_LoanAppNmb, NULL, NULL, NULL, NULL, v_RetVal, p_SelCur24);
         LoanPrinRaceSelTSP (38, p_LoanAppNmb, NULL, NULL, NULL, v_RetVal, p_SelCur25);
         LoanProcdSelTSP (38, p_LoanAppNmb, NULL, NULL, v_RetVal, p_SelCur26);
         LoanPrtCmntSelTSP (38, p_LoanAppNmb, v_RetVal, p_SelCur27);
         LoanSpcPurpsSelTSP (38, p_LoanAppNmb, NULL, NULL, NULL, v_RetVal, p_SelCur28);
         LqdCrSelCSP (38, p_LoanAppNmb, v_RetVal, p_SelCur29, p_SelCur30, p_SelCur31, p_SelCur32, p_SelCur33);
         partner.PrtAgrmtSelTSP (38, v_RetVal, v_prtid, NULL, NULL, NULL, NULL, p_SelCur34);
         loanapp.LoanBorrRaceSelTSP (38, p_LoanAppNmb, NULL, v_RetVal, p_SelCur35);
         LOANAPP.LoanAppBusApprSelTSP (38, v_RetVal, p_LoanAppNmb, p_SelCur36);
         LOANAPP.LoanAppCAUndrServMrktSelTSP (38, v_RetVal, p_LoanAppNmb, p_SelCur37);
         LOANAPP.LoanLmtGntyCollatSelTSP (38, p_LoanAppNmb, NULL, NULL, NULL, NULL, p_SelCur38, v_RetVal);
         LOANAPP.LoanStbyAgrmtDtlSelTSP (38, p_LoanAppNmb, NULL, p_SelCur39, v_RetVal);
         LOANAPP.LoanEconDevObjctChldSelTsp (38, p_LoanAppNmb, NULL, p_SELCUR40, v_RetVal);
         LOANAPP.LoanExprtCntrySelTsp (38, p_LoanAppNmb, p_SELCUR41, v_RetVal);
         LoanApp.LoanAgntSelTsp (38, p_LoanAppNmb, NULL, p_SELCUR42, v_RetVal);
         LoanApp.LoanAgntfeeDtlSelTSp (38, p_LoanAppNmb, NULL, p_SELCUR43, v_RetVal);
         LoanApp.LoanSubProcdSelTSP (38, p_LoanAppNmb, NULL,NULL,v_RetVal,p_SelCur44);
      END;
   END IF;
END ExtractSelCSP;
/

CREATE OR REPLACE PROCEDURE LOANAPP.LOANAPPUPDCSP (p_Identifier                         NUMBER := NULL,
                                                   p_RetVal                         OUT NUMBER,
                                                   p_LoanAppNmb                         NUMBER := 0,
                                                   p_LoanAppNm                          VARCHAR2 := NULL,
                                                   p_LoanAppRqstAmt                     NUMBER := NULL,
                                                   p_LoanAppSBAGntyPct                  NUMBER := NULL,
                                                   p_LoanAppSBARcmndAmt                 NUMBER := NULL,
                                                   p_LoanAppRqstMatMoQty                NUMBER := NULL,
                                                   p_LoanAppEPCInd                      CHAR := NULL,
                                                   p_LoanAppPymtAmt                     NUMBER := NULL,
                                                   p_LoanAppFullAmortPymtInd            CHAR := NULL,
                                                   p_LoanAppMoIntQty                    NUMBER := NULL,
                                                   p_LoanAppLifInsurRqmtInd             CHAR := NULL,
                                                   p_LoanAppRcnsdrtnInd                 CHAR := NULL,
                                                   p_LoanAppInjctnInd                   CHAR := NULL,
                                                   p_LoanAppEWCPSnglTransPostInd        CHAR := NULL,
                                                   p_LoanAppEWCPSnglTransInd            CHAR := NULL,
                                                   p_LoanAppDisasterCntrlNmb            VARCHAR2 := NULL,
                                                   p_LoanCollatInd                      CHAR := NULL,
                                                   p_SpcPurpsLoanCds                    VARCHAR2 := NULL,
                                                   p_LoanAppCreatUserId                 CHAR := NULL,
                                                   p_RecovInd                           CHAR := NULL,
                                                   p_LoanAppOutPrgrmAreaOfOperInd       CHAR := NULL,
                                                   p_LoanAppParntLoanNmb                CHAR := NULL,
                                                   p_LoanAppCDCGntyAmt                  NUMBER := 0,
                                                   p_LoanAppCDCNetDbentrAmt             NUMBER := 0,
                                                   p_LoanAppCDCFundFeeAmt               NUMBER := 0,
                                                   p_LoanAppCDCSeprtPrcsFeeInd          CHAR := NULL,
                                                   p_LoanAppCDCPrcsFeeAmt               NUMBER := 0,
                                                   p_LoanAppCDCClsCostAmt               NUMBER := 0,
                                                   p_LoanAppCDCOthClsCostAmt            NUMBER := 0,
                                                   p_LoanAppCDCUndrwtrFeeAmt            NUMBER := 0,
                                                   p_LoanAppContribPct                  NUMBER := 0,
                                                   p_LoanAppContribAmt                  NUMBER := 0,
                                                   p_LOANDISASTRAPPFEECHARGED           NUMBER := 0,
                                                   p_LOANDISASTRAPPDCSN                 CHAR := NULL,
                                                   p_LOANASSOCDISASTRAPPNMB             CHAR := NULL,
                                                   p_LOANASSOCDISASTRLOANNMB            CHAR := NULL,
                                                   p_LoanAppPymtDefMoNmb                NUMBER := NULL,
                                                   p_LOANDISASTRAPPNMB                  CHAR := NULL,
                                                   p_LOANPYMNINSTLMNTFREQCD             CHAR := NULL,
                                                   p_LOANAPPINTDTLCD                    VARCHAR2 := NULL,
                                                   p_UNDRWRITNGBY                       CHAR := NULL,
                                                   p_LOANAMTLIMEXMPTIND                 CHAR := NULL,
                                                   p_LoanPymntSchdlFreq                 VARCHAR2 := NULL,
                                                   p_LoanPymtRepayInstlTypCd            CHAR := NULL,
                                                   p_LOANGNTYMATSTIND                   CHAR := NULL,
                                                   p_LoanGntyNoteDt                     DATE := NULL,
                                                   p_LoanAppCDCGrossDbentrAmt           NUMBER := NULL,
                                                   p_LoanAppBalToBorrAmt                NUMBER := NULL,
                                                   p_LoanAppRevlMoIntQty                NUMBER := NULL,
                                                   p_LoanAppAmortMoIntQty               NUMBER := NULL,
                                                   p_LoanAppExtraServFeeInd             CHAR := NULL,
                                                   p_LoanAppExtraServFeeAMT             NUMBER := NULL,
                                                   p_LoanAppNetEarnInd                  CHAR := NULL,
                                                   p_LoanAppMonthPayroll                NUMBER := 0,
                                                   p_LoanAppUSResInd                    CHAR := NULL,
                                                   p_LOANAPPSCHEDCIND                   CHAR := NULL, 
                                                   p_LOANAPPSCHEDCYR                    NUMBER := NULL, 
                                                   p_LOANAPPGROSSINCOMEAMT              NUMBER := NULL) AS
    /* Date         :
    Programmer   :
    Remarks      :
    Parameters   :
    ***********************************************************************************
    APK -- 02/06/2012 -- modified to add new column LOANDISASTRAPPNMB, as its added to the loanapptbl in development.
    RSURAP - 10/29/2013 -- Modified the procedure to add new column LoanAppPymtAdjPrdCd
    RGG   -- 11/07/2013 -- Modified to LoanAppPymtAdjPrdCd rename to LOANPYMNINSTLMNTFREQCD
    SB -   10//2014 -- Modified to add parameter and to update UNDRWRITNGBY column
    SS-- 04/27/16 -- Modified to add new column LOANAMTLIMEXMPTIND for 504 refi
    SS--09/30/2016 --Added new column LoanPymntSchdlFreq for OPSMDEV 1116 (repayments)
     SS---10/12/2016 Added LoanPymtRepayInstlTypCd column OPSMDEV1116
     SS--12/8/2016 Added LOANGNTYMATSTIND OPSMDEV 1288
     SS---12/12/2016 Added LoanGntyNoteDt to identifier OPSMDEV1288
     RY--07/19/2017--CAFSOPER-805-- Added LoanAppCDCGrossDbentrAmt,LoanAppBalToBorrAmt to ID-0
     NK--01/16/2018--OPSMDEV 1644--Added LoanAppRevlMoIntQty, LoanAppAmortMoIntQty  to ID 0
     SS--10/30/2018--OPSMDEV 1965  --Added  LoanAppExtraServFeeAMT, LoanAppExtraServFeeInd to ID 0
     SS--02/21/2020 OPSMDEV 2406 Added LoanAppNetEarnInd
     SL--04/01/2020 CAFSCARES35 Added p_LoanAppMonthPayroll
     SL--01/08/2021--SODSTORY-360. Added p_LoanAppUSResInd
     JP --1/14/2021 SODSTORY-390 Changed P_UNDRWRITNGBY to SBA for PPP/PPS
     SL--03/03/2021 SODSTORY-571 Added new columns: LOANAPPSCHEDCIND, LOANAPPSCHEDCYR, LOANAPPGROSSINCOMEAMT 

    *********************************************************************************
    */
    v_SpcPurpsLoanCd   CHAR (4);
    LEN                NUMBER (10, 0);
    cnt                NUMBER (10, 0);
BEGIN
    /* Update Application Screen into the Loan Incomplete Application Table */
    IF p_Identifier = 0 THEN
        BEGIN
            UPDATE LoanAppTbl
            SET LoanAppNmb = p_LoanAppNmb,
                LoanAppNm = p_LoanAppNm,
                LoanAppRqstAmt = p_LoanAppRqstAmt,
                LoanAppSBAGntyPct = p_LoanAppSBAGntyPct,
                LoanAppSBARcmndAmt = p_LoanAppSBARcmndAmt,
                LoanAppRqstMatMoQty = p_LoanAppRqstMatMoQty,
                LoanAppEPCInd = p_LoanAppEPCInd,
                LoanAppPymtAmt = p_LoanAppPymtAmt,
                LoanAppFullAmortPymtInd = p_LoanAppFullAmortPymtInd,
                LoanAppMoIntQty = p_LoanAppMoIntQty,
                LoanAppLifInsurRqmtInd = p_LoanAppLifInsurRqmtInd,
                LoanAppRcnsdrtnInd = p_LoanAppRcnsdrtnInd,
                LoanAppInjctnInd = p_LoanAppInjctnInd,
                LoanAppEWCPSnglTransPostInd = p_LoanAppEWCPSnglTransPostInd,
                LoanAppEWCPSnglTransInd = p_LoanAppEWCPSnglTransInd,
                LoanAppDisasterCntrlNmb = p_LoanAppDisasterCntrlNmb,
                LoanCollatInd = p_LoanCollatInd,
                RecovInd = p_RecovInd,
                LoanAppOutPrgrmAreaOfOperInd = p_LoanAppOutPrgrmAreaOfOperInd,
                LoanAppParntLoanNmb = p_LoanAppParntLoanNmb,
                LoanAppCDCGntyAmt = p_LoanAppCDCGntyAmt,
                LoanAppCDCNetDbentrAmt = p_LoanAppCDCNetDbentrAmt,
                LoanAppCDCFundFeeAmt = p_LoanAppCDCFundFeeAmt,
                LoanAppCDCSeprtPrcsFeeInd = p_LoanAppCDCSeprtPrcsFeeInd,
                LoanAppCDCPrcsFeeAmt = p_LoanAppCDCPrcsFeeAmt,
                LoanAppCDCClsCostAmt = p_LoanAppCDCClsCostAmt,
                LoanAppCDCOthClsCostAmt = p_LoanAppCDCOthClsCostAmt,
                LoanAppCDCUndrwtrFeeAmt = p_LoanAppCDCUndrwtrFeeAmt,
                LoanAppContribPct = p_LoanAppContribPct,
                LoanAppContribAmt = p_LoanAppContribAmt,
                LOANDISASTRAPPFEECHARGED = p_LOANDISASTRAPPFEECHARGED,
                LOANDISASTRAPPDCSN = p_LOANDISASTRAPPDCSN,
                LOANASSOCDISASTRAPPNMB = p_LOANASSOCDISASTRAPPNMB,
                LOANASSOCDISASTRLOANNMB = p_LOANASSOCDISASTRLOANNMB,
                LoanAppPymtDefMoNmb = p_LoanAppPymtDefMoNmb,
                LoanAppCreatUserId = p_LoanAppCreatUserId,
                LoanAppCreatDt = SYSDATE,
                LASTUPDTUSERID = NVL (p_LoanAppCreatUserId, USER),
                LASTUPDTDT = SYSDATE,
                LOANDISASTRAPPNMB = p_LOANDISASTRAPPNMB,
                LOANPYMNINSTLMNTFREQCD = p_LOANPYMNINSTLMNTFREQCD,
                LOANAPPINTDTLCD = p_LOANAPPINTDTLCD,
                UNDRWRITNGBY = CASE WHEN PrcsMthdCd IN ('PPP', 'PPS') THEN 'SBA' ELSE P_UNDRWRITNGBY END,
                --    p_UNDRWRITNGBY,
                LOANAMTLIMEXMPTIND = p_LOANAMTLIMEXMPTIND,
                LoanPymntSchdlFreq = p_LoanPymntSchdlFreq,
                LoanPymtRepayInstlTypCd = NVL (p_LoanPymtRepayInstlTypCd, LoanPymtRepayInstlTypCd),
                LOANGNTYMATSTIND = p_LOANGNTYMATSTIND,
                LoanGntyNoteDt = p_LoanGntyNoteDt,
                LoanAppCDCGrossDbentrAmt = p_LoanAppCDCGrossDbentrAmt,
                LoanAppBalToBorrAmt = p_LoanAppBalToBorrAmt,
                LoanAppRevlMoIntQty = p_LoanAppRevlMoIntQty,
                LoanAppAmortMoIntQty = p_LoanAppAmortMoIntQty,
                LoanAppExtraServFeeAMT = p_LoanAppExtraServFeeAMT,
                LoanAppExtraServFeeInd = p_LoanAppExtraServFeeInd,
                LoanAppMonthPayroll = p_LoanAppMonthPayroll,
                LoanAppUSResInd = p_LoanAppUSResInd,
                LOANAPPSCHEDCIND = p_LOANAPPSCHEDCIND, 
                LOANAPPSCHEDCYR = p_LOANAPPSCHEDCYR, 
                LOANAPPGROSSINCOMEAMT = p_LOANAPPGROSSINCOMEAMT,
                LoanAppNetEarnInd = p_LoanAppNetEarnInd
            WHERE LoanAppNmb = p_LoanAppNmb;

            p_RetVal := SQL%ROWCOUNT;
        END;
    END IF;

    /* Update Special Purpose Table */
    BEGIN
        DELETE FROM LoanSpcPurpsTbl
        WHERE LoanAppNmb = p_LoanAppNmb;
    END;

    v_SpcPurpsLoanCd := NULL;

    IF p_SpcPurpsLoanCds IS NOT NULL THEN
        BEGIN
            SELECT LENGTH (p_SpcPurpsLoanCds) INTO LEN FROM DUAL;

            cnt := 1;

            WHILE LEN >= cnt
            LOOP
                BEGIN
                    --SELECT substr('NRAL,REVL',1,4)
                    --FROM DUAL;
                    SELECT SUBSTR (p_SpcPurpsLoanCds, cnt, 4) INTO v_SpcPurpsLoanCd FROM DUAL;

                    -- DBMS_OUTPUT.put_line(':'||v_SpcPurpsLoanCd);
                    INSERT INTO LoanSpcPurpsTbl (LoanAppNmb,
                                                 LoanSpcPurpsSeqNmb,
                                                 SpcPurpsLoanCd,
                                                 CreatUserId,
                                                 CreatDt,
                                                 LASTUPDTUSERID,
                                                 LASTUPDTDT)
                        SELECT p_LoanAppNmb,
                               (  NVL (
                                      (SELECT MAX (LoanSpcPurpsSeqNmb)
                                       FROM LoanSpcPurpsTbl z
                                       WHERE z.LoanAppNmb = p_LoanAppNmb),
                                      0)
                                + 1)
                                   AS LoanSpcPurpsSeqNmb,
                               v_SpcPurpsLoanCd,
                               p_LoanAppCreatUserId,
                               SYSDATE,
                               NVL (p_LoanAppCreatUserId, USER),
                               SYSDATE
                        FROM DUAL;

                    --LoanSpcPurpsTbl
                    --WHERE LoanAppNmb =p_LoanAppNmb;
                    p_RetVal := SQL%ROWCOUNT;
                    p_RetVal := p_RetVal + 1;
                    cnt := cnt + 5;
                END;
            END LOOP;
        END;
    END IF;

    p_RetVal := NVL (p_RetVal, 0);
END;
/

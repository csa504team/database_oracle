CREATE OR REPLACE PROCEDURE LOANAPP.LOANAPPSELCSP (
   p_Identifier                  IN     NUMBER DEFAULT 0,
   p_LoanAppNmb                  IN     NUMBER DEFAULT 0,
   p_RetVal                         OUT NUMBER,
   p_LoanAppCDCClsCostAmt           OUT NUMBER,
   p_LoanAppCDCOthClsCostAmt        OUT NUMBER,
   p_LoanAppCDCGntyAmt              OUT NUMBER,
   p_LoanAppCDCJobRat               OUT NUMBER,
   p_LoanAppCDCFundFeeAmt           OUT NUMBER,
   p_LoanAppCDCNetDbentrAmt         OUT NUMBER,
   p_LoanAppCDCSeprtPrcsFeeInd      OUT VARCHAR2,
   p_LoanAppCDCPrcsFeeAmt           OUT NUMBER,
   p_LoanAppCDCUndrwtrFeeAmt        OUT NUMBER,
   p_LoanAppContribPct              OUT NUMBER,
   p_LoanAppContribAmt              OUT NUMBER,
   p_LoanAppCurrEmpQty              OUT NUMBER,
   p_LoanAppFrnchsInd               OUT VARCHAR2,
   p_LoanAppFrnchsCd                OUT VARCHAR2,
   p_LoanAppFrnchsNm                OUT VARCHAR2,
   p_LoanAppJobCreatQty             OUT NUMBER,
   p_LoanAppJobRtnd                 OUT NUMBER,
   p_LoanAppJobRqmtMetInd           OUT VARCHAR2,
   p_LoanAppNetExprtAmt             OUT NUMBER,
   p_LoanAppNewBusCd                OUT VARCHAR2,
   p_LoanAppProjCtyNm               OUT VARCHAR2,
   p_LoanAppProjCntyCd              OUT VARCHAR2,
   p_LoanAppProjStr1Nm              OUT VARCHAR2,
   p_LoanAppProjStr2Nm              OUT VARCHAR2,
   p_LoanAppProjStCd                OUT VARCHAR2,
   p_LoanAppProjZipCd               OUT VARCHAR2,
   p_LoanAppProjZip4Cd              OUT VARCHAR2,
   p_LoanAppRqstAmt                 OUT NUMBER,
   p_LoanAppRqstMatMoQty            OUT NUMBER,
   p_LoanAppRuralUrbanInd           OUT VARCHAR2,
   p_LocId                          OUT NUMBER,
   p_NAICSYrNmb                     OUT NUMBER,
   p_NAICSCd                        OUT NUMBER,
   p_PrcsMthdCd                     OUT VARCHAR2,
   p_PrgmCd                         OUT VARCHAR2,
   p_PrtId                          OUT NUMBER,
   p_StatCd                         OUT VARCHAR2,
   p_SumPrtAmt                      OUT NUMBER,
   p_SumLoanInjctnAmt               OUT NUMBER,
   p_LOANAPPDISASTERCNTRLNMB        OUT VARCHAR2,
   p_LOANDISASTRAPPFEECHARGED       OUT NUMBER,
   p_LOANDISASTRAPPDCSN             OUT VARCHAR2,
   p_LOANASSOCDISASTRAPPNMB         OUT VARCHAR2,
   p_LOANASSOCDISASTRLOANNMB        OUT VARCHAR2,
   p_LOANDISASTRAPPNMB              OUT VARCHAR2,
   p_LoanAppCDCGrossDbentrAmt       OUT NUMBER,
   p_LoanAppBalToBorrAmt            OUT NUMBER,
   p_LOANBUSESTDT                   OUT DATE,
   p_LoanAppRevlMoIntQty                OUT NUMBER ,
   p_LoanAppAmortMoIntQty               OUT NUMBER ,
   p_LOANAPPRURALINITIATVIND out VARCHAR2,
   p_PrtyGrpInd                     OUT VARCHAR2 )
AS
/* Date         :
   Programmer   :
   Remarks      :
    Parameters   :
***********************************************************************************
APK -- 02/06/2012 -- modified to add new column LOANDISASTRAPPNMB, as its added to the loanapptbl in development.
SP  -- 04/07/2014 -- Moved NAICSYRNMB,NAICSCD,LOANAPPFRNCHSCD,LOANAPPFRNCHSNM,LoanAppFrnchsInd from LoanAppTbl to LoanAppProjTbl
NK--03/28/2017-- OPSMDEV 1385-- Removed EconDevObjctCd
RY--07/19/2017--CAFSOPER-805-- Added LoanAppCDCGrossDbentrAmt,LoanAppBalToBorrAmt to ID-0
SS--11/17/2017--OPSMDEV 1566-- Added LOANBUSESTDT
NK--01/16/2018--OPSMDEV 1644--Added LoanAppRevlMoIntQty, LoanAppAmortMoIntQty  to ID 0
RY--08/07/2018--OPSMDEV-1876:: Added LoanAppRuralInitiatvInd 
BO--06/28/2021--OPSMDEV-2768 -- Added PrtyGrpInd column to the query
*********************************************************************************
*/
BEGIN
   SAVEPOINT LoanAppSel;

   SELECT c.LocId,
          c.PrtId,
          a.PrgmCd,
          SUBSTR (a.PrcsMthdCd, 1, 3),
          d.NAICSYrNmb,
          d.NAICSCd,
          a.LoanAppNewBusCd,
          d.LoanAppNetExprtAmt,
          d.LoanAppCurrEmpQty,
          d.LoanAppJobCreatQty,
          d.LoanAppJobRtnd,
          d.LoanAppJobRqmtMetInd,
          d.LoanAppCDCJobRat,
          d.LoanAppFrnchsInd,
          d.LoanAppFrnchsCd,
          d.LoanAppFrnchsNm,
          d.LoanAppRuralUrbanInd,
          a.LoanAppContribPct,
          a.LoanAppContribAmt,
          a.LoanAppRqstAmt,
          a.LoanAppRqstMatMoQty,
          a.LoanAppCDCGntyAmt,
          a.LoanAppCDCNetDbentrAmt,
          a.LoanAppCDCFundFeeAmt,
          a.LoanAppCDCSeprtPrcsFeeInd,
          a.LoanAppCDCPrcsFeeAmt,
          a.LoanAppCDCClsCostAmt,
          a.LoanAppCDCothClsCostAmt,
          a.LoanAppCDCUndrwtrFeeAmt,
          d.LoanAppProjStr1Nm,
          d.LoanAppProjStr2Nm,
          d.LoanAppProjCtyNm,
          d.LoanAppProjCntyCd,
          d.LoanAppProjStCd,
          d.LoanAppProjZipCd,
          d.LoanAppProjZip4Cd,
          a.StatCd,
          
          NVL ( (SELECT SUM (p.LoanPartLendrAmt)
                   FROM LoanPartLendrTbl p
                  WHERE LoanAppNmb = p_LoanAppNmb),
               0),
          NVL ( (SELECT SUM (i.LoanInjctnAmt)
                   FROM LoanInjctnTbl i
                  WHERE LoanAppNmb = p_LoanAppNmb),
               0),
          LOANAPPDISASTERCNTRLNMB,
          LOANDISASTRAPPFEECHARGED,
          LOANDISASTRAPPDCSN,
          LOANASSOCDISASTRAPPNMB,
          LOANASSOCDISASTRLOANNMB,
          LOANDISASTRAPPNMB,
          LoanAppCDCGrossDbentrAmt,
          LoanAppBalToBorrAmt,
          d.LOANBUSESTDT,
          a.LoanAppRevlMoIntQty, 
          a.LoanAppAmortMoIntQty,
          d.LOANAPPRURALINITIATVIND,
          a.PrtyGrpInd 
     INTO p_LocId,
          p_PrtId,
          p_PrgmCd,
          p_PrcsMthdCd,
          p_NAICSYrNmb,
          p_NAICSCd,
          p_LoanAppNewBusCd,
          p_LoanAppNetExprtAmt,
          p_LoanAppCurrEmpQty,
          p_LoanAppJobCreatQty,
          p_LoanAppJobRtnd,
          p_LoanAppJobRqmtMetInd,
          p_LoanAppCDCJobRat,
          p_LoanAppFrnchsInd,
          p_LoanAppFrnchsCd,
          p_LoanAppFrnchsNm,
          p_LoanAppRuralUrbanInd,
          p_LoanAppContribPct,
          p_LoanAppContribAmt,
          p_LoanAppRqstAmt,
          p_LoanAppRqstMatMoQty,
          p_LoanAppCDCGntyAmt,
          p_LoanAppCDCNetDbentrAmt,
          p_LoanAppCDCFundFeeAmt,
          p_LoanAppCDCSeprtPrcsFeeInd,
          p_LoanAppCDCPrcsFeeAmt,
          p_LoanAppCDCClsCostAmt,
          p_LoanAppCDCothClsCostAmt,
          p_LoanAppCDCUndrwtrFeeAmt,
          p_LoanAppProjStr1Nm,
          p_LoanAppProjStr2Nm,
          p_LoanAppProjCtyNm,
          p_LoanAppProjCntyCd,
          p_LoanAppProjStCd,
          p_LoanAppProjZipCd,
          p_LoanAppProjZip4Cd,
          p_StatCd,         
          p_SumPrtAmt,
          p_SumLoanInjctnAmt,
          p_LOANAPPDISASTERCNTRLNMB,
          p_LOANDISASTRAPPFEECHARGED,
          p_LOANDISASTRAPPDCSN,
          p_LOANASSOCDISASTRAPPNMB,
          p_LOANASSOCDISASTRLOANNMB,
          p_LOANDISASTRAPPNMB,
          p_LoanAppCDCGrossDbentrAmt,
          p_LoanAppBalToBorrAmt,
          p_LOANBUSESTDT,
          p_LoanAppRevlMoIntQty, 
          p_LoanAppAmortMoIntQty,
          p_LOANAPPRURALINITIATVIND,
          p_PrtyGrpInd 
     FROM LoanAppTbl a, LoanAppPrtTbl c, LoanAppProjTbl d
    WHERE     a.LoanAppNmb = p_LoanAppNmb
          AND a.LoanAppNmb = c.LoanAppNmb(+)
          AND a.LoanAppNmb = d.LoanAppNmb(+);

   p_RetVal := SQL%ROWCOUNT;
   p_RetVal := NVL (p_RetVal, 0);
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      NULL; 
   WHEN OTHERS
   THEN
      BEGIN
         ROLLBACK TO LoanAppSel;
         RAISE;
      END;
END LOANAPPSELCSP;
/

CREATE OR REPLACE PROCEDURE LOANAPP.CALCDEBENTURECSP(
p_LoanAppFundDt IN DATE := SYSDATE,
p_LoanAppFirstDisbDt IN DATE := SYSDATE,
p_LoanAppRqstMatMoQty IN NUMBER := 0,
p_LoanAppCDCNetDbentrAmt IN NUMBER := 0,
p_LoanAppCDCClsCostAmt IN NUMBER := 0,
p_LOANAPPCDCOTHCLSCOSTAMT IN NUMBER :=0,
p_LoanAppCDCSeprtPrcsFeeInd IN CHAR := NULL,
p_LoanAppCDCPrcsFeeAmt IN OUT NUMBER,
p_LoanAppCDCGntyAmt OUT NUMBER,
p_LoanAppCDCFundFeeAmt OUT NUMBER,
p_LoanAppCDCUndrwtrFeeAmt OUT NUMBER,
p_LoanAppCDCGrossDbentrAmt OUT NUMBER,
p_LoanAppCDCSubTotAmt OUT NUMBER,
p_LoanAppCDCAdminCostAmt OUT NUMBER,
p_LoanAppBalToBorrAmt OUT NUMBER)
AS
v_CDCGntyFeeMulti NUMBER;
v_CDCFundFeeMulti NUMBER;
v_CDCPrcsFeeMulti NUMBER;
v_CDCGrossDbentrMulti NUMBER;
v_CDCUndrwtrFeeMulti NUMBER;
/* 
This procedure calculates the Gross Debenture Amount
If the user does not enter the processing fee, then it is calculated based on net debentrue amount.
Note the procedure will not reject the processing fee entered by user which is more than 1.5% * net debenture amount. But the processing fee should be rejected by validation procedures.
*/

BEGIN
      SELECT TO_NUMBER(IMPrmtrValTxt)
      INTO v_CDCGntyFeeMulti
      FROM loanapp.IMPrmtrTbl a, loanapp.IMPrmtrValTbl b
      WHERE     a.IMPrmtrSeqNmb = b.IMPrmtrSeqNmb
            AND a.IMPrmtrId = 'CDCGntyFeeMulti'
            AND IMPrmtrValStrtDt <= p_LoanAppFundDt
            AND ( IMPrmtrValEndDt IS NULL OR p_LoanAppFundDt < IMPrmtrValEndDt + 1);

      SELECT TO_NUMBER (IMPrmtrValTxt)
      INTO v_CDCFundFeeMulti
      FROM loanapp.IMPrmtrTbl a, loanapp.IMPrmtrValTbl b
      WHERE     a.IMPrmtrSeqNmb = b.IMPrmtrSeqNmb
            AND a.IMPrmtrId = 'CDCFundFeeMulti'
            AND IMPrmtrValStrtDt <= p_LoanAppFundDt
            AND ( IMPrmtrValEndDt IS NULL OR p_LoanAppFundDt < IMPrmtrValEndDt + 1);

      SELECT TO_NUMBER (IMPrmtrValTxt)
      INTO v_CDCPrcsFeeMulti
      FROM loanapp.IMPrmtrTbl a, loanapp.IMPrmtrValTbl b
      WHERE     a.IMPrmtrSeqNmb = b.IMPrmtrSeqNmb
            AND a.IMPrmtrId = 'CDCPrcsFeeMulti'
            AND IMPrmtrValStrtDt <= p_LoanAppFundDt
            AND ( IMPrmtrValEndDt IS NULL OR p_LoanAppFundDt < IMPrmtrValEndDt + 1);
	
    p_LoanAppCDCGntyAmt := ROUND(p_LoanAppCDCNetDbentrAmt * v_CDCGntyFeeMulti, 2);
    p_LoanAppCDCFundFeeAmt := ROUND(p_LoanAppCDCNetDbentrAmt * v_CDCFundFeeMulti, 2);
    
    IF p_LoanAppCDCPrcsFeeAmt IS NULL THEN
    	p_LoanAppCDCPrcsFeeAmt := ROUND(p_LoanAppCDCNetDbentrAmt * v_CDCPrcsFeeMulti, 2);
    END IF;
    
    IF p_LoanAppRqstMatMoQty = 300 THEN
		SELECT TO_NUMBER (IMPrmtrValTxt)
		INTO v_CDCGrossDbentrMulti
		FROM loanapp.IMPrmtrTbl a, loanapp.IMPrmtrValTbl b
		WHERE     a.IMPrmtrSeqNmb = b.IMPrmtrSeqNmb
			AND a.IMPrmtrId = 'CDCUndrwtrFee25YrDbentrDiv'
			AND IMPrmtrValStrtDt <= p_LoanAppFirstDisbDt
			AND ( IMPrmtrValEndDt IS NULL OR p_LoanAppFirstDisbDt < IMPrmtrValEndDt + 1);
		
		SELECT TO_NUMBER (IMPrmtrValTxt)
		INTO v_CDCUndrwtrFeeMulti
		FROM loanapp.IMPrmtrTbl a, loanapp.IMPrmtrValTbl b
		WHERE     a.IMPrmtrSeqNmb = b.IMPrmtrSeqNmb
			AND a.IMPrmtrId = 'UndrwtrFee25YrDbentrPct'
			AND IMPrmtrValStrtDt <= p_LoanAppFundDt
			AND ( IMPrmtrValEndDt IS NULL OR p_LoanAppFundDt < IMPrmtrValEndDt + 1);
    ELSIF p_LoanAppRqstMatMoQty = 240 THEN
		SELECT TO_NUMBER (IMPrmtrValTxt)
		INTO v_CDCGrossDbentrMulti
		FROM loanapp.IMPrmtrTbl a, loanapp.IMPrmtrValTbl b
		WHERE     a.IMPrmtrSeqNmb = b.IMPrmtrSeqNmb
			AND a.IMPrmtrId = 'CDCUndrwtrFee20YrDbentrDiv'
			AND IMPrmtrValStrtDt <= p_LoanAppFirstDisbDt
			AND ( IMPrmtrValEndDt IS NULL OR p_LoanAppFirstDisbDt < IMPrmtrValEndDt + 1);
		
		SELECT TO_NUMBER (IMPrmtrValTxt)
		INTO v_CDCUndrwtrFeeMulti
		FROM loanapp.IMPrmtrTbl a, loanapp.IMPrmtrValTbl b
		WHERE     a.IMPrmtrSeqNmb = b.IMPrmtrSeqNmb
			AND a.IMPrmtrId = 'UndrwtrFee20YrDbentrPct'
			AND IMPrmtrValStrtDt <= p_LoanAppFundDt
			AND ( IMPrmtrValEndDt IS NULL OR p_LoanAppFundDt < IMPrmtrValEndDt + 1);
    ELSE
		SELECT TO_NUMBER (IMPrmtrValTxt)
		INTO v_CDCGrossDbentrMulti
		FROM loanapp.IMPrmtrTbl a, loanapp.IMPrmtrValTbl b
		WHERE     a.IMPrmtrSeqNmb = b.IMPrmtrSeqNmb
			AND a.IMPrmtrId = 'CDCUndrwtrFee10YrDbentrDiv'
			AND IMPrmtrValStrtDt <= p_LoanAppFirstDisbDt
			AND ( IMPrmtrValEndDt IS NULL OR p_LoanAppFirstDisbDt < IMPrmtrValEndDt + 1);
		
		SELECT TO_NUMBER (IMPrmtrValTxt)
		INTO v_CDCUndrwtrFeeMulti
		FROM loanapp.IMPrmtrTbl a, loanapp.IMPrmtrValTbl b
		WHERE     a.IMPrmtrSeqNmb = b.IMPrmtrSeqNmb
			AND a.IMPrmtrId = 'UndrwtrFee10YrDbentrPct'
			AND IMPrmtrValStrtDt <= p_LoanAppFundDt
			AND ( IMPrmtrValEndDt IS NULL OR p_LoanAppFundDt < IMPrmtrValEndDt + 1);
    END IF;
    
    p_LoanAppCDCGrossDbentrAmt := CEIL((p_LoanAppCDCNetDbentrAmt + p_LoanAppCDCGntyAmt + p_LoanAppCDCFundFeeAmt + p_LoanAppCDCPrcsFeeAmt + p_LoanAppCDCClsCostAmt + p_LOANAPPCDCOTHCLSCOSTAMT) / v_CDCGrossDbentrMulti / 1000) * 1000;
    p_LoanAppCDCUndrwtrFeeAmt := ROUND(p_LoanAppCDCGrossDbentrAmt * v_CDCUndrwtrFeeMulti, 2);
    p_LoanAppCDCSubTotAmt := p_LoanAppCDCGntyAmt + p_LoanAppCDCFundFeeAmt + /*CASE WHEN p_LoanAppCDCSeprtPrcsFeeInd = 'Y' THEN 0 ELSE p_LoanAppCDCPrcsFeeAmt END*/ p_LoanAppCDCPrcsFeeAmt + p_LoanAppCDCClsCostAmt + p_LOANAPPCDCOTHCLSCOSTAMT;
    p_LoanAppCDCAdminCostAmt := p_LoanAppCDCSubTotAmt + p_LoanAppCDCUndrwtrFeeAmt;
    p_LoanAppBalToBorrAmt := p_LoanAppCDCGrossDbentrAmt - p_LoanAppCDCNetDbentrAmt - p_LoanAppCDCAdminCostAmt;
    
EXCEPTION
  WHEN NO_DATA_FOUND THEN
          
    p_LoanAppCDCGrossDbentrAmt := 0;
    p_LoanAppCDCUndrwtrFeeAmt  := 0;
    p_LoanAppCDCSubTotAmt      := 0;
    p_LoanAppCDCAdminCostAmt   := 0;
    p_LoanAppBalToBorrAmt      := 0;
    
  WHEN OTHERS THEN
          
      dbms_output.put_line(sqlerrm);
      raise;
    
END;
/
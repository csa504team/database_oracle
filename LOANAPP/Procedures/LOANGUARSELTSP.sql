CREATE OR REPLACE PROCEDURE LOANAPP.LOANGUARSELTSP(
p_Identifier     IN NUMBER  DEFAULT 0,
p_LoanAppNmb     IN NUMBER  DEFAULT 0,
p_GuarSeqNmb     IN NUMBER  DEFAULT 0,
p_TaxId         IN CHAR  DEFAULT NULL,
p_GuarBusPerInd IN CHAR  DEFAULT NULL,
p_RetVal         OUT NUMBER,
p_SelCur         OUT sys_refcursor)
AS
/* Date         : Feb 11 2004
   Programmer   : Prasad Mente
   Remarks      :
   Parameters   :  No.   Name        Type     Description
                    1    @p_Identifier  int     Identifies which batch
                                              to execute.
                    2    @RetVal      int     No. of rows returned/
                                              affected
                    3    Primary key columns
*********************************************************************************
Revised By      Date        Commments
*********************************************************************************
APK            07/20/2011   Modified ID=12 to add an additional condition using BusPerInd.
SP             9/26/2012    Modified as column LoanAffilInd is
                            added FOR determining companion relationship
SP             02/04/2014   Modified to add column LACGntyCondTypCd to support LADS
*/
/* Variable Declaration */
/* End of Variable Declaration */
/*[SPCONV-ERR(26)]:('@TRANCOUNT') Global Variable treated as variable
SB -- Added Identifier 38 for Extract XML */
/*NK --03/21/2016 --TO_CHAR (ColumnName, '999999999999990.99')  to TO_CHAR (ColumnName, 'FM999999999999990.00')
NK -- 04/06/2016-- included p.PerBrthCntCd in Identifier 38 
NK --04/19/2016-- included m.GuarSeqNmb in Identifier 38*/
--NK--09/09/2016--OPSMDEV1018--Removed LoanCntlIntInd  ID 0,11,38
--BR--09/30/2016--OPSMDEV    -- Added new columns GntySubCondTypCd, GntyLmtAmt, GntyLmtPct, LoanCollatSeqNmb, GntyLmtYrNmb  
--NK--10/11/2016-- OPSMDEV1141-- added GntySubCondTypCd, GntyLmtAmt, GntyLmtPct, LoanCollatSeqNmb, GntyLmtYrNmb in ID 11 for Copy function.
--NK--12/29/2016-- Added GntySubCondTypCd, GntyLmtAmt, GntyLmtPct, LoanCollatSeqNmb, GntyLmtYrNmb in ID 38.
BEGIN--SS--11/26/2018--OPSMDEV 2043
    
            SAVEPOINT LoanGuarSel;
    
        IF  p_Identifier = 0 THEN
        BEGIN
            OPEN p_SelCur FOR
            SELECT LoanAppNmb,
                   GuarSeqNmb,
                   TaxId,
                   GuarBusPerInd,
                   LoanGuarOperCoInd,
                   LACGntyCondTypCd,
                   GntySubCondTypCd, 
                   GntyLmtAmt, 
                   GntyLmtPct, 
                   LoanCollatSeqNmb, 
                   GntyLmtYrNmb,
                   LIABINSURRQRDIND,
                   PRODLIABINSURRQRDIND,
                   LIQLIABINSURRQRDIND,
                   MALPRCTSINSURRQRDIND,
                   OTHINSURRQRDIND,
                   WORKRSCOMPINSRQRDIND,
                   OTHINSURDESCTXT
              FROM LoanGuarTbl
             WHERE LoanAppNmb = p_LoanAppNmb
               AND GuarSeqNmb = p_GuarSeqNmb;    
        END;

        ELSIF  p_Identifier = 2 THEN
            BEGIN
                OPEN p_SelCur FOR
                SELECT 1
                  FROM LoanGuarTbl
                 WHERE LoanAppNmb = p_LoanAppNmb
                   AND GuarSeqNmb = p_GuarSeqNmb; 
            END;

            ELSIF  p_Identifier = 11 THEN
                BEGIN
                    OPEN p_SelCur FOR
                    SELECT LoanAppNmb,
                           GuarSeqNmb,
                           TaxId,
                           GuarBusPerInd,
                           LoanGuarOperCoInd,
                           LACGntyCondTypCd,
                           GntySubCondTypCd, 
                           GntyLmtAmt, 
                           GntyLmtPct, 
                           LoanCollatSeqNmb, 
                           GntyLmtYrNmb  
                      FROM LoanGuarTbl
                     WHERE LoanAppNmb = p_LoanAppNmb;
                END;

                    
/*Check The Existance Of Row based on LoanAppNmb and TaxId*/
                ELSIF  p_Identifier = 12 THEN
                    BEGIN
                        OPEN p_SelCur FOR
                        SELECT GuarSeqNmb
                          FROM LoanGuarTbl
                         WHERE LoanAppNmb   = p_LoanAppNmb
                           AND TaxId         = p_TaxId
                           AND GuarBusPerInd = p_GuarBusPerInd;
                     END;
                  
                 ELSIF  p_Identifier = 14 THEN
                    BEGIN
                            OPEN p_SelCur FOR
                            SELECT GuarSeqNmb,
                                   GuarBusPerInd,
                                   a.TaxId,
                                   BusNm,
                                   NULL PerFirstNm,
                                   NULL PerLastNm,
                                   NULL PerInitialNm,
                                   NULL PerSfxNm
                              FROM LoanBusTbl b, LoanGuarTbl a
                             WHERE a.LoanAppNmb = p_LoanAppNmb
                               and b.LoanAppNmb = p_LoanAppNmb
                               and a.TaxId = b.TaxId
                               and GuarBusPerInd = 'B'
                              UNION
                            SELECT GuarSeqNmb,
                                   GuarBusPerInd,
                                   a.TaxId,
                                   NULL BusNm,
                                   ocadataout(PerFirstNm) PerFirstNm,
                                  ocadataout( PerLastNm)  PerLastNm,
                                   PerInitialNm,
                                   PerSfxNm
                              FROM LoanPerTbl b, LoanGuarTbl a
                             WHERE a.LoanAppNmb = p_LoanAppNmb
                               and b.LoanAppNmb = p_LoanAppNmb
                               and a.TaxId = b.TaxId
                               and GuarBusPerInd = 'P'
                             ORDER BY GuarSeqNmb ;
                            
                        END;
    ELSIF  p_Identifier = 38 THEN
            BEGIN
                OPEN p_SelCur FOR
        /* 38 = Leet for Extract XML, initially indented for union */
    select                                                               m.TaxId,
                                                                         m.GuarBusPerInd,
                                                                         m.GuarSeqNmb,
                                                                         m.LACGntyCondTypCd,
                                                                         m.LoanGuarOperCoInd,
                                                                         null  LoanAffilInd,--NK--10/12/2016--OPSMDEV 1018
                                                                         m.GntyLmtAmt,
                                                                         m.GntySubCondTypCd,
                                                                         m.LoanCollatSeqNmb,
                                                                         m.GntyLmtPct,
                                                                         m.GntyLmtYrNmb,
                                                                         
                -- Both Bus and Per:
                b.BusBnkrptInd                                             BnkrptcyInd,
                m.GuarBusPerInd                                            BusPerInd,
        to_char(b.BusExtrnlCrdtScorDt,            'Mon dd yyyy hh:miAM')   ExtrnlCrdtScorDt,
                b.BusExtrnlCrdtScorInd                                     ExtrnlCrdtScorInd,
                b.BusExtrnlCrdtScorNmb                                     ExtrnlCrdtScorNmb,
                                                                         b.IMCrdtScorSourcCd,
                b.BusLwsuitInd                                             LawsuitInd,
                b.BusMailAddrCtyNm                                         MailAddrCtyNm,
                b.BusMailAddrStCd                                          MailAddrStCd,
                b.BusMailAddrStNm                                          MailAddrStNm,
                b.BusMailAddrCntCd                                         MailAddrCntCd,
                b.BusMailAddrPostCd                                        MailAddrPostCd,
                b.BusMailAddrStr1Nm                                        MailAddrStr1Nm,
                b.BusMailAddrStr2Nm                                        MailAddrStr2Nm,
                b.BusMailAddrZip4Cd                                        MailAddrZip4Cd,
                b.BusMailAddrZipCd                                         MailAddrZipCd,
                b.BusPhyAddrCtyNm                                          PhyAddrCtyNm,
                b.BusPhyAddrStCd                                           PhyAddrStCd,
                b.BusPhyAddrStNm                                           PhyAddrStNm,
                b.BusPhyAddrCntCd                                          PhyAddrCntCd,
                b.BusPhyAddrPostCd                                         PhyAddrPostCd,
                b.BusPhyAddrStr1Nm                                         PhyAddrStr1Nm,
                b.BusPhyAddrStr2Nm                                         PhyAddrStr2Nm,
                b.BusPhyAddrZip4Cd                                         PhyAddrZip4Cd,
                b.BusPhyAddrZipCd                                          PhyAddrZipCd,
                b.BusPrimPhnNmb                                            PrimPhnNmb,
                -- Unique to Bus:
        to_char(b.BusChkngBalAmt,                 'FM999999999999990.00')    BusChkngBalAmt,
                                                                         b.BusCurBnkNm,
        to_char(b.BusCurOwnrshpDt,                'Mon dd yyyy hh:miAM')   BusCurOwnrshpDt,
                                                                         b.BusDUNSNmb,
                                                                         b.BusEINCertInd,
                                                                         b.BusExprtInd,
                                                                         b.BusNm,
                                                                         b.BusPriorSBALoanInd,
                                                                         b.BusTrdNm,
                                                                         b.BusTypCd,
                null                                                       LqdCrCBDInfo,
                null                                                       LqdCrBBADunBradCrRiskScr,
                -- Unique to Per:
                null                                                       PerAffilEmpFedInd,
                null                                                       PerAlienRgstrtnNmb,
                null                                                       PerBrthCntCd,
                null                                                       PerBrthCntNm,
                null                                                       PerBrthCtyNm,
                null                                                       PerBrthDt,
                null                                                       PerBrthStCd,
                null                                                       PerCnvctInd,
                null                                                       PerCrmnlOffnsInd,
                null                                                       PerFirstNm,
                null                                                       PerFngrprntWaivDt,
                null                                                       PerIndctPrleProbatnInd,
                null                                                       PerInitialNm,
                null                                                       PerIOBInd,
                null                                                       PerLastNm,
                null                                                       PerSfxNm,
                null                                                       PerTitlTxt,
                null                                                       PerUSCitznInd,
                null                                                       LIABINSURRQRDIND,
                null                                                       PRODLIABINSURRQRDIND,
                null                                                       LIQLIABINSURRQRDIND,
                null                                                       MALPRCTSINSURRQRDIND,
                null                                                       OTHINSURRQRDIND,
                null                                                       WORKRSCOMPINSRQRDIND                                                     
    from        loanapp.LoanGuarTbl               m
    inner join  loanapp.LoanBusTbl                b on (   (m.LoanAppNmb   = b.LoanAppNmb)
                                                       and (m.TaxId        = b.TaxId))
    where       m.LoanAppNmb                      = p_LoanAppNmb
    and         m.GuarBusPerInd                   = 'B'
union
    select                                                               m.TaxId,
                                                                         m.GuarBusPerInd,
                                                                         m.GuarSeqNmb,
                                                                         m.LACGntyCondTypCd,
                                                                         m.LoanGuarOperCoInd,
                                                                         null  LoanAffilInd,
                                                                         m.GntyLmtAmt,
                                                                         m.GntySubCondTypCd,
                                                                         m.LoanCollatSeqNmb,
                                                                         m.GntyLmtPct,
                                                                         m.GntyLmtYrNmb,
                                                                         
                -- Both Bus and Per:
                p.PerBnkrptcyInd                                           BnkrptcyInd,
                m.GuarBusPerInd                                            BusPerInd,
        to_char(p.PerExtrnlCrdtScorDt,            'Mon dd yyyy hh:miAM')   ExtrnlCrdtScorDt,
                p.PerExtrnlCrdtScorInd                                     ExtrnlCrdtScorInd,
                p.PerExtrnlCrdtScorNmb                                     ExtrnlCrdtScorNmb,
                                                                         p.IMCrdtScorSourcCd,
                p.PerPndngLwsuitInd                                        LawsuitInd,
                p.PerMailAddrCtyNm                                         MailAddrCtyNm,
                p.PerMailAddrStCd                                          MailAddrStCd,
                p.PerMailAddrStNm                                          MailAddrStNm,
                p.PerMailAddrCntCd                                         MailAddrCntCd,
                p.PerMailAddrPostCd                                        MailAddrPostCd,
                ocadataout(p.PerMailAddrStr1Nm )                                       MailAddrStr1Nm,
                ocadataout(p.PerMailAddrStr2Nm)                                        MailAddrStr2Nm,
                p.PerMailAddrZip4Cd                                        MailAddrZip4Cd,
                p.PerMailAddrZipCd                                         MailAddrZipCd,
                p.PerPhyAddrCtyNm                                          PhyAddrCtyNm,
                p.PerPhyAddrStCd                                           PhyAddrStCd,
                p.PerPhyAddrStNm                                           PhyAddrStNm,
                p.PerPhyAddrCntCd                                          PhyAddrCntCd,
                p.PerPhyAddrPostCd                                         PhyAddrPostCd,
               ocadataout(p.PerPhyAddrStr1Nm )                                        PhyAddrStr1Nm,
               ocadataout(p.PerPhyAddrStr2Nm)                                         PhyAddrStr2Nm,
                p.PerPhyAddrZip4Cd                                         PhyAddrZip4Cd,
                p.PerPhyAddrZipCd                                          PhyAddrZipCd,
                 ocadataout(p.PerPrimPhnNmb )                                           PrimPhnNmb,
                -- Unique to Bus:
                null                                                       BusChkngBalAmt,
                null                                                       BusCurBnkNm,
                null                                                       BusCurOwnrshpDt,
                null                                                       BusDUNSNmb,
                null                                                       BusEINCertInd,
                null                                                       BusExprtInd,
                null                                                       BusNm,
                null                                                       BusPriorSBALoanInd,
                null                                                       BusTrdNm,
                null                                                       BusTypCd,
                null                                                       LqdCrCBDInfo,
                null                                                       LqdCrBBADunBradCrRiskScr,
                -- Unique to Per:
                                                                         p.PerAffilEmpFedInd,
                                                                         p.PerAlienRgstrtnNmb,
                                                                         p.PerBrthCntCd,
                                                                         p.PerBrthCntNm,
                                                                         p.PerBrthCtyNm,
        to_char(p.PerBrthDt,                      'Mon dd yyyy hh:miAM')   PerBrthDt,
                                                                         p.PerBrthStCd,
                                                                         p.PerCnvctInd,
                                                                         p.PerCrmnlOffnsInd,
                                                                         ocadataout(p.PerFirstNm)  PerFirstNm,
        to_char(p.PerFngrprntWaivDt,              'Mon dd yyyy hh:miAM')   PerFngrprntWaivDt,
                                                                         p.PerIndctPrleProbatnInd,
                                                                         p.PerInitialNm,
                                                                         p.PerIOBInd,
                                                                         ocadataout(p.PerLastNm)  PerLastNm,
                                                                         p.PerSfxNm,
                                                                         p.PerTitlTxt,
                                                                         p.PerUSCitznInd,
        null                                                             LIABINSURRQRDIND,
        null                                                             PRODLIABINSURRQRDIND,
        null                                                             LIQLIABINSURRQRDIND,
        null                                                             MALPRCTSINSURRQRDIND,
        null                                                             OTHINSURRQRDIND,
        null                                                             WORKRSCOMPINSRQRDIND   
    from        loanapp.LoanGuarTbl               m
    inner join  loanapp.LoanPerTbl                p on (   (m.LoanAppNmb = p.LoanAppNmb)
                                                       and (m.TaxId      = p.TaxId))
    where       m.LoanAppNmb                      = p_LoanAppNmb
    and         m.GuarBusPerInd                   = 'P'
order by        1,2; -- TaxId, GuarBusPerInd

        END;
   END IF;
p_RetVal := nvl(p_RetVal,0);      
  EXCEPTION
 WHEN OTHERS THEN
                    BEGIN
                    ROLLBACK TO LoanGuarSel;
                    RAISE;
                    END;
   
END LOANGUARSELTSP;
/



GRANT EXECUTE ON LOANAPP.LOANGUARSELTSP TO CDCONLINEDEVROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARSELTSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARSELTSP TO LOANACCTREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARSELTSP TO LOANAPPDEVROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARSELTSP TO LOANAPPREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARSELTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARSELTSP TO LOANDEVROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARSELTSP TO LOANORIGBORROWER;

GRANT EXECUTE ON LOANAPP.LOANGUARSELTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARSELTSP TO LOANREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARSELTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARSELTSP TO POOLSECADMINROLE;

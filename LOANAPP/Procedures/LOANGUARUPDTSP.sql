CREATE OR REPLACE PROCEDURE LOANAPP.LOANGUARUPDTSP(
  p_Identifier      	NUMBER:=0,
  p_RetVal       	OUT NUMBER,
  p_LoanAppNmb      	NUMBER:=0,
  p_GuarSeqNmb      	NUMBER:=0,
  p_TaxId 	          CHAR:=NULL,
  p_GuarBusPerInd 	  CHAR:=NULL,
  p_LoanGuarOperCoInd CHAR:=NULL,
  p_GuarCreatUserId 	CHAR:=NULL,
  p_LastUpdtUserId    CHAR := null,
  p_LACGntyCondTypCd  NUMBER := NULL, 
  p_GntySubCondTypCd  NUMBER := NULL,
  p_GntyLmtAmt        NUMBER := NULL,
  p_GntyLmtPct        NUMBER := NULL,
  p_LoanCollatSeqNmb  NUMBER := NULL,
  p_GntyLmtYrNmb      NUMBER := NULL,
  p_LIABINSURRQRDIND               CHAR:=NULL,
  p_PRODLIABINSURRQRDIND           CHAR:=NULL,
  p_LIQLIABINSURRQRDIND            CHAR:=NULL,
  p_MALPRCTSINSURRQRDIND           CHAR:=NULL,
  p_OTHINSURRQRDIND                CHAR:=NULL,
  p_WORKRSCOMPINSRQRDIND           CHAR:=NULL,
  p_OTHINSURDESCTXT                VARCHAR2:=NULL
)
AS
/*  Date         : October 1, 2000
    Programmer   : Sheri Stine
    Remarks      :
    Parameters   :  No.   Name        Type     Description
                    1     @p_Identifier  int     Identifies which batch
                                               to execute.
                    2     @p_RetVal      int     No. of rows returned/
                                               affected
                    3    table columns to be inserted
***************************************************************************************
Priya Varigala 07/17/01   LoanGuarOperCoInd new column
SP:09/26/2012: Modified as column LoanAffilInd is added to the table for determining companion relationship 
SP:02/07/2014: Modified to add  LACGntyCondTypCd to support LADS
NK--08/05/2016--OPSMDEV1018-- Removed LoanCntlIntInd 
BR -- 09/21/2016 -- OPSMDEV??? -- added GntySubCondTypCd,GntyLmtAmt,GntyLmtPct,LoanCollatSeqNmb,GntyLmtYrNmb to 0, 11, 12, 38                    number(4);
BJR -- 01/25/2018 -- CAFSOPER-1381 -- Removed created by and created dates from the update statements so that the original creator/create date stays intact for the record.
*/
	BEGIN
					SAVEPOINT LoanGuarUpd;
/* Insert into LoanGuar Table */
		IF  p_Identifier = 0 THEN
		BEGIN
		       UPDATE LoanGuarTbl
		        	SET LoanAppNmb        = p_LoanAppNmb,
			            GuarSeqNmb        = p_GuarSeqNmb,
                  TaxId             = p_TaxId,
			          	GuarBusPerInd     = p_GuarBusPerInd,
			           	LoanGuarOperCoInd = p_LoanGuarOperCoInd,
                  LastUpdtUserId    = nvl(p_LastUpdtUserId,user),
                  LastUpdtDt        = sysdate,             
                  LACGntyCondTypCd  = p_LACGntyCondTypCd, 
                  GntySubCondTypCd  = p_GntySubCondTypCd,
                  GntyLmtAmt        = p_GntyLmtAmt,
                  GntyLmtPct        = p_GntyLmtPct,
                  LoanCollatSeqNmb  = p_LoanCollatSeqNmb,
                  GntyLmtYrNmb      = p_GntyLmtYrNmb,
                  LIABINSURRQRDIND = p_LIABINSURRQRDIND,
                  PRODLIABINSURRQRDIND = p_PRODLIABINSURRQRDIND,
                  LIQLIABINSURRQRDIND = p_LIQLIABINSURRQRDIND,
                  MALPRCTSINSURRQRDIND = p_MALPRCTSINSURRQRDIND,
                  OTHINSURRQRDIND = p_OTHINSURRQRDIND,
                  WORKRSCOMPINSRQRDIND = p_WORKRSCOMPINSRQRDIND,
                  OTHINSURDESCTXT = p_OTHINSURDESCTXT
			    	WHERE LoanAppNmb = p_LoanAppNmb
			      	AND GuarSeqNmb = p_GuarSeqNmb;
p_RetVal := SQL%ROWCOUNT;
     END;  
     END IF;
p_RetVal := nvl(p_RetVal,0);    
EXCEPTION
    WHEN OTHERS THEN
        BEGIN
          ROLLBACK TO LOANGUARUPD;
          RAISE;
        END;
END LOANGUARUPDTSP;
/


GRANT EXECUTE ON LOANAPP.LOANGUARUPDTSP TO CDCONLINEDEVROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARUPDTSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARUPDTSP TO LOANACCTREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARUPDTSP TO LOANAPPDEVROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARUPDTSP TO LOANAPPREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARUPDTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARUPDTSP TO LOANDEVROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARUPDTSP TO LOANORIGBORROWER;

GRANT EXECUTE ON LOANAPP.LOANGUARUPDTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARUPDTSP TO LOANREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARUPDTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOANAPP.LOANGUARUPDTSP TO POOLSECADMINROLE;

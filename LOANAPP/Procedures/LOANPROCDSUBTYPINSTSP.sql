CREATE OR REPLACE PROCEDURE LOANAPP.LOANPROCDSUBTYPINSTSP (
    p_Identifier                NUMBER := 0,
    p_RetVal                OUT NUMBER,
    p_LOANPROCDSUBTYPID       OUT  NUMBER,
    p_LoanAppNmb                NUMBER := 0,
    p_LoanProcdTypcd            CHAR,
    p_LOANPROCDTEXTBLOCK        CHAR,
    p_LOANPROCDAMT              NUMBER := 0,
    p_LoanProcdAddr		CHAR,
    p_LoanProcdDesc		CHAR,
    p_ProcdSubTyLkupId		NUMBER := 0,
    p_Lender                VARCHAR2 := NULL  )
AS
    cursor textblck_cur is select TEXTBLOCK from SBAREF.PROCTXTBLCK where LOANPROCDTYPCD=p_LoanProcdTypcd; --template
    cursor desc_cur is select PROCDSUBTYPDESCTXT from SBAREF.LOANPROCDSUBTYPLKUPTBL where PROCDSUBTYLKUPID = p_ProcdSubTyLkupId; --insert into template
 
    v_LOANPROCDTEXTBLOCK varchar2(250) := null;
    v_LoanProcdDesc varchar2(250) := null;
    v1_LOANPROCDTEXTBLOCK varchar2(250) := null;
    v_PROCDSUBTYPDESCTXT varchar2(250) := null;
    v_LoanProcdTypcd varchar2(5) := null;
    
BEGIN
    SAVEPOINT LOANPROCDSUBTYPINSTSP;

    /* Insert into LOANPROCDSUBTYPTBL Table */
    IF p_Identifier = 0
    THEN
        v_LOANPROCDTEXTBLOCK := p_LOANPROCDTEXTBLOCK;
        v_LoanProcdDesc := p_LoanProcdDesc;
        v_LoanProcdTypcd := p_LoanProcdTypcd;
        IF v_LOANPROCDTEXTBLOCK is null then
            open textblck_cur;
            fetch textblck_cur into v1_LOANPROCDTEXTBLOCK;
            v_LOANPROCDTEXTBLOCK := v1_LOANPROCDTEXTBLOCK;
            open desc_cur;
            fetch desc_cur into v_PROCDSUBTYPDESCTXT;
        END IF;
        
        IF v_LoanProcdTypcd <> 'E04' then
        v_LOANPROCDTEXTBLOCK := replace(replace(replace(replace(v_LOANPROCDTEXTBLOCK,'[ProcdSubTypDesc]',v_PROCDSUBTYPDESCTXT),'[Description]',nvl(trim(v_LoanProcdDesc),'[Description]')),'[Address]',nvl(trim(p_LoanProcdAddr),'[Address]')),'[Lender]',nvl(trim(p_Lender),'[Lender]'));
        ELSE
        v_LOANPROCDTEXTBLOCK := replace(replace(replace(replace(v_LOANPROCDTEXTBLOCK,'[ProcdSubTypDesc2]',  SUBSTR(v_PROCDSUBTYPDESCTXT, 0, INSTR(v_PROCDSUBTYPDESCTXT, ',')-1)),'[ProcdSubTypDesc1]',SUBSTR(v_PROCDSUBTYPDESCTXT,INSTR(v_PROCDSUBTYPDESCTXT, ',')+1)),'[Description]',v_LoanProcdDesc),'[Address]',p_LoanProcdAddr);
        END IF;
            
        BEGIN
            select LOANAPP.LOANPROCDSUBTYPIDSEQ.nextval into p_LOANPROCDSUBTYPID from dual;
            INSERT INTO LOANPROCDSUBTYPTBL (LOANPROCDSUBTYPID,
                                            LOANAPPNMB,
                                            LOANPROCDTYPCD,
                                            LOANPROCDTEXTBLOCK,
                                            LOANPROCDAMT,
					    LoanProcdAddr,	
					    LoanProcdDesc,	
					    ProcdSubTyLkupId,
					    Lender)
                SELECT 	p_LOANPROCDSUBTYPID,
                       	p_LoanAppNmb,
                       	p_LoanProcdTypcd,
                       	v_LOANPROCDTEXTBLOCK,
                       	p_LOANPROCDAMT,
			p_LoanProcdAddr,
			v_LoanProcdDesc,
			p_ProcdSubTyLkupId,
			p_Lender
                  FROM DUAL;
            LOANPROCDSUBTYPSUMTSP(p_LoanAppNmb,p_LoanProcdTypcd);

            p_RetVal := SQL%ROWCOUNT;
        END;
    END IF;

    p_RetVal := NVL (p_RetVal, 0);
--END;
EXCEPTION
    WHEN OTHERS
    THEN
        BEGIN
            ROLLBACK TO LOANPROCDSUBTYPINSTSP;
            RAISE;
        END;
END LOANPROCDSUBTYPINSTSP;
/



GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPINSTSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPINSTSP TO LOANACCTREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPINSTSP TO LOANAPPREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPINSTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPINSTSP TO LOANORIGBORROWER;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPINSTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPINSTSP TO LOANREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPINSTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPINSTSP TO POOLSECADMINROLE;

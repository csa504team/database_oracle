CREATE OR REPLACE PROCEDURE LOANAPP.LOANAPPSELTSP (p_identifier    IN     NUMBER DEFAULT 0,
                                                   p_loanappnmb    IN     NUMBER DEFAULT 0,
                                                   p_imfundofccd   IN     CHAR DEFAULT NULL,
                                                   p_prtseqnmb     IN     NUMBER DEFAULT NULL,
                                                   p_datefrom      IN     DATE DEFAULT NULL,
                                                   p_dateto        IN     DATE DEFAULT NULL,
                                                   p_RqstSEQNMB    IN     NUMBER DEFAULT NULL,
                                                   p_LocId         IN     NUMBER DEFAULT NULL,
                                                   p_retval           OUT NUMBER,
                                                   p_selcur           OUT SYS_REFCURSOR)
AS
    /* Date         : Jul 10 2000
       Programmer   : Gulam Samdani
       Remarks      :
        Parameters   :  No.   Name        Type     Description
                        1     @p_Identifier  int     Identifies which batch
                                                   to execute.
                        2     @RetVal      int     No. of rows returned/
                                                   affected
                        3    Primary key columns
    ***********************************************************************************
    Revised By      Date        Comments
    ----------     --------    ----------------------------------------------------------
    Sheri Stine     10/01/00    Removed columns that were deleted from table.
    *********************************************************************************
    Sheri Stine     11/04/00    Removed columns LoanAppNatrTxt,LoanAppBusEstbDt
                                LoanAppBusOwnrshpEstbDt
    *********************************************************************************
    Sheri Stine     01/17/01    Renamed RaceCd, SICCd, SICSeceptionSeqNmb, IMRtTypCd
                                BusTypCd, LoanAppPurpsCd and LoanAppAdjPrd
                                Removed LoanAppBaseRtSourc
    *********************************************************************************
    Sheri Stine     01/18/01    Removed LoanAppExptrInd, LoanAppStbyAgrmtInd,
                                LoanAppstbyAgrmtAmt, LoanAppCntctAppvDt
    *********************************************************************************
    APK -- 02/06/2012 -- modified to add new column LOANDISASTRAPPNMB, as its added to the loanapptbl in development.
    SP -- 02/28/2013 -- modified to add new columns LOANNMB,LOANAPPFUNDDT,UNDRWRITNGBY,
    COHORTCD,FUNDSFISCLYR,LOANTYPIND in id=0,16
    RSURAPA - 10/23/2013 -- Modified the procedure to add new column LOANAPPINTDTLCD
    RSURAP - 10/29/2013 -- Modified the procedure to add new column LoanAppPymtAdjPrdCd
    RGG --   11/07/2013 -- Modified to rename the column LoanAppPymtAdjPrdCd to LOANPYMNINSTLMNTFREQCD
    SP  --   11/26/2013 -- Removed refernces to  columns :IMRtTypCd,LoanAppFixVarInd,
    LoanAppBaseRt,LoanAppInitlIntPct,LoanAppInitlSprdOvrPrimePct,LoanAppOthAdjPrdTxt,LoanAppAdjPrdCd in id= 0
    SP  --   04/07/2014 -- Modified to remove NAICSYRNMB,NAICSCD,LOANAPPFRNCHSCD,LOANAPPFRNCHSNM,LoanAppFrnchsInd as these are moved to loanappprojtbl
    SB -- Added Identifier 38 for Extract XML
    RGG -- 03/25/2015 -- Added UNDRWRITNGCRDRISKCERTIND  to ID 0.
    NK --03/21/2016 --TO_CHAR (ColumnName, '999999999999990.99')  to TO_CHAR (ColumnName, 'FM999999999999990.00')
    RGG -- 04/01/2016 : Added LOANAGNTINVLVDIND to ID 38
    SS--04/21/2016 Modified to add new column LOANAMTLIMEXMPTIND
    BR -- 08/11/2016 Modified to add new column VNDRTYPIND
    SS--09/29/2016 Modified procedure to add new column LoanPymntSchdlFreq OPSMDEV-1123
    SS--11/29/2016 Added LOANGNTYMATSTIND OPSMDEV  1245
    NK-- 02/06/2017--OPSMDEV-1337-- Added LoanAppLspLastNm,LoanAppLspFirstNm, LoanAppLspInitialNm, LoanAppLspSfxNm, LoanAppLspTitlTxt, LoanAppLspPhn, LoanAppLspFax, LoanAppLspEMail in Id 38
    NK-- 02/27/2017-- OPSMDEV-1372-- Added LoanGntyNoteDt in ID 38
    NK-- 03/16/2017-- OPSMDEV-1385-- Added e.LoanEconDevObjctCd in ID 38
    SS--05/19/2017 --OPSMDEV 1436 Added CDCServFeePct to identifier 0.Added identifier 18
    RY--07/19/2017--CAFSOPER-805-- Added LoanAppCDCGrossDbentrAmt,LoanAppBalToBorrAmt to ID-0 and 38
    NK--09/13/2017--OPSMDEV 1529-- Added  b.LoanAppPrtNm as LenderLSPName, b.LoanAppLspHQLocId as LenderLSPLocId
    SS--10/17/2017 --OPSMDEV 1548-- Added LoanAppDisasterCntrlNmb to identifier 16
    JP - 10/26/2017 -- OPSMDEV-1123 -- Restored LoanPymntSchdlFreq column name in Identifier 38
    JP - 11/15/2017 -- CAFSOPER-1219 -- Got lenderlspname in Identifier 38 from partner schema
    SS--11/15/2017--OPSMDEV 1573 --Added LoanPrtCntctCellPhn,LoanPrtAltCntctFirstNm   ,LoanPrtAltCntctLastNm  ,LoanPrtAltCntctInitialNm  ,LoanPrtAltCntctTitlTxt  ,LoanPrtAltCntctPrimPhn  ,LoanPrtAltCntctCellPhn  ,
 LoanPrtAltCntctemail,LendrAltCntctTypCd  to identifier 38
   SS--11/16/2015 --OPSMDEV1569  Added LoanBusEstDt to identifier 38
   SS-- OPSMDEV 1573 Dropped  loanapplsplastnm,b.loanapplspfirstnm,loanapplspinitialnm,loanapplsptitltxt,loanapplspphn,loanapplspemail from identifier 38
   NK--01/12/2017--OPSMDEV 1644--Added LoanAppRevlMoIntQty, LoanAppAmortMoIntQty  to ID 0,38
   SS--Opsmdev 1081--01/24/2018-- added identifier 19
   JP -- 05/07/2018 -- OPSMDEV 1716 - for  LoanIntBaseRt and LoanIntRt fields formatted to 5 digit decimal as 'FM990.00000'
  SS--10/25/2018--OPSMDEV 1965 Added  LoanAppExtraServFeeAMT,LoanAppExtraServFeeInd  for extraordinary servicing fee to identifier 0 and 38
  SS--11/16/2018--Added function to identifier 38 for encryption OPSMDEV 2043
 SS--06/18/2019--OPSMDEV 2230 Added RqstSeqNmb to identifier 0 and 38
  JP -- 07/29/2019 OPSMDEV 2255 added p_identifier 20 and added parameters p_RqstSEQNMB and p_LocId
  SS-- 08/28/2019  Added identifier 21
  RY:09/04/2019: OPSMDEV-2271:: Added LastUpdtDt in id-19
  SS-03/03/2020 Added identifier 21 OPSMDEV 2406
  SS--04/03/2020 CARESACT - 63  added LoanAppMonthPayroll to identifier 0 and 38
  SL--01/04/2021 SODSTORY-317  Added LOANAPPPROJLMIND to the identerfier 38
  SL--01/05/2021 SODSTORY-318  Added LOANAPPPROJSIZECD to the identerfier 38
  SL--01/07/2021 SODSTORY-360/361.  Added: LOANAPPUSRESIND, PPPLOANNMB, LOANAPPPROJDELQIND, EIDLLOANNMB,LOANAPPPROJGRORCPTAMT1,LOANAPPPROJGRORCPTAMT2
  SL--01/07/2021 SODSTORY-554 Added preprocessing dates to id 18
  SL--03/03/2021 SODSTORY-571 Added new columns: LOANAPPSCHEDCIND, LOANAPPSCHEDCYR, LOANAPPGROSSINCOMEAMT  
  BO--06/28/2021--OPSMDEV-2768 -- Added PrtyGrpInd column to the "SELECT" list where identifier is 0 and 18
*/

    v_maxlqdcrseqnmb   NUMBER;
BEGIN
    --SAVEPOINT loanappsel;

    /* Select Row On the basis of Key*/
    --IF p_identifier = 0 THEN
    CASE p_identifier
        WHEN 0
        THEN
            BEGIN
                OPEN p_selcur FOR SELECT loanappnmb,
                                         prgmcd,
                                         prcsmthdcd,
                                         loanappnm,
                                         loanapprecvdt,
                                         loanappentrydt,
                                         loanapprqstamt,
                                         loanappsbagntypct,
                                         loanappsbarcmndamt,
                                         loanapprqstmatmoqty,
                                         loanappepcind,
                                         loanapppymtamt,
                                         loanappfullamortpymtind,
                                         loanappmointqty,
                                         loanapplifinsurrqmtind,
                                         loanapprcnsdrtnind,
                                         loanappinjctnind,
                                         loanappewcpsngltranspostind,
                                         loanappewcpsngltransind,
                                         loanappeligevalind,
                                         loanappnewbuscd,
                                         loanappdisastercntrlnmb,
                                         loancollatind,
                                         recovind,
                                         loanappoutprgrmareaofoperind,
                                         loanappparntloannmb,
                                         loanappmicrolendrid,
                                         statcd,
                                         loanappcdcgntyamt,
                                         loanappcdcnetdbentramt,
                                         loanappcdcfundfeeamt,
                                         loanappcdcseprtprcsfeeind,
                                         loanappcdcprcsfeeamt,
                                         loanappcdcclscostamt,
                                         loanappcdcothclscostamt
                                         loanappcdcundrwtrfeeamt,
                                         loanappcontribpct,
                                         loanappcontribamt,
                                         loandisastrappnmb,
                                         loandisastrappfeecharged,
                                         loandisastrappdcsn,
                                         loanassocdisastrappnmb,
                                         loanassocdisastrloannmb,
                                         loannmb,
                                         loanappfunddt,
                                         undrwritngby,
                                         cohortcd,
                                         fundsfisclyr,
                                         loantypind,
                                         loanappintdtlcd,
                                         loanpymninstlmntfreqcd,
                                         fininstrmnttypind,
                                         undrwritngcrdriskcertind,
                                         loanamtlimexmptind,
                                         loanpymntschdlfreq,
                                         loangntymatstind,
                                         loangntynotedt,
                                         cdcservfeepct,
                                         loanappcdcgrossdbentramt,
                                         loanappbaltoborramt,
                                         LoanAppRevlMoIntQty,
                                         LoanAppAmortMoIntQty,
                                         LoanAppExtraServFeeAMT,
                                         LoanAppExtraServFeeInd,
                                         RqstSeqNmb,
                                         LoanAppMonthPayroll,
                                         PrtyGrpInd
                                    FROM loanapptbl
                                   WHERE loanappnmb = p_loanappnmb;
            END;    
        WHEN 2
        THEN
            /*Check The Existance Of Row*/

            BEGIN
                OPEN p_selcur FOR SELECT 1
                                    FROM loanapptbl
                                   WHERE loanappnmb = p_loanappnmb;
            END;
        WHEN 11
        THEN
            BEGIN
                OPEN p_selcur FOR SELECT loanappeligevalind,
                                         ROUND (TO_NUMBER (loanapprqstamt), 2)     loanapprqstnum,
                                         prgmcd,
                                         prcsmthdcd
                                    FROM loanapptbl
                                   WHERE loanappnmb = p_loanappnmb;
            END;
        WHEN 12
        THEN
            /* Select all the Incomplete Applications for a given PrtSeqNmb */

            BEGIN
                OPEN p_selcur FOR SELECT a.loanappnmb, a.loanappnmb, b.borrseqnmb
                                    FROM loanapptbl a, loanborrtbl b
                                   WHERE a.loanappnmb = b.loanappnmb;
            END;
        WHEN 14
        THEN
            /* Select all the Incomplete Applications */

            BEGIN
                /*  b.BusSeqNmb ,
                              a.LoanAppAppcntFirstNm,
                              a.LoanAppAppcntLastNm*/
                OPEN p_selcur FOR
                    SELECT a.loanappnmb, b.borrseqnmb
                      FROM loanapptbl a, loanborrtbl b
                     WHERE     a.loanappnmb = b.loanappnmb
                           AND RPAD (111, 10, ' ') >= RPAD (TO_CHAR (p_datefrom, 'yyyy/mm/dd'), 10, ' ')
                           AND RPAD (111, 10, ' ') <= RPAD (TO_CHAR (p_dateto, 'yyyy/mm/dd'), 10, ' ');
            END;
        WHEN 15
        THEN
            BEGIN
                OPEN p_selcur FOR SELECT loanappnm
                                    FROM loanapptbl
                                   WHERE loanappnmb = p_loanappnmb;
            END;
        WHEN 16
        THEN
            BEGIN
                OPEN p_selcur FOR
                    SELECT a.loanappnmb,
                           a.loanappnm,
                           ROUND (TO_NUMBER (a.loanapprqstamt), 2)     loanapprqstnum,
                           loanappprojctynm,
                           loanappprojstcd,
                           a.statcd,
                           d.stattxt,
                           e.loannmb,
                           a.loanappfunddt,
                           a.undrwritngby,
                           a.cohortcd,
                           a.fundsfisclyr,
                           a.loantypind,
                           a.loanappdisastercntrlnmb
                      FROM loanapptbl        a,
                           loanappprojtbl    b,
                           loan.loangntytbl  e,
                           sbaref.statcdtbl  d
                     WHERE     a.loanappnmb = p_loanappnmb
                           AND a.loanappnmb = e.loanappnmb(+)
                           AND a.loanappnmb = b.loanappnmb(+)
                           AND a.statcd = d.statcd;
            END;
        WHEN 17
        THEN
            BEGIN
                OPEN p_selcur FOR
                    SELECT a.prcsmthdcd,
                           a.statcd,
                           a.loanappprcsofccd,
                           b.loanrvwruserid     loanapprvwr1userid,
                           c.loanrvwruserid     loanapprvwr2userid,
                           d.loanrvwruserid     loanapprvwr3userid
                      FROM loanapptbl  a
                           LEFT OUTER JOIN loanrvwrtbl b ON (a.loanappnmb = b.loanappnmb AND b.loanrvwrtyp = '1')
                           LEFT OUTER JOIN loanrvwrtbl c ON (a.loanappnmb = c.loanappnmb AND c.loanrvwrtyp = '2')
                           LEFT OUTER JOIN loanrvwrtbl d ON (a.loanappnmb = d.loanappnmb AND d.loanrvwrtyp = '3')
                     WHERE a.loanappnmb = p_loanappnmb;
            END;
        WHEN 38
        THEN
            BEGIN
                SELECT MAX (lqdcrseqnmb)
                  INTO v_maxlqdcrseqnmb
                  FROM loanapp.lqdcrwsctbl a
                 WHERE a.loanappnmb = p_loanappnmb;

                OPEN p_selcur FOR
                    /* 38 = Leet for Extract XML */
                    SELECT a.loanappnmb
                               applicationnumber,
                           p.addtnllocacqlmtind,
                           r.armmartypind,
                           TO_CHAR (r.armmarceilrt, 'FM990.000')
                               armmarceilrt,
                           TO_CHAR (r.armmarfloorrt, 'FM990.000')
                               armmarfloorrt,
                           p.bulksalelawcomplyind,
                           TO_CHAR (p.compslmtamt, 'FM999999999999990.00')
                               compslmtamt,
                           p.compslmtind,
                           e.loanecondevobjctcd
                               AS econdevobjctcd,
                           r.escrowacctrqrdind,
                           TO_CHAR (p.fixassetacqlmtamt, 'FM999999999999990.00')
                               fixassetacqlmtamt,
                           p.fixassetacqlmtind,
                           p.frnchsfeedefind,
                           p.frnchsfeedefmonnmb,
                           p.frnchslendrsameoppind,
                           p.frnchsrecrdaccsind,
                           p.frnchstermnotcind,
                           i.imrttypcd,
                           r.latechgaftdaynmb,
                           TO_CHAR (r.latechgfeepct, 'FM990.000')
                               latechgfeepct,
                           r.latechgind,
                           b.loanagntinvlvdind,                                                 -- RGG added on 04/01/16
                           a.loanamtlimexmptind,
                           i.loanintadjprdcd
                               loanappadjprdcd,
                           a.loanappappvdt,
                           TO_CHAR (i.loanintbasert, 'FM990.00000')
                               loanappbasert,
                           TO_CHAR (a.loanappcdcclscostamt, 'FM999999999999990.00')
                               loanappcdcclscostamt,
                           TO_CHAR (a.loanappcdcothclscostamt, 'FM999999999999990.00')
                               loanappcdcothclscostamt,
                           TO_CHAR (a.loanappcdcfundfeeamt, 'FM999999999999990.00')
                               loanappcdcfundfeeamt,
                           TO_CHAR (a.loanappcdcgntyamt, 'FM999999999999990.00')
                               loanappcdcgntyamt,
                           p.loanappcdcjobrat,
                           TO_CHAR (a.loanappcdcnetdbentramt, 'FM999999999999990.00')
                               loanappcdcnetdbentramt,
                           TO_CHAR (a.loanappcdcprcsfeeamt, 'FM999999999999990.00')
                               loanappcdcprcsfeeamt,
                           a.loanappcdcseprtprcsfeeind,
                           TO_CHAR (a.loanappcdcundrwtrfeeamt, 'FM999999999999990.00')
                               loanappcdcundrwtrfeeamt,
                           ocadataout (b.loanappcntctemail)
                               loanappcntctemail,
                           b.loanappcntctfax,
                           ocadataout (b.loanappcntctfirstnm)
                               loanappcntctfirstnm,
                           b.loanappcntctinitialnm,
                           ocadataout (b.loanappcntctlastnm)
                               loanappcntctlastnm,
                           ocadataout (b.loanappcntctphn)
                               loanappcntctphn,
                           b.loanappcntctsfxnm,
                           b.loanappcntcttitltxt,
                           --   b.loanapplsplastnm,
                           --   b.loanapplspfirstnm,
                           --     b.loanapplspinitialnm,
                           b.loanapplspsfxnm,
                           --   b.loanapplsptitltxt,
                           --   b.loanapplspphn,
                           b.loanapplspfax,
                           --   b.loanapplspemail,
                           TO_CHAR (a.loanappcontribamt, 'FM999999999999990.00')
                               loanappcontribamt,
                           a.loanappcontribpct,
                           p.loanappcurrempqty,
                           a.loanappdisastercntrlnmb,
                           a.loanappeligevalind,
                           a.loanappepcind,
                           a.loanappetranvennm,                                                 --SS added on 06/09/2016
                           a.loanappewcpsngltransind,
                           a.loanappewcpsngltranspostind,
                           i.loanintfixvarind
                               loanappfixvarind,
                           p.loanappfrnchscd,
                           p.loanappfrnchsind,
                           p.loanappfrnchsnm,
                           a.loanappfullamortpymtind,
                           TO_CHAR (i.loanintrt, 'FM990.00000')
                               loanappinitlintpct,
                           TO_CHAR (i.loanintsprdovrpct, 'FM990.000')
                               loanappinitlsprdovrprimepct,
                           a.loanappinjctnind,
                           a.loanappintdtlcd,
                           p.loanappjobcreatqty,
                           p.loanappjobrqmtmetind,
                           p.loanappjobrtnd,
                           a.loanapplifinsurrqmtind,
                           a.loanappmointqty,
                           a.LoanAppMonthPayroll, -- JP added 04/02/2020
                           a.loanappnetearnind,
                           TO_CHAR (p.loanappnetexprtamt, 'FM999999999999990.00')
                               loanappnetexprtamt,
                           a.loanappnewbuscd,
                           RTRIM (a.loanappnm)
                               loanappnm,
                           NULL
                               loanappothadjprdtxt,
                           a.loanappoutprgrmareaofoperind,
                           b.loanapppckgsourcctynm,
                           b.loanapppckgsourcnm,
                           b.loanapppckgsourcstcd,
                           b.loanapppckgsourcstr1nm,
                           b.loanapppckgsourcstr2nm,
                           b.loanapppckgsourczipcd,
                           b.loanapppckgsourczip4cd,
                           p.loanappprojcntycd,                                                  -- NK added on 04/26/16
                           p.loanappprojctynm,
                           p.loanappprojstcd,
                           p.loanappprojstr1nm,
                           p.loanappprojstr2nm,
                           p.loanappprojzipcd,
                           p.loanappprojzip4cd,
                           b.loanappprtappnmb,
                           b.loanappprtloannmb,
                           TO_CHAR (a.loanapppymtamt, 'FM999999999999990.00')
                               loanapppymtamt,
                           a.loanapprecvdt,
                           a.loanapprevlind,
                           a.loanapprcnsdrtnind,
                           TO_CHAR (a.loanapprqstamt, 'FM999999999999990.00')
                               loanapprqstamt,
                           a.loanapprqstmatmoqty,
                           p.loanappruralurbanind,
                           a.loanappsbagntypct,
                           a.loanappservofccd,
                           a.loanassocdisastrloannmb,
                           a.loancollatind,
                           a.loandisastrappdcsn,
                           a.loandisastrappfeecharged,
                           a.loandisastrappnmb,
                           b.loanpckgsourctypcd,                                                 -- NK added on 04/26/16
                           a.loanpymninstlmntfreqcd,
                           --a.LoanPymntSchdlFreq, -- BS added on 07/13/17
                           --REGEXP_REPLACE (a.loanpymntschdlfreq, ','), -- NK 10/04/2017 --CAFSOPER
                           a.loanpymntschdlfreq,                                                        -- JP 10/26/2017
                           a.loanpymtrepayinstltypcd,
                           a.loantypind,
                           w.lqdcrtotscr,
                           p.naicscd,
                           r.netearnclauseind,
                           TO_CHAR (r.netearnpymntoveramt, 'FM999999999999990.00')
                               netearnpymntoveramt,
                           TO_CHAR (r.netearnpymntpct, 'FM990.000')
                               netearnpymntpct,
                           a.prcsmthdcd,
                           a.prgmcd,                                                              --NK added on 04/26/16
                           r.pymntbegnmonmb,
                           r.pymntdaynmb,
                           r.pymntintonlybegnmonmb,
                           r.pymntintonlydaynmb,
                           r.pymntintonlyfreqcd,
                           a.statcd,
                           r.stintrtreductnind,
                           r.stintrtreductnprgmnm,
                           a.undrwritngby,
                           a.loangntymatstind,                                                   --SS added on 12/2/2016
                           (SELECT vndrtypind
                              FROM sbaref.vndrswcdtbl
                             WHERE vndrswnm = a.loanappetranvennm)
                               AS vndrtypind,                                              --BR added on 08/11/2016 #862
                           a.loangntynotedt,                                                            --NK--02/27/2017
                           e.loanecondevobjctcd,
                           a.loanappcdcgrossdbentramt,
                           a.loanappbaltoborramt,
                           --b.loanappprtnm                                              AS lenderlspname,
                           prt.prtlglnm
                               AS lenderlspname,                                                        -- JP 11/15/2017
                           b.loanapplsphqlocid
                               AS lenderlsplocid,
                           ocadataout (LoanPrtCntctCellPhn)
                               AS LoanPrtCntctCellPhn,
                           ocadataout (b.LoanPrtAltCntctFirstNm)
                               AS loanapplspfirstnm,
                           ocadataout (b.LoanPrtAltCntctLastNm)
                               AS loanapplsplastnm,
                           b.LoanPrtAltCntctInitialNm
                               AS loanapplspinitialnm,
                           b.LoanPrtAltCntctTitlTxt
                               AS loanapplsptitltxt,
                           ocadataout (b.LoanPrtAltCntctPrimPhn)
                               AS loanapplspphn,
                           ocadataout (b.LoanPrtAltCntctCellPhn)
                               AS LoanPrtAltCntctCellPhn,
                           ocadataout (b.LoanPrtAltCntctemail)
                               AS loanapplspemail,
                           b.LendrAltCntctTypCd,
                           p.LoanBusEstDt,
                           ocadataout (b.LoanPrtAltCntctFirstNm)
                               AS LoanPrtAltCntctFirstNm,
                           ocadataout (b.LoanPrtAltCntctLastNm)
                               AS LoanPrtAltCntctLastNm,
                           b.LoanPrtAltCntctInitialNm,
                           b.LoanPrtAltCntctTitlTxt,
                           ocadataout (b.LoanPrtAltCntctPrimPhn)
                               AS LoanPrtAltCntctPrimPhn,
                           ocadataout (b.LoanPrtAltCntctemail)
                               AS LoanPrtAltCntctemail,
                           a.LoanAppRevlMoIntQty,
                           a.LoanAppAmortMoIntQty,
                           a.LoanAppExtraServFeeAmt,                                                  -- SS --10/25/2018
                           a.LoanAppExtraServFeeInd,                                                  -- SS --10/25/2018
                           a.RqstSeqNmb,
                           p.LOANAPPPROJLMIND,                                                        -- SL -- 01/04/2021 SOD-317
                           p.LOANAPPPROJSIZECD,                                                       -- SL -- 01/05/2021 SOD-318
                           p.LOANAPPPROJDELQIND,                                                      -- SL -- 01/07/2021 SOD-360/361
                           p.EIDLLOANNMB,                                                             -- SL -- 01/07/2021 SOD-360/361
                           p.LOANAPPPROJGRORCPTAMT1,                                                  -- SL -- 01/07/2021 SOD-360/361
                           p.LOANAPPPROJGRORCPTAMT2,                                                  -- SL -- 01/07/2021 SOD-360/361
                           p.PPPLOANNMB,                                                              -- SL -- 01/07/2021 SOD-360/361
                           LoanAppMonthPayroll,
                           a.LOANAPPUSRESIND,                                                          -- SL -- 01/07/2021 SOD-360/361
                           a.LOANAPPSCHEDCIND,
                           a.LOANAPPSCHEDCYR,
                           a.LOANAPPGROSSINCOMEAMT 
                      FROM loanapp.loanapptbl  a
                           LEFT OUTER JOIN loanapp.loanappprttbl b ON (b.loanappnmb = a.loanappnmb)          -- b = bank
                           LEFT OUTER JOIN loanapp.loanappprojtbl p ON (p.loanappnmb = a.loanappnmb)         -- p = proj
                           LEFT OUTER JOIN loanapp.loanpymnttbl r ON (r.loanappnmb = a.loanappnmb) -- r = repayment #1118
                           LEFT OUTER JOIN loanapp.lqdcrwsctbl w
                               ON ((w.loanappnmb = a.loanappnmb)                                   -- w = weighted score
                                                                 AND (w.lqdcrseqnmb = v_maxlqdcrseqnmb))
                           LEFT OUTER JOIN loanapp.loanintdtltbl i
                               ON ((i.loanappnmb = a.loanappnmb)                                         -- i = interest
                                                                 AND (i.loanintdtlseqnmb = 1))
                           LEFT OUTER JOIN loanapp.loanecondevobjctchldtbl e
                               ON ((e.loanappnmb = a.loanappnmb)                                   -- e = econdevobjctcd
                                                                 AND (e.loanecondevobjctseqnmb = 1))
                           LEFT OUTER JOIN partner.locsrchtbl loc ON (loc.locid = b.loanapplsphqlocid)  -- JP 11/15/2017
                           LEFT OUTER JOIN partner.prtsrchtbl prt ON (prt.prtid = loc.prtid)            -- JP 11/15/2017
                     WHERE a.loanappnmb = p_loanappnmb;
            END;
        WHEN 18
        THEN
            BEGIN
                OPEN p_selcur FOR SELECT a.cdcservfeepct,
                                         a.DspstnStatCd,
                                         dspstn.DspstnStatDescTxt,
                                         prePrcs.STARRECVDT,
                                         prePrcs.STARUPLDDT,
                                         a.PrtyGrpInd
                                    FROM loanapp.loanapptbl a
                                    LEFT JOIN SBAREF.DSPSTNSTATCDTBL dspstn ON dspstn.DspstnStatCd = a.DspstnStatCd
                                    LEFT JOIN loanApp.CARESPrePrcsTbl prePrcs ON a.loanappnmb = prePrcs.loanappnmb
                                   WHERE a.loanappnmb = p_loanappnmb;
            END;
        WHEN 19
        THEN
            OPEN p_selcur FOR SELECT Statcd, LastUpdtDt
                                FROM loanapp.loanapptbl
                               WHERE loanappnmb = p_loanappnmb;
        WHEN 20
        --check if a request id exists under the same location id
        THEN
            OPEN p_selcur FOR SELECT RqstSEQNMB
                                FROM loanapp.Loanapptbl a, loanapp.loanappprttbl b
                               WHERE a.loanappnmb = b.loanappnmb AND a.RqstSEQNMB = p_RqstSeqNmb AND b.LocId = p_LocId;
        WHEN 21
        THEN
            BEGIN
                OPEN p_selcur FOR SELECT a.loanappsourctypcd, loanappsourctypdesctxt
                                    FROM loanapp.LoanappTbl a, loanapp.loanappsourctyptbl b
                                   WHERE loanappnmb = p_loanappnmb AND a.loanappsourctypcd = b.loanappsourctypcd;
            END;
    END CASE;

    p_retval := NVL (p_retval, 0);
EXCEPTION
    WHEN OTHERS
    THEN
        BEGIN
            --   ROLLBACK TO loanappsel;
            RAISE;
        END;
END loanappseltsp;
/
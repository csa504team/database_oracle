CREATE OR REPLACE PROCEDURE LOANAPP.LOANSUBPROCDDELTSP(
p_Identifier 	NUMBER:=0,
p_RetVal OUT NUMBER,
p_LoanSubProcdId 	NUMBER:=0
)
AS
v_LOANAPPNMB NUMBER :=0;
v_LOANPROCDTYPCD CHAR(3) := NULL;
v_PROCDTYPCD CHAR(1) := NULL;

BEGIN
	SAVEPOINT LOANSUBPROCDDELTSP;
	IF  p_Identifier = 0 THEN
	BEGIN
        select LOANAPPNMB, LOANPROCDTYPCD, PROCDTYPCD into v_LOANAPPNMB, v_LOANPROCDTYPCD, v_PROCDTYPCD from LOANAPP.LOANSUBPROCDTBL where LOANSUBPROCDID = p_LOANSUBPROCDID;
		DELETE  LOANSUBPROCDTBL
		WHERE LOANSUBPROCDID = p_LOANSUBPROCDID;
		
        LOANSUBPROCDSUMTSP(v_LOANAPPNMB,v_LOANPROCDTYPCD,v_PROCDTYPCD);
		p_RetVal := SQL%ROWCOUNT;
	END;
	END IF;
p_RetVal := nvl(p_RetVal,0);    
EXCEPTION
WHEN OTHERS THEN
BEGIN
ROLLBACK TO LOANSUBPROCDDELTSP;
RAISE;
END;
END LOANSUBPROCDDELTSP;
/

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDDELTSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDDELTSP TO LOANACCTREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDDELTSP TO LOANAPPREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDDELTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDDELTSP TO LOANORIGBORROWER;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDDELTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDDELTSP TO LOANREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDDELTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDDELTSP TO POOLSECADMINROLE;

CREATE OR REPLACE PROCEDURE LOANAPP.LOANSUBPROCDDELALLTSP(
p_Identifier 	NUMBER:=0,
p_RetVal OUT NUMBER,
p_LoanAppNmb 	NUMBER:=0
)
AS

BEGIN
	SAVEPOINT LOANSUBPROCDDELALLTSP;
	IF  p_Identifier = 0 THEN
	BEGIN
		DELETE  LoanSubProcdTbl
		WHERE LoanAppNmb = p_LoanAppNmb
		and LOANPROCDTYPCD not in (select LOANPROCDTYPCD from LOANAPP.LoanProcdTbl where LOANAPPNMB=p_LoanAppNmb) and PROCDTYPCD='E';
		p_RetVal := SQL%ROWCOUNT;
	END;
	END IF;
p_RetVal := nvl(p_RetVal,0);    
EXCEPTION
WHEN OTHERS THEN
BEGIN
ROLLBACK TO LOANSUBPROCDDELALLTSP;
RAISE;
END;
END LOANSUBPROCDDELALLTSP;
/

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDDELALLTSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDDELALLTSP TO LOANACCTREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDDELALLTSP TO LOANAPPREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDDELALLTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDDELALLTSP TO LOANORIGBORROWER;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDDELALLTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDDELALLTSP TO LOANREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDDELALLTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOANAPP.LOANSUBPROCDDELALLTSP TO POOLSECADMINROLE;

CREATE OR REPLACE PROCEDURE LOANAPP.LOANAUTHDOCCSP (p_LoanAppNMb                             NUMBER := NULL,
                                                    p_CreatuserId                            VARCHAR2 := NULL,
                                                    p_SelCur1                            OUT SYS_REFCURSOR,
                                                    p_SelCur2                            OUT SYS_REFCURSOR,
                                                    p_SelCur3                            OUT SYS_REFCURSOR,
                                                    p_SelCur4                            OUT SYS_REFCURSOR,
                                                    --p_SelCur5                            OUT SYS_REFCURSOR,
                                                    p_SelCur6                            OUT SYS_REFCURSOR,
                                                    --p_SelCur7 out sys_refcursor,
                                                    p_SelCur7                            OUT SYS_REFCURSOR,
                                                    p_SelCur8                            OUT SYS_REFCURSOR,
                                                    p_SelCur9                            OUT SYS_REFCURSOR,
                                                    p_SelCur10                           OUT SYS_REFCURSOR,
                                                    p_SelCur11                           OUT SYS_REFCURSOR,
                                                    p_SelCur12                           OUT SYS_REFCURSOR,
                                                    p_SelCur13                           OUT SYS_REFCURSOR,  
                                                    p_SelCur14                           OUT SYS_REFCURSOR,
                                                    p_TermInYears                        OUT FLOAT,
                                                    p_LoanNmb                            OUT VARCHAR2,
                                                    p_AppvDt                             OUT DATE,
                                                    p_DebentureAmt                       OUT NUMBER,
                                                    p_default_string                     OUT VARCHAR2,
                                                    p_EPCText                            OUT VARCHAR2,
                                                    p_LOANAPPEPCIND                      OUT CHAR,
                                                    p_REFIOPTION                         OUT VARCHAR2,
                                                    --p_ProceedText out varchar2,
                                                    --p_LoanProcdAmt out number,
                                                    p_total_LOANPROCDAMT                 OUT NUMBER,
                                                    p_LoanPartLendrAmt                   OUT NUMBER,
                                                    p_NetDebenturePercent                OUT NUMBER,
                                                    --p_ClosingCosts         OUT NUMBER,
                                                    p_LoanInterimLoanSet_flag            OUT NUMBER,
                                                    p_TotalClosing                       OUT NUMBER,
                                                    p_SubtotalAdminCosts                 OUT NUMBER,
                                                    p_TotalDebentureAdmin                OUT NUMBER,
                                                    p_UnderwriterText                    OUT VARCHAR2,
                                                    p_SubTotalText                       OUT VARCHAR2,
                                                    p_DisburseMonths                     OUT VARCHAR2,
                                                    p_OCtext                             OUT VARCHAR2,
                                                    p_TotalInterimAmt                    OUT NUMBER,
                                                    p_D2A_InterimLoanStart               OUT VARCHAR2,
                                                    p_D2B_InterimLoanRow                 OUT VARCHAR2,
                                                    p_D2C_InterimLoanEnd                 OUT VARCHAR2,
                                                    p_D3_Escrow                          OUT VARCHAR2,
                                                    p_ThirdPartyPercent                  OUT NUMBER,
                                                    p_D4B_PermLoanRow                    OUT VARCHAR2,
                                                    p_D4C_Preexisting3rdPartyLoan        OUT VARCHAR2,
                                                    p_Minimum3rdPartyLoanTerm            OUT VARCHAR2,
                                                    p_AnnualGntyFeePct                   OUT NUMBER,
                                                    p_LoanAppContribAmt                  OUT NUMBER,
                                                    p_LoanAppContribPct                  OUT NUMBER) AS
    origstate                   BOOLEAN := TRUE;
    loannmb_in                  CHAR (10) := NULL;
    loanapp_table_rec           LOANAPP.LOANAPPTBL%ROWTYPE;
    loan_table_rec              LOAN.LOANGNTYTBL%ROWTYPE;
    sbaofcd                     CHAR (5) := NULL;
    InCA                        BOOLEAN := FALSE;
    BusinessRECollateral        BOOLEAN := FALSE;
    PPCollateral                BOOLEAN := FALSE;
    MarineCollateral            BOOLEAN := FALSE;
    LiabilityInsurance          BOOLEAN := FALSE;
    ProductLiabilityInsurance   BOOLEAN := FALSE;
    DramShopInsurance           BOOLEAN := FALSE;
    MalpracticeInsurance        BOOLEAN := FALSE;
    WorkersCompInsurance        BOOLEAN := FALSE;
    OtherInsurance              BOOLEAN := FALSE;
    InsuranceTypes              VARCHAR2 (255) := NULL;
    EnvironmentalRequired       BOOLEAN := FALSE;
    /* count borrowers for H2_CertificationAgreements() */
    NumBorrowers                NUMBER := 0;
    Suffix                      VARCHAR2 (10) := NULL;
    TradeName                   VARCHAR2 (80) := NULL;
    modified_Borr_Address       VARCHAR2 (255) := NULL;
    modified_Borr_Insurance     VARCHAR2 (100) := NULL;
    modified_Guar_Address       VARCHAR2 (255) := NULL;
    modified_Guar_Insurance     VARCHAR2 (100) := NULL;
    p_ProceedText               VARCHAR2 (250) := NULL;
    p_LoanProcdAmt              NUMBER := 0;
    v_TotalInterimAmt           NUMBER :=0;
    ProcFeeToBePaid             NUMBER := 0;
    ClosingCostToBePaid         NUMBER := 0;
    PreExistingLoanAmt          NUMBER := 0;

-- Created 7/15/2021 JK for OPSMDEV 2729


    --LOAN or LOANAPP
    CURSOR OrignationRec 
    IS
        SELECT * FROM LOANAPP.LOANAPPTBL where LOANAPPNMB=p_LoanAppNmb;
        
    CURSOR ServicingRec 
    IS
        SELECT * FROM LOAN.LOANGNTYTBL where LOANAPPNMB=p_LoanAppNmb;
    --Check if OC is Guarantor
    CURSOR OCGuarantor IS
        SELECT g.*,
                  b.BusPhyAddrStr1NM
               || ' '
               || b.BusPhyAddrStr2Nm
               || ' '
               || b.BusPhyAddrCtyNm
               || ' , '
               || b.BusPhyAddrStCd
               || ' '
               || b.BusPhyAddrZipCd
                   AS Address,
               b.BusPhyAddrStCd
                   AS State,
               b.BusNm
                   AS Name,
               b.BusTrdNm
        FROM LoanApp.LoanGuarTbl g
             JOIN LoanApp.LoanBusTbl b
             ON (    g.TaxId = b.TaxId
                 AND g.LoanAppNmb = b.LoanAppNmb)
        WHERE     g.GUARBUSPERIND = 'B'
              AND g.LoanGuarOperCoInd = 'Y'
              AND g.LOANAPPNMB = p_LoanAppNmb;

    CURSOR OCGuarantor1 IS
        SELECT g.*,
                  b.BusPhyAddrStr1NM
               || ' '
               || b.BusPhyAddrStr2Nm
               || ' '
               || b.BusPhyAddrCtyNm
               || ' , '
               || b.BusPhyAddrStCd
               || ' '
               || b.BusPhyAddrZipCd
                   AS Address,
               b.BusPhyAddrStCd
                   AS State,
               b.BusNm
                   AS Name,
               b.BusTrdNm
        FROM Loan.LOANGNTYGUARTBL g
             JOIN Loan.BusTbl b
             ON (g.TaxId = b.TaxId)
        WHERE     g.GUARBUSPERIND = 'B'
              AND g.LoanGuarOperCoInd = 'Y'
              AND g.LOANAPPNMB = p_LoanAppNmb;


    CURSOR LoanAppBorr IS
        SELECT bor.*,
                  bus.BusPhyAddrStr1NM
               || ' '
               || bus.BusPhyAddrStr2Nm
               || ' '
               || bus.BusPhyAddrCtyNm
               || ','
               || bus.BusPhyAddrStCd
               || ' '
               || bus.BusPhyAddrZipCd
                   AS Address,
               bus.BusPhyAddrStCd
                   AS State,
               bus.BusNm
                   AS Name,
               bus.BusTrdNm
        FROM LOANAPP.LoanBorrTbl bor
             JOIN LOANAPP.LoanBusTbl bus
             ON (    bor.TaxId = bus.TaxId
                 AND bor.LoanAppNmb = bus.LoanAppNmb)
        WHERE     bor.BorrBusPerInd = 'B'
              AND bor.LOANAPPNMB = p_LoanAppNmb
        UNION
        --Person type borrower's information if origination=true
        SELECT bor.*,
                  p.PerPhyAddrStr1NM
               || ' '
               || p.PerPhyAddrStr2Nm
               || ' '
               || p.PerPhyAddrCtyNm
               || ' , '
               || p.PerPhyAddrStCd
               || ' '
               || p.PerPhyAddrZipCd
                   AS Address,
               p.PerPhyAddrStCd
                   AS State,
                  ocadataout (p.PerFirstNm)
               || ' '
               || CAST (p.PerInitialNm AS VARCHAR2 (2))
               || ' '
               || ocadataout (p.PerLastNm)
               || ' '
               || CAST (p.PerSfxNm AS VARCHAR2 (5))
                   AS Name,
               NULL
                   AS BusTrdNm
        FROM loanapp.LoanBorrTbl bor
             JOIN loanapp.LoanPerTbl p
             ON (    bor.TAXID = p.TaxId
                 AND bor.LoanAppNmb = p.LoanAppNmb)
        WHERE     bor.BorrBusPerInd = 'P'
              AND bor.LOANAPPNMB = p_LoanAppNmb;

    CURSOR LoanBorr IS
        SELECT bor.*,
                  bus.BusPhyAddrStr1NM
               || ' '
               || bus.BusPhyAddrStr2Nm
               || ' '
               || bus.BusPhyAddrCtyNm
               || ','
               || bus.BusPhyAddrStCd
               || ' '
               || bus.BusPhyAddrZipCd
                   AS Address,
               bus.BusPhyAddrStCd
                   AS State,
               bus.BusNm
                   AS Name,
               bus.BusTrdNm
        FROM LOAN.LoanGntyBorrTbl bor
             JOIN LOAN.BusTbl bus
             ON (bor.TaxId = bus.TaxId)
        WHERE     bor.BorrBusPerInd = 'B'
              AND bor.LOANAPPNMB = p_LoanAppNmb
        UNION
        --Person type borrower's information if origination=false
        SELECT bor.*,
                  p.PerPhyAddrStr1NM
               || ' '
               || p.PerPhyAddrStr2Nm
               || ' '
               || p.PerPhyAddrCtyNm
               || ' , '
               || p.PerPhyAddrStCd
               || ' '
               || p.PerPhyAddrZipCd
                   AS Address,
               p.PerPhyAddrStCd
                   AS State,
                  ocadataout (p.PerFirstNm)
               || ' '
               || CAST (p.PerInitialNm AS VARCHAR2 (2))
               || ' '
               || ocadataout (p.PerLastNm)
               || ' '
               || CAST (p.PerSfxNm AS VARCHAR2 (5))
                   AS Name,
               NULL
                   AS BusTrdNm
        FROM loan.LoanGntyBorrTbl bor
             JOIN loan.PerTbl p
             ON (bor.TAXID = p.TaxId)
        WHERE     bor.BorrBusPerInd = 'P'
              AND bor.LOANAPPNMB = p_LoanAppNmb;

    --Records for LoanSunProceed
    CURSOR LOANSUBPROCD IS
        SELECT *
        FROM LOANAPP.LOANSUBPROCDTBL
        WHERE     LOANAPPNMB = p_LoanAppNMb
              AND PROCDTYPCD = 'E'
              AND LOANPROCDTYPCD IN ('01',
                                     '02',
                                     '04',
                                     '10',
                                     '15',
                                     '19')
        ORDER BY LOANPROCDADDR DESC, LOANPROCDTYPCD ASC;

    CURSOR LOANGNTYSUBPROCD IS
        SELECT *
        FROM LOAN.LOANGNTYSUBPROCDTBL
        WHERE     LOANAPPNMB = p_LoanAppNMb
              AND PROCDTYPCD = 'E'
              AND LOANPROCDTYPCD IN ('01',
                                     '02',
                                     '04',
                                     '10',
                                     '15',
                                     '19')
        ORDER BY LOANPROCDADDR DESC, LOANPROCDTYPCD ASC;

    --Records for LoanSunProceed LOANPROCDTYPCD 15
    CURSOR LOANSUBPROCD_15 IS
        SELECT *
        FROM LOANAPP.LOANSUBPROCDTBL
        WHERE     LOANAPPNMB = p_LoanAppNMb
              AND PROCDTYPCD = 'E'
              AND LOANPROCDTYPCD = '15'
        ORDER BY LOANPROCDADDR DESC, LOANPROCDTYPCD ASC;

    CURSOR LOANGNTYSUBPROCD_15 IS
        SELECT *
        FROM LOAN.LOANGNTYSUBPROCDTBL
        WHERE     LOANAPPNMB = p_LoanAppNMb
              AND PROCDTYPCD = 'E'
              AND LOANPROCDTYPCD = '15'
        ORDER BY LOANPROCDADDR DESC, LOANPROCDTYPCD ASC;

    --LoanProceed Records
    CURSOR LOANPROCD IS
        SELECT *
        FROM LOANAPP.LOANPROCDTBL
        WHERE     LoanAppNmb = p_LoanAppNMb
              AND ProcdTypCd = 'E'
        ORDER BY LoanProcdTypCD;

    CURSOR LOANGNTYPROCD IS
        SELECT *
        FROM LOAN.LOANGNTYPROCDTBL
        WHERE     LoanAppNmb = p_LoanAppNMb
              AND ProcdTypCd = 'E'
        ORDER BY LoanProcdTypCD;
        
        
    --3rd Party Lender
    CURSOR LoanThirdPartySet
    IS
    SELECT LoanPartLendrNm, LoanPartLendrAmt,LOANLENDRGROSSAMT, LoanPartLendrTypCd, LocId
    FROM LoanApp.LoanPartLendrTbl  
    WHERE LoanAppNmb = p_LoanAppNMb;
    
    CURSOR LoanGntyThirdPartySet
    IS
    SELECT LoanPartLendrNm, LoanPartLendrAmt,LOANLENDRGROSSAMT, LoanPartLendrTypCd, LocId
    FROM Loan.LoanGntyPartLendrTbl  
    WHERE LoanAppNmb = p_LoanAppNMb;
    
    --Frank's request
    CURSOR LOANCOLLAT IS  
    SELECT Collat.*,ROWNUM
                FROM LoanApp.LoanCollatTbl Collat
                WHERE LOANAPPNMB = p_LOANAPPNMB
                        and Collat.LOANAPPNMB NOT IN ( 
                                SELECT       LOANAPPNMB 
                                FROM   LoanApp.LoanLmtGntyCollatTbl LmtGnty
                                WHERE LmtGnty.LOANAPPNMB = Collat.LOANAPPNMB )
                ORDER BY LOANCOLLATSEQNMB, LOANCOLLATSUBTYPCD, LOANCOLLATTYPCD;
    CURSOR LOANGNTYCOLLAT IS
    SELECT Collat.*,ROWNUM
                FROM Loan.LoanGntyCollatTbl Collat
                WHERE LOANAPPNMB = p_LOANAPPNMB
                        and Collat.LOANAPPNMB NOT IN ( 
                                SELECT       LOANAPPNMB 
                                FROM   Loan.LoanLmtGntyCollatTbl LmtGnty
                                WHERE LmtGnty.LOANAPPNMB = Collat.LOANAPPNMB )
                ORDER BY LOANCOLLATSEQNMB, LOANCOLLATSUBTYPCD, LOANCOLLATTYPCD;
                
    OrignationRec_rec           OrignationRec%ROWTYPE;
    ServicingRec_rec            ServicingRec%ROWTYPE;
    ocguarantor_rec             OCGuarantor%ROWTYPE;
    ocguarantor_rec1            OCGuarantor1%ROWTYPE;
    LoanAppBorr_rec             LoanAppBorr%ROWTYPE;
    LoanBorr_rec                LoanBorr%ROWTYPE;
    LOANSUBPROCD_rec            LOANSUBPROCD%ROWTYPE;
    LOANGNTYSUBPROCD_rec        LOANGNTYSUBPROCD%ROWTYPE;
    LOANSUBPROCD_rec15          LOANSUBPROCD%ROWTYPE;
    LOANGNTYSUBPROCD_rec15      LOANGNTYSUBPROCD%ROWTYPE;
    LOANPROCD_rec               LOANPROCD%ROWTYPE;
    LOANGNTYPROCD_rec           LOANGNTYPROCD%ROWTYPE;
    LOANTHIRDPARTYSET_rec       LOANTHIRDPARTYSET%ROWTYPE;
    LOANGNTYTHIRDPARTYSET_rec   LOANGNTYTHIRDPARTYSET%ROWTYPE;
    LOANINTERIMLOANSET_rec      LOANTHIRDPARTYSET%ROWTYPE;
    LOANGNTYINTERIMLOANSET_rec  LOANGNTYTHIRDPARTYSET%ROWTYPE;
    LOANCOLLAT_rec              LOANCOLLAT%ROWTYPE;
    LOANGNTYCOLLAT_rec          LOANGNTYCOLLAT%ROWTYPE;
BEGIN
    SAVEPOINT LOANAUTHDOCCSP;
    -- to check if loan is in originaiton or servicing
    p_default_string := '[MISSING DATA]';

    SELECT LOANNMB
    INTO loannmb_in
    FROM LOANAPP.LOANAPPTBL
    WHERE LOANAPPNMB = p_LoanAppNmb;

    IF (loannmb_in IS NULL) THEN
        origstate := TRUE;
    ELSE
        origstate := FALSE;
    END IF;
    
        IF origstate=true then
        OPEN OrignationRec;
            LOOP 
                FETCH OrignationRec into OrignationRec_rec;
                EXIT WHEN OrignationRec%notfound;
            END LOOP;
        CLOSE OrignationRec;
    ELSE
        OPEN ServicingRec;
            LOOP 
                FETCH ServicingRec into ServicingRec_rec;
                EXIT WHEN ServicingRec%notfound;
            END LOOP;
        CLOSE ServicingRec;    
    END IF;

    --checks if in origination or servicing
    IF origstate = TRUE THEN
        --cursor to retrieve Loan information
        OPEN p_SelCur1 FOR
            SELECT CAST (NVL (LoanNmb, p_default_string) AS CHAR (25))       AS LOANNMB,
                   CAST (NVL (LoanAppNm, p_default_string) AS CHAR (25))     AS LOANAPPNM
            FROM LOANAPP.LOANAPPTBL
            WHERE LoanappNmb = p_LoanAppNmb;

        SELECT *
        INTO loanapp_table_rec
        FROM LOANAPP.LOANAPPTBL
        WHERE LoanappNmb = p_LoanAppNmb;
    ELSE
        OPEN p_SelCur1 FOR
            SELECT CAST (NVL (LoanNmb, p_default_string) AS CHAR (25))       AS LOANNMB,
                   CAST (NVL (LoanAppNm, p_default_string) AS CHAR (25))     AS LOANAPPNM
            FROM LOAN.LOANGNTYTBL
            WHERE LoanappNmb = p_LoanAppNmb;

        SELECT *
        INTO loan_table_rec
        FROM LOAN.LOANGNTYTBL
        WHERE LoanappNmb = p_LoanAppNmb;
    END IF;



    --cursor to retrieve LOANAPPPRTTBL information
    OPEN p_SelCur2 FOR SELECT CAST (NVL (LoanAppPrtNm, p_default_string) AS VARCHAR2 (200))       AS LOANAPPPRTNM,
                              CAST (NVL (LoanAppFirsNmb, p_default_string) AS CHAR (25))          AS LOANAPPFIRSNMB,
                              CAST (NVL (LoanAppPrtStr1, p_default_string) AS VARCHAR2 (80))      AS LOANAPPPRTSTR1,
                              CAST (NVL (LoanAppPrtStr2, p_default_string) AS VARCHAR2 (80))      AS LOANAPPPRTSTR2,
                              CAST (NVL (LoanAppPrtCtyNm, p_default_string) AS VARCHAR2 (40))     AS LOANAPPPRTCTYNM,
                              CAST (NVL (LoanAppPrtStCd, p_default_string) AS CHAR (25))          AS LOANAPPPRTSTCD,
                              CAST (NVL (LoanAppPrtZipCd, p_default_string) AS CHAR (25))         AS LOANAPPPRTZIPCD
                       FROM LOANAPP.LOANAPPPRTTBL
                       WHERE LoanAppNmb = p_LoanAppNmb;

    -- cursor to retrieve OFCCD information
    IF origstate = TRUE THEN
        sbaofcd := loanapp_table_rec.LOANAPPORIGNTNOFCCD;

        OPEN p_SelCur3 FOR SELECT CAST (NVL (SBAOFC1NM, p_default_string) AS VARCHAR2 (80))        AS SBAOFC1NM,
                                  CAST (NVL (SBAOFCSTRNM, p_default_string) AS VARCHAR2 (80))      AS SBAOFCSTRNM,
                                  CAST (NVL (SBAOFCSTR2NM, p_default_string) AS VARCHAR2 (80))     AS SBAOFCSTR2NM,
                                  CAST (NVL (SBAOFCCTYNM, p_default_string) AS VARCHAR2 (80))      AS SBAOFCCTYNM,
                                  CAST (NVL (STCD, p_default_string) AS CHAR (2))                  AS STCD,
                                  CAST (NVL (ZIPCD5, p_default_string) AS CHAR (5))                AS ZIPCD5
                           FROM SBAREF.SBAOFCTBL
                           WHERE SBAOFCCD = sbaofcd;
    ELSE
        sbaofcd := loan_table_rec.LOANAPPORIGNTNOFCCD;

        OPEN p_SelCur3 FOR SELECT CAST (NVL (SBAOFC1NM, p_default_string) AS VARCHAR2 (80))        AS SBAOFC1NM,
                                  CAST (NVL (SBAOFCSTRNM, p_default_string) AS VARCHAR2 (80))      AS SBAOFCSTRNM,
                                  CAST (NVL (SBAOFCSTR2NM, p_default_string) AS VARCHAR2 (80))     AS SBAOFCSTR2NM,
                                  CAST (NVL (SBAOFCCTYNM, p_default_string) AS VARCHAR2 (80))      AS SBAOFCCTYNM,
                                  CAST (NVL (STCD, p_default_string) AS CHAR (2)),
                                  CAST (NVL (ZIPCD5, p_default_string) AS CHAR (5))
                           FROM SBAREF.SBAOFCTBL
                           WHERE SBAOFCCD = sbaofcd;
    END IF;

    --output variable for TermInYears
    IF origstate = TRUE THEN
        p_TermInYears := loanapp_table_rec.LOANAPPRQSTMATMOQTY / 12;
        IF (p_TermInYears = 120) THEN 
            p_Minimum3rdPartyLoanTerm := '7';
        ELSE
            p_Minimum3rdPartyLoanTerm := '10';
        END IF;
    ELSE
        p_TermInYears := loan_table_rec.LOANAPPRQSTMATMOQTY / 12;
        IF (p_TermInYears = 120) THEN 
            p_Minimum3rdPartyLoanTerm := '7';
        ELSE
            p_Minimum3rdPartyLoanTerm := '10';
        END IF;
    END IF;

    --Debenture amount
    IF origstate = TRUE THEN
        p_LoanNmb := NULL;
        p_AppvDt := NULL;
        p_DebentureAmt := loanapp_table_rec.LOANAPPRQSTAMT;
    ELSE
        p_LoanNmb := loan_table_rec.LOANNMB;
        p_AppvDt := loan_table_rec.LOANLASTAPPVDT;
        p_DebentureAmt := loan_table_rec.LOANCURRAPPVAMT;
    END IF;

    --Business type borrower's information if origination=true
    IF origstate = TRUE THEN
        OPEN p_SelCur4 FOR
            SELECT bor.*,
                      bus.BusPhyAddrStr1NM
                   || ' '
                   || bus.BusPhyAddrStr2Nm
                   || ' '
                   || bus.BusPhyAddrCtyNm
                   || ','
                   || bus.BusPhyAddrStCd
                   || ' '
                   || bus.BusPhyAddrZipCd
                       AS Address,
                   bus.BusPhyAddrStCd
                       AS State,
                   bus.BusNm
                       AS Name,
                   bus.BusTrdNm
            FROM LOANAPP.LoanBorrTbl bor
                 JOIN LOANAPP.LoanBusTbl bus
                 ON (    bor.TaxId = bus.TaxId
                     AND bor.LoanAppNmb = bus.LoanAppNmb)
            WHERE     bor.BorrBusPerInd = 'B'
                  AND bor.LOANAPPNMB = p_LoanAppNmb
            UNION
            --Person type borrower's information if origination=true
            SELECT bor.*,
                      p.PerPhyAddrStr1NM
                   || ' '
                   || p.PerPhyAddrStr2Nm
                   || ' '
                   || p.PerPhyAddrCtyNm
                   || ' , '
                   || p.PerPhyAddrStCd
                   || ' '
                   || p.PerPhyAddrZipCd
                       AS Address,
                   p.PerPhyAddrStCd
                       AS State,
                      ocadataout (p.PerFirstNm)
                   || ' '
                   || CAST (p.PerInitialNm AS VARCHAR2 (2))
                   || ' '
                   || ocadataout (p.PerLastNm)
                   || ' '
                   || CAST (p.PerSfxNm AS VARCHAR2 (5))
                       AS Name,
                   NULL
                       AS BusTrdNm
            FROM loanapp.LoanBorrTbl bor
                 JOIN loanapp.LoanPerTbl p
                 ON (    bor.TAXID = p.TaxId
                     AND bor.LoanAppNmb = p.LoanAppNmb)
            WHERE     bor.BorrBusPerInd = 'P'
                  AND bor.LOANAPPNMB = p_LoanAppNmb;
    ELSE
        OPEN p_SelCur4 FOR
            --Business type borrower's information if origination=false
            SELECT bor.*,
                      bus.BusPhyAddrStr1NM
                   || ' '
                   || bus.BusPhyAddrStr2Nm
                   || ' '
                   || bus.BusPhyAddrCtyNm
                   || ','
                   || bus.BusPhyAddrStCd
                   || ' '
                   || bus.BusPhyAddrZipCd
                       AS Address,
                   bus.BusPhyAddrStCd
                       AS State,
                   bus.BusNm
                       AS Name,
                   bus.BusTrdNm
            FROM LOAN.LoanGntyBorrTbl bor
                 JOIN LOAN.BusTbl bus
                 ON (bor.TaxId = bus.TaxId)
            WHERE     bor.BorrBusPerInd = 'B'
                  AND bor.LOANAPPNMB = p_LoanAppNmb
            UNION
            --Person type borrower's information if origination=false
            SELECT bor.*,
                      p.PerPhyAddrStr1NM
                   || ' '
                   || p.PerPhyAddrStr2Nm
                   || ' '
                   || p.PerPhyAddrCtyNm
                   || ' , '
                   || p.PerPhyAddrStCd
                   || ' '
                   || p.PerPhyAddrZipCd
                       AS Address,
                   p.PerPhyAddrStCd
                       AS State,
                      ocadataout (p.PerFirstNm)
                   || ' '
                   || CAST (p.PerInitialNm AS VARCHAR2 (2))
                   || ' '
                   || ocadataout (p.PerLastNm)
                   || ' '
                   || CAST (p.PerSfxNm AS VARCHAR2 (5))
                       AS Name,
                   NULL
                       AS BusTrdNm
            FROM loan.LoanGntyBorrTbl bor
                 JOIN loan.PerTbl p
                 ON (bor.TAXID = p.TaxId)
            WHERE     bor.BorrBusPerInd = 'P'
                  AND bor.LOANAPPNMB = p_LoanAppNmb;
    END IF;


    -- If EPC transaction, then need to check if OC is guarantor. OC will always be a business.
    IF origstate = TRUE THEN
        p_LOANAPPEPCIND := loanapp_table_rec.LOANAPPEPCIND;

        IF (loanapp_table_rec.LOANAPPEPCIND = 'Y') THEN
            OPEN OCGuarantor;

            LOOP
                FETCH OCGuarantor INTO ocguarantor_rec;

                EXIT WHEN OCGuarantor%NOTFOUND;
            END LOOP;

            IF (OCGuarantor%ROWCOUNT > 0) THEN
                p_EPCText := 'Guarantor Operating Company:';
            END IF;

            CLOSE OCGuarantor;
        END IF;
    ELSE
        p_LOANAPPEPCIND := loan_table_rec.LOANAPPEPCIND;

        IF (loan_table_rec.LOANAPPEPCIND = 'Y') THEN
            OPEN OCGuarantor1;

            LOOP
                FETCH OCGuarantor1 INTO ocguarantor_rec1;

                EXIT WHEN OCGuarantor1%NOTFOUND;
            END LOOP;

            IF (OCGuarantor1%ROWCOUNT > 0) THEN
                p_EPCText := 'Guarantor Operating Company:';
            END IF;

            CLOSE OCGuarantor1;
        END IF;
    END IF;



    --List all borrowers identified in p_SelCur4 or p_SelCur5
    IF origstate = TRUE THEN
        DELETE FROM LOANAPP.Borr_Address;

        DELETE FROM LOANAPP.Borr_Insurance;

        OPEN LoanAppBorr;

        LOOP
            FETCH LoanAppBorr INTO LoanAppBorr_rec;

            EXIT WHEN LoanAppBorr%NOTFOUND;

            -- if borrower is in California
            IF (    LoanAppBorr_rec.State = 'CA'
                AND InCA = FALSE) THEN
                InCA := TRUE;
            END IF;

            IF (loanapp_table_rec.LoanAppEPCInd = 'Y') THEN
                IF (LoanAppBorr_rec.ImEPCOperCd = 2) THEN
                    Suffix := '(EPC)';
                ELSIF (LoanAppBorr_rec.ImEPCOperCd = 3) THEN
                    Suffix := '(Operating Concern)';
                END IF;
            END IF;

            IF (LoanAppBorr_rec.BusTrdNm IS NOT NULL) THEN
                TradeName := 'dba ' || LoanAppBorr_rec.BusTrdNm;
            ELSE
                TradeName := NULL;
            END IF;

            modified_Borr_Address :=
                LoanAppBorr_rec.Name || ' ' || TradeName || ' ' || Suffix || ' ' || LoanAppBorr_rec.Address;

            INSERT INTO LOANAPP.Borr_Address (Address)
            VALUES (modified_Borr_Address);

            COMMIT;

            --if any of insurance required for this borrower. Must be business.
            IF (LoanAppBorr_rec.BORRBUSPERIND = 'B') THEN
                --  dbms_output.put_line('Inside loop for BORRBUSPERIND as B'||LoanAppBorr_rec.LOANAPPNMB);
                IF (LoanAppBorr_rec.LiabInsurRqrdInd = 'Y') THEN
                    LiabilityInsurance := TRUE;
                END IF;

                IF (LoanAppBorr_rec.ProdLiabInsurRqrdInd = 'Y') THEN
                    ProductLiabilityInsurance := TRUE;
                END IF;

                IF (LoanAppBorr_rec.LiqLiabInsurRqrdInd = 'Y') THEN
                    DramShopInsurance := TRUE;
                END IF;

                IF (LoanAppBorr_rec.MalprctsInsurRqrdInd = 'Y') THEN
                    MalpracticeInsurance := TRUE;
                END IF;

                IF (LoanAppBorr_rec.WORKRSCOMPINSRQRDIND = 'Y') THEN
                    WorkersCompInsurance := TRUE;
                END IF;

                --  dbms_output.put_line('LoanAppBorr_rec.OthInsurRqrdInd: '||LoanAppBorr_rec.OthInsurRqrdInd);
                IF (LoanAppBorr_rec.OthInsurRqrdInd = 'Y') THEN
                    OtherInsurance := TRUE;

                    IF (InsuranceTypes IS NULL) THEN
                        InsuranceTypes := LoanAppBorr_rec.OthInsurDescTxt;

                        INSERT INTO LOANAPP.Borr_Insurance (Insurance)
                        VALUES (InsuranceTypes);

                        COMMIT;
                    --   dbms_output.put_line('InsuranceTypes inside if: '||InsuranceTypes);
                    ELSE
                        InsuranceTypes := InsuranceTypes || ', ' || LoanAppBorr_rec.OthInsurDescTxt;

                        INSERT INTO LOANAPP.Borr_Insurance (Insurance)
                        VALUES (InsuranceTypes);

                        COMMIT;
                    --   dbms_output.put_line('InsuranceTypes inside else: '||InsuranceTypes);
                    END IF;
                END IF;
            END IF;

            NumBorrowers := NumBorrowers + 1;
        --   dbms_output.put_line('LoanAppBorr_rec.Address: '||LoanAppBorr_rec.Address);
        --   dbms_output.put_line('NumBorrowers: '||NumBorrowers);
        END LOOP;

        CLOSE LoanAppBorr;

        OPEN p_SelCur6 FOR SELECT Address FROM LOANAPP.Borr_Address;
    --open p_SelCur7 for
    -- select Insurance from LOANAPP.Borr_Insurance;

    ELSE
        DELETE FROM LOANAPP.Borr_Address;

        DELETE FROM LOANAPP.Borr_Insurance;

        OPEN LoanBorr;

        LOOP
            FETCH LoanBorr INTO LoanBorr_rec;

            EXIT WHEN LoanBorr%NOTFOUND;

            --if borrower is in California
            IF (    LoanBorr_rec.State = 'CA'
                AND InCA = FALSE) THEN
                InCA := TRUE;
            END IF;

            IF (loan_table_rec.LoanAppEPCInd = 'Y') THEN
                IF (LoanBorr_rec.ImEPCOperCd = 2) THEN
                    Suffix := '(EPC)';
                ELSIF (LoanBorr_rec.ImEPCOperCd = 3) THEN
                    Suffix := '(Operating Concern)';
                END IF;
            END IF;

            IF (LoanBorr_rec.BusTrdNm IS NOT NULL) THEN
                TradeName := 'dba ' || LoanBorr_rec.BusTrdNm;
            ELSE
                TradeName := NULL;
            END IF;

            modified_Borr_Address :=
                LoanBorr_rec.Name || ' ' || TradeName || ' ' || Suffix || ' ' || LoanBorr_rec.Address;

            INSERT INTO LOANAPP.Borr_Address (Address)
            VALUES (modified_Borr_Address);

            COMMIT;

            -- dbms_output.put_line('LoanBorr_rec.Address: '||LoanBorr_rec.Address);
            --if any of insurance required for this borrower. Must be business.
            IF (LoanBorr_rec.BORRBUSPERIND = 'B') THEN
                IF (LoanBorr_rec.LiabInsurRqrdInd = 'Y') THEN
                    LiabilityInsurance := TRUE;
                END IF;

                IF (LoanBorr_rec.ProdLiabInsurRqrdInd = 'Y') THEN
                    ProductLiabilityInsurance := TRUE;
                END IF;

                IF (LoanBorr_rec.LiqLiabInsurRqrdInd = 'Y') THEN
                    DramShopInsurance := TRUE;
                END IF;

                IF (LoanBorr_rec.MalprctsInsurRqrdInd = 'Y') THEN
                    MalpracticeInsurance := TRUE;
                END IF;

                IF (LoanBorr_rec.WORKRSCOMPINSRQRDIND = 'Y') THEN
                    WorkersCompInsurance := TRUE;
                END IF;

                IF (LoanBorr_rec.OthInsurRqrdInd = 'Y') THEN
                    OtherInsurance := TRUE;

                    IF (InsuranceTypes IS NULL) THEN
                        InsuranceTypes := LoanBorr_rec.OthInsurDescTxt;

                        INSERT INTO LOANAPP.Borr_Insurance (Insurance)
                        VALUES (InsuranceTypes);

                        COMMIT;
                    ELSE
                        InsuranceTypes := InsuranceTypes || ', ' || LoanBorr_rec.OthInsurDescTxt;

                        INSERT INTO LOANAPP.Borr_Insurance (Insurance)
                        VALUES (InsuranceTypes);

                        COMMIT;
                    END IF;
                END IF;
            END IF;

            NumBorrowers := NumBorrowers + 1;
        END LOOP;

        CLOSE LoanBorr;

        OPEN p_SelCur6 FOR SELECT Address FROM LOANAPP.Borr_Address;
    --open p_SelCur7 for
    --select Insurance from LOANAPP.Borr_Insurance;
    END IF;

    -- If EPC/OC and if OC is guarantor, OC gets put into a separate column in authorization.
    IF origstate = TRUE THEN
        DELETE FROM LOANAPP.Guar_Address;

        DELETE FROM LOANAPP.Guar_Insurance;

        OPEN OCGuarantor;

        LOOP
            FETCH OCGuarantor INTO ocguarantor_rec;

            EXIT WHEN OCGuarantor%NOTFOUND;

            IF (ocguarantor_rec.BusTrdNm IS NOT NULL) THEN
                TradeName := 'dba ' || ocguarantor_rec.BusTrdNm;
            ELSE
                TradeName := NULL;
            END IF;

            modified_Guar_Address := ocguarantor_rec.Name || ' ' || TradeName || ' Guarantor';

            INSERT INTO LOANAPP.Guar_Address (Address)
            VALUES (modified_Guar_Address);

            COMMIT;

            IF (ocguarantor_rec.LiabInsurRqrdInd = 'Y') THEN
                LiabilityInsurance := TRUE;
            END IF;

            IF (ocguarantor_rec.ProdLiabInsurRqrdInd = 'Y') THEN
                ProductLiabilityInsurance := TRUE;
            END IF;

            IF (ocguarantor_rec.LiqLiabInsurRqrdInd = 'Y') THEN
                DramShopInsurance := TRUE;
            END IF;

            IF (ocguarantor_rec.MalprctsInsurRqrdInd = 'Y') THEN
                MalpracticeInsurance := TRUE;
            END IF;

            IF (ocguarantor_rec.WORKRSCOMPINSRQRDIND = 'Y') THEN
                WorkersCompInsurance := TRUE;
            END IF;

            IF (ocguarantor_rec.OthInsurRqrdInd = 'Y') THEN
                OtherInsurance := TRUE;

                IF (InsuranceTypes IS NULL) THEN
                    InsuranceTypes := ocguarantor_rec.OthInsurDescTxt;

                    INSERT INTO LOANAPP.Guar_Insurance (Insurance)
                    VALUES (InsuranceTypes);

                    COMMIT;
                ELSE
                    InsuranceTypes := InsuranceTypes || ', ' || ocguarantor_rec.OthInsurDescTxt;

                    INSERT INTO LOANAPP.Guar_Insurance (Insurance)
                    VALUES (InsuranceTypes);

                    COMMIT;
                END IF;
            END IF;
        -- dbms_output.put_line('ocguarantor_rec.Address: '||ocguarantor_rec.Address);
        END LOOP;

        CLOSE OCGuarantor;

        OPEN p_SelCur7 FOR SELECT Address FROM LOANAPP.Guar_Address;
    --open p_SelCur9 for
    --select Insurance from LOANAPP.Guar_Insurance;
    ELSE
        DELETE FROM LOANAPP.Guar_Address;

        DELETE FROM LOANAPP.Guar_Insurance;

        OPEN OCGuarantor1;

        LOOP
            FETCH OCGuarantor1 INTO ocguarantor_rec1;

            EXIT WHEN OCGuarantor1%NOTFOUND;

            IF (ocguarantor_rec1.BusTrdNm IS NOT NULL) THEN
                TradeName := 'dba ' || ocguarantor_rec1.BusTrdNm;
            ELSE
                TradeName := NULL;
            END IF;

            modified_Guar_Address := ocguarantor_rec1.Name || ' ' || TradeName || ' Guarantor';

            INSERT INTO LOANAPP.Guar_Address (Address)
            VALUES (modified_Guar_Address);

            COMMIT;

            IF (ocguarantor_rec1.LiabInsurRqrdInd = 'Y') THEN
                LiabilityInsurance := TRUE;
            END IF;

            IF (ocguarantor_rec1.ProdLiabInsurRqrdInd = 'Y') THEN
                ProductLiabilityInsurance := TRUE;
            END IF;

            IF (ocguarantor_rec1.LiqLiabInsurRqrdInd = 'Y') THEN
                DramShopInsurance := TRUE;
            END IF;

            IF (ocguarantor_rec1.MalprctsInsurRqrdInd = 'Y') THEN
                MalpracticeInsurance := TRUE;
            END IF;

            IF (ocguarantor_rec1.WORKRSCOMPINSRQRDIND = 'Y') THEN
                WorkersCompInsurance := TRUE;
            END IF;

            IF (ocguarantor_rec1.OthInsurRqrdInd = 'Y') THEN
                OtherInsurance := TRUE;

                IF (InsuranceTypes IS NULL) THEN
                    InsuranceTypes := ocguarantor_rec1.OthInsurDescTxt;

                    INSERT INTO LOANAPP.Guar_Insurance (Insurance)
                    VALUES (InsuranceTypes);

                    COMMIT;
                ELSE
                    InsuranceTypes := InsuranceTypes || ', ' || ocguarantor_rec1.OthInsurDescTxt;

                    INSERT INTO LOANAPP.Guar_Insurance (Insurance)
                    VALUES (InsuranceTypes);

                    COMMIT;
                END IF;
            END IF;

            EXIT WHEN OCGuarantor1%NOTFOUND;
        --dbms_output.put_line('ocguarantor_rec1.Address: '||ocguarantor_rec1.Address);
        END LOOP;

        CLOSE OCGuarantor1;

        OPEN p_SelCur7 FOR SELECT Address FROM LOANAPP.Guar_Address;
    --open p_SelCur9 for
    --select Insurance from LOANAPP.Guar_Insurance;
    END IF;

    --Get SubProcdTbl information
    IF origstate = TRUE THEN
        OPEN p_SelCur8 FOR SELECT subprcd.*
                           FROM LOANAPP.LOANSUBPROCDTBL subprcd
                           WHERE subprcd.LOANAPPNMB = p_LoanAppNmb;
    ELSE
        OPEN p_SelCur8 FOR SELECT subprcd.*
                           FROM LOAN.LOANGNTYSUBPROCDTBL subprcd
                           WHERE subprcd.LOANAPPNMB = p_LoanAppNmb;
    END IF;

    -- Determine if any Use of Proceeds are for debt refinancing
    IF origstate = TRUE THEN
        OPEN LOANSUBPROCD_15;

        LOOP
            FETCH LOANSUBPROCD_15 INTO LOANSUBPROCD_rec15;

            EXIT WHEN LOANSUBPROCD_15%NOTFOUND;

            IF LOANSUBPROCD_15%FOUND THEN
                p_REFIOPTION := 'and refinancing';
            END IF;
        END LOOP;

        CLOSE LOANSUBPROCD_15;
    ELSE
        OPEN LOANGNTYSUBPROCD_15;

        LOOP
            FETCH LOANGNTYSUBPROCD_15 INTO LOANGNTYSUBPROCD_rec15;

            EXIT WHEN LOANGNTYSUBPROCD_15%NOTFOUND;

            IF LOANGNTYSUBPROCD_15%FOUND THEN
                p_REFIOPTION := 'and refinancing';
            END IF;
        END LOOP;

        CLOSE LOANSUBPROCD_15;
    END IF;

    --Get SubProcdTbl information for LOANPROCDTYPCD in ('01','02','04','10','15','19')
    IF origstate = TRUE THEN
        OPEN p_SelCur9 FOR SELECT *
                           FROM LOANAPP.LOANSUBPROCDTBL
                           WHERE     LOANAPPNMB = p_LoanAppNMb
                                 AND PROCDTYPCD = 'E'
                                 AND LOANPROCDTYPCD IN ('01',
                                                        '02',
                                                        '04',
                                                        '10',
                                                        '15',
                                                        '19')
                           ORDER BY LOANPROCDADDR DESC, LOANPROCDTYPCD ASC;
    ELSE
        OPEN p_SelCur9 FOR SELECT *
                           FROM LOAN.LOANGNTYSUBPROCDTBL
                           WHERE     LOANAPPNMB = p_LoanAppNMb
                                 AND PROCDTYPCD = 'E'
                                 AND LOANPROCDTYPCD IN ('01',
                                                        '02',
                                                        '04',
                                                        '10',
                                                        '15',
                                                        '19')
                           ORDER BY LOANPROCDADDR DESC, LOANPROCDTYPCD ASC;
    END IF;

    --Check each Procd
    IF origstate = TRUE THEN
        p_total_LOANPROCDAMT := 0;

        OPEN LOANPROCD;

        LOOP
            FETCH LOANPROCD INTO LOANPROCD_rec;

            EXIT WHEN LOANPROCD%NOTFOUND;
            --get total project costs by summing all uses of proceeds
            p_total_LOANPROCDAMT := nvl(p_total_LOANPROCDAMT,0) + nvl(LOANPROCD_rec.LOANPROCDAMT,0);

            IF (   LOANPROCD_rec.LOANPROCDTYPCD = '01'
                OR LOANPROCD_rec.LOANPROCDTYPCD = '02'
                OR LOANPROCD_rec.LOANPROCDTYPCD = '04'
                OR LOANPROCD_rec.LOANPROCDTYPCD = '10') THEN
                /* Purchase Land Only */
                CASE
                    WHEN (LOANPROCD_rec.LOANPROCDTYPCD = '01') THEN
                        p_ProceedText := 'Purchase Land';
                    /* Purchase Land and Existing Building */
                    WHEN (LOANPROCD_rec.LOANPROCDTYPCD = '02') THEN
                        p_ProceedText := 'Purchase Land ' || CHR (38) || ' Building';
                    /* Building (Construction, Remodeling, L/H improvement, etc.)  */
                    WHEN (LOANPROCD_rec.LOANPROCDTYPCD = '05') THEN
                        p_ProceedText := 'Construction/Remodeling';
                    /* Machinery and Equipment (purchase, installation, etc.) */
                    WHEN (LOANPROCD_rec.LOANPROCDTYPCD = '10') THEN
                        p_ProceedText := 'Purchase/Install Equipment';
                    ELSE
                        NULL;
                END CASE;

                p_LoanProcdAmt := LOANPROCD_rec.LoanProcdAmt;
            /*elsif(LOANPROCD_rec.LOANPROCDTYPCD='15') then
                open LOANSUBPROCD_15;
                loop
                    fetch LOANSUBPROCD_15 into LOANSUBPROCD_rec15;
                    exit when LOANSUBPROCD_15%notfound;
                end loop;
                close LOANSUBPROCD_15;
                p_LoanProcdAmt :=  LOANPROCD_rec.LoanProcdAmt;*/
            ELSIF (   LOANPROCD_rec.LOANPROCDTYPCD = '15'
                   OR LOANPROCD_rec.LOANPROCDTYPCD = '19'
                   OR LOANPROCD_rec.LOANPROCDTYPCD = '21'
                   OR LOANPROCD_rec.LOANPROCDTYPCD = '22') THEN
                /* Eligible business expenses under Debt Refinancing: New use
                of proceeds introduced with revised 1244 Form.  */

                CASE
                    WHEN (LOANPROCD_rec.LOANPROCDTYPCD = '19') THEN
                        p_ProceedText := 'Eligible Business Expenses under Debt Refinancing';
                    /* Other Expenses (eligible contingency expenses, interim interest, etc.) */

                    WHEN (LOANPROCD_rec.LOANPROCDTYPCD = '21') THEN
                        p_ProceedText := 'Other Expenses (construction contingencies, interim interest)';
                    /* Professional Fees (appraiser, architect, legal, etc.)  */
                    WHEN (LOANPROCD_rec.LOANPROCDTYPCD = '22') THEN
                        p_ProceedText := 'Professional Fees';
                    ELSE
                        NULL;
                END CASE;

                p_LoanProcdAmt := LOANPROCD_rec.LoanProcdAmt;
            END IF;
        END LOOP;

        CLOSE LOANPROCD;
    ELSIF origstate = FALSE THEN
        p_total_LOANPROCDAMT := 0;

        OPEN LOANGNTYPROCD;

        LOOP
            FETCH LOANGNTYPROCD INTO LOANGNTYPROCD_rec;

            EXIT WHEN LOANGNTYPROCD%NOTFOUND;
            --get total project costs by summing all uses of proceeds
            p_total_LOANPROCDAMT := nvl(p_total_LOANPROCDAMT,0) + nvl(LOANGNTYPROCD_rec.LOANPROCDAMT,0);

            IF (   LOANGNTYPROCD_rec.LOANPROCDTYPCD = '01'
                OR LOANGNTYPROCD_rec.LOANPROCDTYPCD = '02'
                OR LOANGNTYPROCD_rec.LOANPROCDTYPCD = '04'
                OR LOANGNTYPROCD_rec.LOANPROCDTYPCD = '10') THEN
                /* Purchase Land Only */
                CASE
                    WHEN (LOANGNTYPROCD_rec.LOANPROCDTYPCD = '01') THEN
                        p_ProceedText := 'Purchase Land';
                    /* Purchase Land and Existing Building */
                    WHEN (LOANGNTYPROCD_rec.LOANPROCDTYPCD = '02') THEN
                        p_ProceedText := 'Purchase Land ' || CHR (38) || ' Building';
                    /* Building (Construction, Remodeling, L/H improvement, etc.)  */
                    WHEN (LOANGNTYPROCD_rec.LOANPROCDTYPCD = '04') THEN
                        p_ProceedText := 'Construction/Remodeling';
                    /* Machinery and Equipment (purchase, installation, etc.) */
                    WHEN (LOANGNTYPROCD_rec.LOANPROCDTYPCD = '10') THEN
                        p_ProceedText := 'Purchase/Install Equipment';
                    ELSE
                        NULL;
                END CASE;

                p_LoanProcdAmt := LOANGNTYPROCD_rec.LoanProcdAmt;
            /*elsif(LOANPROCD_rec.LOANPROCDTYPCD='15') then
                open LOANSUBPROCD_15;
                loop
                    fetch LOANSUBPROCD_15 into LOANSUBPROCD_rec15;
                    exit when LOANSUBPROCD_15%notfound;
                end loop;
                close LOANSUBPROCD_15;
                p_LoanProcdAmt :=  LOANPROCD_rec.LoanProcdAmt;*/
            ELSIF (   LOANGNTYPROCD_rec.LOANPROCDTYPCD = '15'
                   OR LOANGNTYPROCD_rec.LOANPROCDTYPCD = '19'
                   OR LOANGNTYPROCD_rec.LOANPROCDTYPCD = '21'
                   OR LOANGNTYPROCD_rec.LOANPROCDTYPCD = '22') THEN
                /* Eligible business expenses under Debt Refinancing: New use
                of proceeds introduced with revised 1244 Form.  */

                CASE
                    WHEN (LOANGNTYPROCD_rec.LOANPROCDTYPCD = '19') THEN
                        p_ProceedText := 'Eligible Business Expenses under Debt Refinancing';
                    /* Other Expenses (eligible contingency expenses, interim interest, etc.) */

                    WHEN (LOANGNTYPROCD_rec.LOANPROCDTYPCD = '21') THEN
                        p_ProceedText := 'Other Expenses (construction contingencies, interim interest)';
                    /* Professional Fees (appraiser, architect, legal, etc.)  */
                    WHEN (LOANGNTYPROCD_rec.LOANPROCDTYPCD = '22') THEN
                        p_ProceedText := 'Professional Fees';
                    ELSE
                        NULL;
                END CASE;

                p_LoanProcdAmt := LOANGNTYPROCD_rec.LoanProcdAmt;
            END IF;
        END LOOP;

        CLOSE LOANGNTYPROCD;
    END IF;


    --Get Text information as per sample
    IF origstate = TRUE THEN
        OPEN p_SelCur10 FOR SELECT CASE
                                       WHEN lp.LOANPROCDTYPCD = '01' THEN
                                           'Purchase Land'
                                       WHEN slp.LOANPROCDTYPCD = '02' THEN
                                           'Purchase Land ' || CHR (38) || ' Building'
                                       WHEN slp.LOANPROCDTYPCD = '04' THEN
                                           'Construction/Remodeling'
                                       WHEN slp.LOANPROCDTYPCD = '10' THEN
                                           'Purchase/Install Equipment'
                                       WHEN slp.LOANPROCDTYPCD = '19' THEN
                                           'Eligible Business Expenses under Debt Refinancing'
                                       WHEN slp.LOANPROCDTYPCD = '21' THEN
                                           'Other Expenses (construction contingencies, interim interest)'
                                       WHEN slp.LOANPROCDTYPCD = '22' THEN
                                           'Professional Fees'
                                       WHEN slp.LOANPROCDTYPCD = '03' THEN
                                           (SELECT LOANPROCDTYPDESCTXT
                                            FROM SBAREF.LOANPROCDTYPTBL
                                            WHERE     PROCDTYPCD = 'E'
                                                  AND LOANPROCDTYPCD = '03')
                                       WHEN slp.LOANPROCDTYPCD = '05' THEN
                                           (SELECT LOANPROCDTYPDESCTXT
                                            FROM SBAREF.LOANPROCDTYPTBL
                                            WHERE     PROCDTYPCD = 'E'
                                                  AND LOANPROCDTYPCD = '05')
                                       WHEN slp.LOANPROCDTYPCD = '06' THEN
                                           (SELECT LOANPROCDTYPDESCTXT
                                            FROM SBAREF.LOANPROCDTYPTBL
                                            WHERE     PROCDTYPCD = 'E'
                                                  AND LOANPROCDTYPCD = '06')
                                       WHEN slp.LOANPROCDTYPCD = '09' THEN
                                           (SELECT LOANPROCDTYPDESCTXT
                                            FROM SBAREF.LOANPROCDTYPTBL
                                            WHERE     PROCDTYPCD = 'E'
                                                  AND LOANPROCDTYPCD = '09')
                                       WHEN slp.LOANPROCDTYPCD = '11' THEN
                                           (SELECT LOANPROCDTYPDESCTXT
                                            FROM SBAREF.LOANPROCDTYPTBL
                                            WHERE     PROCDTYPCD = 'E'
                                                  AND LOANPROCDTYPCD = '11')
                                       WHEN slp.LOANPROCDTYPCD = '15' THEN
                                           (SELECT LOANPROCDTYPDESCTXT
                                            FROM SBAREF.LOANPROCDTYPTBL
                                            WHERE     PROCDTYPCD = 'E'
                                                  AND LOANPROCDTYPCD = '15')
                                   END
                                       AS LOANPROCDTYPDESC,
                                   NVL (lp.LOANPROCDAMT, 0)
                                       LOANPROCDAMT
                            FROM SBAREF.LOANPROCDTYPTBL slp
                                 LEFT OUTER JOIN LOANAPP.LOANPROCDTBL lp
                                 ON     lp.PROCDTYPCD = slp.PROCDTYPCD
                                    AND lp.LOANPROCDTYPCD = slp.LOANPROCDTYPCD
                                    AND lp.LOANAPPNMB = p_LoanAppNmb
                            WHERE slp.PROCDTYPCD = 'E';
    ELSE
        OPEN p_SelCur10 FOR SELECT CASE
                                       WHEN lp.LOANPROCDTYPCD = '01' THEN
                                           'Purchase Land'
                                       WHEN slp.LOANPROCDTYPCD = '02' THEN
                                           'Purchase Land ' || CHR (38) || ' Building'
                                       WHEN slp.LOANPROCDTYPCD = '04' THEN
                                           'Construction/Remodeling'
                                       WHEN slp.LOANPROCDTYPCD = '10' THEN
                                           'Purchase/Install Equipment'
                                       WHEN slp.LOANPROCDTYPCD = '19' THEN
                                           'Eligible Business Expenses under Debt Refinancing'
                                       WHEN slp.LOANPROCDTYPCD = '21' THEN
                                           'Other Expenses (construction contingencies, interim interest)'
                                       WHEN slp.LOANPROCDTYPCD = '22' THEN
                                           'Professional Fees'
                                       WHEN slp.LOANPROCDTYPCD = '03' THEN
                                           (SELECT LOANPROCDTYPDESCTXT
                                            FROM SBAREF.LOANPROCDTYPTBL
                                            WHERE     PROCDTYPCD = 'E'
                                                  AND LOANPROCDTYPCD = '03')
                                       WHEN slp.LOANPROCDTYPCD = '05' THEN
                                           (SELECT LOANPROCDTYPDESCTXT
                                            FROM SBAREF.LOANPROCDTYPTBL
                                            WHERE     PROCDTYPCD = 'E'
                                                  AND LOANPROCDTYPCD = '05')
                                       WHEN slp.LOANPROCDTYPCD = '06' THEN
                                           (SELECT LOANPROCDTYPDESCTXT
                                            FROM SBAREF.LOANPROCDTYPTBL
                                            WHERE     PROCDTYPCD = 'E'
                                                  AND LOANPROCDTYPCD = '06')
                                       WHEN slp.LOANPROCDTYPCD = '09' THEN
                                           (SELECT LOANPROCDTYPDESCTXT
                                            FROM SBAREF.LOANPROCDTYPTBL
                                            WHERE     PROCDTYPCD = 'E'
                                                  AND LOANPROCDTYPCD = '09')
                                       WHEN slp.LOANPROCDTYPCD = '11' THEN
                                           (SELECT LOANPROCDTYPDESCTXT
                                            FROM SBAREF.LOANPROCDTYPTBL
                                            WHERE     PROCDTYPCD = 'E'
                                                  AND LOANPROCDTYPCD = '11')
                                       WHEN slp.LOANPROCDTYPCD = '15' THEN
                                           (SELECT LOANPROCDTYPDESCTXT
                                            FROM SBAREF.LOANPROCDTYPTBL
                                            WHERE     PROCDTYPCD = 'E'
                                                  AND LOANPROCDTYPCD = '15')
                                   END
                                       AS LOANPROCDTYPDESC,
                                   NVL (lp.LOANPROCDAMT, 0)
                                       LOANPROCDAMT
                            FROM SBAREF.LOANPROCDTYPTBL slp
                                 LEFT OUTER JOIN LOAN.LOANGNTYPROCDTBL lp
                                 ON     lp.PROCDTYPCD = slp.PROCDTYPCD
                                    AND lp.LOANPROCDTYPCD = slp.LOANPROCDTYPCD
                                    AND lp.LOANAPPNMB = p_LoanAppNmb
                            WHERE slp.PROCDTYPCD = 'E';
    
    END IF;
    
    /*Total 3rd loans equals sum of all 3rd party loans that are permanent financing.*/
    IF origstate=true THEN
                p_LoanPartLendrAmt :=0;
                open LoanThirdPartySet;
                LOOP 
                    fetch LoanThirdPartySet INTO LoanThirdPartySet_rec;
                    EXIT WHEN LoanThirdPartySet%notfound;
                    IF LoanThirdPartySet_rec.LoanPartLendrTypCd='P' THEN
                        p_LoanPartLendrAmt := nvl(p_LoanPartLendrAmt,0) + nvl(LoanThirdPartySet_rec.LoanPartLendrAmt,0);
                        PreExistingLoanAmt := nvl(LoanThirdPartySet_rec.LOANLENDRGROSSAMT,0) - nvl(LoanThirdPartySet_rec.LOANPARTLENDRAMT,0);
                        p_D4B_PermLoanRow := 'a.	'||LoanThirdPartySet_rec.LoanPartLendrNm||' in the principal amount of '||LoanThirdPartySet_rec.LoanPartLendrAmt;
                        IF (PreExistingLoanAmt > 0 ) THEN 
                            p_D4C_Preexisting3rdPartyLoan := 'In addition to this amount, the Third Party Lender mortgage will include a pre-existing non-project debt in the amount of '||PreExistingLoanAmt;
                        END IF;
                    END IF;
                END LOOP;
                CLOSE LoanThirdPartySet;
    ELSE
                p_LoanPartLendrAmt :=0;
                open LoanGntyThirdPartySet;
                LOOP 
                    fetch LoanGntyThirdPartySet INTO LoanGntyThirdPartySet_rec;
                    EXIT WHEN LoanGntyThirdPartySet%notfound;
                    IF LoanGntyThirdPartySet_rec.LoanPartLendrTypCd='P' THEN
                        p_LoanPartLendrAmt := nvl(p_LoanPartLendrAmt,0) + nvl(LoanGntyThirdPartySet_rec.LoanPartLendrAmt,0);
                        PreExistingLoanAmt := nvl(LoanGntyThirdPartySet_rec.LOANLENDRGROSSAMT,0) - nvl(LoanGntyThirdPartySet_rec.LOANPARTLENDRAMT,0);
                        p_D4B_PermLoanRow := 'a.	'||LoanGntyThirdPartySet_rec.LoanPartLendrNm||' in the principal amount of '||LoanGntyThirdPartySet_rec.LoanPartLendrAmt;
                        IF (PreExistingLoanAmt > 0 ) THEN 
                            p_D4C_Preexisting3rdPartyLoan := 'In addition to this amount, the Third Party Lender mortgage will include a pre-existing non-project debt in the amount of '||PreExistingLoanAmt;
                        END IF;
                    END IF;
                END LOOP;
                CLOSE LoanGntyThirdPartySet;
    END IF;
    IF origstate=true THEN
        p_NetDebenturePercent := round((nvl(OrignationRec_rec.LOANAPPCDCNETDBENTRAMT,0) / nvl(p_total_LOANPROCDAMT,0)) * 100,2);
        --p_ClosingCosts := least(OrignationRec_rec.LOANAPPCDCCLSCOSTAMT, 2500);
        p_TotalClosing := nvl(OrignationRec_rec.LOANAPPCDCCLSCOSTAMT,0) + nvl(OrignationRec_rec.LOANAPPCDCOTHCLSCOSTAMT,0);
        /* Pay Processing Fee upfront. */
        p_SubTotalText := 'Subtotal(b.1 through b.5)';
        ProcFeeToBePaid := OrignationRec_rec.LoanAppCDCPrcsFeeAmt;
        IF (OrignationRec_rec.SEPRATECLOSINGCOSTIND='Y') THEN
            ProcFeeToBePaid := 0;
            p_UnderwriterText := 'If the CDC Processing fee is paid for by the Borrower and the Borrower is not to be reimbursed, 
                                b.3 is added to the sum of a. and b.5 Subtotal when computing the Underwriters Fee.';
            p_SubTotalText := 'Subtotal(b.1, b.2 and b.4 because borrower pays for b.3.)';
        END IF;
        /* Pay Closing Costs upfront */
        ClosingCostToBePaid := p_TotalClosing;
        IF (OrignationRec_rec.SEPRATECLOSINGCOSTIND='Y') THEN
            ClosingCostToBePaid := 0;
            IF (OrignationRec_rec.LOANAPPCDCSEPRTPRCSFEEIND ='Y') THEN
                p_SubTotalText := 'Subtotal(b.1 and b.2 because borrower pays 
                                for b.3 and b.4)';
            ELSE
                p_SubTotalText := 'Subtotal(b.1, b.2 and b.3 because borrower pays for b.4)';
            END IF;
        END IF;
        p_SubtotalAdminCosts := nvl(OrignationRec_rec.LoanAppCDCGntyAmt,0)
                                + nvl(OrignationRec_rec.LoanAppCDCFundFeeAmt,0)
                                + nvl(OrignationRec_rec.LoanAppCDCPrcsFeeAmt,0)
                                + nvl(p_TotalClosing,0);
        p_TotalDebentureAdmin := nvl(p_SubtotalAdminCosts,0) + 
                                nvl(OrignationRec_rec.LOANAPPCDCUNDRWTRFEEAMT,0);
        CASE WHEN (OrignationRec_rec.DISBDEADLNDT is not null and OrignationRec_rec.LOANINITLAPPVDT is not null) THEN
            p_DisburseMonths := to_char(MONTHS_BETWEEN(OrignationRec_rec.DISBDEADLNDT,OrignationRec_rec.LOANINITLAPPVDT));
        WHEN (OrignationRec_rec.PRCSMTHDCD != '5RE' or OrignationRec_rec.PRCSMTHDCD != '5RX') THEN
            p_DisburseMonths := '48';
            p_AnnualGntyFeePct := 0.4865;
        WHEN (OrignationRec_rec.PRCSMTHDCD = '5RE' or OrignationRec_rec.PRCSMTHDCD = '5RX') THEN
            p_DisburseMonths := '9';
            p_AnnualGntyFeePct := 0.4517;
        END CASE;
        OPEN p_SelCur11 for
                    SELECT * FROM LOANAPP.LOANAPPTBL where LOANAPPNMB=p_LoanAppNmb;   
                                



    ELSE
        p_NetDebenturePercent := round((nvl(ServicingRec_rec.LOANAPPCDCNETDBENTRAMT,0) / nvl(p_total_LOANPROCDAMT,0)) * 100,2);
        --p_ClosingCosts := least(ServicingRec_rec.LOANAPPCDCCLSCOSTAMT, 2500);
        p_TotalClosing := nvl(ServicingRec_rec.LOANAPPCDCCLSCOSTAMT,0) + nvl(ServicingRec_rec.LOANAPPCDCOTHCLSCOSTAMT,0);
        /* Pay Processing Fee upfront. */
        p_SubTotalText := 'Subtotal(b.1 through b.5)';
        ProcFeeToBePaid := ServicingRec_rec.LoanAppCDCPrcsFeeAmt;
        IF (ServicingRec_rec.LOANAPPCDCOTHCLSCOSTAMT > 0) THEN
            ProcFeeToBePaid := 0;
            p_UnderwriterText := 'If the CDC Processing fee is paid for by the Borrower and the Borrower is not to be reimbursed, 
                                b.3 is added to the sum of a. and b.5 Subtotal when computing the Underwriters Fee.';
            p_SubTotalText := 'Subtotal(b.1, b.2 and b.4 because borrower pays for b.3.)';
        END IF;
        /* Pay Closing Costs upfront */
        ClosingCostToBePaid := p_TotalClosing;
        IF (ServicingRec_rec.LOANAPPCDCOTHCLSCOSTAMT > 0) THEN
            ClosingCostToBePaid := 0;
            IF (ServicingRec_rec.LOANAPPCDCSEPRTPRCSFEEIND ='Y') THEN
                p_SubTotalText := 'Subtotal(b.1 and b.2 because borrower pays 
                                for b.3 and b.4)';
            ELSE
                p_SubTotalText := 'Subtotal(b.1, b.2 and b.3 because borrower pays for b.4)';
            END IF;
        END IF;


        p_SubtotalAdminCosts := nvl(ServicingRec_rec.LoanAppCDCGntyAmt,0)
                        + nvl(ServicingRec_rec.LoanAppCDCFundFeeAmt,0)
                        + nvl(ServicingRec_rec.LoanAppCDCPrcsFeeAmt,0)
                        + nvl(p_TotalClosing,0);
        p_TotalDebentureAdmin := p_SubtotalAdminCosts 
                        + nvl(ServicingRec_rec.LOANAPPCDCUNDRWTRFEEAMT,0);
                        
        CASE WHEN (ServicingRec_rec.DISBDEADLNDT is not null and ServicingRec_rec.LOANINITLAPPVDT is not null) THEN
            p_DisburseMonths := to_char(MONTHS_BETWEEN(ServicingRec_rec.DISBDEADLNDT,ServicingRec_rec.LOANINITLAPPVDT));
        WHEN (ServicingRec_rec.PRCSMTHDCD != '5RE' or ServicingRec_rec.PRCSMTHDCD != '5RX') THEN
            p_DisburseMonths := '48';
        WHEN (ServicingRec_rec.PRCSMTHDCD = '5RE' or ServicingRec_rec.PRCSMTHDCD = '5RX') THEN
            p_DisburseMonths := '9';
        END CASE;  
        OPEN p_SelCur11 for
            SELECT * FROM LOAN.LOANGNTYTBL where LOANAPPNMB=p_LoanAppNmb;

                        
    END IF;
    
    IF p_EPCText = 'Guarantor Operating Company:' THEN
        p_OCtext := ', Operating Company';
    END IF;
    
       /*Interim Loan */
       p_LoanInterimLoanSet_flag :=0;
    IF origstate=true THEN
                OPEN LoanThirdPartySet;
                LOOP 
                    FETCH LoanThirdPartySet INTO LoanInterimLoanSet_rec;
                    EXIT WHEN LoanThirdPartySet%notfound;
                    IF LoanInterimLoanSet_rec.LoanPartLendrTypCd='I' THEN
                        p_LoanInterimLoanSet_flag :=1;
                        v_TotalInterimAmt := nvl(v_TotalInterimAmt,0) + nvl(LoanInterimLoanSet_rec.LoanPartLendrAmt,0);
                    END IF;
                END LOOP;
                CLOSE LoanThirdPartySet;
    ELSE
                OPEN LoanGntyThirdPartySet;
                LOOP 
                    FETCH LoanGntyThirdPartySet INTO LoanGntyInterimLoanSet_rec;
                    EXIT WHEN LoanGntyThirdPartySet%notfound;
                    IF LoanGntyInterimLoanSet_rec.LoanPartLENDrTypCd='I' THEN
                        p_LoanInterimLoanSet_flag :=1;
                        v_TotalInterimAmt := nvl(v_TotalInterimAmt,0) + nvl(LoanGntyInterimLoanSet_rec.LoanPartLENDrAmt,0);
                    END IF;
                END LOOP;
                CLOSE LoanGntyThirdPartySet;
    END IF;
    
    IF p_LoanInterimLoanSet_flag = 1 THEN
        p_D2A_InterimLoanStart := 'Interim Financing (paid off by the Debenture):
                                 a. Interim Lender: An interim loan in the total principal amount of '||v_TotalInterimAmt||' will be provided by the following lender(s) ("Interim Lender"):';
        p_TotalInterimAmt := v_TotalInterimAmt;
         IF origstate=true THEN
                DELETE FROM LOANAPP.LoanPartLENDrTbl_Temp;
                OPEN LoanThirdPartySet;
                    LOOP
                        FETCH LoanThirdPartySet INTO LoanThirdPartySet_rec;
                        EXIT WHEN LoanThirdPartySet%notfound;
                        IF LoanThirdPartySet_rec.LoanPartLENDrTypCd='I' THEN  
                            p_D2B_InterimLoanRow := '(1)	'||LoanThirdPartySet_rec.LoanPartLendrNm||' in the principal amount of '||LoanThirdPartySet_rec.LoanPartLendrAmt;
                            
                            
                            insert INTO LOANAPP.LoanPartLENDrTbl_Temp(LOANAPPNMB,LOANPARTLENDRTYPCD,LOANPARTLENDRAMT,LOCID,LOANPARTLENDRNM)
                            VALUES(p_LOANAPPNMB,LoanThirdPartySet_rec.LOANPARTLENDRTYPCD,LoanThirdPartySet_rec.LOANPARTLENDRAMT,
                            LoanThirdPartySet_rec.LOCID,LoanThirdPartySet_rec.LOANPARTLENDRNM);
                            COMMIT;
                        END IF;            
                    END LOOP;
                    CLOSE LoanThirdPartySet;
                    OPEN p_SelCur12 for
                    SELECT * FROM LOANAPP.LoanPartLENDrTbl_Temp;           
            ELSE
                DELETE FROM LOANAPP.LoanPartLENDrTbl_Temp;
                OPEN LoanGntyThirdPartySet;
                    LOOP
                        FETCH LoanGntyThirdPartySet INTO LoanGntyThirdPartySet_rec;
                        EXIT WHEN LoanGntyThirdPartySet%notfound;
                        IF LoanGntyThirdPartySet_rec.LoanPartLENDrTypCd='I' THEN     
                            p_D2B_InterimLoanRow := '(1)	'||LoanGntyThirdPartySet_rec.LoanPartLendrNm||' in the principal amount of '||LoanGntyThirdPartySet_rec.LoanPartLendrAmt;
                            

                            insert INTO LOANAPP.LoanPartLENDrTbl_Temp(LOANAPPNMB,LOANPARTLENDRTYPCD,LOANPARTLENDRAMT,LOCID,LOANPARTLENDRNM)
                            VALUES(p_LOANAPPNMB,LoanGntyThirdPartySet_rec.LOANPARTLENDRTYPCD,LoanGntyThirdPartySet_rec.LOANPARTLENDRAMT,
                            LoanGntyThirdPartySet_rec.LOCID,LoanGntyThirdPartySet_rec.LOANPARTLENDRNM);
                            COMMIT;
                        END IF;
                      
                    END LOOP;
                    CLOSE LoanGntyThirdPartySet;
                    OPEN p_SelCur12 for
                    SELECT * FROM LOANAPP.LoanPartLENDrTbl_Temp; 
            END IF;
        p_D2C_InterimLoanEnd := 'Application of Net Debenture Proceeds to Interim Loan: Upon sale of the Debenture, the Net Debenture Proceeds (the portion of Debenture Proceeds that finance Project Cost) 
                               will be applied to pay off the balance of the interim loan. If the Interim Lender is also the Third Party Lender, this payment will reduce the total balance owed to Third Party 
                               Lender to the amount specified in Paragraph XX. below.
                               c. Required Certifications Before 504 Loan Closing: Following completion of the Project, but no earlier than the 5th day of the month prior to the month in which the CDC submits 
                               this loan to SBA for debenture funding, CDC must cause Interim Lender to certify the amount of the interim loan disbursed, that the interim loan has been disbursed in reasonable 
                               compliance with this Authorization, and that it has no knowledge of any unremedied substantial adverse change in the condition of the Borrower and Operating Company since the date 
                               of the loan application to the Interim Lender.';
    ELSE
        p_D3_Escrow := 'Escrow Closing (No Interim Financing):
                        The project will not require interim financing. The following will be required for an escrow closing:
                        a. Escrow Account:
                        (1) SBA must approve the escrow agreement. Escrow agent must be approved by CDC and SBA and follow escrow instructions provided by CDC and SBA.
                        (2) Debenture sale funds must be wired directly into the escrow account. 
                        (3) The funds in escrow may not be distributed and the escrow account may not be dissolved until SBA is satisfied that all collateral documents have been properly filed, lien positions 
                            properly perfected, and a final title policy (or other evidence of title if no title policy is required) issued showing required title and lien position.
                        (4) The escrow must close no later than 5 months from the date of the Debenture sale.
                        If it does not, the escrow funds will be used to pay off the Debenture in full.
                        (5) Borrower must deposit an additional 10% of the total project costs. This deposit must be in cash or an irrevocable Letter of Credit.
                        b. Borrower must agree in writing:
                        (1) That the required deposit must be used to make up any deficiency in the escrow account due to costs associated with the Debenture sale;
                        (2) To pay all costs of prepaying the Debenture if any lien position required by this Authorization is not perfected in a reasonable time (not over 5 months) after disbursement of Debenture 
                            proceeds including:
                        (a) Costs associated with the Debenture sale including any CDC processing fee not paid by SBA, CSA fee, CDC attorney fee/closing costs, SBA Guarantee Fee, Funding Fee, and Underwriter�s Fee.
                        (b) The cost of prepaying a Debenture including the prepayment premium and 6 months worth of interest on the original Debenture amount.';


    END IF; 
    p_ThirdPartyPercent := round((nvl(p_LoanPartLendrAmt,0)/ nvl(p_total_LOANPROCDAMT,0)) * 100,2);

    /* Borrower's Cntrobution */
    IF origstate=true THEN
    p_LoanAppContribAmt := OrignationRec_rec.LoanAppContribAmt;
    p_LoanAppContribPct := OrignationRec_rec.LoanAppContribPct;
    ELSE
    p_LoanAppContribAmt := ServicingRec_rec.LoanAppContribAmt;
    p_LoanAppContribPct := ServicingRec_rec.LoanAppContribPct;
    END IF;
    
    --Frank's queries
    IF origstate = TRUE THEN
        OPEN p_SelCur13 FOR SELECT Collat.*,
                        ROWNUM
                FROM LoanApp.LoanCollatTbl Collat
                WHERE LOANAPPNMB = p_LOANAPPNMB
                        and Collat.LOANAPPNMB NOT IN ( 
                                SELECT       LOANAPPNMB 
                                FROM   LoanApp.LoanLmtGntyCollatTbl LmtGnty
                                WHERE LmtGnty.LOANAPPNMB = Collat.LOANAPPNMB )
                ORDER BY LOANCOLLATSEQNMB, LOANCOLLATSUBTYPCD, LOANCOLLATTYPCD;
                DELETE FROM LOANAPP.LOANCOLLATLIEN_TEMP;
                OPEN LOANCOLLAT;
                    LOOP
                        FETCH LOANCOLLAT INTO LOANCOLLAT_rec;
                        EXIT WHEN LOANCOLLAT%notfound;                            
                            insert INTO LOANCOLLATLIEN_TEMP
                            select LOANCOLLATLIEN.*,  ROWNUM
                            from LOANAPP.LOANCOLLATLIENTBL LOANCOLLATLIEN
                            where LOANAPPNMB = LOANCOLLAT_rec.LOANAPPNMB and LoanCollatSeqNmb = LOANCOLLAT_rec.LoanCollatSeqNmb;
                            COMMIT;                           
                    END LOOP;
        OPEN p_SelCur14 FOR SELECT * FROM LOANAPP.LOANCOLLATLIEN_TEMP;
    ELSE
        OPEN p_SelCur13 FOR SELECT Collat.*,
                        ROWNUM
                FROM Loan.LoanGntyCollatTbl Collat
                WHERE LOANAPPNMB = p_LOANAPPNMB
                        and Collat.LOANAPPNMB NOT IN ( 
                                SELECT       LOANAPPNMB 
                                FROM   Loan.LoanLmtGntyCollatTbl LmtGnty
                                WHERE LmtGnty.LOANAPPNMB = Collat.LOANAPPNMB )
                ORDER BY LOANCOLLATSEQNMB, LOANCOLLATSUBTYPCD, LOANCOLLATTYPCD;
                DELETE FROM LOANAPP.LOANCOLLATLIEN_TEMP;
                OPEN LOANCOLLAT;
                    LOOP
                        FETCH LOANCOLLAT INTO LOANCOLLAT_rec;
                        EXIT WHEN LOANCOLLAT%notfound;                            
                            insert INTO LOANCOLLATLIEN_TEMP
                            select LOANCOLLATLIEN.*,  ROWNUM
                            from LOANAPP.LOANCOLLATLIENTBL LOANCOLLATLIEN
                            where LOANAPPNMB = LOANCOLLAT_rec.LOANAPPNMB and LoanCollatSeqNmb = LOANCOLLAT_rec.LoanCollatSeqNmb;
                            COMMIT;                           
                    END LOOP;
        OPEN p_SelCur14 FOR SELECT * FROM LOANAPP.LOANCOLLATLIEN_TEMP;
    END IF;   
    
EXCEPTION
    WHEN OTHERS THEN
        BEGIN
            ROLLBACK TO LOANAUTHDOCCSP;
            RAISE;
        END;
END;
/
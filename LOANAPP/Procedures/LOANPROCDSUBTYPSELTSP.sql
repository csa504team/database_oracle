CREATE OR REPLACE PROCEDURE LOANAPP.LOANPROCDSUBTYPSELTSP (
    p_Identifier       NUMBER := 0,
    p_LoanAppNmb       NUMBER := 0,
    p_LOANPROCDTYPCD   CHAR:=NULL,
    p_SelCur       OUT SYS_REFCURSOR,
    p_RetVal       OUT NUMBER)
AS
BEGIN
    SAVEPOINT LOANPROCDSUBTYPSELTSP;

    /* Select from LOANPROCDSUBTYPTBL Table */
    IF p_Identifier = 0
    THEN
        BEGIN
            OPEN p_SelCur FOR 
                SELECT 	LOANPROCDSUBTYPID,
                        LOANAPPNMB,
                        LOANPROCDTYPCD,
                        LOANPROCDTEXTBLOCK,
                        LOANPROCDAMT,
                        LoanProcdAddr,
                        LoanProcdDesc,
                        ProcdSubTyLkupId,
                        Lender
                FROM    LOANPROCDSUBTYPTBL
                WHERE   LoanAppNmb = p_LoanAppNmb order by LASTUPDTDT desc;

            p_RetVal := SQL%ROWCOUNT;
        END;
    ELSIF p_Identifier = 1
        THEN
        BEGIN
            OPEN p_SelCur FOR 
                SELECT 	LOANPROCDSUBTYPID,
                        LOANAPPNMB,
                        LOANPROCDTYPCD,
                        LOANPROCDTEXTBLOCK,
                        LOANPROCDAMT,
                        LoanProcdAddr,
                        LoanProcdDesc,
                        ProcdSubTyLkupId,
                        Lender	
                FROM    LOANPROCDSUBTYPTBL
                WHERE   LoanAppNmb = p_LoanAppNmb
                AND     LOANPROCDTYPCD=p_LOANPROCDTYPCD order by LASTUPDTDT desc;

            p_RetVal := SQL%ROWCOUNT;
        END;
    ELSIF p_Identifier = 38
        THEN
        BEGIN
            OPEN p_SelCur FOR   /* 38 = Leet for Extract XML */
                SELECT 	LOANAPPNMB,
                        LOANPROCDTYPCD,
                        LoanProcdAddr,
                        LoanProcdDesc,                        
                        LOANPROCDTEXTBLOCK,
                        to_char(LOANPROCDAMT, 'FM999999999999990.00') LoanProcdAmt,
                        Lender,
                        ProcdSubTyLkupId 
                FROM    LOANPROCDSUBTYPTBL
                WHERE   LoanAppNmb = p_LoanAppNmb
                order by LOANPROCDTYPCD, LASTUPDTDT desc;

            p_RetVal := SQL%ROWCOUNT;
        END;

    END IF;

    p_RetVal := NVL (p_RetVal, 0);
--END;
EXCEPTION
    WHEN OTHERS
    THEN
        BEGIN
            ROLLBACK TO LOANPROCDSUBTYPSELTSP;
            RAISE;
        END;
END LOANPROCDSUBTYPSELTSP;
/




GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPSELTSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPSELTSP TO LOANACCTREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPSELTSP TO LOANAPPREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPSELTSP TO LOANAPPUPDAPPROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPSELTSP TO LOANORIGBORROWER;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPSELTSP TO LOANPOSTSERVSUROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPSELTSP TO LOANREADALLROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPSELTSP TO LOANUPDROLE;

GRANT EXECUTE ON LOANAPP.LOANPROCDSUBTYPSELTSP TO POOLSECADMINROLE;

CREATE OR REPLACE PROCEDURE LOANAPP.LOANAPPUPDTSP (p_Identifier                            NUMBER := 0,
                                                   p_RetVal                            OUT NUMBER,
                                                   p_LoanAppNmb                            NUMBER := 0,
                                                   p_PrgmCd                                CHAR := NULL,
                                                   p_PrcsMthdCd                            CHAR := NULL,
                                                   p_LoanAppNm                             VARCHAR2 := NULL,
                                                   p_LoanAppRecvDt                         DATE := NULL,
                                                   p_LoanAppEntryDt                        DATE := NULL,
                                                   p_LoanAppRqstAmt                 IN OUT NUMBER,
                                                   p_LoanAppSBAGntyPct                     NUMBER := NULL,
                                                   p_LoanAppSBARcmndAmt                    NUMBER := NULL,
                                                   p_LoanAppRqstMatMoQty                   NUMBER := NULL,
                                                   p_LoanAppEPCInd                         CHAR := NULL,
                                                   p_LoanAppPymtAmt                        NUMBER := NULL,
                                                   p_LoanAppFullAmortPymtInd               CHAR := NULL,
                                                   p_LoanAppMoIntQty                       NUMBER := NULL,
                                                   p_LoanAppLifInsurRqmtInd                CHAR := NULL,
                                                   p_LoanAppRcnsdrtnInd                    CHAR := NULL,
                                                   p_LoanAppInjctnInd                      CHAR := NULL,
                                                   p_LoanAppEWCPSnglTransPostInd           CHAR := NULL,
                                                   p_LoanAppEWCPSnglTransInd               CHAR := NULL,
                                                   p_LoanAppEligEvalInd                    CHAR := NULL,
                                                   p_LoanAppNewBusCd                       CHAR := NULL,
                                                   p_NAICSCd                               NUMBER := NULL,
                                                   p_LoanCollatInd                         CHAR := NULL,
                                                   p_LoanAppDisasterCntrlNmb               VARCHAR2 := NULL,
                                                   p_LoanAppCreatUserId                    VARCHAR2 := NULL,
                                                   p_LoanAppOutPrgrmAreaOfOperInd          CHAR := NULL,
                                                   p_LoanAppParntLoanNmb                   CHAR := NULL,
                                                   p_LoanAppMicroLendrId                   CHAR := NULL,
                                                   p_ACHRtngNmb                            VARCHAR2 := NULL,
                                                   p_ACHAcctNmb                            VARCHAR2 := NULL,
                                                   p_ACHAcctTypCd                          CHAR := NULL,
                                                   p_LOANAPPCDCGNTYAMT                     NUMBER := NULL,
                                                   p_LOANAPPCDCNETDBENTRAMT                NUMBER := NULL,
                                                   p_LOANAPPCDCFUNDFEEAMT                  NUMBER := NULL,
                                                   p_LOANAPPCDCSEPRTPRCSFEEIND             CHAR := NULL,
                                                   p_LOANAPPCDCPRCSFEEAMT                  NUMBER := NULL,
                                                   p_LOANAPPCDCCLSCOSTAMT                  NUMBER := NULL,
                                                   p_LOANAPPCDCOTHCLSCOSTAMT               NUMBER := NULL,
                                                   p_LOANAPPCDCUNDRWTRFEEAMT               NUMBER := NULL,
                                                   p_LOANAPPCONTRIBPCT                     NUMBER := NULL,
                                                   p_LOANAPPCONTRIBAMT                     NUMBER := NULL,
                                                   p_LOANDISASTRAPPFEECHARGED              NUMBER := NULL,
                                                   p_LOANDISASTRAPPDCSN                    CHAR := NULL,
                                                   p_LOANASSOCDISASTRAPPNMB                CHAR := NULL,
                                                   p_LOANASSOCDISASTRLOANNMB               CHAR := NULL,
                                                   p_LOANDISASTRAPPNMB                     CHAR := NULL,
                                                   p_LoanAppUndrwritngExprtDt              DATE := NULL,
                                                   p_LOANNMB                               CHAR := NULL,
                                                   p_LOANAPPFUNDDT                         DATE := NULL,
                                                   p_UNDRWRITNGBY                          CHAR := NULL,
                                                   p_COHORTCD                              CHAR := NULL,
                                                   p_FUNDSFISCLYR                          NUMBER := NULL,
                                                   p_LOANTYPIND                            CHAR := NULL,
                                                   p_LoanAppUndrwritngExprtRsn             VARCHAR2 := NULL,
                                                   p_LOANAPPINTDTLCD                       VARCHAR2 := NULL,
                                                   p_LOANPYMNINSTLMNTFREQCD                CHAR := NULL,
                                                   p_UndrwritngCrdRiskCertInd              VARCHAR2 := NULL,
                                                   p_Statcd                                CHAR := NULL,
                                                   p_LoanAmtLimExmptInd                    CHAR := NULL,
                                                   p_LoanAppEtranVenNm                     CHAR := NULL,
                                                   p_LoanPymntSchdlFreq                    VARCHAR := NULL,
                                                   p_LOANGNTYMATSTIND                      CHAR := NULL,
                                                   p_LoanGntyNoteDt                        DATE := NULL,
                                                   p_CDCServFeePct                         NUMBER := NULL,
                                                   p_LoanAppCDCGrossDbentrAmt              NUMBER := NULL,
                                                   p_LoanAppBalToBorrAmt                   NUMBER := NULL,
                                                   p_LoanAppRevlMoIntQty                   NUMBER := NULL,
                                                   p_LoanAppAmortMoIntQty                  NUMBER := NULL,
                                                   p_LoanPymtRepayInstlTypCd               CHAR := NULL,
                                                   p_LoanAppExtraServFeeAMT                NUMBER := NULL,
                                                   p_LoanAppExtraServFeeInd                CHAR := NULL,
                                                   p_Rqstseqnmb                            NUMBER := NULL,
                                                   p_lndrmatchind                          CHAR := NULL,
                                                   p_LoanAppMonthPayroll                   NUMBER := NULL,
                                                   p_LoanAppUSResInd                       CHAR:=NULL,
                                                   p_LOANAPPSCHEDCIND                   CHAR := NULL, 
                                                   p_LOANAPPSCHEDCYR                    NUMBER := NULL, 
                                                   p_LOANAPPGROSSINCOMEAMT              NUMBER := NULL)
AS
    /*
      created by:
      purpose:
      parameters:
      Revision:
               Modified by APK - 09/23/2010- to include LoanSBAGntyBalAmt as part of calculation instead of LoanLendrGrossAmt, in Identifier 14.
                09/28/2010-- APK-- included the FININSTRMNTTYPIND change for PrgrmValidTbl and parameter tables.
      10/05/2010 -- APK -- Removed the FININSTRMNTTYPIND column from parameter tables and
     hence in the procedure.
       10/07/2010 -- APK -- modify identifier 14 in LOANAPP.LOANAPPUPDTSP remove the update
       to  LoanAppSBAGntyPct field..
     APK -- 02/06/2012 -- modified to add new column LOANDISASTRAPPNMB, as its added to the loanapptbl in development.
     RGG -- 03/26/2012 -- added an IF condition at line 347
     SP  -- 11/16/2012 -- added id=17  to update when a loan application was successfully exported to the Loan Guaranty Processing Center for SBA Underwriting
     SP -- 02/28/2013 -- modified to add new columns LOANNMB,LOANAPPFUNDDT,UNDRWRITNGBY,
    COHORTCD,FUNDSFISCLYR,LOANTYPIND to id=4,18
     SP -- 06/05/2013 -- Added id =19 to update LoanAppRqstAmt,LoanAppSBARcmndAmt as SBA reviewers are allowed to make these changes to the application at the time they render the decision to approve or decline the application
     RGG -- 06/19/2013 -- Changed Id 18 to reflect for Funds pending and added ID 20 for Funded loans.
     RGG -- 06/28/2013 -- Chnged Id 18 to add ,LoanAppAppvDt       = sysdate at line 423 as per Developer's request.
     SP -- 09/02/2013 -- Changed id =17 to update LoanAppUndrwritngExprtRsn
     RSURAPA - 10/23/2013 -- Modified the procedure to add new column LOANAPPINTDTLCD
     RSURAP - 10/29/2013 -- Modified the procedure to add new column LoanAppPymtAdjPrdCd
     RGG -- 11/01/2013 -- Added new columns to ID 19.
     RGG -- 11/07/2013 -- Renamed LoanAppPymtAdjPrdCd to LOANPYMNINSTLMNTFREQCD
     RGG -- 11/08/2013 -- Added LoanAppInitlSprdOvrPrimePct to ID 19.
     SP  -- 11/27/2013 -- Removed references to columns :IMRtTypCd(id= 0,1,12,19),LoanAppFixVarInd(id= 0,1,12,19),
    LoanAppBaseRt(id= 0,1,12,19),LoanAppInitlIntPct(id= 0,1,12,14,19),LoanAppInitlSprdOvrPrimePct(id= 0,1,12,19),LoanAppOthAdjPrdTxt,LoanAppAdjPrdCd(id= 0,1,12,19)
    SP   -- 01/28/2014  -- Added id =21 to update UnderwritingBy for reconsideration changes
    SP   -- 02/20/2014  -- Added id =22 Update only interest detail code for Underwriting web service
    SP   -- 04/07/2014  -- Modified id = 0,12 to remove NAICSYRNMB,NAICSCD(0,12,19),LOANAPPFRNCHSCD,LOANAPPFRNCHSNM,loanappfrnchsind as these are moved to loanappprojtbl
    SB - 09/03/2014 -- Modified the procedure for (identifier 4) with logic IF PrgrmCd=E then Financial Instrument Ind = `E� else `L�
    PSP - 09/26/2014 -- Modified to add new identifier to update UndrwritngCrdRiskCertInd for lender to update their own underwriting decision, despite LqdCr
    SS-05/05/2016 --Modified to add new column LoanAmtLimExmptInd to identifier 25
    BR - 08/12/2016 -- Modified to add new column LoanAppEtranVenNm to identifier 25
    SS--09/29/2016 --Modified to add new column LoanPymntSchdlFreq  --OPSMDEV 1116
    RY--11/14/16--OPSMDEV-1212 --Added LoanPymntSchdlFreq condition to Identifier12
    SS--11/29/2016 OPSMDEV 1245 --Added LOANGNTYMATSTIND to identifier 0
    SS--01/04/2017 Added identifier 31
    --NK--04/20/2017--CAFSOPER-- Added p_LoanGntyNoteDt in ID 25.
    SS--05/19/2017 --OPSMDEV 1436 Added CDCServFeePct to Identifier 0
    SS--05/23/2017 --OPSMDEV 1436 Added identifier 32
    RY--07/19/2017--CAFSOPER-805-- Added LoanAppCDCGrossDbentrAmt,LoanAppBalToBorrAmt to ID- 0 and 25
    JP -- 09/29/2017 - CAFSOPER-1201 Modified the identifier 16 code
    NK--01/12/2018--OPSMDEV 1644--Added LoanAppRevlMoIntQty, LoanAppAmortMoIntQty  to ID 0
    NK--01/18/2018--OPSMDEV 1644--Added LoanAppRevlMoIntQty, LoanAppAmortMoIntQty  to ID 25
    BJR -- 01/25/2018 -- CAFSOPER-1381 -- Removed created by and created dates from the update statements so that the original creator/create date stays intact for the record.
 Ry--04/19/18 --OPSMDEV--1783--Added LoanPymtRepayInstlTypCd in Id-0,1,12,19,25,31
 SS--10/25/2018--OPSMDEV 1965 Added  LoanAppExtraServFeeAMT,LoanAppExtraServFeeInd  for extraordinary servicing fee to identifier 0 and 25
 RY--08/15/2019--OPSMDEV--2269:: Added Id-34
 JP -- 04/03/2020 CARESACT-63 added LoanAppMonthPayroll for ID 25 update of LoanAppTbl
 SL--01/08/2021--SODSTORY-360. Added p_LoanAppUSResInd to ID 25
 SL--03/03/2021 SODSTORY-571 Added new columns: LOANAPPSCHEDCIND, LOANAPPSCHEDCYR, LOANAPPGROSSINCOMEAMT 

 */
    v_LoanAppOrigntnOfcCd   CHAR (4);
    v_LoanAppPrcsOfcCd      CHAR (4);
    v_IMPrmtrValChk         VARCHAR (255);
    v_IMPrmtrId             VARCHAR2 (255);
    v_RecovInd              CHAR (1);
    v_PrcsMthdCd            CHAR (3);
    v_LoanAppRecvDt         DATE;
    v_LoanSBAGntyBalAmt     NUMBER (19, 4);
    v_LOANINTRT             NUMBER (6, 3);
BEGIN
    /* Update All in the Loan Incomplete Application Table */
    CASE p_Identifier
        WHEN 0
        THEN
            BEGIN
                UPDATE LoanAppTbl
                   SET LoanAppNmb = p_LoanAppNmb,
                       PrgmCd = TRIM (p_PrgmCd),
                       PrcsMthdCd = TRIM (p_PrcsMthdCd),
                       LoanAppNm = p_LoanAppNm,
                       LoanAppRecvDt = p_LoanAppRecvDt,
                       LoanAppRqstAmt = p_LoanAppRqstAmt,
                       LoanAppSBAGntyPct = p_LoanAppSBAGntyPct,
                       LoanAppSBARcmndAmt = p_LoanAppSBARcmndAmt,
                       LoanAppRqstMatMoQty = p_LoanAppRqstMatMoQty,
                       LoanAppEPCInd = p_LoanAppEPCInd,
                       LoanAppPymtAmt = p_LoanAppPymtAmt,
                       LoanAppFullAmortPymtInd = p_LoanAppFullAmortPymtInd,
                       LoanAppMoIntQty = p_LoanAppMoIntQty,
                       LoanAppLifInsurRqmtInd = p_LoanAppLifInsurRqmtInd,
                       LoanAppRcnsdrtnInd = p_LoanAppRcnsdrtnInd,
                       LoanAppInjctnInd = p_LoanAppInjctnInd,
                       LoanAppEWCPSnglTransPostInd = p_LoanAppEWCPSnglTransPostInd,
                       LoanAppEWCPSnglTransInd = p_LoanAppEWCPSnglTransInd,
                       LoanAppEligEvalInd = p_LoanAppEligEvalInd,
                       LoanAppNewBusCd = p_LoanAppNewBusCd,
                       LoanAppDisasterCntrlNmb = p_LoanAppDisasterCntrlNmb,
                       LoanCollatInd = p_LoanCollatInd,
                       LoanAppOutPrgrmAreaOfOperInd = p_LoanAppOutPrgrmAreaOfOperInd,
                       LoanAppParntLoanNmb = p_LoanAppParntLoanNmb,
                       LoanAppMicroLendrId = p_LoanAppMicroLendrId,
                       LOANDISASTRAPPFEECHARGED = p_LOANDISASTRAPPFEECHARGED,
                       LOANDISASTRAPPDCSN = p_LOANDISASTRAPPDCSN,
                       LOANASSOCDISASTRAPPNMB = p_LOANASSOCDISASTRAPPNMB,
                       LOANASSOCDISASTRLOANNMB = p_LOANASSOCDISASTRLOANNMB,
                       LASTUPDTUSERID = NVL (p_LoanAppCreatUserId, USER),
                       LASTUPDTDT = SYSDATE,
                       LOANAPPINTDTLCD = p_LOANAPPINTDTLCD,
                       LOANPYMNINSTLMNTFREQCD = p_LOANPYMNINSTLMNTFREQCD,
                       LoanPymntSchdlFreq = p_LoanPymntSchdlFreq,
                       LOANGNTYMATSTIND = p_LOANGNTYMATSTIND,
                       LoanGntyNoteDt = p_LoanGntyNoteDt,
                       CDCServFeePct = p_CDCServFeePct,
                       LoanAppCDCGrossDbentrAmt = p_LoanAppCDCGrossDbentrAmt,
                       LoanAppBalToBorrAmt = p_LoanAppBalToBorrAmt,
                       LoanAppRevlMoIntQty = p_LoanAppRevlMoIntQty,
                       LoanAppAmortMoIntQty = p_LoanAppAmortMoIntQty,
                       LoanPymtRepayInstlTypCd = p_LoanPymtRepayInstlTypCd,
                       LoanAppExtraServFeeAMT = p_LoanAppExtraServFeeAMT,
                       LoanAppExtraServFeeInd = p_LoanAppExtraServFeeInd
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 1
        /* Update Application Screen into the Loan Incomplete Application Table */
        THEN
            BEGIN
                UPDATE LoanAppTbl
                   SET LoanAppNmb = p_LoanAppNmb,
                       PrgmCd = p_PrgmCd,
                       PrcsMthdCd = p_PrcsMthdCd,
                       LoanAppNm = p_LoanAppNm,
                       LoanAppRecvDt = p_LoanAppRecvDt,
                       LoanAppRqstAmt = p_LoanAppRqstAmt,
                       LoanAppSBAGntyPct = p_LoanAppSBAGntyPct,
                       LoanAppSBARcmndAmt = p_LoanAppSBARcmndAmt,
                       LoanAppRqstMatMoQty = p_LoanAppRqstMatMoQty,
                       LoanAppEPCInd = p_LoanAppEPCInd,
                       LoanAppPymtAmt = p_LoanAppPymtAmt,
                       LoanAppFullAmortPymtInd = p_LoanAppFullAmortPymtInd,
                       LoanAppMoIntQty = p_LoanAppMoIntQty,
                       LoanAppLifInsurRqmtInd = p_LoanAppLifInsurRqmtInd,
                       LoanAppRcnsdrtnInd = p_LoanAppRcnsdrtnInd,
                       LoanAppInjctnInd = p_LoanAppInjctnInd,
                       LoanAppEWCPSnglTransPostInd = LoanAppEWCPSnglTransPostInd,
                       LoanAppEWCPSnglTransInd = LoanAppEWCPSnglTransInd,
                       LoanAppDisasterCntrlNmb = LoanAppDisasterCntrlNmb,
                       LASTUPDTUSERID = NVL (p_LoanAppCreatUserId, USER),
                       LASTUPDTDT = SYSDATE,
                       LOANPYMNINSTLMNTFREQCD = p_LOANPYMNINSTLMNTFREQCD,
                       LoanPymtRepayInstlTypCd = p_LoanPymtRepayInstlTypCd
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 2
        /* Update Eligibility in the Loan Incomplete Application Table */
        THEN
            BEGIN
                UPDATE LoanAppTbl
                   SET LoanAppEligEvalInd = p_LoanAppEligEvalInd,
                       LASTUPDTUSERID = NVL (p_LoanAppCreatUserId, USER),
                       LASTUPDTDT = SYSDATE
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 3                                       /* Update the Received Date in Loan Incomplete Application Table */
        THEN
            BEGIN
                UPDATE LoanAppTbl
                   SET LoanAppNmb = p_LoanAppNmb,
                       LoanAppRecvDt = p_LoanAppRecvDt,
                       LASTUPDTUSERID = NVL (p_LoanAppCreatUserId, USER),
                       LASTUPDTDT = SYSDATE
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 4                            /* Update the LoanAppNm and PrcsMthdCd in Loan Incomplete Application Table */
        THEN
            BEGIN
                UPDATE LoanAppTbl
                   SET PrcsMthdCd = p_PrcsMthdCd,
                       LoanAppNm = p_LoanAppNm,
                       LASTUPDTUSERID = NVL (p_LoanAppCreatUserId, USER),
                       LASTUPDTDT = SYSDATE,
                       UNDRWRITNGBY = p_UNDRWRITNGBY,
                       LOANTYPIND = p_LOANTYPIND,
                       PrgmCd = p_PrgmCd,
                       FinInstrmntTypInd = (CASE WHEN p_Prgmcd = 'E' THEN 'E' ELSE 'L' END)
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 11                                                                     -- only to update recommended amount
        THEN
            BEGIN
                UPDATE LoanAppTbl
                   SET LoanAppSBARcmndAmt = p_LoanAppSBARcmndAmt,
                       LASTUPDTUSERID = NVL (p_LoanAppCreatUserId, USER),
                       LASTUPDTDT = SYSDATE
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 12
        THEN
            BEGIN
                UPDATE LoanAppTbl
                   SET PrgmCd = p_PrgmCd,
                       PrcsMthdCd = p_PrcsMthdCd,
                       LoanAppNm = p_LoanAppNm,
                       LoanAppRecvDt = p_LoanAppRecvDt,
                       LoanAppRqstAmt = p_LoanAppRqstAmt,
                       LoanAppSBAGntyPct = p_LoanAppSBAGntyPct,
                       LoanAppSBARcmndAmt = p_LoanAppSBARcmndAmt,
                       LoanAppRqstMatMoQty = p_LoanAppRqstMatMoQty,
                       LoanAppEPCInd = p_LoanAppEPCInd,
                       LoanAppPymtAmt = p_LoanAppPymtAmt,
                       LoanAppFullAmortPymtInd = p_LoanAppFullAmortPymtInd,
                       LoanAppMoIntQty = p_LoanAppMoIntQty,
                       LoanAppLifInsurRqmtInd = p_LoanAppLifInsurRqmtInd,
                       LoanAppRcnsdrtnInd = p_LoanAppRcnsdrtnInd,
                       LoanAppInjctnInd = p_LoanAppInjctnInd,
                       LoanAppEWCPSnglTransPostInd = p_LoanAppEWCPSnglTransPostInd,
                       LoanAppEWCPSnglTransInd = p_LoanAppEWCPSnglTransInd,
                       LoanAppEligEvalInd = p_LoanAppEligEvalInd,
                       LoanAppNewBusCd = p_LoanAppNewBusCd,
                       LoanAppDisasterCntrlNmb = p_LoanAppDisasterCntrlNmb,
                       LoanCollatInd = p_LoanCollatInd,
                       LoanAppOutPrgrmAreaOfOperInd = p_LoanAppOutPrgrmAreaOfOperInd,
                       LoanAppParntLoanNmb = p_LoanAppParntLoanNmb,
                       LoanAppMicroLendrId = p_LoanAppMicroLendrId,
                       LOANAPPCDCGNTYAMT = p_LOANAPPCDCGNTYAMT,
                       LOANAPPCDCNETDBENTRAMT = p_LOANAPPCDCNETDBENTRAMT,
                       LOANAPPCDCFUNDFEEAMT = p_LOANAPPCDCFUNDFEEAMT,
                       LOANAPPCDCSEPRTPRCSFEEIND = p_LOANAPPCDCSEPRTPRCSFEEIND,
                       LOANAPPCDCPRCSFEEAMT = p_LOANAPPCDCPRCSFEEAMT,
                       LOANAPPCDCCLSCOSTAMT = p_LOANAPPCDCCLSCOSTAMT,
                       LOANAPPCDCOTHCLSCOSTAMT = p_LOANAPPCDCOTHCLSCOSTAMT,
                       LOANAPPCDCUNDRWTRFEEAMT = p_LOANAPPCDCUNDRWTRFEEAMT,
                       LOANAPPCONTRIBPCT = p_LOANAPPCONTRIBPCT,
                       LOANAPPCONTRIBAMT = p_LOANAPPCONTRIBAMT,
                       LASTUPDTUSERID = NVL (p_LoanAppCreatUserId, USER),
                       LASTUPDTDT = SYSDATE,
                       LOANAPPINTDTLCD = p_LOANAPPINTDTLCD,
                       LOANPYMNINSTLMNTFREQCD = p_LOANPYMNINSTLMNTFREQCD,
                       LoanPymntSchdlFreq = p_LoanPymntSchdlFreq,
                       LOANGNTYMATSTIND = p_LOANGNTYMATSTIND,
                       LoanGntyNoteDt = p_LoanGntyNoteDt,
                       LoanPymtRepayInstlTypCd = p_LoanPymtRepayInstlTypCd
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 13
        THEN
            BEGIN
                UPDATE loanapptbl
                   SET loanappnm = p_LoanAppNm
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 14
        THEN
            BEGIN
                SELECT LoanIntRt
                  INTO v_LoanIntRt
                  FROM loanapp.LoanIntDtlTbl
                 WHERE loanappnmb = p_LoanAppNmb AND LoanIntDtlSeqNmb = 1;

                SELECT NVL (SUM (LoanSBAGntyBalAmt), 0)
                  INTO v_LoanSBAGntyBalAmt
                  FROM loanapp.LoanPartLendrTbl p
                 WHERE p.LoanAppNmb = p_LoanAppNmb;

                UPDATE loanapptbl a
                   SET LoanAppRqstAmt = v_LoanSBAGntyBalAmt,
                       LoanAppPymtAmt = v_LoanSBAGntyBalAmt * (1 + (v_LoanIntRt / 100)) / LoanAppRqstMatMoQty, /*(1+(LoanAppInitlIntPct/100))*/
                       LoanAppSBARcmndAmt = v_LoanSBAGntyBalAmt,
                       LASTUPDTUSERID = NVL (p_LoanAppCreatUserId, USER),
                       LASTUPDTDT = SYSDATE
                 WHERE LoanAppNmb = p_LoanAppNmb;

                /*OPEN p_SelCur FOR*/
                SELECT LoanAppRqstAmt
                  INTO p_LoanAppRqstAmt
                  FROM loanapp.LoanAppTbl
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 15
        THEN
            BEGIN
                UPDATE loanapptbl
                   SET LOANDISASTRAPPDCSN = p_LOANDISASTRAPPDCSN,
                       LOANASSOCDISASTRAPPNMB = p_LOANASSOCDISASTRAPPNMB,
                       LASTUPDTUSERID = NVL (p_LoanAppCreatUserId, USER),
                       LASTUPDTDT = SYSDATE
                 WHERE loanappnmb = p_loanappnmb;
            END;
        WHEN 16                                                   -- LoanAppOrigntnOfcCd, LoanAppPrcsOfcCd and LoanAppNm
        THEN
            BEGIN
                /*IF p_PrcsMthdCd IS NULL
                THEN
                    BEGIN
                        SELECT PrcsMthdCd
                          INTO v_PrcsMthdCd
                          FROM LoanAppTbl
                         WHERE LoanAppNmb = p_LoanAppNmb;
                    EXCEPTION
                        WHEN NO_DATA_FOUND
                        THEN
                            v_PrcsMthdCd := NULL;
                    END;
                ELSE
                    v_PrcsMthdCd := p_PrcsMthdCd;
                END IF;

                IF v_PrcsMthdCd IN ('DPB', 'DPH', 'DPO')
                THEN
                    v_LoanAppOrigntnOfcCd := '9030';
                ELSE
                    BEGIN
                        v_LoanAppRecvDt := SYSDATE;
                        v_RecovInd := NULL;
                        v_IMPrmtrId := 'OrigntOfcCd';
                        v_IMPrmtrValChk :=
                            LOANAPP.GET_IMPRMTRVAL (v_IMPrmtrId,
                                                    v_PrcsMthdCd,
                                                    v_LoanAppRecvDt,
                                                    v_RecovInd);

                        IF v_IMPrmtrValChk IS NULL
                        THEN
                            BEGIN
                                SELECT c.OrignOfcCd
                                  INTO v_LoanAppOrigntnOfcCd
                                  FROM loanapp.LoanAppProjTbl p,                                           -- p for proj
                                                                 sbaref.CntyTbl c                          -- c for cnty
                                 WHERE     c.CntyCd = p.LoanAppProjCntyCd
                                       AND c.StCd = p.LoanAppProjStCd
                                       AND p.LoanAppNmb = p_LoanAppNmb;
                            EXCEPTION
                                WHEN NO_DATA_FOUND
                                THEN
                                    v_LoanAppOrigntnOfcCd := NULL;
                            END;
                        ELSE
                            v_LoanAppOrigntnOfcCd := v_IMPrmtrValChk;
                        END IF;
                    END;
                END IF;

                BEGIN
                    v_LoanAppRecvDt := SYSDATE;
                    v_RecovInd := NULL;
                    v_IMPrmtrId := 'SrvcngOfcCd';
                    v_IMPrmtrValChk :=
                        LOANAPP.GET_IMPRMTRVAL (v_IMPrmtrId,
                                                v_PrcsMthdCd,
                                                v_LoanAppRecvDt,
                                                v_RecovInd);

                    IF v_IMPrmtrValChk IS NULL
                    THEN
                        BEGIN
                            SELECT c.ServOfcCd
                              INTO v_LoanAppPrcsOfcCd
                              FROM loanapp.LoanAppProjTbl p,                                               -- p for proj
                                                             sbaref.CntyTbl c                              -- c for cnty
                             WHERE     c.CntyCd = p.LoanAppProjCntyCd
                                   AND c.StCd = p.LoanAppProjStCd
                                   AND p.LoanAppNmb = p_LoanAppNmb;
                        EXCEPTION
                            WHEN NO_DATA_FOUND
                            THEN
                                v_LoanAppPrcsOfcCd := NULL;
                        END;
                    ELSE
                        v_LoanAppPrcsOfcCd := v_IMPrmtrValChk;
                    END IF;
                END;*/

                UPDATE LoanAppTbl SET
                   	   --LoanAppOrigntnOfcCd = NVL (v_LoanAppOrigntnOfcCd, LoanAppOrigntnOfcCd),
                       --LoanAppPrcsOfcCd = NVL (v_LoanAppPrcsOfcCd, LoanAppPrcsOfcCd),
                       LoanAppNm = NVL (p_LoanAppNm, LoanAppNm),
                       LASTUPDTUSERID = NVL (p_LoanAppCreatUserId, USER),
                       LASTUPDTDT = SYSDATE
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 17 /*update when a loan application was successfully exported to the Loan Guaranty Processing Center for SBA Underwriting*/
        THEN
            BEGIN
                UPDATE loanapptbl
                   SET LoanAppUndrwritngExprtDt = p_LoanAppUndrwritngExprtDt,
                       LoanAppUndrwritngExprtRsn = p_LoanAppUndrwritngExprtRsn,
                       LASTUPDTUSERID = NVL (p_LoanAppCreatUserId, USER),
                       LASTUPDTDT = SYSDATE
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 18                                                                       /*Mark the loan as funds pending*/
        THEN
            BEGIN
                UPDATE loanapptbl
                   SET Statcd = 'FP',
                       loanappstatuserid = p_LoanAppCreatUserId,
                       loanappstatdt = SYSDATE,
                       LoanAppAppvDt = SYSDATE,
                       loanappfunddt = p_loanappfunddt,
                       cohortcd = p_cohortcd,
                       FUNDSFISCLYR = p_FUNDSFISCLYR,
                       LASTUPDTUSERID = NVL (p_LoanAppCreatUserId, USER),
                       LASTUPDTDT = SYSDATE
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 19 /*Update LoanAppRqstAmt for SBA reviewers at the time of rendering the decision to approve or decline the application */
        THEN
            BEGIN
                UPDATE loanapptbl
                   SET LoanAppRqstAmt = p_LoanAppRqstAmt,
                       LoanAppSBARcmndAmt = p_LoanAppSBARcmndAmt,
                       LoanAppInjctnInd = p_LoanAppInjctnInd,
                       LoanAppMoIntQty = p_LoanAppMoIntQty,
                       LoanAppPymtAmt = p_LoanAppPymtAmt,
                       LoanAppRcnsdrtnInd = p_LoanAppRcnsdrtnInd,
                       LoanAppRqstMatMoQty = p_LoanAppRqstMatMoQty,
                       LoanAppSBAGntyPct = p_LoanAppSBAGntyPct,
                       LASTUPDTUSERID = NVL (p_LoanAppCreatUserId, USER),
                       LASTUPDTDT = SYSDATE,
                       LoanPymtRepayInstlTypCd = p_LoanPymtRepayInstlTypCd
                 WHERE LoanAppNmb = p_LoanAppNmb;

                UPDATE loanappprojtbl
                   SET NAICSCd = p_NAICSCd, LASTUPDTUSERID = NVL (p_LoanAppCreatUserId, USER), LASTUPDTDT = SYSDATE
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 20                                                                              /*Mark the loan as funded*/
        THEN
            BEGIN
                UPDATE loanapptbl
                   SET Statcd = 'FD',
                       loanappstatuserid = p_LoanAppCreatUserId,
                       loanappstatdt = SYSDATE,
                       loannmb = p_loannmb,
                       LASTUPDTUSERID = NVL (p_LoanAppCreatUserId, USER),
                       LASTUPDTDT = SYSDATE
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 21                                              /*Update only underwriting by for reconsideration changes*/
        THEN
            BEGIN
                UPDATE loanapptbl
                   SET UndrwritngBy = p_UndrwritngBy,
                       LastUpdtUserId = NVL (p_LoanAppCreatUserId, USER),
                       LastUpdtDt = SYSDATE
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 22                                       /*Update only interest detail code for Underwriting web service */
        THEN
            BEGIN
                UPDATE loanapptbl
                   SET LoanAppIntDtlCd = p_LoanAppIntDtlCd,
                       LastUpdtUserId = NVL (p_LoanAppCreatUserId, USER),
                       LastUpdtDt = SYSDATE
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 23 /*updates UndrwritngCrdRiskCertInd for lender to update their own underwriting decision, despite LqdCr */
        THEN
            BEGIN
                LOANAPP.MOVEAUDTLOANSCSP (P_LOANAPPNMB);

                UPDATE loanapptbl
                   SET UndrwritngCrdRiskCertInd = p_UndrwritngCrdRiskCertInd,
                       StatCd = p_StatCd,
                       loanappstatuserid = p_LoanAppCreatUserId,
                       loanappstatdt = SYSDATE,
                       LastUpdtUserId = p_LoanAppCreatUserId,
                       LastUpdtDt = SYSDATE
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 24        /* Update the Received Date in Loan Incomplete Application Table use LOANAPP.LOANAPPRECVUPDCSP */
        THEN
            BEGIN
                SELECT LoanAppRecvDt
                  INTO v_LoanAppRecvDt
                  FROM LoanAppTbl
                 WHERE LoanAppNmb = p_LoanAppNmb;

                IF v_LoanAppRecvDt IS NULL
                THEN
                    LOANAPP.LOANAPPRECVUPDCSP (p_LoanAppNmb, p_LoanAppRecvDt, p_LoanAppCreatUserId);
                    p_RetVal := 1;
                END IF;
            END;
        WHEN 25                                  /* Updates required by the OrigUpdate and Underwriting web services. */
        THEN
            BEGIN
                UPDATE LoanAppTbl
                   SET LastUpdtDt = SYSDATE,
                       LastUpdtUserId = NVL (p_LoanAppCreatUserId, USER),
                       LoanAppCDCClsCostAmt = p_LoanAppCDCClsCostAmt,
                       LoanAppCDCOthClsCostAmt = p_LoanAppCDCOthClsCostAmt,
                       LoanAppCDCFundFeeAmt = p_LoanAppCDCFundFeeAmt,
                       LoanAppCDCGntyAmt = p_LoanAppCDCGntyAmt,
                       LoanAppCDCNetDbentrAmt = p_LoanAppCDCNetDbentrAmt,
                       LoanAppCDCPrcsFeeAmt = p_LoanAppCDCPrcsFeeAmt,
                       LoanAppCDCSeprtPrcsFeeInd = p_LoanAppCDCSeprtPrcsFeeInd,
                       LoanAppCDCUndrwtrFeeAmt = p_LoanAppCDCUndrwtrFeeAmt,
                       LoanAppContribAmt = p_LoanAppContribAmt,
                       LoanAppContribPct = p_LoanAppContribPct,
                       LoanAppDisasterCntrlNmb = p_LoanAppDisasterCntrlNmb,
                       LoanAppEPCInd = p_LoanAppEPCInd,
                       LoanAppEWCPSnglTransInd = p_LoanAppEWCPSnglTransInd,
                       LoanAppEWCPSnglTransPostInd = p_LoanAppEWCPSnglTransPostInd,
                       LoanAppFullAmortPymtInd = p_LoanAppFullAmortPymtInd,
                       LoanAppInjctnInd = p_LoanAppInjctnInd,
                       LoanAppIntDtlCd = p_LoanAppIntDtlCd,
                       LoanAppLifInsurRqmtInd = p_LoanAppLifInsurRqmtInd,
                       LoanAppMoIntQty = p_LoanAppMoIntQty,
                       LoanAppNewBusCd = p_LoanAppNewBusCd,
                       LoanAppNm = p_LoanAppNm,
                       LoanAppOutPrgrmAreaOfOperInd = p_LoanAppOutPrgrmAreaOfOperInd,
                       LoanAppPymtAmt = p_LoanAppPymtAmt,
                       LoanAppRcnsdrtnInd = p_LoanAppRcnsdrtnInd,
                       LoanAppRqstAmt =
                           CASE WHEN StatCd IN ('R1', 'R2', 'R3') THEN LoanAppRqstAmt ELSE p_LoanAppRqstAmt END,
                       LoanAppSBARcmndAmt = p_LoanAppRqstAmt,
                       LoanAppRqstMatMoQty = p_LoanAppRqstMatMoQty,
                       LoanAppSBAGntyPct = p_LoanAppSBAGntyPct,
                       LoanAssocDisastrLoanNmb = p_LoanAssocDisastrLoanNmb,
                       LoanCollatInd = p_LoanCollatInd,
                       LoanDisastrAppDcsn = p_LoanDisastrAppDcsn,
                       LoanDisastrAppFeeCharged = p_LoanDisastrAppFeeCharged,
                       LoanDisastrAppNmb = p_LoanDisastrAppNmb,
                       LoanPymnInstlmntFreqCd = p_LoanPymnInstlmntFreqCd,
                       UndrwritngBy = p_UndrwritngBy,
                       LoanAmtLimExmptInd = p_LoanAmtLimExmptInd,
                       LoanAppEtranVenNm = p_LoanAppEtranVenNm,                 -- BR 08/12/2016 added LoanAppEtranVenNm
                       LOANGNTYMATSTIND = p_LOANGNTYMATSTIND,
                       LoanGntyNoteDt = p_LoanGntyNoteDt,
                       LoanAppCDCGrossDbentrAmt = p_LoanAppCDCGrossDbentrAmt,
                       LoanAppBalToBorrAmt = p_LoanAppBalToBorrAmt,
                       LoanAppRevlMoIntQty = p_LoanAppRevlMoIntQty,
                       LoanAppAmortMoIntQty = p_LoanAppAmortMoIntQty,
                       LoanPymtRepayInstlTypCd = p_LoanPymtRepayInstlTypCd,
                       LoanAppExtraServFeeAMT = p_LoanAppExtraServFeeAMT,
                       LoanAppExtraServFeeInd = p_LoanAppExtraServFeeInd,
                       LoanAppMonthPayroll = p_LoanAppMonthPayroll,
                       LoanAppUSResInd= p_LoanAppUSResInd,
                       LOANAPPSCHEDCIND = p_LOANAPPSCHEDCIND, 
                        LOANAPPSCHEDCYR = p_LOANAPPSCHEDCYR, 
                        LOANAPPGROSSINCOMEAMT = p_LOANAPPGROSSINCOMEAMT
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 31
        THEN
            BEGIN
                UPDATE LoanAppTbl
                   SET LoanAppIntDtlCd = p_LoanAppIntDtlCd,
                       LoanPymnInstlmntFreqCd = p_LoanPymnInstlmntFreqCd,
                       LoanPymntSchdlFreq = p_LoanPymntSchdlFreq,
                       LoanAppMoIntQty = p_LoanAppMoIntQty,
                       LoanAppPymtAmt = p_LoanAppPymtAmt,
                       LASTUPDTUSERID = NVL (p_LoanAppCreatUserId, USER),
                       LASTUPDTDT = SYSDATE,
                       LoanPymtRepayInstlTypCd = NVL (p_LoanPymtRepayInstlTypCd, LoanPymtRepayInstlTypCd)
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 32
        THEN
            BEGIN
                UPDATE LoanAppTbl
                   SET CDCServFeePct = p_CDCServFeePct,
                       LASTUPDTUSERID = NVL (p_LoanAppCreatUserId, USER),
                       LASTUPDTDT = SYSDATE
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 33
        THEN
            BEGIN
                UPDATE LoanAppTbl
                   SET Rqstseqnmb = p_rqstseqnmb, lndrmatchind = p_lndrmatchind
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
        WHEN 34
        THEN
            BEGIN
                UPDATE loanapptbl
                   SET Statcd = p_StatCd
                 WHERE LoanAppNmb = p_LoanAppNmb;
            END;
    END CASE;

    IF p_RetVal IS NULL
    THEN
        p_RetVal := SQL%ROWCOUNT;
        p_RetVal := NVL (p_RetVal, 0);
    END IF;
END LOANAPPUPDTSP;
/

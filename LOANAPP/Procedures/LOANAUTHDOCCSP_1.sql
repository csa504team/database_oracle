CREATE OR REPLACE procedure LOANAPP.LOANAUTHDOCCSP_1
(
p_LoanAppNMb number := null,
p_CreatuserId varchar2:=null,
p_SelCur1 out sys_refcursor,
p_SelCur2 out sys_refcursor,
p_SelCur3 out sys_refcursor,
p_TermInYears out float,
p_LoanNmb out varchar2,
p_AppvDt out date,
p_DebentureAmt out number,
p_default_string out char
)
as
origstate boolean := true;
loannmb_in char(10) := null;
loanapp_table_rec LOANAPP.LOANAPPTBL%rowtype;
loan_table_rec LOAN.LOANGNTYTBL%rowtype;
sbaofcd char(5):= null;
InCA boolean := false;
BusinessRECollateral boolean := false;
PPCollateral boolean := false;
MarineCollateral boolean := false;
LiabilityInsurance boolean := false;
ProductLiabilityInsurance boolean := false;
DramShopInsurance boolean := false;
MalpracticeInsurance boolean := false;
WorkersCompInsurance boolean := false;
OtherInsurance boolean := false;
InsuranceTypes char(1) :=null;
EnvironmentalRequired boolean :=false;
EPCText varchar2(50) :=null;
--Business type borrower's information if origination=true
Cursor BorrowerBusOrigSet
is
    Select bor.*, 
    bus.BusPhyAddrStr1NM||' '||bus.BusPhyAddrStr2Nm||' '||bus.BusPhyAddrCtyNm||','||bus.BusPhyAddrStCd||' '||bus.BusPhyAddrZipCd as Address,
    bus.BusPhyAddrStCd, 
    bus.BusNm,
    bus.BusTrdNm  
    From LOANAPP.LoanBorrTbl bor Join LOANAPP.LoanBusTbl bus 
    On (bor.TaxId = bus.TaxId And bor.LoanAppNmb = bus.LoanAppNmb)
    Where bor.BorrBusPerInd = 'B'
    and bor.LOANAPPNMB=p_LoanAppNmb;
--Business type borrower's information if origination=false
Cursor BorrowerBusSet
is
    Select bor.*, 
    bus.BusPhyAddrStr1NM||' '||bus.BusPhyAddrStr2Nm||' '||bus.BusPhyAddrCtyNm||','||bus.BusPhyAddrStCd||' '||bus.BusPhyAddrZipCd as Address,
    bus.BusPhyAddrStCd, 
    bus.BusNm,
    bus.BusTrdNm  
    From LOAN.LoanGntyBorrTbl bor Join LOAN.BusTbl bus 
    On (bor.TaxId = bus.TaxId)
    Where bor.BorrBusPerInd = 'B'
    and bor.LOANAPPNMB=p_LoanAppNmb;
--Person type borrower's information if origination=true
Cursor PersonBusOrigSet
is
    Select bor.*, 
    p.PerPhyAddrStr1NM||' '||p.PerPhyAddrStr2Nm||' '||p.PerPhyAddrCtyNm||' , '||p.PerPhyAddrStCd||' '||p.PerPhyAddrZipCd as Address,
    p.PerPhyAddrStCd, 
    UTL_RAW.CAST_TO_VARCHAR2(p.PerFirstNm)||' '||cast(p.PerInitialNm as varchar2(2))||' '||UTL_RAW.CAST_TO_VARCHAR2(p.PerLastNm)||' '||cast(p.PerSfxNm as varchar2(5)) as Person,
    null as BusTrdNm  
    From loanapp.LoanBorrTbl bor Join loanapp.LoanPerTbl p 
    On (bor.TAXID = p.TaxId And bor.LoanAppNmb = p.LoanAppNmb)
    Where bor.BorrBusPerInd = 'P' 
    and bor.LOANAPPNMB=p_LoanAppNmb;    
--Person type borrower's information if origination=false
Cursor PersonBusSet
is
    Select bor.*, 
    p.PerPhyAddrStr1NM||' '||p.PerPhyAddrStr2Nm||' '||p.PerPhyAddrCtyNm||' , '||p.PerPhyAddrStCd||' '||p.PerPhyAddrZipCd as Address,
    p.PerPhyAddrStCd, 
    UTL_RAW.CAST_TO_VARCHAR2(p.PerFirstNm)||' '||cast(p.PerInitialNm as varchar2(2))||' '||UTL_RAW.CAST_TO_VARCHAR2(p.PerLastNm)||' '||cast(p.PerSfxNm as varchar2(5)) as Person,
    null as BusTrdNm  
    From loan.LoanGntyBorrTbl bor Join loan.PerTbl p 
    On (bor.TAXID = p.TaxId)
    Where bor.BorrBusPerInd = 'P' 
    and bor.LOANAPPNMB=p_LoanAppNmb; 
    
--Check if OC is Guarantor
Cursor OCGuarantor
is
    Select g.*, 
    b.BusPhyAddrStr1NM||' '||b.BusPhyAddrStr2Nm||' '||b.BusPhyAddrCtyNm||' , '||b.BusPhyAddrStCd||' '||b.BusPhyAddrZipCd as Address,
    b.BusPhyAddrStCd as State,
    b.BusNm as Name,
    b.BusTrdNm  
    From LoanApp.LoanGuarTbl g Join LoanApp.LoanBusTbl b 
    On (g.TaxId = b.TaxId And g.LoanAppNmb = b.LoanAppNmb)
    Where g.GUARBUSPERIND = 'B'
    And g.LoanGuarOperCoInd = 'Y'
    and g.LOANAPPNMB=p_LoanAppNmb;
    
Cursor OCGuarantor1
is
    Select g.*, 
    b.BusPhyAddrStr1NM||' '||b.BusPhyAddrStr2Nm||' '||b.BusPhyAddrCtyNm||' , '||b.BusPhyAddrStCd||' '||b.BusPhyAddrZipCd as Address,
    b.BusPhyAddrStCd as State,
    b.BusNm as Name,
    b.BusTrdNm  
    From Loan.LOANGNTYGUARTBL g Join Loan.BusTbl b 
    On (g.TaxId = b.TaxId)
    Where g.GUARBUSPERIND = 'B'
    And g.LoanGuarOperCoInd = 'Y'
    and g.LOANAPPNMB=p_LoanAppNmb;
    
  
borrower_orig_rec BorrowerBusOrigSet%rowtype;
borrower_rec BorrowerBusSet%rowtype;
person_orig_rec BorrowerBusOrigSet%rowtype;
person_rec BorrowerBusSet%rowtype;
ocguarantor_rec OCGuarantor%rowtype;
ocguarantor_rec1 OCGuarantor1%rowtype;





begin
    SAVEPOINT LOANAUTHDOCCSP;
    -- to check if loan is in originaiton or servicing
    p_default_string:='[MISSING DATA]';
    select LOANNMB into loannmb_in from LOANAPP.LOANAPPTBL
    where LOANAPPNMB=p_LoanAppNmb;
    if(loannmb_in is null) then
        origstate:=true;
    else
        origstate:= false;
    end if;

    --checks if in origination or servicing
    if origstate=true then
    --cursor to retrieve Loan information
        open p_SelCur1 for
            select cast(nvl(LoanNmb,p_default_string)as CHAR(25))  as LOANNMB,
            cast(nvl(LoanAppNm,p_default_string) as CHAR(25)) as LOANAPPNM
            from LOANAPP.LOANAPPTBL
            where LoanappNmb=p_LoanAppNmb;
        select * into loanapp_table_rec from LOANAPP.LOANAPPTBL  where LoanappNmb=p_LoanAppNmb;

    else
        open p_SelCur1 for
            select cast(nvl(LoanNmb,p_default_string)as CHAR(25))  as LOANNMB,
            cast(nvl(LoanAppNm,p_default_string) as CHAR(25)) as LOANAPPNM
            from LOAN.LOANGNTYTBL
            where LoanappNmb=p_LoanAppNmb;
        select * into loan_table_rec from LOAN.LOANGNTYTBL  where LoanappNmb=p_LoanAppNmb;
    end if;



    --cursor to retrieve LOANAPPPRTTBL information
    open p_SelCur2 for
        select cast(nvl(LoanAppPrtNm,p_default_string)as VARCHAR2(200)) as LOANAPPPRTNM,
        cast(nvl(LoanAppFirsNmb,p_default_string)as CHAR(25)) as LOANAPPFIRSNMB,
        cast(nvl(LoanAppPrtStr1,p_default_string) as VARCHAR2(80)) as LOANAPPPRTSTR1,
        cast(nvl(LoanAppPrtStr2,p_default_string) as VARCHAR2(80)) as LOANAPPPRTSTR2,
        cast(nvl(LoanAppPrtCtyNm,p_default_string) as VARCHAR2(40)) as LOANAPPPRTCTYNM,
        cast(nvl(LoanAppPrtStCd,p_default_string) as CHAR(25)) as LOANAPPPRTSTCD,
        cast(nvl(LoanAppPrtZipCd,p_default_string) as CHAR(25)) as LOANAPPPRTZIPCD
        from LOANAPP.LOANAPPPRTTBL	where LoanAppNmb=p_LoanAppNmb;

    -- cursor to retrieve OFCCD information
    if origstate=true then
        sbaofcd:=loanapp_table_rec.LOANAPPORIGNTNOFCCD;
        open p_SelCur3 for
            select cast(nvl(SBAOFC1NM,p_default_string)as VARCHAR2(80)) as SBAOFC1NM,
            cast(nvl(SBAOFCSTRNM,p_default_string) as VARCHAR2(80))as SBAOFCSTRNM ,
            cast(nvl(SBAOFCSTR2NM,p_default_string) as VARCHAR2(80))as SBAOFCSTR2NM ,
            cast(nvl(SBAOFCCTYNM,p_default_string)as VARCHAR2(80)) as SBAOFCCTYNM,
            cast(nvl(STCD,p_default_string)as CHAR(2)) as STCD,
            cast(nvl(ZIPCD5,p_default_string)as CHAR(5)) as ZIPCD5
            from SBAREF.SBAOFCTBL where SBAOFCCD = sbaofcd;
    else
        sbaofcd:=loan_table_rec.LOANAPPORIGNTNOFCCD;
        open p_SelCur3 for
            select cast(nvl(SBAOFC1NM,p_default_string)as VARCHAR2(80)) as SBAOFC1NM,
            cast(nvl(SBAOFCSTRNM,p_default_string) as VARCHAR2(80))as SBAOFCSTRNM ,
            cast(nvl(SBAOFCSTR2NM,p_default_string) as VARCHAR2(80))as SBAOFCSTR2NM ,
            cast(nvl(SBAOFCCTYNM,p_default_string)as VARCHAR2(80)) as SBAOFCCTYNM,
            cast(nvl(STCD,p_default_string)as CHAR(2)),
            cast(nvl(ZIPCD5,p_default_string)as CHAR(5)) from SBAREF.SBAOFCTBL where SBAOFCCD = sbaofcd;
    end if;
    --output variable for TermInYears
    if origstate=true then
        p_TermInYears:=loanapp_table_rec.LOANAPPRQSTMATMOQTY/12;
    else
        p_TermInYears:=loan_table_rec.LOANAPPRQSTMATMOQTY/12;
    end if;

    --Debenture amount
    if origstate=true then
        p_LoanNmb:=null;
        p_AppvDt:=null;
        p_DebentureAmt:=loanapp_table_rec.LOANAPPRQSTAMT;
    else
        p_LoanNmb:=loan_table_rec.LOANNMB;
        p_AppvDt:=loan_table_rec.LOANLASTAPPVDT;
        p_DebentureAmt:=loan_table_rec.LOANCURRAPPVAMT;
    end if;
    --Business type borrower's information
    dbms_output.put_line('Here!!');
    if origstate=true then
        dbms_output.put_line('Business type borrower''s information if origination=true');
        open BorrowerBusOrigSet;
        loop
            fetch BorrowerBusOrigSet into borrower_orig_rec;
            exit when BorrowerBusOrigSet%notfound;
            dbms_output.put_line(' from 1st here: '||borrower_orig_rec.ADDRESS);
        end loop;
        close BorrowerBusOrigSet;
        /*insert into LOANAPP.ORIG_BUS_BOR
       (LOANAPPNMB, BORRSEQNMB, LOANBUSPRIMBORRIND, 
       BORRBUSPERIND, IMEPCOPERCD, BORRCREATUSERID, 
       BORRCREATDT, LASTUPDTUSERID, LASTUPDTDT, 
       LOANCNTLINTIND, GNDRCD, ETHNICCD, 
       LOANCNTLINTTYP, LIABINSURRQRDIND, PRODLIABINSURRQRDIND, 
       LIQLIABINSURRQRDIND, MALPRCTSINSURRQRDIND, OTHINSURRQRDIND, 
       OTHINSURDESCTXT, PYMTLESSCRDTIND, RECRDSTMTTYPCD, 
       RECRDSTMTYRENDDAYNMB, WORKRSCOMPINSRQRDIND, TAXID, 
       ADDRESS, BUSPHYADDRSTCD, BUSNM, 
       BUSTRDNM) values 
       (borrower_orig_rec.LOANAPPNMB, borrower_orig_rec.BORRSEQNMB, borrower_orig_rec.LOANBUSPRIMBORRIND, 
       borrower_orig_rec.BORRBUSPERIND, borrower_orig_rec.IMEPCOPERCD, borrower_orig_rec.BORRCREATUSERID, 
       borrower_orig_rec.BORRCREATDT, borrower_orig_rec.LASTUPDTUSERID, borrower_orig_rec.LASTUPDTDT, 
       borrower_orig_rec.LOANCNTLINTIND, borrower_orig_rec.GNDRCD, borrower_orig_rec.ETHNICCD, 
       borrower_orig_rec.LOANCNTLINTTYP, borrower_orig_rec.LIABINSURRQRDIND, borrower_orig_rec.PRODLIABINSURRQRDIND, 
       borrower_orig_rec.LIQLIABINSURRQRDIND, borrower_orig_rec.MALPRCTSINSURRQRDIND, borrower_orig_rec.OTHINSURRQRDIND,
       borrower_orig_rec.OTHINSURDESCTXT, borrower_orig_rec.PYMTLESSCRDTIND, borrower_orig_rec.RECRDSTMTTYPCD, 
       borrower_orig_rec.RECRDSTMTYRENDDAYNMB, borrower_orig_rec.WORKRSCOMPINSRQRDIND, borrower_orig_rec.TAXID,
       borrower_orig_rec.ADDRESS, borrower_orig_rec.BUSPHYADDRSTCD, borrower_orig_rec.BUSNM, 
       borrower_orig_rec.BUSTRDNM);*/
    else
        dbms_output.put_line('Business type borrower''s information if origination=false');
        open BorrowerBusSet;
        loop
            fetch BorrowerBusSet into borrower_rec;
            exit when BorrowerBusSet%notfound;
            dbms_output.put_line('LOANAPPNMB from 2nd: '||borrower_rec.LOANAPPNMB);
        end loop;
        close BorrowerBusSet;
        /*INSERT INTO LOAN.BUS_BOR (
       LOANAPPNMB, BORRSEQNMB, TAXID, 
       LOANBUSPRIMBORRIND, BORRBUSPERIND, IMEPCOPERCD, 
       LOANBORRACTVINACTIND, OUTLAWCD, OUTLAWOTHRDSCTXT, 
       BORRCREATUSERID, BORRCREATDT, LOANBORRLASTUPDTUSERID, 
       LOANBORRLASTUPDTDT, LOANCNTLINTIND, LOANCNTLINTTYP, 
       LIABINSURRQRDIND, PRODLIABINSURRQRDIND, LIQLIABINSURRQRDIND, 
       MALPRCTSINSURRQRDIND, OTHINSURRQRDIND, OTHINSURDESCTXT, 
       PYMTLESSCRDTIND, RECRDSTMTTYPCD, RECRDSTMTYRENDDAYNMB, 
       ADVERCHNGIND, WORKRSCOMPINSRQRDIND, BUSPHYADDRSTCD, 
       BUSNM, BUSTRDNM)  values 
    (  borrower_rec.LOANAPPNMB,borrower_rec.BORRSEQNMB,borrower_rec.TAXID,
       borrower_rec.LOANBUSPRIMBORRIND,borrower_rec.BORRBUSPERIND,borrower_rec.IMEPCOPERCD,
       borrower_rec.LOANBORRACTVINACTIND,borrower_rec.OUTLAWCD,borrower_rec.OUTLAWOTHRDSCTXT,
       borrower_rec.BORRCREATUSERID,borrower_rec.BORRCREATDT,borrower_rec.LOANBORRLASTUPDTUSERID,
       borrower_rec.LOANBORRLASTUPDTDT,borrower_rec.LOANCNTLINTIND,borrower_rec.LOANCNTLINTTYP,
       borrower_rec.LIABINSURRQRDIND,borrower_rec.PRODLIABINSURRQRDIND,borrower_rec.LIQLIABINSURRQRDIND,
       borrower_rec.MALPRCTSINSURRQRDIND,borrower_rec.OTHINSURRQRDIND,borrower_rec.OTHINSURDESCTXT,
       borrower_rec.PYMTLESSCRDTIND,borrower_rec.RECRDSTMTTYPCD,borrower_rec.RECRDSTMTYRENDDAYNMB,
       borrower_rec.ADVERCHNGIND,borrower_rec.WORKRSCOMPINSRQRDIND,borrower_rec.BUSPHYADDRSTCD,
       borrower_rec.BUSNM,borrower_rec. BUSTRDNM);*/

    end if;
    
    --Person type borrower's information
    dbms_output.put_line('Here!!');
    if origstate=true then
        dbms_output.put_line('Person type borrower''s information if origination=true');
        open PersonBusOrigSet;
        loop
            fetch PersonBusOrigSet into person_orig_rec;
            exit when PersonBusOrigSet%notfound;
            dbms_output.put_line('Address from 1st: '||person_orig_rec.Address);
        end loop;
        close PersonBusOrigSet;
       /*insert into LOANAPP.ORIG_PER_BOR
       (LOANAPPNMB, BORRSEQNMB, LOANBUSPRIMBORRIND, 
       BORRBUSPERIND, IMEPCOPERCD, BORRCREATUSERID, 
       BORRCREATDT, LASTUPDTUSERID, LASTUPDTDT, 
       LOANCNTLINTIND, GNDRCD, ETHNICCD, 
       LOANCNTLINTTYP, LIABINSURRQRDIND, PRODLIABINSURRQRDIND, 
       LIQLIABINSURRQRDIND, MALPRCTSINSURRQRDIND, OTHINSURRQRDIND, 
       OTHINSURDESCTXT, PYMTLESSCRDTIND, RECRDSTMTTYPCD, 
       RECRDSTMTYRENDDAYNMB, WORKRSCOMPINSRQRDIND, TAXID, 
       ADDRESS, PERPHYADDRSTCD, PERNAME, 
       BUSTRDNM) values 
       (person_orig_rec.LOANAPPNMB, person_orig_rec.BORRSEQNMB, person_orig_rec.LOANBUSPRIMBORRIND, 
       person_orig_rec.BORRBUSPERIND, person_orig_rec.IMEPCOPERCD, person_orig_rec.BORRCREATUSERID, 
       person_orig_rec.BORRCREATDT, person_orig_rec.LASTUPDTUSERID, person_orig_rec.LASTUPDTDT, 
       person_orig_rec.LOANCNTLINTIND, person_orig_rec.GNDRCD, person_orig_rec.ETHNICCD, 
       person_orig_rec.LOANCNTLINTTYP, person_orig_rec.LIABINSURRQRDIND, person_orig_rec.PRODLIABINSURRQRDIND, 
       person_orig_rec.LIQLIABINSURRQRDIND, person_orig_rec.MALPRCTSINSURRQRDIND, person_orig_rec.OTHINSURRQRDIND,
       person_orig_rec.OTHINSURDESCTXT, person_orig_rec.PYMTLESSCRDTIND, person_orig_rec.RECRDSTMTTYPCD, 
       person_orig_rec.RECRDSTMTYRENDDAYNMB, person_orig_rec.WORKRSCOMPINSRQRDIND, person_orig_rec.TAXID,
       person_orig_rec.ADDRESS, person_orig_rec.PERPHYADDRSTCD, person_orig_rec.Person, 
       person_orig_rec.BUSTRDNM);*/
        
    else
        dbms_output.put_line('Person type borrower''s information if origination=false');
        open PersonBusSet;
        loop
            fetch PersonBusSet into person_rec;
            exit when PersonBusSet%notfound;
            dbms_output.put_line('LOANAPPNMB from 2nd: '||person_rec.LOANAPPNMB);
        end loop;
        close PersonBusSet;
        /*INSERT INTO LOAN.PER_BOR (
           LOANAPPNMB, BORRSEQNMB, TAXID, 
           LOANBUSPRIMBORRIND, BORRBUSPERIND, IMEPCOPERCD, 
           LOANBORRACTVINACTIND, OUTLAWCD, OUTLAWOTHRDSCTXT, 
           BORRCREATUSERID, BORRCREATDT, LOANBORRLASTUPDTUSERID, 
           LOANBORRLASTUPDTDT, LOANCNTLINTIND, LOANCNTLINTTYP, 
           LIABINSURRQRDIND, PRODLIABINSURRQRDIND, LIQLIABINSURRQRDIND, 
           MALPRCTSINSURRQRDIND, OTHINSURRQRDIND, OTHINSURDESCTXT, 
           PYMTLESSCRDTIND, RECRDSTMTTYPCD, RECRDSTMTYRENDDAYNMB, 
           ADVERCHNGIND, WORKRSCOMPINSRQRDIND, PERPHYADDRSTCD, 
           PERNAME, BUSTRDNM)  values 
        (  person_rec.LOANAPPNMB,person_rec.BORRSEQNMB,person_rec.TAXID,
           person_rec.LOANBUSPRIMBORRIND,person_rec.BORRBUSPERIND,person_rec.IMEPCOPERCD,
           person_rec.LOANBORRACTVINACTIND,person_rec.OUTLAWCD,person_rec.OUTLAWOTHRDSCTXT,
           person_rec.BORRCREATUSERID,person_rec.BORRCREATDT,person_rec.LOANBORRLASTUPDTUSERID,
           person_rec.LOANBORRLASTUPDTDT,person_rec.LOANCNTLINTIND,person_rec.LOANCNTLINTTYP,
           person_rec.LIABINSURRQRDIND,person_rec.PRODLIABINSURRQRDIND,person_rec.LIQLIABINSURRQRDIND,
           person_rec.MALPRCTSINSURRQRDIND,person_rec.OTHINSURRQRDIND,person_rec.OTHINSURDESCTXT,
           person_rec.PYMTLESSCRDTIND,person_rec.RECRDSTMTTYPCD,person_rec.RECRDSTMTYRENDDAYNMB,
           person_rec.ADVERCHNGIND,person_rec.WORKRSCOMPINSRQRDIND,person_rec.PERPHYADDRSTCD,
           person_rec.Person,person_rec. BUSTRDNM) ;*/

    end if;
    
    -- If EPC transaction, then need to check if OC is guarantor. OC will always be a business. 
    if origstate=true then
        if(loanapp_table_rec.LOANAPPEPCIND = 'Y') then 
            open OCGuarantor;
            loop
                fetch OCGuarantor into ocguarantor_rec;
                exit when OCGuarantor%notfound;
                if(OCGuarantor%rowcount >0) then
                    EPCText:= 'Guarantor Operating Company:';
                end if;   
            end loop;
            close OCGuarantor;    
        end if;
    else
        if(loan_table_rec.LOANAPPEPCIND='Y') then
            open OCGuarantor1;
            loop
                fetch OCGuarantor1 into ocguarantor_rec1;
                exit when OCGuarantor1%notfound;
                if(OCGuarantor1%rowcount >0) then
                    EPCText:= 'Guarantor Operating Company:';
                end if;
            end loop;
            close OCGuarantor1;
        end if;
    end if;       
        
                

        

   
EXCEPTION
WHEN OTHERS THEN
					BEGIN
					ROLLBACK TO LOANAUTHDOCCSP;
					RAISE;
					END;

end;
/

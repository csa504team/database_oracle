INSERT INTO LOANAPP.LOANVALIDATIONERRTBL (
   ERRCD, ERRTYP, ERRTXT, 
   LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, LASTUPDTUSERID, 
   LASTUPDTDT, LOANSECTNTYPCD) 
VALUES ( 4238,
 1,
 'For Agent^1, Loan packaging has an amount paid by Applicant and SBA Lender. The loan packaging amount can only be paid by applicant or lender, not both.',
 user,
 sysdate,
 user,
 sysdate,
 NULL );
 commit;
 
 INSERT INTO LOANAPP.LOANVALIDATIONERRTBL (
   ERRCD, ERRTYP, ERRTXT, 
   LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, LASTUPDTUSERID, 
   LASTUPDTDT, LOANSECTNTYPCD) 
VALUES ( 4239,
 1,
 'For Agent^1, Financial statement preparation has an amount paid by Applicant and SBA Lender. The Financial statement preparation amount can only be paid by applicant or lender, not both.',
 user,
 sysdate,
 user,
 sysdate,
 NULL );
 
 
 INSERT INTO LOANAPP.LOANVALIDATIONERRTBL (
   ERRCD, ERRTYP, ERRTXT, 
   LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, LASTUPDTUSERID, 
   LASTUPDTDT, LOANSECTNTYPCD) 
VALUES ( 4240,
 1,
 'For Agent^1, Broker or Referral services has an amount paid by Applicant and SBA Lender. The Broker or Referral services amount can only be paid by applicant or lender, not both.',
 user,
 sysdate,
 user,
 sysdate,
 NULL );
 
  INSERT INTO LOANAPP.LOANVALIDATIONERRTBL (
   ERRCD, ERRTYP, ERRTXT, 
   LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, LASTUPDTUSERID, 
   LASTUPDTDT, LOANSECTNTYPCD) 
VALUES ( 4241,
 1,
 'For Agent^1, Consultant services has an amount paid by Applicant and SBA Lender. The Consultant services amount can only be paid by applicant or lender, not both.',
 user,
 sysdate,
 user,
 sysdate,
 NULL );
 
INSERT INTO LOANAPP.LOANVALIDATIONERRTBL (
   ERRCD, ERRTYP, ERRTXT, 
   LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, LASTUPDTUSERID, 
   LASTUPDTDT, LOANSECTNTYPCD) 
VALUES ( 4242,
 1,
 'For Agent^1, Other has an amount paid by Applicant and SBA Lender. The Other amount can only be paid by applicant or lender, not both.',
 user,
 sysdate,
 user,
 sysdate,
 NULL );
 commit;
 
INSERT INTO LOANAPP.LOANVALIDATIONERRTBL (
   ERRCD, ERRTYP, ERRTXT, 
   LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, LASTUPDTUSERID, 
   LASTUPDTDT, LOANSECTNTYPCD) 
VALUES ( 4243,
 1,
 'For  Agent^1,  Description is mandatory for other type of agent.',
 user,
 sysdate,
 user,
 sysdate,
 NULL );
 
 
INSERT INTO LOANAPP.LOANVALIDATIONERRTBL (
   ERRCD, ERRTYP, ERRTXT, 
   LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, LASTUPDTUSERID, 
   LASTUPDTDT, LOANSECTNTYPCD) 
VALUES ( 4245,
 1,
 'For Agent^1, type of service amount exceeds 5 figures for Loan Packaging service.',
 user,
 sysdate,
 user,
 sysdate,
 NULL );
 
INSERT INTO LOANAPP.LOANVALIDATIONERRTBL (
   ERRCD, ERRTYP, ERRTXT, 
   LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, LASTUPDTUSERID, 
   LASTUPDTDT, LOANSECTNTYPCD) 
VALUES ( 4246,
 1,
 'For Agent^1, type of service amount exceeds 5 figures for Financial Statement service.',
 user,
 sysdate,
 user,
 sysdate,
 NULL );
 
INSERT INTO LOANAPP.LOANVALIDATIONERRTBL (
   ERRCD, ERRTYP, ERRTXT, 
   LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, LASTUPDTUSERID, 
   LASTUPDTDT, LOANSECTNTYPCD) 
VALUES ( 4247,
 1,
 'For Agent^1, type of service amount exceeds 5 figures for Broker or Referal services.',
 user,
 sysdate,
 user,
 sysdate,
 NULL );

INSERT INTO LOANAPP.LOANVALIDATIONERRTBL (
   ERRCD, ERRTYP, ERRTXT, 
   LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, LASTUPDTUSERID, 
   LASTUPDTDT, LOANSECTNTYPCD) 
VALUES ( 4248,
 1,
 'For Agent^1, type of service amount exceeds 5 figures for Consultant services.',
 user,
 sysdate,
 user,
 sysdate,
 NULL );
 
INSERT INTO LOANAPP.LOANVALIDATIONERRTBL (
   ERRCD, ERRTYP, ERRTXT, 
   LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, LASTUPDTUSERID, 
   LASTUPDTDT, LOANSECTNTYPCD) 
VALUES ( 4249,
 1,
 'For Agent^1, type of service amount exceeds 5 figures for Other services.',
 user,
 sysdate,
 user,
 sysdate,
 NULL );
 commit;
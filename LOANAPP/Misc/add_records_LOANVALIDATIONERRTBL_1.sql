INSERT INTO LOANAPP.LOANVALIDATIONERRTBL (
   ERRCD, ERRTYP, ERRTXT, 
   LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, LASTUPDTUSERID, 
   LASTUPDTDT, LOANSECTNTYPCD) 
VALUES ( 4257,
 1,
 'For Agent^1, Amount paid for services should not be blank or non-numeric.',
 user,
 sysdate,
 user,
 sysdate,
 NULL );
 commit;
 
 
 INSERT INTO LOANAPP.LOANVALIDATIONERRTBL (
   ERRCD, ERRTYP, ERRTXT, 
   LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, LASTUPDTUSERID, 
   LASTUPDTDT, LOANSECTNTYPCD) 
VALUES ( 4258,
 1,
 'For Agent^1, Agent Zipcode must contain exactly 5 characters.',
 user,
 sysdate,
 user,
 sysdate,
 NULL );
 commit;
 
INSERT INTO LOANAPP.LOANVALIDATIONERRTBL (
   ERRCD, ERRTYP, ERRTXT, 
   LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, LASTUPDTUSERID, 
   LASTUPDTDT, LOANSECTNTYPCD) 
VALUES ( 4243,
 1,
 'For  Agent^1,  Description is mandatory for other type of agent.',
 user,
 sysdate,
 user,
 sysdate,
 NULL );
 
 
 
INSERT INTO LOANAPP.LOANVALIDATIONERRTBL (
   ERRCD, ERRTYP, ERRTXT, 
   LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, LASTUPDTUSERID, 
   LASTUPDTDT, LOANSECTNTYPCD) 
VALUES ( 4245,
 1,
 'For Agent^1, type of service amount exceeds 5 figures for Loan Packaging service.',
 user,
 sysdate,
 user,
 sysdate,
 NULL );
 
INSERT INTO LOANAPP.LOANVALIDATIONERRTBL (
   ERRCD, ERRTYP, ERRTXT, 
   LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, LASTUPDTUSERID, 
   LASTUPDTDT, LOANSECTNTYPCD) 
VALUES ( 4246,
 1,
 'For Agent^1, type of service amount exceeds 5 figures for Financial Statement service.',
 user,
 sysdate,
 user,
 sysdate,
 NULL );
 
INSERT INTO LOANAPP.LOANVALIDATIONERRTBL (
   ERRCD, ERRTYP, ERRTXT, 
   LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, LASTUPDTUSERID, 
   LASTUPDTDT, LOANSECTNTYPCD) 
VALUES ( 4247,
 1,
 'For Agent^1, type of service amount exceeds 5 figures for Broker or Referal services.',
 user,
 sysdate,
 user,
 sysdate,
 NULL );

INSERT INTO LOANAPP.LOANVALIDATIONERRTBL (
   ERRCD, ERRTYP, ERRTXT, 
   LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, LASTUPDTUSERID, 
   LASTUPDTDT, LOANSECTNTYPCD) 
VALUES ( 4248,
 1,
 'For Agent^1, type of service amount exceeds 5 figures for Consultant services.',
 user,
 sysdate,
 user,
 sysdate,
 NULL );
 
INSERT INTO LOANAPP.LOANVALIDATIONERRTBL (
   ERRCD, ERRTYP, ERRTXT, 
   LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, LASTUPDTUSERID, 
   LASTUPDTDT, LOANSECTNTYPCD) 
VALUES ( 4249,
 1,
 'For Agent^1, type of service amount exceeds 5 figures for Other services.',
 user,
 sysdate,
 user,
 sysdate,
 NULL );
 commit;
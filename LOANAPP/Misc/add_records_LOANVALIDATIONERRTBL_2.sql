INSERT INTO LOANAPP.LOANVALIDATIONERRTBL (
   ERRCD, ERRTYP, ERRTXT, 
   LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, LASTUPDTUSERID, 
   LASTUPDTDT, LOANSECTNTYPCD) 
VALUES ( 4251,
 1,
 'For Agent^1, the total compensation amount paid by SBA Lender exceeds $2,500, itemization and supporting documentation is required.',
 user,
 sysdate,
 user,
 sysdate,
 NULL );
 commit;
 
 
UPDATE LOANAPP.LOANVALIDATIONERRTBL
 SET ERRTXT = 'For Agent^1, the total compensation amount paid by Applicant exceeds $2,500, itemization and supporting documentation is required.'
 WHERE ERRCD=4244;
 commit;
 
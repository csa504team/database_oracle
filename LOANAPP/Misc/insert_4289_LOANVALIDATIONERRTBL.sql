SET DEFINE OFF;
Insert into LOANAPP.LOANVALIDATIONERRTBL
   (ERRCD, ERRTYP, ERRTXT, LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, 
    LASTUPDTUSERID, LASTUPDTDT, LOANSECTNTYPCD)
 Values
   (4289, 1, 'For application, sum of sub proceeds for proceed type ^1 must equal the proceed amount ^3.', user, sysdate, 
    user, sysdate, NULL);
Insert into LOANAPP.LOANVALIDATIONERRTBL
   (ERRCD, ERRTYP, ERRTXT, LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, 
    LASTUPDTUSERID, LASTUPDTDT, LOANSECTNTYPCD)
 Values
   (4289, 2, 'For application, sum of sub proceeds for proceed type ^1 must equal the proceed amount ^3.', user, sysdate, 
    user, sysdate, NULL);
COMMIT;

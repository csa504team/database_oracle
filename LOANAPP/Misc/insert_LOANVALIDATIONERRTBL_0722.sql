Insert into LOANAPP.LOANVALIDATIONERRTBL
   (ERRCD, ERRTYP, ERRTXT, LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, 
    LASTUPDTUSERID, LASTUPDTDT, LOANSECTNTYPCD)
 Values
   ('4293', '1', 'ALP Express delegated authority may not be used to process 504 loans previously screened out or declined by SLPC or decline by another CDC.', 'CNTRJGOROWADA', '07-JUL-21', 
    'CNTRJGOROWADA', '07-JUL-21', 'A');
Insert into LOANAPP.LOANVALIDATIONERRTBL
   (ERRCD, ERRTYP, ERRTXT, LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, 
    LASTUPDTUSERID, LASTUPDTDT, LOANSECTNTYPCD)
 Values
   ('4293', '2', 'ALP Express delegated authority may not be used to process 504 loans previously screened out or declined by SLPC or decline by another CDC.', 'CNTRJGOROWADA', '07-JUL-21', 
    'CNTRJGOROWADA', '07-JUL-21', '');
COMMIT;

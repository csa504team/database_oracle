Insert into LOANAPP.LOANVALIDATIONERRTBL
   (ERRCD, ERRTYP, ERRTXT, LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, 
    LASTUPDTUSERID, LASTUPDTDT, LOANSECTNTYPCD)
 Values
   ('4293', '1', 'For Application, The SBA Guaranty percentage ^3 multiplied by Requested Amount ^2 exceeds maximum ^1', user, sysdate, 
    user, sysdate, 'A');
Insert into LOANAPP.LOANVALIDATIONERRTBL
   (ERRCD, ERRTYP, ERRTXT, LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, 
    LASTUPDTUSERID, LASTUPDTDT, LOANSECTNTYPCD)
 Values
   ('4293', '2', 'For Application, The SBA Guaranty percentage ^3 multiplied by Requested Amount ^2 exceeds maximum ^1', user, sysdate, 
    user, sysdate, '');
Insert into LOANAPP.LOANVALIDATIONERRTBL
   (ERRCD, ERRTYP, ERRTXT, LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, 
    LASTUPDTUSERID, LASTUPDTDT, LOANSECTNTYPCD)
 Values
   ('4294', '1', 'For Application, ALP Express Loans may not be made for NAICS Code ^1', user, sysdate, 
    user, sysdate, 'A');
Insert into LOANAPP.LOANVALIDATIONERRTBL
   (ERRCD, ERRTYP, ERRTXT, LOANVALIDERRCREATUSERID, LOANVALIDERRCREATDT, 
    LASTUPDTUSERID, LASTUPDTDT, LOANSECTNTYPCD)
 Values
   ('4294', '2', 'For Application, ALP Express Loans may not be made for NAICS Code ^1', user, sysdate, 
    user, sysdate, '');
COMMIT;

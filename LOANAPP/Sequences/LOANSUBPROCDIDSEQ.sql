CREATE SEQUENCE LOANAPP.LOANSUBPROCDIDSEQ
  START WITH 133
  MAXVALUE 9999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  NOCACHE
  ORDER
  NOKEEP
  GLOBAL;

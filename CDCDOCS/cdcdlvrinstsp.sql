CREATE OR REPLACE PROCEDURE cdcdocs.cdcdlvrinstsp( 
p_identifier	NUMBER := NULL
, p_retval		OUT NUMBER
, p_cdcdlvrid	OUT NUMBER
, p_dlvrid		NUMBER := NULL
, p_cdcregncd	CHAR
, p_cdcnmb		CHAR
, p_actvind		CHAR := NULL
, p_creatuserid	VARCHAR2 := NULL)
AS
BEGIN
	SAVEPOINT cdcdlvrinstsp;
	p_cdcdlvrid := cdcdlvridseq.NEXTVAL;
	IF p_identifier = 0 THEN
		BEGIN
			INSERT INTO cdcdocs.cdcdlvrtbl
			(
				cdcdlvrid
				, dlvrid
				, cdcregncd
				, cdcnmb
				, actvind
				, creatuserid
				, creatdt
				, lastupduserid
				, lastupddt
			)
			VALUES
			(
				p_cdcdlvrid
				, p_dlvrid
				, p_cdcregncd
				, p_cdcnmb
				, p_actvind
				, p_creatuserid
				, SYSDATE
				, p_creatuserid
				, SYSDATE
			);
			
			p_retval := SQL % rowcount;
		END ;
	END IF;
EXCEPTION
WHEN OTHERS THEN
	BEGIN
		ROLLBACK TO cdcdlvrinstsp;
		RAISE;
	END ;
END ;
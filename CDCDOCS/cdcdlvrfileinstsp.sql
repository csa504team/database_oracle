CREATE OR REPLACE PROCEDURE cdcdocs.cdcdlvrfileinstsp( 
p_identifier	NUMBER := NULL
, p_retval		OUT NUMBER
, p_cdcdlvrid	NUMBER := NULL
, p_dlvrid		NUMBER := NULL
, p_actvind		CHAR := NULL
, p_creatuserid	VARCHAR2 := NULL)
AS
BEGIN
	SAVEPOINT cdcdlvrfileinstsp;

	IF p_identifier = 0 THEN
		BEGIN
			INSERT INTO cdcdocs.cdcdlvrfiletbl
			(
				cdcdlvrid
				, dlvrid
				, actvind
				, creatuserid
				, creatdt
				, lastupduserid
				, lastupddt
			)
			VALUES
			(
				p_cdcdlvrid
				, p_dlvrid
				, p_actvind
				, p_creatuserid
				, SYSDATE
				, p_creatuserid
				, SYSDATE
			);
			
			p_retval := SQL % rowcount;
		END ;
	END IF;
EXCEPTION
WHEN OTHERS THEN
	BEGIN
		ROLLBACK TO cdcdlvrfileinstsp;
		RAISE;
	END ;
END ;
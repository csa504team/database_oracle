CREATE OR REPLACE PROCEDURE cdcdocs.dlvrseltsp( 
p_identifier	NUMBER := NULL
, p_retval		OUT NUMBER
, p_SelCur 		OUT SYS_REFCURSOR
, p_SelCur2		OUT SYS_REFCURSOR)
AS
BEGIN
	
	-- Retrieve all active deliverable names and their associated years
	IF p_identifier = 0 THEN
		BEGIN
			OPEN p_SelCur FOR
			SELECT
				dlvrid
				, dlvrnm
				, dlvrstartdt
				, dlvrenddt
			FROM
				cdcdocs.dlvrtbl
			WHERE
				TRIM(actvind) = 'Y';
				
			OPEN p_SelCur2 FOR
			SELECT
				EXTRACT(YEAR FROM dlvrstartdt) AS dlvryr
			FROM
				cdcdocs.dlvrtbl
			WHERE
				TRIM(actvind) = 'Y';
			
			p_retval := SQL % rowcount;
		END;
	END IF;
EXCEPTION
WHEN OTHERS THEN
	BEGIN
		RAISE;
	END;
END;

CREATE OR REPLACE PROCEDURE cdcdocs.cdcdlvrselcsp( 
p_identifier	NUMBER := NULL
, p_cdcregncd	CHAR
, p_cdcnmb		CHAR
, p_SelCur 		OUT SYS_REFCURSOR)
AS
BEGIN

	-- returns cdc deliverables for all cdcs
	IF p_identifier = 0 THEN
		BEGIN
			OPEN p_SelCur FOR
			SELECT
				c.cdcdlvrid
				, c.dlvrid
				, c.cdcregncd
				, c.cdcnmb
				, d.dlvrnm
				, d.dlvrstartdt
				, d.dlvrenddt
				, (SELECT COUNT(DISTINCT doc.doctypcd) FROM cdcdocs.cdcdlvrfiletbl f LEFT JOIN cdcdocs.doctbl doc ON doc.docid = f.docid WHERE f.cdcdlvrid = c.cdcdlvrid AND doc.doctypcd IN (SELECT doctypcd FROM cdcdocs.dlvrreqdoctyptbl WHERE dlvrid = c.dlvrid) AND TRIM(f.actvind) = 'Y') AS docsuploaded
				, (SELECT COUNT(doctypcd) FROM cdcdocs.dlvrreqdoctyptbl dlvrreq WHERE dlvrreq.dlvrid = c.dlvrid) AS docsrequired
			FROM
				cdcdocs.cdcdlvrtbl c
			LEFT JOIN
				cdcdocs.dlvrtbl d
			ON(d.dlvrid = c.dlvrid AND TRIM(d.actvind) = 'Y')
			WHERE
				TRIM(c.actvind) = 'Y';
		END;
	ELSIF p_Identifier = 1 THEN
		BEGIN
			OPEN p_SelCur FOR
			SELECT
				c.cdcdlvrid
				, c.dlvrid
				, c.cdcregncd
				, c.cdcnmb
				, d.dlvrnm
				, d.dlvrstartdt
				, d.dlvrenddt
				, (SELECT COUNT(DISTINCT doc.doctypcd) FROM cdcdocs.cdcdlvrfiletbl f LEFT JOIN cdcdocs.doctbl doc ON doc.docid = f.docid WHERE f.cdcdlvrid = c.cdcdlvrid AND doc.doctypcd IN (SELECT doctypcd FROM cdcdocs.dlvrreqdoctyptbl WHERE dlvrid = c.dlvrid) AND TRIM(f.actvind) = 'Y') AS docsuploaded
				, (SELECT COUNT(doctypcd) FROM cdcdocs.dlvrreqdoctyptbl dlvrreq WHERE dlvrreq.dlvrid = c.dlvrid) AS docsrequired
			FROM
				cdcdocs.cdcdlvrtbl c
			LEFT JOIN
				cdcdocs.dlvrtbl d
			ON(d.dlvrid = c.dlvrid AND TRIM(d.actvind) = 'Y')
			WHERE
				TRIM(c.actvind) = 'Y'
			AND
				TRIM(c.cdcnmb) = TRIM(p_cdcnmb)
			AND
				TRIM(c.cdcregncd) = TRIM(p_cdcregncd);
		END;
	END IF;
EXCEPTION
WHEN OTHERS THEN
	BEGIN
		RAISE;
	END;
END;

CREATE OR REPLACE PROCEDURE cdcdocs.cdcdlvrfileinstsp( 
p_identifier	NUMBER := NULL
, p_retval		OUT NUMBER
, p_cdcdlvrid	NUMBER := NULL
, p_docid		NUMBER := NULL
, p_actvind		CHAR := NULL
, p_creatuserid	VARCHAR2 := NULL)
AS
BEGIN
	SAVEPOINT cdcdlvrfileinstsp;

	IF p_identifier = 0 THEN
		BEGIN
			INSERT INTO cdcdocs.cdcdlvrfiletbl
			(
				cdcdlvrid
				, docid
				, actvind
				, creatuserid
				, creatdt
				, lastupduserid
				, lastupddt
			)
			VALUES
			(
				p_cdcdlvrid
				, p_docid
				, p_actvind
				, p_creatuserid
				, SYSDATE
				, p_creatuserid
				, SYSDATE
			);
			
			p_retval := SQL % rowcount;
		END;
	END IF;
EXCEPTION
WHEN OTHERS THEN
	BEGIN
		ROLLBACK TO cdcdlvrfileinstsp;
		RAISE;
	END;
END;

CREATE OR REPLACE PROCEDURE cdcdocs.cdcdlvrinstsp( 
p_identifier	NUMBER := NULL
, p_retval		OUT NUMBER
, p_cdcdlvrid	OUT NUMBER
, p_dlvrid		NUMBER := NULL
, p_cdcregncd	CHAR
, p_cdcnmb		CHAR
, p_actvind		CHAR := NULL
, p_creatuserid	VARCHAR2 := NULL)
AS
BEGIN
	SAVEPOINT cdcdlvrinstsp;
	p_cdcdlvrid := cdcdlvridseq.NEXTVAL;
	IF p_identifier = 0 THEN
		BEGIN
			INSERT INTO cdcdocs.cdcdlvrtbl
			(
				cdcdlvrid
				, dlvrid
				, cdcregncd
				, cdcnmb
				, actvind
				, creatuserid
				, creatdt
				, lastupduserid
				, lastupddt
			)
			VALUES
			(
				p_cdcdlvrid
				, p_dlvrid
				, p_cdcregncd
				, p_cdcnmb
				, p_actvind
				, p_creatuserid
				, SYSDATE
				, p_creatuserid
				, SYSDATE
			);
			
			p_retval := SQL % rowcount;
		END;
	END IF;
EXCEPTION
WHEN OTHERS THEN
	BEGIN
		ROLLBACK TO cdcdlvrinstsp;
		RAISE;
	END;
END;

CREATE OR REPLACE PROCEDURE cdcdocs.cdcdlvrfileupdtsp( 
p_identifier		NUMBER := NULL
, p_retval			OUT NUMBER
, p_cdcdlvrid		NUMBER := NULL
, p_docid			NUMBER := NULL
, p_cdcregncd		CHAR
, p_cdcnmb			CHAR
, p_actvind			CHAR := NULL
, p_lastupduserid	VARCHAR2 := NULL)
AS
BEGIN
	SAVEPOINT cdcdlvrfileupdtsp;

	-- Mark deliverable for a specific cdc as inactive
	IF p_identifier = 0 THEN
		BEGIN
			UPDATE 
				cdcdocs.cdcdlvrfiletbl 
			SET
				actvind = p_actvind
				, lastupduserid = p_lastupduserid
				, lastupddt = SYSDATE
			WHERE
				cdcdlvrid = p_cdcdlvrid
			AND
				docid = p_docid
			AND
				cdcregncd = p_cdcregncd
			AND
				cdcnmb = p_cdcnmb;
			
			p_retval := SQL % rowcount;
		END;
	END IF;
EXCEPTION
WHEN OTHERS THEN
	BEGIN
		ROLLBACK TO cdcdlvrfileupdtsp;
		RAISE;
	END;
END;

CREATE OR REPLACE PROCEDURE cdcdocs.cdcdlvrupdtsp( 
p_identifier		NUMBER := NULL
, p_retval			OUT NUMBER
, p_cdcdlvrid		NUMBER := NULL
, p_cdcregncd		CHAR
, p_cdcnmb			CHAR
, p_actvind			CHAR := NULL
, p_lastupduserid	VARCHAR2 := NULL)
AS
BEGIN
	SAVEPOINT cdcdlvrupdtsp;

	-- Mark deliverable for a specific cdc as inactive
	IF p_identifier = 0 THEN
		BEGIN
			UPDATE 
				cdcdocs.cdcdlvrtbl 
			SET
				actvind = p_actvind
				, lastupduserid = p_lastupduserid
				, lastupddt = SYSDATE
			WHERE
				cdcdlvrid = p_cdcdlvrid
			AND
				cdcregncd = p_cdcregncd
			AND
				cdcnmb = p_cdcnmb;
			
			p_retval := SQL % rowcount;
		END;
	END IF;
EXCEPTION
WHEN OTHERS THEN
	BEGIN
		ROLLBACK TO cdcdlvrupdtsp;
		RAISE;
	END;
END;
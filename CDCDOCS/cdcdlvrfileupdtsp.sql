CREATE OR REPLACE PROCEDURE cdcdocs.cdcdlvrfileupdtsp( 
p_identifier		NUMBER := NULL
, p_retval			OUT NUMBER
, p_cdcdlvrid		NUMBER := NULL
, p_docid			NUMBER := NULL
, p_cdcregncd		CHAR
, p_cdcnmb			CHAR
, p_actvind			CHAR := NULL
, p_lastupduserid	VARCHAR2 := NULL)
AS
BEGIN
	SAVEPOINT cdcdlvrfileupdtsp;

	-- Mark deliverable for a specific cdc as inactive
	IF p_identifier = 0 THEN
		BEGIN
			UPDATE 
				cdcdocs.cdcdlvrfiletbl 
			SET
				actvind = p_actvind
				, lastupduserid = p_lastupduserid
				, lastupddt = SYSDATE
			WHERE
				cdcdlvrid = p_cdcdlvrid
			AND
				docid = p_docid
			AND
				cdcregncd = p_cdcregncd
			AND
				cdcnmb = p_cdcnmb;
			
			p_retval := SQL % rowcount;
		END ;
	END IF;
EXCEPTION
WHEN OTHERS THEN
	BEGIN
		ROLLBACK TO cdcdlvrfileupdtsp;
		RAISE;
	END ;
END;
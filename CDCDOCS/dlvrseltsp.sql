CREATE OR REPLACE PROCEDURE cdcdocs.dlvrseltsp( 
p_identifier	NUMBER := NULL
, p_retval		OUT NUMBER
, p_SelCur 		OUT SYS_REFCURSOR
, p_SelCur2		OUT SYS_REFCURSOR)
AS
BEGIN
	
	-- Retrieve all active deliverable names and their associated years
	IF p_identifier = 0 THEN
		BEGIN
			OPEN p_SelCur FOR
			SELECT
				dlvrid
				, dlvrnm
				, dlvrstartdt
				, dlvrenddt
			FROM
				cdcdocs.dlvrtbl
			WHERE
				TRIM(actvind) = 'Y';
				
			OPEN p_SelCur2 FOR
			SELECT
				EXTRACT(YEAR FROM dlvrstartdt) AS dlvryr
			FROM
				cdcdocs.dlvrtbl
			WHERE
				TRIM(actvind) = 'Y';
			
			p_retval := SQL % rowcount;
		END ;
	END IF;
EXCEPTION
WHEN OTHERS THEN
	BEGIN
		RAISE;
	END ;
END;
GRANT SELECT, UPDATE, INSERT, DELETE ON cdcdocs.cdcdlvrfiletbl TO cdcdocsdevrole;
GRANT SELECT, UPDATE, INSERT, DELETE ON cdcdocs.cdcdlvrfiletbl TO cdconlinecorpgovprtupdate;
GRANT SELECT ON cdcdocs.cdcdlvrfiletbl TO cdconlinegovread;

GRANT SELECT, UPDATE, INSERT, DELETE ON cdcdocs.cdcdlvrtbl TO cdcdocsdevrole;
GRANT SELECT, UPDATE, INSERT, DELETE ON cdcdocs.cdcdlvrtbl TO cdconlinecorpgovprtupdate;
GRANT SELECT ON cdcdocs.cdcdlvrtbl TO cdconlinegovread;


GRANT SELECT, UPDATE, INSERT, DELETE ON cdcdocs.dlvrreqdoctyptbl TO cdcdocsdevrole;
GRANT SELECT, UPDATE, INSERT, DELETE ON cdcdocs.dlvrreqdoctyptbl TO cdconlinecorpgovprtupdate;
GRANT SELECT ON cdcdocs.dlvrreqdoctyptbl TO cdconlinegovread;

GRANT SELECT, UPDATE, INSERT, DELETE ON cdcdocs.dlvrtbl TO cdcdocsdevrole;
GRANT SELECT, UPDATE, INSERT, DELETE ON cdcdocs.dlvrtbl TO cdconlinecorpgovprtupdate;
GRANT SELECT ON cdcdocs.dlvrtbl TO cdconlinegovread;

GRANT SELECT ON cdcdocs.cdcdlvridseq TO cdcdocsdevrole;
GRANT SELECT ON cdcdocs.cdcdlvridseq TO cdconlinecorpgovprtupdate;
GRANT SELECT ON cdcdocs.cdcdlvridseq TO cdconlinegovread;

GRANT SELECT ON cdcdocs.dlvridseq TO cdcdocsdevrole;
GRANT SELECT ON cdcdocs.dlvridseq TO cdconlinecorpgovprtupdate;
GRANT SELECT ON cdcdocs.dlvridseq TO cdconlinegovread;

GRANT SELECT ON cdcdocs.dlvrreqdoctypidseq TO cdcdocsdevrole;
GRANT SELECT ON cdcdocs.dlvrreqdoctypidseq TO cdconlinecorpgovprtupdate;
GRANT SELECT ON cdcdocs.dlvrreqdoctypidseq TO cdconlinegovread;

GRANT EXECUTE ON cdcdocs.cdcdlvrinstsp TO cdcdocsdevrole;
GRANT EXECUTE ON cdcdocs.cdcdlvrinstsp TO cdconlinecorpgovprtupdate;
-- NOT NEEDED: GRANT EXECUTE ON cdcdocs.cdcdlvrinstsp TO cdconlinegovread;

GRANT EXECUTE ON cdcdocs.cdcdlvrfileinstsp TO cdcdocsdevrole;
GRANT EXECUTE ON cdcdocs.cdcdlvrfileinstsp TO cdconlinecorpgovprtupdate;
-- NOT NEEDED: GRANT EXECUTE ON cdcdocs.cdcdlvrinstsp TO cdconlinegovread;

GRANT EXECUTE ON cdcdocs.cdcdlvrupdtsp TO cdcdocsdevrole;
GRANT EXECUTE ON cdcdocs.cdcdlvrupdtsp TO cdconlinecorpgovprtupdate;
-- NOT NEEDED: GRANT EXECUTE ON cdcdocs.cdcdlvrinstsp TO cdconlinegovread;

GRANT EXECUTE ON cdcdocs.cdcdlvrfileupdtsp TO cdcdocsdevrole;
GRANT EXECUTE ON cdcdocs.cdcdlvrfileupdtsp TO cdconlinecorpgovprtupdate;
-- NOT NEEDED: GRANT EXECUTE ON cdcdocs.cdcdlvrinstsp TO cdconlinegovread;

GRANT EXECUTE ON cdcdocs.dlvrseltsp TO cdcdocsdevrole;
GRANT EXECUTE ON cdcdocs.dlvrseltsp TO cdconlinecorpgovprtupdate;
GRANT EXECUTE ON cdcdocs.dlvrseltsp TO cdconlinegovread;

GRANT EXECUTE ON cdcdocs.cdcdlvrselcsp TO cdcdocsdevrole;
GRANT EXECUTE ON cdcdocs.cdcdlvrselcsp TO cdconlinecorpgovprtupdate;
GRANT EXECUTE ON cdcdocs.cdcdlvrselcsp TO cdconlinegovread;
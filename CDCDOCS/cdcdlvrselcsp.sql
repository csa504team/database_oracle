CREATE OR REPLACE PROCEDURE cdcdocs.cdcdlvrselcsp( 
p_identifier	NUMBER := NULL
, p_cdcregncd	CHAR
, p_cdcnmb		CHAR
, p_SelCur 		OUT SYS_REFCURSOR)
AS
BEGIN

	-- returns cdc deliverables for all cdcs
	IF p_identifier = 0 THEN
		BEGIN
			OPEN p_SelCur FOR
			SELECT
				c.cdcdlvrid
				, c.dlvrid
				, c.cdcregncd
				, c.cdcnmb
				, d.dlvrnm
				, d.dlvrstartdt
				, d.dlvrenddt
				, (SELECT COUNT(DISTINCT doc.doctypcd) FROM cdcdocs.cdcdlvrfiletbl f LEFT JOIN cdcdocs.doctbl doc ON doc.docid = f.docid WHERE f.cdcdlvrid = c.cdcdlvrid AND doc.doctypcd IN (SELECT doctypcd FROM cdcdocs.dlvrreqdoctyptbl WHERE dlvrid = c.dlvrid) AND TRIM(f.actvind) = 'Y') AS docsuploaded
				, (SELECT COUNT(doctypcd) FROM cdcdocs.dlvrreqdoctyptbl dlvrreq WHERE dlvrreq.dlvrid = c.dlvrid) AS docsrequired
			FROM
				cdcdocs.cdcdlvrtbl c
			LEFT JOIN
				cdcdocs.dlvrtbl d
			ON(d.dlvrid = c.dlvrid AND TRIM(d.actvind) = 'Y')
			WHERE
				TRIM(c.actvind) = 'Y';
		END;
	ELSIF p_Identifier = 1 THEN
		BEGIN
			OPEN p_SelCur FOR
			SELECT
				c.cdcdlvrid
				, c.dlvrid
				, c.cdcregncd
				, c.cdcnmb
				, d.dlvrnm
				, d.dlvrstartdt
				, d.dlvrenddt
				, (SELECT COUNT(DISTINCT doc.doctypcd) FROM cdcdocs.cdcdlvrfiletbl f LEFT JOIN cdcdocs.doctbl doc ON doc.docid = f.docid WHERE f.cdcdlvrid = c.cdcdlvrid AND doc.doctypcd IN (SELECT doctypcd FROM cdcdocs.dlvrreqdoctyptbl WHERE dlvrid = c.dlvrid) AND TRIM(f.actvind) = 'Y') AS docsuploaded
				, (SELECT COUNT(doctypcd) FROM cdcdocs.dlvrreqdoctyptbl dlvrreq WHERE dlvrreq.dlvrid = c.dlvrid) AS docsrequired
			FROM
				cdcdocs.cdcdlvrtbl c
			LEFT JOIN
				cdcdocs.dlvrtbl d
			ON(d.dlvrid = c.dlvrid AND TRIM(d.actvind) = 'Y')
			WHERE
				TRIM(c.actvind) = 'Y'
			AND
				TRIM(c.cdcnmb) = TRIM(p_cdcnmb)
			AND
				TRIM(c.cdcregncd) = TRIM(p_cdcregncd);
		END;
	END IF;
EXCEPTION
WHEN OTHERS THEN
	BEGIN
		RAISE;
	END ;
END ;
DROP PROCEDURE STGCSA.TOGGLE_AUDITING_ON;

CREATE OR REPLACE PROCEDURE STGCSA.toggle_auditing_on as
begin
  runtime.toggle_auditing_on_YN('Y');
end;
/

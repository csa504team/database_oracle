DROP PROCEDURE STGCSA.TOGGLE_AUDITING_TEST;

CREATE OR REPLACE PROCEDURE STGCSA.toggle_auditing_test as
begin
  dbms_output.put_line('AUDITING: '||runtime.audit_logging_on_YN);
end;
/

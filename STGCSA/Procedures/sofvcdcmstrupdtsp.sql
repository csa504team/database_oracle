DROP PROCEDURE STGCSA.SOFVCDCMSTRUPDTSP;

CREATE OR REPLACE PROCEDURE STGCSA.SOFVCDCMSTRUPDTSP(
    p_retval out number,
    p_errval out number,
    p_errmsg  out varchar2,
    p_identifier number:=0,
    p_userid varchar2
   ,p_CDCREGNCD CHAR:=null
   ,p_CDCCERTNMB CHAR:=null
   ,p_CDCPOLKNMB CHAR:=null
   ,p_CDCNM VARCHAR2:=null
   ,p_CDCMAILADRSTR1NM VARCHAR2:=null
   ,p_CDCMAILADRCTYNM VARCHAR2:=null
   ,p_CDCMAILADRSTCD CHAR:=null
   ,p_CDCMAILADRZIPCD CHAR:=null
   ,p_CDCMAILADRZIP4CD CHAR:=null
   ,p_CDCCNTCT VARCHAR2:=null
   ,p_CDCAREACD CHAR:=null
   ,p_CDCEXCH CHAR:=null
   ,p_CDCEXTN CHAR:=null
   ,p_CDCSTATCD CHAR:=null
   ,p_CDCTAXID CHAR:=null
   ,p_CDCSETUPDT DATE:=null
   ,p_CDCLMAINTNDT DATE:=null
   ,p_CDCTOTDOLLRDBENTRAMT VARCHAR2:=null
   ,p_CDCTOTNMBPART VARCHAR2:=null
   ,p_CDCTOTPAIDDBENTRAMT VARCHAR2:=null
   ,p_CDCHLDPYMT CHAR:=null
   ,p_CDCDIST CHAR:=null
   ,p_CDCMAILADRSTR2NM VARCHAR2:=null
   ,p_CDCACHBNK VARCHAR2:=null
   ,p_CDCACHMAILADRSTR1NM VARCHAR2:=null
   ,p_CDCACHMAILADRSTR2NM VARCHAR2:=null
   ,p_CDCACHMAILADRCTYNM VARCHAR2:=null
   ,p_CDCACHMAILADRSTCD CHAR:=null
   ,p_CDCACHMAILADRZIPCD CHAR:=null
   ,p_CDCACHMAILADRZIP4CD CHAR:=null
   ,p_CDCACHBR CHAR:=null
   ,p_CDCACHACCTTYP CHAR:=null
   ,p_CDCACHACCTNMB VARCHAR2:=null
   ,p_CDCACHROUTSYM CHAR:=null
   ,p_CDCACHTRANSCD CHAR:=null
   ,p_CDCACHBNKIDNMB VARCHAR2:=null
   ,p_CDCACHDEPSTR VARCHAR2:=null
   ,p_CDCACHSIGNDT DATE:=null
   ,p_CDCPRENOTETESTDT DATE:=null
   ,p_CDCACHPRENTIND CHAR:=null
   ,p_CDCCLSBALAMT VARCHAR2:=null
   ,p_CDCCLSBALDT DATE:=null
   ,p_CDCNETPREPAYAMT VARCHAR2:=null
   ,p_CDCFAXAREACD CHAR:=null
   ,p_CDCFAXEXCH CHAR:=null
   ,p_CDCFAXEXTN CHAR:=null
   ,p_CDCPREPAYCLSBALAMT VARCHAR2:=null
   ,p_CDCNAMADDRID CHAR:=null
) as
 /*
  Created on: 2018-11-07 11:35:32
  Created by: GENR
  Crerated from template stdtblupd_template v3.22 12 Oct 2018 on 2018-11-07 11:35:32
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  Procedure SOFVCDCMSTRUPDTSP performs UPDATE on STGCSA.SOFVCDCMSTRTBL
    for P_IDENTIFIER=0 qualification is on Primary Key: CDCREGNCD, CDCCERTNMB
    Updates all columns except the qualification keys and columns for which a
      null value is passed.
    SPECIAL secret update kludge!
      if p_identifier is set to -1 then a flag is set to use all input parm values
      and p_identifier is changed to 0 (update by PK).
      THEREFORE if using p_identifier=-1 you MUST pass intended value for all columns.
      This option allows setting colulmns to null!
*/
  dbrec STGCSA.SOFVCDCMSTRTBL%rowtype;
  logged_msg_id number;
  recnum number;
  rec_rowid rowid;
  keystouse varchar2(256);
  maxsev number:=0;
  pctsign char(1):='%';
  cur_colname varchar2(100):=null;
  cur_colvalue varchar2(4000);
  do_normal_field_checking char(1):='Y';
  overridden_p_identifier number;
  -- standard activity log capture vars
  l_LOANNMB char(10):=null;
  l_TRANSID number:=null;
  l_dbmdl varchar2(10):=null;
  l_entrydt date:=null;
  l_userid  varchar2(32):=null;
  l_usernm  varchar2(199):=null;
  l_timestampfld date:=null;
  crossover_not_active char(1);
procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
begin
  if p_errval=0 then
    begin
      select * into dbrec from STGCSA.SOFVCDCMSTRTBL where rowid=rowid_in;
    exception when others then
      p_errval:=sqlcode;
      maxsev:=3;
      p_retval:=0;
      p_errmsg:='Fetch of STGCSA.SOFVCDCMSTRTBL row by rowid '||rowid_in
        ||' orinally fetched using '||orig_keyset_values
        ||' failed with '||sqlerrm(sqlcode);
    end;
  end if;
  if p_errval=0 then
    -- set this as lastupdt
    DBrec.lastupdtuserid:=p_userid;
    DBrec.lastupdtdt:=sysdate;
    -- copy into DBrec all table column input parameters that are not null,
    -- saving column name/value if needed for error reporting
    --  (if p_identifier=-1 then move input even if null)
    begin
      if P_CDCREGNCD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCREGNCD';
        cur_colvalue:=P_CDCREGNCD;
        DBrec.CDCREGNCD:=P_CDCREGNCD;
      end if;
      if P_CDCCERTNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCCERTNMB';
        cur_colvalue:=P_CDCCERTNMB;
        DBrec.CDCCERTNMB:=P_CDCCERTNMB;
      end if;
      if P_CDCPOLKNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCPOLKNMB';
        cur_colvalue:=P_CDCPOLKNMB;
        DBrec.CDCPOLKNMB:=P_CDCPOLKNMB;
      end if;
      if P_CDCNM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCNM';
        cur_colvalue:=P_CDCNM;
        DBrec.CDCNM:=P_CDCNM;
      end if;
      if P_CDCMAILADRSTR1NM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCMAILADRSTR1NM';
        cur_colvalue:=P_CDCMAILADRSTR1NM;
        DBrec.CDCMAILADRSTR1NM:=P_CDCMAILADRSTR1NM;
      end if;
      if P_CDCMAILADRCTYNM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCMAILADRCTYNM';
        cur_colvalue:=P_CDCMAILADRCTYNM;
        DBrec.CDCMAILADRCTYNM:=P_CDCMAILADRCTYNM;
      end if;
      if P_CDCMAILADRSTCD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCMAILADRSTCD';
        cur_colvalue:=P_CDCMAILADRSTCD;
        DBrec.CDCMAILADRSTCD:=P_CDCMAILADRSTCD;
      end if;
      if P_CDCMAILADRZIPCD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCMAILADRZIPCD';
        cur_colvalue:=P_CDCMAILADRZIPCD;
        DBrec.CDCMAILADRZIPCD:=P_CDCMAILADRZIPCD;
      end if;
      if P_CDCMAILADRZIP4CD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCMAILADRZIP4CD';
        cur_colvalue:=P_CDCMAILADRZIP4CD;
        DBrec.CDCMAILADRZIP4CD:=P_CDCMAILADRZIP4CD;
      end if;
      if P_CDCCNTCT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCCNTCT';
        cur_colvalue:=P_CDCCNTCT;
        DBrec.CDCCNTCT:=P_CDCCNTCT;
      end if;
      if P_CDCAREACD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCAREACD';
        cur_colvalue:=P_CDCAREACD;
        DBrec.CDCAREACD:=P_CDCAREACD;
      end if;
      if P_CDCEXCH is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCEXCH';
        cur_colvalue:=P_CDCEXCH;
        DBrec.CDCEXCH:=P_CDCEXCH;
      end if;
      if P_CDCEXTN is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCEXTN';
        cur_colvalue:=P_CDCEXTN;
        DBrec.CDCEXTN:=P_CDCEXTN;
      end if;
      if P_CDCSTATCD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCSTATCD';
        cur_colvalue:=P_CDCSTATCD;
        DBrec.CDCSTATCD:=P_CDCSTATCD;
      end if;
      if P_CDCTAXID is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCTAXID';
        cur_colvalue:=P_CDCTAXID;
        DBrec.CDCTAXID:=P_CDCTAXID;
      end if;
      if P_CDCSETUPDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCSETUPDT';
        cur_colvalue:=P_CDCSETUPDT;
        DBrec.CDCSETUPDT:=P_CDCSETUPDT;
      end if;
      if P_CDCLMAINTNDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCLMAINTNDT';
        cur_colvalue:=P_CDCLMAINTNDT;
        DBrec.CDCLMAINTNDT:=P_CDCLMAINTNDT;
      end if;
      if P_CDCTOTDOLLRDBENTRAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCTOTDOLLRDBENTRAMT';
        cur_colvalue:=P_CDCTOTDOLLRDBENTRAMT;
        DBrec.CDCTOTDOLLRDBENTRAMT:=P_CDCTOTDOLLRDBENTRAMT;
      end if;
      if P_CDCTOTNMBPART is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCTOTNMBPART';
        cur_colvalue:=P_CDCTOTNMBPART;
        DBrec.CDCTOTNMBPART:=P_CDCTOTNMBPART;
      end if;
      if P_CDCTOTPAIDDBENTRAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCTOTPAIDDBENTRAMT';
        cur_colvalue:=P_CDCTOTPAIDDBENTRAMT;
        DBrec.CDCTOTPAIDDBENTRAMT:=P_CDCTOTPAIDDBENTRAMT;
      end if;
      if P_CDCHLDPYMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCHLDPYMT';
        cur_colvalue:=P_CDCHLDPYMT;
        DBrec.CDCHLDPYMT:=P_CDCHLDPYMT;
      end if;
      if P_CDCDIST is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCDIST';
        cur_colvalue:=P_CDCDIST;
        DBrec.CDCDIST:=P_CDCDIST;
      end if;
      if P_CDCMAILADRSTR2NM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCMAILADRSTR2NM';
        cur_colvalue:=P_CDCMAILADRSTR2NM;
        DBrec.CDCMAILADRSTR2NM:=P_CDCMAILADRSTR2NM;
      end if;
      if P_CDCACHBNK is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCACHBNK';
        cur_colvalue:=P_CDCACHBNK;
        DBrec.CDCACHBNK:=P_CDCACHBNK;
      end if;
      if P_CDCACHMAILADRSTR1NM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCACHMAILADRSTR1NM';
        cur_colvalue:=P_CDCACHMAILADRSTR1NM;
        DBrec.CDCACHMAILADRSTR1NM:=P_CDCACHMAILADRSTR1NM;
      end if;
      if P_CDCACHMAILADRSTR2NM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCACHMAILADRSTR2NM';
        cur_colvalue:=P_CDCACHMAILADRSTR2NM;
        DBrec.CDCACHMAILADRSTR2NM:=P_CDCACHMAILADRSTR2NM;
      end if;
      if P_CDCACHMAILADRCTYNM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCACHMAILADRCTYNM';
        cur_colvalue:=P_CDCACHMAILADRCTYNM;
        DBrec.CDCACHMAILADRCTYNM:=P_CDCACHMAILADRCTYNM;
      end if;
      if P_CDCACHMAILADRSTCD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCACHMAILADRSTCD';
        cur_colvalue:=P_CDCACHMAILADRSTCD;
        DBrec.CDCACHMAILADRSTCD:=P_CDCACHMAILADRSTCD;
      end if;
      if P_CDCACHMAILADRZIPCD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCACHMAILADRZIPCD';
        cur_colvalue:=P_CDCACHMAILADRZIPCD;
        DBrec.CDCACHMAILADRZIPCD:=P_CDCACHMAILADRZIPCD;
      end if;
      if P_CDCACHMAILADRZIP4CD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCACHMAILADRZIP4CD';
        cur_colvalue:=P_CDCACHMAILADRZIP4CD;
        DBrec.CDCACHMAILADRZIP4CD:=P_CDCACHMAILADRZIP4CD;
      end if;
      if P_CDCACHBR is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCACHBR';
        cur_colvalue:=P_CDCACHBR;
        DBrec.CDCACHBR:=P_CDCACHBR;
      end if;
      if P_CDCACHACCTTYP is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCACHACCTTYP';
        cur_colvalue:=P_CDCACHACCTTYP;
        DBrec.CDCACHACCTTYP:=P_CDCACHACCTTYP;
      end if;
      if P_CDCACHACCTNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCACHACCTNMB';
        cur_colvalue:=P_CDCACHACCTNMB;
        DBrec.CDCACHACCTNMB:=P_CDCACHACCTNMB;
      end if;
      if P_CDCACHROUTSYM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCACHROUTSYM';
        cur_colvalue:=P_CDCACHROUTSYM;
        DBrec.CDCACHROUTSYM:=P_CDCACHROUTSYM;
      end if;
      if P_CDCACHTRANSCD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCACHTRANSCD';
        cur_colvalue:=P_CDCACHTRANSCD;
        DBrec.CDCACHTRANSCD:=P_CDCACHTRANSCD;
      end if;
      if P_CDCACHBNKIDNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCACHBNKIDNMB';
        cur_colvalue:=P_CDCACHBNKIDNMB;
        DBrec.CDCACHBNKIDNMB:=P_CDCACHBNKIDNMB;
      end if;
      if P_CDCACHDEPSTR is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCACHDEPSTR';
        cur_colvalue:=P_CDCACHDEPSTR;
        DBrec.CDCACHDEPSTR:=P_CDCACHDEPSTR;
      end if;
      if P_CDCACHSIGNDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCACHSIGNDT';
        cur_colvalue:=P_CDCACHSIGNDT;
        DBrec.CDCACHSIGNDT:=P_CDCACHSIGNDT;
      end if;
      if P_CDCPRENOTETESTDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCPRENOTETESTDT';
        cur_colvalue:=P_CDCPRENOTETESTDT;
        DBrec.CDCPRENOTETESTDT:=P_CDCPRENOTETESTDT;
      end if;
      if P_CDCACHPRENTIND is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCACHPRENTIND';
        cur_colvalue:=P_CDCACHPRENTIND;
        DBrec.CDCACHPRENTIND:=P_CDCACHPRENTIND;
      end if;
      if P_CDCCLSBALAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCCLSBALAMT';
        cur_colvalue:=P_CDCCLSBALAMT;
        DBrec.CDCCLSBALAMT:=P_CDCCLSBALAMT;
      end if;
      if P_CDCCLSBALDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCCLSBALDT';
        cur_colvalue:=P_CDCCLSBALDT;
        DBrec.CDCCLSBALDT:=P_CDCCLSBALDT;
      end if;
      if P_CDCNETPREPAYAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCNETPREPAYAMT';
        cur_colvalue:=P_CDCNETPREPAYAMT;
        DBrec.CDCNETPREPAYAMT:=P_CDCNETPREPAYAMT;
      end if;
      if P_CDCFAXAREACD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCFAXAREACD';
        cur_colvalue:=P_CDCFAXAREACD;
        DBrec.CDCFAXAREACD:=P_CDCFAXAREACD;
      end if;
      if P_CDCFAXEXCH is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCFAXEXCH';
        cur_colvalue:=P_CDCFAXEXCH;
        DBrec.CDCFAXEXCH:=P_CDCFAXEXCH;
      end if;
      if P_CDCFAXEXTN is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCFAXEXTN';
        cur_colvalue:=P_CDCFAXEXTN;
        DBrec.CDCFAXEXTN:=P_CDCFAXEXTN;
      end if;
      if P_CDCPREPAYCLSBALAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCPREPAYCLSBALAMT';
        cur_colvalue:=P_CDCPREPAYCLSBALAMT;
        DBrec.CDCPREPAYCLSBALAMT:=P_CDCPREPAYCLSBALAMT;
      end if;
      if P_CDCNAMADDRID is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCNAMADDRID';
        cur_colvalue:=P_CDCNAMADDRID;
        DBrec.CDCNAMADDRID:=P_CDCNAMADDRID;
      end if;
    exception when others then
      p_errval:=sqlcode;
      if maxsev<3 then maxsev:=3; end if;
      p_errmsg:='Error using '||cur_colname||' Value >'||cur_colvalue||'<';
    end;
  end if;
  -- if all still ok do actual update
  if p_errval=0 then
    Begin
      update STGCSA.SOFVCDCMSTRTBL
        set lastupdtuserid=DBrec.lastupdtuserid,
          lastupdtdt=DBrec.lastupdtdt
          ,CDCREGNCD=DBrec.CDCREGNCD
          ,CDCCERTNMB=DBrec.CDCCERTNMB
          ,CDCPOLKNMB=DBrec.CDCPOLKNMB
          ,CDCNM=DBrec.CDCNM
          ,CDCMAILADRSTR1NM=DBrec.CDCMAILADRSTR1NM
          ,CDCMAILADRCTYNM=DBrec.CDCMAILADRCTYNM
          ,CDCMAILADRSTCD=DBrec.CDCMAILADRSTCD
          ,CDCMAILADRZIPCD=DBrec.CDCMAILADRZIPCD
          ,CDCMAILADRZIP4CD=DBrec.CDCMAILADRZIP4CD
          ,CDCCNTCT=DBrec.CDCCNTCT
          ,CDCAREACD=DBrec.CDCAREACD
          ,CDCEXCH=DBrec.CDCEXCH
          ,CDCEXTN=DBrec.CDCEXTN
          ,CDCSTATCD=DBrec.CDCSTATCD
          ,CDCTAXID=DBrec.CDCTAXID
          ,CDCSETUPDT=DBrec.CDCSETUPDT
          ,CDCLMAINTNDT=DBrec.CDCLMAINTNDT
          ,CDCTOTDOLLRDBENTRAMT=DBrec.CDCTOTDOLLRDBENTRAMT
          ,CDCTOTNMBPART=DBrec.CDCTOTNMBPART
          ,CDCTOTPAIDDBENTRAMT=DBrec.CDCTOTPAIDDBENTRAMT
          ,CDCHLDPYMT=DBrec.CDCHLDPYMT
          ,CDCDIST=DBrec.CDCDIST
          ,CDCMAILADRSTR2NM=DBrec.CDCMAILADRSTR2NM
          ,CDCACHBNK=DBrec.CDCACHBNK
          ,CDCACHMAILADRSTR1NM=DBrec.CDCACHMAILADRSTR1NM
          ,CDCACHMAILADRSTR2NM=DBrec.CDCACHMAILADRSTR2NM
          ,CDCACHMAILADRCTYNM=DBrec.CDCACHMAILADRCTYNM
          ,CDCACHMAILADRSTCD=DBrec.CDCACHMAILADRSTCD
          ,CDCACHMAILADRZIPCD=DBrec.CDCACHMAILADRZIPCD
          ,CDCACHMAILADRZIP4CD=DBrec.CDCACHMAILADRZIP4CD
          ,CDCACHBR=DBrec.CDCACHBR
          ,CDCACHACCTTYP=DBrec.CDCACHACCTTYP
          ,CDCACHACCTNMB=DBrec.CDCACHACCTNMB
          ,CDCACHROUTSYM=DBrec.CDCACHROUTSYM
          ,CDCACHTRANSCD=DBrec.CDCACHTRANSCD
          ,CDCACHBNKIDNMB=DBrec.CDCACHBNKIDNMB
          ,CDCACHDEPSTR=DBrec.CDCACHDEPSTR
          ,CDCACHSIGNDT=DBrec.CDCACHSIGNDT
          ,CDCPRENOTETESTDT=DBrec.CDCPRENOTETESTDT
          ,CDCACHPRENTIND=DBrec.CDCACHPRENTIND
          ,CDCCLSBALAMT=DBrec.CDCCLSBALAMT
          ,CDCCLSBALDT=DBrec.CDCCLSBALDT
          ,CDCNETPREPAYAMT=DBrec.CDCNETPREPAYAMT
          ,CDCFAXAREACD=DBrec.CDCFAXAREACD
          ,CDCFAXEXCH=DBrec.CDCFAXEXCH
          ,CDCFAXEXTN=DBrec.CDCFAXEXTN
          ,CDCPREPAYCLSBALAMT=DBrec.CDCPREPAYCLSBALAMT
          ,CDCNAMADDRID=DBrec.CDCNAMADDRID
        where rowid=rowid_in;
      recnum:=recnum+1;
    exception when others then
      p_errval:=sqlcode;
      p_retval:=0;
      if maxsev<3 then maxsev:=3; end if;
      p_errmsg:= 'Error while doing update on STGCSA.SOFVCDCMSTRTBL, on rec with rowid '
      ||rowid_in||' previously fetched using key: '||orig_keyset_values
      ||' got Oracle error '||sqlerrm(sqlcode);
    end;
  end if;
END;
begin
  -- setup
  savepoint SOFVCDCMSTRUPDTSP;
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  recnum:=0;
  runtime.logger(logged_msg_id,'STDLOG',100,'Init SOFVCDCMSTRUPDTSP',p_userid,4,
    logtxt1=>'SOFVCDCMSTRUPDTSP',logtxt2=>'stdtblupd_template v3.22 12 Oct 2018',
    PROGRAM_NAME=>'SOFVCDCMSTRUPDTSP');
  --
  --
  -- Check to see if crossover is ongoing, in which case updates are prohibited
  SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
  IF crossover_not_active != 'Y' THEN
    p_errval := -20099;
    p_errmsg := 'Warning: Cannot update table SOFVCDCMSTRTBL at this time because crossover is running, please try later.';
  END IF;
  -- end of crossover_lockout_check
--
-- special p_identifier kludge
--  if p_identifier is set to -1 then it treated as 0 (update by PK)
--  but a flag is set use passed value EVEN IF NULL.  Since for option -1
--  every column is updated to what was passed, intended values MUST be passed
--  for all columns.  This option allows setting colulmns to null!
  overridden_p_identifier:=p_identifier;
  if p_identifier=-1 then
    do_normal_field_checking:='N';
    overridden_p_identifier:=0;
  end if;
  case overridden_p_identifier
  when 0 then
    -- case to update one row based on primary key
    -- p_identifier=0 (choice 0) is default action for a stored procedure
    -- get existing version of row
    if p_errval=0 then
      keystouse:='(CDCREGNCD)=('||P_CDCREGNCD||')'||' and '||'(CDCCERTNMB)=('||P_CDCCERTNMB||')';
      begin
        select rowid into rec_rowid from STGCSA.SOFVCDCMSTRTBL where 1=1
         and CDCREGNCD=P_CDCREGNCD         and CDCCERTNMB=P_CDCCERTNMB;
      exception when no_data_found then
        p_errval:=0;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Warning No STGCSA.SOFVCDCMSTRTBL row to update with key(s) '
          ||keystouse;
      end;
    end if;
    if p_errval=0 then
      fetch_and_update_by_rowid(rec_rowid, keystouse);
    end if;
  else /* P_IDENTIFIER was unexpected value */
    p_errval:=-20002;
    if maxsev<3 then maxsev:=3; end if;
    p_errmsg:='Error P_IDENTIFIER contained unexpected value: '||p_identifier;
  end case;
  P_RETVAL:=RECNUM;
  -- post to log
  if p_errval=0 then
    runtime.logger(logged_msg_id,'STDLOG',101,
      'End SOFVCDCMSTRUPDTSP With 0 return, P_RETVAL='
      ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
      1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld
      ,PROGRAM_NAME=>'SOFVCDCMSTRUPDTSP');
  else
    ROLLBACK TO SOFVCDCMSTRUPDTSP;
    p_errmsg:=p_errmsg||' in SOFVCDCMSTRUPDTSP build 2018-11-07 11:35:32';
    runtime.logger(logged_msg_id,'STDERR',103,p_errmsg,p_userid,
      maxsev, l_usernm, l_loannmb,l_transid, l_dbmdl, l_entrydt, l_timestampfld,
      PROGRAM_NAME=>'SOFVCDCMSTRUPDTSP',force_log_entry=>true);
  end if;
EXCEPTION
WHEN OTHERS THEN
  ROLLBACK TO SOFVCDCMSTRUPDTSP;
  p_errmsg:=' Error outer exception handler caught Oracle error:'||SQLERRM(SQLCODE)
    ||' in SOFVCDCMSTRUPDTSP build 2018-11-07 11:35:32';
  runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
    3,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld,
    PROGRAM_NAME=>'SOFVCDCMSTRUPDTSP',force_log_entry=>true);
--
END SOFVCDCMSTRUPDTSP;
/


GRANT EXECUTE ON STGCSA.SOFVCDCMSTRUPDTSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.SOFVCDCMSTRUPDTSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.SOFVCDCMSTRUPDTSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.SOFVCDCMSTRUPDTSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.SOFVCDCMSTRUPDTSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.SOFVCDCMSTRUPDTSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.SOFVCDCMSTRUPDTSP TO STGCSADEVROLE;

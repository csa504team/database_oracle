DROP PROCEDURE STGCSA.SOFVDFPYINSTSP;

CREATE OR REPLACE PROCEDURE STGCSA.SOFVDFPYINSTSP(
    p_retval out number,
    p_errval out number,
    p_errmsg  out varchar2,
    p_identifier number:=0,
    p_userid varchar2
   ,p_LOANNMB CHAR:=null
   ,p_DEFPLANNEWMOPYMTAMT VARCHAR2:=null
   ,p_DEFPLANOPNDT DATE:=null
   ,p_DEFPLANDTCLS DATE:=null
   ,p_DEFPLANLASTMAINTNDT DATE:=null
   ,p_DEFPLANCMNT1 VARCHAR2:=null
   ,p_DEFPLANCMNT2 VARCHAR2:=null
) as
 /*
  Created on: 2018-11-07 11:35:44
  Created by: GENR
  Crerated from template stdtblins_template v3.05 13 Sep 2018 on 2018-11-07 11:35:44
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  Procedure performs INSERT on STGCSA.SOFVDFPYTBLfor P_IDENTIFIER=0
*/
  rundate date;
  gn global_name.global_name%type;
  new_rec STGCSA.SOFVDFPYTBL%rowtype;
  logged_msgid_retval number;
  audretval number;
  crossover_active char(1);
  l_LOANNMB char(10):=null;
  l_TRANSID number:=null;
  l_dbmdl varchar2(10):=null;
  l_entrydt date:=null;
  l_userid  varchar2(32):=null;
  l_usernm  varchar2(199):=null;
  l_timestampfld date:=null;
  cur_col_name varchar2(100):=null;
  maxsev number:=0;
  holdstring varchar2(4000);
  keystouse varchar2(1000);
  crossover_not_active char(1);
  -- vars for checking non-defaultable columns
  onespace varchar2(10):=' ';
  ndfltmsg varchar2(300);
  v_errfnd boolean:=false;
  -- std date for column defaulting
  JAN_1_1900  constant date:=to_date('JAN-01-1900','MON-DD-YYYY');
  cur_colvalue varchar2(4095);
  cur_colname varchar2(30);
-- Main body begins here
begin
  -- setup
  savepoint SOFVDFPYINSTSP;
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  runtime.logger(logged_msgid_retval,'STDLOG',100,'Init SOFVDFPYINSTSP',p_userid,4,
    logtxt1=>'SOFVDFPYINSTSP',logtxt2=>'stdtblins_template v3.05 13 Sep 2018',
    PROGRAM_NAME=>'SOFVDFPYINSTSP');
  select global_name, sysdate into gn, rundate from global_name;
  --
  -- Check to see if crossover is ongoing, in which case updates are prohibited
  SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
  IF crossover_not_active != 'Y' THEN
    p_errval := -20099;
    p_errmsg := 'Warning: Cannot update table SOFVDFPYTBL at this time because crossover is running, please try later.';
  END IF;
  -- end of crossover_lockout_check
  begin
    cur_colname:='LOANNMB';
    cur_colvalue:=P_LOANNMB;
    l_LOANNMB:=P_LOANNMB;
  exception when others then
    p_errval:=sqlcode;
    p_errmsg:='Error '||sqlerrm(sqlcode)||' using '||cur_colname||' Value >'||cur_colvalue||'<';
  end;
  new_rec.creatuserid:=p_userid;
  new_rec.creatdt:=sysdate;
  new_rec.lastupdtuserid:=p_userid;
  new_rec.lastupdtdt:=sysdate;
  if p_identifier=0 then
    -- p_identifier=0 this is default for a stored procedure, only action for insert
    runtime.errfound:=false;
    runtime.errstr:=' ';
    --
    -- Default columns as applicable
  if p_errval=0 then
    begin
      cur_colname:='P_LOANNMB';
      cur_colvalue:=P_LOANNMB;
      if P_LOANNMB is null then
        raise_application_error(-20001,cur_colname||' may not be null');
      else
        new_rec.LOANNMB:=P_LOANNMB;
      end if;
      cur_colname:='P_DEFPLANNEWMOPYMTAMT';
      cur_colvalue:=P_DEFPLANNEWMOPYMTAMT;
      if P_DEFPLANNEWMOPYMTAMT is null then

        new_rec.DEFPLANNEWMOPYMTAMT:=0;

      else
        new_rec.DEFPLANNEWMOPYMTAMT:=P_DEFPLANNEWMOPYMTAMT;
      end if;
      cur_colname:='P_DEFPLANOPNDT';
      cur_colvalue:=P_DEFPLANOPNDT;
      if P_DEFPLANOPNDT is null then

        new_rec.DEFPLANOPNDT:=jan_1_1900;

      else
        new_rec.DEFPLANOPNDT:=P_DEFPLANOPNDT;
      end if;
      cur_colname:='P_DEFPLANDTCLS';
      cur_colvalue:=P_DEFPLANDTCLS;
      if P_DEFPLANDTCLS is null then

        new_rec.DEFPLANDTCLS:=jan_1_1900;

      else
        new_rec.DEFPLANDTCLS:=P_DEFPLANDTCLS;
      end if;
      cur_colname:='P_DEFPLANLASTMAINTNDT';
      cur_colvalue:=P_DEFPLANLASTMAINTNDT;
      if P_DEFPLANLASTMAINTNDT is null then

        new_rec.DEFPLANLASTMAINTNDT:=jan_1_1900;

      else
        new_rec.DEFPLANLASTMAINTNDT:=P_DEFPLANLASTMAINTNDT;
      end if;
      cur_colname:='P_DEFPLANCMNT1';
      cur_colvalue:=P_DEFPLANCMNT1;
      if P_DEFPLANCMNT1 is null then

        new_rec.DEFPLANCMNT1:=' ';

      else
        new_rec.DEFPLANCMNT1:=P_DEFPLANCMNT1;
      end if;
      cur_colname:='P_DEFPLANCMNT2';
      cur_colvalue:=P_DEFPLANCMNT2;
      if P_DEFPLANCMNT2 is null then

        new_rec.DEFPLANCMNT2:=' ';

      else
        new_rec.DEFPLANCMNT2:=P_DEFPLANCMNT2;
      end if;
    exception when others then
       p_errval:=sqlcode;
       p_errmsg:='Error - Oracle returned '||sqlerrm(sqlcode)||' using '
           ||cur_colname||' Value >'||cur_colvalue||'<';
    end;
  end if;
    keystouse:='(LOANNMB)=('||new_rec.LOANNMB||')';
    if p_errval=0 then
      begin
        insert into STGCSA.SOFVDFPYTBL values new_rec;
        p_RETVAL := SQL%ROWCOUNT;
      exception
      when dup_val_on_index then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Insert caused dupe on unique IX: '||sqlerrm(sqlcode);
      when others then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<3 then maxsev:=3; end if;
        p_errmsg:='Inserting record caught SQL error '||sqlerrm(sqlcode);
      end;
    end if;
  else /* P_IDENTIFIER was unexpected value */
    p_errval:=-20002;
    if maxsev<3 then maxsev:=3; end if;
    p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier
      ||' inserting record with key '||keystouse;
  end if;
  if p_errval=0 then
    runtime.logger(logged_msgid_retval,'STDLOG',101,
      'End SOFVDFPYINSTSP With 0 return, P_RETVAL='
      ||p_retval||' Record Key '||keystouse,p_userid,
      1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt,
      l_timestampfld,PROGRAM_NAME=>'SOFVDFPYINSTSP');
  else
    p_errmsg:='Error: '||P_ERRMSG||' in SOFVDFPYINSTSP Record Key '||keystouse
      ||' Build: 2018-11-07 11:35:44';
    ROLLBACK TO SOFVDFPYINSTSP;
    runtime.logger(logged_msgid_retval,'STDERR',103,
      p_errmsg,p_userid,maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,
      l_entrydt, l_timestampfld,PROGRAM_NAME=>'SOFVDFPYINSTSP',force_log_entry=>TRUE);
  end if;
EXCEPTION
WHEN OTHERS THEN
  p_errval:=SQLCODE;
  p_errmsg:=' In outer exception handler in SOFVDFPYINSTSP caught exception '
    ||sqlerrm(sqlcode)||' Record Key '||keystouse||' build 2018-11-07 11:35:44';
  ROLLBACK TO SOFVDFPYINSTSP;
  runtime.logger(logged_msgid_retval,'STDERR',20,p_errmsg,p_userid,3,
  PROGRAM_NAME=>'SOFVDFPYINSTSP',force_log_entry=>TRUE);
--
END SOFVDFPYINSTSP;
/


GRANT EXECUTE ON STGCSA.SOFVDFPYINSTSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.SOFVDFPYINSTSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.SOFVDFPYINSTSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.SOFVDFPYINSTSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.SOFVDFPYINSTSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.SOFVDFPYINSTSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.SOFVDFPYINSTSP TO STGCSADEVROLE;

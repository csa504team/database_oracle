DROP PROCEDURE STGCSA.TEMPCDAMORTUPDTSP;

CREATE OR REPLACE PROCEDURE STGCSA.TEMPCDAMORTUPDTSP(
    p_retval out number,
    p_errval out number,
    p_errmsg  out varchar2,
    p_identifier number:=0,
    p_userid varchar2
   ,p_CDAMORTID VARCHAR2:=null
   ,p_LOANNMB CHAR:=null
   ,p_PD DATE:=null
   ,p_PD_BEGIN_PRIN VARCHAR2:=null
   ,p_PYMT_AMT VARCHAR2:=null
   ,p_PRIN_AMT VARCHAR2:=null
   ,p_INT_AMT VARCHAR2:=null
   ,p_CDC_AMT VARCHAR2:=null
   ,p_SBA_AMT VARCHAR2:=null
   ,p_CSA_AMT VARCHAR2:=null
   ,p_LATE_AMT VARCHAR2:=null
   ,p_PRIN_BALANCE VARCHAR2:=null
   ,p_PRIN_NEEDED VARCHAR2:=null
   ,p_INT_NEEDED VARCHAR2:=null
   ,p_CDC_FEE_NEEDED VARCHAR2:=null
   ,p_SBA_FEE_NEEDED VARCHAR2:=null
   ,p_CSA_FEE_NEEDED VARCHAR2:=null
   ,p_LATE_FEE_NEEDED VARCHAR2:=null
   ,p_UNALLOCATED_AMT VARCHAR2:=null
   ,p_PRIN_PYMT VARCHAR2:=null
   ,p_INT_PYMT VARCHAR2:=null
   ,p_TOTAL_PRIN VARCHAR2:=null
   ,p_STATUS VARCHAR2:=null
   ,p_TYPENM VARCHAR2:=null
   ,p_FIRSTPAYMENTDT DATE:=null
   ,p_LASTPAYMENTDT DATE:=null
) as
 /*
  Created on: 2018-11-07 11:36:29
  Created by: GENR
  Crerated from template stdtblupd_template v3.22 12 Oct 2018 on 2018-11-07 11:36:29
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  Procedure TEMPCDAMORTUPDTSP performs UPDATE on STGCSA.TEMPCDAMORTTBL
    for P_IDENTIFIER=0 qualification is on Primary Key: CDAMORTID
    Updates all columns except the qualification keys and columns for which a
      null value is passed.
    SPECIAL secret update kludge!
      if p_identifier is set to -1 then a flag is set to use all input parm values
      and p_identifier is changed to 0 (update by PK).
      THEREFORE if using p_identifier=-1 you MUST pass intended value for all columns.
      This option allows setting colulmns to null!
*/
  dbrec STGCSA.TEMPCDAMORTTBL%rowtype;
  logged_msg_id number;
  recnum number;
  rec_rowid rowid;
  keystouse varchar2(256);
  maxsev number:=0;
  pctsign char(1):='%';
  cur_colname varchar2(100):=null;
  cur_colvalue varchar2(4000);
  do_normal_field_checking char(1):='Y';
  overridden_p_identifier number;
  -- standard activity log capture vars
  l_LOANNMB char(10):=null;
  l_TRANSID number:=null;
  l_dbmdl varchar2(10):=null;
  l_entrydt date:=null;
  l_userid  varchar2(32):=null;
  l_usernm  varchar2(199):=null;
  l_timestampfld date:=null;
procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
begin
  if p_errval=0 then
    begin
      select * into dbrec from STGCSA.TEMPCDAMORTTBL where rowid=rowid_in;
    exception when others then
      p_errval:=sqlcode;
      maxsev:=3;
      p_retval:=0;
      p_errmsg:='Fetch of STGCSA.TEMPCDAMORTTBL row by rowid '||rowid_in
        ||' orinally fetched using '||orig_keyset_values
        ||' failed with '||sqlerrm(sqlcode);
    end;
  end if;
  if p_errval=0 then
    -- set this as lastupdt
    DBrec.lastupdtuserid:=p_userid;
    DBrec.lastupdtdt:=sysdate;
    -- copy into DBrec all table column input parameters that are not null,
    -- saving column name/value if needed for error reporting
    --  (if p_identifier=-1 then move input even if null)
    begin
      if P_CDAMORTID is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDAMORTID';
        cur_colvalue:=P_CDAMORTID;
        DBrec.CDAMORTID:=P_CDAMORTID;
      end if;
      if P_LOANNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_LOANNMB';
        cur_colvalue:=P_LOANNMB;
        DBrec.LOANNMB:=P_LOANNMB;
      end if;
      if P_PD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PD';
        cur_colvalue:=P_PD;
        DBrec.PD:=P_PD;
      end if;
      if P_PD_BEGIN_PRIN is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PD_BEGIN_PRIN';
        cur_colvalue:=P_PD_BEGIN_PRIN;
        DBrec.PD_BEGIN_PRIN:=P_PD_BEGIN_PRIN;
      end if;
      if P_PYMT_AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PYMT_AMT';
        cur_colvalue:=P_PYMT_AMT;
        DBrec.PYMT_AMT:=P_PYMT_AMT;
      end if;
      if P_PRIN_AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PRIN_AMT';
        cur_colvalue:=P_PRIN_AMT;
        DBrec.PRIN_AMT:=P_PRIN_AMT;
      end if;
      if P_INT_AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_INT_AMT';
        cur_colvalue:=P_INT_AMT;
        DBrec.INT_AMT:=P_INT_AMT;
      end if;
      if P_CDC_AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDC_AMT';
        cur_colvalue:=P_CDC_AMT;
        DBrec.CDC_AMT:=P_CDC_AMT;
      end if;
      if P_SBA_AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SBA_AMT';
        cur_colvalue:=P_SBA_AMT;
        DBrec.SBA_AMT:=P_SBA_AMT;
      end if;
      if P_CSA_AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CSA_AMT';
        cur_colvalue:=P_CSA_AMT;
        DBrec.CSA_AMT:=P_CSA_AMT;
      end if;
      if P_LATE_AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_LATE_AMT';
        cur_colvalue:=P_LATE_AMT;
        DBrec.LATE_AMT:=P_LATE_AMT;
      end if;
      if P_PRIN_BALANCE is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PRIN_BALANCE';
        cur_colvalue:=P_PRIN_BALANCE;
        DBrec.PRIN_BALANCE:=P_PRIN_BALANCE;
      end if;
      if P_PRIN_NEEDED is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PRIN_NEEDED';
        cur_colvalue:=P_PRIN_NEEDED;
        DBrec.PRIN_NEEDED:=P_PRIN_NEEDED;
      end if;
      if P_INT_NEEDED is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_INT_NEEDED';
        cur_colvalue:=P_INT_NEEDED;
        DBrec.INT_NEEDED:=P_INT_NEEDED;
      end if;
      if P_CDC_FEE_NEEDED is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDC_FEE_NEEDED';
        cur_colvalue:=P_CDC_FEE_NEEDED;
        DBrec.CDC_FEE_NEEDED:=P_CDC_FEE_NEEDED;
      end if;
      if P_SBA_FEE_NEEDED is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SBA_FEE_NEEDED';
        cur_colvalue:=P_SBA_FEE_NEEDED;
        DBrec.SBA_FEE_NEEDED:=P_SBA_FEE_NEEDED;
      end if;
      if P_CSA_FEE_NEEDED is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CSA_FEE_NEEDED';
        cur_colvalue:=P_CSA_FEE_NEEDED;
        DBrec.CSA_FEE_NEEDED:=P_CSA_FEE_NEEDED;
      end if;
      if P_LATE_FEE_NEEDED is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_LATE_FEE_NEEDED';
        cur_colvalue:=P_LATE_FEE_NEEDED;
        DBrec.LATE_FEE_NEEDED:=P_LATE_FEE_NEEDED;
      end if;
      if P_UNALLOCATED_AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_UNALLOCATED_AMT';
        cur_colvalue:=P_UNALLOCATED_AMT;
        DBrec.UNALLOCATED_AMT:=P_UNALLOCATED_AMT;
      end if;
      if P_PRIN_PYMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PRIN_PYMT';
        cur_colvalue:=P_PRIN_PYMT;
        DBrec.PRIN_PYMT:=P_PRIN_PYMT;
      end if;
      if P_INT_PYMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_INT_PYMT';
        cur_colvalue:=P_INT_PYMT;
        DBrec.INT_PYMT:=P_INT_PYMT;
      end if;
      if P_TOTAL_PRIN is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_TOTAL_PRIN';
        cur_colvalue:=P_TOTAL_PRIN;
        DBrec.TOTAL_PRIN:=P_TOTAL_PRIN;
      end if;
      if P_STATUS is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_STATUS';
        cur_colvalue:=P_STATUS;
        DBrec.STATUS:=P_STATUS;
      end if;
      if P_TYPENM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_TYPENM';
        cur_colvalue:=P_TYPENM;
        DBrec.TYPENM:=P_TYPENM;
      end if;
      if P_FIRSTPAYMENTDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_FIRSTPAYMENTDT';
        cur_colvalue:=P_FIRSTPAYMENTDT;
        DBrec.FIRSTPAYMENTDT:=P_FIRSTPAYMENTDT;
      end if;
      if P_LASTPAYMENTDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_LASTPAYMENTDT';
        cur_colvalue:=P_LASTPAYMENTDT;
        DBrec.LASTPAYMENTDT:=P_LASTPAYMENTDT;
      end if;
    exception when others then
      p_errval:=sqlcode;
      if maxsev<3 then maxsev:=3; end if;
      p_errmsg:='Error using '||cur_colname||' Value >'||cur_colvalue||'<';
    end;
  end if;
  -- if all still ok do actual update
  if p_errval=0 then
    Begin
      update STGCSA.TEMPCDAMORTTBL
        set lastupdtuserid=DBrec.lastupdtuserid,
          lastupdtdt=DBrec.lastupdtdt
          ,CDAMORTID=DBrec.CDAMORTID
          ,LOANNMB=DBrec.LOANNMB
          ,PD=DBrec.PD
          ,PD_BEGIN_PRIN=DBrec.PD_BEGIN_PRIN
          ,PYMT_AMT=DBrec.PYMT_AMT
          ,PRIN_AMT=DBrec.PRIN_AMT
          ,INT_AMT=DBrec.INT_AMT
          ,CDC_AMT=DBrec.CDC_AMT
          ,SBA_AMT=DBrec.SBA_AMT
          ,CSA_AMT=DBrec.CSA_AMT
          ,LATE_AMT=DBrec.LATE_AMT
          ,PRIN_BALANCE=DBrec.PRIN_BALANCE
          ,PRIN_NEEDED=DBrec.PRIN_NEEDED
          ,INT_NEEDED=DBrec.INT_NEEDED
          ,CDC_FEE_NEEDED=DBrec.CDC_FEE_NEEDED
          ,SBA_FEE_NEEDED=DBrec.SBA_FEE_NEEDED
          ,CSA_FEE_NEEDED=DBrec.CSA_FEE_NEEDED
          ,LATE_FEE_NEEDED=DBrec.LATE_FEE_NEEDED
          ,UNALLOCATED_AMT=DBrec.UNALLOCATED_AMT
          ,PRIN_PYMT=DBrec.PRIN_PYMT
          ,INT_PYMT=DBrec.INT_PYMT
          ,TOTAL_PRIN=DBrec.TOTAL_PRIN
          ,STATUS=DBrec.STATUS
          ,TYPENM=DBrec.TYPENM
          ,FIRSTPAYMENTDT=DBrec.FIRSTPAYMENTDT
          ,LASTPAYMENTDT=DBrec.LASTPAYMENTDT
        where rowid=rowid_in;
      recnum:=recnum+1;
    exception when others then
      p_errval:=sqlcode;
      p_retval:=0;
      if maxsev<3 then maxsev:=3; end if;
      p_errmsg:= 'Error while doing update on STGCSA.TEMPCDAMORTTBL, on rec with rowid '
      ||rowid_in||' previously fetched using key: '||orig_keyset_values
      ||' got Oracle error '||sqlerrm(sqlcode);
    end;
  end if;
END;
begin
  -- setup
  savepoint TEMPCDAMORTUPDTSP;
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  recnum:=0;
  runtime.logger(logged_msg_id,'STDLOG',100,'Init TEMPCDAMORTUPDTSP',p_userid,4,
    logtxt1=>'TEMPCDAMORTUPDTSP',logtxt2=>'stdtblupd_template v3.22 12 Oct 2018',
    PROGRAM_NAME=>'TEMPCDAMORTUPDTSP');
  --
  l_LOANNMB:=P_LOANNMB;
--
-- special p_identifier kludge
--  if p_identifier is set to -1 then it treated as 0 (update by PK)
--  but a flag is set use passed value EVEN IF NULL.  Since for option -1
--  every column is updated to what was passed, intended values MUST be passed
--  for all columns.  This option allows setting colulmns to null!
  overridden_p_identifier:=p_identifier;
  if p_identifier=-1 then
    do_normal_field_checking:='N';
    overridden_p_identifier:=0;
  end if;
  case overridden_p_identifier
  when 0 then
    -- case to update one row based on primary key
    -- p_identifier=0 (choice 0) is default action for a stored procedure
    -- get existing version of row
    if p_errval=0 then
      keystouse:='(CDAMORTID)=('||P_CDAMORTID||')';
      begin
        select rowid into rec_rowid from STGCSA.TEMPCDAMORTTBL where 1=1
         and CDAMORTID=P_CDAMORTID;
      exception when no_data_found then
        p_errval:=0;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Warning No STGCSA.TEMPCDAMORTTBL row to update with key(s) '
          ||keystouse;
      end;
    end if;
    if p_errval=0 then
      fetch_and_update_by_rowid(rec_rowid, keystouse);
    end if;
  else /* P_IDENTIFIER was unexpected value */
    p_errval:=-20002;
    if maxsev<3 then maxsev:=3; end if;
    p_errmsg:='Error P_IDENTIFIER contained unexpected value: '||p_identifier;
  end case;
  P_RETVAL:=RECNUM;
  -- post to log
  if p_errval=0 then
    runtime.logger(logged_msg_id,'STDLOG',101,
      'End TEMPCDAMORTUPDTSP With 0 return, P_RETVAL='
      ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
      1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld
      ,PROGRAM_NAME=>'TEMPCDAMORTUPDTSP');
  else
    ROLLBACK TO TEMPCDAMORTUPDTSP;
    p_errmsg:=p_errmsg||' in TEMPCDAMORTUPDTSP build 2018-11-07 11:36:29';
    runtime.logger(logged_msg_id,'STDERR',103,p_errmsg,p_userid,
      maxsev, l_usernm, l_loannmb,l_transid, l_dbmdl, l_entrydt, l_timestampfld,
      PROGRAM_NAME=>'TEMPCDAMORTUPDTSP',force_log_entry=>true);
  end if;
EXCEPTION
WHEN OTHERS THEN
  ROLLBACK TO TEMPCDAMORTUPDTSP;
  p_errmsg:=' Error outer exception handler caught Oracle error:'||SQLERRM(SQLCODE)
    ||' in TEMPCDAMORTUPDTSP build 2018-11-07 11:36:29';
  runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
    3,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld,
    PROGRAM_NAME=>'TEMPCDAMORTUPDTSP',force_log_entry=>true);
--
END TEMPCDAMORTUPDTSP;
/


GRANT EXECUTE ON STGCSA.TEMPCDAMORTUPDTSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.TEMPCDAMORTUPDTSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.TEMPCDAMORTUPDTSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.TEMPCDAMORTUPDTSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.TEMPCDAMORTUPDTSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.TEMPCDAMORTUPDTSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.TEMPCDAMORTUPDTSP TO STGCSADEVROLE;

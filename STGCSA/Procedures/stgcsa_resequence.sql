DROP PROCEDURE STGCSA.STGCSA_RESEQUENCE;

CREATE OR REPLACE PROCEDURE STGCSA.stgcsa_resequence as
-- STGCSA_RESEQUENCE V 3.2 26 Mar 2019 JL
--   This procedure verifies that many STGCSA sequences 
--    (mostly used to create sequenced record keys) 
--    are incremented beyond existing high values  
  returned_logid number;
  p_user varchar2(30):=user;
  pgm varchar2(80):='STGCSA_RESEQUENCE';
  ver varchar2(80):='V3.2 26MAR2019';
  mval number;
  mval2 number;
  nval number;
  nx number;
  msgtext varchar2(1000);
  seqcnt number:=0;
  fixcnt number:=0;
  sqltxt varchar2(1000);
begin
  -- need to make sure seq for coreactvtylog is good so this procedure can log msgs
  select max(logentryid) into mval from coreactvtylogtbl;
  select max(updtlogid) into mval2 from STGCSA.STGUPDTLOGTBL;
  if mval2>mval then mval:=mval2; end if;
  select LOGENTRYIDSEQ.nextval into nval from dual;
  nx:=nval;
  while nval<=mval loop
    select LOGENTRYIDSEQ.nextval into nval from dual;
  end loop; 
  --
  -- additional case, check that csalogsidseq is incremented beyond
  -- existing high value in COREACTVTYLOGTBL  
  select max(csa_sid) into mval from coreactvtylogtbl;
  select csalogsidseq.nextval into nval from dual;
  while nval<=mval loop
    select csalogsidseq.nextval into nval from dual;
  end loop; 
  --
  --- now that actvtylog sequence, csa_sid sequence are good, start normal process 
  runtime.logger(returned_logid,'STGCSAUTIL',201,pgm||' '||ver||' Started',p_user,0);
  for r in (select sname, oname, cname, substr(custom_default_value,6) seq from stgcsa.refcolstbl
    where custom_default_value like '$SEQ:%' and process_scope='GLOBAL' and sname='STGCSA'
    order by oname)
  loop
    seqcnt:=seqcnt+1;
    sqltxt:='select max('||r.cname||'), '||r.seq||'.nextval from '||r.oname;
    execute immediate 'select nvl(max('||r.cname||'),-1) from '||r.sname||'.'||r.oname into mval;
    execute immediate 'select '||r.sname||'.'||r.seq||'.nextval from dual' into nval;
    if mval>=nval then
      for z in nval..mval+1 loop
        execute immediate 'select '||r.sname||'.'||r.seq||'.nextval from dual' into nx;
      end loop;
      fixcnt:=fixcnt+1;
      runtime.logger(returned_logid,'STGCSAUTIL',205,
        'Incr '||r.seq||' from: '||nval||' to: '||nx,user,0,
        logtxt1=>r.seq,lognmb1=>nval,lognmb2=>mval);
    else
      runtime.logger(returned_logid,'STGCSAUTIL',204,
        'OK   '||r.seq||' is at: '||nval||' DB has max: '||mval,p_user,0,
        logtxt1=>r.seq,lognmb1=>nval,lognmb2=>mval);
    end if;
  end loop;
  runtime.logger(returned_logid,'STGCSAUTIL',202,
    pgm||' '||ver||' completed, Seq checked: '||seqcnt||' Seq inremented: '||fixcnt,
    p_user,1);
  commit;
exception when others then
  runtime.logger(returned_logid,'STGCSAUTIL',203,
    pgm||' '||ver||' Recieved unexpected oracle error: '||sqlerrm(sqlcode) ,
    p_user,3);
  Raise;    
end;
/


GRANT EXECUTE ON STGCSA.STGCSA_RESEQUENCE TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.STGCSA_RESEQUENCE TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.STGCSA_RESEQUENCE TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.STGCSA_RESEQUENCE TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.STGCSA_RESEQUENCE TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.STGCSA_RESEQUENCE TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.STGCSA_RESEQUENCE TO STGCSADEVROLE;



CREATE OR REPLACE PROCEDURE STGCSA.MFUPDTPAYPOSTCSP (
   p_taskname          VARCHAR2 := '',
   P_CREATUSERID       VARCHAR2 := NULL,
   p_retval        OUT NUMBER,
   p_errval        OUT NUMBER,
   p_errmsg        OUT VARCHAR2)
AS
-- V1.0 ???
-- V1.1 JL 25 Apr 2019 insert sysdate as lastupdtdt for FTPO
   ---p1 NUMBER; p2 NUMBER; p3 VARCHAR2(200);
   prog varchar2(80):='MFUPDTPAYPOSTCSP';
   ver varchar2(40):='V1.1 25 Apr 2019';
   v_sysdate       DATE := SYSDATE;
   v_status        CHAR (1);
   v_lastupdtdt    DATE;
   v_duplist       VARCHAR2 (2000);
   v_cnt           NUMBER;
   v_curtask       VARCHAR (200);
   v_taskisopen    CHAR (1) := 'N';
   v_testingdate   DATE;
   v_errval        NUMBER := 0;
   v_errmsg        VARCHAR2 (2000) := 'Success';
   logged_msg_id number;
   p_userid varchar2(80);
BEGIN
   p_errval := 0;
   p_retval := -1000;
   p_errmsg := 'Success';
   v_duplist := '';
   v_sysdate:=sysdate;
  runtime.logger(logged_msg_id,'STDLOG',251,'starting '||prog||' '||ver,p_userid,4,
    logtxt1=>PROG,logtxt2=>Ver,
    PROGRAM_NAME=>PROG);
   

   SELECT stgcsa.isProcessAllowed (p_taskname), user INTO v_status, p_userid FROM DUAL;

   IF v_status != 'Y'
   THEN
      p_errval := -21999;

      SELECT JOBTYPEDESC
        INTO v_curtask
        FROM (     SELECT REF.JOBTYPEDESC
                     FROM stgcsa.MFJOBDETAILTBL DTL
                          JOIN stgcsa.REFJOBTYPETBL REF
                             ON REF.JOBTYPEID = DTL.JOBTYPEFK
                    WHERE DTL.TaskEndDt IS NULL
                 ORDER BY DTL.JOBDETAILID
              FETCH FIRST 1 ROWS ONLY);

      p_errmsg :=
            'Warning: Cannot perform MF Update for ['
         || UPPER (p_taskname)
         || '] at this time because a ['
         || v_curtask
         || '] task is currently running.';

      DBMS_OUTPUT.PUT_LINE (
            'P_RETVAL = '
         || p_retval
         || ' / P_ERRVAL = '
         || p_errval
         || ' / P_ERRMSG = '
         || p_errmsg);
      RETURN;
   END IF;

        SELECT DECODE (TRIM (UPPER (CONTROLVALTXT)),
                       'NULL', TRUNC (SYSDATE),
                       stgcsa.getFixedDate (CONTROLVALTXT))
          INTO v_testingdate
          FROM stgCSA.CORECONTROLVALTBL
         WHERE CONTROLNM = 'Testing_Date'
      ORDER BY ROWID DESC
   FETCH FIRST 1 ROW ONLY;

   SELECT COUNT (*)
     INTO v_cnt
     FROM (  SELECT 'A' AS X, trn.loannmb, COUNT (trn.transid) AS CNT
               FROM (SELECT DISTINCT transid
                       FROM STGCSA.coretransattrtbl
                      WHERE attrid IN (20)) Y
                    JOIN STGCSA.coretranstbl trn
                       ON     Y.transid = trn.transid
                          AND trn.LOANNMB IS NOT NULL
                          AND trn.STATIDCUR = 19
                    JOIN coretransattrtbl att3
                       ON     trn.transid = att3.transid
                          AND att3.attrid = 27
                          AND stgcsa.getFixedDate (att3.attrval) =
                                 v_testingdate
           GROUP BY trn.loannmb
             HAVING COUNT (trn.transid) > 1) XXX;

   IF v_cnt > 0
   THEN
        SELECT LISTAGG (loannmb, ', ') WITHIN GROUP (ORDER BY loannmb)
          INTO v_duplist
          FROM (  SELECT 'A' AS X, trn.loannmb, COUNT (trn.transid) AS CNT
                    FROM (SELECT DISTINCT transid
                            FROM STGCSA.coretransattrtbl
                           WHERE attrid IN (20)) Y
                         JOIN STGCSA.coretranstbl trn
                            ON     Y.transid = trn.transid
                               AND trn.LOANNMB IS NOT NULL
                               AND trn.STATIDCUR = 19
                         JOIN coretransattrtbl att3
                            ON     trn.transid = att3.transid
                               AND att3.attrid = 27
                               AND stgcsa.getFixedDate (att3.attrval) =
                                      v_testingdate
                GROUP BY trn.loannmb
                  HAVING COUNT (trn.transid) > 1) Z
      GROUP BY X;

      p_errval := -21999;
      p_errmsg :=
            'Warning: Duplicate payments exist for loan number(s) ['
         || v_duplist
         || ']';
      DBMS_OUTPUT.PUT_LINE (
            'P_RETVAL = '
         || p_retval
         || ' / P_ERRVAL = '
         || p_errval
         || ' / P_ERRMSG = '
         || p_errmsg);

      RETURN;
   END IF;

   SELECT NVL (MAX (taskenddt), TO_DATE ('2017-10-25', 'yyyy-mm-dd'))
     INTO v_lastupdtdt
     FROM STGCSA.mfjobdetailtbl
    WHERE jobtypefk = 10;

   taskstartcsp (p_taskname,
                 p_creatuserid,
                 p_retval   => p_retval,
                 p_errval   => p_errval,
                 p_errmsg   => p_errmsg);
   v_taskisopen := 'Y';
   DBMS_OUTPUT.PUT_LINE (
         'P_RETVAL = '
      || p_retval
      || ' / P_ERRVAL = '
      || p_errval
      || ' / P_ERRMSG = '
      || p_errmsg);

   EXECUTE IMMEDIATE 'TRUNCATE TABLE stgcsa.TempPymtPostTBL';

   INSERT INTO stgcsa.TempPymtPostTBL (TRANSID,
                                       LOANNMB,
                                       CREATUSERID,
                                       CREATDT,
                                       LASTUPDTUSERID,
                                       LASTUPDTDT,
                                       PYMTAMT,
                                       PYMTTYPABBR,
                                       DUPEIND)
      SELECT trn.transid,
             --trn.transtypid,
             trn.loannmb,
             trn.creatuserid,
             trn.creatdt,
             trn.lastupdtuserid,
             trn.lastupdtdt,
             --att1.attrval as pymttyp,
             att2.attrval AS pymtamt,
             --att1.lastupdtdt as pymttyp_lastupdtdt,
             --att2.lastupdtdt as pymtamt_lastupdtdt,
             --att1.lastupdtuserid as pymttyp_lastupdtuserid,
             --att2.lastupdtuserid as pymtamt_lastupdtuserid,
             DECODE (att1.attrval,
                     1, 'C',
                     2, 'W',
                     3, 'A',
                     4, 'C',
                     5, 'W',
                     6, 'M')
                AS PYMTTYPABBR,
             'N'
        FROM (SELECT DISTINCT transid
                FROM STGCSA.coretransattrtbl
               WHERE attrid IN (20)) X
             JOIN coretranstbl trn
                ON     x.transid = trn.transid
                   AND trn.LOANNMB IS NOT NULL
                   AND trn.STATIDCUR = 19
             JOIN coretransattrtbl att3
                ON     trn.transid = att3.transid
                   AND att3.attrid = 27
                   AND stgcsa.getFixedDate (att3.attrval) = v_testingdate
             LEFT OUTER JOIN coretransattrtbl att1
                ON trn.transid = att1.transid AND att1.attrid = 19
             LEFT OUTER JOIN coretransattrtbl att2
                ON trn.transid = att2.transid AND att2.attrid = 20;

   UPDATE stgcsa.TempPymtPostTBL TMP
      SET DUPEIND = 'Y'
    WHERE EXISTS
             (SELECT 1
                FROM stgCSA.SOFVFTPOTBL FTPO
               WHERE FTPO.LOANNMB = TMP.LOANNMB);

   /* here is the main update/merge statement */
   INSERT INTO stgCSA.SOFVFTPOTBL (PREPOSTRVWRECRDTYPCD,
                                   PREPOSTRVWTRANSCD,
                                   PREPOSTRVWTRANSITROUTINGNMB,
                                   PREPOSTRVWTRANSITROUTINGCHKDGT,
                                   PREPOSTRVWBNKACCTNMB,
                                   PREPOSTRVWPYMTAMT,
                                   LOANNMB,
                                   PREPOSTRVWINDVLNM,
                                   PREPOSTRVWCOMPBNKDISC,
                                   PREPOSTRVWADDENDRECIND,
                                   PREPOSTRVWTRACENMB,
                                   PREPOSTRVWCMNT,
                                   CREATUSERID,
                                   CREATDT,
                                   LASTUPDTUSERID,
                                   LASTUPDTDT)
      SELECT TMP.PYMTTYPABBR,
             '00             ',
             '00000000       ',
             '0',
             ' ',
             TMP.pymtamt,
             TMP.loannmb,
             ' ',
             '  ',
             0,
             0,
             ' ',
             TMP.creatuserid,
             TMP.creatdt,
             TMP.lastupdtuserid,
             v_sysdate
        FROM stgcsa.TempPymtPostTBL TMP
       WHERE TMP.DUPEIND = 'N';

   /* end of the main merge */

   /* insert new Stat for Pending 110 which is 25  */
   INSERT INTO stgcsa.CORETRANSSTATTBL (TRANSID,
                                        STATID,
                                        TIMESTAMPFLD,
                                        CREATUSERID,
                                        CREATDT,
                                        LASTUPDTUSERID,
                                        LASTUPDTDT)
      SELECT TRANSID,
             25,
             v_sysdate,
             'MFUPDATE',
             v_sysdate,
             'MFUPDATE',
             v_sysdate
        FROM stgcsa.TempPymtPostTBL
       WHERE DUPEIND = 'N';

   /* update the cores trans */
   UPDATE stgCSA.CORETRANSTBL txns
      SET STATIDCUR = 25, LASTUPDTUSERID = 'MFUPDATE', LASTUPDTDT = v_sysdate
    WHERE EXISTS
             (SELECT 1
                FROM stgcsa.TempPymtPostTBL tmp
               WHERE txns.TRANSID = tmp.TRANSID AND tmp.DUPEIND = 'N');

   /* for those duplicate ones */
   SELECT COUNT (*)
     INTO v_cnt
     FROM stgcsa.TempPymtPostTBL
    WHERE DUPEIND = 'Y';

   IF v_cnt > 0
   THEN
      /* insert new Stat for Pending Suspense which is 15  */
      INSERT INTO stgcsa.CORETRANSSTATTBL (TRANSID,
                                           STATID,
                                           TIMESTAMPFLD,
                                           CREATUSERID,
                                           CREATDT,
                                           LASTUPDTUSERID,
                                           LASTUPDTDT)
         SELECT TRANSID,
                15,
                v_sysdate,
                'MFUPDATE',
                v_sysdate,
                'MFUPDATE',
                v_sysdate
           FROM stgcsa.TempPymtPostTBL
          WHERE DUPEIND = 'Y';

      /* update the cores trans */
      UPDATE stgCSA.CORETRANSTBL txns
         SET STATIDCUR = 15,
             LASTUPDTUSERID = 'MFUPDATE',
             LASTUPDTDT = v_sysdate
       WHERE EXISTS
                (SELECT 1
                   FROM stgcsa.TempPymtPostTBL tmp
                  WHERE txns.TRANSID = tmp.TRANSID AND tmp.DUPEIND = 'Y');

        SELECT LISTAGG (DSP, ', ') WITHIN GROUP (ORDER BY DSP)
          INTO v_duplist
          FROM (SELECT 'A' AS X,
                          LOANNMB
                       || ' ('
                       || TRIM (TO_CHAR (PYMTAMT, '$999,999,999.00'))
                       || ')'
                          AS DSP
                  FROM stgcsa.TempPymtPostTBL
                 WHERE DUPEIND = 'Y') Z
      GROUP BY X;

      v_errval := -21998;
      v_errmsg :=
            'Warning: These loans have prior unprocessed payment(s) and they have been marked as Pending Suspense: ['
         || v_duplist
         || ']. All other payments have been processed.';
   END IF;

   TaskEndCSP (p_taskname,
               p_retval   => p_retval,
               p_errval   => p_errval,
               p_errmsg   => p_errmsg);
   v_taskisopen := 'N';

   IF v_errval != 0
   THEN
      p_errval := v_errval;
      p_errmsg := v_errmsg;
   END IF;

  runtime.logger(logged_msg_id,'STDLOG',252,'Done '||prog||' '||ver,p_userid,5,
    logtxt1=>PROG,logtxt2=>Ver,
    PROGRAM_NAME=>PROG);

   DBMS_OUTPUT.PUT_LINE (
         'P_RETVAL = '
      || p_retval
      || ' / P_ERRVAL = '
      || p_errval
      || ' / P_ERRMSG = '
      || p_errmsg);
EXCEPTION
   WHEN OTHERS
   THEN
      /* this is necessary to record what previous error is
      INSERT INTO COREACTVTYLOGTBL(DBMDL, ACTVTYLOGCAT, ACTVTYDTLS, ENTRYDT, USERID, CREATUSERID, CREATDT, LASTUPDTUSERID, LASTUPDTDT, LOGTXT1, LOGTXT2, PROGRAM)
      VALUES ('JobTask', 'PAYPOST', p_errmsg, sysdate, P_CREATUSERID, P_CREATUSERID, sysdate, P_CREATUSERID, sysdate, p_retval, p_errval, 'MFUPDTPAYPOSTCSP');
      */
      p_errval := SQLCODE;
      p_errmsg := SQLERRM (SQLCODE);
  runtime.logger(logged_msg_id,'STDLOG',253,'Error: Outer Exception Handeler caught '
    ||p_errmsg,p_userid,3,logtxt1=>PROG,logtxt2=>Ver,
    PROGRAM_NAME=>PROG,FORCE_LOG_ENTRY=>TRUE);

      IF v_taskisopen = 'Y'
      THEN
         TaskEndCSP (p_taskname,
                     p_retval   => p_retval,
                     p_errval   => p_errval,
                     p_errmsg   => p_errmsg);           /* close it any way */
      END IF;
--RAISE;
END MFUPDTPAYPOSTCSP;
/


GRANT EXECUTE ON STGCSA.MFUPDTPAYPOSTCSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.MFUPDTPAYPOSTCSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.MFUPDTPAYPOSTCSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.MFUPDTPAYPOSTCSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.MFUPDTPAYPOSTCSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.MFUPDTPAYPOSTCSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.MFUPDTPAYPOSTCSP TO STGCSADEVROLE;

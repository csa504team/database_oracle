DROP PROCEDURE STGCSA.GENPACSIMPTDTLUPDTSP;

CREATE OR REPLACE PROCEDURE STGCSA.GENPACSIMPTDTLUPDTSP(
    p_retval out number,
    p_errval out number,
    p_errmsg  out varchar2,
    p_identifier number:=0,
    p_userid varchar2
   ,p_PACSIMPTDTLID VARCHAR2:=null
   ,p_PACSIMPTID VARCHAR2:=null
   ,p_RQSTID VARCHAR2:=null
   ,p_RELTDTBLNM VARCHAR2:=null
   ,p_RELTDTBLREF VARCHAR2:=null
   ,p_RQSTTYP VARCHAR2:=null
   ,p_PYMTTYP VARCHAR2:=null
   ,p_TRANSID VARCHAR2:=null
   ,p_TRANSAMT VARCHAR2:=null
   ,p_TRANSTYP VARCHAR2:=null
   ,p_PACSACCT VARCHAR2:=null
   ,p_ACCTNM VARCHAR2:=null
   ,p_INTRNLEXTRNLACCT VARCHAR2:=null
   ,p_TRANAU VARCHAR2:=null
   ,p_ABABNKNMB VARCHAR2:=null
   ,p_ABABNKNM VARCHAR2:=null
   ,p_SWIFTBNK VARCHAR2:=null
   ,p_INTRMEDRYABANMB VARCHAR2:=null
   ,p_INTRMEDRYSWIFT VARCHAR2:=null
   ,p_BNKIRC VARCHAR2:=null
   ,p_BNKIBAN VARCHAR2:=null
   ,p_TRANSINFO VARCHAR2:=null
   ,p_STATCD VARCHAR2:=null
   ,p_TRANSDT DATE:=null
   ,p_POSTEDDT DATE:=null
   ,p_POSTERLOC VARCHAR2:=null
   ,p_PACSIMPTPTIC VARCHAR2:=null
   ,p_RCPTCD VARCHAR2:=null
   ,p_DISBCD VARCHAR2:=null
   ,p_PRININC VARCHAR2:=null
   ,p_SEIBTCHID VARCHAR2:=null
   ,p_SEITRANTYP VARCHAR2:=null
   ,p_RQSTAU VARCHAR2:=null
   ,p_INITBY VARCHAR2:=null
   ,p_APPVBY VARCHAR2:=null
   ,p_ACHADDENDA VARCHAR2:=null
   ,p_TIMESTAMPFLD DATE:=null
) as
 /*
  Created on: 2018-11-07 11:34:09
  Created by: GENR
  Crerated from template stdtblupd_template v3.22 12 Oct 2018 on 2018-11-07 11:34:09
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  Procedure GENPACSIMPTDTLUPDTSP performs UPDATE on STGCSA.GENPACSIMPTDTLTBL
    for P_IDENTIFIER=0 qualification is on Primary Key: PACSIMPTDTLID
    for P_IDENTIFIER=1 qualification is on RQSTID
    Updates all columns except the qualification keys and columns for which a
      null value is passed.
    SPECIAL secret update kludge!
      if p_identifier is set to -1 then a flag is set to use all input parm values
      and p_identifier is changed to 0 (update by PK).
      THEREFORE if using p_identifier=-1 you MUST pass intended value for all columns.
      This option allows setting colulmns to null!
*/
  dbrec STGCSA.GENPACSIMPTDTLTBL%rowtype;
  logged_msg_id number;
  recnum number;
  rec_rowid rowid;
  keystouse varchar2(256);
  maxsev number:=0;
  pctsign char(1):='%';
  cur_colname varchar2(100):=null;
  cur_colvalue varchar2(4000);
  do_normal_field_checking char(1):='Y';
  overridden_p_identifier number;
  -- standard activity log capture vars
  l_LOANNMB char(10):=null;
  l_TRANSID number:=null;
  l_dbmdl varchar2(10):=null;
  l_entrydt date:=null;
  l_userid  varchar2(32):=null;
  l_usernm  varchar2(199):=null;
  l_timestampfld date:=null;
procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
begin
  if p_errval=0 then
    begin
      select * into dbrec from STGCSA.GENPACSIMPTDTLTBL where rowid=rowid_in;
    exception when others then
      p_errval:=sqlcode;
      maxsev:=3;
      p_retval:=0;
      p_errmsg:='Fetch of STGCSA.GENPACSIMPTDTLTBL row by rowid '||rowid_in
        ||' orinally fetched using '||orig_keyset_values
        ||' failed with '||sqlerrm(sqlcode);
    end;
  end if;
  if p_errval=0 then
    -- set this as lastupdt
    DBrec.lastupdtuserid:=p_userid;
    DBrec.lastupdtdt:=sysdate;
    -- copy into DBrec all table column input parameters that are not null,
    -- saving column name/value if needed for error reporting
    --  (if p_identifier=-1 then move input even if null)
    begin
      if P_PACSIMPTDTLID is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PACSIMPTDTLID';
        cur_colvalue:=P_PACSIMPTDTLID;
        DBrec.PACSIMPTDTLID:=P_PACSIMPTDTLID;
      end if;
      if P_PACSIMPTID is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PACSIMPTID';
        cur_colvalue:=P_PACSIMPTID;
        DBrec.PACSIMPTID:=P_PACSIMPTID;
      end if;
      if P_RQSTID is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_RQSTID';
        cur_colvalue:=P_RQSTID;
        DBrec.RQSTID:=P_RQSTID;
      end if;
      if P_RELTDTBLNM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_RELTDTBLNM';
        cur_colvalue:=P_RELTDTBLNM;
        DBrec.RELTDTBLNM:=P_RELTDTBLNM;
      end if;
      if P_RELTDTBLREF is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_RELTDTBLREF';
        cur_colvalue:=P_RELTDTBLREF;
        DBrec.RELTDTBLREF:=P_RELTDTBLREF;
      end if;
      if P_RQSTTYP is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_RQSTTYP';
        cur_colvalue:=P_RQSTTYP;
        DBrec.RQSTTYP:=P_RQSTTYP;
      end if;
      if P_PYMTTYP is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PYMTTYP';
        cur_colvalue:=P_PYMTTYP;
        DBrec.PYMTTYP:=P_PYMTTYP;
      end if;
      if P_TRANSID is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_TRANSID';
        cur_colvalue:=P_TRANSID;
        DBrec.TRANSID:=P_TRANSID;
      end if;
      if P_TRANSAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_TRANSAMT';
        cur_colvalue:=P_TRANSAMT;
        DBrec.TRANSAMT:=P_TRANSAMT;
      end if;
      if P_TRANSTYP is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_TRANSTYP';
        cur_colvalue:=P_TRANSTYP;
        DBrec.TRANSTYP:=P_TRANSTYP;
      end if;
      if P_PACSACCT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PACSACCT';
        cur_colvalue:=P_PACSACCT;
        DBrec.PACSACCT:=P_PACSACCT;
      end if;
      if P_ACCTNM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_ACCTNM';
        cur_colvalue:=P_ACCTNM;
        DBrec.ACCTNM:=P_ACCTNM;
      end if;
      if P_INTRNLEXTRNLACCT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_INTRNLEXTRNLACCT';
        cur_colvalue:=P_INTRNLEXTRNLACCT;
        DBrec.INTRNLEXTRNLACCT:=P_INTRNLEXTRNLACCT;
      end if;
      if P_TRANAU is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_TRANAU';
        cur_colvalue:=P_TRANAU;
        DBrec.TRANAU:=P_TRANAU;
      end if;
      if P_ABABNKNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_ABABNKNMB';
        cur_colvalue:=P_ABABNKNMB;
        DBrec.ABABNKNMB:=P_ABABNKNMB;
      end if;
      if P_ABABNKNM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_ABABNKNM';
        cur_colvalue:=P_ABABNKNM;
        DBrec.ABABNKNM:=P_ABABNKNM;
      end if;
      if P_SWIFTBNK is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SWIFTBNK';
        cur_colvalue:=P_SWIFTBNK;
        DBrec.SWIFTBNK:=P_SWIFTBNK;
      end if;
      if P_INTRMEDRYABANMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_INTRMEDRYABANMB';
        cur_colvalue:=P_INTRMEDRYABANMB;
        DBrec.INTRMEDRYABANMB:=P_INTRMEDRYABANMB;
      end if;
      if P_INTRMEDRYSWIFT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_INTRMEDRYSWIFT';
        cur_colvalue:=P_INTRMEDRYSWIFT;
        DBrec.INTRMEDRYSWIFT:=P_INTRMEDRYSWIFT;
      end if;
      if P_BNKIRC is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_BNKIRC';
        cur_colvalue:=P_BNKIRC;
        DBrec.BNKIRC:=P_BNKIRC;
      end if;
      if P_BNKIBAN is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_BNKIBAN';
        cur_colvalue:=P_BNKIBAN;
        DBrec.BNKIBAN:=P_BNKIBAN;
      end if;
      if P_TRANSINFO is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_TRANSINFO';
        cur_colvalue:=P_TRANSINFO;
        DBrec.TRANSINFO:=P_TRANSINFO;
      end if;
      if P_STATCD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_STATCD';
        cur_colvalue:=P_STATCD;
        DBrec.STATCD:=P_STATCD;
      end if;
      if P_TRANSDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_TRANSDT';
        cur_colvalue:=P_TRANSDT;
        DBrec.TRANSDT:=P_TRANSDT;
      end if;
      if P_POSTEDDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_POSTEDDT';
        cur_colvalue:=P_POSTEDDT;
        DBrec.POSTEDDT:=P_POSTEDDT;
      end if;
      if P_POSTERLOC is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_POSTERLOC';
        cur_colvalue:=P_POSTERLOC;
        DBrec.POSTERLOC:=P_POSTERLOC;
      end if;
      if P_PACSIMPTPTIC is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PACSIMPTPTIC';
        cur_colvalue:=P_PACSIMPTPTIC;
        DBrec.PACSIMPTPTIC:=P_PACSIMPTPTIC;
      end if;
      if P_RCPTCD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_RCPTCD';
        cur_colvalue:=P_RCPTCD;
        DBrec.RCPTCD:=P_RCPTCD;
      end if;
      if P_DISBCD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DISBCD';
        cur_colvalue:=P_DISBCD;
        DBrec.DISBCD:=P_DISBCD;
      end if;
      if P_PRININC is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PRININC';
        cur_colvalue:=P_PRININC;
        DBrec.PRININC:=P_PRININC;
      end if;
      if P_SEIBTCHID is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SEIBTCHID';
        cur_colvalue:=P_SEIBTCHID;
        DBrec.SEIBTCHID:=P_SEIBTCHID;
      end if;
      if P_SEITRANTYP is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SEITRANTYP';
        cur_colvalue:=P_SEITRANTYP;
        DBrec.SEITRANTYP:=P_SEITRANTYP;
      end if;
      if P_RQSTAU is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_RQSTAU';
        cur_colvalue:=P_RQSTAU;
        DBrec.RQSTAU:=P_RQSTAU;
      end if;
      if P_INITBY is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_INITBY';
        cur_colvalue:=P_INITBY;
        DBrec.INITBY:=P_INITBY;
      end if;
      if P_APPVBY is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_APPVBY';
        cur_colvalue:=P_APPVBY;
        DBrec.APPVBY:=P_APPVBY;
      end if;
      if P_ACHADDENDA is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_ACHADDENDA';
        cur_colvalue:=P_ACHADDENDA;
        DBrec.ACHADDENDA:=P_ACHADDENDA;
      end if;
      if P_TIMESTAMPFLD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_TIMESTAMPFLD';
        cur_colvalue:=P_TIMESTAMPFLD;
        DBrec.TIMESTAMPFLD:=P_TIMESTAMPFLD;
      end if;
    exception when others then
      p_errval:=sqlcode;
      if maxsev<3 then maxsev:=3; end if;
      p_errmsg:='Error using '||cur_colname||' Value >'||cur_colvalue||'<';
    end;
  end if;
  -- if all still ok do actual update
  if p_errval=0 then
    Begin
      update STGCSA.GENPACSIMPTDTLTBL
        set lastupdtuserid=DBrec.lastupdtuserid,
          lastupdtdt=DBrec.lastupdtdt
          ,PACSIMPTDTLID=DBrec.PACSIMPTDTLID
          ,PACSIMPTID=DBrec.PACSIMPTID
          ,RQSTID=DBrec.RQSTID
          ,RELTDTBLNM=DBrec.RELTDTBLNM
          ,RELTDTBLREF=DBrec.RELTDTBLREF
          ,RQSTTYP=DBrec.RQSTTYP
          ,PYMTTYP=DBrec.PYMTTYP
          ,TRANSID=DBrec.TRANSID
          ,TRANSAMT=DBrec.TRANSAMT
          ,TRANSTYP=DBrec.TRANSTYP
          ,PACSACCT=DBrec.PACSACCT
          ,ACCTNM=DBrec.ACCTNM
          ,INTRNLEXTRNLACCT=DBrec.INTRNLEXTRNLACCT
          ,TRANAU=DBrec.TRANAU
          ,ABABNKNMB=DBrec.ABABNKNMB
          ,ABABNKNM=DBrec.ABABNKNM
          ,SWIFTBNK=DBrec.SWIFTBNK
          ,INTRMEDRYABANMB=DBrec.INTRMEDRYABANMB
          ,INTRMEDRYSWIFT=DBrec.INTRMEDRYSWIFT
          ,BNKIRC=DBrec.BNKIRC
          ,BNKIBAN=DBrec.BNKIBAN
          ,TRANSINFO=DBrec.TRANSINFO
          ,STATCD=DBrec.STATCD
          ,TRANSDT=DBrec.TRANSDT
          ,POSTEDDT=DBrec.POSTEDDT
          ,POSTERLOC=DBrec.POSTERLOC
          ,PACSIMPTPTIC=DBrec.PACSIMPTPTIC
          ,RCPTCD=DBrec.RCPTCD
          ,DISBCD=DBrec.DISBCD
          ,PRININC=DBrec.PRININC
          ,SEIBTCHID=DBrec.SEIBTCHID
          ,SEITRANTYP=DBrec.SEITRANTYP
          ,RQSTAU=DBrec.RQSTAU
          ,INITBY=DBrec.INITBY
          ,APPVBY=DBrec.APPVBY
          ,ACHADDENDA=DBrec.ACHADDENDA
          ,TIMESTAMPFLD=DBrec.TIMESTAMPFLD
        where rowid=rowid_in;
      recnum:=recnum+1;
    exception when others then
      p_errval:=sqlcode;
      p_retval:=0;
      if maxsev<3 then maxsev:=3; end if;
      p_errmsg:= 'Error while doing update on STGCSA.GENPACSIMPTDTLTBL, on rec with rowid '
      ||rowid_in||' previously fetched using key: '||orig_keyset_values
      ||' got Oracle error '||sqlerrm(sqlcode);
    end;
  end if;
END;
begin
  -- setup
  savepoint GENPACSIMPTDTLUPDTSP;
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  recnum:=0;
  runtime.logger(logged_msg_id,'STDLOG',100,'Init GENPACSIMPTDTLUPDTSP',p_userid,4,
    logtxt1=>'GENPACSIMPTDTLUPDTSP',logtxt2=>'stdtblupd_template v3.22 12 Oct 2018',
    PROGRAM_NAME=>'GENPACSIMPTDTLUPDTSP');
  --
  l_TRANSID:=P_TRANSID;  l_TIMESTAMPFLD:=P_TIMESTAMPFLD;
--
-- special p_identifier kludge
--  if p_identifier is set to -1 then it treated as 0 (update by PK)
--  but a flag is set use passed value EVEN IF NULL.  Since for option -1
--  every column is updated to what was passed, intended values MUST be passed
--  for all columns.  This option allows setting colulmns to null!
  overridden_p_identifier:=p_identifier;
  if p_identifier=-1 then
    do_normal_field_checking:='N';
    overridden_p_identifier:=0;
  end if;
  case overridden_p_identifier
  when 0 then
    -- case to update one row based on primary key
    -- p_identifier=0 (choice 0) is default action for a stored procedure
    -- get existing version of row
    if p_errval=0 then
      keystouse:='(PACSIMPTDTLID)=('||P_PACSIMPTDTLID||')';
      begin
        select rowid into rec_rowid from STGCSA.GENPACSIMPTDTLTBL where 1=1
         and PACSIMPTDTLID=P_PACSIMPTDTLID;
      exception when no_data_found then
        p_errval:=0;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Warning No STGCSA.GENPACSIMPTDTLTBL row to update with key(s) '
          ||keystouse;
      end;
    end if;
    if p_errval=0 then
      fetch_and_update_by_rowid(rec_rowid, keystouse);
    end if;
  when 1 then
    -- case to update one or more rows based on keyset KEYS1
    if p_errval=0 then
      keystouse:='(RQSTID)=('||P_RQSTID||')';
      begin
        for r1 in (select rowid from STGCSA.GENPACSIMPTDTLTBL a where 1=1
          and RQSTID=P_RQSTID
  )
        loop
          rec_rowid:=r1.rowid;
          if p_errval=0 then
            fetch_and_update_by_rowid(rec_rowid, keystouse);
          end if;
        end loop;
      exception when others then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<3 then maxsev:=3; end if;
        p_errmsg:='Error fetching STGCSA.GENPACSIMPTDTLTBL row(s) with key(s) '
         ||keystouse||' Oracle returned '||sqlerrm(sqlcode);
      end;
      if recnum=0 then
        p_errval:=0;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Warning No STGCSA.GENPACSIMPTDTLTBL row(s) to update with key(s) '
          ||keystouse;
      end if;
    end if;
  else /* P_IDENTIFIER was unexpected value */
    p_errval:=-20002;
    if maxsev<3 then maxsev:=3; end if;
    p_errmsg:='Error P_IDENTIFIER contained unexpected value: '||p_identifier;
  end case;
  P_RETVAL:=RECNUM;
  -- post to log
  if p_errval=0 then
    runtime.logger(logged_msg_id,'STDLOG',101,
      'End GENPACSIMPTDTLUPDTSP With 0 return, P_RETVAL='
      ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
      1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld
      ,PROGRAM_NAME=>'GENPACSIMPTDTLUPDTSP');
  else
    ROLLBACK TO GENPACSIMPTDTLUPDTSP;
    p_errmsg:=p_errmsg||' in GENPACSIMPTDTLUPDTSP build 2018-11-07 11:34:09';
    runtime.logger(logged_msg_id,'STDERR',103,p_errmsg,p_userid,
      maxsev, l_usernm, l_loannmb,l_transid, l_dbmdl, l_entrydt, l_timestampfld,
      PROGRAM_NAME=>'GENPACSIMPTDTLUPDTSP',force_log_entry=>true);
  end if;
EXCEPTION
WHEN OTHERS THEN
  ROLLBACK TO GENPACSIMPTDTLUPDTSP;
  p_errmsg:=' Error outer exception handler caught Oracle error:'||SQLERRM(SQLCODE)
    ||' in GENPACSIMPTDTLUPDTSP build 2018-11-07 11:34:09';
  runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
    3,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld,
    PROGRAM_NAME=>'GENPACSIMPTDTLUPDTSP',force_log_entry=>true);
--
END GENPACSIMPTDTLUPDTSP;
/


GRANT EXECUTE ON STGCSA.GENPACSIMPTDTLUPDTSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.GENPACSIMPTDTLUPDTSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.GENPACSIMPTDTLUPDTSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.GENPACSIMPTDTLUPDTSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.GENPACSIMPTDTLUPDTSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.GENPACSIMPTDTLUPDTSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.GENPACSIMPTDTLUPDTSP TO STGCSADEVROLE;

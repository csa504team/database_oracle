DROP PROCEDURE STGCSA.PRPMFDUEFROMBORRINSTSP;

CREATE OR REPLACE PROCEDURE STGCSA.PRPMFDUEFROMBORRINSTSP(
    p_retval out number,
    p_errval out number,
    p_errmsg  out varchar2,
    p_identifier number:=0,
    p_userid varchar2
   ,p_PRPMFDUEFROMBORRID VARCHAR2:=null
   ,p_LOANNMB CHAR:=null
   ,p_SEMIANAMT VARCHAR2:=null
   ,p_ESCPRIORPYMTAMT VARCHAR2:=null
   ,p_CDCDUEFROMBORRAMT VARCHAR2:=null
   ,p_DUEFROMBORRAMT VARCHAR2:=null
   ,p_CSADUEFROMBORRAMT VARCHAR2:=null
   ,p_SEMIANDT DATE:=null
   ,p_APPRDT DATE:=null
   ,p_CURRFEEBALAMT VARCHAR2:=null
   ,p_LATEFEEAMT VARCHAR2:=null
   ,p_MATRDT DATE:=null
) as
 /*
  Created on: 2018-11-07 11:34:47
  Created by: GENR
  Crerated from template stdtblins_template v3.05 13 Sep 2018 on 2018-11-07 11:34:47
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  Procedure performs INSERT on STGCSA.PRPMFDUEFROMBORRTBLfor P_IDENTIFIER=0
*/
  rundate date;
  gn global_name.global_name%type;
  new_rec STGCSA.PRPMFDUEFROMBORRTBL%rowtype;
  logged_msgid_retval number;
  audretval number;
  crossover_active char(1);
  l_LOANNMB char(10):=null;
  l_TRANSID number:=null;
  l_dbmdl varchar2(10):=null;
  l_entrydt date:=null;
  l_userid  varchar2(32):=null;
  l_usernm  varchar2(199):=null;
  l_timestampfld date:=null;
  cur_col_name varchar2(100):=null;
  maxsev number:=0;
  holdstring varchar2(4000);
  keystouse varchar2(1000);
  -- std date for column defaulting
  JAN_1_1900  constant date:=to_date('JAN-01-1900','MON-DD-YYYY');
  cur_colvalue varchar2(4095);
  cur_colname varchar2(30);
-- Function used to initialize PRPMFDUEFROMBORRID from sequence PRPMFDUEFROMBORRIDSEQ
Function NEXTVAL_PRPMFDUEFROMBORRIDSEQ return number is
  N number;
begin
  select PRPMFDUEFROMBORRIDSEQ.nextval into n from dual;
  return n;
end;
-- Main body begins here
begin
  -- setup
  savepoint PRPMFDUEFROMBORRINSTSP;
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  runtime.logger(logged_msgid_retval,'STDLOG',100,'Init PRPMFDUEFROMBORRINSTSP',p_userid,4,
    logtxt1=>'PRPMFDUEFROMBORRINSTSP',logtxt2=>'stdtblins_template v3.05 13 Sep 2018',
    PROGRAM_NAME=>'PRPMFDUEFROMBORRINSTSP');
  select global_name, sysdate into gn, rundate from global_name;
  begin
    cur_colname:='LOANNMB';
    cur_colvalue:=P_LOANNMB;
    l_LOANNMB:=P_LOANNMB;
  exception when others then
    p_errval:=sqlcode;
    p_errmsg:='Error '||sqlerrm(sqlcode)||' using '||cur_colname||' Value >'||cur_colvalue||'<';
  end;
  new_rec.creatuserid:=p_userid;
  new_rec.creatdt:=sysdate;
  new_rec.lastupdtuserid:=p_userid;
  new_rec.lastupdtdt:=sysdate;
  if p_identifier=0 then
    -- p_identifier=0 this is default for a stored procedure, only action for insert
    runtime.errfound:=false;
    runtime.errstr:=' ';
    --
    -- Default columns as applicable
  if p_errval=0 then
    begin
      cur_colname:='P_PRPMFDUEFROMBORRID';
      cur_colvalue:=P_PRPMFDUEFROMBORRID;
      if P_PRPMFDUEFROMBORRID is null then

        new_rec.PRPMFDUEFROMBORRID:=NEXTVAL_PRPMFDUEFROMBORRIDSEQ();

      else
        new_rec.PRPMFDUEFROMBORRID:=P_PRPMFDUEFROMBORRID;
      end if;
      cur_colname:='P_LOANNMB';
      cur_colvalue:=P_LOANNMB;
      if P_LOANNMB is null then

        new_rec.LOANNMB:=rpad(' ',10);

      else
        new_rec.LOANNMB:=P_LOANNMB;
      end if;
      cur_colname:='P_SEMIANAMT';
      cur_colvalue:=P_SEMIANAMT;
      if P_SEMIANAMT is null then

        new_rec.SEMIANAMT:=0;

      else
        new_rec.SEMIANAMT:=P_SEMIANAMT;
      end if;
      cur_colname:='P_ESCPRIORPYMTAMT';
      cur_colvalue:=P_ESCPRIORPYMTAMT;
      if P_ESCPRIORPYMTAMT is null then

        new_rec.ESCPRIORPYMTAMT:=0;

      else
        new_rec.ESCPRIORPYMTAMT:=P_ESCPRIORPYMTAMT;
      end if;
      cur_colname:='P_CDCDUEFROMBORRAMT';
      cur_colvalue:=P_CDCDUEFROMBORRAMT;
      if P_CDCDUEFROMBORRAMT is null then

        new_rec.CDCDUEFROMBORRAMT:=0;

      else
        new_rec.CDCDUEFROMBORRAMT:=P_CDCDUEFROMBORRAMT;
      end if;
      cur_colname:='P_DUEFROMBORRAMT';
      cur_colvalue:=P_DUEFROMBORRAMT;
      if P_DUEFROMBORRAMT is null then

        new_rec.DUEFROMBORRAMT:=0;

      else
        new_rec.DUEFROMBORRAMT:=P_DUEFROMBORRAMT;
      end if;
      cur_colname:='P_CSADUEFROMBORRAMT';
      cur_colvalue:=P_CSADUEFROMBORRAMT;
      if P_CSADUEFROMBORRAMT is null then

        new_rec.CSADUEFROMBORRAMT:=0;

      else
        new_rec.CSADUEFROMBORRAMT:=P_CSADUEFROMBORRAMT;
      end if;
      cur_colname:='P_SEMIANDT';
      cur_colvalue:=P_SEMIANDT;
      if P_SEMIANDT is null then

        new_rec.SEMIANDT:=jan_1_1900;

      else
        new_rec.SEMIANDT:=P_SEMIANDT;
      end if;
      cur_colname:='P_APPRDT';
      cur_colvalue:=P_APPRDT;
      if P_APPRDT is null then

        new_rec.APPRDT:=jan_1_1900;

      else
        new_rec.APPRDT:=P_APPRDT;
      end if;
      cur_colname:='P_CURRFEEBALAMT';
      cur_colvalue:=P_CURRFEEBALAMT;
      if P_CURRFEEBALAMT is null then

        new_rec.CURRFEEBALAMT:=0;

      else
        new_rec.CURRFEEBALAMT:=P_CURRFEEBALAMT;
      end if;
      cur_colname:='P_LATEFEEAMT';
      cur_colvalue:=P_LATEFEEAMT;
      if P_LATEFEEAMT is null then

        new_rec.LATEFEEAMT:=0;

      else
        new_rec.LATEFEEAMT:=P_LATEFEEAMT;
      end if;
      cur_colname:='P_MATRDT';
      cur_colvalue:=P_MATRDT;
      if P_MATRDT is null then

        new_rec.MATRDT:=jan_1_1900;

      else
        new_rec.MATRDT:=P_MATRDT;
      end if;
    exception when others then
       p_errval:=sqlcode;
       p_errmsg:='Error - Oracle returned '||sqlerrm(sqlcode)||' using '
           ||cur_colname||' Value >'||cur_colvalue||'<';
    end;
  end if;
    keystouse:='(PRPMFDUEFROMBORRID)=('||new_rec.PRPMFDUEFROMBORRID||')';
    if p_errval=0 then
      begin
        insert into STGCSA.PRPMFDUEFROMBORRTBL values new_rec;
        p_RETVAL := SQL%ROWCOUNT;
      exception
      when dup_val_on_index then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Insert caused dupe on unique IX: '||sqlerrm(sqlcode);
      when others then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<3 then maxsev:=3; end if;
        p_errmsg:='Inserting record caught SQL error '||sqlerrm(sqlcode);
      end;
    end if;
  else /* P_IDENTIFIER was unexpected value */
    p_errval:=-20002;
    if maxsev<3 then maxsev:=3; end if;
    p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier
      ||' inserting record with key '||keystouse;
  end if;
  if p_errval=0 then
    runtime.logger(logged_msgid_retval,'STDLOG',101,
      'End PRPMFDUEFROMBORRINSTSP With 0 return, P_RETVAL='
      ||p_retval||' Record Key '||keystouse,p_userid,
      1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt,
      l_timestampfld,PROGRAM_NAME=>'PRPMFDUEFROMBORRINSTSP');
  else
    p_errmsg:='Error: '||P_ERRMSG||' in PRPMFDUEFROMBORRINSTSP Record Key '||keystouse
      ||' Build: 2018-11-07 11:34:46';
    ROLLBACK TO PRPMFDUEFROMBORRINSTSP;
    runtime.logger(logged_msgid_retval,'STDERR',103,
      p_errmsg,p_userid,maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,
      l_entrydt, l_timestampfld,PROGRAM_NAME=>'PRPMFDUEFROMBORRINSTSP',force_log_entry=>TRUE);
  end if;
EXCEPTION
WHEN OTHERS THEN
  p_errval:=SQLCODE;
  p_errmsg:=' In outer exception handler in PRPMFDUEFROMBORRINSTSP caught exception '
    ||sqlerrm(sqlcode)||' Record Key '||keystouse||' build 2018-11-07 11:34:46';
  ROLLBACK TO PRPMFDUEFROMBORRINSTSP;
  runtime.logger(logged_msgid_retval,'STDERR',20,p_errmsg,p_userid,3,
  PROGRAM_NAME=>'PRPMFDUEFROMBORRINSTSP',force_log_entry=>TRUE);
--
END PRPMFDUEFROMBORRINSTSP;
/


GRANT EXECUTE ON STGCSA.PRPMFDUEFROMBORRINSTSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.PRPMFDUEFROMBORRINSTSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.PRPMFDUEFROMBORRINSTSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.PRPMFDUEFROMBORRINSTSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.PRPMFDUEFROMBORRINSTSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.PRPMFDUEFROMBORRINSTSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.PRPMFDUEFROMBORRINSTSP TO STGCSADEVROLE;

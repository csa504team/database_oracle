CREATE OR REPLACE PROCEDURE STGCSA.PYMT110RPTARCHVCSP(
P_IDENTIFIER	NUMBER := NULL
, P_POSTDATE	DATE := SYSDATE
, P_SELCUR 		OUT SYS_REFCURSOR)
AS
	v_prcsind		CHAR;
BEGIN
	--
	IF P_IDENTIFIER = 0 THEN

		SELECT prcsind INTO v_prcsind FROM stgcsa.pymt110rptarchvtbl WHERE TRUNC(postdt) = STGCSA.FN_BIZDATEADD (p_postdate, -1) ORDER BY postdt DESC FETCH FIRST 1 ROW ONLY;

		IF v_prcsind = 'Y' THEN
			BEGIN
				OPEN P_SELCUR FOR
				SELECT
					PRININTTOTAMT,
					UNALLOCAMT,
					REPDAMT,
					FEEPDAMT,
					LATEFEEAMT,
					LENDRFEEAMT,
					PREPAYAMT,
					PRCSIND,
					CREATUSERID,
					CREATDT,
					LASTUPDTUSERID,
					LASTUPDTDT,
					POSTDT
				FROM
					STGCSA.PYMT110RPTARCHVTBL
				WHERE
					TRUNC(POSTDT) = STGCSA.FN_BIZDATEADD (P_POSTDATE, -1)
				AND
					PRCSIND = 'Y'
				ORDER BY
					POSTDT DESC FETCH FIRST 1 ROW ONLY;
			END;
		ELSIF v_prcsind = 'N' THEN
			BEGIN
				UPDATE STGCSA.PYMT110RPTARCHVTBL
					SET PREPAYAMT = ( SELECT NVL(SUM(amount),0) AS PREPAYAMT
									  FROM (SELECT CT.TransID,
												CT.LoanNmb AS LoanNmb,
												CT.TransInd1,
												CT.CreatDt,
												CT.StatIDCur,
												MCTA.MaxOftransAttrID,
												TO_NUMBER(MCTA.AttrVal) AS Amount,
												MCTA2.AttrVal AS ImptDt,
												MCTA3.AttrVal AS PostDate,
												MCTA1.AttrVal AS "Current",
												--RTS.StatNm AS Status,
												MCTA22.AttrVal AS BatchNo,
												MCTA19.AttrVal AS PymtTypeID
											  FROM stgCSA.CoreTransTbl CT
											  INNER JOIN (SELECT MAX(TransAttrID) AS MaxOfTransAttrID, TransID, AttrVal FROM stgCSA.CoreTransAttrTbl WHERE AttrID=20 GROUP BY TransID, AttrVal order by TransID DESC) MCTA ON CT.TransID = MCTA.TransID --PymtAmt
											  INNER JOIN (SELECT MAX(TransAttrID) AS MaxOfTransAttrID, TransID, AttrVal FROM stgCSA.CoreTransAttrTbl WHERE AttrID=24 GROUP BY TransID, AttrVal order by TransID DESC) MCTA1 ON CT.TransID = MCTA1.TransID --Current
											  INNER JOIN (SELECT TransID,
												CASE
												WHEN REGEXP_LIKE(Trim(XXAttrVal),'^(\d)+$') THEN TO_DATE ('12/31/1899', 'MM/DD/YYYY') + XXAttrVal
												WHEN REGEXP_LIKE(Trim(XXAttrVal),'^(\d){4}/(\d){1,2}/(\d){1,2}') THEN TO_DATE (XXAttrVal, 'YYYY/MM/DD')
												WHEN REGEXP_LIKE(Trim(XXAttrVal),'^(\d){1,2}/(\d){1,2}/(\d){4}') THEN TO_DATE (XXAttrVal, 'MM/DD/YYYY')
												WHEN INSTR(XXAttrVal,'-') > 0 THEN TO_DATE (XXAttrVal, 'YYYY-MM-DD HH24:MI:SS')
												ELSE null
												END
												AS AttrVal
												FROM (
												  SELECT MAX(TransAttrID) AS MaxOfTransAttrID, TransID, AttrVal AS XXAttrVal
												  FROM stgCSA.CoreTransAttrTbl WHERE AttrID=34 GROUP BY TransID, AttrVal order by TransID DESC
												  ) XXX ) MCTA2 ON CT.TransID = MCTA2.TransID --ImpDt
											  INNER JOIN (SELECT TransID,
												CASE
												WHEN REGEXP_LIKE(Trim(XXAttrVal),'^(\d)+$') THEN TO_DATE ('12/31/1899', 'MM/DD/YYYY') + XXAttrVal
												WHEN REGEXP_LIKE(Trim(XXAttrVal),'^(\d){4}/(\d){1,2}/(\d){1,2}') THEN TO_DATE (XXAttrVal, 'YYYY/MM/DD')
												WHEN REGEXP_LIKE(Trim(XXAttrVal),'^(\d){1,2}/(\d){1,2}/(\d){4}') THEN TO_DATE (XXAttrVal, 'MM/DD/YYYY')
												WHEN INSTR(XXAttrVal,'-') > 0 THEN TO_DATE (XXAttrVal, 'YYYY-MM-DD HH24:MI:SS')
												ELSE null
												END
												AS AttrVal
												FROM (
												  SELECT MAX(TransAttrID) AS MaxOfTransAttrID, TransID, AttrVal AS XXAttrVal
												  FROM stgCSA.CoreTransAttrTbl WHERE AttrID=27 GROUP BY TransID, AttrVal order by TransID DESC
												  ) XXX ) MCTA3 ON CT.TransID = MCTA3.TransID --PostDt
											  INNER JOIN (SELECT MAX(TransAttrID) AS MaxOfTransAttrID, TransID, AttrVal FROM stgCSA.CoreTransAttrTbl WHERE AttrID=19 GROUP BY TransID, AttrVal order by TransID DESC) MCTA19 ON CT.TransID = MCTA19.TransID --PymtTyp
											  LEFT OUTER JOIN (SELECT MAX(TransAttrID) AS MaxOfTransAttrID, TransID, AttrVal FROM stgCSA.CoreTransAttrTbl WHERE AttrID=22 GROUP BY TransID, AttrVal order by TransID DESC) MCTA22 ON CT.TransID = MCTA22.TransID --BatchNo
											  WHERE CT.StatIDCur IN (6, 7, 18, 33, 41)
											  --INNER JOIN stgCSA.RefTransStatTbl RTS ON RTS.StatID = CT.StatIDCur WHERE 0=0 AND transTypID = 3
											  --AND TRUNC(MCTA3.AttrVal) = <cfqueryparam cfsqltype="CF_SQL_DATE" value="#arguments.cashReconDate#">
											  --AND TRIM(RTS.StatNm) IN ('Pending Prepay', 'Prepayment', 'Prepay Reject', 'Prepay NSF', 'Pending Mgmt Approval')
											  AND TRUNC(MCTA3.AttrVal) > add_months(sysdate, -12)
											  ORDER BY ImptDt DESC, Amount DESC)
									),
						LENDRFEEAMT =( SELECT NVL(SUM(amount),0) AS LENDRFEEAMT
									   FROM (SELECT
												CT.TransID,
												CT.LoanNmb AS LoanNmb,
												CT.TransInd1,
												CT.CreatDt,
												CT.StatIDCur,
												MCTA.MaxOftransAttrID,
												TO_NUMBER(MCTA.AttrVal) AS Amount,
												MCTA2.AttrVal AS ImptDt,
												MCTA3.AttrVal AS PostDate,
												MCTA1.AttrVal AS "Current",
												--RTS.StatNm AS Status,
												MCTA22.AttrVal AS BatchNo,
												MCTA19.AttrVal AS PymtTypeID
											  FROM stgCSA.CoreTransTbl CT
											  INNER JOIN (SELECT MAX(TransAttrID) AS MaxOfTransAttrID, TransID, AttrVal FROM stgCSA.CoreTransAttrTbl WHERE AttrID=20 GROUP BY TransID, AttrVal order by TransID DESC) MCTA ON CT.TransID = MCTA.TransID --PymtAmt
											  INNER JOIN (SELECT MAX(TransAttrID) AS MaxOfTransAttrID, TransID, AttrVal FROM stgCSA.CoreTransAttrTbl WHERE AttrID=24 GROUP BY TransID, AttrVal order by TransID DESC) MCTA1 ON CT.TransID = MCTA1.TransID --Current
											  INNER JOIN (SELECT TransID,
												CASE
												WHEN REGEXP_LIKE(Trim(XXAttrVal),'^(\d)+$') THEN TO_DATE ('12/31/1899', 'MM/DD/YYYY') + XXAttrVal
												WHEN REGEXP_LIKE(Trim(XXAttrVal),'^(\d){4}/(\d){1,2}/(\d){1,2}') THEN TO_DATE (XXAttrVal, 'YYYY/MM/DD')
												WHEN REGEXP_LIKE(Trim(XXAttrVal),'^(\d){1,2}/(\d){1,2}/(\d){4}') THEN TO_DATE (XXAttrVal, 'MM/DD/YYYY')
												WHEN INSTR(XXAttrVal,'-') > 0 THEN TO_DATE (XXAttrVal, 'YYYY-MM-DD HH24:MI:SS')
												ELSE null
												END
												AS AttrVal
												FROM (
												  SELECT MAX(TransAttrID) AS MaxOfTransAttrID, TransID, AttrVal AS XXAttrVal
												  FROM stgCSA.CoreTransAttrTbl WHERE AttrID=34 GROUP BY TransID, AttrVal order by TransID DESC
												  ) XXX ) MCTA2 ON CT.TransID = MCTA2.TransID --ImpDt
											  INNER JOIN (SELECT TransID,
												CASE
												WHEN REGEXP_LIKE(Trim(XXAttrVal),'^(\d)+$') THEN TO_DATE ('12/31/1899', 'MM/DD/YYYY') + XXAttrVal
												WHEN REGEXP_LIKE(Trim(XXAttrVal),'^(\d){4}/(\d){1,2}/(\d){1,2}') THEN TO_DATE (XXAttrVal, 'YYYY/MM/DD')
												WHEN REGEXP_LIKE(Trim(XXAttrVal),'^(\d){1,2}/(\d){1,2}/(\d){4}') THEN TO_DATE (XXAttrVal, 'MM/DD/YYYY')
												WHEN INSTR(XXAttrVal,'-') > 0 THEN TO_DATE (XXAttrVal, 'YYYY-MM-DD HH24:MI:SS')
												ELSE null
												END
												AS AttrVal
												FROM (
												  SELECT MAX(TransAttrID) AS MaxOfTransAttrID, TransID, AttrVal AS XXAttrVal
												  FROM stgCSA.CoreTransAttrTbl WHERE AttrID=27 GROUP BY TransID, AttrVal order by TransID DESC
												  ) XXX ) MCTA3 ON CT.TransID = MCTA3.TransID --PostDt
											  INNER JOIN (SELECT MAX(TransAttrID) AS MaxOfTransAttrID, TransID, AttrVal FROM stgCSA.CoreTransAttrTbl WHERE AttrID=19 GROUP BY TransID, AttrVal order by TransID DESC) MCTA19 ON CT.TransID = MCTA19.TransID --PymtTyp
											  LEFT OUTER JOIN (SELECT MAX(TransAttrID) AS MaxOfTransAttrID, TransID, AttrVal FROM stgCSA.CoreTransAttrTbl WHERE AttrID=22 GROUP BY TransID, AttrVal order by TransID DESC) MCTA22 ON CT.TransID = MCTA22.TransID --BatchNo
											  WHERE CT.StatIDCur = 24
											  --INNER JOIN stgCSA.RefTransStatTbl RTS ON RTS.StatID = CT.StatIDCur WHERE 0=0 AND transTypID = 3
											  --AND TRUNC(MCTA3.AttrVal) = <cfqueryparam cfsqltype="CF_SQL_DATE" value="#arguments.cashReconDate#">
											  --AND TRIM(RTS.StatNm) IN ('Pending Prepay', 'Prepayment', 'Prepay Reject', 'Prepay NSF', 'Pending Mgmt Approval')
											  AND TRUNC(MCTA3.AttrVal) > add_months(sysdate, -12)
											  ORDER BY ImptDt DESC, Amount DESC
												)
									),
						PRCSIND = 'Y',
						LASTUPDTDT = SYSDATE,
						LASTUPDTUSERID = USER
						WHERE TRUNC(POSTDT) = STGCSA.FN_BIZDATEADD (P_POSTDATE, -1);
						-- the above query will update all records for that post date
			END;

			BEGIN
				OPEN P_SELCUR FOR
				SELECT
					PRININTTOTAMT,
					UNALLOCAMT,
					REPDAMT,
					FEEPDAMT,
					LATEFEEAMT,
					LENDRFEEAMT,
					PREPAYAMT,
					PRCSIND,
					CREATUSERID,
					CREATDT,
					LASTUPDTUSERID,
					LASTUPDTDT,
					POSTDT
				FROM
					STGCSA.PYMT110RPTARCHVTBL
				WHERE
					TRUNC(POSTDT) = STGCSA.FN_BIZDATEADD (P_POSTDATE, -1)
				AND
					PRCSIND = 'Y'
				ORDER BY
					POSTDT DESC FETCH FIRST 1 ROW ONLY;
			END;
		END IF;
	END IF;
EXCEPTION
WHEN OTHERS THEN
	BEGIN
		RAISE;
	END;
END;
/
grant execute on STGCSA.PYMT110RPTARCHVCSP to LOANCSAADMINROLE;                 
grant execute on STGCSA.PYMT110RPTARCHVCSP to LOANCSAANALYSTROLE;               
grant execute on STGCSA.PYMT110RPTARCHVCSP to LOANCSAREVIEWERROLE;              
grant execute on STGCSA.PYMT110RPTARCHVCSP to LOANCSAMANAGERROLE;               
grant execute on STGCSA.PYMT110RPTARCHVCSP to LOANCSAREADALLROLE;         
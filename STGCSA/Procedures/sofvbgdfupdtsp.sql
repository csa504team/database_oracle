DROP PROCEDURE STGCSA.SOFVBGDFUPDTSP;

CREATE OR REPLACE PROCEDURE STGCSA.SOFVBGDFUPDTSP(
    p_retval out number,
    p_errval out number,
    p_errmsg  out varchar2,
    p_identifier number:=0,
    p_userid varchar2
   ,p_DEFRMNTGPKEY CHAR:=null
   ,p_DEFRMNTSTRTDT1 DATE:=null
   ,p_DEFRMNTENDDT1 DATE:=null
   ,p_DEFRMNTREQ1AMT VARCHAR2:=null
   ,p_DEFRMNTSTRTDT2 DATE:=null
   ,p_DEFRMNTENDDT2 DATE:=null
   ,p_DEFRMNTREQ2AMT VARCHAR2:=null
   ,p_DEFRMNTSTRTDT3 DATE:=null
   ,p_DEFRMNTENDDT3 DATE:=null
   ,p_DEFRMNTREQ3AMT VARCHAR2:=null
   ,p_DEFRMNTSTRTDT4 DATE:=null
   ,p_DEFRMNTENDDT4 DATE:=null
   ,p_DEFRMNTREQ4AMT VARCHAR2:=null
   ,p_DEFRMNTSTRTDT5 DATE:=null
   ,p_DEFRMNTENDDT5 DATE:=null
   ,p_DEFRMNTREQ5AMT VARCHAR2:=null
   ,p_DEFRMNTSTRTDT6 DATE:=null
   ,p_DEFRMNTENDDT6 DATE:=null
   ,p_DEFRMNTREQ6AMT VARCHAR2:=null
   ,p_DEFRMNTSTRTDT7 DATE:=null
   ,p_DEFRMNTENDDT7 DATE:=null
   ,p_DEFRMNTREQ7AMT VARCHAR2:=null
   ,p_DEFRMNTSTRTDT8 DATE:=null
   ,p_DEFRMNTENDDT8 DATE:=null
   ,p_DEFRMNTREQ8AMT VARCHAR2:=null
   ,p_DEFRMNTSTRTDT9 DATE:=null
   ,p_DEFRMNTENDDT9 DATE:=null
   ,p_DEFRMNTREQ9AMT VARCHAR2:=null
   ,p_DEFRMNTSTRTDT10 DATE:=null
   ,p_DEFRMNTENDDT10 DATE:=null
   ,p_DEFRMNTREQ10AMT VARCHAR2:=null
   ,p_DEFRMNTSTRTDT11 DATE:=null
   ,p_DEFRMNTENDDT11 DATE:=null
   ,p_DEFRMNTREQ11AMT VARCHAR2:=null
   ,p_DEFRMNTSTRTDT12 DATE:=null
   ,p_DEFRMNTENDDT12 DATE:=null
   ,p_DEFRMNTREQ12AMT VARCHAR2:=null
   ,p_DEFRMNTSTRTDT13 DATE:=null
   ,p_DEFRMNTENDDT13 DATE:=null
   ,p_DEFRMNTREQ13AMT VARCHAR2:=null
   ,p_DEFRMNTSTRTDT14 DATE:=null
   ,p_DEFRMNTENDDT14 DATE:=null
   ,p_DEFRMNTREQ14AMT VARCHAR2:=null
   ,p_DEFRMNTSTRTDT15 DATE:=null
   ,p_DEFRMNTENDDT15 DATE:=null
   ,p_DEFRMNTREQ15AMT VARCHAR2:=null
   ,p_DEFRMNTDTCREAT DATE:=null
   ,p_DEFRMNTDTMOD DATE:=null
) as
 /*
  Created on: 2018-11-07 11:35:23
  Created by: GENR
  Crerated from template stdtblupd_template v3.22 12 Oct 2018 on 2018-11-07 11:35:23
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  Procedure SOFVBGDFUPDTSP performs UPDATE on STGCSA.SOFVBGDFTBL
    for P_IDENTIFIER=0 qualification is on Primary Key: DEFRMNTGPKEY
    Updates all columns except the qualification keys and columns for which a
      null value is passed.
    SPECIAL secret update kludge!
      if p_identifier is set to -1 then a flag is set to use all input parm values
      and p_identifier is changed to 0 (update by PK).
      THEREFORE if using p_identifier=-1 you MUST pass intended value for all columns.
      This option allows setting colulmns to null!
*/
  dbrec STGCSA.SOFVBGDFTBL%rowtype;
  logged_msg_id number;
  recnum number;
  rec_rowid rowid;
  keystouse varchar2(256);
  maxsev number:=0;
  pctsign char(1):='%';
  cur_colname varchar2(100):=null;
  cur_colvalue varchar2(4000);
  do_normal_field_checking char(1):='Y';
  overridden_p_identifier number;
  -- standard activity log capture vars
  l_LOANNMB char(10):=null;
  l_TRANSID number:=null;
  l_dbmdl varchar2(10):=null;
  l_entrydt date:=null;
  l_userid  varchar2(32):=null;
  l_usernm  varchar2(199):=null;
  l_timestampfld date:=null;
  crossover_not_active char(1);
procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
begin
  if p_errval=0 then
    begin
      select * into dbrec from STGCSA.SOFVBGDFTBL where rowid=rowid_in;
    exception when others then
      p_errval:=sqlcode;
      maxsev:=3;
      p_retval:=0;
      p_errmsg:='Fetch of STGCSA.SOFVBGDFTBL row by rowid '||rowid_in
        ||' orinally fetched using '||orig_keyset_values
        ||' failed with '||sqlerrm(sqlcode);
    end;
  end if;
  if p_errval=0 then
    -- set this as lastupdt
    DBrec.lastupdtuserid:=p_userid;
    DBrec.lastupdtdt:=sysdate;
    -- copy into DBrec all table column input parameters that are not null,
    -- saving column name/value if needed for error reporting
    --  (if p_identifier=-1 then move input even if null)
    begin
      if P_DEFRMNTGPKEY is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTGPKEY';
        cur_colvalue:=P_DEFRMNTGPKEY;
        DBrec.DEFRMNTGPKEY:=P_DEFRMNTGPKEY;
      end if;
      if P_DEFRMNTSTRTDT1 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTSTRTDT1';
        cur_colvalue:=P_DEFRMNTSTRTDT1;
        DBrec.DEFRMNTSTRTDT1:=P_DEFRMNTSTRTDT1;
      end if;
      if P_DEFRMNTENDDT1 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTENDDT1';
        cur_colvalue:=P_DEFRMNTENDDT1;
        DBrec.DEFRMNTENDDT1:=P_DEFRMNTENDDT1;
      end if;
      if P_DEFRMNTREQ1AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTREQ1AMT';
        cur_colvalue:=P_DEFRMNTREQ1AMT;
        DBrec.DEFRMNTREQ1AMT:=P_DEFRMNTREQ1AMT;
      end if;
      if P_DEFRMNTSTRTDT2 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTSTRTDT2';
        cur_colvalue:=P_DEFRMNTSTRTDT2;
        DBrec.DEFRMNTSTRTDT2:=P_DEFRMNTSTRTDT2;
      end if;
      if P_DEFRMNTENDDT2 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTENDDT2';
        cur_colvalue:=P_DEFRMNTENDDT2;
        DBrec.DEFRMNTENDDT2:=P_DEFRMNTENDDT2;
      end if;
      if P_DEFRMNTREQ2AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTREQ2AMT';
        cur_colvalue:=P_DEFRMNTREQ2AMT;
        DBrec.DEFRMNTREQ2AMT:=P_DEFRMNTREQ2AMT;
      end if;
      if P_DEFRMNTSTRTDT3 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTSTRTDT3';
        cur_colvalue:=P_DEFRMNTSTRTDT3;
        DBrec.DEFRMNTSTRTDT3:=P_DEFRMNTSTRTDT3;
      end if;
      if P_DEFRMNTENDDT3 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTENDDT3';
        cur_colvalue:=P_DEFRMNTENDDT3;
        DBrec.DEFRMNTENDDT3:=P_DEFRMNTENDDT3;
      end if;
      if P_DEFRMNTREQ3AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTREQ3AMT';
        cur_colvalue:=P_DEFRMNTREQ3AMT;
        DBrec.DEFRMNTREQ3AMT:=P_DEFRMNTREQ3AMT;
      end if;
      if P_DEFRMNTSTRTDT4 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTSTRTDT4';
        cur_colvalue:=P_DEFRMNTSTRTDT4;
        DBrec.DEFRMNTSTRTDT4:=P_DEFRMNTSTRTDT4;
      end if;
      if P_DEFRMNTENDDT4 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTENDDT4';
        cur_colvalue:=P_DEFRMNTENDDT4;
        DBrec.DEFRMNTENDDT4:=P_DEFRMNTENDDT4;
      end if;
      if P_DEFRMNTREQ4AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTREQ4AMT';
        cur_colvalue:=P_DEFRMNTREQ4AMT;
        DBrec.DEFRMNTREQ4AMT:=P_DEFRMNTREQ4AMT;
      end if;
      if P_DEFRMNTSTRTDT5 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTSTRTDT5';
        cur_colvalue:=P_DEFRMNTSTRTDT5;
        DBrec.DEFRMNTSTRTDT5:=P_DEFRMNTSTRTDT5;
      end if;
      if P_DEFRMNTENDDT5 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTENDDT5';
        cur_colvalue:=P_DEFRMNTENDDT5;
        DBrec.DEFRMNTENDDT5:=P_DEFRMNTENDDT5;
      end if;
      if P_DEFRMNTREQ5AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTREQ5AMT';
        cur_colvalue:=P_DEFRMNTREQ5AMT;
        DBrec.DEFRMNTREQ5AMT:=P_DEFRMNTREQ5AMT;
      end if;
      if P_DEFRMNTSTRTDT6 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTSTRTDT6';
        cur_colvalue:=P_DEFRMNTSTRTDT6;
        DBrec.DEFRMNTSTRTDT6:=P_DEFRMNTSTRTDT6;
      end if;
      if P_DEFRMNTENDDT6 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTENDDT6';
        cur_colvalue:=P_DEFRMNTENDDT6;
        DBrec.DEFRMNTENDDT6:=P_DEFRMNTENDDT6;
      end if;
      if P_DEFRMNTREQ6AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTREQ6AMT';
        cur_colvalue:=P_DEFRMNTREQ6AMT;
        DBrec.DEFRMNTREQ6AMT:=P_DEFRMNTREQ6AMT;
      end if;
      if P_DEFRMNTSTRTDT7 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTSTRTDT7';
        cur_colvalue:=P_DEFRMNTSTRTDT7;
        DBrec.DEFRMNTSTRTDT7:=P_DEFRMNTSTRTDT7;
      end if;
      if P_DEFRMNTENDDT7 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTENDDT7';
        cur_colvalue:=P_DEFRMNTENDDT7;
        DBrec.DEFRMNTENDDT7:=P_DEFRMNTENDDT7;
      end if;
      if P_DEFRMNTREQ7AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTREQ7AMT';
        cur_colvalue:=P_DEFRMNTREQ7AMT;
        DBrec.DEFRMNTREQ7AMT:=P_DEFRMNTREQ7AMT;
      end if;
      if P_DEFRMNTSTRTDT8 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTSTRTDT8';
        cur_colvalue:=P_DEFRMNTSTRTDT8;
        DBrec.DEFRMNTSTRTDT8:=P_DEFRMNTSTRTDT8;
      end if;
      if P_DEFRMNTENDDT8 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTENDDT8';
        cur_colvalue:=P_DEFRMNTENDDT8;
        DBrec.DEFRMNTENDDT8:=P_DEFRMNTENDDT8;
      end if;
      if P_DEFRMNTREQ8AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTREQ8AMT';
        cur_colvalue:=P_DEFRMNTREQ8AMT;
        DBrec.DEFRMNTREQ8AMT:=P_DEFRMNTREQ8AMT;
      end if;
      if P_DEFRMNTSTRTDT9 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTSTRTDT9';
        cur_colvalue:=P_DEFRMNTSTRTDT9;
        DBrec.DEFRMNTSTRTDT9:=P_DEFRMNTSTRTDT9;
      end if;
      if P_DEFRMNTENDDT9 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTENDDT9';
        cur_colvalue:=P_DEFRMNTENDDT9;
        DBrec.DEFRMNTENDDT9:=P_DEFRMNTENDDT9;
      end if;
      if P_DEFRMNTREQ9AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTREQ9AMT';
        cur_colvalue:=P_DEFRMNTREQ9AMT;
        DBrec.DEFRMNTREQ9AMT:=P_DEFRMNTREQ9AMT;
      end if;
      if P_DEFRMNTSTRTDT10 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTSTRTDT10';
        cur_colvalue:=P_DEFRMNTSTRTDT10;
        DBrec.DEFRMNTSTRTDT10:=P_DEFRMNTSTRTDT10;
      end if;
      if P_DEFRMNTENDDT10 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTENDDT10';
        cur_colvalue:=P_DEFRMNTENDDT10;
        DBrec.DEFRMNTENDDT10:=P_DEFRMNTENDDT10;
      end if;
      if P_DEFRMNTREQ10AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTREQ10AMT';
        cur_colvalue:=P_DEFRMNTREQ10AMT;
        DBrec.DEFRMNTREQ10AMT:=P_DEFRMNTREQ10AMT;
      end if;
      if P_DEFRMNTSTRTDT11 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTSTRTDT11';
        cur_colvalue:=P_DEFRMNTSTRTDT11;
        DBrec.DEFRMNTSTRTDT11:=P_DEFRMNTSTRTDT11;
      end if;
      if P_DEFRMNTENDDT11 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTENDDT11';
        cur_colvalue:=P_DEFRMNTENDDT11;
        DBrec.DEFRMNTENDDT11:=P_DEFRMNTENDDT11;
      end if;
      if P_DEFRMNTREQ11AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTREQ11AMT';
        cur_colvalue:=P_DEFRMNTREQ11AMT;
        DBrec.DEFRMNTREQ11AMT:=P_DEFRMNTREQ11AMT;
      end if;
      if P_DEFRMNTSTRTDT12 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTSTRTDT12';
        cur_colvalue:=P_DEFRMNTSTRTDT12;
        DBrec.DEFRMNTSTRTDT12:=P_DEFRMNTSTRTDT12;
      end if;
      if P_DEFRMNTENDDT12 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTENDDT12';
        cur_colvalue:=P_DEFRMNTENDDT12;
        DBrec.DEFRMNTENDDT12:=P_DEFRMNTENDDT12;
      end if;
      if P_DEFRMNTREQ12AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTREQ12AMT';
        cur_colvalue:=P_DEFRMNTREQ12AMT;
        DBrec.DEFRMNTREQ12AMT:=P_DEFRMNTREQ12AMT;
      end if;
      if P_DEFRMNTSTRTDT13 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTSTRTDT13';
        cur_colvalue:=P_DEFRMNTSTRTDT13;
        DBrec.DEFRMNTSTRTDT13:=P_DEFRMNTSTRTDT13;
      end if;
      if P_DEFRMNTENDDT13 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTENDDT13';
        cur_colvalue:=P_DEFRMNTENDDT13;
        DBrec.DEFRMNTENDDT13:=P_DEFRMNTENDDT13;
      end if;
      if P_DEFRMNTREQ13AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTREQ13AMT';
        cur_colvalue:=P_DEFRMNTREQ13AMT;
        DBrec.DEFRMNTREQ13AMT:=P_DEFRMNTREQ13AMT;
      end if;
      if P_DEFRMNTSTRTDT14 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTSTRTDT14';
        cur_colvalue:=P_DEFRMNTSTRTDT14;
        DBrec.DEFRMNTSTRTDT14:=P_DEFRMNTSTRTDT14;
      end if;
      if P_DEFRMNTENDDT14 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTENDDT14';
        cur_colvalue:=P_DEFRMNTENDDT14;
        DBrec.DEFRMNTENDDT14:=P_DEFRMNTENDDT14;
      end if;
      if P_DEFRMNTREQ14AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTREQ14AMT';
        cur_colvalue:=P_DEFRMNTREQ14AMT;
        DBrec.DEFRMNTREQ14AMT:=P_DEFRMNTREQ14AMT;
      end if;
      if P_DEFRMNTSTRTDT15 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTSTRTDT15';
        cur_colvalue:=P_DEFRMNTSTRTDT15;
        DBrec.DEFRMNTSTRTDT15:=P_DEFRMNTSTRTDT15;
      end if;
      if P_DEFRMNTENDDT15 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTENDDT15';
        cur_colvalue:=P_DEFRMNTENDDT15;
        DBrec.DEFRMNTENDDT15:=P_DEFRMNTENDDT15;
      end if;
      if P_DEFRMNTREQ15AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTREQ15AMT';
        cur_colvalue:=P_DEFRMNTREQ15AMT;
        DBrec.DEFRMNTREQ15AMT:=P_DEFRMNTREQ15AMT;
      end if;
      if P_DEFRMNTDTCREAT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTDTCREAT';
        cur_colvalue:=P_DEFRMNTDTCREAT;
        DBrec.DEFRMNTDTCREAT:=P_DEFRMNTDTCREAT;
      end if;
      if P_DEFRMNTDTMOD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DEFRMNTDTMOD';
        cur_colvalue:=P_DEFRMNTDTMOD;
        DBrec.DEFRMNTDTMOD:=P_DEFRMNTDTMOD;
      end if;
    exception when others then
      p_errval:=sqlcode;
      if maxsev<3 then maxsev:=3; end if;
      p_errmsg:='Error using '||cur_colname||' Value >'||cur_colvalue||'<';
    end;
  end if;
  -- if all still ok do actual update
  if p_errval=0 then
    Begin
      update STGCSA.SOFVBGDFTBL
        set lastupdtuserid=DBrec.lastupdtuserid,
          lastupdtdt=DBrec.lastupdtdt
          ,DEFRMNTGPKEY=DBrec.DEFRMNTGPKEY
          ,DEFRMNTSTRTDT1=DBrec.DEFRMNTSTRTDT1
          ,DEFRMNTENDDT1=DBrec.DEFRMNTENDDT1
          ,DEFRMNTREQ1AMT=DBrec.DEFRMNTREQ1AMT
          ,DEFRMNTSTRTDT2=DBrec.DEFRMNTSTRTDT2
          ,DEFRMNTENDDT2=DBrec.DEFRMNTENDDT2
          ,DEFRMNTREQ2AMT=DBrec.DEFRMNTREQ2AMT
          ,DEFRMNTSTRTDT3=DBrec.DEFRMNTSTRTDT3
          ,DEFRMNTENDDT3=DBrec.DEFRMNTENDDT3
          ,DEFRMNTREQ3AMT=DBrec.DEFRMNTREQ3AMT
          ,DEFRMNTSTRTDT4=DBrec.DEFRMNTSTRTDT4
          ,DEFRMNTENDDT4=DBrec.DEFRMNTENDDT4
          ,DEFRMNTREQ4AMT=DBrec.DEFRMNTREQ4AMT
          ,DEFRMNTSTRTDT5=DBrec.DEFRMNTSTRTDT5
          ,DEFRMNTENDDT5=DBrec.DEFRMNTENDDT5
          ,DEFRMNTREQ5AMT=DBrec.DEFRMNTREQ5AMT
          ,DEFRMNTSTRTDT6=DBrec.DEFRMNTSTRTDT6
          ,DEFRMNTENDDT6=DBrec.DEFRMNTENDDT6
          ,DEFRMNTREQ6AMT=DBrec.DEFRMNTREQ6AMT
          ,DEFRMNTSTRTDT7=DBrec.DEFRMNTSTRTDT7
          ,DEFRMNTENDDT7=DBrec.DEFRMNTENDDT7
          ,DEFRMNTREQ7AMT=DBrec.DEFRMNTREQ7AMT
          ,DEFRMNTSTRTDT8=DBrec.DEFRMNTSTRTDT8
          ,DEFRMNTENDDT8=DBrec.DEFRMNTENDDT8
          ,DEFRMNTREQ8AMT=DBrec.DEFRMNTREQ8AMT
          ,DEFRMNTSTRTDT9=DBrec.DEFRMNTSTRTDT9
          ,DEFRMNTENDDT9=DBrec.DEFRMNTENDDT9
          ,DEFRMNTREQ9AMT=DBrec.DEFRMNTREQ9AMT
          ,DEFRMNTSTRTDT10=DBrec.DEFRMNTSTRTDT10
          ,DEFRMNTENDDT10=DBrec.DEFRMNTENDDT10
          ,DEFRMNTREQ10AMT=DBrec.DEFRMNTREQ10AMT
          ,DEFRMNTSTRTDT11=DBrec.DEFRMNTSTRTDT11
          ,DEFRMNTENDDT11=DBrec.DEFRMNTENDDT11
          ,DEFRMNTREQ11AMT=DBrec.DEFRMNTREQ11AMT
          ,DEFRMNTSTRTDT12=DBrec.DEFRMNTSTRTDT12
          ,DEFRMNTENDDT12=DBrec.DEFRMNTENDDT12
          ,DEFRMNTREQ12AMT=DBrec.DEFRMNTREQ12AMT
          ,DEFRMNTSTRTDT13=DBrec.DEFRMNTSTRTDT13
          ,DEFRMNTENDDT13=DBrec.DEFRMNTENDDT13
          ,DEFRMNTREQ13AMT=DBrec.DEFRMNTREQ13AMT
          ,DEFRMNTSTRTDT14=DBrec.DEFRMNTSTRTDT14
          ,DEFRMNTENDDT14=DBrec.DEFRMNTENDDT14
          ,DEFRMNTREQ14AMT=DBrec.DEFRMNTREQ14AMT
          ,DEFRMNTSTRTDT15=DBrec.DEFRMNTSTRTDT15
          ,DEFRMNTENDDT15=DBrec.DEFRMNTENDDT15
          ,DEFRMNTREQ15AMT=DBrec.DEFRMNTREQ15AMT
          ,DEFRMNTDTCREAT=DBrec.DEFRMNTDTCREAT
          ,DEFRMNTDTMOD=DBrec.DEFRMNTDTMOD
        where rowid=rowid_in;
      recnum:=recnum+1;
    exception when others then
      p_errval:=sqlcode;
      p_retval:=0;
      if maxsev<3 then maxsev:=3; end if;
      p_errmsg:= 'Error while doing update on STGCSA.SOFVBGDFTBL, on rec with rowid '
      ||rowid_in||' previously fetched using key: '||orig_keyset_values
      ||' got Oracle error '||sqlerrm(sqlcode);
    end;
  end if;
END;
begin
  -- setup
  savepoint SOFVBGDFUPDTSP;
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  recnum:=0;
  runtime.logger(logged_msg_id,'STDLOG',100,'Init SOFVBGDFUPDTSP',p_userid,4,
    logtxt1=>'SOFVBGDFUPDTSP',logtxt2=>'stdtblupd_template v3.22 12 Oct 2018',
    PROGRAM_NAME=>'SOFVBGDFUPDTSP');
  --
  --
  -- Check to see if crossover is ongoing, in which case updates are prohibited
  SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
  IF crossover_not_active != 'Y' THEN
    p_errval := -20099;
    p_errmsg := 'Warning: Cannot update table SOFVBGDFTBL at this time because crossover is running, please try later.';
  END IF;
  -- end of crossover_lockout_check
--
-- special p_identifier kludge
--  if p_identifier is set to -1 then it treated as 0 (update by PK)
--  but a flag is set use passed value EVEN IF NULL.  Since for option -1
--  every column is updated to what was passed, intended values MUST be passed
--  for all columns.  This option allows setting colulmns to null!
  overridden_p_identifier:=p_identifier;
  if p_identifier=-1 then
    do_normal_field_checking:='N';
    overridden_p_identifier:=0;
  end if;
  case overridden_p_identifier
  when 0 then
    -- case to update one row based on primary key
    -- p_identifier=0 (choice 0) is default action for a stored procedure
    -- get existing version of row
    if p_errval=0 then
      keystouse:='(DEFRMNTGPKEY)=('||P_DEFRMNTGPKEY||')';
      begin
        select rowid into rec_rowid from STGCSA.SOFVBGDFTBL where 1=1
         and DEFRMNTGPKEY=P_DEFRMNTGPKEY;
      exception when no_data_found then
        p_errval:=0;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Warning No STGCSA.SOFVBGDFTBL row to update with key(s) '
          ||keystouse;
      end;
    end if;
    if p_errval=0 then
      fetch_and_update_by_rowid(rec_rowid, keystouse);
    end if;
  else /* P_IDENTIFIER was unexpected value */
    p_errval:=-20002;
    if maxsev<3 then maxsev:=3; end if;
    p_errmsg:='Error P_IDENTIFIER contained unexpected value: '||p_identifier;
  end case;
  P_RETVAL:=RECNUM;
  -- post to log
  if p_errval=0 then
    runtime.logger(logged_msg_id,'STDLOG',101,
      'End SOFVBGDFUPDTSP With 0 return, P_RETVAL='
      ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
      1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld
      ,PROGRAM_NAME=>'SOFVBGDFUPDTSP');
  else
    ROLLBACK TO SOFVBGDFUPDTSP;
    p_errmsg:=p_errmsg||' in SOFVBGDFUPDTSP build 2018-11-07 11:35:23';
    runtime.logger(logged_msg_id,'STDERR',103,p_errmsg,p_userid,
      maxsev, l_usernm, l_loannmb,l_transid, l_dbmdl, l_entrydt, l_timestampfld,
      PROGRAM_NAME=>'SOFVBGDFUPDTSP',force_log_entry=>true);
  end if;
EXCEPTION
WHEN OTHERS THEN
  ROLLBACK TO SOFVBGDFUPDTSP;
  p_errmsg:=' Error outer exception handler caught Oracle error:'||SQLERRM(SQLCODE)
    ||' in SOFVBGDFUPDTSP build 2018-11-07 11:35:23';
  runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
    3,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld,
    PROGRAM_NAME=>'SOFVBGDFUPDTSP',force_log_entry=>true);
--
END SOFVBGDFUPDTSP;
/


GRANT EXECUTE ON STGCSA.SOFVBGDFUPDTSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.SOFVBGDFUPDTSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.SOFVBGDFUPDTSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.SOFVBGDFUPDTSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.SOFVBGDFUPDTSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.SOFVBGDFUPDTSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.SOFVBGDFUPDTSP TO STGCSADEVROLE;

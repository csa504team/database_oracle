DROP PROCEDURE STGCSA.CORETRANSUPDTSP2;

CREATE OR REPLACE PROCEDURE STGCSA.CORETRANSUPDTSP2(
    p_retval out number,
    p_errval out number,
    p_errmsg  out varchar2,
    p_identifier number:=0,
    p_userid varchar2
   ,p_CREATDT DATE:=null
   ,p_CREATUSERID VARCHAR2:=null
   ,p_LOANNMB CHAR:=null
   ,p_STATIDCUR NUMBER:=null
   ,p_TIMESTAMPFLD DATE:=null
   ,p_TRANSID NUMBER:=null
   ,p_TRANSIND1 NUMBER:=null
   ,p_TRANSTYPID NUMBER:=null
) as
 /*
  Created on: 2018-02-19 15:16:25
  Created by: GENR
  Crerated from template stdtblupd_template v3.13 03 Feb 2018 on 2018-02-19 15:16:26
    Using SNAP V3.04 12 Feb 2018 J. Low Binary Frond, Select Computing
  Procedure CORETRANSUPDTSP performs UPDATE on STGCSA.CORETRANSTBL
    for P_IDENTIFIER=0 qualification is on Primary Key: TRANSID
    for P_IDENTIFIER=1 qualification is on TRANSID, TRANSTYPID
    for P_IDENTIFIER=2 qualification is on CREATUSERID, CREATDT
    Updates all columns except the qualification keys and columns for which a
    null value is passed.  Logging to audit is included if required for this table.
    SPECIAL secret update kludge!
      if p_identifier is set to -1 then a flag is set to use all input parm values
      and p_identifier is changed to 0 (update by PK).
      THEREFORE if using p_identifier=-1 you MUST pass intended value for all columns.
      This option allows setting colulmns to null!
*/
  old_rec STGCSA.CORETRANSTBL%rowtype;
  new_rec STGCSA.CORETRANSTBL%rowtype;
  logged_msg_id number;
  audretval number;
  recnum number;
  rec_rowid rowid;
  keystouse varchar2(256);
  pctsign char(1):='%';
  -- standard activity log capture vard
  log_LOANNMB char(10):=null;
  log_TRANSID number:=null;
  log_dbmdl varchar2(10):=null;
  log_entrydt date:=null;
  log_userid  varchar2(32):=null;
  log_usernm  varchar2(199):=null;
  log_timestampfld date:=null;
  cur_col_name varchar2(100):=null;
  do_normal_field_checking char(1):='Y';
  overridden_p_identifier number;
procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
begin
  if p_errval<>0 then
    return;
  end if;
  begin
    select * into old_rec from STGCSA.CORETRANSTBL where rowid=rowid_in;
  exception when others then
    p_errval:=sqlcode;
    p_retval:=0;
    p_errmsg:=' In  CORETRANSUPDTSP build 2018-02-19 15:16:25 '
      ||'Select of STGCSA.CORETRANSTBL row by rowid '||rowid_in
      ||' orinally fetched using '||orig_keyset_values
      ||' failed with '||sqlerrm(sqlcode);
    ROLLBACK TO CORETRANSUPDTSP;
  end;
  if p_errval=0 then
    -- new rec starts with old rec
    new_rec:=old_rec;
    -- set this as lastupdt
    new_rec.lastupdtuserid:=p_userid;
    new_rec.lastupdtdt:=sysdate;
    -- copy into new rec all table column input parameters that are not null
    runtime.errfound:=false;
    runtime.errstr:=' ';
    if P_TRANSID is not null or DO_NORMAL_FIELD_CHECKING='N' then
      cur_col_name:='P_TRANSID';
      new_rec.TRANSID:=P_TRANSID;    
    end if;
    if P_TRANSTYPID is not null or DO_NORMAL_FIELD_CHECKING='N' then
      cur_col_name:='P_TRANSTYPID';
      new_rec.TRANSTYPID:=P_TRANSTYPID;    
    end if;
    if P_LOANNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
      cur_col_name:='P_LOANNMB';
      new_rec.LOANNMB:=P_LOANNMB;    
    end if;
    if P_TRANSIND1 is not null or DO_NORMAL_FIELD_CHECKING='N' then
      cur_col_name:='P_TRANSIND1';
      new_rec.TRANSIND1:=P_TRANSIND1;    
    end if;
    if P_STATIDCUR is not null or DO_NORMAL_FIELD_CHECKING='N' then
      cur_col_name:='P_STATIDCUR';
      new_rec.STATIDCUR:=P_STATIDCUR;    
    end if;
    if P_TIMESTAMPFLD is not null or DO_NORMAL_FIELD_CHECKING='N' then
      cur_col_name:='P_TIMESTAMPFLD';
      new_rec.TIMESTAMPFLD:=P_TIMESTAMPFLD;    
    end if;
    if DO_NORMAL_FIELD_CHECKING='N' then
      null;
      cur_col_name:='P_CREATDT';
      new_rec.CREATDT:=P_CREATDT; 
      cur_col_name:='P_CREATUSERID';
      new_rec.CREATUSERID:=P_CREATUSERID;
    end if;
    cur_col_name:=null;
  end if;
  if p_errval=0 then
    -- allow audit to record changed fields as needed
    stgcsa.CORETRANSAUDTSP(0,audretval,p_errval, p_errmsg, p_userid,'C','?',OLD_REC,new_rec);
  end if;
  -- if all still ok do actual update
  if p_errval=0 then
    Begin
      update STGCSA.CORETRANSTBL
        set lastupdtuserid=new_rec.lastupdtuserid,
          lastupdtdt=new_rec.lastupdtdt
        where rowid=rowid_in;
      recnum:=recnum+1;
    exception when others then
      p_errval:=sqlcode;
      p_retval:=0;
      p_errmsg:= ' In  CORETRANSUPDTSP build 2018-02-19 15:16:25 '
      ||'while doing update on STGCSA.CORETRANSTBL, on rec with rowid '
      ||rowid_in||' previously fetched using key: '||orig_keyset_values
      ||' got SQLERR '||sqlerrm(sqlcode);
      ROLLBACK TO CORETRANSUPDTSP;
      runtime.logger(logged_msg_id,'RTERR',20,p_errmsg,p_userid);
    end;
  end if;
END;
begin
  -- setup
  savepoint CORETRANSUPDTSP;
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  recnum:=0;
  --
  log_TRANSID:=P_TRANSID;  log_LOANNMB:=P_LOANNMB;  log_TIMESTAMPFLD:=P_TIMESTAMPFLD;
--
-- special p_identifier kludge
--  if p_identifier is set to -1 then it is changed to 0 (update by PK)
--  but a flag is set to skip normal field validation.
--  In option 0 every column is updated to what is passed, so intended values
--  MUST be passed for all columns.  This option allows setting colulmns to null!
  overridden_p_identifier:=p_identifier;
  if p_identifier=-1 then
    do_normal_field_checking:='N';
    overridden_p_identifier:=0;
  end if;
  case overridden_p_identifier
  when 0 then
    -- case to update one row based on primary key
    -- p_identifier=0 (choice 0) is default action for a stored procedure
    -- get existing version of row
    -- standard caution... only proceed if no error
    if p_errval=0 then
      begin
        keystouse:='(TRANSID)=('||P_TRANSID||') ';
        select rowid into rec_rowid from STGCSA.CORETRANSTBL where 1=1
;
      exception when no_data_found then
        p_errval:=sqlcode;
        p_retval:=0;
        p_errmsg:=' In  CORETRANSUPDTSP build 2018-02-19 15:16:25 '
          ||'No STGCSA.CORETRANSTBL row to update with key(s) '||keystouse;
      end;
    end if;
    if p_errval=0 then
      fetch_and_update_by_rowid(rec_rowid, keystouse);
    end if;
  when 1 then  
    -- case to update one or more rows based on keyset KEYS1
    if p_errval=0 then
    

        keystouse:='(TRANSID)=('||P_TRANSID||') '||
          '(TRANSTYPID)=('||P_TRANSTYPID||') '||'';
      begin
        for r1 in (select rowid from STGCSA.CORETRANSTBL a where 1=1 
          and TRANSID=P_TRANSID
            and TRANSTYPID=P_TRANSTYPID
  ) 
        loop
          rec_rowid:=r1.rowid;
          if p_errval=0 then
            fetch_and_update_by_rowid(rec_rowid, keystouse);
          end if;  
        end loop;  
      exception when others then
        ROLLBACK TO CORETRANSUPDTSP;
        p_errval:=sqlcode;
        p_retval:=0;
        p_errmsg:=' In  CORETRANSUPDTSP build 2018-02-19 15:16:25 '
         ||'Oracle returned error fetching STGCSA.CORETRANSTBL row(s) with key(s) '
         ||keystouse;
      end;
      if recnum=0 then  
        ROLLBACK TO CORETRANSUPDTSP;
        p_errval:=sqlcode;
        p_retval:=0;
        p_errmsg:=' In  CORETRANSUPDTSP build 2018-02-19 15:16:25 '
          ||'No STGCSA.CORETRANSTBL row(s) to update with key(s) '
          ||keystouse;
      end if;    
    end if;
  when 2 then  
    -- case to update one or more rows based on keyset KEYS2
    if p_errval=0 then
    

        keystouse:='(CREATDT)=('||P_CREATDT||') '||
          '(CREATUSERID)=('||P_CREATUSERID||') '||'';
      begin
        for r2 in (select rowid from STGCSA.CORETRANSTBL a where 1=1 
          and CREATDT=P_CREATDT
            and CREATUSERID=P_CREATUSERID
  ) 
        loop
          rec_rowid:=r2.rowid;
          if p_errval=0 then
            fetch_and_update_by_rowid(rec_rowid, keystouse);
          end if;  
        end loop;  
      exception when others then
        ROLLBACK TO CORETRANSUPDTSP;
        p_errval:=sqlcode;
        p_retval:=0;
        p_errmsg:=' In  CORETRANSUPDTSP build 2018-02-19 15:16:25 '
         ||'Oracle returned error fetching STGCSA.CORETRANSTBL row(s) with key(s) '
         ||keystouse;
      end;
      if recnum=0 then  
        ROLLBACK TO CORETRANSUPDTSP;
        p_errval:=sqlcode;
        p_retval:=0;
        p_errmsg:=' In  CORETRANSUPDTSP build 2018-02-19 15:16:25 '
          ||'No STGCSA.CORETRANSTBL row(s) to update with key(s) '
          ||keystouse;
      end if;    
    end if;
  else /* P_IDENTIFIER was unexpected value */
    ROLLBACK TO CORETRANSUPDTSP;
    p_errval:=-20002;
    p_errmsg:=' In  CORETRANSUPDTSP build 2018-02-19 15:16:25 '
      ||'P_IDENTIFIER contained unexpected value: '||p_identifier;
  end case;
  P_RETVAL:=RECNUM;
  -- post to log
  log_TRANSID:=new_rec.TRANSID;  log_LOANNMB:=new_rec.LOANNMB;  log_TIMESTAMPFLD:=new_rec.TIMESTAMPFLD;
  if p_errval=0 then
    runtime.logger(logged_msg_id,'STDLOG',101,
      'End CORETRANSUPDTSP With 0 return, P_RETVAL='
      ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
      1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
  else
    runtime.logger(logged_msg_id,'STDERR',103,
      'CORETRANSUPDTSP returned non-zero, P_RETVAL='
      ||p_retval||' MSG='||p_errmsg,p_userid,
      2,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
  end if;
EXCEPTION
WHEN OTHERS THEN
  p_errval:=SQLCODE;
  if cur_col_name is not null then
    cur_col_name:=' moving input parm '||cur_col_name||' to DB  Column';
  end if;
  p_errmsg:=' In CORETRANSUPDTSP build 2018-02-19 15:16:25'||cur_col_name
   ||' outer handler caught Oracle error:'||SQLERRM(SQLCODE);
  ROLLBACK TO CORETRANSUPDTSP;
  runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
  3,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
--
END CORETRANSUPDTSP2;
/


GRANT EXECUTE ON STGCSA.CORETRANSUPDTSP2 TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.CORETRANSUPDTSP2 TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.CORETRANSUPDTSP2 TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.CORETRANSUPDTSP2 TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.CORETRANSUPDTSP2 TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.CORETRANSUPDTSP2 TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.CORETRANSUPDTSP2 TO STGCSADEVROLE;

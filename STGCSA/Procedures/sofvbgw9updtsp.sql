DROP PROCEDURE STGCSA.SOFVBGW9UPDTSP;

CREATE OR REPLACE PROCEDURE STGCSA.SOFVBGW9UPDTSP(
    p_retval out number,
    p_errval out number,
    p_errmsg  out varchar2,
    p_identifier number:=0,
    p_userid varchar2
   ,p_LOANNMB CHAR:=null
   ,p_W9TAXID CHAR:=null
   ,p_W9NM VARCHAR2:=null
   ,p_W9MAILADRSTR1NM VARCHAR2:=null
   ,p_W9MAILADRSTR2NM VARCHAR2:=null
   ,p_W9MAILADRCTYNM VARCHAR2:=null
   ,p_W9MAILADRSTCD CHAR:=null
   ,p_W9MAILADRZIPCD CHAR:=null
   ,p_W9MAILADRZIP4CD CHAR:=null
   ,p_W9VERIFICATIONIND CHAR:=null
   ,p_W9VERIFICATIONDT DATE:=null
   ,p_W9NMMAILADRCHNGIND CHAR:=null
   ,p_W9NMADRCHNGDT DATE:=null
   ,p_W9TAXFORMFLD CHAR:=null
   ,p_W9TAXMAINTNDT DATE:=null
) as
 /*
  Created on: 2018-11-07 11:35:28
  Created by: GENR
  Crerated from template stdtblupd_template v3.22 12 Oct 2018 on 2018-11-07 11:35:28
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  Procedure SOFVBGW9UPDTSP performs UPDATE on STGCSA.SOFVBGW9TBL
    for P_IDENTIFIER=0 qualification is on Primary Key: LOANNMB
    Updates all columns except the qualification keys and columns for which a
      null value is passed.
    SPECIAL secret update kludge!
      if p_identifier is set to -1 then a flag is set to use all input parm values
      and p_identifier is changed to 0 (update by PK).
      THEREFORE if using p_identifier=-1 you MUST pass intended value for all columns.
      This option allows setting colulmns to null!
*/
  dbrec STGCSA.SOFVBGW9TBL%rowtype;
  logged_msg_id number;
  recnum number;
  rec_rowid rowid;
  keystouse varchar2(256);
  maxsev number:=0;
  pctsign char(1):='%';
  cur_colname varchar2(100):=null;
  cur_colvalue varchar2(4000);
  do_normal_field_checking char(1):='Y';
  overridden_p_identifier number;
  -- standard activity log capture vars
  l_LOANNMB char(10):=null;
  l_TRANSID number:=null;
  l_dbmdl varchar2(10):=null;
  l_entrydt date:=null;
  l_userid  varchar2(32):=null;
  l_usernm  varchar2(199):=null;
  l_timestampfld date:=null;
  crossover_not_active char(1);
procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
begin
  if p_errval=0 then
    begin
      select * into dbrec from STGCSA.SOFVBGW9TBL where rowid=rowid_in;
    exception when others then
      p_errval:=sqlcode;
      maxsev:=3;
      p_retval:=0;
      p_errmsg:='Fetch of STGCSA.SOFVBGW9TBL row by rowid '||rowid_in
        ||' orinally fetched using '||orig_keyset_values
        ||' failed with '||sqlerrm(sqlcode);
    end;
  end if;
  if p_errval=0 then
    -- set this as lastupdt
    DBrec.lastupdtuserid:=p_userid;
    DBrec.lastupdtdt:=sysdate;
    -- copy into DBrec all table column input parameters that are not null,
    -- saving column name/value if needed for error reporting
    --  (if p_identifier=-1 then move input even if null)
    begin
      if P_LOANNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_LOANNMB';
        cur_colvalue:=P_LOANNMB;
        DBrec.LOANNMB:=P_LOANNMB;
      end if;
      if P_W9TAXID is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_W9TAXID';
        cur_colvalue:=P_W9TAXID;
        DBrec.W9TAXID:=P_W9TAXID;
      end if;
      if P_W9NM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_W9NM';
        cur_colvalue:=P_W9NM;
        DBrec.W9NM:=P_W9NM;
      end if;
      if P_W9MAILADRSTR1NM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_W9MAILADRSTR1NM';
        cur_colvalue:=P_W9MAILADRSTR1NM;
        DBrec.W9MAILADRSTR1NM:=P_W9MAILADRSTR1NM;
      end if;
      if P_W9MAILADRSTR2NM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_W9MAILADRSTR2NM';
        cur_colvalue:=P_W9MAILADRSTR2NM;
        DBrec.W9MAILADRSTR2NM:=P_W9MAILADRSTR2NM;
      end if;
      if P_W9MAILADRCTYNM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_W9MAILADRCTYNM';
        cur_colvalue:=P_W9MAILADRCTYNM;
        DBrec.W9MAILADRCTYNM:=P_W9MAILADRCTYNM;
      end if;
      if P_W9MAILADRSTCD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_W9MAILADRSTCD';
        cur_colvalue:=P_W9MAILADRSTCD;
        DBrec.W9MAILADRSTCD:=P_W9MAILADRSTCD;
      end if;
      if P_W9MAILADRZIPCD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_W9MAILADRZIPCD';
        cur_colvalue:=P_W9MAILADRZIPCD;
        DBrec.W9MAILADRZIPCD:=P_W9MAILADRZIPCD;
      end if;
      if P_W9MAILADRZIP4CD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_W9MAILADRZIP4CD';
        cur_colvalue:=P_W9MAILADRZIP4CD;
        DBrec.W9MAILADRZIP4CD:=P_W9MAILADRZIP4CD;
      end if;
      if P_W9VERIFICATIONIND is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_W9VERIFICATIONIND';
        cur_colvalue:=P_W9VERIFICATIONIND;
        DBrec.W9VERIFICATIONIND:=P_W9VERIFICATIONIND;
      end if;
      if P_W9VERIFICATIONDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_W9VERIFICATIONDT';
        cur_colvalue:=P_W9VERIFICATIONDT;
        DBrec.W9VERIFICATIONDT:=P_W9VERIFICATIONDT;
      end if;
      if P_W9NMMAILADRCHNGIND is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_W9NMMAILADRCHNGIND';
        cur_colvalue:=P_W9NMMAILADRCHNGIND;
        DBrec.W9NMMAILADRCHNGIND:=P_W9NMMAILADRCHNGIND;
      end if;
      if P_W9NMADRCHNGDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_W9NMADRCHNGDT';
        cur_colvalue:=P_W9NMADRCHNGDT;
        DBrec.W9NMADRCHNGDT:=P_W9NMADRCHNGDT;
      end if;
      if P_W9TAXFORMFLD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_W9TAXFORMFLD';
        cur_colvalue:=P_W9TAXFORMFLD;
        DBrec.W9TAXFORMFLD:=P_W9TAXFORMFLD;
      end if;
      if P_W9TAXMAINTNDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_W9TAXMAINTNDT';
        cur_colvalue:=P_W9TAXMAINTNDT;
        DBrec.W9TAXMAINTNDT:=P_W9TAXMAINTNDT;
      end if;
    exception when others then
      p_errval:=sqlcode;
      if maxsev<3 then maxsev:=3; end if;
      p_errmsg:='Error using '||cur_colname||' Value >'||cur_colvalue||'<';
    end;
  end if;
  -- if all still ok do actual update
  if p_errval=0 then
    Begin
      update STGCSA.SOFVBGW9TBL
        set lastupdtuserid=DBrec.lastupdtuserid,
          lastupdtdt=DBrec.lastupdtdt
          ,LOANNMB=DBrec.LOANNMB
          ,W9TAXID=DBrec.W9TAXID
          ,W9NM=DBrec.W9NM
          ,W9MAILADRSTR1NM=DBrec.W9MAILADRSTR1NM
          ,W9MAILADRSTR2NM=DBrec.W9MAILADRSTR2NM
          ,W9MAILADRCTYNM=DBrec.W9MAILADRCTYNM
          ,W9MAILADRSTCD=DBrec.W9MAILADRSTCD
          ,W9MAILADRZIPCD=DBrec.W9MAILADRZIPCD
          ,W9MAILADRZIP4CD=DBrec.W9MAILADRZIP4CD
          ,W9VERIFICATIONIND=DBrec.W9VERIFICATIONIND
          ,W9VERIFICATIONDT=DBrec.W9VERIFICATIONDT
          ,W9NMMAILADRCHNGIND=DBrec.W9NMMAILADRCHNGIND
          ,W9NMADRCHNGDT=DBrec.W9NMADRCHNGDT
          ,W9TAXFORMFLD=DBrec.W9TAXFORMFLD
          ,W9TAXMAINTNDT=DBrec.W9TAXMAINTNDT
        where rowid=rowid_in;
      recnum:=recnum+1;
    exception when others then
      p_errval:=sqlcode;
      p_retval:=0;
      if maxsev<3 then maxsev:=3; end if;
      p_errmsg:= 'Error while doing update on STGCSA.SOFVBGW9TBL, on rec with rowid '
      ||rowid_in||' previously fetched using key: '||orig_keyset_values
      ||' got Oracle error '||sqlerrm(sqlcode);
    end;
  end if;
END;
begin
  -- setup
  savepoint SOFVBGW9UPDTSP;
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  recnum:=0;
  runtime.logger(logged_msg_id,'STDLOG',100,'Init SOFVBGW9UPDTSP',p_userid,4,
    logtxt1=>'SOFVBGW9UPDTSP',logtxt2=>'stdtblupd_template v3.22 12 Oct 2018',
    PROGRAM_NAME=>'SOFVBGW9UPDTSP');
  --
  l_LOANNMB:=P_LOANNMB;
  --
  -- Check to see if crossover is ongoing, in which case updates are prohibited
  SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
  IF crossover_not_active != 'Y' THEN
    p_errval := -20099;
    p_errmsg := 'Warning: Cannot update table SOFVBGW9TBL at this time because crossover is running, please try later.';
  END IF;
  -- end of crossover_lockout_check
--
-- special p_identifier kludge
--  if p_identifier is set to -1 then it treated as 0 (update by PK)
--  but a flag is set use passed value EVEN IF NULL.  Since for option -1
--  every column is updated to what was passed, intended values MUST be passed
--  for all columns.  This option allows setting colulmns to null!
  overridden_p_identifier:=p_identifier;
  if p_identifier=-1 then
    do_normal_field_checking:='N';
    overridden_p_identifier:=0;
  end if;
  case overridden_p_identifier
  when 0 then
    -- case to update one row based on primary key
    -- p_identifier=0 (choice 0) is default action for a stored procedure
    -- get existing version of row
    if p_errval=0 then
      keystouse:='(LOANNMB)=('||P_LOANNMB||')';
      begin
        select rowid into rec_rowid from STGCSA.SOFVBGW9TBL where 1=1
         and LOANNMB=P_LOANNMB;
      exception when no_data_found then
        p_errval:=0;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Warning No STGCSA.SOFVBGW9TBL row to update with key(s) '
          ||keystouse;
      end;
    end if;
    if p_errval=0 then
      fetch_and_update_by_rowid(rec_rowid, keystouse);
    end if;
  else /* P_IDENTIFIER was unexpected value */
    p_errval:=-20002;
    if maxsev<3 then maxsev:=3; end if;
    p_errmsg:='Error P_IDENTIFIER contained unexpected value: '||p_identifier;
  end case;
  P_RETVAL:=RECNUM;
  -- post to log
  if p_errval=0 then
    runtime.logger(logged_msg_id,'STDLOG',101,
      'End SOFVBGW9UPDTSP With 0 return, P_RETVAL='
      ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
      1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld
      ,PROGRAM_NAME=>'SOFVBGW9UPDTSP');
  else
    ROLLBACK TO SOFVBGW9UPDTSP;
    p_errmsg:=p_errmsg||' in SOFVBGW9UPDTSP build 2018-11-07 11:35:28';
    runtime.logger(logged_msg_id,'STDERR',103,p_errmsg,p_userid,
      maxsev, l_usernm, l_loannmb,l_transid, l_dbmdl, l_entrydt, l_timestampfld,
      PROGRAM_NAME=>'SOFVBGW9UPDTSP',force_log_entry=>true);
  end if;
EXCEPTION
WHEN OTHERS THEN
  ROLLBACK TO SOFVBGW9UPDTSP;
  p_errmsg:=' Error outer exception handler caught Oracle error:'||SQLERRM(SQLCODE)
    ||' in SOFVBGW9UPDTSP build 2018-11-07 11:35:28';
  runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
    3,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld,
    PROGRAM_NAME=>'SOFVBGW9UPDTSP',force_log_entry=>true);
--
END SOFVBGW9UPDTSP;
/


GRANT EXECUTE ON STGCSA.SOFVBGW9UPDTSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.SOFVBGW9UPDTSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.SOFVBGW9UPDTSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.SOFVBGW9UPDTSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.SOFVBGW9UPDTSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.SOFVBGW9UPDTSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.SOFVBGW9UPDTSP TO STGCSADEVROLE;

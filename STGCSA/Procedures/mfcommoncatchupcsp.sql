DROP PROCEDURE STGCSA.MFCOMMONCATCHUPCSP;

CREATE OR REPLACE PROCEDURE STGCSA.MFCOMMONCATCHUPCSP (
    p_runmode CHAR := 'C', /* C: current month, N: next month */
    p_purpose VARCHAR2 := 'MFUPDATE' /* MFUPDATE or MFLIST */
    )
as
    ---p1 NUMBER; p2 NUMBER; p3 VARCHAR2(200);
    v_sysdate date := sysdate;
	v_nulldate date := TO_DATE('1900-01-01', 'YYYY-MM-DD');
    v_status char(1);
    v_lastupdtdt date;
    v_taskisopen char(1) := 'N';
	v_indexdate date := TRUNC(sysdate, 'mm');
BEGIN
	v_lastupdtdt := v_indexdate; -- always use the beginning of this month

	IF p_runmode = 'N' THEN
		v_indexdate := trunc(Add_Months(sysdate, 1), 'mm');
	END IF;

    /* Note: UpdateInd is used to determine if update BGDF or CTCH is needed
       If pymtAmt is NULL, delete record in DFPY
       If NewLND1Stat is not NULL, SOFVLND1TBL will be updated for the status
       If NewPlanStatID is not NULL, CoreTransTBL will be updated, CoreTransStat will be inserted 
    */   

	/* first list those to be pending removal, regardless the period status, nore the start/end date */
    INSERT INTO TempCUDTbl (PlanTransID, LoanNmb, PlanStatus, DeleteACH, PlanLnSts, PlanStatID, PeriodTransID, PeriodStatus, CUDPDTYPID, TypeNm,  
    StartDt, EndDt, PymtAmt, PeriodUpdtDt, PeriodStatusID, PeriodLoanNmb, PlanUpdtDt, NewPlanStatID, NewPeriodStatID, NewLND1Stat, UpdateInd)
    SELECT PlanTransID, LoanNmb, PlanStatus, DeleteACH, PlanLnSts, PlanStatID, PeriodTransID, PeriodStatus, CUDPDTYPID, TypeNm,  
    StartDt, EndDt, null, PeriodUpdtDt, PeriodStatusID, PeriodLoanNmb, PlanUpdtDt, 4, 4, '01', 'N' 
    FROM DEFCTCHVW VW
    WHERE PlanStatID IN (43, 20)
    AND PlanUpdtDt > v_lastupdtdt; 

	/* check if the current task is for Current Month or Next Month */
	IF p_runmode = 'C' THEN

        -- Case #1: Plan Status Reviewed (2), DF PRD New (1), CU Prd New (1) ==> New Plan Stat = 2 (no change), LND1 Stat = 60, Prd Stat = 44
		INSERT INTO TempCUDTbl (PlanTransID, LoanNmb, PlanStatus, DeleteACH, PlanLnSts, PlanStatID, PeriodTransID, PeriodStatus, CUDPDTYPID, TypeNm,  
		StartDt, EndDt, PymtAmt, PeriodUpdtDt, PeriodStatusID, PeriodLoanNmb, PlanUpdtDt, NewPlanStatID, NewPeriodStatID, NewLND1Stat, UpdateInd)
		SELECT PlanTransID, LoanNmb, PlanStatus, DeleteACH, PlanLnSts, PlanStatID, PeriodTransID, PeriodStatus, CUDPDTYPID, TypeNm,  
		StartDt, EndDt, PymtAmt, PeriodUpdtDt, PeriodStatusID, PeriodLoanNmb, PlanUpdtDt, 2, 44, '60', 'Y'
        FROM DEFCTCHVW VW
		WHERE PlanStatID = 2
		AND v_indexdate BETWEEN TRUNC(StartDt, 'mm') AND TRUNC(EndDt, 'mm')
		AND PlanUpdtDt > v_lastupdtdt
        AND CUDPDTYPID IN (1, 2) /* this case is only for DF */
		AND PeriodStatusID = 1; 

        -- Case #2: Plan Status Approved (35), DF New (1), CU New (1) ==> will update BGDF, CTCH, DFPY, Set LND1 = 60, DF Prd = 44, CU Prd = 44
		INSERT INTO TempCUDTbl (PlanTransID, LoanNmb, PlanStatus, DeleteACH, PlanLnSts, PlanStatID, PeriodTransID, PeriodStatus, CUDPDTYPID, TypeNm,  
		StartDt, EndDt, PymtAmt, PeriodUpdtDt, PeriodStatusID, PeriodLoanNmb, PlanUpdtDt, NewPlanStatID, NewPeriodStatID, NewLND1Stat, UpdateInd)
		SELECT PlanTransID, LoanNmb, PlanStatus, DeleteACH, PlanLnSts, PlanStatID, PeriodTransID, PeriodStatus, CUDPDTYPID, TypeNm,  
		StartDt, EndDt, PymtAmt, PeriodUpdtDt, PeriodStatusID, PeriodLoanNmb, PlanUpdtDt, 44, 44, '60', 'Y' 
        FROM DEFCTCHVW VW
		WHERE PlanStatID = 35
		AND v_indexdate BETWEEN TRUNC(StartDt, 'mm') AND TRUNC(EndDt, 'mm')
		AND PlanUpdtDt > v_lastupdtdt
        AND CUDPDTYPID IN (1, 2) /* this case is only for DF */
		AND PeriodStatusID = 1; 

		/* add all later periods for APPROVED plans only. Note both NewPlanStatID and NewLND1Stat are set to NULL, so these will not update the base table */
		INSERT INTO TempCUDTbl (PlanTransID, LoanNmb, PlanStatus, DeleteACH, PlanLnSts, PlanStatID, PeriodTransID, PeriodStatus, CUDPDTYPID, TypeNm,  
		StartDt, EndDt, PymtAmt, PeriodUpdtDt, PeriodStatusID, PeriodLoanNmb, PlanUpdtDt, NewPlanStatID, NewPeriodStatID, NewLND1Stat, UpdateInd)
		SELECT PlanTransID, LoanNmb, PlanStatus, DeleteACH, PlanLnSts, PlanStatID, PeriodTransID, PeriodStatus, CUDPDTYPID, TypeNm,  
		StartDt, EndDt, PymtAmt, PeriodUpdtDt, PeriodStatusID, PeriodLoanNmb, PlanUpdtDt, NULL, 44, NULL, 'Y' 
        FROM DEFCTCHVW VW
		WHERE EXISTS (SELECT 1 FROM TempCUDTbl tmp WHERE tmp.PlanTransID = VW.PlanTransID AND tmp.PlanStatID = 35 AND tmp.PeriodStatusID = 1 AND VW.StartDT > tmp.EndDt)
        AND PlanStatID = 35
		AND PeriodStatusID = 1; 

        -- Case #3: Plan was Not Followed (46) but changed to In MF (44) for the CU period ==> LND1 = '06', CU Prd = 44, CU, DF Not uupdated, DF Prd 44 
		INSERT INTO TempCUDTbl (PlanTransID, LoanNmb, PlanStatus, DeleteACH, PlanLnSts, PlanStatID, PeriodTransID, PeriodStatus, CUDPDTYPID, TypeNm,  
		StartDt, EndDt, PymtAmt, PeriodUpdtDt, PeriodStatusID, PeriodLoanNmb, PlanUpdtDt, NewPlanStatID, NewPeriodStatID, NewLND1Stat, UpdateInd)
		SELECT PlanTransID, VW.LoanNmb, PlanStatus, DeleteACH, PlanLnSts, PlanStatID, PeriodTransID, PeriodStatus, CUDPDTYPID, TypeNm,  
		StartDt, EndDt, PymtAmt, PeriodUpdtDt, PeriodStatusID, PeriodLoanNmb, PlanUpdtDt, 44, 44, '06', 'N'
        FROM DEFCTCHVW VW JOIN CUDStatChgVW CHG ON VW.PlanTransID = CHG.TransID
        WHERE S2_Stat_ID = 46 AND PlanStatID = 44
		AND v_indexdate BETWEEN TRUNC(StartDt, 'mm') AND TRUNC(EndDt, 'mm')
		AND PlanUpdtDt > v_lastupdtdt
        AND CUDPDTYPID = 3
        AND PeriodStatusID NOT IN (4, 20); 

        -- get the previous DF before this CU to also update to 44. BGDF not updated
		INSERT INTO TempCUDTbl (PlanTransID, LoanNmb, PlanStatus, DeleteACH, PlanLnSts, PlanStatID, PeriodTransID, PeriodStatus, CUDPDTYPID, TypeNm,  
		StartDt, EndDt, PymtAmt, PeriodUpdtDt, PeriodStatusID, PeriodLoanNmb, PlanUpdtDt, NewPlanStatID, NewPeriodStatID, NewLND1Stat, UpdateInd)
		SELECT PlanTransID, VW.LoanNmb, PlanStatus, DeleteACH, PlanLnSts, PlanStatID, PeriodTransID, PeriodStatus, CUDPDTYPID, TypeNm,  
		StartDt, EndDt, PymtAmt, PeriodUpdtDt, PeriodStatusID, PeriodLoanNmb, PlanUpdtDt, NULL, 44, NULL, 'N' 
        FROM DEFCTCHVW VW JOIN CUDStatChgVW CHG ON VW.PlanTransID = CHG.TransID
        WHERE S2_Stat_ID = 46 AND PlanStatID = 44
        AND v_indexdate > TRUNC(EndDt, 'mm')
		AND EXISTS (SELECT 1 FROM TempCUDTbl tmp WHERE tmp.PlanTransID = VW.PlanTransID AND tmp.PlanStatID = VW.PlanStatID 
            AND tmp.PeriodStatusID = VW.PeriodStatusID AND VW.EndDT > tmp.StartDt)
        AND PeriodStatusID NOT IN (4, 20);  -- make sure we do not change what already is completed/cancelled

        -- Case #4: Plan was Reviewed (2) but changed to Approved (35) for the CU period ==> LND1 = '06', CU Prd = 44, CU updated, DF Not uupdated, DF Prd 44
		INSERT INTO TempCUDTbl (PlanTransID, LoanNmb, PlanStatus, DeleteACH, PlanLnSts, PlanStatID, PeriodTransID, PeriodStatus, CUDPDTYPID, TypeNm,  
		StartDt, EndDt, PymtAmt, PeriodUpdtDt, PeriodStatusID, PeriodLoanNmb, PlanUpdtDt, NewPlanStatID, NewPeriodStatID, NewLND1Stat, UpdateInd)
		SELECT PlanTransID, VW.LoanNmb, PlanStatus, DeleteACH, PlanLnSts, PlanStatID, PeriodTransID, PeriodStatus, CUDPDTYPID, TypeNm,  
		StartDt, EndDt, PymtAmt, PeriodUpdtDt, PeriodStatusID, PeriodLoanNmb, PlanUpdtDt, 44, 44, '06', 'Y'
        FROM DEFCTCHVW VW JOIN CUDStatChgVW CHG ON VW.PlanTransID = CHG.TransID
        WHERE S2_Stat_ID = 2 AND PlanStatID = 35
		AND v_indexdate BETWEEN TRUNC(StartDt, 'mm') AND TRUNC(EndDt, 'mm')
		AND PlanUpdtDt > v_lastupdtdt
        AND CUDPDTYPID = 3
        AND PeriodStatusID != 20;  -- 4 would be okay, because previous month's Next Month macro will change the periodStatus = 44


	ELSE /* p_runmode = 'N' for next month */

        /* N-01: pick up plans that will start next month for Reviewed (2), Approved (35), IN MF (44) Plans. Ignore all others such as New (1), Pending Removal (43), Cancelled (20), etc */
		INSERT INTO TempCUDTbl (PlanTransID, LoanNmb, PlanStatus, DeleteACH, PlanLnSts, PlanStatID, PeriodTransID, PeriodStatus, CUDPDTYPID, TypeNm,  
		StartDt, EndDt, PymtAmt, PeriodUpdtDt, PeriodStatusID, PeriodLoanNmb, PlanUpdtDt, NewPlanStatID, NewPeriodStatID, NewLND1Stat, UpdateInd)
		SELECT PlanTransID, LoanNmb, PlanStatus, DeleteACH, PlanLnSts, PlanStatID, PeriodTransID, PeriodStatus, CUDPDTYPID, TypeNm,  
		StartDt, EndDt, PymtAmt, PeriodUpdtDt, PeriodStatusID, PeriodLoanNmb, PlanUpdtDt, 
        DECODE(PlanStatID, 2, 2, 44),         -- NewPlanStatID, IN MF
        44,                                   -- NewPeriodStatID, IN MF
        DECODE(CUDPDTYPID, 3, '06', '60'),    -- NewLND1Stat
        'Y'                                   -- UpdateInd
        FROM DEFCTCHVW VW
		WHERE PlanStatID IN (2, 35, 44)       -- Reviewed/Approved/In MF
		AND v_indexdate = TRUNC(StartDt, 'mm')
		AND PeriodStatusID NOT IN (4, 44);  /* not already IN MF, or Complete */

        /* N-02: If plan status is Reviewed (2) and period is new (1), clear DFPY, mark LND1 to '01', will not update, if the current period is Catch Up */
		UPDATE TempCUDTbl SET PymtAmt = null, NewLND1Stat = '01', NewPeriodStatID = 1, UpdateInd = 'N'
        WHERE PlanStatID = 2 AND CUDPDTYPID = 3 AND PeriodStatusID = 1;

		/* N-03: add all newer periods for APPROVED plans */
		INSERT INTO TempCUDTbl (PlanTransID, LoanNmb, PlanStatus, DeleteACH, PlanLnSts, PlanStatID, PeriodTransID, PeriodStatus, CUDPDTYPID, TypeNm,  
		StartDt, EndDt, PymtAmt, PeriodUpdtDt, PeriodStatusID, PeriodLoanNmb, PlanUpdtDt, NewPlanStatID, NewPeriodStatID, NewLND1Stat, UpdateInd)
		SELECT PlanTransID, LoanNmb, PlanStatus, DeleteACH, PlanLnSts, PlanStatID, PeriodTransID, PeriodStatus, CUDPDTYPID, TypeNm,  
		StartDt, EndDt, PymtAmt, PeriodUpdtDt, PeriodStatusID, PeriodLoanNmb, PlanUpdtDt, NULL, 44, NULL, 'Y' 
        FROM DEFCTCHVW VW
		WHERE EXISTS (SELECT 1 FROM TempCUDTbl tmp WHERE tmp.PlanTransID = VW.PlanTransID AND tmp.PlanStatID = 35 AND VW.StartDT > tmp.EndDt)
        AND VW.PlanStatID = 35
		AND VW.PeriodStatusID NOT IN (4, 44); 

		/* N-04: Now add previously approved CU periods, which only needs to change to '06', and will not update the BGDF and CTCH table by setting UpdateInd = 'N' */
		INSERT INTO TempCUDTbl (PlanTransID, LoanNmb, PlanStatus, DeleteACH, PlanLnSts, PlanStatID, PeriodTransID, PeriodStatus, CUDPDTYPID, TypeNm,  
		StartDt, EndDt, PymtAmt, PeriodUpdtDt, PeriodStatusID, PeriodLoanNmb, PlanUpdtDt, NewPlanStatID, NewPeriodStatID, NewLND1Stat, UpdateInd)
		SELECT PlanTransID, LoanNmb, PlanStatus, DeleteACH, PlanLnSts, PlanStatID, PeriodTransID, PeriodStatus, CUDPDTYPID, TypeNm,  
		StartDt, EndDt, PymtAmt, PeriodUpdtDt, PeriodStatusID, PeriodLoanNmb, PlanUpdtDt, 44, 44, '06', 'N' 
		FROM DEFCTCHVW VW
		WHERE TRUNC(StartDt, 'mm') = v_indexdate
        AND CUDPDTYPID = 3
		AND PeriodStatusID = 44 AND PlanStatID = 44; 

        /* N-05: Not Followed (with any status except Complete/Cancelled), clean out the DFPY, mark LND1 to '01'. This is not on the Excel but as a defect list */
		INSERT INTO TempCUDTbl (PlanTransID, LoanNmb, PlanStatus, DeleteACH, PlanLnSts, PlanStatID, PeriodTransID, PeriodStatus, CUDPDTYPID, TypeNm,  
		StartDt, EndDt, PymtAmt, PeriodUpdtDt, PeriodStatusID, PeriodLoanNmb, PlanUpdtDt, NewPlanStatID, NewPeriodStatID, NewLND1Stat, UpdateInd)
		SELECT PlanTransID, LoanNmb, PlanStatus, DeleteACH, PlanLnSts, PlanStatID, PeriodTransID, PeriodStatus, CUDPDTYPID, TypeNm,  
		StartDt, EndDt, NULL, PeriodUpdtDt, PeriodStatusID, PeriodLoanNmb, PlanUpdtDt, 46, 44, '01', 'N' 
		FROM DEFCTCHVW VW
		WHERE TRUNC(StartDt, 'mm') = v_indexdate
		AND PlanStatID = 46
        AND PeriodStatusID NOT IN (4, 20); 

        /* N-06: Plan Not Followed (46) but changed to In MF (44) for CU period. LND1 => '06', Plan => in MF (44), PRD (44). UpdateInd (N), update DFPY */
		INSERT INTO TempCUDTbl (PlanTransID, LoanNmb, PlanStatus, DeleteACH, PlanLnSts, PlanStatID, PeriodTransID, PeriodStatus, CUDPDTYPID, TypeNm,  
		StartDt, EndDt, PymtAmt, PeriodUpdtDt, PeriodStatusID, PeriodLoanNmb, PlanUpdtDt, NewPlanStatID, NewPeriodStatID, NewLND1Stat, UpdateInd)
		SELECT PlanTransID, VW.LoanNmb, PlanStatus, DeleteACH, PlanLnSts, PlanStatID, PeriodTransID, PeriodStatus, CUDPDTYPID, TypeNm,  
		StartDt, EndDt, PymtAmt, PeriodUpdtDt, PeriodStatusID, PeriodLoanNmb, PlanUpdtDt, 44, 44, '06', 'N'
        FROM DEFCTCHVW VW JOIN CUDStatChgVW CHG ON VW.PlanTransID = CHG.TransID
        WHERE S2_Stat_ID = 46 AND PlanStatID = 44
		AND TRUNC(StartDt, 'mm') = v_indexdate
		AND PlanUpdtDt > v_lastupdtdt
        AND CUDPDTYPID = 3
        AND PeriodStatusID NOT IN (4, 20); 

        /* N-07: get the previous DF before this CU to also update to 44. BGDF not updated */
		INSERT INTO TempCUDTbl (PlanTransID, LoanNmb, PlanStatus, DeleteACH, PlanLnSts, PlanStatID, PeriodTransID, PeriodStatus, CUDPDTYPID, TypeNm,  
		StartDt, EndDt, PymtAmt, PeriodUpdtDt, PeriodStatusID, PeriodLoanNmb, PlanUpdtDt, NewPlanStatID, NewPeriodStatID, NewLND1Stat, UpdateInd)
		SELECT PlanTransID, VW.LoanNmb, PlanStatus, DeleteACH, PlanLnSts, PlanStatID, PeriodTransID, PeriodStatus, CUDPDTYPID, TypeNm,  
		StartDt, EndDt, PymtAmt, PeriodUpdtDt, PeriodStatusID, PeriodLoanNmb, PlanUpdtDt, NULL, 44, NULL, 'N' 
        FROM DEFCTCHVW VW JOIN CUDStatChgVW CHG ON VW.PlanTransID = CHG.TransID
        WHERE S2_Stat_ID = 46 AND PlanStatID = 44
        AND v_indexdate > TRUNC(EndDt, 'mm')
		AND EXISTS (SELECT 1 FROM TempCUDTbl tmp WHERE tmp.PlanTransID = VW.PlanTransID AND tmp.PlanStatID = VW.PlanStatID 
            AND tmp.PeriodStatusID = VW.PeriodStatusID AND VW.EndDT > tmp.StartDt)
        AND PeriodStatusID NOT IN (4, 20);  -- make sure we do not change what already is completed/cancelled

		/* N-08: Add the periods which will be ending This Month for Reviewed and In MF, and Not Followed. 
           These need to be set to complete (4) for the period. 
           Note the UpdateInd is set to 'N' and no update to LND1, and no update to the Plan Status
           Also since these is before next month, the PymtAmt will not be updated in the DFPY table */
		INSERT INTO TempCUDTbl (PlanTransID, LoanNmb, PlanStatus, DeleteACH, PlanLnSts, PlanStatID, PeriodTransID, PeriodStatus, CUDPDTYPID, TypeNm,  
		StartDt, EndDt, PymtAmt, PeriodUpdtDt, PeriodStatusID, PeriodLoanNmb, PlanUpdtDt, NewPlanStatID, NewPeriodStatID, NewLND1Stat, UpdateInd)
		SELECT PlanTransID, LoanNmb, PlanStatus, DeleteACH, PlanLnSts, PlanStatID, PeriodTransID, PeriodStatus, CUDPDTYPID, TypeNm,  
		StartDt, EndDt, PymtAmt, PeriodUpdtDt, PeriodStatusID, PeriodLoanNmb, PlanUpdtDt, NULL, 4, NULL, 'N' 
		FROM DEFCTCHVW VW
		WHERE TRUNC(EndDt, 'mm') = TRUNC(sysdate, 'mm') 
		AND ((PlanStatID IN (2, 44) AND PeriodStatusID = 44) OR PlanStatID = 46);  /* Reviewed/In MF has period of In MF, and anything for Not Followed */

		/* N-09: Close out if there is no more period behind it. Those are applied to In MF (44), or Not Followed. Will not conflict with N-05 because date periods different */
		UPDATE TempCUDTbl t1 SET PymtAmt = null, NewLND1Stat = '01', NewPlanStatID = 4, NewPeriodStatID = 4 
        WHERE (PeriodStatusID = 44 OR PlanStatID = 46)
            AND TRUNC(sysdate, 'mm') = TRUNC(EndDt, 'mm')
			AND NOT EXISTS (SELECT 1 FROM TempCUDTbl t2 WHERE TRUNC(t2.StartDt, 'mm') = v_indexdate AND t2.PlanTransID = t1.PlanTransID);

	END IF;

END MFCOMMONCATCHUPCSP;
/


GRANT EXECUTE ON STGCSA.MFCOMMONCATCHUPCSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.MFCOMMONCATCHUPCSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.MFCOMMONCATCHUPCSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.MFCOMMONCATCHUPCSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.MFCOMMONCATCHUPCSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.MFCOMMONCATCHUPCSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.MFCOMMONCATCHUPCSP TO STGCSADEVROLE;

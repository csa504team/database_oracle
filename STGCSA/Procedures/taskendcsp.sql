DROP PROCEDURE STGCSA.TASKENDCSP;

CREATE OR REPLACE PROCEDURE STGCSA.TaskEndCSP (
    p_taskname VARCHAR2 :='',
    p_retval out number,
    p_errval out number,
    p_errmsg out varchar2)
as
    v_count number;
    v_cur_jobid number;
    v_cur_jobTypeID number;
    v_cur_jobCategory number;
    v_task REFJOBTYPETBL%ROWTYPE;
    v_job MFJOBTBL%ROWTYPE;
    v_jobdetail MFJOBDETAILTBL%ROWTYPE;
    v_seqnmb number := 1;
    v_sysdate date := sysdate;
    v_taskname varchar2(100) := '[' || UPPER(NVL(p_taskname,'')) || ']';
BEGIN 

    p_errval := 0;
    p_retval := -1000;
    p_errmsg := 'Success';

    IF NVL(Length(Trim(p_taskname)),0) = 0 THEN
        p_errmsg := 'Failed: Task name cannot be blank';
        p_errval := -20000;
        --raise_application_error(p_errval, p_errmsg);
        RETURN;
    END IF;

    p_retval := -1;

    SELECT COUNT(*) INTO v_count FROM stgcsa.REFJOBTYPETBL WHERE UPPER(JOBTYPENM) = UPPER(p_taskname);

    p_retval := -2;

    /* the task is not defined. Return to caller */
    IF v_count = 0 THEN
        p_errmsg := 'Failed: Unexpected task value: [' || v_taskname || ']';
        p_errval := -20001;
        --raise_application_error(p_errval, p_errmsg);
        RETURN;
    END IF;    

    p_retval := -3;

    SELECT * INTO v_task FROM stgcsa.REFJOBTYPETBL WHERE UPPER(JOBTYPENM) = UPPER(p_taskname);

    p_retval := -4;

    SELECT NVL(MAX(jobid),0) INTO v_cur_jobid FROM stgcsa.MFJOBTBL;

    p_retval := -5;

    IF v_cur_jobid = 0 THEN
        p_errmsg := 'Failed: There is no existing job at all';
        p_errval := -20002;
        --raise_application_error(p_errval, p_errmsg);
        RETURN;
    END IF;   

    p_retval := -6;

    SELECT * INTO v_job FROM stgcsa.MFJOBTBL WHERE jobid = v_cur_jobid;

    p_retval := -7;

    IF v_job.CURRENTJOBTYPEID != v_task.JOBTYPEID THEN
        p_errmsg := 'Failed: Task ' || v_taskname || ' is not the current open task';
        p_errval := -20003;
        --raise_application_error(p_errval, p_errmsg);
        RETURN;
    ELSIF v_job.CompleteDT IS NOT NULL THEN        
        p_errmsg := 'Failed: No current open job';
        p_errval := -20004;
        --raise_application_error(p_errval, p_errmsg);
        RETURN;
    END IF;    

    p_retval := -8;

    SELECT COUNT(*) INTO v_count FROM stgcsa.MFJOBDETAILTBL 
    WHERE jobfk = v_cur_jobid AND JOBTYPEFK = v_task.JOBTYPEID AND TASKENDDT IS NULL;

    p_retval := -9;

    IF v_count = 0 THEN
        p_errmsg := 'Failed: Task ' || v_taskname || ' is not currently open';
        p_errval := -20005;
        --raise_application_error(p_errval, p_errmsg);
        RETURN;
    ELSIF v_count > 1 THEN
        p_errmsg := 'Failed: Task ' || v_taskname || ' has more than one open instance. Contact Administrator';
        p_errval := -20006;
        --raise_application_error(p_errval, p_errmsg);
        RETURN;
    ELSE

        p_retval := -10;
        SELECT * INTO v_jobdetail FROM stgcsa.MFJOBDETAILTBL WHERE jobfk = v_cur_jobid AND JOBTYPEFK = v_task.JOBTYPEID AND TASKENDDT IS NULL;

        p_retval := -11;

    END IF;

    UPDATE stgcsa.MFJOBDETAILTBL SET TASKENDDT = v_sysdate WHERE jobdetailid = v_jobdetail.jobdetailid;

    p_errmsg := 'Success in closing task ' || v_taskname || '. Job ID: ' || v_cur_jobid || ' / Sequence : ' ||  v_jobdetail.seqnmb; 

    IF v_task.JOBTYPEID = 400 THEN
        UPDATE stgcsa.MFJOBTBL SET CompleteDT = v_sysdate WHERE jobid = v_job.jobid;
        p_errmsg := p_errmsg || '. Also closed the job.';
    END IF;

    p_retval := 0;

    RETURN;

EXCEPTION
WHEN OTHERS THEN
  --p_retval:=p_errval;
  p_errval:=SQLCODE;
  p_errmsg:=SQLERRM(SQLCODE);  
  --RAISE;
END TaskEndCSP;
/


GRANT EXECUTE ON STGCSA.TASKENDCSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.TASKENDCSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.TASKENDCSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.TASKENDCSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.TASKENDCSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.TASKENDCSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.TASKENDCSP TO STGCSADEVROLE;

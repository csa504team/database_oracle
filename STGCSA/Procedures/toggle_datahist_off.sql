DROP PROCEDURE STGCSA.TOGGLE_DATAHIST_OFF;

CREATE OR REPLACE PROCEDURE STGCSA.toggle_datahist_off as
begin
  runtime.toggle_datahist_logging_on_yn('N');
end;
/

--
CREATE OR REPLACE PROCEDURE STGCSA.xover_reset_request_csp
   (p_retval out number,
    p_errval out number,
    p_errmsg out varchar2,
    p_identifier number:=0,
    p_userid varchar2,
      p_what_to_do varchar2:='curr_startdate'      
    ) as
  prog varchar2(40):='XOVER_RESET_REQUEST_CSP';
  ver  varchar2(40):='V1.8 6 May 2019';
  -- V1.5 JL 11 Feb - first version
  -- V1.6 JG 18 Mar - Add P_WHAT_TO_DO parm with three options
  -- V1.8 JL - 6 May 2019 - set end dt in unclosed job records
  --    instead of deleting from these tables.
  reset_date date:=to_date('19000101','yyyymmdd');
  logentryid number;
  statusrec stgcsa.xover_status%rowtype;
begin
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  select * into statusrec from stgcsa.xover_status;
  p_errmsg:='Reset request started, P_WHAT_TO_DO='||P_WHAT_TO_DO;
  runtime.logger(logentryid,'STDXVR',235,p_errmsg,user,4,
    program_name=>prog||' '||ver,force_log_entry=>true);
    if lower(p_what_to_do)='full_reset' then
        -- STGCSA merge end date is set to reset_date i.e. 01/01/1900
        update stgcsa.xover_status set xover_last_event='SM',
      stgcsa_merge_end_dt=reset_date,
      err_step=null,err_dt=null,err_msg=null;
        p_errmsg:='XOVER RESET REQUESTED, STGCSA_MERGE_END_DT set to "1900".';
    elsif lower(p_what_to_do)='curr_startdate' then
        -- STGCSA merge end date is not updated. This is the default.
        update stgcsa.xover_status set xover_last_event='SM',
          err_step=null,err_dt=null,err_msg=null;
        p_errmsg:='XOVER RESET REQUESTED, DB_OPEN date remains '
          ||to_char(statusrec.stgcsa_merge_end_dt,'yyyy-mm-dd hh24:mi:ss');
    elsif lower(p_what_to_do)='as_of_now' then
        --when p_what_to_do is set to 'as_of_now', data is 
        update stgcsa.xover_status set xover_last_event='SM',
          stgcsa_merge_end_dt=sysdate,err_step=null,err_dt=null,err_msg=null;
        p_errmsg:='XOVER RESET REQUESTED, DB_OPEN_DATE set to sysdate: '
          ||to_char(sysdate,'yyyy-mm-dd hh24:mi:ss');
  else
    -- invalid option
    p_errval:=-1;
    p_errmsg:='Error: parm: '||P_WHAT_TO_DO
      ||' is invalid, nothing done.  Options are FULL_RESET, CURR_STARTDATE '
      ||'and AS_OF_NOW';
    end if;
    if p_errval=0 then
    --delete from STGCSA.JOBQUETBL;
    update STGCSA.JOBQUETBL
      set enddt=sysdate, statcd='F',
        commtxt=commtxt||'; Forced closed by RST'
      where enddt is null;
    --delete from STGCSA.MFJOBTBL;
    update STGCSA.MFJOBTBL set completedt=sysdate
      where completedt is null and currentjobtypeid in (200,300,400);
    --delete from STGCSA.MFJOBDETAILTBL;
    update STGCSA.MFJOBDETAILTBL set taskenddt=sysdate
      where taskenddt is null and jobtypefk in (200,300,400);
    commit;
    p_errmsg:=p_errmsg||'  Control tables reset, Normal Completion';
  end if;  
  runtime.logger(logentryid,'STDXVR',236,
    p_errmsg,p_userid,5,program_name=>prog||' '||ver);
end;
/




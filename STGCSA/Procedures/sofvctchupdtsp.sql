DROP PROCEDURE STGCSA.SOFVCTCHUPDTSP;

CREATE OR REPLACE PROCEDURE STGCSA.SOFVCTCHUPDTSP(
    p_retval out number,
    p_errval out number,
    p_errmsg  out varchar2,
    p_identifier number:=0,
    p_userid varchar2
   ,p_CATCHUPPLANSBANMB CHAR:=null
   ,p_CATCHUPPLANSTRTDT1 DATE:=null
   ,p_CATCHUPPLANENDDT1 DATE:=null
   ,p_CATCHUPPLANRQ1AMT VARCHAR2:=null
   ,p_CATCHUPPLANSTRTDT2 DATE:=null
   ,p_CATCHUPPLANENDDT2 DATE:=null
   ,p_CATCHUPPLANRQ2AMT VARCHAR2:=null
   ,p_CATCHUPPLANSTRTDT3 DATE:=null
   ,p_CATCHUPPLANENDDT3 DATE:=null
   ,p_CATCHUPPLANRQ3AMT VARCHAR2:=null
   ,p_CATCHUPPLANSTRTDT4 DATE:=null
   ,p_CATCHUPPLANENDDT4 DATE:=null
   ,p_CATCHUPPLANRQ4AMT VARCHAR2:=null
   ,p_CATCHUPPLANSTRTDT5 DATE:=null
   ,p_CATCHUPPLANENDDT5 DATE:=null
   ,p_CATCHUPPLANRQ5AMT VARCHAR2:=null
   ,p_CATCHUPPLANSTRTDT6 DATE:=null
   ,p_CATCHUPPLANENDDT6 DATE:=null
   ,p_CATCHUPPLANRQ6AMT VARCHAR2:=null
   ,p_CATCHUPPLANSTRTDT7 DATE:=null
   ,p_CATCHUPPLANENDDT7 DATE:=null
   ,p_CATCHUPPLANRQ7AMT VARCHAR2:=null
   ,p_CATCHUPPLANSTRTDT8 DATE:=null
   ,p_CATCHUPPLANENDDT8 DATE:=null
   ,p_CATCHUPPLANRQ8AMT VARCHAR2:=null
   ,p_CATCHUPPLANSTRTDT9 DATE:=null
   ,p_CATCHUPPLANENDDT9 DATE:=null
   ,p_CATCHUPPLANRQ9AMT VARCHAR2:=null
   ,p_CATCHUPPLANSTRTDT10 DATE:=null
   ,p_CATCHUPPLANENDDT10 DATE:=null
   ,p_CATCHUPPLANRQ10AMT VARCHAR2:=null
   ,p_CATCHUPPLANSTRTDT11 DATE:=null
   ,p_CATCHUPPLANENDDT11 DATE:=null
   ,p_CATCHUPPLANRQ11AMT VARCHAR2:=null
   ,p_CATCHUPPLANSTRTDT12 DATE:=null
   ,p_CATCHUPPLANENDDT12 DATE:=null
   ,p_CATCHUPPLANRQ12AMT VARCHAR2:=null
   ,p_CATCHUPPLANSTRTDT13 DATE:=null
   ,p_CATCHUPPLANENDDT13 DATE:=null
   ,p_CATCHUPPLANRQ13AMT VARCHAR2:=null
   ,p_CATCHUPPLANSTRTDT14 DATE:=null
   ,p_CATCHUPPLANENDDT14 DATE:=null
   ,p_CATCHUPPLANRQ14AMT VARCHAR2:=null
   ,p_CATCHUPPLANSTRTDT15 DATE:=null
   ,p_CATCHUPPLANENDDT15 DATE:=null
   ,p_CATCHUPPLANRQ15AMT VARCHAR2:=null
   ,p_CATCHUPPLANCREDT DATE:=null
   ,p_CATCHUPPLANMODDT DATE:=null
) as
 /*
  Created on: 2018-11-07 11:35:37
  Created by: GENR
  Crerated from template stdtblupd_template v3.22 12 Oct 2018 on 2018-11-07 11:35:37
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  Procedure SOFVCTCHUPDTSP performs UPDATE on STGCSA.SOFVCTCHTBL
    for P_IDENTIFIER=0 qualification is on Primary Key: CATCHUPPLANSBANMB
    Updates all columns except the qualification keys and columns for which a
      null value is passed.
    SPECIAL secret update kludge!
      if p_identifier is set to -1 then a flag is set to use all input parm values
      and p_identifier is changed to 0 (update by PK).
      THEREFORE if using p_identifier=-1 you MUST pass intended value for all columns.
      This option allows setting colulmns to null!
*/
  dbrec STGCSA.SOFVCTCHTBL%rowtype;
  logged_msg_id number;
  recnum number;
  rec_rowid rowid;
  keystouse varchar2(256);
  maxsev number:=0;
  pctsign char(1):='%';
  cur_colname varchar2(100):=null;
  cur_colvalue varchar2(4000);
  do_normal_field_checking char(1):='Y';
  overridden_p_identifier number;
  -- standard activity log capture vars
  l_LOANNMB char(10):=null;
  l_TRANSID number:=null;
  l_dbmdl varchar2(10):=null;
  l_entrydt date:=null;
  l_userid  varchar2(32):=null;
  l_usernm  varchar2(199):=null;
  l_timestampfld date:=null;
  crossover_not_active char(1);
procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
begin
  if p_errval=0 then
    begin
      select * into dbrec from STGCSA.SOFVCTCHTBL where rowid=rowid_in;
    exception when others then
      p_errval:=sqlcode;
      maxsev:=3;
      p_retval:=0;
      p_errmsg:='Fetch of STGCSA.SOFVCTCHTBL row by rowid '||rowid_in
        ||' orinally fetched using '||orig_keyset_values
        ||' failed with '||sqlerrm(sqlcode);
    end;
  end if;
  if p_errval=0 then
    -- set this as lastupdt
    DBrec.lastupdtuserid:=p_userid;
    DBrec.lastupdtdt:=sysdate;
    -- copy into DBrec all table column input parameters that are not null,
    -- saving column name/value if needed for error reporting
    --  (if p_identifier=-1 then move input even if null)
    begin
      if P_CATCHUPPLANSBANMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANSBANMB';
        cur_colvalue:=P_CATCHUPPLANSBANMB;
        DBrec.CATCHUPPLANSBANMB:=P_CATCHUPPLANSBANMB;
      end if;
      if P_CATCHUPPLANSTRTDT1 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANSTRTDT1';
        cur_colvalue:=P_CATCHUPPLANSTRTDT1;
        DBrec.CATCHUPPLANSTRTDT1:=P_CATCHUPPLANSTRTDT1;
      end if;
      if P_CATCHUPPLANENDDT1 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANENDDT1';
        cur_colvalue:=P_CATCHUPPLANENDDT1;
        DBrec.CATCHUPPLANENDDT1:=P_CATCHUPPLANENDDT1;
      end if;
      if P_CATCHUPPLANRQ1AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANRQ1AMT';
        cur_colvalue:=P_CATCHUPPLANRQ1AMT;
        DBrec.CATCHUPPLANRQ1AMT:=P_CATCHUPPLANRQ1AMT;
      end if;
      if P_CATCHUPPLANSTRTDT2 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANSTRTDT2';
        cur_colvalue:=P_CATCHUPPLANSTRTDT2;
        DBrec.CATCHUPPLANSTRTDT2:=P_CATCHUPPLANSTRTDT2;
      end if;
      if P_CATCHUPPLANENDDT2 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANENDDT2';
        cur_colvalue:=P_CATCHUPPLANENDDT2;
        DBrec.CATCHUPPLANENDDT2:=P_CATCHUPPLANENDDT2;
      end if;
      if P_CATCHUPPLANRQ2AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANRQ2AMT';
        cur_colvalue:=P_CATCHUPPLANRQ2AMT;
        DBrec.CATCHUPPLANRQ2AMT:=P_CATCHUPPLANRQ2AMT;
      end if;
      if P_CATCHUPPLANSTRTDT3 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANSTRTDT3';
        cur_colvalue:=P_CATCHUPPLANSTRTDT3;
        DBrec.CATCHUPPLANSTRTDT3:=P_CATCHUPPLANSTRTDT3;
      end if;
      if P_CATCHUPPLANENDDT3 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANENDDT3';
        cur_colvalue:=P_CATCHUPPLANENDDT3;
        DBrec.CATCHUPPLANENDDT3:=P_CATCHUPPLANENDDT3;
      end if;
      if P_CATCHUPPLANRQ3AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANRQ3AMT';
        cur_colvalue:=P_CATCHUPPLANRQ3AMT;
        DBrec.CATCHUPPLANRQ3AMT:=P_CATCHUPPLANRQ3AMT;
      end if;
      if P_CATCHUPPLANSTRTDT4 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANSTRTDT4';
        cur_colvalue:=P_CATCHUPPLANSTRTDT4;
        DBrec.CATCHUPPLANSTRTDT4:=P_CATCHUPPLANSTRTDT4;
      end if;
      if P_CATCHUPPLANENDDT4 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANENDDT4';
        cur_colvalue:=P_CATCHUPPLANENDDT4;
        DBrec.CATCHUPPLANENDDT4:=P_CATCHUPPLANENDDT4;
      end if;
      if P_CATCHUPPLANRQ4AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANRQ4AMT';
        cur_colvalue:=P_CATCHUPPLANRQ4AMT;
        DBrec.CATCHUPPLANRQ4AMT:=P_CATCHUPPLANRQ4AMT;
      end if;
      if P_CATCHUPPLANSTRTDT5 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANSTRTDT5';
        cur_colvalue:=P_CATCHUPPLANSTRTDT5;
        DBrec.CATCHUPPLANSTRTDT5:=P_CATCHUPPLANSTRTDT5;
      end if;
      if P_CATCHUPPLANENDDT5 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANENDDT5';
        cur_colvalue:=P_CATCHUPPLANENDDT5;
        DBrec.CATCHUPPLANENDDT5:=P_CATCHUPPLANENDDT5;
      end if;
      if P_CATCHUPPLANRQ5AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANRQ5AMT';
        cur_colvalue:=P_CATCHUPPLANRQ5AMT;
        DBrec.CATCHUPPLANRQ5AMT:=P_CATCHUPPLANRQ5AMT;
      end if;
      if P_CATCHUPPLANSTRTDT6 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANSTRTDT6';
        cur_colvalue:=P_CATCHUPPLANSTRTDT6;
        DBrec.CATCHUPPLANSTRTDT6:=P_CATCHUPPLANSTRTDT6;
      end if;
      if P_CATCHUPPLANENDDT6 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANENDDT6';
        cur_colvalue:=P_CATCHUPPLANENDDT6;
        DBrec.CATCHUPPLANENDDT6:=P_CATCHUPPLANENDDT6;
      end if;
      if P_CATCHUPPLANRQ6AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANRQ6AMT';
        cur_colvalue:=P_CATCHUPPLANRQ6AMT;
        DBrec.CATCHUPPLANRQ6AMT:=P_CATCHUPPLANRQ6AMT;
      end if;
      if P_CATCHUPPLANSTRTDT7 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANSTRTDT7';
        cur_colvalue:=P_CATCHUPPLANSTRTDT7;
        DBrec.CATCHUPPLANSTRTDT7:=P_CATCHUPPLANSTRTDT7;
      end if;
      if P_CATCHUPPLANENDDT7 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANENDDT7';
        cur_colvalue:=P_CATCHUPPLANENDDT7;
        DBrec.CATCHUPPLANENDDT7:=P_CATCHUPPLANENDDT7;
      end if;
      if P_CATCHUPPLANRQ7AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANRQ7AMT';
        cur_colvalue:=P_CATCHUPPLANRQ7AMT;
        DBrec.CATCHUPPLANRQ7AMT:=P_CATCHUPPLANRQ7AMT;
      end if;
      if P_CATCHUPPLANSTRTDT8 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANSTRTDT8';
        cur_colvalue:=P_CATCHUPPLANSTRTDT8;
        DBrec.CATCHUPPLANSTRTDT8:=P_CATCHUPPLANSTRTDT8;
      end if;
      if P_CATCHUPPLANENDDT8 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANENDDT8';
        cur_colvalue:=P_CATCHUPPLANENDDT8;
        DBrec.CATCHUPPLANENDDT8:=P_CATCHUPPLANENDDT8;
      end if;
      if P_CATCHUPPLANRQ8AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANRQ8AMT';
        cur_colvalue:=P_CATCHUPPLANRQ8AMT;
        DBrec.CATCHUPPLANRQ8AMT:=P_CATCHUPPLANRQ8AMT;
      end if;
      if P_CATCHUPPLANSTRTDT9 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANSTRTDT9';
        cur_colvalue:=P_CATCHUPPLANSTRTDT9;
        DBrec.CATCHUPPLANSTRTDT9:=P_CATCHUPPLANSTRTDT9;
      end if;
      if P_CATCHUPPLANENDDT9 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANENDDT9';
        cur_colvalue:=P_CATCHUPPLANENDDT9;
        DBrec.CATCHUPPLANENDDT9:=P_CATCHUPPLANENDDT9;
      end if;
      if P_CATCHUPPLANRQ9AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANRQ9AMT';
        cur_colvalue:=P_CATCHUPPLANRQ9AMT;
        DBrec.CATCHUPPLANRQ9AMT:=P_CATCHUPPLANRQ9AMT;
      end if;
      if P_CATCHUPPLANSTRTDT10 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANSTRTDT10';
        cur_colvalue:=P_CATCHUPPLANSTRTDT10;
        DBrec.CATCHUPPLANSTRTDT10:=P_CATCHUPPLANSTRTDT10;
      end if;
      if P_CATCHUPPLANENDDT10 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANENDDT10';
        cur_colvalue:=P_CATCHUPPLANENDDT10;
        DBrec.CATCHUPPLANENDDT10:=P_CATCHUPPLANENDDT10;
      end if;
      if P_CATCHUPPLANRQ10AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANRQ10AMT';
        cur_colvalue:=P_CATCHUPPLANRQ10AMT;
        DBrec.CATCHUPPLANRQ10AMT:=P_CATCHUPPLANRQ10AMT;
      end if;
      if P_CATCHUPPLANSTRTDT11 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANSTRTDT11';
        cur_colvalue:=P_CATCHUPPLANSTRTDT11;
        DBrec.CATCHUPPLANSTRTDT11:=P_CATCHUPPLANSTRTDT11;
      end if;
      if P_CATCHUPPLANENDDT11 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANENDDT11';
        cur_colvalue:=P_CATCHUPPLANENDDT11;
        DBrec.CATCHUPPLANENDDT11:=P_CATCHUPPLANENDDT11;
      end if;
      if P_CATCHUPPLANRQ11AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANRQ11AMT';
        cur_colvalue:=P_CATCHUPPLANRQ11AMT;
        DBrec.CATCHUPPLANRQ11AMT:=P_CATCHUPPLANRQ11AMT;
      end if;
      if P_CATCHUPPLANSTRTDT12 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANSTRTDT12';
        cur_colvalue:=P_CATCHUPPLANSTRTDT12;
        DBrec.CATCHUPPLANSTRTDT12:=P_CATCHUPPLANSTRTDT12;
      end if;
      if P_CATCHUPPLANENDDT12 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANENDDT12';
        cur_colvalue:=P_CATCHUPPLANENDDT12;
        DBrec.CATCHUPPLANENDDT12:=P_CATCHUPPLANENDDT12;
      end if;
      if P_CATCHUPPLANRQ12AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANRQ12AMT';
        cur_colvalue:=P_CATCHUPPLANRQ12AMT;
        DBrec.CATCHUPPLANRQ12AMT:=P_CATCHUPPLANRQ12AMT;
      end if;
      if P_CATCHUPPLANSTRTDT13 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANSTRTDT13';
        cur_colvalue:=P_CATCHUPPLANSTRTDT13;
        DBrec.CATCHUPPLANSTRTDT13:=P_CATCHUPPLANSTRTDT13;
      end if;
      if P_CATCHUPPLANENDDT13 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANENDDT13';
        cur_colvalue:=P_CATCHUPPLANENDDT13;
        DBrec.CATCHUPPLANENDDT13:=P_CATCHUPPLANENDDT13;
      end if;
      if P_CATCHUPPLANRQ13AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANRQ13AMT';
        cur_colvalue:=P_CATCHUPPLANRQ13AMT;
        DBrec.CATCHUPPLANRQ13AMT:=P_CATCHUPPLANRQ13AMT;
      end if;
      if P_CATCHUPPLANSTRTDT14 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANSTRTDT14';
        cur_colvalue:=P_CATCHUPPLANSTRTDT14;
        DBrec.CATCHUPPLANSTRTDT14:=P_CATCHUPPLANSTRTDT14;
      end if;
      if P_CATCHUPPLANENDDT14 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANENDDT14';
        cur_colvalue:=P_CATCHUPPLANENDDT14;
        DBrec.CATCHUPPLANENDDT14:=P_CATCHUPPLANENDDT14;
      end if;
      if P_CATCHUPPLANRQ14AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANRQ14AMT';
        cur_colvalue:=P_CATCHUPPLANRQ14AMT;
        DBrec.CATCHUPPLANRQ14AMT:=P_CATCHUPPLANRQ14AMT;
      end if;
      if P_CATCHUPPLANSTRTDT15 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANSTRTDT15';
        cur_colvalue:=P_CATCHUPPLANSTRTDT15;
        DBrec.CATCHUPPLANSTRTDT15:=P_CATCHUPPLANSTRTDT15;
      end if;
      if P_CATCHUPPLANENDDT15 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANENDDT15';
        cur_colvalue:=P_CATCHUPPLANENDDT15;
        DBrec.CATCHUPPLANENDDT15:=P_CATCHUPPLANENDDT15;
      end if;
      if P_CATCHUPPLANRQ15AMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANRQ15AMT';
        cur_colvalue:=P_CATCHUPPLANRQ15AMT;
        DBrec.CATCHUPPLANRQ15AMT:=P_CATCHUPPLANRQ15AMT;
      end if;
      if P_CATCHUPPLANCREDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANCREDT';
        cur_colvalue:=P_CATCHUPPLANCREDT;
        DBrec.CATCHUPPLANCREDT:=P_CATCHUPPLANCREDT;
      end if;
      if P_CATCHUPPLANMODDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CATCHUPPLANMODDT';
        cur_colvalue:=P_CATCHUPPLANMODDT;
        DBrec.CATCHUPPLANMODDT:=P_CATCHUPPLANMODDT;
      end if;
    exception when others then
      p_errval:=sqlcode;
      if maxsev<3 then maxsev:=3; end if;
      p_errmsg:='Error using '||cur_colname||' Value >'||cur_colvalue||'<';
    end;
  end if;
  -- if all still ok do actual update
  if p_errval=0 then
    Begin
      update STGCSA.SOFVCTCHTBL
        set lastupdtuserid=DBrec.lastupdtuserid,
          lastupdtdt=DBrec.lastupdtdt
          ,CATCHUPPLANSBANMB=DBrec.CATCHUPPLANSBANMB
          ,CATCHUPPLANSTRTDT1=DBrec.CATCHUPPLANSTRTDT1
          ,CATCHUPPLANENDDT1=DBrec.CATCHUPPLANENDDT1
          ,CATCHUPPLANRQ1AMT=DBrec.CATCHUPPLANRQ1AMT
          ,CATCHUPPLANSTRTDT2=DBrec.CATCHUPPLANSTRTDT2
          ,CATCHUPPLANENDDT2=DBrec.CATCHUPPLANENDDT2
          ,CATCHUPPLANRQ2AMT=DBrec.CATCHUPPLANRQ2AMT
          ,CATCHUPPLANSTRTDT3=DBrec.CATCHUPPLANSTRTDT3
          ,CATCHUPPLANENDDT3=DBrec.CATCHUPPLANENDDT3
          ,CATCHUPPLANRQ3AMT=DBrec.CATCHUPPLANRQ3AMT
          ,CATCHUPPLANSTRTDT4=DBrec.CATCHUPPLANSTRTDT4
          ,CATCHUPPLANENDDT4=DBrec.CATCHUPPLANENDDT4
          ,CATCHUPPLANRQ4AMT=DBrec.CATCHUPPLANRQ4AMT
          ,CATCHUPPLANSTRTDT5=DBrec.CATCHUPPLANSTRTDT5
          ,CATCHUPPLANENDDT5=DBrec.CATCHUPPLANENDDT5
          ,CATCHUPPLANRQ5AMT=DBrec.CATCHUPPLANRQ5AMT
          ,CATCHUPPLANSTRTDT6=DBrec.CATCHUPPLANSTRTDT6
          ,CATCHUPPLANENDDT6=DBrec.CATCHUPPLANENDDT6
          ,CATCHUPPLANRQ6AMT=DBrec.CATCHUPPLANRQ6AMT
          ,CATCHUPPLANSTRTDT7=DBrec.CATCHUPPLANSTRTDT7
          ,CATCHUPPLANENDDT7=DBrec.CATCHUPPLANENDDT7
          ,CATCHUPPLANRQ7AMT=DBrec.CATCHUPPLANRQ7AMT
          ,CATCHUPPLANSTRTDT8=DBrec.CATCHUPPLANSTRTDT8
          ,CATCHUPPLANENDDT8=DBrec.CATCHUPPLANENDDT8
          ,CATCHUPPLANRQ8AMT=DBrec.CATCHUPPLANRQ8AMT
          ,CATCHUPPLANSTRTDT9=DBrec.CATCHUPPLANSTRTDT9
          ,CATCHUPPLANENDDT9=DBrec.CATCHUPPLANENDDT9
          ,CATCHUPPLANRQ9AMT=DBrec.CATCHUPPLANRQ9AMT
          ,CATCHUPPLANSTRTDT10=DBrec.CATCHUPPLANSTRTDT10
          ,CATCHUPPLANENDDT10=DBrec.CATCHUPPLANENDDT10
          ,CATCHUPPLANRQ10AMT=DBrec.CATCHUPPLANRQ10AMT
          ,CATCHUPPLANSTRTDT11=DBrec.CATCHUPPLANSTRTDT11
          ,CATCHUPPLANENDDT11=DBrec.CATCHUPPLANENDDT11
          ,CATCHUPPLANRQ11AMT=DBrec.CATCHUPPLANRQ11AMT
          ,CATCHUPPLANSTRTDT12=DBrec.CATCHUPPLANSTRTDT12
          ,CATCHUPPLANENDDT12=DBrec.CATCHUPPLANENDDT12
          ,CATCHUPPLANRQ12AMT=DBrec.CATCHUPPLANRQ12AMT
          ,CATCHUPPLANSTRTDT13=DBrec.CATCHUPPLANSTRTDT13
          ,CATCHUPPLANENDDT13=DBrec.CATCHUPPLANENDDT13
          ,CATCHUPPLANRQ13AMT=DBrec.CATCHUPPLANRQ13AMT
          ,CATCHUPPLANSTRTDT14=DBrec.CATCHUPPLANSTRTDT14
          ,CATCHUPPLANENDDT14=DBrec.CATCHUPPLANENDDT14
          ,CATCHUPPLANRQ14AMT=DBrec.CATCHUPPLANRQ14AMT
          ,CATCHUPPLANSTRTDT15=DBrec.CATCHUPPLANSTRTDT15
          ,CATCHUPPLANENDDT15=DBrec.CATCHUPPLANENDDT15
          ,CATCHUPPLANRQ15AMT=DBrec.CATCHUPPLANRQ15AMT
          ,CATCHUPPLANCREDT=DBrec.CATCHUPPLANCREDT
          ,CATCHUPPLANMODDT=DBrec.CATCHUPPLANMODDT
        where rowid=rowid_in;
      recnum:=recnum+1;
    exception when others then
      p_errval:=sqlcode;
      p_retval:=0;
      if maxsev<3 then maxsev:=3; end if;
      p_errmsg:= 'Error while doing update on STGCSA.SOFVCTCHTBL, on rec with rowid '
      ||rowid_in||' previously fetched using key: '||orig_keyset_values
      ||' got Oracle error '||sqlerrm(sqlcode);
    end;
  end if;
END;
begin
  -- setup
  savepoint SOFVCTCHUPDTSP;
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  recnum:=0;
  runtime.logger(logged_msg_id,'STDLOG',100,'Init SOFVCTCHUPDTSP',p_userid,4,
    logtxt1=>'SOFVCTCHUPDTSP',logtxt2=>'stdtblupd_template v3.22 12 Oct 2018',
    PROGRAM_NAME=>'SOFVCTCHUPDTSP');
  --
  --
  -- Check to see if crossover is ongoing, in which case updates are prohibited
  SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
  IF crossover_not_active != 'Y' THEN
    p_errval := -20099;
    p_errmsg := 'Warning: Cannot update table SOFVCTCHTBL at this time because crossover is running, please try later.';
  END IF;
  -- end of crossover_lockout_check
--
-- special p_identifier kludge
--  if p_identifier is set to -1 then it treated as 0 (update by PK)
--  but a flag is set use passed value EVEN IF NULL.  Since for option -1
--  every column is updated to what was passed, intended values MUST be passed
--  for all columns.  This option allows setting colulmns to null!
  overridden_p_identifier:=p_identifier;
  if p_identifier=-1 then
    do_normal_field_checking:='N';
    overridden_p_identifier:=0;
  end if;
  case overridden_p_identifier
  when 0 then
    -- case to update one row based on primary key
    -- p_identifier=0 (choice 0) is default action for a stored procedure
    -- get existing version of row
    if p_errval=0 then
      keystouse:='(CATCHUPPLANSBANMB)=('||P_CATCHUPPLANSBANMB||')';
      begin
        select rowid into rec_rowid from STGCSA.SOFVCTCHTBL where 1=1
         and CATCHUPPLANSBANMB=P_CATCHUPPLANSBANMB;
      exception when no_data_found then
        p_errval:=0;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Warning No STGCSA.SOFVCTCHTBL row to update with key(s) '
          ||keystouse;
      end;
    end if;
    if p_errval=0 then
      fetch_and_update_by_rowid(rec_rowid, keystouse);
    end if;
  else /* P_IDENTIFIER was unexpected value */
    p_errval:=-20002;
    if maxsev<3 then maxsev:=3; end if;
    p_errmsg:='Error P_IDENTIFIER contained unexpected value: '||p_identifier;
  end case;
  P_RETVAL:=RECNUM;
  -- post to log
  if p_errval=0 then
    runtime.logger(logged_msg_id,'STDLOG',101,
      'End SOFVCTCHUPDTSP With 0 return, P_RETVAL='
      ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
      1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld
      ,PROGRAM_NAME=>'SOFVCTCHUPDTSP');
  else
    ROLLBACK TO SOFVCTCHUPDTSP;
    p_errmsg:=p_errmsg||' in SOFVCTCHUPDTSP build 2018-11-07 11:35:37';
    runtime.logger(logged_msg_id,'STDERR',103,p_errmsg,p_userid,
      maxsev, l_usernm, l_loannmb,l_transid, l_dbmdl, l_entrydt, l_timestampfld,
      PROGRAM_NAME=>'SOFVCTCHUPDTSP',force_log_entry=>true);
  end if;
EXCEPTION
WHEN OTHERS THEN
  ROLLBACK TO SOFVCTCHUPDTSP;
  p_errmsg:=' Error outer exception handler caught Oracle error:'||SQLERRM(SQLCODE)
    ||' in SOFVCTCHUPDTSP build 2018-11-07 11:35:37';
  runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
    3,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld,
    PROGRAM_NAME=>'SOFVCTCHUPDTSP',force_log_entry=>true);
--
END SOFVCTCHUPDTSP;
/


GRANT EXECUTE ON STGCSA.SOFVCTCHUPDTSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.SOFVCTCHUPDTSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.SOFVCTCHUPDTSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.SOFVCTCHUPDTSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.SOFVCTCHUPDTSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.SOFVCTCHUPDTSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.SOFVCTCHUPDTSP TO STGCSADEVROLE;

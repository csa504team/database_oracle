DROP PROCEDURE STGCSA.REFSBAOFCSELTSP;

CREATE OR REPLACE PROCEDURE STGCSA.RefSBAOfcSelTSP
(
    p_Identifier IN number := 0,
    p_RetVal OUT number,
    p_ErrVal OUT number,
    p_ErrMsg OUT varchar2,
    p_SelCur OUT SYS_REFCURSOR
)AS
-- variable declaration
BEGIN
    SAVEPOINT RefSBAOfcSelTSP;
	
	IF p_Identifier = 0 OR p_Identifier IS NULL
    THEN
    /* select from REFSBAOFCTBL Table */
    BEGIN
        OPEN p_SelCur FOR
		SELECT DISTINCT(SBAOFCCD) 
		FROM stgcsa.REFSBAOFCTBL;
		
		p_RetVal := SQL%ROWCOUNT;
        p_ErrVal := SQLCODE;
        p_ErrMsg := SQLERRM;
    END;
    ELSIF p_Identifier = 2
    THEN
    BEGIN
        OPEN p_SelCur FOR
        SELECT
            1
        FROM stgcsa.REFSBAOFCTBL;
 
        p_RetVal := SQL%ROWCOUNT;
        p_ErrVal := SQLCODE;
        p_ErrMsg := SQLERRM;
    END;
    END IF;
	
EXCEPTION
    WHEN OTHERS THEN
    BEGIN
        p_RetVal := 0;
        p_ErrVal := SQLCODE;
        p_ErrMsg := SQLERRM;
 
        ROLLBACK TO RefSBAOfcSelTSP;
        RAISE;
    END;
END RefSBAOfcSelTSP;
/


GRANT EXECUTE ON STGCSA.REFSBAOFCSELTSP TO CDCONLINEDEVROLE;

GRANT EXECUTE ON STGCSA.REFSBAOFCSELTSP TO CDCONLINEREADALLROLE;

GRANT EXECUTE ON STGCSA.REFSBAOFCSELTSP TO CSAUPDTROLE;

DROP PROCEDURE STGCSA.CNTCTUPDTSP;

CREATE OR REPLACE PROCEDURE STGCSA.CNTCTUPDTSP(
    p_retval out number,
    p_errval out number,
    p_errmsg  out varchar2,
    p_identifier number:=0,
    p_userid varchar2
   ,p_CNTCTID VARCHAR2:=null
   ,p_CNTCTFIRSTNM VARCHAR2:=null
   ,p_CNTCTLASTNM VARCHAR2:=null
   ,p_CNTCTEMAILADR VARCHAR2:=null
   ,p_CNTCTPHNNMB VARCHAR2:=null
   ,p_CNTCTPHNXTN CHAR:=null
   ,p_CNTCTFAXNMB VARCHAR2:=null
   ,p_CNTCTMAILADRSTR1NM VARCHAR2:=null
   ,p_CNTCTMAILADRSTR2NM VARCHAR2:=null
   ,p_CNTCTMAILADRCTYNM VARCHAR2:=null
   ,p_CNTCTMAILADRSTCD CHAR:=null
   ,p_CNTCTMAILADRZIPCD CHAR:=null
   ,p_CNTCTMAILADRZIP4CD CHAR:=null
   ,p_CNTCTTITL VARCHAR2:=null
   ,p_CDCREF VARCHAR2:=null
   ,p_CREATBY VARCHAR2:=null
   ,p_UPDTBY VARCHAR2:=null
   ,p_ENTID VARCHAR2:=null
) as
 /*
  Created on: 2018-11-07 11:32:34
  Created by: GENR
  Crerated from template stdtblupd_template v3.22 12 Oct 2018 on 2018-11-07 11:32:34
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  Procedure CNTCTUPDTSP performs UPDATE on STGCSA.CNTCTTBL
    for P_IDENTIFIER=0 qualification is on Primary Key: CNTCTID
    Updates all columns except the qualification keys and columns for which a
      null value is passed.
    SPECIAL secret update kludge!
      if p_identifier is set to -1 then a flag is set to use all input parm values
      and p_identifier is changed to 0 (update by PK).
      THEREFORE if using p_identifier=-1 you MUST pass intended value for all columns.
      This option allows setting colulmns to null!
*/
  dbrec STGCSA.CNTCTTBL%rowtype;
  logged_msg_id number;
  recnum number;
  rec_rowid rowid;
  keystouse varchar2(256);
  maxsev number:=0;
  pctsign char(1):='%';
  cur_colname varchar2(100):=null;
  cur_colvalue varchar2(4000);
  do_normal_field_checking char(1):='Y';
  overridden_p_identifier number;
  -- standard activity log capture vars
  l_LOANNMB char(10):=null;
  l_TRANSID number:=null;
  l_dbmdl varchar2(10):=null;
  l_entrydt date:=null;
  l_userid  varchar2(32):=null;
  l_usernm  varchar2(199):=null;
  l_timestampfld date:=null;
procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
begin
  if p_errval=0 then
    begin
      select * into dbrec from STGCSA.CNTCTTBL where rowid=rowid_in;
    exception when others then
      p_errval:=sqlcode;
      maxsev:=3;
      p_retval:=0;
      p_errmsg:='Fetch of STGCSA.CNTCTTBL row by rowid '||rowid_in
        ||' orinally fetched using '||orig_keyset_values
        ||' failed with '||sqlerrm(sqlcode);
    end;
  end if;
  if p_errval=0 then
    -- set this as lastupdt
    DBrec.lastupdtuserid:=p_userid;
    DBrec.lastupdtdt:=sysdate;
    -- copy into DBrec all table column input parameters that are not null,
    -- saving column name/value if needed for error reporting
    --  (if p_identifier=-1 then move input even if null)
    begin
      if P_CNTCTID is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CNTCTID';
        cur_colvalue:=P_CNTCTID;
        DBrec.CNTCTID:=P_CNTCTID;
      end if;
      if P_CNTCTFIRSTNM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CNTCTFIRSTNM';
        cur_colvalue:=P_CNTCTFIRSTNM;
        DBrec.CNTCTFIRSTNM:=P_CNTCTFIRSTNM;
      end if;
      if P_CNTCTLASTNM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CNTCTLASTNM';
        cur_colvalue:=P_CNTCTLASTNM;
        DBrec.CNTCTLASTNM:=P_CNTCTLASTNM;
      end if;
      if P_CNTCTEMAILADR is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CNTCTEMAILADR';
        cur_colvalue:=P_CNTCTEMAILADR;
        DBrec.CNTCTEMAILADR:=P_CNTCTEMAILADR;
      end if;
      if P_CNTCTPHNNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CNTCTPHNNMB';
        cur_colvalue:=P_CNTCTPHNNMB;
        DBrec.CNTCTPHNNMB:=P_CNTCTPHNNMB;
      end if;
      if P_CNTCTPHNXTN is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CNTCTPHNXTN';
        cur_colvalue:=P_CNTCTPHNXTN;
        DBrec.CNTCTPHNXTN:=P_CNTCTPHNXTN;
      end if;
      if P_CNTCTFAXNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CNTCTFAXNMB';
        cur_colvalue:=P_CNTCTFAXNMB;
        DBrec.CNTCTFAXNMB:=P_CNTCTFAXNMB;
      end if;
      if P_CNTCTMAILADRSTR1NM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CNTCTMAILADRSTR1NM';
        cur_colvalue:=P_CNTCTMAILADRSTR1NM;
        DBrec.CNTCTMAILADRSTR1NM:=P_CNTCTMAILADRSTR1NM;
      end if;
      if P_CNTCTMAILADRSTR2NM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CNTCTMAILADRSTR2NM';
        cur_colvalue:=P_CNTCTMAILADRSTR2NM;
        DBrec.CNTCTMAILADRSTR2NM:=P_CNTCTMAILADRSTR2NM;
      end if;
      if P_CNTCTMAILADRCTYNM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CNTCTMAILADRCTYNM';
        cur_colvalue:=P_CNTCTMAILADRCTYNM;
        DBrec.CNTCTMAILADRCTYNM:=P_CNTCTMAILADRCTYNM;
      end if;
      if P_CNTCTMAILADRSTCD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CNTCTMAILADRSTCD';
        cur_colvalue:=P_CNTCTMAILADRSTCD;
        DBrec.CNTCTMAILADRSTCD:=P_CNTCTMAILADRSTCD;
      end if;
      if P_CNTCTMAILADRZIPCD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CNTCTMAILADRZIPCD';
        cur_colvalue:=P_CNTCTMAILADRZIPCD;
        DBrec.CNTCTMAILADRZIPCD:=P_CNTCTMAILADRZIPCD;
      end if;
      if P_CNTCTMAILADRZIP4CD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CNTCTMAILADRZIP4CD';
        cur_colvalue:=P_CNTCTMAILADRZIP4CD;
        DBrec.CNTCTMAILADRZIP4CD:=P_CNTCTMAILADRZIP4CD;
      end if;
      if P_CNTCTTITL is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CNTCTTITL';
        cur_colvalue:=P_CNTCTTITL;
        DBrec.CNTCTTITL:=P_CNTCTTITL;
      end if;
      if P_CDCREF is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCREF';
        cur_colvalue:=P_CDCREF;
        DBrec.CDCREF:=P_CDCREF;
      end if;
      if P_CREATBY is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CREATBY';
        cur_colvalue:=P_CREATBY;
        DBrec.CREATBY:=P_CREATBY;
      end if;
      if P_UPDTBY is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_UPDTBY';
        cur_colvalue:=P_UPDTBY;
        DBrec.UPDTBY:=P_UPDTBY;
      end if;
      if P_ENTID is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_ENTID';
        cur_colvalue:=P_ENTID;
        DBrec.ENTID:=P_ENTID;
      end if;
    exception when others then
      p_errval:=sqlcode;
      if maxsev<3 then maxsev:=3; end if;
      p_errmsg:='Error using '||cur_colname||' Value >'||cur_colvalue||'<';
    end;
  end if;
  -- if all still ok do actual update
  if p_errval=0 then
    Begin
      update STGCSA.CNTCTTBL
        set lastupdtuserid=DBrec.lastupdtuserid,
          lastupdtdt=DBrec.lastupdtdt
          ,CNTCTID=DBrec.CNTCTID
          ,CNTCTFIRSTNM=DBrec.CNTCTFIRSTNM
          ,CNTCTLASTNM=DBrec.CNTCTLASTNM
          ,CNTCTEMAILADR=DBrec.CNTCTEMAILADR
          ,CNTCTPHNNMB=DBrec.CNTCTPHNNMB
          ,CNTCTPHNXTN=DBrec.CNTCTPHNXTN
          ,CNTCTFAXNMB=DBrec.CNTCTFAXNMB
          ,CNTCTMAILADRSTR1NM=DBrec.CNTCTMAILADRSTR1NM
          ,CNTCTMAILADRSTR2NM=DBrec.CNTCTMAILADRSTR2NM
          ,CNTCTMAILADRCTYNM=DBrec.CNTCTMAILADRCTYNM
          ,CNTCTMAILADRSTCD=DBrec.CNTCTMAILADRSTCD
          ,CNTCTMAILADRZIPCD=DBrec.CNTCTMAILADRZIPCD
          ,CNTCTMAILADRZIP4CD=DBrec.CNTCTMAILADRZIP4CD
          ,CNTCTTITL=DBrec.CNTCTTITL
          ,CDCREF=DBrec.CDCREF
          ,CREATBY=DBrec.CREATBY
          ,UPDTBY=DBrec.UPDTBY
          ,ENTID=DBrec.ENTID
        where rowid=rowid_in;
      recnum:=recnum+1;
    exception when others then
      p_errval:=sqlcode;
      p_retval:=0;
      if maxsev<3 then maxsev:=3; end if;
      p_errmsg:= 'Error while doing update on STGCSA.CNTCTTBL, on rec with rowid '
      ||rowid_in||' previously fetched using key: '||orig_keyset_values
      ||' got Oracle error '||sqlerrm(sqlcode);
    end;
  end if;
END;
begin
  -- setup
  savepoint CNTCTUPDTSP;
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  recnum:=0;
  runtime.logger(logged_msg_id,'STDLOG',100,'Init CNTCTUPDTSP',p_userid,4,
    logtxt1=>'CNTCTUPDTSP',logtxt2=>'stdtblupd_template v3.22 12 Oct 2018',
    PROGRAM_NAME=>'CNTCTUPDTSP');
  --
--
-- special p_identifier kludge
--  if p_identifier is set to -1 then it treated as 0 (update by PK)
--  but a flag is set use passed value EVEN IF NULL.  Since for option -1
--  every column is updated to what was passed, intended values MUST be passed
--  for all columns.  This option allows setting colulmns to null!
  overridden_p_identifier:=p_identifier;
  if p_identifier=-1 then
    do_normal_field_checking:='N';
    overridden_p_identifier:=0;
  end if;
  case overridden_p_identifier
  when 0 then
    -- case to update one row based on primary key
    -- p_identifier=0 (choice 0) is default action for a stored procedure
    -- get existing version of row
    if p_errval=0 then
      keystouse:='(CNTCTID)=('||P_CNTCTID||')';
      begin
        select rowid into rec_rowid from STGCSA.CNTCTTBL where 1=1
         and CNTCTID=P_CNTCTID;
      exception when no_data_found then
        p_errval:=0;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Warning No STGCSA.CNTCTTBL row to update with key(s) '
          ||keystouse;
      end;
    end if;
    if p_errval=0 then
      fetch_and_update_by_rowid(rec_rowid, keystouse);
    end if;
  else /* P_IDENTIFIER was unexpected value */
    p_errval:=-20002;
    if maxsev<3 then maxsev:=3; end if;
    p_errmsg:='Error P_IDENTIFIER contained unexpected value: '||p_identifier;
  end case;
  P_RETVAL:=RECNUM;
  -- post to log
  if p_errval=0 then
    runtime.logger(logged_msg_id,'STDLOG',101,
      'End CNTCTUPDTSP With 0 return, P_RETVAL='
      ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
      1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld
      ,PROGRAM_NAME=>'CNTCTUPDTSP');
  else
    ROLLBACK TO CNTCTUPDTSP;
    p_errmsg:=p_errmsg||' in CNTCTUPDTSP build 2018-11-07 11:32:34';
    runtime.logger(logged_msg_id,'STDERR',103,p_errmsg,p_userid,
      maxsev, l_usernm, l_loannmb,l_transid, l_dbmdl, l_entrydt, l_timestampfld,
      PROGRAM_NAME=>'CNTCTUPDTSP',force_log_entry=>true);
  end if;
EXCEPTION
WHEN OTHERS THEN
  ROLLBACK TO CNTCTUPDTSP;
  p_errmsg:=' Error outer exception handler caught Oracle error:'||SQLERRM(SQLCODE)
    ||' in CNTCTUPDTSP build 2018-11-07 11:32:34';
  runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
    3,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld,
    PROGRAM_NAME=>'CNTCTUPDTSP',force_log_entry=>true);
--
END CNTCTUPDTSP;
/


GRANT EXECUTE ON STGCSA.CNTCTUPDTSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.CNTCTUPDTSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.CNTCTUPDTSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.CNTCTUPDTSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.CNTCTUPDTSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.CNTCTUPDTSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.CNTCTUPDTSP TO STGCSADEVROLE;

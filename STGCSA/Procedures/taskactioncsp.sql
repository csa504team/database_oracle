DROP PROCEDURE STGCSA.TASKACTIONCSP;

CREATE OR REPLACE PROCEDURE STGCSA.TaskActionCSP (
    p_taskname varchar2 := NULL,
    p_StatCd varchar2 := NULL,
    p_CreatUserId varchar2 := NULL )
AS
    p_retval number := 0;
    p_errval number := 0;
    p_errmsg varchar2(500) := '';

BEGIN
	IF (upper(p_StatCd) not in ('S','E'))
    THEN
       BEGIN
            RAISE_APPLICATION_ERROR( -20001, 'Invalid Status. Only S or E are valid. S: Start, E: End');
            RETURN;
       END;
    END IF;

    IF upper(p_StatCd) = 'S' THEN
        STGCSA.TaskStartCSP(p_taskname, p_creatuserid, p_retval => p_retval, p_errval => p_errval, p_errmsg => p_errmsg );
    ELSE
        STGCSA.TaskEndCSP(p_taskname, p_retval => p_retval, p_errval => p_errval, p_errmsg => p_errmsg );
    END IF;

    IF p_retval != 0 THEN
        RAISE_APPLICATION_ERROR( -20002, 'Error: ' || p_errmsg);
        RETURN;
    END IF;

END;
/


GRANT EXECUTE ON STGCSA.TASKACTIONCSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.TASKACTIONCSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.TASKACTIONCSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.TASKACTIONCSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.TASKACTIONCSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.TASKACTIONCSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.TASKACTIONCSP TO STGCSADEVROLE;

DROP PROCEDURE STGCSA.IMPTARPRPTUPDTSP;

CREATE OR REPLACE PROCEDURE STGCSA.IMPTARPRPTUPDTSP(
    p_retval out number,
    p_errval out number,
    p_errmsg  out varchar2,
    p_identifier number:=0,
    p_userid varchar2
   ,p_IMPTARPRPTID VARCHAR2:=null
   ,p_STARTDT DATE:=null
   ,p_ENDDT DATE:=null
   ,p_BNKID VARCHAR2:=null
   ,p_BNKNM VARCHAR2:=null
   ,p_BNKSTCD CHAR:=null
   ,p_ACCTNMB VARCHAR2:=null
   ,p_ACCTTYP VARCHAR2:=null
   ,p_ACCTNM VARCHAR2:=null
   ,p_CURCY VARCHAR2:=null
   ,p_SCTNRPTNM VARCHAR2:=null
   ,p_TRANSTYP VARCHAR2:=null
   ,p_SRLNMBREFNMB VARCHAR2:=null
   ,p_CHKAMT VARCHAR2:=null
   ,p_ISSDT DATE:=null
   ,p_POSTEDDT DATE:=null
   ,p_STOPDT DATE:=null
   ,p_RLSEDT DATE:=null
   ,p_ASOFDT DATE:=null
   ,p_RVRSDDT DATE:=null
   ,p_DRCRDTCD VARCHAR2:=null
   ,p_OPTINFOTRANDESC VARCHAR2:=null
) as
 /*
  Created on: 2018-11-07 11:34:18
  Created by: GENR
  Crerated from template stdtblupd_template v3.22 12 Oct 2018 on 2018-11-07 11:34:19
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  Procedure IMPTARPRPTUPDTSP performs UPDATE on STGCSA.IMPTARPRPTTBL
    for P_IDENTIFIER=0 qualification is on Primary Key: IMPTARPRPTID
    for P_IDENTIFIER=1 qualification is on IMPTARPRPTID like pctsign
    Updates all columns except the qualification keys and columns for which a
      null value is passed.
    SPECIAL secret update kludge!
      if p_identifier is set to -1 then a flag is set to use all input parm values
      and p_identifier is changed to 0 (update by PK).
      THEREFORE if using p_identifier=-1 you MUST pass intended value for all columns.
      This option allows setting colulmns to null!
*/
  dbrec STGCSA.IMPTARPRPTTBL%rowtype;
  logged_msg_id number;
  recnum number;
  rec_rowid rowid;
  keystouse varchar2(256);
  maxsev number:=0;
  pctsign char(1):='%';
  cur_colname varchar2(100):=null;
  cur_colvalue varchar2(4000);
  do_normal_field_checking char(1):='Y';
  overridden_p_identifier number;
  -- standard activity log capture vars
  l_LOANNMB char(10):=null;
  l_TRANSID number:=null;
  l_dbmdl varchar2(10):=null;
  l_entrydt date:=null;
  l_userid  varchar2(32):=null;
  l_usernm  varchar2(199):=null;
  l_timestampfld date:=null;
procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
begin
  if p_errval=0 then
    begin
      select * into dbrec from STGCSA.IMPTARPRPTTBL where rowid=rowid_in;
    exception when others then
      p_errval:=sqlcode;
      maxsev:=3;
      p_retval:=0;
      p_errmsg:='Fetch of STGCSA.IMPTARPRPTTBL row by rowid '||rowid_in
        ||' orinally fetched using '||orig_keyset_values
        ||' failed with '||sqlerrm(sqlcode);
    end;
  end if;
  if p_errval=0 then
    -- set this as lastupdt
    DBrec.lastupdtuserid:=p_userid;
    DBrec.lastupdtdt:=sysdate;
    -- copy into DBrec all table column input parameters that are not null,
    -- saving column name/value if needed for error reporting
    --  (if p_identifier=-1 then move input even if null)
    begin
      if P_IMPTARPRPTID is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_IMPTARPRPTID';
        cur_colvalue:=P_IMPTARPRPTID;
        DBrec.IMPTARPRPTID:=P_IMPTARPRPTID;
      end if;
      if P_STARTDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_STARTDT';
        cur_colvalue:=P_STARTDT;
        DBrec.STARTDT:=P_STARTDT;
      end if;
      if P_ENDDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_ENDDT';
        cur_colvalue:=P_ENDDT;
        DBrec.ENDDT:=P_ENDDT;
      end if;
      if P_BNKID is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_BNKID';
        cur_colvalue:=P_BNKID;
        DBrec.BNKID:=P_BNKID;
      end if;
      if P_BNKNM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_BNKNM';
        cur_colvalue:=P_BNKNM;
        DBrec.BNKNM:=P_BNKNM;
      end if;
      if P_BNKSTCD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_BNKSTCD';
        cur_colvalue:=P_BNKSTCD;
        DBrec.BNKSTCD:=P_BNKSTCD;
      end if;
      if P_ACCTNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_ACCTNMB';
        cur_colvalue:=P_ACCTNMB;
        DBrec.ACCTNMB:=P_ACCTNMB;
      end if;
      if P_ACCTTYP is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_ACCTTYP';
        cur_colvalue:=P_ACCTTYP;
        DBrec.ACCTTYP:=P_ACCTTYP;
      end if;
      if P_ACCTNM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_ACCTNM';
        cur_colvalue:=P_ACCTNM;
        DBrec.ACCTNM:=P_ACCTNM;
      end if;
      if P_CURCY is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CURCY';
        cur_colvalue:=P_CURCY;
        DBrec.CURCY:=P_CURCY;
      end if;
      if P_SCTNRPTNM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SCTNRPTNM';
        cur_colvalue:=P_SCTNRPTNM;
        DBrec.SCTNRPTNM:=P_SCTNRPTNM;
      end if;
      if P_TRANSTYP is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_TRANSTYP';
        cur_colvalue:=P_TRANSTYP;
        DBrec.TRANSTYP:=P_TRANSTYP;
      end if;
      if P_SRLNMBREFNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SRLNMBREFNMB';
        cur_colvalue:=P_SRLNMBREFNMB;
        DBrec.SRLNMBREFNMB:=P_SRLNMBREFNMB;
      end if;
      if P_CHKAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CHKAMT';
        cur_colvalue:=P_CHKAMT;
        DBrec.CHKAMT:=P_CHKAMT;
      end if;
      if P_ISSDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_ISSDT';
        cur_colvalue:=P_ISSDT;
        DBrec.ISSDT:=P_ISSDT;
      end if;
      if P_POSTEDDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_POSTEDDT';
        cur_colvalue:=P_POSTEDDT;
        DBrec.POSTEDDT:=P_POSTEDDT;
      end if;
      if P_STOPDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_STOPDT';
        cur_colvalue:=P_STOPDT;
        DBrec.STOPDT:=P_STOPDT;
      end if;
      if P_RLSEDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_RLSEDT';
        cur_colvalue:=P_RLSEDT;
        DBrec.RLSEDT:=P_RLSEDT;
      end if;
      if P_ASOFDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_ASOFDT';
        cur_colvalue:=P_ASOFDT;
        DBrec.ASOFDT:=P_ASOFDT;
      end if;
      if P_RVRSDDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_RVRSDDT';
        cur_colvalue:=P_RVRSDDT;
        DBrec.RVRSDDT:=P_RVRSDDT;
      end if;
      if P_DRCRDTCD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DRCRDTCD';
        cur_colvalue:=P_DRCRDTCD;
        DBrec.DRCRDTCD:=P_DRCRDTCD;
      end if;
      if P_OPTINFOTRANDESC is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_OPTINFOTRANDESC';
        cur_colvalue:=P_OPTINFOTRANDESC;
        DBrec.OPTINFOTRANDESC:=P_OPTINFOTRANDESC;
      end if;
    exception when others then
      p_errval:=sqlcode;
      if maxsev<3 then maxsev:=3; end if;
      p_errmsg:='Error using '||cur_colname||' Value >'||cur_colvalue||'<';
    end;
  end if;
  -- if all still ok do actual update
  if p_errval=0 then
    Begin
      update STGCSA.IMPTARPRPTTBL
        set lastupdtuserid=DBrec.lastupdtuserid,
          lastupdtdt=DBrec.lastupdtdt
          ,IMPTARPRPTID=DBrec.IMPTARPRPTID
          ,STARTDT=DBrec.STARTDT
          ,ENDDT=DBrec.ENDDT
          ,BNKID=DBrec.BNKID
          ,BNKNM=DBrec.BNKNM
          ,BNKSTCD=DBrec.BNKSTCD
          ,ACCTNMB=DBrec.ACCTNMB
          ,ACCTTYP=DBrec.ACCTTYP
          ,ACCTNM=DBrec.ACCTNM
          ,CURCY=DBrec.CURCY
          ,SCTNRPTNM=DBrec.SCTNRPTNM
          ,TRANSTYP=DBrec.TRANSTYP
          ,SRLNMBREFNMB=DBrec.SRLNMBREFNMB
          ,CHKAMT=DBrec.CHKAMT
          ,ISSDT=DBrec.ISSDT
          ,POSTEDDT=DBrec.POSTEDDT
          ,STOPDT=DBrec.STOPDT
          ,RLSEDT=DBrec.RLSEDT
          ,ASOFDT=DBrec.ASOFDT
          ,RVRSDDT=DBrec.RVRSDDT
          ,DRCRDTCD=DBrec.DRCRDTCD
          ,OPTINFOTRANDESC=DBrec.OPTINFOTRANDESC
        where rowid=rowid_in;
      recnum:=recnum+1;
    exception when others then
      p_errval:=sqlcode;
      p_retval:=0;
      if maxsev<3 then maxsev:=3; end if;
      p_errmsg:= 'Error while doing update on STGCSA.IMPTARPRPTTBL, on rec with rowid '
      ||rowid_in||' previously fetched using key: '||orig_keyset_values
      ||' got Oracle error '||sqlerrm(sqlcode);
    end;
  end if;
END;
begin
  -- setup
  savepoint IMPTARPRPTUPDTSP;
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  recnum:=0;
  runtime.logger(logged_msg_id,'STDLOG',100,'Init IMPTARPRPTUPDTSP',p_userid,4,
    logtxt1=>'IMPTARPRPTUPDTSP',logtxt2=>'stdtblupd_template v3.22 12 Oct 2018',
    PROGRAM_NAME=>'IMPTARPRPTUPDTSP');
  --
--
-- special p_identifier kludge
--  if p_identifier is set to -1 then it treated as 0 (update by PK)
--  but a flag is set use passed value EVEN IF NULL.  Since for option -1
--  every column is updated to what was passed, intended values MUST be passed
--  for all columns.  This option allows setting colulmns to null!
  overridden_p_identifier:=p_identifier;
  if p_identifier=-1 then
    do_normal_field_checking:='N';
    overridden_p_identifier:=0;
  end if;
  case overridden_p_identifier
  when 0 then
    -- case to update one row based on primary key
    -- p_identifier=0 (choice 0) is default action for a stored procedure
    -- get existing version of row
    if p_errval=0 then
      keystouse:='(IMPTARPRPTID)=('||P_IMPTARPRPTID||')';
      begin
        select rowid into rec_rowid from STGCSA.IMPTARPRPTTBL where 1=1
         and IMPTARPRPTID=P_IMPTARPRPTID;
      exception when no_data_found then
        p_errval:=0;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Warning No STGCSA.IMPTARPRPTTBL row to update with key(s) '
          ||keystouse;
      end;
    end if;
    if p_errval=0 then
      fetch_and_update_by_rowid(rec_rowid, keystouse);
    end if;
  when 1 then
    -- case to update one or more rows based on keyset KEYS1
    if p_errval=0 then
      keystouse:='(IMPTARPRPTID)like pctsign';
      begin
        for r1 in (select rowid from STGCSA.IMPTARPRPTTBL a where 1=1

            and IMPTARPRPTID like pctsign
  )
        loop
          rec_rowid:=r1.rowid;
          if p_errval=0 then
            fetch_and_update_by_rowid(rec_rowid, keystouse);
          end if;
        end loop;
      exception when others then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<3 then maxsev:=3; end if;
        p_errmsg:='Error fetching STGCSA.IMPTARPRPTTBL row(s) with key(s) '
         ||keystouse||' Oracle returned '||sqlerrm(sqlcode);
      end;
      if recnum=0 then
        p_errval:=0;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Warning No STGCSA.IMPTARPRPTTBL row(s) to update with key(s) '
          ||keystouse;
      end if;
    end if;
  else /* P_IDENTIFIER was unexpected value */
    p_errval:=-20002;
    if maxsev<3 then maxsev:=3; end if;
    p_errmsg:='Error P_IDENTIFIER contained unexpected value: '||p_identifier;
  end case;
  P_RETVAL:=RECNUM;
  -- post to log
  if p_errval=0 then
    runtime.logger(logged_msg_id,'STDLOG',101,
      'End IMPTARPRPTUPDTSP With 0 return, P_RETVAL='
      ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
      1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld
      ,PROGRAM_NAME=>'IMPTARPRPTUPDTSP');
  else
    ROLLBACK TO IMPTARPRPTUPDTSP;
    p_errmsg:=p_errmsg||' in IMPTARPRPTUPDTSP build 2018-11-07 11:34:18';
    runtime.logger(logged_msg_id,'STDERR',103,p_errmsg,p_userid,
      maxsev, l_usernm, l_loannmb,l_transid, l_dbmdl, l_entrydt, l_timestampfld,
      PROGRAM_NAME=>'IMPTARPRPTUPDTSP',force_log_entry=>true);
  end if;
EXCEPTION
WHEN OTHERS THEN
  ROLLBACK TO IMPTARPRPTUPDTSP;
  p_errmsg:=' Error outer exception handler caught Oracle error:'||SQLERRM(SQLCODE)
    ||' in IMPTARPRPTUPDTSP build 2018-11-07 11:34:18';
  runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
    3,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld,
    PROGRAM_NAME=>'IMPTARPRPTUPDTSP',force_log_entry=>true);
--
END IMPTARPRPTUPDTSP;
/


GRANT EXECUTE ON STGCSA.IMPTARPRPTUPDTSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.IMPTARPRPTUPDTSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.IMPTARPRPTUPDTSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.IMPTARPRPTUPDTSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.IMPTARPRPTUPDTSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.IMPTARPRPTUPDTSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.IMPTARPRPTUPDTSP TO STGCSADEVROLE;

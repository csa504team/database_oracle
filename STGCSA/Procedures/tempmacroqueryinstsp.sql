DROP PROCEDURE STGCSA.TEMPMACROQUERYINSTSP;

CREATE OR REPLACE PROCEDURE STGCSA.TEMPMACROQUERYINSTSP(
    p_retval out number,
    p_errval out number,
    p_errmsg  out varchar2,
    p_identifier number:=0,
    p_userid varchar2
   ,p_RECORDID VARCHAR2:=null
   ,p_LOANNMB CHAR:=null
   ,p_CDCREGNCD CHAR:=null
   ,p_CDCCERTNMB CHAR:=null
   ,p_STATUS CHAR:=null
   ,p_KEYTYPE CHAR:=null
   ,p_KEYDATE DATE:=null
   ,p_DISBIND CHAR:=null
) as
 /*
  Created on: 2018-11-07 11:36:45
  Created by: GENR
  Crerated from template stdtblins_template v3.05 13 Sep 2018 on 2018-11-07 11:36:45
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  Procedure performs INSERT on STGCSA.TEMPMACROQUERYTBLfor P_IDENTIFIER=0
*/
  rundate date;
  gn global_name.global_name%type;
  new_rec STGCSA.TEMPMACROQUERYTBL%rowtype;
  logged_msgid_retval number;
  audretval number;
  crossover_active char(1);
  l_LOANNMB char(10):=null;
  l_TRANSID number:=null;
  l_dbmdl varchar2(10):=null;
  l_entrydt date:=null;
  l_userid  varchar2(32):=null;
  l_usernm  varchar2(199):=null;
  l_timestampfld date:=null;
  cur_col_name varchar2(100):=null;
  maxsev number:=0;
  holdstring varchar2(4000);
  keystouse varchar2(1000);
  -- std date for column defaulting
  JAN_1_1900  constant date:=to_date('JAN-01-1900','MON-DD-YYYY');
  cur_colvalue varchar2(4095);
  cur_colname varchar2(30);
-- Function used to initialize RECORDID from sequence TEMPMACROQUERYSEQ
Function NEXTVAL_TEMPMACROQUERYSEQ return number is
  N number;
begin
  select TEMPMACROQUERYSEQ.nextval into n from dual;
  return n;
end;
-- Main body begins here
begin
  -- setup
  savepoint TEMPMACROQUERYINSTSP;
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  runtime.logger(logged_msgid_retval,'STDLOG',100,'Init TEMPMACROQUERYINSTSP',p_userid,4,
    logtxt1=>'TEMPMACROQUERYINSTSP',logtxt2=>'stdtblins_template v3.05 13 Sep 2018',
    PROGRAM_NAME=>'TEMPMACROQUERYINSTSP');
  select global_name, sysdate into gn, rundate from global_name;
  begin
    cur_colname:='LOANNMB';
    cur_colvalue:=P_LOANNMB;
    l_LOANNMB:=P_LOANNMB;
  exception when others then
    p_errval:=sqlcode;
    p_errmsg:='Error '||sqlerrm(sqlcode)||' using '||cur_colname||' Value >'||cur_colvalue||'<';
  end;
  new_rec.creatuserid:=p_userid;
  new_rec.creatdt:=sysdate;
  new_rec.lastupdtuserid:=p_userid;
  new_rec.lastupdtdt:=sysdate;
  if p_identifier=0 then
    -- p_identifier=0 this is default for a stored procedure, only action for insert
    runtime.errfound:=false;
    runtime.errstr:=' ';
    --
    -- Default columns as applicable
  if p_errval=0 then
    begin
      cur_colname:='P_RECORDID';
      cur_colvalue:=P_RECORDID;
      if P_RECORDID is null then

        new_rec.RECORDID:=NEXTVAL_TEMPMACROQUERYSEQ();

      else
        new_rec.RECORDID:=P_RECORDID;
      end if;
      cur_colname:='P_LOANNMB';
      cur_colvalue:=P_LOANNMB;
      if P_LOANNMB is null then

        new_rec.LOANNMB:=rpad(' ',10);

      else
        new_rec.LOANNMB:=P_LOANNMB;
      end if;
      cur_colname:='P_CDCREGNCD';
      cur_colvalue:=P_CDCREGNCD;
      if P_CDCREGNCD is null then

        new_rec.CDCREGNCD:=rpad(' ',2);

      else
        new_rec.CDCREGNCD:=P_CDCREGNCD;
      end if;
      cur_colname:='P_CDCCERTNMB';
      cur_colvalue:=P_CDCCERTNMB;
      if P_CDCCERTNMB is null then

        new_rec.CDCCERTNMB:=rpad(' ',4);

      else
        new_rec.CDCCERTNMB:=P_CDCCERTNMB;
      end if;
      cur_colname:='P_STATUS';
      cur_colvalue:=P_STATUS;
      if P_STATUS is null then

        new_rec.STATUS:=rpad(' ',1);

      else
        new_rec.STATUS:=P_STATUS;
      end if;
      cur_colname:='P_KEYTYPE';
      cur_colvalue:=P_KEYTYPE;
      if P_KEYTYPE is null then

        new_rec.KEYTYPE:=rpad(' ',1);

      else
        new_rec.KEYTYPE:=P_KEYTYPE;
      end if;
      cur_colname:='P_KEYDATE';
      cur_colvalue:=P_KEYDATE;
      if P_KEYDATE is null then

        new_rec.KEYDATE:=jan_1_1900;

      else
        new_rec.KEYDATE:=P_KEYDATE;
      end if;
      cur_colname:='P_DISBIND';
      cur_colvalue:=P_DISBIND;
      if P_DISBIND is null then

        new_rec.DISBIND:=rpad(' ',1);

      else
        new_rec.DISBIND:=P_DISBIND;
      end if;
    exception when others then
       p_errval:=sqlcode;
       p_errmsg:='Error - Oracle returned '||sqlerrm(sqlcode)||' using '
           ||cur_colname||' Value >'||cur_colvalue||'<';
    end;
  end if;
    keystouse:='(RECORDID)=('||new_rec.RECORDID||')';
    if p_errval=0 then
      begin
        insert into STGCSA.TEMPMACROQUERYTBL values new_rec;
        p_RETVAL := SQL%ROWCOUNT;
      exception
      when dup_val_on_index then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Insert caused dupe on unique IX: '||sqlerrm(sqlcode);
      when others then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<3 then maxsev:=3; end if;
        p_errmsg:='Inserting record caught SQL error '||sqlerrm(sqlcode);
      end;
    end if;
  else /* P_IDENTIFIER was unexpected value */
    p_errval:=-20002;
    if maxsev<3 then maxsev:=3; end if;
    p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier
      ||' inserting record with key '||keystouse;
  end if;
  if p_errval=0 then
    runtime.logger(logged_msgid_retval,'STDLOG',101,
      'End TEMPMACROQUERYINSTSP With 0 return, P_RETVAL='
      ||p_retval||' Record Key '||keystouse,p_userid,
      1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt,
      l_timestampfld,PROGRAM_NAME=>'TEMPMACROQUERYINSTSP');
  else
    p_errmsg:='Error: '||P_ERRMSG||' in TEMPMACROQUERYINSTSP Record Key '||keystouse
      ||' Build: 2018-11-07 11:36:45';
    ROLLBACK TO TEMPMACROQUERYINSTSP;
    runtime.logger(logged_msgid_retval,'STDERR',103,
      p_errmsg,p_userid,maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,
      l_entrydt, l_timestampfld,PROGRAM_NAME=>'TEMPMACROQUERYINSTSP',force_log_entry=>TRUE);
  end if;
EXCEPTION
WHEN OTHERS THEN
  p_errval:=SQLCODE;
  p_errmsg:=' In outer exception handler in TEMPMACROQUERYINSTSP caught exception '
    ||sqlerrm(sqlcode)||' Record Key '||keystouse||' build 2018-11-07 11:36:45';
  ROLLBACK TO TEMPMACROQUERYINSTSP;
  runtime.logger(logged_msgid_retval,'STDERR',20,p_errmsg,p_userid,3,
  PROGRAM_NAME=>'TEMPMACROQUERYINSTSP',force_log_entry=>TRUE);
--
END TEMPMACROQUERYINSTSP;
/


GRANT EXECUTE ON STGCSA.TEMPMACROQUERYINSTSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.TEMPMACROQUERYINSTSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.TEMPMACROQUERYINSTSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.TEMPMACROQUERYINSTSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.TEMPMACROQUERYINSTSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.TEMPMACROQUERYINSTSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.TEMPMACROQUERYINSTSP TO STGCSADEVROLE;

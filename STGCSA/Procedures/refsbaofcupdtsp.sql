DROP PROCEDURE STGCSA.REFSBAOFCUPDTSP;

CREATE OR REPLACE PROCEDURE STGCSA.REFSBAOFCUPDTSP(
    p_retval out number,
    p_errval out number,
    p_errmsg  out varchar2,
    p_identifier number:=0,
    p_userid varchar2
   ,p_SBAOFCCD CHAR:=null
   ,p_SBAOFCNM VARCHAR2:=null
   ,p_SBAOFCADRSTR1NM VARCHAR2:=null
   ,p_SBAOFCADRSTR2NM VARCHAR2:=null
   ,p_SBAOFCADRCTYNM VARCHAR2:=null
   ,p_SBAOFCADRSTCD CHAR:=null
   ,p_SBAOFCADRZIPCD CHAR:=null
   ,p_SBAOFCADRZIP4CD CHAR:=null
   ,p_SBAOFCPHNNMB CHAR:=null
) as
 /*
  Created on: 2018-11-07 11:35:11
  Created by: GENR
  Crerated from template stdtblupd_template v3.22 12 Oct 2018 on 2018-11-07 11:35:11
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  Procedure REFSBAOFCUPDTSP performs UPDATE on STGCSA.REFSBAOFCTBL
    for P_IDENTIFIER=0 qualification is on Primary Key: SBAOFCCD
    Updates all columns except the qualification keys and columns for which a
      null value is passed.
    SPECIAL secret update kludge!
      if p_identifier is set to -1 then a flag is set to use all input parm values
      and p_identifier is changed to 0 (update by PK).
      THEREFORE if using p_identifier=-1 you MUST pass intended value for all columns.
      This option allows setting colulmns to null!
*/
  dbrec STGCSA.REFSBAOFCTBL%rowtype;
  logged_msg_id number;
  recnum number;
  rec_rowid rowid;
  keystouse varchar2(256);
  maxsev number:=0;
  pctsign char(1):='%';
  cur_colname varchar2(100):=null;
  cur_colvalue varchar2(4000);
  do_normal_field_checking char(1):='Y';
  overridden_p_identifier number;
  -- standard activity log capture vars
  l_LOANNMB char(10):=null;
  l_TRANSID number:=null;
  l_dbmdl varchar2(10):=null;
  l_entrydt date:=null;
  l_userid  varchar2(32):=null;
  l_usernm  varchar2(199):=null;
  l_timestampfld date:=null;
procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
begin
  if p_errval=0 then
    begin
      select * into dbrec from STGCSA.REFSBAOFCTBL where rowid=rowid_in;
    exception when others then
      p_errval:=sqlcode;
      maxsev:=3;
      p_retval:=0;
      p_errmsg:='Fetch of STGCSA.REFSBAOFCTBL row by rowid '||rowid_in
        ||' orinally fetched using '||orig_keyset_values
        ||' failed with '||sqlerrm(sqlcode);
    end;
  end if;
  if p_errval=0 then
    -- set this as lastupdt
    DBrec.lastupdtuserid:=p_userid;
    DBrec.lastupdtdt:=sysdate;
    -- copy into DBrec all table column input parameters that are not null,
    -- saving column name/value if needed for error reporting
    --  (if p_identifier=-1 then move input even if null)
    begin
      if P_SBAOFCCD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SBAOFCCD';
        cur_colvalue:=P_SBAOFCCD;
        DBrec.SBAOFCCD:=P_SBAOFCCD;
      end if;
      if P_SBAOFCNM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SBAOFCNM';
        cur_colvalue:=P_SBAOFCNM;
        DBrec.SBAOFCNM:=P_SBAOFCNM;
      end if;
      if P_SBAOFCADRSTR1NM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SBAOFCADRSTR1NM';
        cur_colvalue:=P_SBAOFCADRSTR1NM;
        DBrec.SBAOFCADRSTR1NM:=P_SBAOFCADRSTR1NM;
      end if;
      if P_SBAOFCADRSTR2NM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SBAOFCADRSTR2NM';
        cur_colvalue:=P_SBAOFCADRSTR2NM;
        DBrec.SBAOFCADRSTR2NM:=P_SBAOFCADRSTR2NM;
      end if;
      if P_SBAOFCADRCTYNM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SBAOFCADRCTYNM';
        cur_colvalue:=P_SBAOFCADRCTYNM;
        DBrec.SBAOFCADRCTYNM:=P_SBAOFCADRCTYNM;
      end if;
      if P_SBAOFCADRSTCD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SBAOFCADRSTCD';
        cur_colvalue:=P_SBAOFCADRSTCD;
        DBrec.SBAOFCADRSTCD:=P_SBAOFCADRSTCD;
      end if;
      if P_SBAOFCADRZIPCD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SBAOFCADRZIPCD';
        cur_colvalue:=P_SBAOFCADRZIPCD;
        DBrec.SBAOFCADRZIPCD:=P_SBAOFCADRZIPCD;
      end if;
      if P_SBAOFCADRZIP4CD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SBAOFCADRZIP4CD';
        cur_colvalue:=P_SBAOFCADRZIP4CD;
        DBrec.SBAOFCADRZIP4CD:=P_SBAOFCADRZIP4CD;
      end if;
      if P_SBAOFCPHNNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SBAOFCPHNNMB';
        cur_colvalue:=P_SBAOFCPHNNMB;
        DBrec.SBAOFCPHNNMB:=P_SBAOFCPHNNMB;
      end if;
    exception when others then
      p_errval:=sqlcode;
      if maxsev<3 then maxsev:=3; end if;
      p_errmsg:='Error using '||cur_colname||' Value >'||cur_colvalue||'<';
    end;
  end if;
  -- if all still ok do actual update
  if p_errval=0 then
    Begin
      update STGCSA.REFSBAOFCTBL
        set lastupdtuserid=DBrec.lastupdtuserid,
          lastupdtdt=DBrec.lastupdtdt
          ,SBAOFCCD=DBrec.SBAOFCCD
          ,SBAOFCNM=DBrec.SBAOFCNM
          ,SBAOFCADRSTR1NM=DBrec.SBAOFCADRSTR1NM
          ,SBAOFCADRSTR2NM=DBrec.SBAOFCADRSTR2NM
          ,SBAOFCADRCTYNM=DBrec.SBAOFCADRCTYNM
          ,SBAOFCADRSTCD=DBrec.SBAOFCADRSTCD
          ,SBAOFCADRZIPCD=DBrec.SBAOFCADRZIPCD
          ,SBAOFCADRZIP4CD=DBrec.SBAOFCADRZIP4CD
          ,SBAOFCPHNNMB=DBrec.SBAOFCPHNNMB
        where rowid=rowid_in;
      recnum:=recnum+1;
    exception when others then
      p_errval:=sqlcode;
      p_retval:=0;
      if maxsev<3 then maxsev:=3; end if;
      p_errmsg:= 'Error while doing update on STGCSA.REFSBAOFCTBL, on rec with rowid '
      ||rowid_in||' previously fetched using key: '||orig_keyset_values
      ||' got Oracle error '||sqlerrm(sqlcode);
    end;
  end if;
END;
begin
  -- setup
  savepoint REFSBAOFCUPDTSP;
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  recnum:=0;
  runtime.logger(logged_msg_id,'STDLOG',100,'Init REFSBAOFCUPDTSP',p_userid,4,
    logtxt1=>'REFSBAOFCUPDTSP',logtxt2=>'stdtblupd_template v3.22 12 Oct 2018',
    PROGRAM_NAME=>'REFSBAOFCUPDTSP');
  --
--
-- special p_identifier kludge
--  if p_identifier is set to -1 then it treated as 0 (update by PK)
--  but a flag is set use passed value EVEN IF NULL.  Since for option -1
--  every column is updated to what was passed, intended values MUST be passed
--  for all columns.  This option allows setting colulmns to null!
  overridden_p_identifier:=p_identifier;
  if p_identifier=-1 then
    do_normal_field_checking:='N';
    overridden_p_identifier:=0;
  end if;
  case overridden_p_identifier
  when 0 then
    -- case to update one row based on primary key
    -- p_identifier=0 (choice 0) is default action for a stored procedure
    -- get existing version of row
    if p_errval=0 then
      keystouse:='(SBAOFCCD)=('||P_SBAOFCCD||')';
      begin
        select rowid into rec_rowid from STGCSA.REFSBAOFCTBL where 1=1
         and SBAOFCCD=P_SBAOFCCD;
      exception when no_data_found then
        p_errval:=0;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Warning No STGCSA.REFSBAOFCTBL row to update with key(s) '
          ||keystouse;
      end;
    end if;
    if p_errval=0 then
      fetch_and_update_by_rowid(rec_rowid, keystouse);
    end if;
  else /* P_IDENTIFIER was unexpected value */
    p_errval:=-20002;
    if maxsev<3 then maxsev:=3; end if;
    p_errmsg:='Error P_IDENTIFIER contained unexpected value: '||p_identifier;
  end case;
  P_RETVAL:=RECNUM;
  -- post to log
  if p_errval=0 then
    runtime.logger(logged_msg_id,'STDLOG',101,
      'End REFSBAOFCUPDTSP With 0 return, P_RETVAL='
      ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
      1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld
      ,PROGRAM_NAME=>'REFSBAOFCUPDTSP');
  else
    ROLLBACK TO REFSBAOFCUPDTSP;
    p_errmsg:=p_errmsg||' in REFSBAOFCUPDTSP build 2018-11-07 11:35:11';
    runtime.logger(logged_msg_id,'STDERR',103,p_errmsg,p_userid,
      maxsev, l_usernm, l_loannmb,l_transid, l_dbmdl, l_entrydt, l_timestampfld,
      PROGRAM_NAME=>'REFSBAOFCUPDTSP',force_log_entry=>true);
  end if;
EXCEPTION
WHEN OTHERS THEN
  ROLLBACK TO REFSBAOFCUPDTSP;
  p_errmsg:=' Error outer exception handler caught Oracle error:'||SQLERRM(SQLCODE)
    ||' in REFSBAOFCUPDTSP build 2018-11-07 11:35:11';
  runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
    3,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld,
    PROGRAM_NAME=>'REFSBAOFCUPDTSP',force_log_entry=>true);
--
END REFSBAOFCUPDTSP;
/

DROP PROCEDURE STGCSA.TASKSTARTCSP;

CREATE OR REPLACE PROCEDURE STGCSA.TaskStartCSP (
    p_taskname VARCHAR2 :='',
    P_CREATUSERID VARCHAR2:=NULL,
    p_retval out number,
    p_errval out number,
    p_errmsg out varchar2)
as
    v_count number;
    v_cur_jobid number;
    v_cur_jobTypeID number;
    v_cur_jobCategory number;
    v_task REFJOBTYPETBL%ROWTYPE;
    v_seqnmb number := 1;
    v_sysdate date := sysdate;
    v_taskname varchar2(100) := '[' || UPPER(NVL(p_taskname,'')) || ']';
BEGIN 

    p_errval := 0;
    p_retval := -1;
    p_errmsg := 'Success';

    IF NVL(Length(Trim(p_taskname)),0) = 0 THEN
        p_errmsg := 'Failed: Task name cannot be blank';
        p_errval := -20000;
        --raise_application_error(p_errval, p_errmsg);
        RETURN;
    END IF;

    p_retval := -2;

    SELECT COUNT(*) INTO v_count FROM stgcsa.REFJOBTYPETBL WHERE UPPER(JOBTYPENM) = UPPER(p_taskname);

    /* the task is not defined. Return to caller */
    IF v_count = 0 THEN
        p_errmsg := 'Failed: Unexpected task value: ' || v_taskname;
        p_errval := -20001;
        --raise_application_error(p_errval, p_errmsg);
        RETURN;
    END IF;    

    p_retval := -3;

    SELECT * INTO v_task FROM stgcsa.REFJOBTYPETBL WHERE UPPER(JOBTYPENM) = UPPER(p_taskname);

    p_retval := -4;

    SELECT NVL(MAX(jobid),0) INTO v_cur_jobid FROM stgcsa.MFJOBTBL;

    SELECT COUNT(*) INTO v_count FROM stgcsa.MFJOBTBL WHERE COMPLETEDT IS NULL;

    /* if this is from the scheduled job where no MF Update is open, still allow the process */
    IF v_task.JOBTYPENM = 'STG2CSA' AND v_count = 0 THEN
        p_errval := 0; /* redundant, but just to make sure there is something in this block */
    ELSIF v_task.jobtypecategory > 1 THEN
    /* for category > 1, it will be followed sequentially and must have a job open */
        p_retval := -5;
        --SELECT COUNT(*) INTO v_count FROM stgcsa.MFJOBTBL WHERE COMPLETEDT IS NULL;
        /* currently not in the middle of any MF job */
        IF v_count = 0 THEN
            p_errmsg := 'Failed: Task ' || v_taskname || ' must be inside an open job';
            p_errval := -20002;
            --raise_application_error(p_errval, p_errmsg);
            RETURN;
        END IF;       

        /* in the middle of some job. need to find out which one it is */
        p_retval := -6;
        SELECT CurrentJobTypeID INTO v_cur_jobTypeID FROM stgcsa.MFJOBTBL WHERE jobid = v_cur_jobid;
        p_retval := -7;
        SELECT NVL(REF.jobtypecategory, 1) INTO v_cur_jobCategory FROM REFJOBTYPETBL REF WHERE REF.JOBTYPEID = v_cur_jobTypeID;

        p_retval := -8;
        /* check if that task has been finished */        
        SELECT COUNT(*) INTO v_count FROM stgcsa.MFJOBDETAILTBL 
        WHERE jobfk = v_cur_jobid AND jobtypefk = v_cur_jobTypeID AND TASKENDDT IS NULL;

        /* can only process if prior task is done, and can only move forward */
        IF v_count != 0 THEN
            p_errmsg := 'Failed: Task ' || v_taskname || ' cannot be created when other task is not completed';
            p_errval := -20003;
            --raise_application_error(p_errval, p_errmsg);
            RETURN;
        ELSIF v_task.jobtypecategory != v_cur_jobCategory + 1 THEN
            p_errmsg := 'Failed: Task ' || v_taskname || ' cannot be created out of sequence';
            p_errval := -20004;
            --raise_application_error(p_errval, p_errmsg);
            RETURN;
        END IF;            
    ELSE /* new task is category 1*/
        p_retval := -9;

        SELECT COUNT(*) INTO v_count FROM stgcsa.MFJOBTBL WHERE COMPLETEDT IS NULL AND jobid = v_cur_jobid AND CURRENTJOBTYPEID >= 200;
        IF v_count != 0 THEN
            p_errmsg := 'Failed: Task ' || v_taskname || ' cannot be created out of sequence';
            p_errval := -20006;
            --raise_application_error(p_errval, p_errmsg);
            RETURN;
        END IF;

        SELECT COUNT(*) INTO v_count FROM stgcsa.MFJOBDETAILTBL 
        WHERE jobfk = v_cur_jobid AND TASKENDDT IS NULL;

        IF v_count != 0 THEN
            p_errmsg := 'Failed: Task ' || v_taskname || ' cannot be created when other task is not completed';
            p_errval := -20005;
            --raise_application_error(p_errval, p_errmsg);
            RETURN;
        END IF;
    END IF;

    IF p_errval = 0 THEN
    /* create the job */
        p_retval := -10;
        SELECT NVL(MAX(jobid),0) INTO v_cur_jobid FROM stgcsa.MFJOBTBL WHERE COMPLETEDT is NULL;

        IF v_cur_jobid = 0 THEN
            p_retval := -11;
            SELECT STGCSA.MFJOBIDSEQ.NEXTVAL INTO v_cur_jobid FROM DUAL;
            INSERT INTO MFJOBTBL(JOBID, CURRENTJOBTYPEID, CREATDT) VALUES (v_cur_jobid, v_task.JOBTYPEID, v_sysdate);
        ELSE
            UPDATE MFJOBTBL SET CURRENTJOBTYPEID = v_task.JOBTYPEID WHERE JOBID = v_cur_jobid;
        END IF;

        p_retval := -12;
        SELECT NVL(MAX(SEQNMB),0) + 1 INTO v_seqnmb FROM stgcsa.MFJOBDETAILTBL WHERE JOBFK = v_cur_jobid;

        INSERT INTO MFJOBDETAILTBL (JOBFK, JOBTYPEFK, CREATUSERID, TASKSTARTTDT, SEQNMB)
        VALUES (v_cur_jobid, v_task.JOBTYPEID, P_CREATUSERID, v_sysdate, v_seqnmb);

        p_errmsg := 'Success in creating task ' || v_taskname || '. Job ID: ' || v_cur_jobid || ' / Sequence : ' ||  v_seqnmb; 
        p_retval := 0;

        RETURN;
    END IF;       

EXCEPTION
WHEN OTHERS THEN
  p_errval:=SQLCODE;
  p_errmsg:=SQLERRM(SQLCODE);  
  --RAISE;
END TaskStartCSP;
/


GRANT EXECUTE ON STGCSA.TASKSTARTCSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.TASKSTARTCSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.TASKSTARTCSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.TASKSTARTCSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.TASKSTARTCSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.TASKSTARTCSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.TASKSTARTCSP TO STGCSADEVROLE;

DROP PROCEDURE STGCSA.SOFVLND1TBLSELTSP;

CREATE OR REPLACE PROCEDURE STGCSA.SOFVLND1TBLSELTSP
(
    p_Identifier IN number := 0,
    p_RetVal OUT number,
    p_ErrVal OUT number,
    p_ErrMsg OUT varchar2,
    p_SelCur OUT SYS_REFCURSOR,
	p_CreatDT DATE:=null
)AS
-- variable declaration
BEGIN
    SAVEPOINT SOFVLND1TBLSELTSP;
	
	IF p_Identifier = 1
    THEN
    /* select from SOFVLND1TBL Table */
    BEGIN
        OPEN p_SelCur FOR
		SELECT 
			L.LOANNMB,
			L.LOANDTLSTATCDOFLOAN,
			L.CREATDT,
			L.LOANDTLNOTEDT
		FROM STGCSA.SOFVLND1TBL L
		WHERE L.CREATDT >= TRUNC(p_CreatDT)
		ORDER BY L.LOANNMB;
		
		p_RetVal := SQL%ROWCOUNT;
        p_ErrVal := SQLCODE;
        p_ErrMsg := SQLERRM;
    END;
    ELSIF p_Identifier = 2
    THEN
    BEGIN
        OPEN p_SelCur FOR
        SELECT
            1
		FROM STGCSA.SOFVLND1TBL L
		WHERE L.CREATDT >= TRUNC(p_CreatDT)
		ORDER BY L.LOANNMB;
 
        p_RetVal := SQL%ROWCOUNT;
        p_ErrVal := SQLCODE;
        p_ErrMsg := SQLERRM;
    END;
    END IF;
	
EXCEPTION
    WHEN OTHERS THEN
    BEGIN
        p_RetVal := 0;
        p_ErrVal := SQLCODE;
        p_ErrMsg := SQLERRM;
 
        ROLLBACK TO SOFVLND1TBLSELTSP;
        RAISE;
    END;
END SOFVLND1TBLSELTSP;
/

DROP PROCEDURE STGCSA.SEL_CORETRANSTBL;

CREATE OR REPLACE PROCEDURE STGCSA.Sel_CoreTransTbl (
  arg_TransID IN stgCSA.CoreTransTbl.TransID%type,
 arg_TransID1 OUT stgcsa.CoreTransTbl.TransID%type,
 arg_LoanNmb OUT stgcsa.CoreTransTbl.LoanNmb%type
  )
AS
V_TransID   stgcsa.CoreTransTbl.TransID%type;
V_LoanNmb   stgcsa.CoreTransTbl.LoanNmb%type;

BEGIN
/******************************************************************************
   NAME:       Sel_CoreTransTbl
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0       10/28/2017    Victor T     1.  Return TransID, LoanNmb From
                    stgCSA.RefTransAttrTbl Table
     ******************************************************************************/

SELECT  TransID, LoanNmb
        INTO V_TransID, V_LoanNmb
FROM stgCSA.CoreTransTbl
WHERE 0=0 AND TransID =  arg_TransID;

arg_TransID1 := V_TransID ;
arg_LoanNmb := V_LoanNmb ;

EXCEPTION
 WHEN NO_DATA_FOUND THEN
    arg_TransID1 := 0 ;
arg_LoanNmb := 0 ;

END Sel_CoreTransTbl;
/


GRANT EXECUTE ON STGCSA.SEL_CORETRANSTBL TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.SEL_CORETRANSTBL TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.SEL_CORETRANSTBL TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.SEL_CORETRANSTBL TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.SEL_CORETRANSTBL TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.SEL_CORETRANSTBL TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.SEL_CORETRANSTBL TO STGCSADEVROLE;

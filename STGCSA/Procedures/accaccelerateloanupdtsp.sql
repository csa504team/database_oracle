DROP PROCEDURE STGCSA.ACCACCELERATELOANUPDTSP;

CREATE OR REPLACE PROCEDURE STGCSA.ACCACCELERATELOANUPDTSP(
    p_retval out number,
    p_errval out number,
    p_errmsg  out varchar2,
    p_identifier number:=0,
    p_userid varchar2
   ,p_ACCELERATNID VARCHAR2:=null
   ,p_LOANNMB CHAR:=null
   ,p_TRANSID VARCHAR2:=null
   ,p_PRIORACCELERATN VARCHAR2:=null
   ,p_STATID VARCHAR2:=null
   ,p_ACCELERATECMNT VARCHAR2:=null
   ,p_SEMIANDT DATE:=null
   ,p_MFINITIALSCRAPEDT DATE:=null
   ,p_MFLASTSCRAPEDT DATE:=null
   ,p_MFCURSTATID VARCHAR2:=null
   ,p_MFCURCMNT VARCHAR2:=null
   ,p_MFINITIALSTATID VARCHAR2:=null
   ,p_MFINITIALCMNT VARCHAR2:=null
   ,p_MFLASTUPDTDT DATE:=null
   ,p_MFISSDT DATE:=null
   ,p_MFLOANMATDT DATE:=null
   ,p_MFORGLPRINAMT VARCHAR2:=null
   ,p_MFINTRTPCT VARCHAR2:=null
   ,p_MFDBENTRAMT VARCHAR2:=null
   ,p_MFTOBECURAMT VARCHAR2:=null
   ,p_MFLASTPYMTDT DATE:=null
   ,p_MFESCROWPRIORPYMTAMT VARCHAR2:=null
   ,p_MFCDCPAIDTHRUDT CHAR:=null
   ,p_MFCDCMODELNQ VARCHAR2:=null
   ,p_MFCDCMOFEEAMT VARCHAR2:=null
   ,p_MFCDCFEEAMT VARCHAR2:=null
   ,p_MFCDCDUEFROMBORRAMT VARCHAR2:=null
   ,p_MFCSAPAIDTHRUDT CHAR:=null
   ,p_MFCSAMODELNQ VARCHAR2:=null
   ,p_MFCSAMOFEEAMT VARCHAR2:=null
   ,p_MFCSAFEEAMT VARCHAR2:=null
   ,p_MFCSADUEFROMBORRAMT VARCHAR2:=null
   ,p_PPAENTRYDT DATE:=null
   ,p_PPAGROUP VARCHAR2:=null
   ,p_PPACDCNMB CHAR:=null
   ,p_PPAORGLPRINAMT VARCHAR2:=null
   ,p_PPADBENTRBALATSEMIANAMT VARCHAR2:=null
   ,p_PPAINTDUEATSEMIANAMT VARCHAR2:=null
   ,p_PPADUEATSEMIANAMT VARCHAR2:=null
   ,p_SBAENTRYDT DATE:=null
   ,p_SBALASTTRNSCRPTIMPTDT DATE:=null
   ,p_SBADBENTRBALATSEMIANAMT VARCHAR2:=null
   ,p_SBAINTDUEATSEMIANAMT VARCHAR2:=null
   ,p_SBACSAFEEAMT VARCHAR2:=null
   ,p_SBACDCFEEAMT VARCHAR2:=null
   ,p_SBAESCROWPRIORPYMTAMT VARCHAR2:=null
   ,p_SBAACCELAMT VARCHAR2:=null
   ,p_SBADUEDT DATE:=null
   ,p_SBANOTERTPCT VARCHAR2:=null
   ,p_SBALASTNOTEBALAMT VARCHAR2:=null
   ,p_SBALASTPYMTDT DATE:=null
   ,p_SBAACCRINTDUEAMT VARCHAR2:=null
   ,p_SBAEXCESSAMT VARCHAR2:=null
   ,p_MFPREVSTATID VARCHAR2:=null
   ,p_MFPREVCMNT VARCHAR2:=null
   ,p_CDCNMB CHAR:=null
   ,p_SERVCNTRID VARCHAR2:=null
   ,p_TIMESTAMPFLD DATE:=null
) as
 /*
  Created on: 2018-11-07 11:32:00
  Created by: GENR
  Crerated from template stdtblupd_template v3.22 12 Oct 2018 on 2018-11-07 11:32:00
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  Procedure ACCACCELERATELOANUPDTSP performs UPDATE on STGCSA.ACCACCELERATELOANTBL
    for P_IDENTIFIER=0 qualification is on Primary Key: ACCELERATNID
    for P_IDENTIFIER=1 qualification is on SEMIANDT, STATID
    for P_IDENTIFIER=2 qualification is on SEMIANDT, STATID =49
    for P_IDENTIFIER=3 qualification is on SEMIANDT, STATID =50
    for P_IDENTIFIER=4 qualification is on TRANSID
    for P_IDENTIFIER=5 qualification is on LOANNMB
    Updates all columns except the qualification keys and columns for which a
      null value is passed.
    SPECIAL secret update kludge!
      if p_identifier is set to -1 then a flag is set to use all input parm values
      and p_identifier is changed to 0 (update by PK).
      THEREFORE if using p_identifier=-1 you MUST pass intended value for all columns.
      This option allows setting colulmns to null!
*/
  dbrec STGCSA.ACCACCELERATELOANTBL%rowtype;
  logged_msg_id number;
  recnum number;
  rec_rowid rowid;
  keystouse varchar2(256);
  maxsev number:=0;
  pctsign char(1):='%';
  cur_colname varchar2(100):=null;
  cur_colvalue varchar2(4000);
  do_normal_field_checking char(1):='Y';
  overridden_p_identifier number;
  -- standard activity log capture vars
  l_LOANNMB char(10):=null;
  l_TRANSID number:=null;
  l_dbmdl varchar2(10):=null;
  l_entrydt date:=null;
  l_userid  varchar2(32):=null;
  l_usernm  varchar2(199):=null;
  l_timestampfld date:=null;
procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
begin
  if p_errval=0 then
    begin
      select * into dbrec from STGCSA.ACCACCELERATELOANTBL where rowid=rowid_in;
    exception when others then
      p_errval:=sqlcode;
      maxsev:=3;
      p_retval:=0;
      p_errmsg:='Fetch of STGCSA.ACCACCELERATELOANTBL row by rowid '||rowid_in
        ||' orinally fetched using '||orig_keyset_values
        ||' failed with '||sqlerrm(sqlcode);
    end;
  end if;
  if p_errval=0 then
    -- set this as lastupdt
    DBrec.lastupdtuserid:=p_userid;
    DBrec.lastupdtdt:=sysdate;
    -- copy into DBrec all table column input parameters that are not null,
    -- saving column name/value if needed for error reporting
    --  (if p_identifier=-1 then move input even if null)
    begin
      if P_ACCELERATNID is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_ACCELERATNID';
        cur_colvalue:=P_ACCELERATNID;
        DBrec.ACCELERATNID:=P_ACCELERATNID;
      end if;
      if P_LOANNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_LOANNMB';
        cur_colvalue:=P_LOANNMB;
        DBrec.LOANNMB:=P_LOANNMB;
      end if;
      if P_TRANSID is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_TRANSID';
        cur_colvalue:=P_TRANSID;
        DBrec.TRANSID:=P_TRANSID;
      end if;
      if P_PRIORACCELERATN is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PRIORACCELERATN';
        cur_colvalue:=P_PRIORACCELERATN;
        DBrec.PRIORACCELERATN:=P_PRIORACCELERATN;
      end if;
      if P_STATID is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_STATID';
        cur_colvalue:=P_STATID;
        DBrec.STATID:=P_STATID;
      end if;
      if P_ACCELERATECMNT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_ACCELERATECMNT';
        cur_colvalue:=P_ACCELERATECMNT;
        DBrec.ACCELERATECMNT:=P_ACCELERATECMNT;
      end if;
      if P_SEMIANDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SEMIANDT';
        cur_colvalue:=P_SEMIANDT;
        DBrec.SEMIANDT:=P_SEMIANDT;
      end if;
      if P_MFINITIALSCRAPEDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_MFINITIALSCRAPEDT';
        cur_colvalue:=P_MFINITIALSCRAPEDT;
        DBrec.MFINITIALSCRAPEDT:=P_MFINITIALSCRAPEDT;
      end if;
      if P_MFLASTSCRAPEDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_MFLASTSCRAPEDT';
        cur_colvalue:=P_MFLASTSCRAPEDT;
        DBrec.MFLASTSCRAPEDT:=P_MFLASTSCRAPEDT;
      end if;
      if P_MFCURSTATID is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_MFCURSTATID';
        cur_colvalue:=P_MFCURSTATID;
        DBrec.MFCURSTATID:=P_MFCURSTATID;
      end if;
      if P_MFCURCMNT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_MFCURCMNT';
        cur_colvalue:=P_MFCURCMNT;
        DBrec.MFCURCMNT:=P_MFCURCMNT;
      end if;
      if P_MFINITIALSTATID is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_MFINITIALSTATID';
        cur_colvalue:=P_MFINITIALSTATID;
        DBrec.MFINITIALSTATID:=P_MFINITIALSTATID;
      end if;
      if P_MFINITIALCMNT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_MFINITIALCMNT';
        cur_colvalue:=P_MFINITIALCMNT;
        DBrec.MFINITIALCMNT:=P_MFINITIALCMNT;
      end if;
      if P_MFLASTUPDTDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_MFLASTUPDTDT';
        cur_colvalue:=P_MFLASTUPDTDT;
        DBrec.MFLASTUPDTDT:=P_MFLASTUPDTDT;
      end if;
      if P_MFISSDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_MFISSDT';
        cur_colvalue:=P_MFISSDT;
        DBrec.MFISSDT:=P_MFISSDT;
      end if;
      if P_MFLOANMATDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_MFLOANMATDT';
        cur_colvalue:=P_MFLOANMATDT;
        DBrec.MFLOANMATDT:=P_MFLOANMATDT;
      end if;
      if P_MFORGLPRINAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_MFORGLPRINAMT';
        cur_colvalue:=P_MFORGLPRINAMT;
        DBrec.MFORGLPRINAMT:=P_MFORGLPRINAMT;
      end if;
      if P_MFINTRTPCT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_MFINTRTPCT';
        cur_colvalue:=P_MFINTRTPCT;
        DBrec.MFINTRTPCT:=P_MFINTRTPCT;
      end if;
      if P_MFDBENTRAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_MFDBENTRAMT';
        cur_colvalue:=P_MFDBENTRAMT;
        DBrec.MFDBENTRAMT:=P_MFDBENTRAMT;
      end if;
      if P_MFTOBECURAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_MFTOBECURAMT';
        cur_colvalue:=P_MFTOBECURAMT;
        DBrec.MFTOBECURAMT:=P_MFTOBECURAMT;
      end if;
      if P_MFLASTPYMTDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_MFLASTPYMTDT';
        cur_colvalue:=P_MFLASTPYMTDT;
        DBrec.MFLASTPYMTDT:=P_MFLASTPYMTDT;
      end if;
      if P_MFESCROWPRIORPYMTAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_MFESCROWPRIORPYMTAMT';
        cur_colvalue:=P_MFESCROWPRIORPYMTAMT;
        DBrec.MFESCROWPRIORPYMTAMT:=P_MFESCROWPRIORPYMTAMT;
      end if;
      if P_MFCDCPAIDTHRUDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_MFCDCPAIDTHRUDT';
        cur_colvalue:=P_MFCDCPAIDTHRUDT;
        DBrec.MFCDCPAIDTHRUDT:=P_MFCDCPAIDTHRUDT;
      end if;
      if P_MFCDCMODELNQ is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_MFCDCMODELNQ';
        cur_colvalue:=P_MFCDCMODELNQ;
        DBrec.MFCDCMODELNQ:=P_MFCDCMODELNQ;
      end if;
      if P_MFCDCMOFEEAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_MFCDCMOFEEAMT';
        cur_colvalue:=P_MFCDCMOFEEAMT;
        DBrec.MFCDCMOFEEAMT:=P_MFCDCMOFEEAMT;
      end if;
      if P_MFCDCFEEAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_MFCDCFEEAMT';
        cur_colvalue:=P_MFCDCFEEAMT;
        DBrec.MFCDCFEEAMT:=P_MFCDCFEEAMT;
      end if;
      if P_MFCDCDUEFROMBORRAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_MFCDCDUEFROMBORRAMT';
        cur_colvalue:=P_MFCDCDUEFROMBORRAMT;
        DBrec.MFCDCDUEFROMBORRAMT:=P_MFCDCDUEFROMBORRAMT;
      end if;
      if P_MFCSAPAIDTHRUDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_MFCSAPAIDTHRUDT';
        cur_colvalue:=P_MFCSAPAIDTHRUDT;
        DBrec.MFCSAPAIDTHRUDT:=P_MFCSAPAIDTHRUDT;
      end if;
      if P_MFCSAMODELNQ is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_MFCSAMODELNQ';
        cur_colvalue:=P_MFCSAMODELNQ;
        DBrec.MFCSAMODELNQ:=P_MFCSAMODELNQ;
      end if;
      if P_MFCSAMOFEEAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_MFCSAMOFEEAMT';
        cur_colvalue:=P_MFCSAMOFEEAMT;
        DBrec.MFCSAMOFEEAMT:=P_MFCSAMOFEEAMT;
      end if;
      if P_MFCSAFEEAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_MFCSAFEEAMT';
        cur_colvalue:=P_MFCSAFEEAMT;
        DBrec.MFCSAFEEAMT:=P_MFCSAFEEAMT;
      end if;
      if P_MFCSADUEFROMBORRAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_MFCSADUEFROMBORRAMT';
        cur_colvalue:=P_MFCSADUEFROMBORRAMT;
        DBrec.MFCSADUEFROMBORRAMT:=P_MFCSADUEFROMBORRAMT;
      end if;
      if P_PPAENTRYDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PPAENTRYDT';
        cur_colvalue:=P_PPAENTRYDT;
        DBrec.PPAENTRYDT:=P_PPAENTRYDT;
      end if;
      if P_PPAGROUP is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PPAGROUP';
        cur_colvalue:=P_PPAGROUP;
        DBrec.PPAGROUP:=P_PPAGROUP;
      end if;
      if P_PPACDCNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PPACDCNMB';
        cur_colvalue:=P_PPACDCNMB;
        DBrec.PPACDCNMB:=P_PPACDCNMB;
      end if;
      if P_PPAORGLPRINAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PPAORGLPRINAMT';
        cur_colvalue:=P_PPAORGLPRINAMT;
        DBrec.PPAORGLPRINAMT:=P_PPAORGLPRINAMT;
      end if;
      if P_PPADBENTRBALATSEMIANAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PPADBENTRBALATSEMIANAMT';
        cur_colvalue:=P_PPADBENTRBALATSEMIANAMT;
        DBrec.PPADBENTRBALATSEMIANAMT:=P_PPADBENTRBALATSEMIANAMT;
      end if;
      if P_PPAINTDUEATSEMIANAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PPAINTDUEATSEMIANAMT';
        cur_colvalue:=P_PPAINTDUEATSEMIANAMT;
        DBrec.PPAINTDUEATSEMIANAMT:=P_PPAINTDUEATSEMIANAMT;
      end if;
      if P_PPADUEATSEMIANAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PPADUEATSEMIANAMT';
        cur_colvalue:=P_PPADUEATSEMIANAMT;
        DBrec.PPADUEATSEMIANAMT:=P_PPADUEATSEMIANAMT;
      end if;
      if P_SBAENTRYDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SBAENTRYDT';
        cur_colvalue:=P_SBAENTRYDT;
        DBrec.SBAENTRYDT:=P_SBAENTRYDT;
      end if;
      if P_SBALASTTRNSCRPTIMPTDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SBALASTTRNSCRPTIMPTDT';
        cur_colvalue:=P_SBALASTTRNSCRPTIMPTDT;
        DBrec.SBALASTTRNSCRPTIMPTDT:=P_SBALASTTRNSCRPTIMPTDT;
      end if;
      if P_SBADBENTRBALATSEMIANAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SBADBENTRBALATSEMIANAMT';
        cur_colvalue:=P_SBADBENTRBALATSEMIANAMT;
        DBrec.SBADBENTRBALATSEMIANAMT:=P_SBADBENTRBALATSEMIANAMT;
      end if;
      if P_SBAINTDUEATSEMIANAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SBAINTDUEATSEMIANAMT';
        cur_colvalue:=P_SBAINTDUEATSEMIANAMT;
        DBrec.SBAINTDUEATSEMIANAMT:=P_SBAINTDUEATSEMIANAMT;
      end if;
      if P_SBACSAFEEAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SBACSAFEEAMT';
        cur_colvalue:=P_SBACSAFEEAMT;
        DBrec.SBACSAFEEAMT:=P_SBACSAFEEAMT;
      end if;
      if P_SBACDCFEEAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SBACDCFEEAMT';
        cur_colvalue:=P_SBACDCFEEAMT;
        DBrec.SBACDCFEEAMT:=P_SBACDCFEEAMT;
      end if;
      if P_SBAESCROWPRIORPYMTAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SBAESCROWPRIORPYMTAMT';
        cur_colvalue:=P_SBAESCROWPRIORPYMTAMT;
        DBrec.SBAESCROWPRIORPYMTAMT:=P_SBAESCROWPRIORPYMTAMT;
      end if;
      if P_SBAACCELAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SBAACCELAMT';
        cur_colvalue:=P_SBAACCELAMT;
        DBrec.SBAACCELAMT:=P_SBAACCELAMT;
      end if;
      if P_SBADUEDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SBADUEDT';
        cur_colvalue:=P_SBADUEDT;
        DBrec.SBADUEDT:=P_SBADUEDT;
      end if;
      if P_SBANOTERTPCT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SBANOTERTPCT';
        cur_colvalue:=P_SBANOTERTPCT;
        DBrec.SBANOTERTPCT:=P_SBANOTERTPCT;
      end if;
      if P_SBALASTNOTEBALAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SBALASTNOTEBALAMT';
        cur_colvalue:=P_SBALASTNOTEBALAMT;
        DBrec.SBALASTNOTEBALAMT:=P_SBALASTNOTEBALAMT;
      end if;
      if P_SBALASTPYMTDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SBALASTPYMTDT';
        cur_colvalue:=P_SBALASTPYMTDT;
        DBrec.SBALASTPYMTDT:=P_SBALASTPYMTDT;
      end if;
      if P_SBAACCRINTDUEAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SBAACCRINTDUEAMT';
        cur_colvalue:=P_SBAACCRINTDUEAMT;
        DBrec.SBAACCRINTDUEAMT:=P_SBAACCRINTDUEAMT;
      end if;
      if P_SBAEXCESSAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SBAEXCESSAMT';
        cur_colvalue:=P_SBAEXCESSAMT;
        DBrec.SBAEXCESSAMT:=P_SBAEXCESSAMT;
      end if;
      if P_MFPREVSTATID is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_MFPREVSTATID';
        cur_colvalue:=P_MFPREVSTATID;
        DBrec.MFPREVSTATID:=P_MFPREVSTATID;
      end if;
      if P_MFPREVCMNT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_MFPREVCMNT';
        cur_colvalue:=P_MFPREVCMNT;
        DBrec.MFPREVCMNT:=P_MFPREVCMNT;
      end if;
      if P_CDCNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCNMB';
        cur_colvalue:=P_CDCNMB;
        DBrec.CDCNMB:=P_CDCNMB;
      end if;
      if P_SERVCNTRID is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SERVCNTRID';
        cur_colvalue:=P_SERVCNTRID;
        DBrec.SERVCNTRID:=P_SERVCNTRID;
      end if;
      if P_TIMESTAMPFLD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_TIMESTAMPFLD';
        cur_colvalue:=P_TIMESTAMPFLD;
        DBrec.TIMESTAMPFLD:=P_TIMESTAMPFLD;
      end if;
    exception when others then
      p_errval:=sqlcode;
      if maxsev<3 then maxsev:=3; end if;
      p_errmsg:='Error using '||cur_colname||' Value >'||cur_colvalue||'<';
    end;
  end if;
  -- if all still ok do actual update
  if p_errval=0 then
    Begin
      update STGCSA.ACCACCELERATELOANTBL
        set lastupdtuserid=DBrec.lastupdtuserid,
          lastupdtdt=DBrec.lastupdtdt
          ,ACCELERATNID=DBrec.ACCELERATNID
          ,LOANNMB=DBrec.LOANNMB
          ,TRANSID=DBrec.TRANSID
          ,PRIORACCELERATN=DBrec.PRIORACCELERATN
          ,STATID=DBrec.STATID
          ,ACCELERATECMNT=DBrec.ACCELERATECMNT
          ,SEMIANDT=DBrec.SEMIANDT
          ,MFINITIALSCRAPEDT=DBrec.MFINITIALSCRAPEDT
          ,MFLASTSCRAPEDT=DBrec.MFLASTSCRAPEDT
          ,MFCURSTATID=DBrec.MFCURSTATID
          ,MFCURCMNT=DBrec.MFCURCMNT
          ,MFINITIALSTATID=DBrec.MFINITIALSTATID
          ,MFINITIALCMNT=DBrec.MFINITIALCMNT
          ,MFLASTUPDTDT=DBrec.MFLASTUPDTDT
          ,MFISSDT=DBrec.MFISSDT
          ,MFLOANMATDT=DBrec.MFLOANMATDT
          ,MFORGLPRINAMT=DBrec.MFORGLPRINAMT
          ,MFINTRTPCT=DBrec.MFINTRTPCT
          ,MFDBENTRAMT=DBrec.MFDBENTRAMT
          ,MFTOBECURAMT=DBrec.MFTOBECURAMT
          ,MFLASTPYMTDT=DBrec.MFLASTPYMTDT
          ,MFESCROWPRIORPYMTAMT=DBrec.MFESCROWPRIORPYMTAMT
          ,MFCDCPAIDTHRUDT=DBrec.MFCDCPAIDTHRUDT
          ,MFCDCMODELNQ=DBrec.MFCDCMODELNQ
          ,MFCDCMOFEEAMT=DBrec.MFCDCMOFEEAMT
          ,MFCDCFEEAMT=DBrec.MFCDCFEEAMT
          ,MFCDCDUEFROMBORRAMT=DBrec.MFCDCDUEFROMBORRAMT
          ,MFCSAPAIDTHRUDT=DBrec.MFCSAPAIDTHRUDT
          ,MFCSAMODELNQ=DBrec.MFCSAMODELNQ
          ,MFCSAMOFEEAMT=DBrec.MFCSAMOFEEAMT
          ,MFCSAFEEAMT=DBrec.MFCSAFEEAMT
          ,MFCSADUEFROMBORRAMT=DBrec.MFCSADUEFROMBORRAMT
          ,PPAENTRYDT=DBrec.PPAENTRYDT
          ,PPAGROUP=DBrec.PPAGROUP
          ,PPACDCNMB=DBrec.PPACDCNMB
          ,PPAORGLPRINAMT=DBrec.PPAORGLPRINAMT
          ,PPADBENTRBALATSEMIANAMT=DBrec.PPADBENTRBALATSEMIANAMT
          ,PPAINTDUEATSEMIANAMT=DBrec.PPAINTDUEATSEMIANAMT
          ,PPADUEATSEMIANAMT=DBrec.PPADUEATSEMIANAMT
          ,SBAENTRYDT=DBrec.SBAENTRYDT
          ,SBALASTTRNSCRPTIMPTDT=DBrec.SBALASTTRNSCRPTIMPTDT
          ,SBADBENTRBALATSEMIANAMT=DBrec.SBADBENTRBALATSEMIANAMT
          ,SBAINTDUEATSEMIANAMT=DBrec.SBAINTDUEATSEMIANAMT
          ,SBACSAFEEAMT=DBrec.SBACSAFEEAMT
          ,SBACDCFEEAMT=DBrec.SBACDCFEEAMT
          ,SBAESCROWPRIORPYMTAMT=DBrec.SBAESCROWPRIORPYMTAMT
          ,SBAACCELAMT=DBrec.SBAACCELAMT
          ,SBADUEDT=DBrec.SBADUEDT
          ,SBANOTERTPCT=DBrec.SBANOTERTPCT
          ,SBALASTNOTEBALAMT=DBrec.SBALASTNOTEBALAMT
          ,SBALASTPYMTDT=DBrec.SBALASTPYMTDT
          ,SBAACCRINTDUEAMT=DBrec.SBAACCRINTDUEAMT
          ,SBAEXCESSAMT=DBrec.SBAEXCESSAMT
          ,MFPREVSTATID=DBrec.MFPREVSTATID
          ,MFPREVCMNT=DBrec.MFPREVCMNT
          ,CDCNMB=DBrec.CDCNMB
          ,SERVCNTRID=DBrec.SERVCNTRID
          ,TIMESTAMPFLD=DBrec.TIMESTAMPFLD
        where rowid=rowid_in;
      recnum:=recnum+1;
    exception when others then
      p_errval:=sqlcode;
      p_retval:=0;
      if maxsev<3 then maxsev:=3; end if;
      p_errmsg:= 'Error while doing update on STGCSA.ACCACCELERATELOANTBL, on rec with rowid '
      ||rowid_in||' previously fetched using key: '||orig_keyset_values
      ||' got Oracle error '||sqlerrm(sqlcode);
    end;
  end if;
END;
begin
  -- setup
  savepoint ACCACCELERATELOANUPDTSP;
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  recnum:=0;
  runtime.logger(logged_msg_id,'STDLOG',100,'Init ACCACCELERATELOANUPDTSP',p_userid,4,
    logtxt1=>'ACCACCELERATELOANUPDTSP',logtxt2=>'stdtblupd_template v3.22 12 Oct 2018',
    PROGRAM_NAME=>'ACCACCELERATELOANUPDTSP');
  --
  l_LOANNMB:=P_LOANNMB;  l_TRANSID:=P_TRANSID;  l_TIMESTAMPFLD:=P_TIMESTAMPFLD;
--
-- special p_identifier kludge
--  if p_identifier is set to -1 then it treated as 0 (update by PK)
--  but a flag is set use passed value EVEN IF NULL.  Since for option -1
--  every column is updated to what was passed, intended values MUST be passed
--  for all columns.  This option allows setting colulmns to null!
  overridden_p_identifier:=p_identifier;
  if p_identifier=-1 then
    do_normal_field_checking:='N';
    overridden_p_identifier:=0;
  end if;
  case overridden_p_identifier
  when 0 then
    -- case to update one row based on primary key
    -- p_identifier=0 (choice 0) is default action for a stored procedure
    -- get existing version of row
    if p_errval=0 then
      keystouse:='(ACCELERATNID)=('||P_ACCELERATNID||')';
      begin
        select rowid into rec_rowid from STGCSA.ACCACCELERATELOANTBL where 1=1
         and ACCELERATNID=P_ACCELERATNID;
      exception when no_data_found then
        p_errval:=0;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Warning No STGCSA.ACCACCELERATELOANTBL row to update with key(s) '
          ||keystouse;
      end;
    end if;
    if p_errval=0 then
      fetch_and_update_by_rowid(rec_rowid, keystouse);
    end if;
  when 1 then
    -- case to update one or more rows based on keyset KEYS1
    if p_errval=0 then
      keystouse:='(STATID)=('||P_STATID||')'||' and '||'(SEMIANDT)=('||P_SEMIANDT||')';
      begin
        for r1 in (select rowid from STGCSA.ACCACCELERATELOANTBL a where 1=1
          and SEMIANDT=P_SEMIANDT
            and STATID=P_STATID
  )
        loop
          rec_rowid:=r1.rowid;
          if p_errval=0 then
            fetch_and_update_by_rowid(rec_rowid, keystouse);
          end if;
        end loop;
      exception when others then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<3 then maxsev:=3; end if;
        p_errmsg:='Error fetching STGCSA.ACCACCELERATELOANTBL row(s) with key(s) '
         ||keystouse||' Oracle returned '||sqlerrm(sqlcode);
      end;
      if recnum=0 then
        p_errval:=0;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Warning No STGCSA.ACCACCELERATELOANTBL row(s) to update with key(s) '
          ||keystouse;
      end if;
    end if;
  when 2 then
    -- case to update one or more rows based on keyset KEYS2
    if p_errval=0 then
      keystouse:='(STATID)=49'||' and '||'(SEMIANDT)=('||P_SEMIANDT||')';
      begin
        for r2 in (select rowid from STGCSA.ACCACCELERATELOANTBL a where 1=1
          and SEMIANDT=P_SEMIANDT

            and STATID =49
  )
        loop
          rec_rowid:=r2.rowid;
          if p_errval=0 then
            fetch_and_update_by_rowid(rec_rowid, keystouse);
          end if;
        end loop;
      exception when others then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<3 then maxsev:=3; end if;
        p_errmsg:='Error fetching STGCSA.ACCACCELERATELOANTBL row(s) with key(s) '
         ||keystouse||' Oracle returned '||sqlerrm(sqlcode);
      end;
      if recnum=0 then
        p_errval:=0;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Warning No STGCSA.ACCACCELERATELOANTBL row(s) to update with key(s) '
          ||keystouse;
      end if;
    end if;
  when 3 then
    -- case to update one or more rows based on keyset KEYS3
    if p_errval=0 then
      keystouse:='(STATID)=50'||' and '||'(SEMIANDT)=('||P_SEMIANDT||')';
      begin
        for r3 in (select rowid from STGCSA.ACCACCELERATELOANTBL a where 1=1
          and SEMIANDT=P_SEMIANDT

            and STATID =50
  )
        loop
          rec_rowid:=r3.rowid;
          if p_errval=0 then
            fetch_and_update_by_rowid(rec_rowid, keystouse);
          end if;
        end loop;
      exception when others then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<3 then maxsev:=3; end if;
        p_errmsg:='Error fetching STGCSA.ACCACCELERATELOANTBL row(s) with key(s) '
         ||keystouse||' Oracle returned '||sqlerrm(sqlcode);
      end;
      if recnum=0 then
        p_errval:=0;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Warning No STGCSA.ACCACCELERATELOANTBL row(s) to update with key(s) '
          ||keystouse;
      end if;
    end if;
  when 4 then
    -- case to update one or more rows based on keyset KEYS4
    if p_errval=0 then
      keystouse:='(TRANSID)=('||P_TRANSID||')';
      begin
        for r4 in (select rowid from STGCSA.ACCACCELERATELOANTBL a where 1=1
          and TRANSID=P_TRANSID
  )
        loop
          rec_rowid:=r4.rowid;
          if p_errval=0 then
            fetch_and_update_by_rowid(rec_rowid, keystouse);
          end if;
        end loop;
      exception when others then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<3 then maxsev:=3; end if;
        p_errmsg:='Error fetching STGCSA.ACCACCELERATELOANTBL row(s) with key(s) '
         ||keystouse||' Oracle returned '||sqlerrm(sqlcode);
      end;
      if recnum=0 then
        p_errval:=0;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Warning No STGCSA.ACCACCELERATELOANTBL row(s) to update with key(s) '
          ||keystouse;
      end if;
    end if;
  when 5 then
    -- case to update one or more rows based on keyset KEYS5
    if p_errval=0 then
      keystouse:='(LOANNMB)=('||P_LOANNMB||')';
      begin
        for r5 in (select rowid from STGCSA.ACCACCELERATELOANTBL a where 1=1
          and LOANNMB=P_LOANNMB
  )
        loop
          rec_rowid:=r5.rowid;
          if p_errval=0 then
            fetch_and_update_by_rowid(rec_rowid, keystouse);
          end if;
        end loop;
      exception when others then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<3 then maxsev:=3; end if;
        p_errmsg:='Error fetching STGCSA.ACCACCELERATELOANTBL row(s) with key(s) '
         ||keystouse||' Oracle returned '||sqlerrm(sqlcode);
      end;
      if recnum=0 then
        p_errval:=0;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Warning No STGCSA.ACCACCELERATELOANTBL row(s) to update with key(s) '
          ||keystouse;
      end if;
    end if;
  else /* P_IDENTIFIER was unexpected value */
    p_errval:=-20002;
    if maxsev<3 then maxsev:=3; end if;
    p_errmsg:='Error P_IDENTIFIER contained unexpected value: '||p_identifier;
  end case;
  P_RETVAL:=RECNUM;
  -- post to log
  if p_errval=0 then
    runtime.logger(logged_msg_id,'STDLOG',101,
      'End ACCACCELERATELOANUPDTSP With 0 return, P_RETVAL='
      ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
      1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld
      ,PROGRAM_NAME=>'ACCACCELERATELOANUPDTSP');
  else
    ROLLBACK TO ACCACCELERATELOANUPDTSP;
    p_errmsg:=p_errmsg||' in ACCACCELERATELOANUPDTSP build 2018-11-07 11:32:00';
    runtime.logger(logged_msg_id,'STDERR',103,p_errmsg,p_userid,
      maxsev, l_usernm, l_loannmb,l_transid, l_dbmdl, l_entrydt, l_timestampfld,
      PROGRAM_NAME=>'ACCACCELERATELOANUPDTSP',force_log_entry=>true);
  end if;
EXCEPTION
WHEN OTHERS THEN
  ROLLBACK TO ACCACCELERATELOANUPDTSP;
  p_errmsg:=' Error outer exception handler caught Oracle error:'||SQLERRM(SQLCODE)
    ||' in ACCACCELERATELOANUPDTSP build 2018-11-07 11:32:00';
  runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
    3,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld,
    PROGRAM_NAME=>'ACCACCELERATELOANUPDTSP',force_log_entry=>true);
--
END ACCACCELERATELOANUPDTSP;
/


GRANT EXECUTE ON STGCSA.ACCACCELERATELOANUPDTSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.ACCACCELERATELOANUPDTSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.ACCACCELERATELOANUPDTSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.ACCACCELERATELOANUPDTSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.ACCACCELERATELOANUPDTSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.ACCACCELERATELOANUPDTSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.ACCACCELERATELOANUPDTSP TO STGCSADEVROLE;

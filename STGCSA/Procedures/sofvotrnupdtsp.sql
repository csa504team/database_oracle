DROP PROCEDURE STGCSA.SOFVOTRNUPDTSP;

CREATE OR REPLACE PROCEDURE STGCSA.SOFVOTRNUPDTSP(
    p_retval out number,
    p_errval out number,
    p_errmsg  out varchar2,
    p_identifier number:=0,
    p_userid varchar2
   ,p_LOANNMB CHAR:=null
   ,p_ONLNTRANSTRANSDT TIMESTAMP:=null
   ,p_ONLNTRANSTRMNLID CHAR:=null
   ,p_ONLNTRANSOPRTRINITIAL CHAR:=null
   ,p_ONLNTRANSFILENM CHAR:=null
   ,p_ONLNTRANSSCN CHAR:=null
   ,p_ONLNTRANSFIL10 CHAR:=null
   ,p_ONLNTRANSDETPYMTDT DATE:=null
   ,p_ONLNTRANSDTLREST CHAR:=null
   ,p_ONLNTRANSDTLREST5 CHAR:=null
) as
 /*
  Created on: 2018-11-07 11:36:16
  Created by: GENR
  Crerated from template stdtblupd_template v3.22 12 Oct 2018 on 2018-11-07 11:36:16
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  Procedure SOFVOTRNUPDTSP performs UPDATE on STGCSA.SOFVOTRNTBL
    for P_IDENTIFIER=0 qualification is on Primary Key: LOANNMB, ONLNTRANSTRANSDT, ONLNTRANSTRMNLID, ONLNTRANSOPRTRINITIAL
    Updates all columns except the qualification keys and columns for which a
      null value is passed.
    SPECIAL secret update kludge!
      if p_identifier is set to -1 then a flag is set to use all input parm values
      and p_identifier is changed to 0 (update by PK).
      THEREFORE if using p_identifier=-1 you MUST pass intended value for all columns.
      This option allows setting colulmns to null!
*/
  dbrec STGCSA.SOFVOTRNTBL%rowtype;
  logged_msg_id number;
  recnum number;
  rec_rowid rowid;
  keystouse varchar2(256);
  maxsev number:=0;
  pctsign char(1):='%';
  cur_colname varchar2(100):=null;
  cur_colvalue varchar2(4000);
  do_normal_field_checking char(1):='Y';
  overridden_p_identifier number;
  -- standard activity log capture vars
  l_LOANNMB char(10):=null;
  l_TRANSID number:=null;
  l_dbmdl varchar2(10):=null;
  l_entrydt date:=null;
  l_userid  varchar2(32):=null;
  l_usernm  varchar2(199):=null;
  l_timestampfld date:=null;
  crossover_not_active char(1);
procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
begin
  if p_errval=0 then
    begin
      select * into dbrec from STGCSA.SOFVOTRNTBL where rowid=rowid_in;
    exception when others then
      p_errval:=sqlcode;
      maxsev:=3;
      p_retval:=0;
      p_errmsg:='Fetch of STGCSA.SOFVOTRNTBL row by rowid '||rowid_in
        ||' orinally fetched using '||orig_keyset_values
        ||' failed with '||sqlerrm(sqlcode);
    end;
  end if;
  if p_errval=0 then
    -- set this as lastupdt
    DBrec.lastupdtuserid:=p_userid;
    DBrec.lastupdtdt:=sysdate;
    -- copy into DBrec all table column input parameters that are not null,
    -- saving column name/value if needed for error reporting
    --  (if p_identifier=-1 then move input even if null)
    begin
      if P_LOANNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_LOANNMB';
        cur_colvalue:=P_LOANNMB;
        DBrec.LOANNMB:=P_LOANNMB;
      end if;
      if P_ONLNTRANSTRANSDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_ONLNTRANSTRANSDT';
        cur_colvalue:=P_ONLNTRANSTRANSDT;
        DBrec.ONLNTRANSTRANSDT:=P_ONLNTRANSTRANSDT;
      end if;
      if P_ONLNTRANSTRMNLID is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_ONLNTRANSTRMNLID';
        cur_colvalue:=P_ONLNTRANSTRMNLID;
        DBrec.ONLNTRANSTRMNLID:=P_ONLNTRANSTRMNLID;
      end if;
      if P_ONLNTRANSOPRTRINITIAL is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_ONLNTRANSOPRTRINITIAL';
        cur_colvalue:=P_ONLNTRANSOPRTRINITIAL;
        DBrec.ONLNTRANSOPRTRINITIAL:=P_ONLNTRANSOPRTRINITIAL;
      end if;
      if P_ONLNTRANSFILENM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_ONLNTRANSFILENM';
        cur_colvalue:=P_ONLNTRANSFILENM;
        DBrec.ONLNTRANSFILENM:=P_ONLNTRANSFILENM;
      end if;
      if P_ONLNTRANSSCN is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_ONLNTRANSSCN';
        cur_colvalue:=P_ONLNTRANSSCN;
        DBrec.ONLNTRANSSCN:=P_ONLNTRANSSCN;
      end if;
      if P_ONLNTRANSFIL10 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_ONLNTRANSFIL10';
        cur_colvalue:=P_ONLNTRANSFIL10;
        DBrec.ONLNTRANSFIL10:=P_ONLNTRANSFIL10;
      end if;
      if P_ONLNTRANSDETPYMTDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_ONLNTRANSDETPYMTDT';
        cur_colvalue:=P_ONLNTRANSDETPYMTDT;
        DBrec.ONLNTRANSDETPYMTDT:=P_ONLNTRANSDETPYMTDT;
      end if;
      if P_ONLNTRANSDTLREST is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_ONLNTRANSDTLREST';
        cur_colvalue:=P_ONLNTRANSDTLREST;
        DBrec.ONLNTRANSDTLREST:=P_ONLNTRANSDTLREST;
      end if;
      if P_ONLNTRANSDTLREST5 is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_ONLNTRANSDTLREST5';
        cur_colvalue:=P_ONLNTRANSDTLREST5;
        DBrec.ONLNTRANSDTLREST5:=P_ONLNTRANSDTLREST5;
      end if;
    exception when others then
      p_errval:=sqlcode;
      if maxsev<3 then maxsev:=3; end if;
      p_errmsg:='Error using '||cur_colname||' Value >'||cur_colvalue||'<';
    end;
  end if;
  -- if all still ok do actual update
  if p_errval=0 then
    Begin
      update STGCSA.SOFVOTRNTBL
        set lastupdtuserid=DBrec.lastupdtuserid,
          lastupdtdt=DBrec.lastupdtdt
          ,LOANNMB=DBrec.LOANNMB
          ,ONLNTRANSTRANSDT=DBrec.ONLNTRANSTRANSDT
          ,ONLNTRANSTRMNLID=DBrec.ONLNTRANSTRMNLID
          ,ONLNTRANSOPRTRINITIAL=DBrec.ONLNTRANSOPRTRINITIAL
          ,ONLNTRANSFILENM=DBrec.ONLNTRANSFILENM
          ,ONLNTRANSSCN=DBrec.ONLNTRANSSCN
          ,ONLNTRANSFIL10=DBrec.ONLNTRANSFIL10
          ,ONLNTRANSDETPYMTDT=DBrec.ONLNTRANSDETPYMTDT
          ,ONLNTRANSDTLREST=DBrec.ONLNTRANSDTLREST
          ,ONLNTRANSDTLREST5=DBrec.ONLNTRANSDTLREST5
        where rowid=rowid_in;
      recnum:=recnum+1;
    exception when others then
      p_errval:=sqlcode;
      p_retval:=0;
      if maxsev<3 then maxsev:=3; end if;
      p_errmsg:= 'Error while doing update on STGCSA.SOFVOTRNTBL, on rec with rowid '
      ||rowid_in||' previously fetched using key: '||orig_keyset_values
      ||' got Oracle error '||sqlerrm(sqlcode);
    end;
  end if;
END;
begin
  -- setup
  savepoint SOFVOTRNUPDTSP;
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  recnum:=0;
  runtime.logger(logged_msg_id,'STDLOG',100,'Init SOFVOTRNUPDTSP',p_userid,4,
    logtxt1=>'SOFVOTRNUPDTSP',logtxt2=>'stdtblupd_template v3.22 12 Oct 2018',
    PROGRAM_NAME=>'SOFVOTRNUPDTSP');
  --
  l_LOANNMB:=P_LOANNMB;
  --
  -- Check to see if crossover is ongoing, in which case updates are prohibited
  SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
  IF crossover_not_active != 'Y' THEN
    p_errval := -20099;
    p_errmsg := 'Warning: Cannot update table SOFVOTRNTBL at this time because crossover is running, please try later.';
  END IF;
  -- end of crossover_lockout_check
--
-- special p_identifier kludge
--  if p_identifier is set to -1 then it treated as 0 (update by PK)
--  but a flag is set use passed value EVEN IF NULL.  Since for option -1
--  every column is updated to what was passed, intended values MUST be passed
--  for all columns.  This option allows setting colulmns to null!
  overridden_p_identifier:=p_identifier;
  if p_identifier=-1 then
    do_normal_field_checking:='N';
    overridden_p_identifier:=0;
  end if;
  case overridden_p_identifier
  when 0 then
    -- case to update one row based on primary key
    -- p_identifier=0 (choice 0) is default action for a stored procedure
    -- get existing version of row
    if p_errval=0 then
      keystouse:='(LOANNMB)=('||P_LOANNMB||')'||' and '||'(ONLNTRANSTRANSDT)=('||P_ONLNTRANSTRANSDT||')'||' and '||'(ONLNTRANSTRMNLID)=('||P_ONLNTRANSTRMNLID||')'||' and '||'(ONLNTRANSOPRTRINITIAL)=('||P_ONLNTRANSOPRTRINITIAL||')';
      begin
        select rowid into rec_rowid from STGCSA.SOFVOTRNTBL where 1=1
         and LOANNMB=P_LOANNMB         and ONLNTRANSTRANSDT=P_ONLNTRANSTRANSDT         and ONLNTRANSTRMNLID=P_ONLNTRANSTRMNLID         and ONLNTRANSOPRTRINITIAL=P_ONLNTRANSOPRTRINITIAL;
      exception when no_data_found then
        p_errval:=0;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Warning No STGCSA.SOFVOTRNTBL row to update with key(s) '
          ||keystouse;
      end;
    end if;
    if p_errval=0 then
      fetch_and_update_by_rowid(rec_rowid, keystouse);
    end if;
  else /* P_IDENTIFIER was unexpected value */
    p_errval:=-20002;
    if maxsev<3 then maxsev:=3; end if;
    p_errmsg:='Error P_IDENTIFIER contained unexpected value: '||p_identifier;
  end case;
  P_RETVAL:=RECNUM;
  -- post to log
  if p_errval=0 then
    runtime.logger(logged_msg_id,'STDLOG',101,
      'End SOFVOTRNUPDTSP With 0 return, P_RETVAL='
      ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
      1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld
      ,PROGRAM_NAME=>'SOFVOTRNUPDTSP');
  else
    ROLLBACK TO SOFVOTRNUPDTSP;
    p_errmsg:=p_errmsg||' in SOFVOTRNUPDTSP build 2018-11-07 11:36:16';
    runtime.logger(logged_msg_id,'STDERR',103,p_errmsg,p_userid,
      maxsev, l_usernm, l_loannmb,l_transid, l_dbmdl, l_entrydt, l_timestampfld,
      PROGRAM_NAME=>'SOFVOTRNUPDTSP',force_log_entry=>true);
  end if;
EXCEPTION
WHEN OTHERS THEN
  ROLLBACK TO SOFVOTRNUPDTSP;
  p_errmsg:=' Error outer exception handler caught Oracle error:'||SQLERRM(SQLCODE)
    ||' in SOFVOTRNUPDTSP build 2018-11-07 11:36:16';
  runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
    3,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld,
    PROGRAM_NAME=>'SOFVOTRNUPDTSP',force_log_entry=>true);
--
END SOFVOTRNUPDTSP;
/


GRANT EXECUTE ON STGCSA.SOFVOTRNUPDTSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.SOFVOTRNUPDTSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.SOFVOTRNUPDTSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.SOFVOTRNUPDTSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.SOFVOTRNUPDTSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.SOFVOTRNUPDTSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.SOFVOTRNUPDTSP TO STGCSADEVROLE;

DROP PROCEDURE STGCSA.TOGGLE_AUDITING_OFF;

CREATE OR REPLACE PROCEDURE STGCSA.toggle_auditing_off as
begin
  runtime.toggle_auditing_on_YN('N');
end;
/

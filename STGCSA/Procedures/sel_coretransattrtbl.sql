DROP PROCEDURE STGCSA.SEL_CORETRANSATTRTBL;

CREATE OR REPLACE PROCEDURE STGCSA.Sel_CoreTransAttrTbl (
 arg_TransID IN stgCSA.CoreTransAttrTbl.TransID%type,
 arg_AttrID IN stgCSA.CoreTransAttrTbl.AttrID%type,
 arg_Out_TransID OUT stgcsa.CoreTransAttrTbl.TransID%type

  )
AS
V_Out_TransID   stgcsa.CoreTransTbl.TransID%type;

BEGIN
/******************************************************************************
   NAME:       Sel_CoreTransTbl
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0       10/28/2017    Victor T     1.  Return TransID  From
                    stgCSA.CoreTransAttrTbl
     ******************************************************************************/

SELECT distinct TransID
INTO V_Out_TransID
FROM stgCSA.CoreTransAttrTbl
WHERE 0=0
AND TransID = arg_TransID
AND AttrID = arg_AttrID;

arg_Out_TransID := V_Out_TransID ;

EXCEPTION
 WHEN NO_DATA_FOUND THEN
    arg_Out_TransID := 0 ;

END Sel_CoreTransAttrTbl;
/


GRANT EXECUTE ON STGCSA.SEL_CORETRANSATTRTBL TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.SEL_CORETRANSATTRTBL TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.SEL_CORETRANSATTRTBL TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.SEL_CORETRANSATTRTBL TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.SEL_CORETRANSATTRTBL TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.SEL_CORETRANSATTRTBL TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.SEL_CORETRANSATTRTBL TO STGCSADEVROLE;

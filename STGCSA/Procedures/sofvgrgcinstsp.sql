DROP PROCEDURE STGCSA.SOFVGRGCINSTSP;

CREATE OR REPLACE PROCEDURE STGCSA.SOFVGRGCINSTSP(
    p_retval out number,
    p_errval out number,
    p_errmsg  out varchar2,
    p_identifier number:=0,
    p_userid varchar2
   ,p_CALNDRKEYDT DATE:=null
   ,p_CALNDRDAY CHAR:=null
   ,p_CALNDRHOLIDAYIND CHAR:=null
   ,p_CALNDRREMITTANCEIND CHAR:=null
   ,p_CALNDRCUTOFFIND CHAR:=null
   ,p_CALNDRSUSPROLLIND CHAR:=null
   ,p_CALNDRPAYOUTIND CHAR:=null
   ,p_CALNDRDESC VARCHAR2:=null
) as
 /*
  Created on: 2018-11-07 11:35:58
  Created by: GENR
  Crerated from template stdtblins_template v3.05 13 Sep 2018 on 2018-11-07 11:35:58
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  Procedure performs INSERT on STGCSA.SOFVGRGCTBLfor P_IDENTIFIER=0
*/
  rundate date;
  gn global_name.global_name%type;
  new_rec STGCSA.SOFVGRGCTBL%rowtype;
  logged_msgid_retval number;
  audretval number;
  crossover_active char(1);
  l_LOANNMB char(10):=null;
  l_TRANSID number:=null;
  l_dbmdl varchar2(10):=null;
  l_entrydt date:=null;
  l_userid  varchar2(32):=null;
  l_usernm  varchar2(199):=null;
  l_timestampfld date:=null;
  cur_col_name varchar2(100):=null;
  maxsev number:=0;
  holdstring varchar2(4000);
  keystouse varchar2(1000);
  crossover_not_active char(1);
  -- std date for column defaulting
  JAN_1_1900  constant date:=to_date('JAN-01-1900','MON-DD-YYYY');
  cur_colvalue varchar2(4095);
  cur_colname varchar2(30);
-- Main body begins here
begin
  -- setup
  savepoint SOFVGRGCINSTSP;
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  runtime.logger(logged_msgid_retval,'STDLOG',100,'Init SOFVGRGCINSTSP',p_userid,4,
    logtxt1=>'SOFVGRGCINSTSP',logtxt2=>'stdtblins_template v3.05 13 Sep 2018',
    PROGRAM_NAME=>'SOFVGRGCINSTSP');
  select global_name, sysdate into gn, rundate from global_name;
  --
  -- Check to see if crossover is ongoing, in which case updates are prohibited
  SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
  IF crossover_not_active != 'Y' THEN
    p_errval := -20099;
    p_errmsg := 'Warning: Cannot update table SOFVGRGCTBL at this time because crossover is running, please try later.';
  END IF;
  -- end of crossover_lockout_check
  new_rec.creatuserid:=p_userid;
  new_rec.creatdt:=sysdate;
  new_rec.lastupdtuserid:=p_userid;
  new_rec.lastupdtdt:=sysdate;
  if p_identifier=0 then
    -- p_identifier=0 this is default for a stored procedure, only action for insert
    runtime.errfound:=false;
    runtime.errstr:=' ';
    --
    -- Default columns as applicable
  if p_errval=0 then
    begin
      cur_colname:='P_CALNDRKEYDT';
      cur_colvalue:=P_CALNDRKEYDT;
      if P_CALNDRKEYDT is null then

        new_rec.CALNDRKEYDT:=jan_1_1900;

      else
        new_rec.CALNDRKEYDT:=P_CALNDRKEYDT;
      end if;
      cur_colname:='P_CALNDRDAY';
      cur_colvalue:=P_CALNDRDAY;
      if P_CALNDRDAY is null then

        new_rec.CALNDRDAY:=rpad(' ',3);

      else
        new_rec.CALNDRDAY:=P_CALNDRDAY;
      end if;
      cur_colname:='P_CALNDRHOLIDAYIND';
      cur_colvalue:=P_CALNDRHOLIDAYIND;
      if P_CALNDRHOLIDAYIND is null then

        new_rec.CALNDRHOLIDAYIND:=rpad(' ',1);

      else
        new_rec.CALNDRHOLIDAYIND:=P_CALNDRHOLIDAYIND;
      end if;
      cur_colname:='P_CALNDRREMITTANCEIND';
      cur_colvalue:=P_CALNDRREMITTANCEIND;
      if P_CALNDRREMITTANCEIND is null then

        new_rec.CALNDRREMITTANCEIND:=rpad(' ',1);

      else
        new_rec.CALNDRREMITTANCEIND:=P_CALNDRREMITTANCEIND;
      end if;
      cur_colname:='P_CALNDRCUTOFFIND';
      cur_colvalue:=P_CALNDRCUTOFFIND;
      if P_CALNDRCUTOFFIND is null then

        new_rec.CALNDRCUTOFFIND:=rpad(' ',1);

      else
        new_rec.CALNDRCUTOFFIND:=P_CALNDRCUTOFFIND;
      end if;
      cur_colname:='P_CALNDRSUSPROLLIND';
      cur_colvalue:=P_CALNDRSUSPROLLIND;
      if P_CALNDRSUSPROLLIND is null then

        new_rec.CALNDRSUSPROLLIND:=rpad(' ',1);

      else
        new_rec.CALNDRSUSPROLLIND:=P_CALNDRSUSPROLLIND;
      end if;
      cur_colname:='P_CALNDRPAYOUTIND';
      cur_colvalue:=P_CALNDRPAYOUTIND;
      if P_CALNDRPAYOUTIND is null then

        new_rec.CALNDRPAYOUTIND:=rpad(' ',1);

      else
        new_rec.CALNDRPAYOUTIND:=P_CALNDRPAYOUTIND;
      end if;
      cur_colname:='P_CALNDRDESC';
      cur_colvalue:=P_CALNDRDESC;
      if P_CALNDRDESC is null then

        new_rec.CALNDRDESC:=' ';

      else
        new_rec.CALNDRDESC:=P_CALNDRDESC;
      end if;
    exception when others then
       p_errval:=sqlcode;
       p_errmsg:='Error - Oracle returned '||sqlerrm(sqlcode)||' using '
           ||cur_colname||' Value >'||cur_colvalue||'<';
    end;
  end if;
    keystouse:='(CALNDRKEYDT)=('||new_rec.CALNDRKEYDT||')';
    if p_errval=0 then
      begin
        insert into STGCSA.SOFVGRGCTBL values new_rec;
        p_RETVAL := SQL%ROWCOUNT;
      exception
      when dup_val_on_index then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Insert caused dupe on unique IX: '||sqlerrm(sqlcode);
      when others then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<3 then maxsev:=3; end if;
        p_errmsg:='Inserting record caught SQL error '||sqlerrm(sqlcode);
      end;
    end if;
  else /* P_IDENTIFIER was unexpected value */
    p_errval:=-20002;
    if maxsev<3 then maxsev:=3; end if;
    p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier
      ||' inserting record with key '||keystouse;
  end if;
  if p_errval=0 then
    runtime.logger(logged_msgid_retval,'STDLOG',101,
      'End SOFVGRGCINSTSP With 0 return, P_RETVAL='
      ||p_retval||' Record Key '||keystouse,p_userid,
      1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt,
      l_timestampfld,PROGRAM_NAME=>'SOFVGRGCINSTSP');
  else
    p_errmsg:='Error: '||P_ERRMSG||' in SOFVGRGCINSTSP Record Key '||keystouse
      ||' Build: 2018-11-07 11:35:58';
    ROLLBACK TO SOFVGRGCINSTSP;
    runtime.logger(logged_msgid_retval,'STDERR',103,
      p_errmsg,p_userid,maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,
      l_entrydt, l_timestampfld,PROGRAM_NAME=>'SOFVGRGCINSTSP',force_log_entry=>TRUE);
  end if;
EXCEPTION
WHEN OTHERS THEN
  p_errval:=SQLCODE;
  p_errmsg:=' In outer exception handler in SOFVGRGCINSTSP caught exception '
    ||sqlerrm(sqlcode)||' Record Key '||keystouse||' build 2018-11-07 11:35:58';
  ROLLBACK TO SOFVGRGCINSTSP;
  runtime.logger(logged_msgid_retval,'STDERR',20,p_errmsg,p_userid,3,
  PROGRAM_NAME=>'SOFVGRGCINSTSP',force_log_entry=>TRUE);
--
END SOFVGRGCINSTSP;
/


GRANT EXECUTE ON STGCSA.SOFVGRGCINSTSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.SOFVGRGCINSTSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.SOFVGRGCINSTSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.SOFVGRGCINSTSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.SOFVGRGCINSTSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.SOFVGRGCINSTSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.SOFVGRGCINSTSP TO STGCSADEVROLE;

DROP PROCEDURE STGCSA.REFRESHPRPLOANRQSTBYLOANNMBCSP;

CREATE OR REPLACE PROCEDURE STGCSA.refreshPRPLoanRqstByLoanNmbCSP(
    p_retval out number,
    p_errval out number,
    p_errmsg out varchar2,
    p_userid varchar2,
    p_LOANNMB CHAR:=null

) as
/*******************************************************************
  This is CF include function refreshPRPLoanRqstByLoanNmb converted to PL/SQL.
  John Low, 21 Feb 2018
  Following comments from original CF source: 

<!---

	AUTHOR:				IAI for the US Small Business Administration.
	DATE:				12/08/2017.
	DESCRIPTION:		form submission for a new address, ACH.
	NOTES:				Include file for form submission for a new address, ACH.
	INPUT:				None.
	OUTPUT:				form submission for a new address, ACH.
	REVISION HISTORY:	12/08/2017, IAI: UAT ready

--->

	Written October 28, 2017
    When a wire is statused in Payment Posting (PP) to 'Pending Prepay' the system will capture data from CDCONLINE
    and recalculate the prepayment financials for validation prior to performing any fund transfers.

	The result of this function will be a new record in stgCSA.PRPLOANRQSTTBL populated with all values to be 
	validated in Prepayments
*/
  pgm varchar2(80):='REFRESHPRPLOANRQSTBYLOANNMBCSP';
  ver varchar2(80):='V1.1 22 Feb 2018';
  rowcnt   number;
  cur_stmt number; 
  /* cur_stmt is used to assign a unique number to each stmt, so if we wind up
     in an exception handler we know which stmt was being executed */
-- vars for rtn values from queries
  a_WKPREPAYAMT CDCONLINE.LOANSAVETBL.WKPREPAYAMT%type;
  a_RQSTREF     CDCONLINE.LOANSAVETBL.RQSTREF%type;   
  a_CREATDT     CDCONLINE.LOANSAVETBL.CREATDT%type;  
  a_POSTINGDT   stgCSA.PRPLOANRQSTTBL.POSTINGDT%type;  
  logentryid number;
  PAYMENTS_count number;
  sum_GNTYGNTYAMT_Total number;
  sum_gntygntyamt number;
Begin     
  Savepoint refreshPRPLoanRqstByLoanNmbCSP;     
  p_retval:=0;
  p_errval:=0;
  p_errmsg:='';
     
  runtime_logger(logentryid, 'CSPMSG',800,pgm||' '||ver||' started.  p_loannmb='
    ||p_loannmb,  p_userid,1);     
     
 --    <cfargument name="loanNmb" required="yes">

      cur_stmt:=101;
	    DELETE FROM stgCSA.PRPLOANRQSTTBL
	    WHERE LOANNMB = p_loannmb;

      cur_stmt:=102;
	    DELETE FROM stgCSA.PRPMFDUEFROMBORRTBL
	    WHERE LOANNMB = p_loannmb;

      cur_stmt:=103;
	    DELETE FROM stgCSA.PRPMFGNTYTBL
	    WHERE LOANNMB = p_loannmb;

      cur_stmt:=104;
	    DELETE FROM stgCSA.TEMPGETOGPI6MOTBL
	    WHERE LOANNMB = p_loannmb;

      cur_stmt:=105;
	    DELETE FROM stgCSA.TEMPGETDTTBL
	    WHERE LOANNMB = p_loannmb;

      cur_stmt:=106;
	    DELETE FROM stgCSA.TEMPGETOGERRRAWTBL
	    WHERE LOANNMB = p_loannmb;

      cur_stmt:=107;
	    DELETE FROM stgCSA.TEMPGETOGERRTBL
	    WHERE LOANNMB = p_loannmb;

-- <!---Guarantee info for 'MF Data' control in original app
--		moved here to centralize prepayment calculations  --->

      cur_stmt:=108;
	    INSERT INTO stgCSA.PRPMFGNTYTBL
	    (
	        LOANNMB, OPENGNTYDT, OPENGNTYTYP, GNTYAMT, STATCD, GNTYCLS, PRPCMNT,
	        OPENGNTYDTCONVERTED,
	        CREATUSERID, CREATDT, LASTUPDTUSERID, LASTUPDTDT
	    )
	    SELECT SOFVGNTY.LOANNMB,
	        SOFVGNTY.GNTYDT,
	        SOFVGNTY.GNTYTYP,
	        NVL(SOFVGNTY.GNTYGNTYAMT, 0),
	        SOFVGNTY.GNTYSTATCD,
	        SOFVGNTY.GNTYCLSDT,
	        SOFVGNTY.GNTYCMNT,
	        SOFVGNTY.GNTYDT,
	        '#variables.userID#',
	        SYSTIMESTAMP,
	        '#variables.userID#',
	        SYSTIMESTAMP
	    FROM
	        (SELECT *
	        FROM stgCSA.SOFVGNTYTBL
	        ) SOFVGNTY
	    INNER JOIN
		  (SELECT SOFVGNTY.LOANNMB,
		    SOFVGNTY.GNTYSTATCD,
		    SOFVGNTY.GNTYTYP
		  FROM
		    (SELECT * FROM stgCSA.SOFVGNTYTBL
		    ) SOFVGNTY
		  WHERE SOFVGNTY.GNTYSTATCD = '1'
		  AND SOFVGNTY.LOANNMB      = p_loannmb
		  AND SOFVGNTY.GNTYTYP      = '1'
		  GROUP BY SOFVGNTY.LOANNMB,
		    SOFVGNTY.GNTYSTATCD,
		    SOFVGNTY.GNTYTYP
		  ) TYPE1
		ON SOFVGNTY.LOANNMB    = TYPE1.LOANNMB
		AND SOFVGNTY.GNTYSTATCD   = TYPE1.GNTYSTATCD
		AND SOFVGNTY.GNTYTYP   = TYPE1.GNTYTYP
		WHERE SOFVGNTY.LOANNMB = p_loannmb;

    cur_stmt:=109;
	  INSERT INTO stgCSA.PRPMFGNTYTBL
	    (
	        LOANNMB, OPENGNTYDT, OPENGNTYTYP, GNTYAMT, STATCD, GNTYCLS, PRPCMNT,
	        --SEMIANDT, PREPAYDT,
	        OPENGNTYDTCONVERTED,
	        --OPENGNTYDTPREPAYDTDIFF, GNTYIND, SEMIANDTCONVERTED, TIMESTAMPFLD,
	        CREATUSERID, CREATDT, LASTUPDTUSERID, LASTUPDTDT
	    )
	    SELECT SOFVGNTY.LOANNMB,
	        SOFVGNTY.GNTYDT,
	        SOFVGNTY.GNTYTYP,
	        NVL(SOFVGNTY.GNTYGNTYAMT, 0),
	        SOFVGNTY.GNTYSTATCD,
	        SOFVGNTY.GNTYCLSDT,
	        SOFVGNTY.GNTYCMNT,
	        SOFVGNTY.GNTYDT,
	        p_userid,
	        SYSTIMESTAMP,
	        p_userid,
	        SYSTIMESTAMP
	    FROM
	        (SELECT *
	        FROM stgCSA.SOFVGNTYTBL
	        ) SOFVGNTY
	        INNER JOIN
		  (SELECT SOFVGNTY.LOANNMB,
		    SOFVGNTY.GNTYSTATCD,
		    SOFVGNTY.GNTYTYP
		  FROM
		    (SELECT * FROM stgCSA.SOFVGNTYTBL
		    ) SOFVGNTY
		  WHERE SOFVGNTY.GNTYSTATCD = '1'
		  AND SOFVGNTY.LOANNMB      = p_loannmb
		  AND SOFVGNTY.GNTYTYP      = '2'
		  GROUP BY SOFVGNTY.LOANNMB,
		    SOFVGNTY.GNTYSTATCD,
		    SOFVGNTY.GNTYTYP
		  ) TYPE1
		ON SOFVGNTY.LOANNMB    = TYPE1.LOANNMB
		AND SOFVGNTY.GNTYSTATCD   = TYPE1.GNTYSTATCD
		AND SOFVGNTY.GNTYTYP   = TYPE1.GNTYTYP
		WHERE SOFVGNTY.LOANNMB = p_loannmb;

      cur_stmt:=110;
	    INSERT INTO stgCSA.PRPLOANRQSTTBL (
	        LOANNMB,
	        RQSTCD,
	        RQSTREF,
	        PREPAYDT,
	        PREPAYMO,
	        PREPAYYRNMB,
	        SEMIANNDT,
	        CREATUSERID,
	        CREATDT,
	        LASTUPDTUSERID,
	        LASTUPDTDT,
	        CDCSBAFEEDISBAMT, --Not calculated here - field set to NOT NULL so 0 for now
	        CDCREGNCD,
	        CDCNMB,
	        LOANSTAT,
	        --LOANSTATCD,
	        AMORTCDCAMT,
	        BORRNM
	    )
	    SELECT
	        REQUESTS.LOANNMB,
	        REQUESTS.RQSTCD,
	        REQUESTS.RQSTREF,
	        PREPAYDT,
	        to_char( PREPAYDT, 'mm' )  AS PREPAYMO,
	        to_char( PREPAYDT, 'yyyy' )  AS PREPAYYRNMB,
	        SEMIANNDT,
	        p_userid,
	        SYSTIMESTAMP,
	        p_loannmb,
	        SYSTIMESTAMP,
	        0,
	        lt.CDCREGNCD,
	        lt.CDCNMB,
	        lt.LOANSTATCD,
	        --SUBSTR(lt.LOANSTATCD,0,2),
	        lt.AMORTCDCAMT,
	        lt.BORRNM
	    FROM 	(
	    		SELECT REQUESTS4.LOANNMB,
	                    REQUESTS4.RQSTREF,
	                    REQUESTS4.RQSTCD
	             FROM   CDCONLINE.RQSTTBL REQUESTS4
	             JOIN 	(
	             			SELECT REQUESTS4_PPDATE.LOANNMB, 
	             			Max(REQUESTS4_PPDATE.PREPAYDT) AS PREPAYDT
	                    	FROM   	CDCONLINE.RQSTTBL REQUESTS4_PPDATE
	                    	WHERE	REQUESTS4_PPDATE.RQSTCD = 4 
	                    	  AND REQUESTS4_PPDATE.cdcportflstatcdcd = 'CL'
	                    	GROUP BY REQUESTS4_PPDATE.LOANNMB
	        			) REQUESTS4_PPDATE_MAX
	                ON REQUESTS4.LOANNMB = 
	                  REQUESTS4_PPDATE_MAX.LOANNMB 
	                AND REQUESTS4.PREPAYDT = REQUESTS4_PPDATE_MAX.PREPAYDT
				WHERE	REQUESTS4.RQSTCD = 4 AND REQUESTS4.cdcportflstatcdcd = 'CL'
	            UNION
	            SELECT 	REQUESTS9.LOANNMB, REQUESTS9.RQSTREF, REQUESTS9.RQSTCD
				FROM   	(CDCONLINE.RQSTTBL REQUESTS9
				JOIN 	(
							SELECT REQUESTS9_PPDATE.LOANNMB,  
							  Max(REQUESTS9_PPDATE.PREPAYDT) AS PREPAYDT
	            FROM CDCONLINE.RQSTTBL REQUESTS9_PPDATE
	            			WHERE	REQUESTS9_PPDATE.RQSTCD = 9 
	            			  AND REQUESTS9_PPDATE.cdcportflstatcdcd = 'CL'
	            			GROUP BY REQUESTS9_PPDATE.LOANNMB
	            		) REQUESTS9_PPDATE_MAX
	                  ON ( REQUESTS9.PREPAYDT = REQUESTS9_PPDATE_MAX.PREPAYDT ) 
	                    AND ( REQUESTS9.LOANNMB = REQUESTS9_PPDATE_MAX.LOANNMB )
				)
		LEFT JOIN (
				SELECT	REQUESTS4.LOANNMB, REQUESTS4.RQSTREF, REQUESTS4.RQSTCD
	            FROM	CDCONLINE.RQSTTBL  REQUESTS4
	            JOIN	(
	            		SELECT REQUESTS4_PPDATE.LOANNMB, Max(REQUESTS4_PPDATE.PREPAYDT) AS PREPAYDT
	                    FROM   CDCONLINE.RQSTTBL REQUESTS4_PPDATE
                            WHERE  REQUESTS4_PPDATE.RQSTCD = 4
                                    AND REQUESTS4_PPDATE.cdcportflstatcdcd = 'CL'
                            GROUP  BY REQUESTS4_PPDATE.LOANNMB)
                            REQUESTS4_PPDATE_MAX
                        ON REQUESTS4.LOANNMB =
                            REQUESTS4_PPDATE_MAX.LOANNMB
                            AND REQUESTS4.PREPAYDT =
                                REQUESTS4_PPDATE_MAX.PREPAYDT
                          WHERE  REQUESTS4.RQSTCD = 4
                                  AND REQUESTS4.cdcportflstatcdcd = 'CL') REQUESTS4
                      ON REQUESTS9.LOANNMB = REQUESTS4.LOANNMB
	                WHERE  REQUESTS9.RQSTCD = 9
	                    AND REQUESTS9.cdcportflstatcdcd = 'CL'
	                    AND REQUESTS4.RQSTREF IS NULL
	    ) REQUESTS
	    INNER JOIN CDCONLINE.RQSTTBL
	        ON REQUESTS.RQSTREF = CDCONLINE.RQSTTBL.RQSTREF
	    INNER JOIN cdconline.LOANTBL LT
	        ON lt.LOANNMB = REQUESTS.LOANNMB

	    WHERE REQUESTS.LOANNMB = p_loannmb;

--	<!--- $$$ Add Bobs code here when available --->
-- <!---New code added to handle no records in CDCONLINE.RQSTTBL--->
      cur_stmt:=178;
      SELECT count(*) into rowcnt FROM stgCSA.PRPLOANRQSTTBL
        WHERE LOANNMB = P_LOANNMB;
--       <cfif checkPRPLOANRQSTTBLrecordQ.RecordCount EQ 0 >
      if rowcnt=0 then
         cur_stmt:=179;
         INSERT INTO stgCSA.PRPLOANRQSTTBL (
             LOANNMB,
             RQSTCD,
             RQSTREF,
             PREPAYDT,
             PREPAYMO,
             PREPAYYRNMB,
             SEMIANNDT,
             CREATUSERID,
             CREATDT,
             LASTUPDTUSERID,
             LASTUPDTDT,
             CDCSBAFEEDISBAMT,
             CDCREGNCD,
             CDCNMB,
             LOANSTAT,
             AMORTCDCAMT,
             BORRNM
         )
         SELECT
            p_LOANNMB,
             0 AS RQSTCD,
             0 AS RQSTREF,
             '01-JAN-00' AS PREPAYDT,
             0 AS PREPAYMO,
             0 AS PREPAYYRNMB,
             '01-JAN-00' AS SEMIANNDT,
             p_userid AS CREATUSERID,
             SYSTIMESTAMP AS CREATDT,
             p_userid AS LASTUPDTUSERID,
             SYSTIMESTAMP AS LASTUPDTDT,
             0 AS CDCSBAFEEDISBAMT,
             '00' AS CDCREGNCD,
             '000' AS CDCNMB,
             '00' AS LOANSTAT,
             0 AS AMORTCDCAMT,
             0 AS BORRNM
         FROM DUAL;
      end if;           
--       </cfif>
-- <!---End of new code added to handle no records in CDCONLINE.RQSTTBL--->
 	
      cur_stmt:=111;
	    UPDATE stgCSA.PRPLOANRQSTTBL
	    SET
	        AMORTCDCAMT = NVL(AMORTCDCAMT, 0),
	        AMORTCSAAMT = NVL(AMORTCSAAMT, 0),
	        AMORTSBAAMT = NVL(AMORTSBAAMT, 0),
	        BALAMT = NVL(BALAMT, 0),
	        BALCALC1AMT = NVL(BALCALC1AMT, 0),
	        BALCALC2AMT = NVL(BALCALC2AMT, 0),
	        BALCALC3AMT = NVL(BALCALC3AMT, 0),
	        BALCALC4AMT = NVL(BALCALC4AMT, 0),
	        BALCALC5AMT = NVL(BALCALC5AMT, 0),
	        BALCALC6AMT = NVL(BALCALC6AMT, 0),
	        BALCALCAMT = NVL(BALCALCAMT, 0),
	        BEHINDAMT = NVL(BEHINDAMT, 0),
	        --SBABEHINDAMT = NVL(SBABEHINDAMT, 0),
	        BORRFEEAMT = NVL(BORRFEEAMT, 0),
	        CDCBEHINDAMT = NVL(CDCBEHINDAMT, 0),
	        CDCDUEFROMBORRAMT = NVL(CDCDUEFROMBORRAMT, 0),
	        CDCFEECALCONLYAMT = NVL(CDCFEECALCONLYAMT, 0),
	        CDCFEEDISBAMT = NVL(CDCFEEDISBAMT, 0),
	        CDCFEENEEDEDAMT = NVL(CDCFEENEEDEDAMT, 0),
	        CDCPCT = NVL(CDCPCT, 0),
	        CSABEHINDAMT = NVL(CSABEHINDAMT, 0),
	        CSADUEFROMBORRAMT = NVL(CSADUEFROMBORRAMT, 0),
	        CSAFEECALCAMT = NVL(CSAFEECALCAMT, 0),
	        CSAFEECALCONLYAMT = NVL(CSAFEECALCONLYAMT, 0),
	        CSAFEENEEDEDAMT = NVL(CSAFEENEEDEDAMT, 0),
	        CURRFEEBALAMT = NVL(CURRFEEBALAMT, 0),
	        DBENTRBALAMT = NVL(DBENTRBALAMT, 0),
	        DUEFROMBORRAMT = NVL(DUEFROMBORRAMT, 0),
	        --SBADUEFROMBORRAMT = NVL(SBADUEFROMBORRAMT, 0),
	        ESCROWAMT = NVL(ESCROWAMT, 0),
	        FEECALCAMT = NVL(FEECALCAMT, 0),
	        FEECALCONLYAMT = NVL(FEECALCONLYAMT, 0),
	        FEENEEDEDAMT = NVL(FEENEEDEDAMT, 0),
	        --SBAFEECALCAMT = NVL(SBAFEECALCAMT, 0),
	        --SBAFEECALCONLYAMT = NVL(SBAFEECALCONLYAMT, 0),
	        --SBAFEENEEDEDAMT = NVL(SBAFEENEEDEDAMT, 0),
	        FIVEYRADJ = NVL(FIVEYRADJ, 0),
	        FIVEYRADJCDCAMT = NVL(FIVEYRADJCDCAMT, 0),
	        FIVEYRADJCSAAMT = NVL(FIVEYRADJCSAAMT, 0),
	        FIVEYRADJSBAAMT = NVL(FIVEYRADJSBAAMT, 0),
	        GFDAMT = NVL(GFDAMT, 0),
	        INT2AMT = NVL(INT2AMT, 0),
	        INT3AMT = NVL(INT3AMT, 0),
	        INT4AMT = NVL(INT4AMT, 0),
	        INT5AMT = NVL(INT5AMT, 0),
	        INT6AMT = NVL(INT6AMT, 0),
	        INTAMT = NVL(INTAMT, 0),
	        INTNEEDEDAMT = NVL(INTNEEDEDAMT, 0),
	        LATEFEENEEDEDAMT = NVL(LATEFEENEEDEDAMT, 0),
	        MODELNQ = NVL(MODELNQ, 0),
	        NOTEBALCALCAMT = NVL(NOTEBALCALCAMT, 0),
	        NOTERTPCT = NVL(NOTERTPCT, 0),
	        OPENGNTYAMT = NVL(OPENGNTYAMT, 0),
	        OPENGNTYERRAMT = NVL(OPENGNTYERRAMT, 0),
	        OPENGNTYREGAMT = NVL(OPENGNTYREGAMT, 0),
	        OPENGNTYUNALLOCAMT = NVL(OPENGNTYUNALLOCAMT, 0),
	        PI6MOAMT = NVL(PI6MOAMT, 0),
	        PIAMT = NVL(PIAMT, 0),
	        PIPROJAMT = NVL(PIPROJAMT, 0),
	        POSTINGMO = NVL(POSTINGMO, 0),
	        PREPAYAMT = NVL(PREPAYAMT, 0),
	        PREPAYPCT = NVL(PREPAYPCT, 0),
	        PREPAYPREMAMT = NVL(PREPAYPREMAMT, 0),
	        PRIN2AMT = NVL(PRIN2AMT, 0),
	        PRIN3AMT = NVL(PRIN3AMT, 0),
	        PRIN4AMT = NVL(PRIN4AMT, 0),
	        PRIN5AMT = NVL(PRIN5AMT, 0),
	        PRIN6AMT = NVL(PRIN6AMT, 0),
	        PRINAMT = NVL(PRINAMT, 0),
	        PRINCURAMT = NVL(PRINCURAMT, 0),
	        PRINNEEDEDAMT = NVL(PRINNEEDEDAMT, 0),
	        PRINPROJAMT = NVL(PRINPROJAMT, 0),
	        SEMIANMO = NVL(SEMIANMO, 0),
	        SEMIANNAMT = NVL(SEMIANNAMT, 0),
	        SEMIANNPYMTAMT = NVL(SEMIANNPYMTAMT, 0),
	        UNALLOCAMT = NVL(UNALLOCAMT, 0)
	    WHERE LOANNMB = p_loannmb;

      cur_stmt:=112;
	    UPDATE stgCSA.PRPLOANRQSTTBL t1
	    SET
	    (
	        t1.APPVDT
	    ) =
	    (
	    SELECT LOANDTLAPPVDT
	    FROM stgCSA.SOFVLND1TBL
	    WHERE LOANNMB = p_loannmb
	    )
	    WHERE LOANNMB = p_loannmb;

      cur_stmt:=113;
	    INSERT INTO stgCSA.PRPMFDUEFROMBORRTBL
	    (
	        LOANNMB,
	        APPRDT,
	        CREATUSERID,
	        CREATDT,
	        LASTUPDTUSERID,
	        LASTUPDTDT
	    )
	    SELECT
	        p_loannmb,
	        t1.APPVDT,
	        p_userid,
	        SYSTIMESTAMP,
	        p_userid,
	        SYSTIMESTAMP
	    FROM stgCSA.PRPLOANRQSTTBL t1
	    WHERE t1.LOANNMB = p_loannmb;

      cur_stmt:=114;
	    UPDATE stgCSA.PRPMFDUEFROMBORRTBL t1
	    SET
	    (
	        t1.MATRDT
	    ) =
	    (
	    SELECT LOANDTLDBENTRMATDT
	    FROM stgCSA.SOFVLND1TBL
	    WHERE LOANNMB = p_loannmb
	    )
	    WHERE LOANNMB = p_loannmb;

      cur_stmt:=115;
	    UPDATE STGCSA.PRPMFDUEFROMBORRTBL t1
	    SET
	        t1.CDCDUEFROMBORRAMT = NVL((
	                            SELECT SUM(DUEFROMBORRDOLLRAMT) AS DFBAMT
	                            FROM
	                            (SELECT LOANNMB,
	                                DUEFROMBORRRECRDTYP,
	                                DUEFROMBORRDOLLRAMT,
	                                DUEFROMBORRRECRDSTATCD
	                            FROM STGCSA.SOFVDUEBTBL
	                            WHERE LOANNMB = p_loannmb
	                            ) SOFVDATA
	                            WHERE DUEFROMBORRRECRDSTATCD = 1
	                            GROUP BY LOANNMB,
	                            DUEFROMBORRRECRDTYP,
	                            DUEFROMBORRRECRDSTATCD
	                            HAVING DUEFROMBORRRECRDTYP = 2
	                            AND LOANNMB = p_loannmb
	                            ), 0),
	        t1.DUEFROMBORRAMT = NVL((
	                            SELECT SUM(DUEFROMBORRDOLLRAMT) AS DFBAMT
	                            FROM
	                            (SELECT LOANNMB,
	                                DUEFROMBORRRECRDTYP,
	                                DUEFROMBORRDOLLRAMT,
	                                DUEFROMBORRRECRDSTATCD
	                            FROM STGCSA.SOFVDUEBTBL
	                            WHERE LOANNMB = p_loannmb
	                            ) SOFVDATA
	                            WHERE DUEFROMBORRRECRDSTATCD = 1
	                            GROUP BY LOANNMB,
	                            DUEFROMBORRRECRDTYP,
	                            DUEFROMBORRRECRDSTATCD
	                            HAVING DUEFROMBORRRECRDTYP = 7
	                            AND LOANNMB = p_loannmb
	                            ), 0),
	        t1.CSADUEFROMBORRAMT = NVL((
	                            SELECT SUM(DUEFROMBORRDOLLRAMT) AS DFBAMT
	                            FROM
	                            (SELECT LOANNMB,
	                                DUEFROMBORRRECRDTYP,
	                                DUEFROMBORRDOLLRAMT,
	                                DUEFROMBORRRECRDSTATCD
	                            FROM STGCSA.SOFVDUEBTBL
	                            WHERE LOANNMB = p_loannmb
	                            ) SOFVDATA
	                            WHERE DUEFROMBORRRECRDSTATCD = 1
	                            GROUP BY LOANNMB,
	                            DUEFROMBORRRECRDTYP,
	                            DUEFROMBORRRECRDSTATCD
	                            HAVING DUEFROMBORRRECRDTYP = 1
	                            AND LOANNMB = p_loannmb
	                            ), 0),
	        t1.CURRFEEBALAMT = NVL((
	                            SELECT LOANDTLCURFEEBALAMT FROM STGCSA.SOFVLND1TBL 
	                            WHERE LOANNMB = p_loannmb
	                            ), 0),
	        t1.LATEFEEAMT = NVL((
	                            SELECT SUM(DUEFROMBORRDOLLRAMT) AS DFBAMT
	                            FROM
	                            (SELECT LOANNMB,
	                                DUEFROMBORRRECRDTYP,
	                                DUEFROMBORRDOLLRAMT,
	                                DUEFROMBORRRECRDSTATCD
	                            FROM STGCSA.SOFVDUEBTBL
	                            WHERE LOANNMB = p_loannmb
	                            ) SOFVDATA
	                            WHERE DUEFROMBORRRECRDSTATCD = 1
	                            GROUP BY LOANNMB,
	                            DUEFROMBORRRECRDTYP,
	                            DUEFROMBORRRECRDSTATCD
	                            HAVING DUEFROMBORRRECRDTYP = 3
	                            AND LOANNMB = p_loannmb
	                            ), 0)
	    WHERE LOANNMB = p_loannmb;

      cur_stmt:=116;
	    UPDATE stgCSA.PRPMFGNTYTBL t2
	    SET
	    (
	      t2.PREPAYDT, t2.SEMIANDT
	    ) =
	    (
	    SELECT
	      t1.PREPAYDT,
	      t1.SEMIANNDT
	    FROM stgCSA.PRPLOANRQSTTBL t1
	    WHERE t1.LOANNMB = p_loannmb
	    )
	    WHERE LOANNMB = p_loannmb
	    AND OPENGNTYTYP = '1';

      cur_stmt:=117;
	    UPDATE stgCSA.PRPMFGNTYTBL t2
	    SET
	    (
	      t2.PREPAYDT, t2.SEMIANDT
	    ) =
	    (
	    SELECT
	      t1.PREPAYDT,
	      t1.SEMIANNDT
	    FROM stgCSA.PRPLOANRQSTTBL t1
	    WHERE t1.LOANNMB = p_loannmb
	    )
	    WHERE LOANNMB = p_loannmb
	    AND OPENGNTYTYP = '2';

      cur_stmt:=118;
	    UPDATE stgCSA.PRPMFGNTYTBL t1
	    SET OPENGNTYDTPREPAYDTDIFF = OPENGNTYDT - PREPAYDT
	    WHERE LOANNMB = p_loannmb;


      cur_stmt:=119;
	    UPDATE stgCSA.PRPLOANRQSTTBL t1
	    SET
	        t1.ACTUALRQST = 
            CASE WHEN RQSTCD=4
                AND EXTRACT(month FROM SYSTIMESTAMP ) = EXTRACT(month FROM t1.PREPAYDT)
                AND EXTRACT(year FROM SYSTIMESTAMP ) = EXTRACT(year FROM t1.PREPAYDT)
              THEN 'Yes'
              ELSE 'No' END,
	        t1.CSAPAIDTHRUDT =  
	          CASE WHEN EXTRACT(month FROM ADD_MONTHS(t1.SEMIANNDT, -1)) = 2
	            THEN EXTRACT(year FROM t1.SEMIANNDT) || '/02/30'
	            ELSE EXTRACT(year FROM ADD_MONTHS(t1.SEMIANNDT, -1)) || '/' 
	              || SUBSTR('0' 
	              || to_char(EXTRACT(month FROM ADD_MONTHS(t1.SEMIANNDT, -1))), -2) 
	              || '/' || '30'
	            END,
	        t1.BORRPAIDTHRUDT = 
	          CASE WHEN EXTRACT(month FROM ADD_MONTHS(t1.SEMIANNDT, -1)) = 2
	            THEN EXTRACT(year FROM t1.SEMIANNDT) || '/02/30'
	            ELSE EXTRACT(year FROM ADD_MONTHS(t1.SEMIANNDT, -1)) || '/' 
	              || SUBSTR('0' 
	              || to_char(EXTRACT(month FROM ADD_MONTHS(t1.SEMIANNDT, -1))), -2) 
	              || '/' || '30'
	            END,
	        t1.INTPAIDTHRUDT =  
	          CASE WHEN EXTRACT(month FROM ADD_MONTHS(t1.SEMIANNDT, -1)) = 2
	            THEN EXTRACT(year FROM t1.SEMIANNDT) || '/02/30'
	            ELSE EXTRACT(year FROM ADD_MONTHS(t1.SEMIANNDT, -1)) || '/' 
	              || SUBSTR('0' 
	              || to_char(EXTRACT(month FROM ADD_MONTHS(t1.SEMIANNDT, -1))), -2) 
	              || '/' || '30'
	          END
	    WHERE LOANNMB = p_loannmb;

--<!--- PYMTHISTRYTBL works with out a record --->
      cur_stmt:=120;
	    UPDATE stgCSA.PRPLOANRQSTTBL t1
	    SET
	    (
	        t1.PREPAYAMT, t1.POSTINGDT, t1.POSTINGMO, t1.POSTINGYRNMB
	    ) =
	    (
	    SELECT
	        NVL(SUM(t2.PREPAYAMT), 0) AS PREPAYAMT,
	        MAX(t2.POSTINGDT) AS POSTINGDT,
	        EXTRACT(month FROM MAX(t2.POSTINGDT)),
	        EXTRACT(year FROM MAX(t2.POSTINGDT))
	    FROM CDCONLINE.PYMTHISTRYTBL t2
	    WHERE (t2.PYMTTYP = 'X' OR t2.PYMTTYP IS NULL)
	    AND t2.PREPAYAMT <> 0
	    AND t1.LOANNMB = t2.LOANNMB
	    GROUP BY t2.LOANNMB
	    )
	    WHERE LOANNMB = p_loannmb;

	    UPDATE stgCSA.PRPLOANRQSTTBL t1
	    SET
	    (
	        t1.POSTINGDT, t1.POSTINGMO, t1.POSTINGYRNMB
	    ) =
	    (
			SELECT WirePostDates.POSTDT LastPostDate,
				        EXTRACT(month FROM WirePostDates.POSTDT) PostMo,
				        EXTRACT(year FROM WirePostDates.POSTDT) PostYr
			FROM
			  (SELECT CTPrePay.TRANSID
			  FROM stgCSA.CORETRANSTBL CTPrePay
			  WHERE CTPrePay.LOANNMB  = p_loannmb
			  AND CTPrePay.TRANSTYPID = 5
			  ) Prepay
			INNER JOIN
			  (SELECT CTA.TRANSID WIRETRANSID,
			    CTA.ATTRVAL TRANSID
			  FROM stgCSA.CORETRANSATTRTBL CTA
			  WHERE CTA.ATTRID = 28
			  ) Wires
			ON Prepay.TRANSID = Wires.TRANSID
			INNER JOIN
			  (SELECT CTA27.TRANSID WIRETRANSID,
			    TO_DATE(CTA27.ATTRVAL, 'mm/dd/yyyy') POSTDT
			  FROM stgCSA.CORETRANSATTRTBL CTA27
			  WHERE CTA27.ATTRID = 27
			  ) WirePostDates
			ON Wires.WIRETRANSID = WirePostDates.WIRETRANSID
			ORDER BY WirePostDates.POSTDT DESC
			FETCH FIRST 1 ROWS ONLY
	    )
	    WHERE LOANNMB = p_loannmb
	    AND POSTINGDT IS NULL;

--	<!--- IF ISSUE WITH MISSING RECORD CDC ONLINE OR NULL POSTING DATE FROM PAYMENT POSTING, 
--	THIS WILL PUT GOOD DATA IN--->
    cur_stmt:=121;
    for ra in 
      (SELECT t1.LOANNMB, t1.WKPREPAYAMT, t1.RQSTREF, t1.CREATDT, t2.POSTINGDT
      FROM CDCONLINE.LOANSAVETBL t1
      INNER JOIN stgCSA.PRPLOANRQSTTBL t2
      ON t1.RQSTREF = t2.RQSTREF
      WHERE t1.LOANNMB = p_loannmb
      ORDER BY t1.CREATDT DESC)
    loop  
      -- note the for-loop construct replaces cfif because if no recs loop runs zero times
      --	<!--- If record found in LOANSAVE, use the WKPREPAY amount   --->
      --	<cfif getLoanWKPREPAYAMT.recordcount>
      cur_stmt:=122;
			UPDATE stgCSA.PRPLOANRQSTTBL
			SET
				PREPAYAMT = ra.WKPREPAYAMT
			WHERE LOANNMB = p_LOANNMB;
    exit; /* just do above update for first row returned */
/*	note this section was commented out in CF source:	
    <!--- If POSTINGDT is NULL in PRPLOANRQST, us the WKPREPAY CREATDT
		<cfif getLoanWKPREPAYAMT.POSTINGDT EQ ''>
			<cfquery datasource="#variables.db#" username="#variables.username#" password="#variables.password#">
				UPDATE stgCSA.PRPLOANRQSTTBL
				SET
					POSTINGDT = '#getLoanWKPREPAYAMT.CREATDT#',
					POSTINGMO = EXTRACT(month FROM TRUNC(TO_DATE('#getLoanWKPREPAYAMT.CREATDT#', 'yyyy/mm/dd'))),
					POSTINGYRNMB = EXTRACT(year FROM TRUNC(TO_DATE('#getLoanWKPREPAYAMT.CREATDT#', 'yyyy/mm/dd')))
				WHERE LOANNMB = <cfqueryparam value="#arguments.loanNmb#" cfsqltype="CF_SQL_VARCHAR">
			</cfquery>
		</cfif>   --->
*/		
      null;
	  end loop;

      cur_stmt:=123;
	    UPDATE stgCSA.PRPLOANRQSTTBL t1
	    SET
	    (
	        t1.BUSDAY6DT, t1.THIRDTHURSDAY, t1.PREPAYCALCDT
	    ) =
	    (
	    SELECT
	        t2.CALNDRBUSDAYBUSDAY6DT,
	        t2.THIRDTHURSDAYDt,
	        CASE WHEN t1.POSTINGDT > t2.THIRDTHURSDAYDt AND t1.PREPAYDT > t2.THIRDTHURSDAYDt
	        THEN ADD_MONTHS(t1.PREPAYDT, -1)
	        ELSE t1.PREPAYDT
	        END
	    FROM stgCSA.REFCALNDRBUSDAYTBL t2
	    WHERE t1.POSTINGYRNMB = t2.CALNDRBUSDAYYR
	    AND t1.POSTINGMO = t2.CALNDRBUSDAYMO
	    )
	    WHERE t1.LOANNMB = p_loannmb;

	    UPDATE stgCSA.PRPLOANRQSTTBL
	    SET
	        PREPAYCALCDT = PREPAYDT
	    WHERE PREPAYCALCDT IS NULL
	    AND LOANNMB = p_loannmb;

      UPDATE stgCSA.PRPLOANRQSTTBL t1
	    SET
	        SEMIANMO =
	                CASE WHEN MONTHS_BETWEEN(LAST_DAY(t1.SEMIANNDT), LAST_DAY(t1.PREPAYDT)) = 6
	                THEN 6
	                ELSE
	                    CASE WHEN TRUNC(t1.POSTINGDT) > TRUNC(t1.THIRDTHURSDAY) AND TRUNC(t1.PREPAYDT) 
	                      > TRUNC(t1.THIRDTHURSDAY)
	                    THEN MONTHS_BETWEEN(LAST_DAY(t1.SEMIANNDT), LAST_DAY(t1.PREPAYDT)) + 1
	                    ELSE MONTHS_BETWEEN(LAST_DAY(t1.SEMIANNDT), LAST_DAY(t1.PREPAYDT))
	                    END
	                END
	    WHERE t1.LOANNMB = p_loannmb;

      cur_stmt:=124;
	    UPDATE stgCSA.PRPLOANRQSTTBL t1
	    SET
	        CDCPAIDTHRUDT =
	        CASE WHEN EXTRACT(month FROM PREPAYCALCDT)-1 = 3
	        THEN EXTRACT(year FROM PREPAYCALCDT) || '/02/30'
	        ELSE EXTRACT(year FROM PREPAYCALCDT) || '/' || SUBSTR('0' || to_char(EXTRACT(month FROM ADD_MONTHS(PREPAYCALCDT, -1))), -2) || '/30'
	        END
	    WHERE t1.LOANNMB = p_loannmb;

      cur_stmt:=125;
	    UPDATE stgCSA.PRPLOANRQSTTBL t1
	    SET
	    (
	        CSADUEFROMBORRAMT, CDCDUEFROMBORRAMT, DUEFROMBORRAMT, APPVDT, CURRFEEBALAMT, SEMIANNPYMTAMT
	  	    --CSADUEFROMBORRAMT, CDCDUEFROMBORRAMT, SBADUEFROMBORRAMT, APPVDT, CURRFEEBALAMT, SEMIANNPYMTAMT
	    ) =
	    (
	    SELECT DISTINCT
	        NVL(t2.CSADUEFROMBORRAMT, 0),
	        NVL(t2.CDCDUEFROMBORRAMT, 0),
	        NVL(t2.DUEFROMBORRAMT, 0),
	        --NVL(t2.SBADUEFROMBORRAMT, 0),
	        t2.APPRDT,
	        NVL(t2.CURRFEEBALAMT, 0),
	        NVL(t2.SEMIANAMT, 0)
	    FROM stgCSA.PRPMFDUEFROMBORRTBL t2
	    WHERE t1.LOANNMB = t2.LOANNMB
	    )
	    WHERE t1.LOANNMB = p_loannmb;

      cur_stmt:=126;
	    UPDATE stgCSA.PRPLOANRQSTTBL
	    SET
	        CDCDUEFROMBORRAMT = NVL(CDCDUEFROMBORRAMT, 0),
	        CSADUEFROMBORRAMT = NVL(CSADUEFROMBORRAMT, 0),
	        DUEFROMBORRAMT = NVL(DUEFROMBORRAMT, 0),
	        --SBADUEFROMBORRAMT = NVL(SBADUEFROMBORRAMT, 0),
	        CURRFEEBALAMT = NVL(CURRFEEBALAMT, 0),
	        SEMIANNPYMTAMT = NVL(SEMIANNPYMTAMT, 0)
	    WHERE LOANNMB = p_loannmb;

      cur_stmt:=127;
	    UPDATE stgCSA.PRPLOANRQSTTBL t1
	    SET
	    (
	        t1.PRINCURAMT, t1.BALAMT
	    ) =
	    (
	    SELECT
	        NVL(SUM(CASE t2.PYMTTYP WHEN 'X' THEN t2.PRINAMT ELSE 0 END), 0) AS PRINCURAMT,
	        NVL(SUM(CASE t2.PYMTTYP WHEN 'X' THEN 0 ELSE t2.BALAMT END), 0) AS BALAMT
	    FROM CDCONLINE.PYMTSAVETBL t2
	    WHERE t1.RQSTREF = t2.RQSTREF
	    GROUP BY t1.LOANNMB
	    )
	    WHERE t1.LOANNMB = p_loannmb;

      cur_stmt:=128;
	    UPDATE stgCSA.PRPLOANRQSTTBL t1
	    SET
	    (
	        t1.PIAMT, t1.NOTERTPCT, t1.SEMIANNAMT, t1.SEMIANNPYMTAMT, t1.ISSDT, t1.DBENTRBALAMT, t1.PREPAYPREMAMT,
	        t1.PRINNEEDEDAMT, t1.INTNEEDEDAMT, t1.CSAFEENEEDEDAMT, t1.CDCFEENEEDEDAMT, t1.LATEFEENEEDEDAMT, t1.FEENEEDEDAMT,
	        t1.AMORTSBAAMT, t1.AMORTCSAAMT, t1.CDCPCT, t1.PREPAYPCT
	    ) =
	        --t1.AMORTSBAAMT, t1.AMORTCSAAMT, t1.CDCPCT, t1.SBAPCT) =
	    (
	    SELECT
	        NVL(t2.AMORTPRINAMT, 0) + NVL(t2.AMORTINTAMT, 0) AS PIAMT,
	        NVL(t2.NOTERTPCT, 0),
	        NVL(t2.SEMIANNPYMTAMT, 0),
	        NVL(t2.SEMIANNPYMTAMT, 0),
	        t2.ISSDT,
	        NVL(t2.DBENTRBALAMT, 0),
	        NVL(t2.PREPAYPREMAMT, 0),
	        NVL(t2.PRINNEEDEDAMT, 0),
	        NVL(t2.INTNEEDEDAMT, 0),
	        NVL(t2.CSAFEENEEDEDAMT, 0),
	        NVL(t2.CDCFEENEEDEDAMT, 0),
	        NVL(t2.LATEFEENEEDEDAMT, 0),
	        NVL(t2.SBAFEENEEDEDAMT, 0) AS FEENEEDEDAMT,
	        NVL(t2.AMORTSBAAMT, 0),
	        NVL(t2.AMORTCSAAMT, 0),
	        NVL(t2.CDCPCT, 0),
	        NVL(t2.SBAPCT, 0)
	    FROM CDCONLINE.LoanSaveTbl t2
	    WHERE t1.RQSTREF = t2.RQSTREF
	    )
	    WHERE t1.LOANNMB = p_loannmb;

      cur_stmt:=129;
	    UPDATE stgCSA.PRPMFDUEFROMBORRTBL t1
	    SET
	    (
	        SEMIANAMT,
	        SEMIANDT
	    ) =
	    (
	    SELECT SEMIANNAMT, SEMIANNDT
	    FROM stgCSA.PRPLOANRQSTTBL
	    WHERE LOANNMB = p_loannmb
	    )
	    WHERE LOANNMB = p_loannmb;

      cur_stmt:=130;
	    UPDATE stgCSA.PRPLOANRQSTTBL SET SEMIANNAMT = SEMIANNPYMTAMT 
	      WHERE SEMIANNAMT = 0 AND SEMIANNPYMTAMT <> 0;

      cur_stmt:=131;
	    UPDATE stgCSA.PRPLOANRQSTTBL
	    SET
	        BALCALCAMT = CASE WHEN BALAMT = PRINCURAMT
	                    THEN BALAMT - PRINNEEDEDAMT
	                    ELSE BALAMT - PRINNEEDEDAMT - PRINAMT
	                    END
	    WHERE LOANNMB = p_loannmb;

      cur_stmt:=132;
	    UPDATE stgCSA.PRPLOANRQSTTBL t1
	    SET
	        t1.FIVEYRADJ = ROUND(MONTHS_BETWEEN(t1.PREPAYDT, t1.ISSDT), 0) + 1,
	        t1.FIVEYRADJCSAAMT = (0.001 * t1.BALCALCAMT) / 12,
	        t1.FIVEYRADJCDCAMT = (t1.CDCPCT * t1.BALCALCAMT) / 12,
	        t1.FIVEYRADJSBAAMT = (t1.PREPAYPCT * t1.BALCALCAMT) / 12
	        --t1.FIVEYRADJSBAAMT = (t1.SBAPCT * t1.BALCALCAMT) / 12
	    WHERE LOANNMB = p_loannmb;

      cur_stmt:=133;
	    UPDATE stgCSA.PRPLOANRQSTTBL t1
	    SET
	        CSAFEECALCAMT =
	            CASE WHEN FIVEYRADJ IN (1,61,121,181,241,301)
	            THEN ROUND(FIVEYRADJCSAAMT * SEMIANMO, 2)
	            ELSE NVL(CSAFEENEEDEDAMT, 0) - NVL(CSADUEFROMBORRAMT, 0) + AMORTCSAAMT * SEMIANMO
	            END,
	        FEECALCAMT =
	        --SBAFEECALCAMT =
	            CASE WHEN FIVEYRADJ IN (1,61,121,181,241,301)
	            THEN ROUND(FIVEYRADJSBAAMT * SEMIANMO,2)
	            ELSE NVL(FEENEEDEDAMT, 0) - NVL(DUEFROMBORRAMT, 0) + AMORTSBAAMT * SEMIANMO
	            END,
	        CSAFEECALCONLYAMT =
	            CASE WHEN FIVEYRADJ IN (1,61,121,181,241,301)
	            THEN ROUND(FIVEYRADJCSAAMT * SEMIANMO, 2)
	            ELSE AMORTCSAAMT * SEMIANMO
	            END,
	        CDCFEECALCONLYAMT =
	            CASE WHEN FIVEYRADJ IN (1,61,121,181,241,301)
	            THEN ROUND(FIVEYRADJCDCAMT * SEMIANMO, 2)
	            ELSE AMORTCDCAMT * SEMIANMO
	            END,
	        FEECALCONLYAMT =
	            CASE WHEN FIVEYRADJ IN (1,61,121,181,241,301)
	            THEN ROUND(FIVEYRADJSBAAMT * SEMIANMO, 2)
	            ELSE AMORTSBAAMT * SEMIANMO
	            END
	        --SBAFEECALCONLYAMT = CASE WHEN FIVEYRADJ IN (1,61,121,181,241,301) 
	        -- THEN ROUND(FIVEYRADJSBAAMT * SEMIANMO, 2) ELSE AMORTSBAAMT * SEMIANMO END
	    WHERE LOANNMB = p_loannmb;

      cur_stmt:=134;
	    UPDATE stgCSA.PRPLOANRQSTTBL SET BALCALC1AMT = 
	        BALCALCAMT - (PIAMT - ROUND((BALCALCAMT * (NOTERTPCT / 100) / 360) * 30, 2)) 
	      WHERE LOANNMB = p_loannmb;

      cur_stmt:=135;
    	UPDATE stgCSA.PRPLOANRQSTTBL SET BALCALC2AMT = 
    	    BALCALC1AMT - (PIAMT - ROUND((BALCALC1AMT * (NOTERTPCT / 100) / 360) * 30, 2)) 
    	  WHERE LOANNMB = p_loannmb;

      cur_stmt:=136;
	    UPDATE stgCSA.PRPLOANRQSTTBL SET BALCALC3AMT = 
	        BALCALC2AMT - (PIAMT - ROUND((BALCALC2AMT * (NOTERTPCT / 100) / 360) * 30, 2)) 
	      WHERE LOANNMB = p_loannmb;

      cur_stmt:=137;
    	UPDATE stgCSA.PRPLOANRQSTTBL SET BALCALC4AMT = 
    	    BALCALC3AMT - (PIAMT - ROUND((BALCALC3AMT * (NOTERTPCT / 100) / 360) * 30, 2)) 
    	  WHERE LOANNMB = p_loannmb;

      cur_stmt:=138;
	    UPDATE stgCSA.PRPLOANRQSTTBL SET BALCALC5AMT = 
	        BALCALC4AMT - (PIAMT - ROUND((BALCALC4AMT * (NOTERTPCT / 100) / 360) * 30, 2)) 
	      WHERE LOANNMB = p_loannmb;

      cur_stmt:=139;
	    UPDATE stgCSA.PRPLOANRQSTTBL SET BALCALC6AMT = 
	        BALCALC5AMT - (PIAMT - ROUND((BALCALC5AMT * (NOTERTPCT / 100) / 360) * 30, 2)) 
	      WHERE LOANNMB = p_loannmb;

      cur_stmt:=140;
	    UPDATE stgCSA.PRPLOANRQSTTBL
	    SET
	        NOTEBALCALCAMT =
	        CASE
	        WHEN SEMIANMO = 1 THEN
	            CASE WHEN BALCALC1AMT < 0 THEN 0 ELSE BALCALC1AMT END
	        WHEN SEMIANMO = 2 THEN
	            CASE WHEN BALCALC2AMT < 0 THEN 0 ELSE BALCALC2AMT END
	        WHEN SEMIANMO = 3 THEN
	            CASE WHEN BALCALC3AMT < 0 THEN 0 ELSE BALCALC3AMT END
	        WHEN SEMIANMO = 4 THEN
	            CASE WHEN BALCALC4AMT < 0 THEN 0 ELSE BALCALC4AMT END
	        WHEN SEMIANMO = 5 THEN
	            CASE WHEN BALCALC5AMT < 0 THEN 0 ELSE BALCALC5AMT END
	        WHEN SEMIANMO = 6 THEN
	            CASE WHEN BALCALC6AMT < 0 THEN 0 ELSE BALCALC6AMT END
	        ELSE BALCALCAMT
	        END
	    WHERE LOANNMB = p_loannmb;

      cur_stmt:=141;
	    UPDATE stgCSA.PRPLOANRQSTTBL SET PRINAMT = 
	        PIAMT-ROUND((BALCALCAMT*(NOTERTPCT/100)/360)*30,2) 
	      WHERE LOANNMB = p_loannmb;

      cur_stmt:=142;
	    UPDATE stgCSA.PRPLOANRQSTTBL SET PRIN2AMT = BALCALC1AMT-BALCALC2AMT 
	      WHERE LOANNMB = p_loannmb;

      cur_stmt:=143;
	    UPDATE stgCSA.PRPLOANRQSTTBL SET PRIN3AMT = BALCALC2AMT-BALCALC3AMT 
	      WHERE LOANNMB = p_loannmb;

      cur_stmt:=144;
	    UPDATE stgCSA.PRPLOANRQSTTBL SET PRIN4AMT = BALCALC3AMT-BALCALC4AMT 
	      WHERE LOANNMB = p_loannmb;

      cur_stmt:=145;
	    UPDATE stgCSA.PRPLOANRQSTTBL SET PRIN5AMT = BALCALC4AMT-BALCALC5AMT 
	      WHERE LOANNMB = p_loannmb;

      cur_stmt:=146;
	    UPDATE stgCSA.PRPLOANRQSTTBL SET PRIN6AMT = BALCALC5AMT-BALCALC6AMT 
	      WHERE LOANNMB = p_loannmb;

      cur_stmt:=178;
	    UPDATE stgCSA.PRPLOANRQSTTBL
	    SET
	        PRINPROJAMT =
	        CASE
	        WHEN SEMIANMO = 6 THEN PRINAMT+PRIN2AMT+PRIN3AMT+PRIN4AMT+PRIN5AMT+PRIN6AMT
	        WHEN SEMIANMO = 5 THEN PRINAMT+PRIN2AMT+PRIN3AMT+PRIN4AMT+PRIN5AMT
	        WHEN SEMIANMO = 4 THEN PRINAMT+PRIN2AMT+PRIN3AMT+PRIN4AMT
	        WHEN SEMIANMO = 3 THEN PRINAMT+PRIN2AMT+PRIN3AMT
	        WHEN SEMIANMO = 2 THEN PRINAMT+PRIN2AMT
	        ELSE PRINAMT
	        END
	    WHERE LOANNMB = p_loannmb;

      cur_stmt:=147;
	    UPDATE stgCSA.PRPLOANRQSTTBL SET PIPROJAMT = PIAMT*SEMIANMO 
	      WHERE LOANNMB = p_loannmb;

      cur_stmt:=148;
	    UPDATE stgCSA.PRPLOANRQSTTBL SET INTAMT = ROUND((BALCALCAMT*(NOTERTPCT/100)/360)*30,2) 
	      WHERE LOANNMB = p_loannmb;

      cur_stmt:=149;
	    UPDATE stgCSA.PRPLOANRQSTTBL SET INT2AMT = PIAMT-BALCALC1AMT-BALCALC2AMT 
	      WHERE LOANNMB = p_loannmb;

      cur_stmt:=150;
	    UPDATE stgCSA.PRPLOANRQSTTBL SET INT3AMT = PIAMT-BALCALC2AMT-BALCALC3AMT 
	      WHERE LOANNMB = p_loannmb;

      cur_stmt:=151;
	    UPDATE stgCSA.PRPLOANRQSTTBL SET INT4AMT = PIAMT-BALCALC3AMT-BALCALC4AMT 
	      WHERE LOANNMB = p_loannmb;

      cur_stmt:=152;
	    UPDATE stgCSA.PRPLOANRQSTTBL SET INT5AMT = PIAMT-BALCALC4AMT-BALCALC5AMT 
	      WHERE LOANNMB = p_loannmb;

      cur_stmt:=153;
	    UPDATE stgCSA.PRPLOANRQSTTBL SET INT6AMT = PIAMT-BALCALC5AMT-BALCALC6AMT 
	      WHERE LOANNMB = p_loannmb;

      cur_stmt:=154;
	    UPDATE stgCSA.PRPLOANRQSTTBL
	    SET
	        CSABEHINDAMT = CSAFEENEEDEDAMT-CSADUEFROMBORRAMT,
	        CDCBEHINDAMT = CDCFEENEEDEDAMT-CDCDUEFROMBORRAMT,
	        BEHINDAMT = FEENEEDEDAMT-DUEFROMBORRAMT,
	    --    SBABEHINDAMT = SBAFEENEEDEDAMT-DUEFROMBORRAMT,
	        BORRFEEAMT = (AMORTSBAAMT*SEMIANMO)+FEENEEDEDAMT+DUEFROMBORRAMT
	    WHERE LOANNMB = p_loannmb;

      cur_stmt:=155;
	    UPDATE stgCSA.PRPLOANRQSTTBL
	    SET
	        MODELNQ =
	        CASE WHEN (AMORTCDCAMT = 0 OR CDCBEHINDAMT = 0)
	        THEN 0
	        ELSE CDCBEHINDAMT / AMORTCDCAMT
	        END
	    WHERE LOANNMB = p_loannmb;

--  <cfset PymtHistCount = checkPYMTHISTRY(arguments.loanNmb) />
      cur_stmt:=156;
      SELECT COUNT(*) into PAYMENTS_count
           FROM CDCONLINE.PYMTHISTRYTBL PH
           WHERE (PH.PYMTTYP = 'X' OR PH.PYMTTYP IS NULL)
           AND PH.LOANNMB = p_loannmb
           AND PH.PREPAYAMT <> 0;

-- <cfif 1 eq 1> :-)

      cur_stmt:=157;
	    UPDATE stgCSA.PRPLOANRQSTTBL
	    SET
	        CDCSBAFEEDISBAMT =
	        CASE WHEN APPVDT > '30-SEP-96'
	        THEN ((CURRFEEBALAMT * 0.00125) / 12) * MODELNQ
	        ELSE 0
	        END,
	        CDCFEEDISBAMT =
	        CASE WHEN APPVDT > '30-SEP-96'
	        THEN CDCFEENEEDEDAMT - ((CURRFEEBALAMT * 0.00125) / 12) * MODELNQ
	        ELSE CDCFEENEEDEDAMT
	        END
	    WHERE LOANNMB = p_loannmb;

--	<!--- PYMTHISTRYTBL --->
      cur_stmt:=158;
		    UPDATE stgCSA.PRPLOANRQSTTBL t1
		    SET
		    (
		        t1.ESCROWAMT
		    ) =
		    (
		    SELECT
		        SUM(NVL(t2.PRINAMT,0) + NVL(t2.INTAMT,0)) AS ESCROW
		    FROM CDCONLINE.PYMTHISTRYTBL t2
		    WHERE
		        t2.POSTINGDT <
		        (
		        SELECT SEMIANNDT
		        FROM CDCONLINE.RqstTbl t3
		        WHERE t3.RQSTREF = t1.RQSTREF
		        )
		        AND t2.POSTINGDT >
		        (
		        SELECT ADD_MONTHS(SEMIANNDT, -5)
		        FROM CDCONLINE.RqstTbl t3
		        WHERE t3.RQSTREF = t1.RQSTREF
		        )
		        AND t2.PYMTTYP <> 'X'
		        AND t1.LOANNMB = t2.LOANNMB
		    GROUP BY t1.LOANNMB
		    )
		    WHERE LOANNMB = p_loannmb;

      cur_stmt:=158;
		    UPDATE stgCSA.PRPMFDUEFROMBORRTBL t1
		    SET
		    (
		        ESCPRIORPYMTAMT
		    ) =
		    (
		    SELECT ESCROWAMT
		    FROM stgCSA.PRPLOANRQSTTBL
		    WHERE LOANNMB = p_loannmb
		    )
		    WHERE LOANNMB = p_loannmb;

      cur_stmt:=159;
		    UPDATE stgCSA.PRPLOANRQSTTBL SET ESCROWAMT = NVL(ESCROWAMT, 0) 
		      WHERE LOANNMB = p_loannmb;

--	<!--- PYMTHISTRYTBL --->
      cur_stmt:=160;
		    UPDATE stgCSA.PRPLOANRQSTTBL t1
		    SET
		    (
		        UNALLOCAMT
		    ) =
		    (
		        SELECT
		        NVL(SUM(t2.UNALLOCAMT), 0) AS UNALLOCAMT
		        FROM cdconline.PYMTHISTRYTBL t2
		        WHERE t2.PREPAYAMT = 0
		        AND t1.LOANNMB = t2.LOANNMB
		        GROUP BY t1.LOANNMB
		    )
		    WHERE LOANNMB = p_loannmb;

      cur_stmt:=161;
			UPDATE stgCSA.PRPLOANRQSTTBL
			SET
				PREPAYAMT = (PREPAYAMT - UNALLOCAMT)
			WHERE LOANNMB = p_loannmb;

      cur_stmt:=162;
      UPDATE stgCSA.PRPLOANRQSTTBL SET UNALLOCAMT = NVL(UNALLOCAMT, 0) 
      WHERE LOANNMB = p_loannmb;

--	<!--- PYMTHISTRYTBL --->
      cur_stmt:=163;
		    UPDATE stgCSA.PRPLOANRQSTTBL t1
		    SET
		    (
		        GFDAMT
		    ) =
		    (
		        SELECT SUM(PREPAYAMT)
		        FROM
		        (
		            SELECT t2.PREPAYAMT
		            FROM CDCONLINE.PYMTHISTRYTBL t2
		            WHERE TRIM(t2.PYMTTYP) IS NULL
		            AND t2.LOANNMB = t1.LOANNMB
		            AND t2.PREPAYAMT >= -1000
		            AND t2.PREPAYAMT <= 1000
		        )
		        GROUP BY LOANNMB
		        HAVING SUM(PREPAYAMT) = 1000
		        OR SUM(PREPAYAMT) = -1000
		    )
		    WHERE LOANNMB = p_loannmb;

      cur_stmt:=164;
		    UPDATE stgCSA.PRPLOANRQSTTBL SET GFDAMT = NVL(GFDAMT, 0) 
		      WHERE LOANNMB = p_loannmb;

      cur_stmt:=165;
		    INSERT INTO stgCSA.TEMPGETDTTBL
		    (
		    LOANNMB, PREPAYDT,CREATDT,CREATUSERID,LASTUPDTDT,LASTUPDTUSERID
		    )
		    SELECT
		        t2.LOANNMB, t2.OPENGNTYDTCONVERTED,
		        SYSTIMESTAMP,
		        p_userid,
		        SYSTIMESTAMP,
		        p_userid
		    FROM stgCSA.PRPLOANRQSTTBL t3
		    INNER JOIN stgCSA.PRPMFGNTYTBL t2
		    ON t3.LOANNMB = t2.LOANNMB
		    WHERE t2.PRPCMNT Not Like '%1-5%'
		    AND t2.OPENGNTYDTCONVERTED IS NOT NULL
		    AND t3.LOANNMB = p_loannmb;

--	<!--- PYMTHISTRYTBL --->
      cur_stmt:=166;
		    INSERT INTO stgCSA.TEMPGETOGPI6MOTBL
		    (
		        LOANNMB, PRININT6MOAMT, CREATDT,CREATUSERID,LASTUPDTDT,LASTUPDTUSERID
		    )
		    SELECT
		        t2.LOANNMB,
		        (t2.PRINAMT + t2.INTAMT) AS PRININT6MOAMT,
		        SYSTIMESTAMP,
		        p_userid,
		        SYSTIMESTAMP,
		        p_userid
		    FROM CDCONLINE.PYMTHISTRYTBL t2
		    INNER JOIN stgCSA.TEMPGETDTTBL t3
		    ON t2.LOANNMB = t3.LOANNMB
		    WHERE t2.POSTINGDT >= t3.PREPAYDT
		    AND PREPAYAMT = 0 AND PRINAMT <> 0
		    AND CAST(MONTHS_BETWEEN(t2.POSTINGDT, t3.PREPAYDT) AS INT) = 0
		    AND t2.LOANNMB = p_loannmb;

      cur_stmt:=167;
		    UPDATE stgCSA.PRPLOANRQSTTBL t1
		    SET
		        PI6MOAMT = NVL(
		        (
		        SELECT Sum(t2.PRININT6MOAMT) AS SumOfPI6Month
		        FROM stgCSA.TEMPGETOGPI6MOTBL t2
		        WHERE t1.LOANNMB = t2.LOANNMB
		        ), 0)
		    WHERE t1.LOANNMB = p_loannmb;

      cur_stmt:=168;
		    UPDATE stgCSA.PRPMFGNTYTBL t1
		    SET
		    (
		        SEMIANDT, PREPAYDT, SEMIANDTCONVERTED
		    ) =
		    (
		    SELECT SEMIANNDT, PREPAYDT, SEMIANNDT
		    FROM stgCSA.PRPLOANRQSTTBL
		    WHERE LOANNMB = p_loannmb
		    )
		    WHERE LOANNMB = p_loannmb;

--	<!--- PYMTHISTRYTBL --->
      cur_stmt:=169;
		    INSERT INTO stgCSA.TEMPGETOGERRRAWTBL
		    (
		        GETOPENGNTYERRRAWID,
		        LOANNMB,
		        OPENGNTYERRAMT,
		        THIRDTHURSDAYDT,
		        PREPAYDT,
		        POSTINGDT,
		        DTCHKIND,
		        DTCHKTRUE,
		        DTCHKFALSE,
		        CREATUSERID,
		        CREATDT,
		        LASTUPDTUSERID,
		        LASTUPDTDT
		    )
		        SELECT
		        PRPMFGNTYTBL.PRPMFGNTYID,
		        PRPMFGNTYTBL.LOANNMB,
		        PRPMFGNTYTBL.GNTYAMT,
		        REFCALNDRBUSDAYTBL.THIRDTHURSDAYDT,
		        PRPMFGNTYTBL.PREPAYDT,
		        PYMTHISTRYTBL.Max_POSTINGDT,
		        CASE
		            WHEN PYMTHISTRYTBL.Max_POSTINGDT > REFCALNDRBUSDAYTBL.THIRDTHURSDAYDT
		            AND PRPMFGNTYTBL.PREPAYDT        > REFCALNDRBUSDAYTBL.THIRDTHURSDAYDT
		            THEN 1
		            ELSE 0
		        END                                                                               AS DATECHECKFLAG,
		        ROUND(MONTHS_BETWEEN(LOANTBL.SEMIANNDT, ADD_MONTHS(PRPMFGNTYTBL.PREPAYDT, -1))+1) AS DATECHECKTRUE,
		        ROUND(MONTHS_BETWEEN(LOANTBL.SEMIANNDT, PRPMFGNTYTBL.PREPAYDT)+1)                 AS DATECHECKFALSE,
		        p_userid,
		        SYSTIMESTAMP,
		        p_userid,
		        SYSTIMESTAMP
		        FROM
		        (SELECT
		            LOANNMB,
		            SEMIANNDT
		        FROM stgCSA.PRPLOANRQSTTBL
		        ) LOANTBL
		        LEFT JOIN
		        (SELECT
		            LOANNMB,
		            MAX(POSTINGDT)                AS Max_POSTINGDT,
		            MAX(PYMTTYP)                  AS Max_PYMTTYP,
		            SUM(PREPAYAMT)                AS Sum_PREPAYAMT,
		            Extract(MONTH FROM POSTINGDT) AS POSTINGMO,
		            Extract(YEAR FROM POSTINGDT)  AS POSTINGYR
		        FROM
		            (SELECT
		            t2.LOANNMB,
		            t2.POSTINGDT,
		            t2.PYMTTYP,
		            t2.PREPAYAMT
		            FROM CDCONLINE.PYMTHISTRYTBL t2
		            WHERE (t2.PYMTTYP = 'X' OR t2.PYMTTYP IS NULL)
		            AND t2.PREPAYAMT <> 0
		            )
		        GROUP BY LOANNMB,
		            Extract(MONTH FROM POSTINGDT),
		            Extract(YEAR FROM POSTINGDT)
		        ) PYMTHISTRYTBL
		        ON LOANTBL.LOANNMB = PYMTHISTRYTBL.LOANNMB
		        LEFT JOIN
		        (SELECT * FROM stgCSA.REFCALNDRBUSDAYTBL
		        ) REFCALNDRBUSDAYTBL
		        ON PYMTHISTRYTBL.POSTINGMO  = REFCALNDRBUSDAYTBL.CALNDRBUSDAYMO
		        AND PYMTHISTRYTBL.POSTINGYR = REFCALNDRBUSDAYTBL.CALNDRBUSDAYYR
		        LEFT JOIN
		        (SELECT
		            PRPMFGNTYTBL.PRPMFGNTYID,
		            PRPMFGNTYTBL.LOANNMB,
		            PRPMFGNTYTBL.GNTYAMT,
		            PRPMFGNTYTBL.PREPAYDT,
		            Extract(MONTH FROM PRPMFGNTYTBL.OPENGNTYDTCONVERTED) AS OPENGNTYDTCONVERTEDMO,
		            Extract(YEAR FROM PRPMFGNTYTBL.OPENGNTYDTCONVERTED)  AS OPENGNTYDTCONVERTEDYR,
		            Extract(MONTH FROM PRPMFGNTYTBL.PREPAYDT) AS PREPAYMO,
		            Extract(YEAR FROM PRPMFGNTYTBL.PREPAYDT)  AS PREPAYYR
		        FROM stgCSA.PRPMFGNTYTBL
		        ) PRPMFGNTYTBL
		        ON PYMTHISTRYTBL.LOANNMB = PRPMFGNTYTBL.LOANNMB
		        WHERE PRPMFGNTYTBL.OPENGNTYDTCONVERTEDMO - PRPMFGNTYTBL.PREPAYMO = 0
		        AND PRPMFGNTYTBL.OPENGNTYDTCONVERTEDYR - PRPMFGNTYTBL.PREPAYYR = 0
		        AND PRPMFGNTYTBL.LOANNMB IS NOT NULL
		        AND PRPMFGNTYTBL.LOANNMB = p_loannmb;

      cur_stmt:=170;
		    INSERT INTO stgCSA.TEMPGETOGERRTBL
		    (
		        GETOPENGNTYERRID,
		        LOANNMB,
		        OPENGNTYERRAMT,
		        CREATUSERID,
		        CREATDT,
		        LASTUPDTUSERID,
		        LASTUPDTDT
		    )
		        SELECT
		        GETOPENGNTYERRRAWID,
		        LOANNMB,
		        OPENGNTYERRAMT,
		        p_userid,
		        SYSTIMESTAMP,
		        p_userid,
		        SYSTIMESTAMP
		        FROM stgCSA.TEMPGETOGERRRAWTBL
		        WHERE CASE WHEN DTCHKIND = 1 THEN DTCHKTRUE ELSE DTCHKFALSE END = 1
		        AND LOANNMB = p_loannmb;

        cur_stmt:=171;
   	    UPDATE stgCSA.PRPLOANRQSTTBL t1
		    SET
		        OPENGNTYERRAMT = NVL(
		        (
		        SELECT Sum(t2.OPENGNTYERRAMT) AS SumOPENGNTYERRAMT
		        FROM stgCSA.TEMPGETOGERRTBL t2
		        WHERE t1.LOANNMB = t2.LOANNMB
		        ), 0)
		    WHERE LOANNMB = p_loannmb;

--	<!--- PYMTHISTRYTBL --->
      cur_stmt:=172;
		    UPDATE stgCSA.PRPLOANRQSTTBL t1
		    SET
		    OPENGNTYREGAMT = NVL(
		    (
		        SELECT
		        SUM(PRPMFGNTYTBL.GNTYAMT) AS Sum_GNTYAMT
		        FROM
		        (SELECT LOANNMB, SEMIANNDT FROM stgCSA.PRPLOANRQSTTBL
		        ) LOANTBL
		        LEFT JOIN
		        (SELECT LOANNMB,
		            MAX(POSTINGDT)                AS Max_POSTINGDT,
		            MAX(PYMTTYP)                  AS Max_PYMTTYP,
		            SUM(PREPAYAMT)                AS Sum_PREPAYAMT,
		            Extract(MONTH FROM POSTINGDT) AS POSTINGMO,
		            Extract(YEAR FROM POSTINGDT)  AS POSTINGYR
		        FROM
		            (SELECT t2.LOANNMB,
		            t2.POSTINGDT,
		            t2.PYMTTYP,
		            t2.PREPAYAMT
		            FROM CDCONLINE.PYMTHISTRYTBL t2
		            WHERE (t2.PYMTTYP = 'X'
		            OR t2.PYMTTYP    IS NULL)
		            AND t2.PREPAYAMT <> 0
		            )
		        GROUP BY LOANNMB,
		            Extract(MONTH FROM POSTINGDT),
		            Extract(YEAR FROM POSTINGDT)
		        ) PYMTHISTRYTBL
		        ON LOANTBL.LOANNMB = PYMTHISTRYTBL.LOANNMB
		        LEFT JOIN
		        (SELECT * FROM stgCSA.REFCALNDRBUSDAYTBL
		        ) REFCALNDRBUSDAYTBL
		        ON PYMTHISTRYTBL.POSTINGMO  = REFCALNDRBUSDAYTBL.CALNDRBUSDAYMO
		        AND PYMTHISTRYTBL.POSTINGYR = REFCALNDRBUSDAYTBL.CALNDRBUSDAYYR
		        LEFT JOIN
		        (SELECT PRPMFGNTYTBL.PRPMFGNTYID,
		            PRPMFGNTYTBL.LOANNMB,
		            PRPMFGNTYTBL.GNTYAMT,
		            PRPMFGNTYTBL.PREPAYDT,
		            Extract(MONTH FROM PRPMFGNTYTBL.OPENGNTYDTCONVERTED) AS OPENGNTYDTCONVERTEDMO,
		            Extract(YEAR FROM PRPMFGNTYTBL.OPENGNTYDTCONVERTED)  AS OPENGNTYDTCONVERTEDYR,
		            Extract(MONTH FROM PRPMFGNTYTBL.PREPAYDT)            AS PREPAYMO,
		            Extract(YEAR FROM PRPMFGNTYTBL.PREPAYDT)             AS PREPAYYR
		        FROM stgCSA.PRPMFGNTYTBL
		        ) PRPMFGNTYTBL
		        ON PYMTHISTRYTBL.LOANNMB = PRPMFGNTYTBL.LOANNMB
		        LEFT JOIN stgCSA.TEMPGETOGERRRAWTBL
		        ON PRPMFGNTYTBL.PRPMFGNTYID                   = TEMPGETOGERRRAWTBL.GETOPENGNTYERRRAWID
		        WHERE TEMPGETOGERRRAWTBL.GETOPENGNTYERRRAWID IS NULL
		        AND LOANTBL.LOANNMB = t1.LOANNMB
		        GROUP BY PRPMFGNTYTBL.LOANNMB,
		        p_userid,
		        sysdate,
		        p_loannmb,
		        sysdate
		    ), 0)
		    WHERE t1.LOANNMB = p_loannmb;

--	</cfif>

      cur_stmt:=173;
      SELECT SUM(GNTYGNTYAMT) into sum_GNTYGNTYAMT_Total
      FROM stgcsa.SOFVGNTYTBL
      WHERE loanNmb = p_loannmb
      AND GNTYSTATCD = 1
      AND GNTYTYP IN (1,2);

      cur_stmt:=174;
      UPDATE stgCSA.PRPLOANRQSTTBL
      SET OPENGNTYRegAMT = sum_GNTYGNTYAMT_Total
      WHERE LOANNMB = p_loannmb;

      cur_stmt:=175;
      SELECT SUM(GNTYGNTYAMT) into sum_gntygntyamt
      FROM stgcsa.SOFVGNTYTBL
      WHERE loanNmb = p_loannmb
      AND GNTYSTATCD = 1
      AND GNTYTYP = 2;

/*	<!---
	<cfquery datasource="#variables.db#" name="getLoansQ" username="#variables.username#" password="#variables.password#">
	UPDATE stgCSA.PRPLOANRQSTTBL
	SET OPENGNTYErrAMT = <cfqueryparam value="#getOPENGNTYErrAMT.Total#" cfsqltype="CF_SQL_VARCHAR">
	WHERE LOANNMB = <cfqueryparam value="#arguments.loanNmb#" cfsqltype="CF_SQL_VARCHAR" >
	</cfquery>
	---> */
      cur_stmt:=176;
      UPDATE stgCSA.PRPLOANRQSTTBL
      SET OPENGNTYUNALLOCAMT = UNALLOCAMT
      WHERE OPENGNTYREGAMT > 0 AND UNALLOCAMT > 0
      AND LOANNMB = p_loannmb;

      cur_stmt:=177;
      UPDATE stgCSA.PRPLOANRQSTTBL SET OPENGNTYAMT = OPENGNTYREGAMT + OPENGNTYERRAMT 
        WHERE LOANNMB = p_loannmb;

  runtime_logger(logentryid, 'CSPMSG',801,pgm||' '||ver||' ended.  p_loannmb='
    ||p_loannmb,  p_userid,1);   
    
exception when others then    
  rollback to refreshPRPLoanRqstByLoanNmbCSP;
  p_errval:=sqlcode;
  p_errmsg:='While executing stmt '||cur_stmt||' caught oracle error '||p_errval
    ||' '||sqlerrm(sqlcode);
  runtime_logger(logentryid, 'CSPMSG',802,pgm||' '||ver||' Ended with error: '
    ||p_errmsg,  p_userid,3);      
end refreshPRPLoanRqstByLoanNmbCSP;
/


GRANT EXECUTE ON STGCSA.REFRESHPRPLOANRQSTBYLOANNMBCSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.REFRESHPRPLOANRQSTBYLOANNMBCSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.REFRESHPRPLOANRQSTBYLOANNMBCSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.REFRESHPRPLOANRQSTBYLOANNMBCSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.REFRESHPRPLOANRQSTBYLOANNMBCSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.REFRESHPRPLOANRQSTBYLOANNMBCSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.REFRESHPRPLOANRQSTBYLOANNMBCSP TO STGCSADEVROLE;

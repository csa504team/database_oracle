DROP PROCEDURE STGCSA.CNTCTSUPDTSP;

CREATE OR REPLACE PROCEDURE STGCSA.CNTCTSUPDTSP(
    p_retval out number,
    p_errval out number,
    p_errmsg  out varchar2,
    p_identifier number:=0,
    p_userid varchar2
   ,p_CDCNMB CHAR:=null
   ,p_CNTCTFIRSTNM VARCHAR2:=null
   ,p_LASTNMCNTCT VARCHAR2:=null
   ,p_CNTCTMAINPHNNMB CHAR:=null
   ,p_CNTCTSERVICINGPHNNMB CHAR:=null
   ,p_CNTCTFUNDINGPHNNMB CHAR:=null
   ,p_BOUNCEDCHK VARCHAR2:=null
   ,p_MAILDOCPOC VARCHAR2:=null
   ,p_PROBLEMLOANMTHDOFCNTCT VARCHAR2:=null
   ,p_BUSEMAILADR VARCHAR2:=null
   ,p_CNTCTBUSPHNNMB VARCHAR2:=null
   ,p_CNTCTBUSFAXNMB VARCHAR2:=null
   ,p_CNTCTCDC VARCHAR2:=null
   ,p_CNTCTMAILADRSTR1NM VARCHAR2:=null
   ,p_CNTCTMAILADRSTR2NM VARCHAR2:=null
   ,p_CNTCTMAILADRCTYNM VARCHAR2:=null
   ,p_CNTCTMAILADRSTCD CHAR:=null
   ,p_CNTCTMAILADRZIPCD CHAR:=null
   ,p_CNTCTMAILADRZIP4CD CHAR:=null
   ,p_CNTCTSID VARCHAR2:=null
   ,p_MAINCNTCT VARCHAR2:=null
   ,p_SERVICINGCNTCTNM VARCHAR2:=null
   ,p_FUNDINGCNTCT VARCHAR2:=null
   ,p_CDCID VARCHAR2:=null
) as
 /*
  Created on: 2018-11-07 11:32:29
  Created by: GENR
  Crerated from template stdtblupd_template v3.22 12 Oct 2018 on 2018-11-07 11:32:30
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  Procedure CNTCTSUPDTSP performs UPDATE on STGCSA.CNTCTSTBL
    for P_IDENTIFIER=0 qualification is on Primary Key: CNTCTSID
    Updates all columns except the qualification keys and columns for which a
      null value is passed.
    SPECIAL secret update kludge!
      if p_identifier is set to -1 then a flag is set to use all input parm values
      and p_identifier is changed to 0 (update by PK).
      THEREFORE if using p_identifier=-1 you MUST pass intended value for all columns.
      This option allows setting colulmns to null!
*/
  dbrec STGCSA.CNTCTSTBL%rowtype;
  logged_msg_id number;
  recnum number;
  rec_rowid rowid;
  keystouse varchar2(256);
  maxsev number:=0;
  pctsign char(1):='%';
  cur_colname varchar2(100):=null;
  cur_colvalue varchar2(4000);
  do_normal_field_checking char(1):='Y';
  overridden_p_identifier number;
  -- standard activity log capture vars
  l_LOANNMB char(10):=null;
  l_TRANSID number:=null;
  l_dbmdl varchar2(10):=null;
  l_entrydt date:=null;
  l_userid  varchar2(32):=null;
  l_usernm  varchar2(199):=null;
  l_timestampfld date:=null;
procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
begin
  if p_errval=0 then
    begin
      select * into dbrec from STGCSA.CNTCTSTBL where rowid=rowid_in;
    exception when others then
      p_errval:=sqlcode;
      maxsev:=3;
      p_retval:=0;
      p_errmsg:='Fetch of STGCSA.CNTCTSTBL row by rowid '||rowid_in
        ||' orinally fetched using '||orig_keyset_values
        ||' failed with '||sqlerrm(sqlcode);
    end;
  end if;
  if p_errval=0 then
    -- set this as lastupdt
    DBrec.lastupdtuserid:=p_userid;
    DBrec.lastupdtdt:=sysdate;
    -- copy into DBrec all table column input parameters that are not null,
    -- saving column name/value if needed for error reporting
    --  (if p_identifier=-1 then move input even if null)
    begin
      if P_CDCNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCNMB';
        cur_colvalue:=P_CDCNMB;
        DBrec.CDCNMB:=P_CDCNMB;
      end if;
      if P_CNTCTFIRSTNM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CNTCTFIRSTNM';
        cur_colvalue:=P_CNTCTFIRSTNM;
        DBrec.CNTCTFIRSTNM:=P_CNTCTFIRSTNM;
      end if;
      if P_LASTNMCNTCT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_LASTNMCNTCT';
        cur_colvalue:=P_LASTNMCNTCT;
        DBrec.LASTNMCNTCT:=P_LASTNMCNTCT;
      end if;
      if P_CNTCTMAINPHNNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CNTCTMAINPHNNMB';
        cur_colvalue:=P_CNTCTMAINPHNNMB;
        DBrec.CNTCTMAINPHNNMB:=P_CNTCTMAINPHNNMB;
      end if;
      if P_CNTCTSERVICINGPHNNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CNTCTSERVICINGPHNNMB';
        cur_colvalue:=P_CNTCTSERVICINGPHNNMB;
        DBrec.CNTCTSERVICINGPHNNMB:=P_CNTCTSERVICINGPHNNMB;
      end if;
      if P_CNTCTFUNDINGPHNNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CNTCTFUNDINGPHNNMB';
        cur_colvalue:=P_CNTCTFUNDINGPHNNMB;
        DBrec.CNTCTFUNDINGPHNNMB:=P_CNTCTFUNDINGPHNNMB;
      end if;
      if P_BOUNCEDCHK is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_BOUNCEDCHK';
        cur_colvalue:=P_BOUNCEDCHK;
        DBrec.BOUNCEDCHK:=P_BOUNCEDCHK;
      end if;
      if P_MAILDOCPOC is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_MAILDOCPOC';
        cur_colvalue:=P_MAILDOCPOC;
        DBrec.MAILDOCPOC:=P_MAILDOCPOC;
      end if;
      if P_PROBLEMLOANMTHDOFCNTCT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PROBLEMLOANMTHDOFCNTCT';
        cur_colvalue:=P_PROBLEMLOANMTHDOFCNTCT;
        DBrec.PROBLEMLOANMTHDOFCNTCT:=P_PROBLEMLOANMTHDOFCNTCT;
      end if;
      if P_BUSEMAILADR is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_BUSEMAILADR';
        cur_colvalue:=P_BUSEMAILADR;
        DBrec.BUSEMAILADR:=P_BUSEMAILADR;
      end if;
      if P_CNTCTBUSPHNNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CNTCTBUSPHNNMB';
        cur_colvalue:=P_CNTCTBUSPHNNMB;
        DBrec.CNTCTBUSPHNNMB:=P_CNTCTBUSPHNNMB;
      end if;
      if P_CNTCTBUSFAXNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CNTCTBUSFAXNMB';
        cur_colvalue:=P_CNTCTBUSFAXNMB;
        DBrec.CNTCTBUSFAXNMB:=P_CNTCTBUSFAXNMB;
      end if;
      if P_CNTCTCDC is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CNTCTCDC';
        cur_colvalue:=P_CNTCTCDC;
        DBrec.CNTCTCDC:=P_CNTCTCDC;
      end if;
      if P_CNTCTMAILADRSTR1NM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CNTCTMAILADRSTR1NM';
        cur_colvalue:=P_CNTCTMAILADRSTR1NM;
        DBrec.CNTCTMAILADRSTR1NM:=P_CNTCTMAILADRSTR1NM;
      end if;
      if P_CNTCTMAILADRSTR2NM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CNTCTMAILADRSTR2NM';
        cur_colvalue:=P_CNTCTMAILADRSTR2NM;
        DBrec.CNTCTMAILADRSTR2NM:=P_CNTCTMAILADRSTR2NM;
      end if;
      if P_CNTCTMAILADRCTYNM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CNTCTMAILADRCTYNM';
        cur_colvalue:=P_CNTCTMAILADRCTYNM;
        DBrec.CNTCTMAILADRCTYNM:=P_CNTCTMAILADRCTYNM;
      end if;
      if P_CNTCTMAILADRSTCD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CNTCTMAILADRSTCD';
        cur_colvalue:=P_CNTCTMAILADRSTCD;
        DBrec.CNTCTMAILADRSTCD:=P_CNTCTMAILADRSTCD;
      end if;
      if P_CNTCTMAILADRZIPCD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CNTCTMAILADRZIPCD';
        cur_colvalue:=P_CNTCTMAILADRZIPCD;
        DBrec.CNTCTMAILADRZIPCD:=P_CNTCTMAILADRZIPCD;
      end if;
      if P_CNTCTMAILADRZIP4CD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CNTCTMAILADRZIP4CD';
        cur_colvalue:=P_CNTCTMAILADRZIP4CD;
        DBrec.CNTCTMAILADRZIP4CD:=P_CNTCTMAILADRZIP4CD;
      end if;
      if P_CNTCTSID is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CNTCTSID';
        cur_colvalue:=P_CNTCTSID;
        DBrec.CNTCTSID:=P_CNTCTSID;
      end if;
      if P_MAINCNTCT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_MAINCNTCT';
        cur_colvalue:=P_MAINCNTCT;
        DBrec.MAINCNTCT:=P_MAINCNTCT;
      end if;
      if P_SERVICINGCNTCTNM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SERVICINGCNTCTNM';
        cur_colvalue:=P_SERVICINGCNTCTNM;
        DBrec.SERVICINGCNTCTNM:=P_SERVICINGCNTCTNM;
      end if;
      if P_FUNDINGCNTCT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_FUNDINGCNTCT';
        cur_colvalue:=P_FUNDINGCNTCT;
        DBrec.FUNDINGCNTCT:=P_FUNDINGCNTCT;
      end if;
      if P_CDCID is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCID';
        cur_colvalue:=P_CDCID;
        DBrec.CDCID:=P_CDCID;
      end if;
    exception when others then
      p_errval:=sqlcode;
      if maxsev<3 then maxsev:=3; end if;
      p_errmsg:='Error using '||cur_colname||' Value >'||cur_colvalue||'<';
    end;
  end if;
  -- if all still ok do actual update
  if p_errval=0 then
    Begin
      update STGCSA.CNTCTSTBL
        set lastupdtuserid=DBrec.lastupdtuserid,
          lastupdtdt=DBrec.lastupdtdt
          ,CDCNMB=DBrec.CDCNMB
          ,CNTCTFIRSTNM=DBrec.CNTCTFIRSTNM
          ,LASTNMCNTCT=DBrec.LASTNMCNTCT
          ,CNTCTMAINPHNNMB=DBrec.CNTCTMAINPHNNMB
          ,CNTCTSERVICINGPHNNMB=DBrec.CNTCTSERVICINGPHNNMB
          ,CNTCTFUNDINGPHNNMB=DBrec.CNTCTFUNDINGPHNNMB
          ,BOUNCEDCHK=DBrec.BOUNCEDCHK
          ,MAILDOCPOC=DBrec.MAILDOCPOC
          ,PROBLEMLOANMTHDOFCNTCT=DBrec.PROBLEMLOANMTHDOFCNTCT
          ,BUSEMAILADR=DBrec.BUSEMAILADR
          ,CNTCTBUSPHNNMB=DBrec.CNTCTBUSPHNNMB
          ,CNTCTBUSFAXNMB=DBrec.CNTCTBUSFAXNMB
          ,CNTCTCDC=DBrec.CNTCTCDC
          ,CNTCTMAILADRSTR1NM=DBrec.CNTCTMAILADRSTR1NM
          ,CNTCTMAILADRSTR2NM=DBrec.CNTCTMAILADRSTR2NM
          ,CNTCTMAILADRCTYNM=DBrec.CNTCTMAILADRCTYNM
          ,CNTCTMAILADRSTCD=DBrec.CNTCTMAILADRSTCD
          ,CNTCTMAILADRZIPCD=DBrec.CNTCTMAILADRZIPCD
          ,CNTCTMAILADRZIP4CD=DBrec.CNTCTMAILADRZIP4CD
          ,CNTCTSID=DBrec.CNTCTSID
          ,MAINCNTCT=DBrec.MAINCNTCT
          ,SERVICINGCNTCTNM=DBrec.SERVICINGCNTCTNM
          ,FUNDINGCNTCT=DBrec.FUNDINGCNTCT
          ,CDCID=DBrec.CDCID
        where rowid=rowid_in;
      recnum:=recnum+1;
    exception when others then
      p_errval:=sqlcode;
      p_retval:=0;
      if maxsev<3 then maxsev:=3; end if;
      p_errmsg:= 'Error while doing update on STGCSA.CNTCTSTBL, on rec with rowid '
      ||rowid_in||' previously fetched using key: '||orig_keyset_values
      ||' got Oracle error '||sqlerrm(sqlcode);
    end;
  end if;
END;
begin
  -- setup
  savepoint CNTCTSUPDTSP;
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  recnum:=0;
  runtime.logger(logged_msg_id,'STDLOG',100,'Init CNTCTSUPDTSP',p_userid,4,
    logtxt1=>'CNTCTSUPDTSP',logtxt2=>'stdtblupd_template v3.22 12 Oct 2018',
    PROGRAM_NAME=>'CNTCTSUPDTSP');
  --
--
-- special p_identifier kludge
--  if p_identifier is set to -1 then it treated as 0 (update by PK)
--  but a flag is set use passed value EVEN IF NULL.  Since for option -1
--  every column is updated to what was passed, intended values MUST be passed
--  for all columns.  This option allows setting colulmns to null!
  overridden_p_identifier:=p_identifier;
  if p_identifier=-1 then
    do_normal_field_checking:='N';
    overridden_p_identifier:=0;
  end if;
  case overridden_p_identifier
  when 0 then
    -- case to update one row based on primary key
    -- p_identifier=0 (choice 0) is default action for a stored procedure
    -- get existing version of row
    if p_errval=0 then
      keystouse:='(CNTCTSID)=('||P_CNTCTSID||')';
      begin
        select rowid into rec_rowid from STGCSA.CNTCTSTBL where 1=1
         and CNTCTSID=P_CNTCTSID;
      exception when no_data_found then
        p_errval:=0;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Warning No STGCSA.CNTCTSTBL row to update with key(s) '
          ||keystouse;
      end;
    end if;
    if p_errval=0 then
      fetch_and_update_by_rowid(rec_rowid, keystouse);
    end if;
  else /* P_IDENTIFIER was unexpected value */
    p_errval:=-20002;
    if maxsev<3 then maxsev:=3; end if;
    p_errmsg:='Error P_IDENTIFIER contained unexpected value: '||p_identifier;
  end case;
  P_RETVAL:=RECNUM;
  -- post to log
  if p_errval=0 then
    runtime.logger(logged_msg_id,'STDLOG',101,
      'End CNTCTSUPDTSP With 0 return, P_RETVAL='
      ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
      1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld
      ,PROGRAM_NAME=>'CNTCTSUPDTSP');
  else
    ROLLBACK TO CNTCTSUPDTSP;
    p_errmsg:=p_errmsg||' in CNTCTSUPDTSP build 2018-11-07 11:32:29';
    runtime.logger(logged_msg_id,'STDERR',103,p_errmsg,p_userid,
      maxsev, l_usernm, l_loannmb,l_transid, l_dbmdl, l_entrydt, l_timestampfld,
      PROGRAM_NAME=>'CNTCTSUPDTSP',force_log_entry=>true);
  end if;
EXCEPTION
WHEN OTHERS THEN
  ROLLBACK TO CNTCTSUPDTSP;
  p_errmsg:=' Error outer exception handler caught Oracle error:'||SQLERRM(SQLCODE)
    ||' in CNTCTSUPDTSP build 2018-11-07 11:32:29';
  runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
    3,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld,
    PROGRAM_NAME=>'CNTCTSUPDTSP',force_log_entry=>true);
--
END CNTCTSUPDTSP;
/


GRANT EXECUTE ON STGCSA.CNTCTSUPDTSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.CNTCTSUPDTSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.CNTCTSUPDTSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.CNTCTSUPDTSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.CNTCTSUPDTSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.CNTCTSUPDTSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.CNTCTSUPDTSP TO STGCSADEVROLE;

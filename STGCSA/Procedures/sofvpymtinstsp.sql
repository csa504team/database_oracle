DROP PROCEDURE STGCSA.SOFVPYMTINSTSP;

CREATE OR REPLACE PROCEDURE STGCSA.SOFVPYMTINSTSP(
    p_retval out number,
    p_errval out number,
    p_errmsg  out varchar2,
    p_identifier number:=0,
    p_userid varchar2
   ,p_LOANNMB CHAR:=null
   ,p_PYMTPOSTPYMTDT DATE:=null
   ,p_PYMTPOSTPYMTTYP CHAR:=null
   ,p_PYMTPOSTDUEAMT VARCHAR2:=null
   ,p_PYMTPOSTREMITTEDAMT VARCHAR2:=null
   ,p_PYMTPOSTPYMTNMB CHAR:=null
   ,p_PYMTPOSTCSAPAIDSTARTDTX CHAR:=null
   ,p_PYMTPOSTCSAPAIDTHRUDTX CHAR:=null
   ,p_PYMTPOSTCSADUEAMT VARCHAR2:=null
   ,p_PYMTPOSTCSAAPPLAMT VARCHAR2:=null
   ,p_PYMTPOSTCSAACTUALINITFEEAMT VARCHAR2:=null
   ,p_PYMTPOSTCSAREPDAMT VARCHAR2:=null
   ,p_PYMTPOSTSBAFEEREPDAMT VARCHAR2:=null
   ,p_PYMTPOSTCDCPAIDSTARTDTX CHAR:=null
   ,p_PYMTPOSTCDCPAIDTHRUDTX CHAR:=null
   ,p_PYMTPOSTCDCDUEAMT VARCHAR2:=null
   ,p_PYMTPOSTCDCAPPLAMT VARCHAR2:=null
   ,p_PYMTPOSTCDCREPDAMT VARCHAR2:=null
   ,p_PYMTPOSTSBAFEEAMT VARCHAR2:=null
   ,p_PYMTPOSTSBAFEEAPPLAMT VARCHAR2:=null
   ,p_PYMTPOSTINTPAIDSTARTDTX CHAR:=null
   ,p_PYMTPOSTINTPAIDTHRUDTX CHAR:=null
   ,p_PYMTPOSTINTBASISCALC VARCHAR2:=null
   ,p_PYMTPOSTINTRTUSEDPCT VARCHAR2:=null
   ,p_PYMTPOSTINTBALUSEDAMT VARCHAR2:=null
   ,p_PYMTPOSTINTDUEAMT VARCHAR2:=null
   ,p_PYMTPOSTINTAPPLAMT VARCHAR2:=null
   ,p_PYMTPOSTINTACTUALBASISDAYS VARCHAR2:=null
   ,p_PYMTPOSTINTACCRADJAMT VARCHAR2:=null
   ,p_PYMTPOSTINTRJCTAMT VARCHAR2:=null
   ,p_PYMTPOSTSBAFEESTARTDTX CHAR:=null
   ,p_PYMTPOSTPRINDUEAMT VARCHAR2:=null
   ,p_PYMTPOSTPRINBALUSEDAMT VARCHAR2:=null
   ,p_PYMTPOSTPRINAPPLAMT VARCHAR2:=null
   ,p_PYMTPOSTPRINBALLEFTAMT VARCHAR2:=null
   ,p_PYMTPOSTPRINRJCTAMT VARCHAR2:=null
   ,p_PYMTPOSTGNTYREPAYAMT VARCHAR2:=null
   ,p_PYMTPOSTSBAFEEENDDTX CHAR:=null
   ,p_PYMTPOSTPAIDBYCOLSONDTX CHAR:=null
   ,p_PYMTPOSTLPYMTAMT VARCHAR2:=null
   ,p_PYMTPOSTESCRCPTSTDAMT VARCHAR2:=null
   ,p_PYMTPOSTLATEFEEASSESSAMT VARCHAR2:=null
   ,p_PYMTPOSTCDCDISBAMT VARCHAR2:=null
   ,p_PYMTPOSTCDCSBADISBAMT VARCHAR2:=null
   ,p_PYMTPOSTXADJCD CHAR:=null
   ,p_PYMTPOSTLEFTOVRAMT VARCHAR2:=null
   ,p_PYMTPOSTRJCTCD CHAR:=null
   ,p_PYMTPOSTCMNT VARCHAR2:=null
   ,p_PYMTPOSTREMITTEDRJCTAMT VARCHAR2:=null
   ,p_PYMTPOSTLEFTOVRRJCTAMT VARCHAR2:=null
   ,p_PYMTPOSTRJCTDT DATE:=null
   ,p_PYMTPOSTLATEFEEPAIDAMT VARCHAR2:=null
   ,p_PYMTPOSTINTWAIVEDAMT VARCHAR2:=null
   ,p_PYMTPOSTPREPAYAMT VARCHAR2:=null
) as
 /*
  Created on: 2018-11-07 11:36:22
  Created by: GENR
  Crerated from template stdtblins_template v3.05 13 Sep 2018 on 2018-11-07 11:36:22
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  Procedure performs INSERT on STGCSA.SOFVPYMTTBLfor P_IDENTIFIER=0
*/
  rundate date;
  gn global_name.global_name%type;
  new_rec STGCSA.SOFVPYMTTBL%rowtype;
  logged_msgid_retval number;
  audretval number;
  crossover_active char(1);
  l_LOANNMB char(10):=null;
  l_TRANSID number:=null;
  l_dbmdl varchar2(10):=null;
  l_entrydt date:=null;
  l_userid  varchar2(32):=null;
  l_usernm  varchar2(199):=null;
  l_timestampfld date:=null;
  cur_col_name varchar2(100):=null;
  maxsev number:=0;
  holdstring varchar2(4000);
  keystouse varchar2(1000);
  crossover_not_active char(1);
  -- vars for checking non-defaultable columns
  onespace varchar2(10):=' ';
  ndfltmsg varchar2(300);
  v_errfnd boolean:=false;
  -- std date for column defaulting
  JAN_1_1900  constant date:=to_date('JAN-01-1900','MON-DD-YYYY');
  cur_colvalue varchar2(4095);
  cur_colname varchar2(30);
-- Main body begins here
begin
  -- setup
  savepoint SOFVPYMTINSTSP;
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  runtime.logger(logged_msgid_retval,'STDLOG',100,'Init SOFVPYMTINSTSP',p_userid,4,
    logtxt1=>'SOFVPYMTINSTSP',logtxt2=>'stdtblins_template v3.05 13 Sep 2018',
    PROGRAM_NAME=>'SOFVPYMTINSTSP');
  select global_name, sysdate into gn, rundate from global_name;
  --
  -- Check to see if crossover is ongoing, in which case updates are prohibited
  SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
  IF crossover_not_active != 'Y' THEN
    p_errval := -20099;
    p_errmsg := 'Warning: Cannot update table SOFVPYMTTBL at this time because crossover is running, please try later.';
  END IF;
  -- end of crossover_lockout_check
  begin
    cur_colname:='LOANNMB';
    cur_colvalue:=P_LOANNMB;
    l_LOANNMB:=P_LOANNMB;
  exception when others then
    p_errval:=sqlcode;
    p_errmsg:='Error '||sqlerrm(sqlcode)||' using '||cur_colname||' Value >'||cur_colvalue||'<';
  end;
  new_rec.creatuserid:=p_userid;
  new_rec.creatdt:=sysdate;
  new_rec.lastupdtuserid:=p_userid;
  new_rec.lastupdtdt:=sysdate;
  if p_identifier=0 then
    -- p_identifier=0 this is default for a stored procedure, only action for insert
    runtime.errfound:=false;
    runtime.errstr:=' ';
    --
    -- Default columns as applicable
  if p_errval=0 then
    begin
      cur_colname:='P_LOANNMB';
      cur_colvalue:=P_LOANNMB;
      if P_LOANNMB is null then
        raise_application_error(-20001,cur_colname||' may not be null');
      else
        new_rec.LOANNMB:=P_LOANNMB;
      end if;
      cur_colname:='P_PYMTPOSTPYMTDT';
      cur_colvalue:=P_PYMTPOSTPYMTDT;
      if P_PYMTPOSTPYMTDT is null then

        new_rec.PYMTPOSTPYMTDT:=jan_1_1900;

      else
        new_rec.PYMTPOSTPYMTDT:=P_PYMTPOSTPYMTDT;
      end if;
      cur_colname:='P_PYMTPOSTPYMTTYP';
      cur_colvalue:=P_PYMTPOSTPYMTTYP;
      if P_PYMTPOSTPYMTTYP is null then

        new_rec.PYMTPOSTPYMTTYP:=rpad(' ',1);

      else
        new_rec.PYMTPOSTPYMTTYP:=P_PYMTPOSTPYMTTYP;
      end if;
      cur_colname:='P_PYMTPOSTDUEAMT';
      cur_colvalue:=P_PYMTPOSTDUEAMT;
      if P_PYMTPOSTDUEAMT is null then

        new_rec.PYMTPOSTDUEAMT:=0;

      else
        new_rec.PYMTPOSTDUEAMT:=P_PYMTPOSTDUEAMT;
      end if;
      cur_colname:='P_PYMTPOSTREMITTEDAMT';
      cur_colvalue:=P_PYMTPOSTREMITTEDAMT;
      if P_PYMTPOSTREMITTEDAMT is null then

        new_rec.PYMTPOSTREMITTEDAMT:=0;

      else
        new_rec.PYMTPOSTREMITTEDAMT:=P_PYMTPOSTREMITTEDAMT;
      end if;
      cur_colname:='P_PYMTPOSTPYMTNMB';
      cur_colvalue:=P_PYMTPOSTPYMTNMB;
      if P_PYMTPOSTPYMTNMB is null then

        new_rec.PYMTPOSTPYMTNMB:=rpad(' ',3);

      else
        new_rec.PYMTPOSTPYMTNMB:=P_PYMTPOSTPYMTNMB;
      end if;
      cur_colname:='P_PYMTPOSTCSAPAIDSTARTDTX';
      cur_colvalue:=P_PYMTPOSTCSAPAIDSTARTDTX;
      if P_PYMTPOSTCSAPAIDSTARTDTX is null then

        new_rec.PYMTPOSTCSAPAIDSTARTDTX:=rpad(' ',8);

      else
        new_rec.PYMTPOSTCSAPAIDSTARTDTX:=P_PYMTPOSTCSAPAIDSTARTDTX;
      end if;
      cur_colname:='P_PYMTPOSTCSAPAIDTHRUDTX';
      cur_colvalue:=P_PYMTPOSTCSAPAIDTHRUDTX;
      if P_PYMTPOSTCSAPAIDTHRUDTX is null then

        new_rec.PYMTPOSTCSAPAIDTHRUDTX:=rpad(' ',8);

      else
        new_rec.PYMTPOSTCSAPAIDTHRUDTX:=P_PYMTPOSTCSAPAIDTHRUDTX;
      end if;
      cur_colname:='P_PYMTPOSTCSADUEAMT';
      cur_colvalue:=P_PYMTPOSTCSADUEAMT;
      if P_PYMTPOSTCSADUEAMT is null then

        new_rec.PYMTPOSTCSADUEAMT:=0;

      else
        new_rec.PYMTPOSTCSADUEAMT:=P_PYMTPOSTCSADUEAMT;
      end if;
      cur_colname:='P_PYMTPOSTCSAAPPLAMT';
      cur_colvalue:=P_PYMTPOSTCSAAPPLAMT;
      if P_PYMTPOSTCSAAPPLAMT is null then

        new_rec.PYMTPOSTCSAAPPLAMT:=0;

      else
        new_rec.PYMTPOSTCSAAPPLAMT:=P_PYMTPOSTCSAAPPLAMT;
      end if;
      cur_colname:='P_PYMTPOSTCSAACTUALINITFEEAMT';
      cur_colvalue:=P_PYMTPOSTCSAACTUALINITFEEAMT;
      if P_PYMTPOSTCSAACTUALINITFEEAMT is null then

        new_rec.PYMTPOSTCSAACTUALINITFEEAMT:=0;

      else
        new_rec.PYMTPOSTCSAACTUALINITFEEAMT:=P_PYMTPOSTCSAACTUALINITFEEAMT;
      end if;
      cur_colname:='P_PYMTPOSTCSAREPDAMT';
      cur_colvalue:=P_PYMTPOSTCSAREPDAMT;
      if P_PYMTPOSTCSAREPDAMT is null then

        new_rec.PYMTPOSTCSAREPDAMT:=0;

      else
        new_rec.PYMTPOSTCSAREPDAMT:=P_PYMTPOSTCSAREPDAMT;
      end if;
      cur_colname:='P_PYMTPOSTSBAFEEREPDAMT';
      cur_colvalue:=P_PYMTPOSTSBAFEEREPDAMT;
      if P_PYMTPOSTSBAFEEREPDAMT is null then

        new_rec.PYMTPOSTSBAFEEREPDAMT:=0;

      else
        new_rec.PYMTPOSTSBAFEEREPDAMT:=P_PYMTPOSTSBAFEEREPDAMT;
      end if;
      cur_colname:='P_PYMTPOSTCDCPAIDSTARTDTX';
      cur_colvalue:=P_PYMTPOSTCDCPAIDSTARTDTX;
      if P_PYMTPOSTCDCPAIDSTARTDTX is null then

        new_rec.PYMTPOSTCDCPAIDSTARTDTX:=rpad(' ',8);

      else
        new_rec.PYMTPOSTCDCPAIDSTARTDTX:=P_PYMTPOSTCDCPAIDSTARTDTX;
      end if;
      cur_colname:='P_PYMTPOSTCDCPAIDTHRUDTX';
      cur_colvalue:=P_PYMTPOSTCDCPAIDTHRUDTX;
      if P_PYMTPOSTCDCPAIDTHRUDTX is null then

        new_rec.PYMTPOSTCDCPAIDTHRUDTX:=rpad(' ',8);

      else
        new_rec.PYMTPOSTCDCPAIDTHRUDTX:=P_PYMTPOSTCDCPAIDTHRUDTX;
      end if;
      cur_colname:='P_PYMTPOSTCDCDUEAMT';
      cur_colvalue:=P_PYMTPOSTCDCDUEAMT;
      if P_PYMTPOSTCDCDUEAMT is null then

        new_rec.PYMTPOSTCDCDUEAMT:=0;

      else
        new_rec.PYMTPOSTCDCDUEAMT:=P_PYMTPOSTCDCDUEAMT;
      end if;
      cur_colname:='P_PYMTPOSTCDCAPPLAMT';
      cur_colvalue:=P_PYMTPOSTCDCAPPLAMT;
      if P_PYMTPOSTCDCAPPLAMT is null then

        new_rec.PYMTPOSTCDCAPPLAMT:=0;

      else
        new_rec.PYMTPOSTCDCAPPLAMT:=P_PYMTPOSTCDCAPPLAMT;
      end if;
      cur_colname:='P_PYMTPOSTCDCREPDAMT';
      cur_colvalue:=P_PYMTPOSTCDCREPDAMT;
      if P_PYMTPOSTCDCREPDAMT is null then

        new_rec.PYMTPOSTCDCREPDAMT:=0;

      else
        new_rec.PYMTPOSTCDCREPDAMT:=P_PYMTPOSTCDCREPDAMT;
      end if;
      cur_colname:='P_PYMTPOSTSBAFEEAMT';
      cur_colvalue:=P_PYMTPOSTSBAFEEAMT;
      if P_PYMTPOSTSBAFEEAMT is null then

        new_rec.PYMTPOSTSBAFEEAMT:=0;

      else
        new_rec.PYMTPOSTSBAFEEAMT:=P_PYMTPOSTSBAFEEAMT;
      end if;
      cur_colname:='P_PYMTPOSTSBAFEEAPPLAMT';
      cur_colvalue:=P_PYMTPOSTSBAFEEAPPLAMT;
      if P_PYMTPOSTSBAFEEAPPLAMT is null then

        new_rec.PYMTPOSTSBAFEEAPPLAMT:=0;

      else
        new_rec.PYMTPOSTSBAFEEAPPLAMT:=P_PYMTPOSTSBAFEEAPPLAMT;
      end if;
      cur_colname:='P_PYMTPOSTINTPAIDSTARTDTX';
      cur_colvalue:=P_PYMTPOSTINTPAIDSTARTDTX;
      if P_PYMTPOSTINTPAIDSTARTDTX is null then

        new_rec.PYMTPOSTINTPAIDSTARTDTX:=rpad(' ',8);

      else
        new_rec.PYMTPOSTINTPAIDSTARTDTX:=P_PYMTPOSTINTPAIDSTARTDTX;
      end if;
      cur_colname:='P_PYMTPOSTINTPAIDTHRUDTX';
      cur_colvalue:=P_PYMTPOSTINTPAIDTHRUDTX;
      if P_PYMTPOSTINTPAIDTHRUDTX is null then

        new_rec.PYMTPOSTINTPAIDTHRUDTX:=rpad(' ',8);

      else
        new_rec.PYMTPOSTINTPAIDTHRUDTX:=P_PYMTPOSTINTPAIDTHRUDTX;
      end if;
      cur_colname:='P_PYMTPOSTINTBASISCALC';
      cur_colvalue:=P_PYMTPOSTINTBASISCALC;
      if P_PYMTPOSTINTBASISCALC is null then

        new_rec.PYMTPOSTINTBASISCALC:=0;

      else
        new_rec.PYMTPOSTINTBASISCALC:=P_PYMTPOSTINTBASISCALC;
      end if;
      cur_colname:='P_PYMTPOSTINTRTUSEDPCT';
      cur_colvalue:=P_PYMTPOSTINTRTUSEDPCT;
      if P_PYMTPOSTINTRTUSEDPCT is null then

        new_rec.PYMTPOSTINTRTUSEDPCT:=0;

      else
        new_rec.PYMTPOSTINTRTUSEDPCT:=P_PYMTPOSTINTRTUSEDPCT;
      end if;
      cur_colname:='P_PYMTPOSTINTBALUSEDAMT';
      cur_colvalue:=P_PYMTPOSTINTBALUSEDAMT;
      if P_PYMTPOSTINTBALUSEDAMT is null then

        new_rec.PYMTPOSTINTBALUSEDAMT:=0;

      else
        new_rec.PYMTPOSTINTBALUSEDAMT:=P_PYMTPOSTINTBALUSEDAMT;
      end if;
      cur_colname:='P_PYMTPOSTINTDUEAMT';
      cur_colvalue:=P_PYMTPOSTINTDUEAMT;
      if P_PYMTPOSTINTDUEAMT is null then

        new_rec.PYMTPOSTINTDUEAMT:=0;

      else
        new_rec.PYMTPOSTINTDUEAMT:=P_PYMTPOSTINTDUEAMT;
      end if;
      cur_colname:='P_PYMTPOSTINTAPPLAMT';
      cur_colvalue:=P_PYMTPOSTINTAPPLAMT;
      if P_PYMTPOSTINTAPPLAMT is null then

        new_rec.PYMTPOSTINTAPPLAMT:=0;

      else
        new_rec.PYMTPOSTINTAPPLAMT:=P_PYMTPOSTINTAPPLAMT;
      end if;
      cur_colname:='P_PYMTPOSTINTACTUALBASISDAYS';
      cur_colvalue:=P_PYMTPOSTINTACTUALBASISDAYS;
      if P_PYMTPOSTINTACTUALBASISDAYS is null then

        new_rec.PYMTPOSTINTACTUALBASISDAYS:=0;

      else
        new_rec.PYMTPOSTINTACTUALBASISDAYS:=P_PYMTPOSTINTACTUALBASISDAYS;
      end if;
      cur_colname:='P_PYMTPOSTINTACCRADJAMT';
      cur_colvalue:=P_PYMTPOSTINTACCRADJAMT;
      if P_PYMTPOSTINTACCRADJAMT is null then

        new_rec.PYMTPOSTINTACCRADJAMT:=0;

      else
        new_rec.PYMTPOSTINTACCRADJAMT:=P_PYMTPOSTINTACCRADJAMT;
      end if;
      cur_colname:='P_PYMTPOSTINTRJCTAMT';
      cur_colvalue:=P_PYMTPOSTINTRJCTAMT;
      if P_PYMTPOSTINTRJCTAMT is null then

        new_rec.PYMTPOSTINTRJCTAMT:=0;

      else
        new_rec.PYMTPOSTINTRJCTAMT:=P_PYMTPOSTINTRJCTAMT;
      end if;
      cur_colname:='P_PYMTPOSTSBAFEESTARTDTX';
      cur_colvalue:=P_PYMTPOSTSBAFEESTARTDTX;
      if P_PYMTPOSTSBAFEESTARTDTX is null then

        new_rec.PYMTPOSTSBAFEESTARTDTX:=rpad(' ',8);

      else
        new_rec.PYMTPOSTSBAFEESTARTDTX:=P_PYMTPOSTSBAFEESTARTDTX;
      end if;
      cur_colname:='P_PYMTPOSTPRINDUEAMT';
      cur_colvalue:=P_PYMTPOSTPRINDUEAMT;
      if P_PYMTPOSTPRINDUEAMT is null then

        new_rec.PYMTPOSTPRINDUEAMT:=0;

      else
        new_rec.PYMTPOSTPRINDUEAMT:=P_PYMTPOSTPRINDUEAMT;
      end if;
      cur_colname:='P_PYMTPOSTPRINBALUSEDAMT';
      cur_colvalue:=P_PYMTPOSTPRINBALUSEDAMT;
      if P_PYMTPOSTPRINBALUSEDAMT is null then

        new_rec.PYMTPOSTPRINBALUSEDAMT:=0;

      else
        new_rec.PYMTPOSTPRINBALUSEDAMT:=P_PYMTPOSTPRINBALUSEDAMT;
      end if;
      cur_colname:='P_PYMTPOSTPRINAPPLAMT';
      cur_colvalue:=P_PYMTPOSTPRINAPPLAMT;
      if P_PYMTPOSTPRINAPPLAMT is null then

        new_rec.PYMTPOSTPRINAPPLAMT:=0;

      else
        new_rec.PYMTPOSTPRINAPPLAMT:=P_PYMTPOSTPRINAPPLAMT;
      end if;
      cur_colname:='P_PYMTPOSTPRINBALLEFTAMT';
      cur_colvalue:=P_PYMTPOSTPRINBALLEFTAMT;
      if P_PYMTPOSTPRINBALLEFTAMT is null then

        new_rec.PYMTPOSTPRINBALLEFTAMT:=0;

      else
        new_rec.PYMTPOSTPRINBALLEFTAMT:=P_PYMTPOSTPRINBALLEFTAMT;
      end if;
      cur_colname:='P_PYMTPOSTPRINRJCTAMT';
      cur_colvalue:=P_PYMTPOSTPRINRJCTAMT;
      if P_PYMTPOSTPRINRJCTAMT is null then

        new_rec.PYMTPOSTPRINRJCTAMT:=0;

      else
        new_rec.PYMTPOSTPRINRJCTAMT:=P_PYMTPOSTPRINRJCTAMT;
      end if;
      cur_colname:='P_PYMTPOSTGNTYREPAYAMT';
      cur_colvalue:=P_PYMTPOSTGNTYREPAYAMT;
      if P_PYMTPOSTGNTYREPAYAMT is null then

        new_rec.PYMTPOSTGNTYREPAYAMT:=0;

      else
        new_rec.PYMTPOSTGNTYREPAYAMT:=P_PYMTPOSTGNTYREPAYAMT;
      end if;
      cur_colname:='P_PYMTPOSTSBAFEEENDDTX';
      cur_colvalue:=P_PYMTPOSTSBAFEEENDDTX;
      if P_PYMTPOSTSBAFEEENDDTX is null then

        new_rec.PYMTPOSTSBAFEEENDDTX:=rpad(' ',8);

      else
        new_rec.PYMTPOSTSBAFEEENDDTX:=P_PYMTPOSTSBAFEEENDDTX;
      end if;
      cur_colname:='P_PYMTPOSTPAIDBYCOLSONDTX';
      cur_colvalue:=P_PYMTPOSTPAIDBYCOLSONDTX;
      if P_PYMTPOSTPAIDBYCOLSONDTX is null then

        new_rec.PYMTPOSTPAIDBYCOLSONDTX:=rpad(' ',8);

      else
        new_rec.PYMTPOSTPAIDBYCOLSONDTX:=P_PYMTPOSTPAIDBYCOLSONDTX;
      end if;
      cur_colname:='P_PYMTPOSTLPYMTAMT';
      cur_colvalue:=P_PYMTPOSTLPYMTAMT;
      if P_PYMTPOSTLPYMTAMT is null then

        new_rec.PYMTPOSTLPYMTAMT:=0;

      else
        new_rec.PYMTPOSTLPYMTAMT:=P_PYMTPOSTLPYMTAMT;
      end if;
      cur_colname:='P_PYMTPOSTESCRCPTSTDAMT';
      cur_colvalue:=P_PYMTPOSTESCRCPTSTDAMT;
      if P_PYMTPOSTESCRCPTSTDAMT is null then

        new_rec.PYMTPOSTESCRCPTSTDAMT:=0;

      else
        new_rec.PYMTPOSTESCRCPTSTDAMT:=P_PYMTPOSTESCRCPTSTDAMT;
      end if;
      cur_colname:='P_PYMTPOSTLATEFEEASSESSAMT';
      cur_colvalue:=P_PYMTPOSTLATEFEEASSESSAMT;
      if P_PYMTPOSTLATEFEEASSESSAMT is null then

        new_rec.PYMTPOSTLATEFEEASSESSAMT:=0;

      else
        new_rec.PYMTPOSTLATEFEEASSESSAMT:=P_PYMTPOSTLATEFEEASSESSAMT;
      end if;
      cur_colname:='P_PYMTPOSTCDCDISBAMT';
      cur_colvalue:=P_PYMTPOSTCDCDISBAMT;
      if P_PYMTPOSTCDCDISBAMT is null then

        new_rec.PYMTPOSTCDCDISBAMT:=0;

      else
        new_rec.PYMTPOSTCDCDISBAMT:=P_PYMTPOSTCDCDISBAMT;
      end if;
      cur_colname:='P_PYMTPOSTCDCSBADISBAMT';
      cur_colvalue:=P_PYMTPOSTCDCSBADISBAMT;
      if P_PYMTPOSTCDCSBADISBAMT is null then

        new_rec.PYMTPOSTCDCSBADISBAMT:=0;

      else
        new_rec.PYMTPOSTCDCSBADISBAMT:=P_PYMTPOSTCDCSBADISBAMT;
      end if;
      cur_colname:='P_PYMTPOSTXADJCD';
      cur_colvalue:=P_PYMTPOSTXADJCD;
      if P_PYMTPOSTXADJCD is null then

        new_rec.PYMTPOSTXADJCD:=rpad(' ',2);

      else
        new_rec.PYMTPOSTXADJCD:=P_PYMTPOSTXADJCD;
      end if;
      cur_colname:='P_PYMTPOSTLEFTOVRAMT';
      cur_colvalue:=P_PYMTPOSTLEFTOVRAMT;
      if P_PYMTPOSTLEFTOVRAMT is null then

        new_rec.PYMTPOSTLEFTOVRAMT:=0;

      else
        new_rec.PYMTPOSTLEFTOVRAMT:=P_PYMTPOSTLEFTOVRAMT;
      end if;
      cur_colname:='P_PYMTPOSTRJCTCD';
      cur_colvalue:=P_PYMTPOSTRJCTCD;
      if P_PYMTPOSTRJCTCD is null then

        new_rec.PYMTPOSTRJCTCD:=rpad(' ',3);

      else
        new_rec.PYMTPOSTRJCTCD:=P_PYMTPOSTRJCTCD;
      end if;
      cur_colname:='P_PYMTPOSTCMNT';
      cur_colvalue:=P_PYMTPOSTCMNT;
      if P_PYMTPOSTCMNT is null then

        new_rec.PYMTPOSTCMNT:=' ';

      else
        new_rec.PYMTPOSTCMNT:=P_PYMTPOSTCMNT;
      end if;
      cur_colname:='P_PYMTPOSTREMITTEDRJCTAMT';
      cur_colvalue:=P_PYMTPOSTREMITTEDRJCTAMT;
      if P_PYMTPOSTREMITTEDRJCTAMT is null then

        new_rec.PYMTPOSTREMITTEDRJCTAMT:=0;

      else
        new_rec.PYMTPOSTREMITTEDRJCTAMT:=P_PYMTPOSTREMITTEDRJCTAMT;
      end if;
      cur_colname:='P_PYMTPOSTLEFTOVRRJCTAMT';
      cur_colvalue:=P_PYMTPOSTLEFTOVRRJCTAMT;
      if P_PYMTPOSTLEFTOVRRJCTAMT is null then

        new_rec.PYMTPOSTLEFTOVRRJCTAMT:=0;

      else
        new_rec.PYMTPOSTLEFTOVRRJCTAMT:=P_PYMTPOSTLEFTOVRRJCTAMT;
      end if;
      cur_colname:='P_PYMTPOSTRJCTDT';
      cur_colvalue:=P_PYMTPOSTRJCTDT;
      if P_PYMTPOSTRJCTDT is null then

        new_rec.PYMTPOSTRJCTDT:=jan_1_1900;

      else
        new_rec.PYMTPOSTRJCTDT:=P_PYMTPOSTRJCTDT;
      end if;
      cur_colname:='P_PYMTPOSTLATEFEEPAIDAMT';
      cur_colvalue:=P_PYMTPOSTLATEFEEPAIDAMT;
      if P_PYMTPOSTLATEFEEPAIDAMT is null then

        new_rec.PYMTPOSTLATEFEEPAIDAMT:=0;

      else
        new_rec.PYMTPOSTLATEFEEPAIDAMT:=P_PYMTPOSTLATEFEEPAIDAMT;
      end if;
      cur_colname:='P_PYMTPOSTINTWAIVEDAMT';
      cur_colvalue:=P_PYMTPOSTINTWAIVEDAMT;
      if P_PYMTPOSTINTWAIVEDAMT is null then

        new_rec.PYMTPOSTINTWAIVEDAMT:=0;

      else
        new_rec.PYMTPOSTINTWAIVEDAMT:=P_PYMTPOSTINTWAIVEDAMT;
      end if;
      cur_colname:='P_PYMTPOSTPREPAYAMT';
      cur_colvalue:=P_PYMTPOSTPREPAYAMT;
      if P_PYMTPOSTPREPAYAMT is null then

        new_rec.PYMTPOSTPREPAYAMT:=0;

      else
        new_rec.PYMTPOSTPREPAYAMT:=P_PYMTPOSTPREPAYAMT;
      end if;
    exception when others then
       p_errval:=sqlcode;
       p_errmsg:='Error - Oracle returned '||sqlerrm(sqlcode)||' using '
           ||cur_colname||' Value >'||cur_colvalue||'<';
    end;
  end if;
    keystouse:='(LOANNMB)=('||new_rec.LOANNMB||')'||' and '||'(PYMTPOSTPYMTDT)=('||new_rec.PYMTPOSTPYMTDT||')';
    if p_errval=0 then
      begin
        insert into STGCSA.SOFVPYMTTBL values new_rec;
        p_RETVAL := SQL%ROWCOUNT;
      exception
      when dup_val_on_index then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Insert caused dupe on unique IX: '||sqlerrm(sqlcode);
      when others then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<3 then maxsev:=3; end if;
        p_errmsg:='Inserting record caught SQL error '||sqlerrm(sqlcode);
      end;
    end if;
  else /* P_IDENTIFIER was unexpected value */
    p_errval:=-20002;
    if maxsev<3 then maxsev:=3; end if;
    p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier
      ||' inserting record with key '||keystouse;
  end if;
  if p_errval=0 then
    runtime.logger(logged_msgid_retval,'STDLOG',101,
      'End SOFVPYMTINSTSP With 0 return, P_RETVAL='
      ||p_retval||' Record Key '||keystouse,p_userid,
      1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt,
      l_timestampfld,PROGRAM_NAME=>'SOFVPYMTINSTSP');
  else
    p_errmsg:='Error: '||P_ERRMSG||' in SOFVPYMTINSTSP Record Key '||keystouse
      ||' Build: 2018-11-07 11:36:22';
    ROLLBACK TO SOFVPYMTINSTSP;
    runtime.logger(logged_msgid_retval,'STDERR',103,
      p_errmsg,p_userid,maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,
      l_entrydt, l_timestampfld,PROGRAM_NAME=>'SOFVPYMTINSTSP',force_log_entry=>TRUE);
  end if;
EXCEPTION
WHEN OTHERS THEN
  p_errval:=SQLCODE;
  p_errmsg:=' In outer exception handler in SOFVPYMTINSTSP caught exception '
    ||sqlerrm(sqlcode)||' Record Key '||keystouse||' build 2018-11-07 11:36:22';
  ROLLBACK TO SOFVPYMTINSTSP;
  runtime.logger(logged_msgid_retval,'STDERR',20,p_errmsg,p_userid,3,
  PROGRAM_NAME=>'SOFVPYMTINSTSP',force_log_entry=>TRUE);
--
END SOFVPYMTINSTSP;
/


GRANT EXECUTE ON STGCSA.SOFVPYMTINSTSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.SOFVPYMTINSTSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.SOFVPYMTINSTSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.SOFVPYMTINSTSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.SOFVPYMTINSTSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.SOFVPYMTINSTSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.SOFVPYMTINSTSP TO STGCSADEVROLE;

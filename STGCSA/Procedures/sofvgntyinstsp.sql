DROP PROCEDURE STGCSA.SOFVGNTYINSTSP;

CREATE OR REPLACE PROCEDURE STGCSA.SOFVGNTYINSTSP(
    p_retval out number,
    p_errval out number,
    p_errmsg  out varchar2,
    p_identifier number:=0,
    p_userid varchar2
   ,p_LOANNMB CHAR:=null
   ,p_GNTYDT DATE:=null
   ,p_GNTYTYP CHAR:=null
   ,p_GNTYGNTYAMT VARCHAR2:=null
   ,p_GNTYGNTYORGLDT DATE:=null
   ,p_GNTYSTATCD CHAR:=null
   ,p_GNTYCLSDT DATE:=null
   ,p_GNTYCREATDT DATE:=null
   ,p_GNTYLASTMAINTNDT DATE:=null
   ,p_GNTYCMNT VARCHAR2:=null
   ,p_GNTYCMNT2 VARCHAR2:=null
) as
 /*
  Created on: 2018-11-07 11:35:55
  Created by: GENR
  Crerated from template stdtblins_template v3.05 13 Sep 2018 on 2018-11-07 11:35:55
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  Procedure performs INSERT on STGCSA.SOFVGNTYTBLfor P_IDENTIFIER=0
*/
  rundate date;
  gn global_name.global_name%type;
  new_rec STGCSA.SOFVGNTYTBL%rowtype;
  logged_msgid_retval number;
  audretval number;
  crossover_active char(1);
  l_LOANNMB char(10):=null;
  l_TRANSID number:=null;
  l_dbmdl varchar2(10):=null;
  l_entrydt date:=null;
  l_userid  varchar2(32):=null;
  l_usernm  varchar2(199):=null;
  l_timestampfld date:=null;
  cur_col_name varchar2(100):=null;
  maxsev number:=0;
  holdstring varchar2(4000);
  keystouse varchar2(1000);
  crossover_not_active char(1);
  -- vars for checking non-defaultable columns
  onespace varchar2(10):=' ';
  ndfltmsg varchar2(300);
  v_errfnd boolean:=false;
  -- std date for column defaulting
  JAN_1_1900  constant date:=to_date('JAN-01-1900','MON-DD-YYYY');
  cur_colvalue varchar2(4095);
  cur_colname varchar2(30);
-- Main body begins here
begin
  -- setup
  savepoint SOFVGNTYINSTSP;
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  runtime.logger(logged_msgid_retval,'STDLOG',100,'Init SOFVGNTYINSTSP',p_userid,4,
    logtxt1=>'SOFVGNTYINSTSP',logtxt2=>'stdtblins_template v3.05 13 Sep 2018',
    PROGRAM_NAME=>'SOFVGNTYINSTSP');
  select global_name, sysdate into gn, rundate from global_name;
  --
  -- Check to see if crossover is ongoing, in which case updates are prohibited
  SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
  IF crossover_not_active != 'Y' THEN
    p_errval := -20099;
    p_errmsg := 'Warning: Cannot update table SOFVGNTYTBL at this time because crossover is running, please try later.';
  END IF;
  -- end of crossover_lockout_check
  begin
    cur_colname:='LOANNMB';
    cur_colvalue:=P_LOANNMB;
    l_LOANNMB:=P_LOANNMB;
  exception when others then
    p_errval:=sqlcode;
    p_errmsg:='Error '||sqlerrm(sqlcode)||' using '||cur_colname||' Value >'||cur_colvalue||'<';
  end;
  new_rec.creatuserid:=p_userid;
  new_rec.creatdt:=sysdate;
  new_rec.lastupdtuserid:=p_userid;
  new_rec.lastupdtdt:=sysdate;
  if p_identifier=0 then
    -- p_identifier=0 this is default for a stored procedure, only action for insert
    runtime.errfound:=false;
    runtime.errstr:=' ';
    --
    -- Default columns as applicable
  if p_errval=0 then
    begin
      cur_colname:='P_LOANNMB';
      cur_colvalue:=P_LOANNMB;
      if P_LOANNMB is null then
        raise_application_error(-20001,cur_colname||' may not be null');
      else
        new_rec.LOANNMB:=P_LOANNMB;
      end if;
      cur_colname:='P_GNTYDT';
      cur_colvalue:=P_GNTYDT;
      if P_GNTYDT is null then

        new_rec.GNTYDT:=jan_1_1900;

      else
        new_rec.GNTYDT:=P_GNTYDT;
      end if;
      cur_colname:='P_GNTYTYP';
      cur_colvalue:=P_GNTYTYP;
      if P_GNTYTYP is null then

        new_rec.GNTYTYP:=rpad(' ',1);

      else
        new_rec.GNTYTYP:=P_GNTYTYP;
      end if;
      cur_colname:='P_GNTYGNTYAMT';
      cur_colvalue:=P_GNTYGNTYAMT;
      if P_GNTYGNTYAMT is null then

        new_rec.GNTYGNTYAMT:=0;

      else
        new_rec.GNTYGNTYAMT:=P_GNTYGNTYAMT;
      end if;
      cur_colname:='P_GNTYGNTYORGLDT';
      cur_colvalue:=P_GNTYGNTYORGLDT;
      if P_GNTYGNTYORGLDT is null then

        new_rec.GNTYGNTYORGLDT:=jan_1_1900;

      else
        new_rec.GNTYGNTYORGLDT:=P_GNTYGNTYORGLDT;
      end if;
      cur_colname:='P_GNTYSTATCD';
      cur_colvalue:=P_GNTYSTATCD;
      if P_GNTYSTATCD is null then

        new_rec.GNTYSTATCD:=rpad(' ',1);

      else
        new_rec.GNTYSTATCD:=P_GNTYSTATCD;
      end if;
      cur_colname:='P_GNTYCLSDT';
      cur_colvalue:=P_GNTYCLSDT;
      if P_GNTYCLSDT is null then

        new_rec.GNTYCLSDT:=jan_1_1900;

      else
        new_rec.GNTYCLSDT:=P_GNTYCLSDT;
      end if;
      cur_colname:='P_GNTYCREATDT';
      cur_colvalue:=P_GNTYCREATDT;
      if P_GNTYCREATDT is null then

        new_rec.GNTYCREATDT:=jan_1_1900;

      else
        new_rec.GNTYCREATDT:=P_GNTYCREATDT;
      end if;
      cur_colname:='P_GNTYLASTMAINTNDT';
      cur_colvalue:=P_GNTYLASTMAINTNDT;
      if P_GNTYLASTMAINTNDT is null then

        new_rec.GNTYLASTMAINTNDT:=jan_1_1900;

      else
        new_rec.GNTYLASTMAINTNDT:=P_GNTYLASTMAINTNDT;
      end if;
      cur_colname:='P_GNTYCMNT';
      cur_colvalue:=P_GNTYCMNT;
      if P_GNTYCMNT is null then

        new_rec.GNTYCMNT:=' ';

      else
        new_rec.GNTYCMNT:=P_GNTYCMNT;
      end if;
      cur_colname:='P_GNTYCMNT2';
      cur_colvalue:=P_GNTYCMNT2;
      if P_GNTYCMNT2 is null then

        new_rec.GNTYCMNT2:=' ';

      else
        new_rec.GNTYCMNT2:=P_GNTYCMNT2;
      end if;
    exception when others then
       p_errval:=sqlcode;
       p_errmsg:='Error - Oracle returned '||sqlerrm(sqlcode)||' using '
           ||cur_colname||' Value >'||cur_colvalue||'<';
    end;
  end if;
    keystouse:='(LOANNMB)=('||new_rec.LOANNMB||')'||' and '||'(GNTYDT)=('||new_rec.GNTYDT||')'||' and '||'(GNTYTYP)=('||new_rec.GNTYTYP||')';
    if p_errval=0 then
      begin
        insert into STGCSA.SOFVGNTYTBL values new_rec;
        p_RETVAL := SQL%ROWCOUNT;
      exception
      when dup_val_on_index then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Insert caused dupe on unique IX: '||sqlerrm(sqlcode);
      when others then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<3 then maxsev:=3; end if;
        p_errmsg:='Inserting record caught SQL error '||sqlerrm(sqlcode);
      end;
    end if;
  else /* P_IDENTIFIER was unexpected value */
    p_errval:=-20002;
    if maxsev<3 then maxsev:=3; end if;
    p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier
      ||' inserting record with key '||keystouse;
  end if;
  if p_errval=0 then
    runtime.logger(logged_msgid_retval,'STDLOG',101,
      'End SOFVGNTYINSTSP With 0 return, P_RETVAL='
      ||p_retval||' Record Key '||keystouse,p_userid,
      1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt,
      l_timestampfld,PROGRAM_NAME=>'SOFVGNTYINSTSP');
  else
    p_errmsg:='Error: '||P_ERRMSG||' in SOFVGNTYINSTSP Record Key '||keystouse
      ||' Build: 2018-11-07 11:35:55';
    ROLLBACK TO SOFVGNTYINSTSP;
    runtime.logger(logged_msgid_retval,'STDERR',103,
      p_errmsg,p_userid,maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,
      l_entrydt, l_timestampfld,PROGRAM_NAME=>'SOFVGNTYINSTSP',force_log_entry=>TRUE);
  end if;
EXCEPTION
WHEN OTHERS THEN
  p_errval:=SQLCODE;
  p_errmsg:=' In outer exception handler in SOFVGNTYINSTSP caught exception '
    ||sqlerrm(sqlcode)||' Record Key '||keystouse||' build 2018-11-07 11:35:55';
  ROLLBACK TO SOFVGNTYINSTSP;
  runtime.logger(logged_msgid_retval,'STDERR',20,p_errmsg,p_userid,3,
  PROGRAM_NAME=>'SOFVGNTYINSTSP',force_log_entry=>TRUE);
--
END SOFVGNTYINSTSP;
/


GRANT EXECUTE ON STGCSA.SOFVGNTYINSTSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.SOFVGNTYINSTSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.SOFVGNTYINSTSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.SOFVGNTYINSTSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.SOFVGNTYINSTSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.SOFVGNTYINSTSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.SOFVGNTYINSTSP TO STGCSADEVROLE;

DROP PROCEDURE STGCSA.CNTCTROLEINSTSP;

CREATE OR REPLACE PROCEDURE STGCSA.CNTCTROLEINSTSP(
    p_retval out number,
    p_errval out number,
    p_errmsg  out varchar2,
    p_identifier number:=0,
    p_userid varchar2
   ,p_CDCCNTCTROLEID VARCHAR2:=null
   ,p_CNTCTID VARCHAR2:=null
   ,p_ROLEID VARCHAR2:=null
   ,p_CNTCTACTV VARCHAR2:=null
   ,p_CREATBY VARCHAR2:=null
) as
 /*
  Created on: 2018-11-07 11:32:27
  Created by: GENR
  Crerated from template stdtblins_template v3.05 13 Sep 2018 on 2018-11-07 11:32:27
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  Procedure performs INSERT on STGCSA.CNTCTROLETBLfor P_IDENTIFIER=0
*/
  rundate date;
  gn global_name.global_name%type;
  new_rec STGCSA.CNTCTROLETBL%rowtype;
  logged_msgid_retval number;
  audretval number;
  crossover_active char(1);
  l_LOANNMB char(10):=null;
  l_TRANSID number:=null;
  l_dbmdl varchar2(10):=null;
  l_entrydt date:=null;
  l_userid  varchar2(32):=null;
  l_usernm  varchar2(199):=null;
  l_timestampfld date:=null;
  cur_col_name varchar2(100):=null;
  maxsev number:=0;
  holdstring varchar2(4000);
  keystouse varchar2(1000);
  -- std date for column defaulting
  JAN_1_1900  constant date:=to_date('JAN-01-1900','MON-DD-YYYY');
  cur_colvalue varchar2(4095);
  cur_colname varchar2(30);
-- Function used to initialize CDCCNTCTROLEID from sequence CDCCNTCTROLEIDSEQ
Function NEXTVAL_CDCCNTCTROLEIDSEQ return number is
  N number;
begin
  select CDCCNTCTROLEIDSEQ.nextval into n from dual;
  return n;
end;
-- Main body begins here
begin
  -- setup
  savepoint CNTCTROLEINSTSP;
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  runtime.logger(logged_msgid_retval,'STDLOG',100,'Init CNTCTROLEINSTSP',p_userid,4,
    logtxt1=>'CNTCTROLEINSTSP',logtxt2=>'stdtblins_template v3.05 13 Sep 2018',
    PROGRAM_NAME=>'CNTCTROLEINSTSP');
  select global_name, sysdate into gn, rundate from global_name;
  new_rec.creatuserid:=p_userid;
  new_rec.creatdt:=sysdate;
  new_rec.lastupdtuserid:=p_userid;
  new_rec.lastupdtdt:=sysdate;
  if p_identifier=0 then
    -- p_identifier=0 this is default for a stored procedure, only action for insert
    runtime.errfound:=false;
    runtime.errstr:=' ';
    --
    -- Default columns as applicable
  if p_errval=0 then
    begin
      cur_colname:='P_CDCCNTCTROLEID';
      cur_colvalue:=P_CDCCNTCTROLEID;
      if P_CDCCNTCTROLEID is null then

        new_rec.CDCCNTCTROLEID:=NEXTVAL_CDCCNTCTROLEIDSEQ();

      else
        new_rec.CDCCNTCTROLEID:=P_CDCCNTCTROLEID;
      end if;
      cur_colname:='P_CNTCTID';
      cur_colvalue:=P_CNTCTID;
      if P_CNTCTID is null then

        new_rec.CNTCTID:=0;

      else
        new_rec.CNTCTID:=P_CNTCTID;
      end if;
      cur_colname:='P_ROLEID';
      cur_colvalue:=P_ROLEID;
      if P_ROLEID is null then

        new_rec.ROLEID:=0;

      else
        new_rec.ROLEID:=P_ROLEID;
      end if;
      cur_colname:='P_CNTCTACTV';
      cur_colvalue:=P_CNTCTACTV;
      if P_CNTCTACTV is null then

        new_rec.CNTCTACTV:=0;

      else
        new_rec.CNTCTACTV:=P_CNTCTACTV;
      end if;
      cur_colname:='P_CREATBY';
      cur_colvalue:=P_CREATBY;
      if P_CREATBY is null then

        new_rec.CREATBY:=0;

      else
        new_rec.CREATBY:=P_CREATBY;
      end if;
    exception when others then
       p_errval:=sqlcode;
       p_errmsg:='Error - Oracle returned '||sqlerrm(sqlcode)||' using '
           ||cur_colname||' Value >'||cur_colvalue||'<';
    end;
  end if;
    keystouse:='(CDCCNTCTROLEID)=('||new_rec.CDCCNTCTROLEID||')';
    if p_errval=0 then
      begin
        insert into STGCSA.CNTCTROLETBL values new_rec;
        p_RETVAL := SQL%ROWCOUNT;
      exception
      when dup_val_on_index then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Insert caused dupe on unique IX: '||sqlerrm(sqlcode);
      when others then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<3 then maxsev:=3; end if;
        p_errmsg:='Inserting record caught SQL error '||sqlerrm(sqlcode);
      end;
    end if;
  else /* P_IDENTIFIER was unexpected value */
    p_errval:=-20002;
    if maxsev<3 then maxsev:=3; end if;
    p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier
      ||' inserting record with key '||keystouse;
  end if;
  if p_errval=0 then
    runtime.logger(logged_msgid_retval,'STDLOG',101,
      'End CNTCTROLEINSTSP With 0 return, P_RETVAL='
      ||p_retval||' Record Key '||keystouse,p_userid,
      1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt,
      l_timestampfld,PROGRAM_NAME=>'CNTCTROLEINSTSP');
  else
    p_errmsg:='Error: '||P_ERRMSG||' in CNTCTROLEINSTSP Record Key '||keystouse
      ||' Build: 2018-11-07 11:32:27';
    ROLLBACK TO CNTCTROLEINSTSP;
    runtime.logger(logged_msgid_retval,'STDERR',103,
      p_errmsg,p_userid,maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,
      l_entrydt, l_timestampfld,PROGRAM_NAME=>'CNTCTROLEINSTSP',force_log_entry=>TRUE);
  end if;
EXCEPTION
WHEN OTHERS THEN
  p_errval:=SQLCODE;
  p_errmsg:=' In outer exception handler in CNTCTROLEINSTSP caught exception '
    ||sqlerrm(sqlcode)||' Record Key '||keystouse||' build 2018-11-07 11:32:27';
  ROLLBACK TO CNTCTROLEINSTSP;
  runtime.logger(logged_msgid_retval,'STDERR',20,p_errmsg,p_userid,3,
  PROGRAM_NAME=>'CNTCTROLEINSTSP',force_log_entry=>TRUE);
--
END CNTCTROLEINSTSP;
/


GRANT EXECUTE ON STGCSA.CNTCTROLEINSTSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.CNTCTROLEINSTSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.CNTCTROLEINSTSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.CNTCTROLEINSTSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.CNTCTROLEINSTSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.CNTCTROLEINSTSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.CNTCTROLEINSTSP TO STGCSADEVROLE;

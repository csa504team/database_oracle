DROP PROCEDURE STGCSA.STGLOGGERCSP;

CREATE OR REPLACE PROCEDURE STGCSA.stgloggercsp(
  p_retval     out   number,
  p_errval     out   number,
  p_errmsg     out   varchar2,
  p_identifier number:=0,
  p_userid varchar2,
  logentryid   out   number,
  msg_id number,
  msg_txt varchar2,
  log_sev number:=1,
  usernm varchar2:=null,
  program_name varchar2:=null,
  msg_cat varchar2:='CF504',
  loannmb char:=null,
  transid number:=null,
  dbmdl varchar:=null,
  entrydt date:=null,
  timestampfield date:=null,
  logtxt1 varchar2:=null,
  logtxt2 varchar2:=null,
  logtxt3 varchar2:=null,
  logtxt4 varchar2:=null,
  logtxt5 varchar2:=null,
  lognmb1 number:=null,
  lognmb2 number:=null,
  lognmb3 number:=null,
  lognmb4 number:=null,
  lognmb5 number:=null,
  logdt1 date:=null,
  logdt2 date:=null,
  logdt3 date:=null,
  logdt4 date:=null,
  logdt5 date:=null,
  force_log char:='N') as
  force_opt boolean;
begin
  P_RETVAL:=0;
  P_ERRVAL:=0;
  P_ERRMSG:='';
  case upper(force_log)
      when 'Y' then force_opt:=TRUE;
      when 'N' then force_opt:=FALSE;
    end case;
  runtime.logger(logentryid, msg_cat, msg_id, msg_txt, p_userid,
      log_sev, usernm,
      -- no program_name,
      loannmb, transid, dbmdl, entrydt, timestampfield,
      logtxt1, logtxt2, logtxt3, logtxt4, logtxt5,
      lognmb1, lognmb2, lognmb3, lognmb4, lognmb5,
      logdt1, logdt2, logdt3, logdt4, logdt5
      -- no force_opt
      );
  return;
end stgloggercsp;
/

DROP PROCEDURE STGCSA.TOGGLE_DATAHIST_ON;

CREATE OR REPLACE PROCEDURE STGCSA.toggle_datahist_on as
begin
  runtime.toggle_datahist_logging_on_yn('Y');
end;
/

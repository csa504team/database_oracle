DROP PROCEDURE STGCSA.SOFVBGDFINSTSP;

CREATE OR REPLACE PROCEDURE STGCSA.SOFVBGDFINSTSP(
    p_retval out number,
    p_errval out number,
    p_errmsg  out varchar2,
    p_identifier number:=0,
    p_userid varchar2
   ,p_DEFRMNTGPKEY CHAR:=null
   ,p_DEFRMNTSTRTDT1 DATE:=null
   ,p_DEFRMNTENDDT1 DATE:=null
   ,p_DEFRMNTREQ1AMT VARCHAR2:=null
   ,p_DEFRMNTSTRTDT2 DATE:=null
   ,p_DEFRMNTENDDT2 DATE:=null
   ,p_DEFRMNTREQ2AMT VARCHAR2:=null
   ,p_DEFRMNTSTRTDT3 DATE:=null
   ,p_DEFRMNTENDDT3 DATE:=null
   ,p_DEFRMNTREQ3AMT VARCHAR2:=null
   ,p_DEFRMNTSTRTDT4 DATE:=null
   ,p_DEFRMNTENDDT4 DATE:=null
   ,p_DEFRMNTREQ4AMT VARCHAR2:=null
   ,p_DEFRMNTSTRTDT5 DATE:=null
   ,p_DEFRMNTENDDT5 DATE:=null
   ,p_DEFRMNTREQ5AMT VARCHAR2:=null
   ,p_DEFRMNTSTRTDT6 DATE:=null
   ,p_DEFRMNTENDDT6 DATE:=null
   ,p_DEFRMNTREQ6AMT VARCHAR2:=null
   ,p_DEFRMNTSTRTDT7 DATE:=null
   ,p_DEFRMNTENDDT7 DATE:=null
   ,p_DEFRMNTREQ7AMT VARCHAR2:=null
   ,p_DEFRMNTSTRTDT8 DATE:=null
   ,p_DEFRMNTENDDT8 DATE:=null
   ,p_DEFRMNTREQ8AMT VARCHAR2:=null
   ,p_DEFRMNTSTRTDT9 DATE:=null
   ,p_DEFRMNTENDDT9 DATE:=null
   ,p_DEFRMNTREQ9AMT VARCHAR2:=null
   ,p_DEFRMNTSTRTDT10 DATE:=null
   ,p_DEFRMNTENDDT10 DATE:=null
   ,p_DEFRMNTREQ10AMT VARCHAR2:=null
   ,p_DEFRMNTSTRTDT11 DATE:=null
   ,p_DEFRMNTENDDT11 DATE:=null
   ,p_DEFRMNTREQ11AMT VARCHAR2:=null
   ,p_DEFRMNTSTRTDT12 DATE:=null
   ,p_DEFRMNTENDDT12 DATE:=null
   ,p_DEFRMNTREQ12AMT VARCHAR2:=null
   ,p_DEFRMNTSTRTDT13 DATE:=null
   ,p_DEFRMNTENDDT13 DATE:=null
   ,p_DEFRMNTREQ13AMT VARCHAR2:=null
   ,p_DEFRMNTSTRTDT14 DATE:=null
   ,p_DEFRMNTENDDT14 DATE:=null
   ,p_DEFRMNTREQ14AMT VARCHAR2:=null
   ,p_DEFRMNTSTRTDT15 DATE:=null
   ,p_DEFRMNTENDDT15 DATE:=null
   ,p_DEFRMNTREQ15AMT VARCHAR2:=null
   ,p_DEFRMNTDTCREAT DATE:=null
   ,p_DEFRMNTDTMOD DATE:=null
) as
 /*
  Created on: 2018-11-07 11:35:25
  Created by: GENR
  Crerated from template stdtblins_template v3.05 13 Sep 2018 on 2018-11-07 11:35:25
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  Procedure performs INSERT on STGCSA.SOFVBGDFTBLfor P_IDENTIFIER=0
*/
  rundate date;
  gn global_name.global_name%type;
  new_rec STGCSA.SOFVBGDFTBL%rowtype;
  logged_msgid_retval number;
  audretval number;
  crossover_active char(1);
  l_LOANNMB char(10):=null;
  l_TRANSID number:=null;
  l_dbmdl varchar2(10):=null;
  l_entrydt date:=null;
  l_userid  varchar2(32):=null;
  l_usernm  varchar2(199):=null;
  l_timestampfld date:=null;
  cur_col_name varchar2(100):=null;
  maxsev number:=0;
  holdstring varchar2(4000);
  keystouse varchar2(1000);
  crossover_not_active char(1);
  -- std date for column defaulting
  JAN_1_1900  constant date:=to_date('JAN-01-1900','MON-DD-YYYY');
  cur_colvalue varchar2(4095);
  cur_colname varchar2(30);
-- Main body begins here
begin
  -- setup
  savepoint SOFVBGDFINSTSP;
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  runtime.logger(logged_msgid_retval,'STDLOG',100,'Init SOFVBGDFINSTSP',p_userid,4,
    logtxt1=>'SOFVBGDFINSTSP',logtxt2=>'stdtblins_template v3.05 13 Sep 2018',
    PROGRAM_NAME=>'SOFVBGDFINSTSP');
  select global_name, sysdate into gn, rundate from global_name;
  --
  -- Check to see if crossover is ongoing, in which case updates are prohibited
  SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
  IF crossover_not_active != 'Y' THEN
    p_errval := -20099;
    p_errmsg := 'Warning: Cannot update table SOFVBGDFTBL at this time because crossover is running, please try later.';
  END IF;
  -- end of crossover_lockout_check
  new_rec.creatuserid:=p_userid;
  new_rec.creatdt:=sysdate;
  new_rec.lastupdtuserid:=p_userid;
  new_rec.lastupdtdt:=sysdate;
  if p_identifier=0 then
    -- p_identifier=0 this is default for a stored procedure, only action for insert
    runtime.errfound:=false;
    runtime.errstr:=' ';
    --
    -- Default columns as applicable
  if p_errval=0 then
    begin
      cur_colname:='P_DEFRMNTGPKEY';
      cur_colvalue:=P_DEFRMNTGPKEY;
      if P_DEFRMNTGPKEY is null then

        new_rec.DEFRMNTGPKEY:=rpad(' ',10);

      else
        new_rec.DEFRMNTGPKEY:=P_DEFRMNTGPKEY;
      end if;
      cur_colname:='P_DEFRMNTSTRTDT1';
      cur_colvalue:=P_DEFRMNTSTRTDT1;
      if P_DEFRMNTSTRTDT1 is null then

        new_rec.DEFRMNTSTRTDT1:=jan_1_1900;

      else
        new_rec.DEFRMNTSTRTDT1:=P_DEFRMNTSTRTDT1;
      end if;
      cur_colname:='P_DEFRMNTENDDT1';
      cur_colvalue:=P_DEFRMNTENDDT1;
      if P_DEFRMNTENDDT1 is null then

        new_rec.DEFRMNTENDDT1:=jan_1_1900;

      else
        new_rec.DEFRMNTENDDT1:=P_DEFRMNTENDDT1;
      end if;
      cur_colname:='P_DEFRMNTREQ1AMT';
      cur_colvalue:=P_DEFRMNTREQ1AMT;
      if P_DEFRMNTREQ1AMT is null then

        new_rec.DEFRMNTREQ1AMT:=0;

      else
        new_rec.DEFRMNTREQ1AMT:=P_DEFRMNTREQ1AMT;
      end if;
      cur_colname:='P_DEFRMNTSTRTDT2';
      cur_colvalue:=P_DEFRMNTSTRTDT2;
      if P_DEFRMNTSTRTDT2 is null then

        new_rec.DEFRMNTSTRTDT2:=jan_1_1900;

      else
        new_rec.DEFRMNTSTRTDT2:=P_DEFRMNTSTRTDT2;
      end if;
      cur_colname:='P_DEFRMNTENDDT2';
      cur_colvalue:=P_DEFRMNTENDDT2;
      if P_DEFRMNTENDDT2 is null then

        new_rec.DEFRMNTENDDT2:=jan_1_1900;

      else
        new_rec.DEFRMNTENDDT2:=P_DEFRMNTENDDT2;
      end if;
      cur_colname:='P_DEFRMNTREQ2AMT';
      cur_colvalue:=P_DEFRMNTREQ2AMT;
      if P_DEFRMNTREQ2AMT is null then

        new_rec.DEFRMNTREQ2AMT:=0;

      else
        new_rec.DEFRMNTREQ2AMT:=P_DEFRMNTREQ2AMT;
      end if;
      cur_colname:='P_DEFRMNTSTRTDT3';
      cur_colvalue:=P_DEFRMNTSTRTDT3;
      if P_DEFRMNTSTRTDT3 is null then

        new_rec.DEFRMNTSTRTDT3:=jan_1_1900;

      else
        new_rec.DEFRMNTSTRTDT3:=P_DEFRMNTSTRTDT3;
      end if;
      cur_colname:='P_DEFRMNTENDDT3';
      cur_colvalue:=P_DEFRMNTENDDT3;
      if P_DEFRMNTENDDT3 is null then

        new_rec.DEFRMNTENDDT3:=jan_1_1900;

      else
        new_rec.DEFRMNTENDDT3:=P_DEFRMNTENDDT3;
      end if;
      cur_colname:='P_DEFRMNTREQ3AMT';
      cur_colvalue:=P_DEFRMNTREQ3AMT;
      if P_DEFRMNTREQ3AMT is null then

        new_rec.DEFRMNTREQ3AMT:=0;

      else
        new_rec.DEFRMNTREQ3AMT:=P_DEFRMNTREQ3AMT;
      end if;
      cur_colname:='P_DEFRMNTSTRTDT4';
      cur_colvalue:=P_DEFRMNTSTRTDT4;
      if P_DEFRMNTSTRTDT4 is null then

        new_rec.DEFRMNTSTRTDT4:=jan_1_1900;

      else
        new_rec.DEFRMNTSTRTDT4:=P_DEFRMNTSTRTDT4;
      end if;
      cur_colname:='P_DEFRMNTENDDT4';
      cur_colvalue:=P_DEFRMNTENDDT4;
      if P_DEFRMNTENDDT4 is null then

        new_rec.DEFRMNTENDDT4:=jan_1_1900;

      else
        new_rec.DEFRMNTENDDT4:=P_DEFRMNTENDDT4;
      end if;
      cur_colname:='P_DEFRMNTREQ4AMT';
      cur_colvalue:=P_DEFRMNTREQ4AMT;
      if P_DEFRMNTREQ4AMT is null then

        new_rec.DEFRMNTREQ4AMT:=0;

      else
        new_rec.DEFRMNTREQ4AMT:=P_DEFRMNTREQ4AMT;
      end if;
      cur_colname:='P_DEFRMNTSTRTDT5';
      cur_colvalue:=P_DEFRMNTSTRTDT5;
      if P_DEFRMNTSTRTDT5 is null then

        new_rec.DEFRMNTSTRTDT5:=jan_1_1900;

      else
        new_rec.DEFRMNTSTRTDT5:=P_DEFRMNTSTRTDT5;
      end if;
      cur_colname:='P_DEFRMNTENDDT5';
      cur_colvalue:=P_DEFRMNTENDDT5;
      if P_DEFRMNTENDDT5 is null then

        new_rec.DEFRMNTENDDT5:=jan_1_1900;

      else
        new_rec.DEFRMNTENDDT5:=P_DEFRMNTENDDT5;
      end if;
      cur_colname:='P_DEFRMNTREQ5AMT';
      cur_colvalue:=P_DEFRMNTREQ5AMT;
      if P_DEFRMNTREQ5AMT is null then

        new_rec.DEFRMNTREQ5AMT:=0;

      else
        new_rec.DEFRMNTREQ5AMT:=P_DEFRMNTREQ5AMT;
      end if;
      cur_colname:='P_DEFRMNTSTRTDT6';
      cur_colvalue:=P_DEFRMNTSTRTDT6;
      if P_DEFRMNTSTRTDT6 is null then

        new_rec.DEFRMNTSTRTDT6:=jan_1_1900;

      else
        new_rec.DEFRMNTSTRTDT6:=P_DEFRMNTSTRTDT6;
      end if;
      cur_colname:='P_DEFRMNTENDDT6';
      cur_colvalue:=P_DEFRMNTENDDT6;
      if P_DEFRMNTENDDT6 is null then

        new_rec.DEFRMNTENDDT6:=jan_1_1900;

      else
        new_rec.DEFRMNTENDDT6:=P_DEFRMNTENDDT6;
      end if;
      cur_colname:='P_DEFRMNTREQ6AMT';
      cur_colvalue:=P_DEFRMNTREQ6AMT;
      if P_DEFRMNTREQ6AMT is null then

        new_rec.DEFRMNTREQ6AMT:=0;

      else
        new_rec.DEFRMNTREQ6AMT:=P_DEFRMNTREQ6AMT;
      end if;
      cur_colname:='P_DEFRMNTSTRTDT7';
      cur_colvalue:=P_DEFRMNTSTRTDT7;
      if P_DEFRMNTSTRTDT7 is null then

        new_rec.DEFRMNTSTRTDT7:=jan_1_1900;

      else
        new_rec.DEFRMNTSTRTDT7:=P_DEFRMNTSTRTDT7;
      end if;
      cur_colname:='P_DEFRMNTENDDT7';
      cur_colvalue:=P_DEFRMNTENDDT7;
      if P_DEFRMNTENDDT7 is null then

        new_rec.DEFRMNTENDDT7:=jan_1_1900;

      else
        new_rec.DEFRMNTENDDT7:=P_DEFRMNTENDDT7;
      end if;
      cur_colname:='P_DEFRMNTREQ7AMT';
      cur_colvalue:=P_DEFRMNTREQ7AMT;
      if P_DEFRMNTREQ7AMT is null then

        new_rec.DEFRMNTREQ7AMT:=0;

      else
        new_rec.DEFRMNTREQ7AMT:=P_DEFRMNTREQ7AMT;
      end if;
      cur_colname:='P_DEFRMNTSTRTDT8';
      cur_colvalue:=P_DEFRMNTSTRTDT8;
      if P_DEFRMNTSTRTDT8 is null then

        new_rec.DEFRMNTSTRTDT8:=jan_1_1900;

      else
        new_rec.DEFRMNTSTRTDT8:=P_DEFRMNTSTRTDT8;
      end if;
      cur_colname:='P_DEFRMNTENDDT8';
      cur_colvalue:=P_DEFRMNTENDDT8;
      if P_DEFRMNTENDDT8 is null then

        new_rec.DEFRMNTENDDT8:=jan_1_1900;

      else
        new_rec.DEFRMNTENDDT8:=P_DEFRMNTENDDT8;
      end if;
      cur_colname:='P_DEFRMNTREQ8AMT';
      cur_colvalue:=P_DEFRMNTREQ8AMT;
      if P_DEFRMNTREQ8AMT is null then

        new_rec.DEFRMNTREQ8AMT:=0;

      else
        new_rec.DEFRMNTREQ8AMT:=P_DEFRMNTREQ8AMT;
      end if;
      cur_colname:='P_DEFRMNTSTRTDT9';
      cur_colvalue:=P_DEFRMNTSTRTDT9;
      if P_DEFRMNTSTRTDT9 is null then

        new_rec.DEFRMNTSTRTDT9:=jan_1_1900;

      else
        new_rec.DEFRMNTSTRTDT9:=P_DEFRMNTSTRTDT9;
      end if;
      cur_colname:='P_DEFRMNTENDDT9';
      cur_colvalue:=P_DEFRMNTENDDT9;
      if P_DEFRMNTENDDT9 is null then

        new_rec.DEFRMNTENDDT9:=jan_1_1900;

      else
        new_rec.DEFRMNTENDDT9:=P_DEFRMNTENDDT9;
      end if;
      cur_colname:='P_DEFRMNTREQ9AMT';
      cur_colvalue:=P_DEFRMNTREQ9AMT;
      if P_DEFRMNTREQ9AMT is null then

        new_rec.DEFRMNTREQ9AMT:=0;

      else
        new_rec.DEFRMNTREQ9AMT:=P_DEFRMNTREQ9AMT;
      end if;
      cur_colname:='P_DEFRMNTSTRTDT10';
      cur_colvalue:=P_DEFRMNTSTRTDT10;
      if P_DEFRMNTSTRTDT10 is null then

        new_rec.DEFRMNTSTRTDT10:=jan_1_1900;

      else
        new_rec.DEFRMNTSTRTDT10:=P_DEFRMNTSTRTDT10;
      end if;
      cur_colname:='P_DEFRMNTENDDT10';
      cur_colvalue:=P_DEFRMNTENDDT10;
      if P_DEFRMNTENDDT10 is null then

        new_rec.DEFRMNTENDDT10:=jan_1_1900;

      else
        new_rec.DEFRMNTENDDT10:=P_DEFRMNTENDDT10;
      end if;
      cur_colname:='P_DEFRMNTREQ10AMT';
      cur_colvalue:=P_DEFRMNTREQ10AMT;
      if P_DEFRMNTREQ10AMT is null then

        new_rec.DEFRMNTREQ10AMT:=0;

      else
        new_rec.DEFRMNTREQ10AMT:=P_DEFRMNTREQ10AMT;
      end if;
      cur_colname:='P_DEFRMNTSTRTDT11';
      cur_colvalue:=P_DEFRMNTSTRTDT11;
      if P_DEFRMNTSTRTDT11 is null then

        new_rec.DEFRMNTSTRTDT11:=jan_1_1900;

      else
        new_rec.DEFRMNTSTRTDT11:=P_DEFRMNTSTRTDT11;
      end if;
      cur_colname:='P_DEFRMNTENDDT11';
      cur_colvalue:=P_DEFRMNTENDDT11;
      if P_DEFRMNTENDDT11 is null then

        new_rec.DEFRMNTENDDT11:=jan_1_1900;

      else
        new_rec.DEFRMNTENDDT11:=P_DEFRMNTENDDT11;
      end if;
      cur_colname:='P_DEFRMNTREQ11AMT';
      cur_colvalue:=P_DEFRMNTREQ11AMT;
      if P_DEFRMNTREQ11AMT is null then

        new_rec.DEFRMNTREQ11AMT:=0;

      else
        new_rec.DEFRMNTREQ11AMT:=P_DEFRMNTREQ11AMT;
      end if;
      cur_colname:='P_DEFRMNTSTRTDT12';
      cur_colvalue:=P_DEFRMNTSTRTDT12;
      if P_DEFRMNTSTRTDT12 is null then

        new_rec.DEFRMNTSTRTDT12:=jan_1_1900;

      else
        new_rec.DEFRMNTSTRTDT12:=P_DEFRMNTSTRTDT12;
      end if;
      cur_colname:='P_DEFRMNTENDDT12';
      cur_colvalue:=P_DEFRMNTENDDT12;
      if P_DEFRMNTENDDT12 is null then

        new_rec.DEFRMNTENDDT12:=jan_1_1900;

      else
        new_rec.DEFRMNTENDDT12:=P_DEFRMNTENDDT12;
      end if;
      cur_colname:='P_DEFRMNTREQ12AMT';
      cur_colvalue:=P_DEFRMNTREQ12AMT;
      if P_DEFRMNTREQ12AMT is null then

        new_rec.DEFRMNTREQ12AMT:=0;

      else
        new_rec.DEFRMNTREQ12AMT:=P_DEFRMNTREQ12AMT;
      end if;
      cur_colname:='P_DEFRMNTSTRTDT13';
      cur_colvalue:=P_DEFRMNTSTRTDT13;
      if P_DEFRMNTSTRTDT13 is null then

        new_rec.DEFRMNTSTRTDT13:=jan_1_1900;

      else
        new_rec.DEFRMNTSTRTDT13:=P_DEFRMNTSTRTDT13;
      end if;
      cur_colname:='P_DEFRMNTENDDT13';
      cur_colvalue:=P_DEFRMNTENDDT13;
      if P_DEFRMNTENDDT13 is null then

        new_rec.DEFRMNTENDDT13:=jan_1_1900;

      else
        new_rec.DEFRMNTENDDT13:=P_DEFRMNTENDDT13;
      end if;
      cur_colname:='P_DEFRMNTREQ13AMT';
      cur_colvalue:=P_DEFRMNTREQ13AMT;
      if P_DEFRMNTREQ13AMT is null then

        new_rec.DEFRMNTREQ13AMT:=0;

      else
        new_rec.DEFRMNTREQ13AMT:=P_DEFRMNTREQ13AMT;
      end if;
      cur_colname:='P_DEFRMNTSTRTDT14';
      cur_colvalue:=P_DEFRMNTSTRTDT14;
      if P_DEFRMNTSTRTDT14 is null then

        new_rec.DEFRMNTSTRTDT14:=jan_1_1900;

      else
        new_rec.DEFRMNTSTRTDT14:=P_DEFRMNTSTRTDT14;
      end if;
      cur_colname:='P_DEFRMNTENDDT14';
      cur_colvalue:=P_DEFRMNTENDDT14;
      if P_DEFRMNTENDDT14 is null then

        new_rec.DEFRMNTENDDT14:=jan_1_1900;

      else
        new_rec.DEFRMNTENDDT14:=P_DEFRMNTENDDT14;
      end if;
      cur_colname:='P_DEFRMNTREQ14AMT';
      cur_colvalue:=P_DEFRMNTREQ14AMT;
      if P_DEFRMNTREQ14AMT is null then

        new_rec.DEFRMNTREQ14AMT:=0;

      else
        new_rec.DEFRMNTREQ14AMT:=P_DEFRMNTREQ14AMT;
      end if;
      cur_colname:='P_DEFRMNTSTRTDT15';
      cur_colvalue:=P_DEFRMNTSTRTDT15;
      if P_DEFRMNTSTRTDT15 is null then

        new_rec.DEFRMNTSTRTDT15:=jan_1_1900;

      else
        new_rec.DEFRMNTSTRTDT15:=P_DEFRMNTSTRTDT15;
      end if;
      cur_colname:='P_DEFRMNTENDDT15';
      cur_colvalue:=P_DEFRMNTENDDT15;
      if P_DEFRMNTENDDT15 is null then

        new_rec.DEFRMNTENDDT15:=jan_1_1900;

      else
        new_rec.DEFRMNTENDDT15:=P_DEFRMNTENDDT15;
      end if;
      cur_colname:='P_DEFRMNTREQ15AMT';
      cur_colvalue:=P_DEFRMNTREQ15AMT;
      if P_DEFRMNTREQ15AMT is null then

        new_rec.DEFRMNTREQ15AMT:=0;

      else
        new_rec.DEFRMNTREQ15AMT:=P_DEFRMNTREQ15AMT;
      end if;
      cur_colname:='P_DEFRMNTDTCREAT';
      cur_colvalue:=P_DEFRMNTDTCREAT;
      if P_DEFRMNTDTCREAT is null then

        new_rec.DEFRMNTDTCREAT:=jan_1_1900;

      else
        new_rec.DEFRMNTDTCREAT:=P_DEFRMNTDTCREAT;
      end if;
      cur_colname:='P_DEFRMNTDTMOD';
      cur_colvalue:=P_DEFRMNTDTMOD;
      if P_DEFRMNTDTMOD is null then

        new_rec.DEFRMNTDTMOD:=jan_1_1900;

      else
        new_rec.DEFRMNTDTMOD:=P_DEFRMNTDTMOD;
      end if;
    exception when others then
       p_errval:=sqlcode;
       p_errmsg:='Error - Oracle returned '||sqlerrm(sqlcode)||' using '
           ||cur_colname||' Value >'||cur_colvalue||'<';
    end;
  end if;
    keystouse:='(DEFRMNTGPKEY)=('||new_rec.DEFRMNTGPKEY||')';
    if p_errval=0 then
      begin
        insert into STGCSA.SOFVBGDFTBL values new_rec;
        p_RETVAL := SQL%ROWCOUNT;
      exception
      when dup_val_on_index then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Insert caused dupe on unique IX: '||sqlerrm(sqlcode);
      when others then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<3 then maxsev:=3; end if;
        p_errmsg:='Inserting record caught SQL error '||sqlerrm(sqlcode);
      end;
    end if;
  else /* P_IDENTIFIER was unexpected value */
    p_errval:=-20002;
    if maxsev<3 then maxsev:=3; end if;
    p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier
      ||' inserting record with key '||keystouse;
  end if;
  if p_errval=0 then
    runtime.logger(logged_msgid_retval,'STDLOG',101,
      'End SOFVBGDFINSTSP With 0 return, P_RETVAL='
      ||p_retval||' Record Key '||keystouse,p_userid,
      1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt,
      l_timestampfld,PROGRAM_NAME=>'SOFVBGDFINSTSP');
  else
    p_errmsg:='Error: '||P_ERRMSG||' in SOFVBGDFINSTSP Record Key '||keystouse
      ||' Build: 2018-11-07 11:35:24';
    ROLLBACK TO SOFVBGDFINSTSP;
    runtime.logger(logged_msgid_retval,'STDERR',103,
      p_errmsg,p_userid,maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,
      l_entrydt, l_timestampfld,PROGRAM_NAME=>'SOFVBGDFINSTSP',force_log_entry=>TRUE);
  end if;
EXCEPTION
WHEN OTHERS THEN
  p_errval:=SQLCODE;
  p_errmsg:=' In outer exception handler in SOFVBGDFINSTSP caught exception '
    ||sqlerrm(sqlcode)||' Record Key '||keystouse||' build 2018-11-07 11:35:24';
  ROLLBACK TO SOFVBGDFINSTSP;
  runtime.logger(logged_msgid_retval,'STDERR',20,p_errmsg,p_userid,3,
  PROGRAM_NAME=>'SOFVBGDFINSTSP',force_log_entry=>TRUE);
--
END SOFVBGDFINSTSP;
/


GRANT EXECUTE ON STGCSA.SOFVBGDFINSTSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.SOFVBGDFINSTSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.SOFVBGDFINSTSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.SOFVBGDFINSTSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.SOFVBGDFINSTSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.SOFVBGDFINSTSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.SOFVBGDFINSTSP TO STGCSADEVROLE;

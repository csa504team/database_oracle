DROP PROCEDURE STGCSA.TEMPGETOGERRRAWINSTSP;

CREATE OR REPLACE PROCEDURE STGCSA.TEMPGETOGERRRAWINSTSP(
    p_retval out number,
    p_errval out number,
    p_errmsg  out varchar2,
    p_identifier number:=0,
    p_userid varchar2
   ,p_GETOPENGNTYERRRAWID VARCHAR2:=null
   ,p_LOANNMB CHAR:=null
   ,p_OPENGNTYERRAMT VARCHAR2:=null
   ,p_THIRDTHURSDAYDT DATE:=null
   ,p_PREPAYDT DATE:=null
   ,p_POSTINGDT DATE:=null
   ,p_DTCHKIND CHAR:=null
   ,p_DTCHKTRUE VARCHAR2:=null
   ,p_DTCHKFALSE VARCHAR2:=null
) as
 /*
  Created on: 2018-11-07 11:36:38
  Created by: GENR
  Crerated from template stdtblins_template v3.05 13 Sep 2018 on 2018-11-07 11:36:38
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  Procedure performs INSERT on STGCSA.TEMPGETOGERRRAWTBLfor P_IDENTIFIER=0
*/
  rundate date;
  gn global_name.global_name%type;
  new_rec STGCSA.TEMPGETOGERRRAWTBL%rowtype;
  logged_msgid_retval number;
  audretval number;
  crossover_active char(1);
  l_LOANNMB char(10):=null;
  l_TRANSID number:=null;
  l_dbmdl varchar2(10):=null;
  l_entrydt date:=null;
  l_userid  varchar2(32):=null;
  l_usernm  varchar2(199):=null;
  l_timestampfld date:=null;
  cur_col_name varchar2(100):=null;
  maxsev number:=0;
  holdstring varchar2(4000);
  keystouse varchar2(1000);
  -- std date for column defaulting
  JAN_1_1900  constant date:=to_date('JAN-01-1900','MON-DD-YYYY');
  cur_colvalue varchar2(4095);
  cur_colname varchar2(30);
-- Function used to initialize GETOPENGNTYERRRAWID from sequence GETOPENGNTYERRRAWIDSEQ
Function NEXTVAL_GETOPENGNTYERRRAWIDSEQ return number is
  N number;
begin
  select GETOPENGNTYERRRAWIDSEQ.nextval into n from dual;
  return n;
end;
-- Main body begins here
begin
  -- setup
  savepoint TEMPGETOGERRRAWINSTSP;
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  runtime.logger(logged_msgid_retval,'STDLOG',100,'Init TEMPGETOGERRRAWINSTSP',p_userid,4,
    logtxt1=>'TEMPGETOGERRRAWINSTSP',logtxt2=>'stdtblins_template v3.05 13 Sep 2018',
    PROGRAM_NAME=>'TEMPGETOGERRRAWINSTSP');
  select global_name, sysdate into gn, rundate from global_name;
  begin
    cur_colname:='LOANNMB';
    cur_colvalue:=P_LOANNMB;
    l_LOANNMB:=P_LOANNMB;
  exception when others then
    p_errval:=sqlcode;
    p_errmsg:='Error '||sqlerrm(sqlcode)||' using '||cur_colname||' Value >'||cur_colvalue||'<';
  end;
  new_rec.creatuserid:=p_userid;
  new_rec.creatdt:=sysdate;
  new_rec.lastupdtuserid:=p_userid;
  new_rec.lastupdtdt:=sysdate;
  if p_identifier=0 then
    -- p_identifier=0 this is default for a stored procedure, only action for insert
    runtime.errfound:=false;
    runtime.errstr:=' ';
    --
    -- Default columns as applicable
  if p_errval=0 then
    begin
      cur_colname:='P_GETOPENGNTYERRRAWID';
      cur_colvalue:=P_GETOPENGNTYERRRAWID;
      if P_GETOPENGNTYERRRAWID is null then

        new_rec.GETOPENGNTYERRRAWID:=NEXTVAL_GETOPENGNTYERRRAWIDSEQ();

      else
        new_rec.GETOPENGNTYERRRAWID:=P_GETOPENGNTYERRRAWID;
      end if;
      cur_colname:='P_LOANNMB';
      cur_colvalue:=P_LOANNMB;
      if P_LOANNMB is null then

        new_rec.LOANNMB:=rpad(' ',10);

      else
        new_rec.LOANNMB:=P_LOANNMB;
      end if;
      cur_colname:='P_OPENGNTYERRAMT';
      cur_colvalue:=P_OPENGNTYERRAMT;
      if P_OPENGNTYERRAMT is null then

        new_rec.OPENGNTYERRAMT:=0;

      else
        new_rec.OPENGNTYERRAMT:=P_OPENGNTYERRAMT;
      end if;
      cur_colname:='P_THIRDTHURSDAYDT';
      cur_colvalue:=P_THIRDTHURSDAYDT;
      if P_THIRDTHURSDAYDT is null then

        new_rec.THIRDTHURSDAYDT:=jan_1_1900;

      else
        new_rec.THIRDTHURSDAYDT:=P_THIRDTHURSDAYDT;
      end if;
      cur_colname:='P_PREPAYDT';
      cur_colvalue:=P_PREPAYDT;
      if P_PREPAYDT is null then

        new_rec.PREPAYDT:=jan_1_1900;

      else
        new_rec.PREPAYDT:=P_PREPAYDT;
      end if;
      cur_colname:='P_POSTINGDT';
      cur_colvalue:=P_POSTINGDT;
      if P_POSTINGDT is null then

        new_rec.POSTINGDT:=jan_1_1900;

      else
        new_rec.POSTINGDT:=P_POSTINGDT;
      end if;
      cur_colname:='P_DTCHKIND';
      cur_colvalue:=P_DTCHKIND;
      if P_DTCHKIND is null then

        new_rec.DTCHKIND:=rpad(' ',1);

      else
        new_rec.DTCHKIND:=P_DTCHKIND;
      end if;
      cur_colname:='P_DTCHKTRUE';
      cur_colvalue:=P_DTCHKTRUE;
      if P_DTCHKTRUE is null then

        new_rec.DTCHKTRUE:=0;

      else
        new_rec.DTCHKTRUE:=P_DTCHKTRUE;
      end if;
      cur_colname:='P_DTCHKFALSE';
      cur_colvalue:=P_DTCHKFALSE;
      if P_DTCHKFALSE is null then

        new_rec.DTCHKFALSE:=0;

      else
        new_rec.DTCHKFALSE:=P_DTCHKFALSE;
      end if;
    exception when others then
       p_errval:=sqlcode;
       p_errmsg:='Error - Oracle returned '||sqlerrm(sqlcode)||' using '
           ||cur_colname||' Value >'||cur_colvalue||'<';
    end;
  end if;
    keystouse:='(GETOPENGNTYERRRAWID)=('||new_rec.GETOPENGNTYERRRAWID||')';
    if p_errval=0 then
      begin
        insert into STGCSA.TEMPGETOGERRRAWTBL values new_rec;
        p_RETVAL := SQL%ROWCOUNT;
      exception
      when dup_val_on_index then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Insert caused dupe on unique IX: '||sqlerrm(sqlcode);
      when others then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<3 then maxsev:=3; end if;
        p_errmsg:='Inserting record caught SQL error '||sqlerrm(sqlcode);
      end;
    end if;
  else /* P_IDENTIFIER was unexpected value */
    p_errval:=-20002;
    if maxsev<3 then maxsev:=3; end if;
    p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier
      ||' inserting record with key '||keystouse;
  end if;
  if p_errval=0 then
    runtime.logger(logged_msgid_retval,'STDLOG',101,
      'End TEMPGETOGERRRAWINSTSP With 0 return, P_RETVAL='
      ||p_retval||' Record Key '||keystouse,p_userid,
      1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt,
      l_timestampfld,PROGRAM_NAME=>'TEMPGETOGERRRAWINSTSP');
  else
    p_errmsg:='Error: '||P_ERRMSG||' in TEMPGETOGERRRAWINSTSP Record Key '||keystouse
      ||' Build: 2018-11-07 11:36:38';
    ROLLBACK TO TEMPGETOGERRRAWINSTSP;
    runtime.logger(logged_msgid_retval,'STDERR',103,
      p_errmsg,p_userid,maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,
      l_entrydt, l_timestampfld,PROGRAM_NAME=>'TEMPGETOGERRRAWINSTSP',force_log_entry=>TRUE);
  end if;
EXCEPTION
WHEN OTHERS THEN
  p_errval:=SQLCODE;
  p_errmsg:=' In outer exception handler in TEMPGETOGERRRAWINSTSP caught exception '
    ||sqlerrm(sqlcode)||' Record Key '||keystouse||' build 2018-11-07 11:36:38';
  ROLLBACK TO TEMPGETOGERRRAWINSTSP;
  runtime.logger(logged_msgid_retval,'STDERR',20,p_errmsg,p_userid,3,
  PROGRAM_NAME=>'TEMPGETOGERRRAWINSTSP',force_log_entry=>TRUE);
--
END TEMPGETOGERRRAWINSTSP;
/


GRANT EXECUTE ON STGCSA.TEMPGETOGERRRAWINSTSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.TEMPGETOGERRRAWINSTSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.TEMPGETOGERRRAWINSTSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.TEMPGETOGERRRAWINSTSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.TEMPGETOGERRRAWINSTSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.TEMPGETOGERRRAWINSTSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.TEMPGETOGERRRAWINSTSP TO STGCSADEVROLE;

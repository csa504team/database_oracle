DROP PROCEDURE STGCSA.SOFVPROPUPDTSP;

CREATE OR REPLACE PROCEDURE STGCSA.SOFVPROPUPDTSP(
    p_retval out number,
    p_errval out number,
    p_errmsg  out varchar2,
    p_identifier number:=0,
    p_userid varchar2
   ,p_LOANNMB CHAR:=null
   ,p_PROPLOANSECURBYREALPROP CHAR:=null
   ,p_PROPSECURMAILADRSTR1NM VARCHAR2:=null
   ,p_PROPSECURMAILADRSTR2NM VARCHAR2:=null
   ,p_PROPSECURMAILADRCTYNM VARCHAR2:=null
   ,p_PROPSECURMAILADRSTCD CHAR:=null
   ,p_PROPSECURZIPCD CHAR:=null
   ,p_PROPSECURZIP4CD CHAR:=null
   ,p_PROPDESCCNT VARCHAR2:=null
   ,p_PROPDESCMAILADRSTCD CHAR:=null
   ,p_PROPDESCAPN VARCHAR2:=null
) as
 /*
  Created on: 2018-11-28 18:24:35
  Created by: GENR
  Crerated from template stdtblupd_template v3.22 12 Oct 2018 on 2018-11-28 18:24:36
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  Procedure SOFVPROPUPDTSP performs UPDATE on STGCSA.SOFVPROPTBL
    for P_IDENTIFIER=0 qualification is on Primary Key: LOANNMB
    Updates all columns except the qualification keys and columns for which a
      null value is passed.
    SPECIAL secret update kludge!
      if p_identifier is set to -1 then a flag is set to use all input parm values
      and p_identifier is changed to 0 (update by PK).
      THEREFORE if using p_identifier=-1 you MUST pass intended value for all columns.
      This option allows setting colulmns to null!
*/
  dbrec STGCSA.SOFVPROPTBL%rowtype;
  logged_msg_id number;
  recnum number;
  rec_rowid rowid;
  keystouse varchar2(256);
  maxsev number:=0;
  pctsign char(1):='%';
  cur_colname varchar2(100):=null;
  cur_colvalue varchar2(4000);
  do_normal_field_checking char(1):='Y';
  overridden_p_identifier number;
  -- standard activity log capture vars
  l_LOANNMB char(10):=null;
  l_TRANSID number:=null;
  l_dbmdl varchar2(10):=null;
  l_entrydt date:=null;
  l_userid  varchar2(32):=null;
  l_usernm  varchar2(199):=null;
  l_timestampfld date:=null;
  crossover_not_active char(1);
procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
begin
  if p_errval=0 then
    begin
      select * into dbrec from STGCSA.SOFVPROPTBL where rowid=rowid_in;
    exception when others then
      p_errval:=sqlcode;
      maxsev:=3;
      p_retval:=0;
      p_errmsg:='Fetch of STGCSA.SOFVPROPTBL row by rowid '||rowid_in
        ||' orinally fetched using '||orig_keyset_values
        ||' failed with '||sqlerrm(sqlcode);
    end;
  end if;
  if p_errval=0 then
    -- set this as lastupdt
    DBrec.lastupdtuserid:=p_userid;
    DBrec.lastupdtdt:=sysdate;
    -- copy into DBrec all table column input parameters that are not null,
    -- saving column name/value if needed for error reporting
    --  (if p_identifier=-1 then move input even if null)
    begin
      if P_LOANNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_LOANNMB';
        cur_colvalue:=P_LOANNMB;
        DBrec.LOANNMB:=P_LOANNMB;
      end if;
      if P_PROPLOANSECURBYREALPROP is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PROPLOANSECURBYREALPROP';
        cur_colvalue:=P_PROPLOANSECURBYREALPROP;
        DBrec.PROPLOANSECURBYREALPROP:=P_PROPLOANSECURBYREALPROP;
      end if;
      if P_PROPSECURMAILADRSTR1NM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PROPSECURMAILADRSTR1NM';
        cur_colvalue:=P_PROPSECURMAILADRSTR1NM;
        DBrec.PROPSECURMAILADRSTR1NM:=P_PROPSECURMAILADRSTR1NM;
      end if;
      if P_PROPSECURMAILADRSTR2NM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PROPSECURMAILADRSTR2NM';
        cur_colvalue:=P_PROPSECURMAILADRSTR2NM;
        DBrec.PROPSECURMAILADRSTR2NM:=P_PROPSECURMAILADRSTR2NM;
      end if;
      if P_PROPSECURMAILADRCTYNM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PROPSECURMAILADRCTYNM';
        cur_colvalue:=P_PROPSECURMAILADRCTYNM;
        DBrec.PROPSECURMAILADRCTYNM:=P_PROPSECURMAILADRCTYNM;
      end if;
      if P_PROPSECURMAILADRSTCD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PROPSECURMAILADRSTCD';
        cur_colvalue:=P_PROPSECURMAILADRSTCD;
        DBrec.PROPSECURMAILADRSTCD:=P_PROPSECURMAILADRSTCD;
      end if;
      if P_PROPSECURZIPCD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PROPSECURZIPCD';
        cur_colvalue:=P_PROPSECURZIPCD;
        DBrec.PROPSECURZIPCD:=P_PROPSECURZIPCD;
      end if;
      if P_PROPSECURZIP4CD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PROPSECURZIP4CD';
        cur_colvalue:=P_PROPSECURZIP4CD;
        DBrec.PROPSECURZIP4CD:=P_PROPSECURZIP4CD;
      end if;
      if P_PROPDESCCNT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PROPDESCCNT';
        cur_colvalue:=P_PROPDESCCNT;
        DBrec.PROPDESCCNT:=P_PROPDESCCNT;
      end if;
      if P_PROPDESCMAILADRSTCD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PROPDESCMAILADRSTCD';
        cur_colvalue:=P_PROPDESCMAILADRSTCD;
        DBrec.PROPDESCMAILADRSTCD:=P_PROPDESCMAILADRSTCD;
      end if;
      if P_PROPDESCAPN is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PROPDESCAPN';
        cur_colvalue:=P_PROPDESCAPN;
        DBrec.PROPDESCAPN:=P_PROPDESCAPN;
      end if;
    exception when others then
      p_errval:=sqlcode;
      if maxsev<3 then maxsev:=3; end if;
      p_errmsg:='Error using '||cur_colname||' Value >'||cur_colvalue||'<';
    end;
  end if;
  -- if all still ok do actual update
  if p_errval=0 then
    Begin
      update STGCSA.SOFVPROPTBL
        set lastupdtuserid=DBrec.lastupdtuserid,
          lastupdtdt=DBrec.lastupdtdt
          ,LOANNMB=DBrec.LOANNMB
          ,PROPLOANSECURBYREALPROP=DBrec.PROPLOANSECURBYREALPROP
          ,PROPSECURMAILADRSTR1NM=DBrec.PROPSECURMAILADRSTR1NM
          ,PROPSECURMAILADRSTR2NM=DBrec.PROPSECURMAILADRSTR2NM
          ,PROPSECURMAILADRCTYNM=DBrec.PROPSECURMAILADRCTYNM
          ,PROPSECURMAILADRSTCD=DBrec.PROPSECURMAILADRSTCD
          ,PROPSECURZIPCD=DBrec.PROPSECURZIPCD
          ,PROPSECURZIP4CD=DBrec.PROPSECURZIP4CD
          ,PROPDESCCNT=DBrec.PROPDESCCNT
          ,PROPDESCMAILADRSTCD=DBrec.PROPDESCMAILADRSTCD
          ,PROPDESCAPN=DBrec.PROPDESCAPN
        where rowid=rowid_in;
      recnum:=recnum+1;
    exception when others then
      p_errval:=sqlcode;
      p_retval:=0;
      if maxsev<3 then maxsev:=3; end if;
      p_errmsg:= 'Error while doing update on STGCSA.SOFVPROPTBL, on rec with rowid '
      ||rowid_in||' previously fetched using key: '||orig_keyset_values
      ||' got Oracle error '||sqlerrm(sqlcode);
    end;
  end if;
END;
begin
  -- setup
  savepoint SOFVPROPUPDTSP;
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  recnum:=0;
  runtime.logger(logged_msg_id,'STDLOG',100,'Init SOFVPROPUPDTSP',p_userid,4,
    logtxt1=>'SOFVPROPUPDTSP',logtxt2=>'stdtblupd_template v3.22 12 Oct 2018',
    PROGRAM_NAME=>'SOFVPROPUPDTSP');
  --
  l_LOANNMB:=P_LOANNMB;
  --
  -- Check to see if crossover is ongoing, in which case updates are prohibited
  SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
  IF crossover_not_active != 'Y' THEN
    p_errval := -20099;
    p_errmsg := 'Warning: Cannot update table SOFVPROPTBL at this time because crossover is running, please try later.';
  END IF;
  -- end of crossover_lockout_check
--
-- special p_identifier kludge
--  if p_identifier is set to -1 then it treated as 0 (update by PK)
--  but a flag is set use passed value EVEN IF NULL.  Since for option -1
--  every column is updated to what was passed, intended values MUST be passed
--  for all columns.  This option allows setting colulmns to null!
  overridden_p_identifier:=p_identifier;
  if p_identifier=-1 then
    do_normal_field_checking:='N';
    overridden_p_identifier:=0;
  end if;
  case overridden_p_identifier
  when 0 then
    -- case to update one row based on primary key
    -- p_identifier=0 (choice 0) is default action for a stored procedure
    -- get existing version of row
    if p_errval=0 then
      keystouse:='(LOANNMB)=('||P_LOANNMB||')';
      begin
        select rowid into rec_rowid from STGCSA.SOFVPROPTBL where 1=1
         and LOANNMB=P_LOANNMB;
      exception when no_data_found then
        p_errval:=0;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Warning No STGCSA.SOFVPROPTBL row to update with key(s) '
          ||keystouse;
      end;
    end if;
    if p_errval=0 then
      fetch_and_update_by_rowid(rec_rowid, keystouse);
    end if;
  else /* P_IDENTIFIER was unexpected value */
    p_errval:=-20002;
    if maxsev<3 then maxsev:=3; end if;
    p_errmsg:='Error P_IDENTIFIER contained unexpected value: '||p_identifier;
  end case;
  P_RETVAL:=RECNUM;
  -- post to log
  if p_errval=0 then
    runtime.logger(logged_msg_id,'STDLOG',101,
      'End SOFVPROPUPDTSP With 0 return, P_RETVAL='
      ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
      1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld
      ,PROGRAM_NAME=>'SOFVPROPUPDTSP');
  else
    ROLLBACK TO SOFVPROPUPDTSP;
    p_errmsg:=p_errmsg||' in SOFVPROPUPDTSP build 2018-11-28 18:24:35';
    runtime.logger(logged_msg_id,'STDERR',103,p_errmsg,p_userid,
      maxsev, l_usernm, l_loannmb,l_transid, l_dbmdl, l_entrydt, l_timestampfld,
      PROGRAM_NAME=>'SOFVPROPUPDTSP',force_log_entry=>true);
  end if;
EXCEPTION
WHEN OTHERS THEN
  ROLLBACK TO SOFVPROPUPDTSP;
  p_errmsg:=' Error outer exception handler caught Oracle error:'||SQLERRM(SQLCODE)
    ||' in SOFVPROPUPDTSP build 2018-11-28 18:24:35';
  runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
    3,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld,
    PROGRAM_NAME=>'SOFVPROPUPDTSP',force_log_entry=>true);
--
END SOFVPROPUPDTSP;
/

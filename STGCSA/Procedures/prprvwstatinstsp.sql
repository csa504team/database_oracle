DROP PROCEDURE STGCSA.PRPRVWSTATINSTSP;

CREATE OR REPLACE PROCEDURE STGCSA.PRPRVWSTATINSTSP(
    p_retval out number,
    p_errval out number,
    p_errmsg  out varchar2,
    p_identifier number:=0,
    p_userid varchar2
   ,p_PRPRVWSTATID VARCHAR2:=null
   ,p_LOANNMB CHAR:=null
   ,p_TRANSID VARCHAR2:=null
   ,p_TRANSTYPID VARCHAR2:=null
   ,p_INITIALPOSTRVW VARCHAR2:=null
   ,p_INITIALPOSTRVWBY VARCHAR2:=null
   ,p_SCNDPOSTRVW VARCHAR2:=null
   ,p_SCNDPOSTRVWBY VARCHAR2:=null
   ,p_THIRDPOSTRVW VARCHAR2:=null
   ,p_THIRDPOSTRVWBY VARCHAR2:=null
   ,p_FOURTHPOSTRVW VARCHAR2:=null
   ,p_FOURTHPOSTRVWBY VARCHAR2:=null
   ,p_FIFTHPOSTRVW VARCHAR2:=null
   ,p_FIFTHPOSTRVWBY VARCHAR2:=null
   ,p_SIXTHPOSTRVW VARCHAR2:=null
   ,p_SIXTHPOSTRVWBY VARCHAR2:=null
   ,p_SIXTHMOPOSTRVW VARCHAR2:=null
   ,p_SIXTHMOPOSTRVWBY VARCHAR2:=null
   ,p_SIXTHMOPOST2RVW VARCHAR2:=null
   ,p_SIXTHMOPOST2RVWBY VARCHAR2:=null
   ,p_SIXTHMOPOST3RVW VARCHAR2:=null
   ,p_SIXTHMOPOST3RVWBY VARCHAR2:=null
   ,p_SIXTHMOPOST4RVW VARCHAR2:=null
   ,p_SIXTHMOPOST4RVWBY VARCHAR2:=null
   ,p_SIXTHMOPOST5RVW VARCHAR2:=null
   ,p_SIXTHMOPOST5RVWBY VARCHAR2:=null
   ,p_THISTRVW VARCHAR2:=null
   ,p_THISTRVWBY VARCHAR2:=null
   ,p_FEEVAL VARCHAR2:=null
   ,p_FEEVALBY VARCHAR2:=null
   ,p_RVWSBMT VARCHAR2:=null
   ,p_RVWSBMTBY VARCHAR2:=null
   ,p_RVWSBMTDT DATE:=null
   ,p_WORKUPVAL VARCHAR2:=null
   ,p_WORKUPVALBY VARCHAR2:=null
   ,p_THISTVAL VARCHAR2:=null
   ,p_THISTVALBY VARCHAR2:=null
   ,p_VALSBMT VARCHAR2:=null
   ,p_VALSBMTBY VARCHAR2:=null
   ,p_VALSBMTDT DATE:=null
   ,p_PROOFVAL VARCHAR2:=null
   ,p_PROOFVALBY VARCHAR2:=null
   ,p_TIMESTAMPFLD DATE:=null
) as
 /*
  Created on: 2018-11-07 11:35:01
  Created by: GENR
  Crerated from template stdtblins_template v3.05 13 Sep 2018 on 2018-11-07 11:35:01
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  Procedure performs INSERT on STGCSA.PRPRVWSTATTBLfor P_IDENTIFIER=0
*/
  rundate date;
  gn global_name.global_name%type;
  new_rec STGCSA.PRPRVWSTATTBL%rowtype;
  logged_msgid_retval number;
  audretval number;
  crossover_active char(1);
  l_LOANNMB char(10):=null;
  l_TRANSID number:=null;
  l_dbmdl varchar2(10):=null;
  l_entrydt date:=null;
  l_userid  varchar2(32):=null;
  l_usernm  varchar2(199):=null;
  l_timestampfld date:=null;
  cur_col_name varchar2(100):=null;
  maxsev number:=0;
  holdstring varchar2(4000);
  keystouse varchar2(1000);
  -- std date for column defaulting
  JAN_1_1900  constant date:=to_date('JAN-01-1900','MON-DD-YYYY');
  cur_colvalue varchar2(4095);
  cur_colname varchar2(30);
-- Function used to initialize PRPRVWSTATID from sequence PRPRVWSTATIDSEQ
Function NEXTVAL_PRPRVWSTATIDSEQ return number is
  N number;
begin
  select PRPRVWSTATIDSEQ.nextval into n from dual;
  return n;
end;
-- Main body begins here
begin
  -- setup
  savepoint PRPRVWSTATINSTSP;
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  runtime.logger(logged_msgid_retval,'STDLOG',100,'Init PRPRVWSTATINSTSP',p_userid,4,
    logtxt1=>'PRPRVWSTATINSTSP',logtxt2=>'stdtblins_template v3.05 13 Sep 2018',
    PROGRAM_NAME=>'PRPRVWSTATINSTSP');
  select global_name, sysdate into gn, rundate from global_name;
  begin
    cur_colname:='LOANNMB';
    cur_colvalue:=P_LOANNMB;
    l_LOANNMB:=P_LOANNMB;    cur_colname:='TRANSID';
    cur_colvalue:=P_TRANSID;
    l_TRANSID:=P_TRANSID;    cur_colname:='TIMESTAMPFLD';
    cur_colvalue:=P_TIMESTAMPFLD;
    l_TIMESTAMPFLD:=P_TIMESTAMPFLD;
  exception when others then
    p_errval:=sqlcode;
    p_errmsg:='Error '||sqlerrm(sqlcode)||' using '||cur_colname||' Value >'||cur_colvalue||'<';
  end;
  new_rec.creatuserid:=p_userid;
  new_rec.creatdt:=sysdate;
  new_rec.lastupdtuserid:=p_userid;
  new_rec.lastupdtdt:=sysdate;
  if p_identifier=0 then
    -- p_identifier=0 this is default for a stored procedure, only action for insert
    runtime.errfound:=false;
    runtime.errstr:=' ';
    --
    -- Default columns as applicable
  if p_errval=0 then
    begin
      cur_colname:='P_PRPRVWSTATID';
      cur_colvalue:=P_PRPRVWSTATID;
      if P_PRPRVWSTATID is null then

        new_rec.PRPRVWSTATID:=NEXTVAL_PRPRVWSTATIDSEQ();

      else
        new_rec.PRPRVWSTATID:=P_PRPRVWSTATID;
      end if;
      cur_colname:='P_LOANNMB';
      cur_colvalue:=P_LOANNMB;
      if P_LOANNMB is null then

        new_rec.LOANNMB:=rpad(' ',10);

      else
        new_rec.LOANNMB:=P_LOANNMB;
      end if;
      cur_colname:='P_TRANSID';
      cur_colvalue:=P_TRANSID;
      if P_TRANSID is null then

        new_rec.TRANSID:=0;

      else
        new_rec.TRANSID:=P_TRANSID;
      end if;
      cur_colname:='P_TRANSTYPID';
      cur_colvalue:=P_TRANSTYPID;
      if P_TRANSTYPID is null then

        new_rec.TRANSTYPID:=0;

      else
        new_rec.TRANSTYPID:=P_TRANSTYPID;
      end if;
      cur_colname:='P_INITIALPOSTRVW';
      cur_colvalue:=P_INITIALPOSTRVW;
      if P_INITIALPOSTRVW is null then

        new_rec.INITIALPOSTRVW:=0;

      else
        new_rec.INITIALPOSTRVW:=P_INITIALPOSTRVW;
      end if;
      cur_colname:='P_INITIALPOSTRVWBY';
      cur_colvalue:=P_INITIALPOSTRVWBY;
      if P_INITIALPOSTRVWBY is null then

        new_rec.INITIALPOSTRVWBY:=' ';

      else
        new_rec.INITIALPOSTRVWBY:=P_INITIALPOSTRVWBY;
      end if;
      cur_colname:='P_SCNDPOSTRVW';
      cur_colvalue:=P_SCNDPOSTRVW;
      if P_SCNDPOSTRVW is null then

        new_rec.SCNDPOSTRVW:=0;

      else
        new_rec.SCNDPOSTRVW:=P_SCNDPOSTRVW;
      end if;
      cur_colname:='P_SCNDPOSTRVWBY';
      cur_colvalue:=P_SCNDPOSTRVWBY;
      if P_SCNDPOSTRVWBY is null then

        new_rec.SCNDPOSTRVWBY:=' ';

      else
        new_rec.SCNDPOSTRVWBY:=P_SCNDPOSTRVWBY;
      end if;
      cur_colname:='P_THIRDPOSTRVW';
      cur_colvalue:=P_THIRDPOSTRVW;
      if P_THIRDPOSTRVW is null then

        new_rec.THIRDPOSTRVW:=0;

      else
        new_rec.THIRDPOSTRVW:=P_THIRDPOSTRVW;
      end if;
      cur_colname:='P_THIRDPOSTRVWBY';
      cur_colvalue:=P_THIRDPOSTRVWBY;
      if P_THIRDPOSTRVWBY is null then

        new_rec.THIRDPOSTRVWBY:=' ';

      else
        new_rec.THIRDPOSTRVWBY:=P_THIRDPOSTRVWBY;
      end if;
      cur_colname:='P_FOURTHPOSTRVW';
      cur_colvalue:=P_FOURTHPOSTRVW;
      if P_FOURTHPOSTRVW is null then

        new_rec.FOURTHPOSTRVW:=0;

      else
        new_rec.FOURTHPOSTRVW:=P_FOURTHPOSTRVW;
      end if;
      cur_colname:='P_FOURTHPOSTRVWBY';
      cur_colvalue:=P_FOURTHPOSTRVWBY;
      if P_FOURTHPOSTRVWBY is null then

        new_rec.FOURTHPOSTRVWBY:=' ';

      else
        new_rec.FOURTHPOSTRVWBY:=P_FOURTHPOSTRVWBY;
      end if;
      cur_colname:='P_FIFTHPOSTRVW';
      cur_colvalue:=P_FIFTHPOSTRVW;
      if P_FIFTHPOSTRVW is null then

        new_rec.FIFTHPOSTRVW:=0;

      else
        new_rec.FIFTHPOSTRVW:=P_FIFTHPOSTRVW;
      end if;
      cur_colname:='P_FIFTHPOSTRVWBY';
      cur_colvalue:=P_FIFTHPOSTRVWBY;
      if P_FIFTHPOSTRVWBY is null then

        new_rec.FIFTHPOSTRVWBY:=' ';

      else
        new_rec.FIFTHPOSTRVWBY:=P_FIFTHPOSTRVWBY;
      end if;
      cur_colname:='P_SIXTHPOSTRVW';
      cur_colvalue:=P_SIXTHPOSTRVW;
      if P_SIXTHPOSTRVW is null then

        new_rec.SIXTHPOSTRVW:=0;

      else
        new_rec.SIXTHPOSTRVW:=P_SIXTHPOSTRVW;
      end if;
      cur_colname:='P_SIXTHPOSTRVWBY';
      cur_colvalue:=P_SIXTHPOSTRVWBY;
      if P_SIXTHPOSTRVWBY is null then

        new_rec.SIXTHPOSTRVWBY:=' ';

      else
        new_rec.SIXTHPOSTRVWBY:=P_SIXTHPOSTRVWBY;
      end if;
      cur_colname:='P_SIXTHMOPOSTRVW';
      cur_colvalue:=P_SIXTHMOPOSTRVW;
      if P_SIXTHMOPOSTRVW is null then

        new_rec.SIXTHMOPOSTRVW:=0;

      else
        new_rec.SIXTHMOPOSTRVW:=P_SIXTHMOPOSTRVW;
      end if;
      cur_colname:='P_SIXTHMOPOSTRVWBY';
      cur_colvalue:=P_SIXTHMOPOSTRVWBY;
      if P_SIXTHMOPOSTRVWBY is null then

        new_rec.SIXTHMOPOSTRVWBY:=' ';

      else
        new_rec.SIXTHMOPOSTRVWBY:=P_SIXTHMOPOSTRVWBY;
      end if;
      cur_colname:='P_SIXTHMOPOST2RVW';
      cur_colvalue:=P_SIXTHMOPOST2RVW;
      if P_SIXTHMOPOST2RVW is null then

        new_rec.SIXTHMOPOST2RVW:=0;

      else
        new_rec.SIXTHMOPOST2RVW:=P_SIXTHMOPOST2RVW;
      end if;
      cur_colname:='P_SIXTHMOPOST2RVWBY';
      cur_colvalue:=P_SIXTHMOPOST2RVWBY;
      if P_SIXTHMOPOST2RVWBY is null then

        new_rec.SIXTHMOPOST2RVWBY:=' ';

      else
        new_rec.SIXTHMOPOST2RVWBY:=P_SIXTHMOPOST2RVWBY;
      end if;
      cur_colname:='P_SIXTHMOPOST3RVW';
      cur_colvalue:=P_SIXTHMOPOST3RVW;
      if P_SIXTHMOPOST3RVW is null then

        new_rec.SIXTHMOPOST3RVW:=0;

      else
        new_rec.SIXTHMOPOST3RVW:=P_SIXTHMOPOST3RVW;
      end if;
      cur_colname:='P_SIXTHMOPOST3RVWBY';
      cur_colvalue:=P_SIXTHMOPOST3RVWBY;
      if P_SIXTHMOPOST3RVWBY is null then

        new_rec.SIXTHMOPOST3RVWBY:=' ';

      else
        new_rec.SIXTHMOPOST3RVWBY:=P_SIXTHMOPOST3RVWBY;
      end if;
      cur_colname:='P_SIXTHMOPOST4RVW';
      cur_colvalue:=P_SIXTHMOPOST4RVW;
      if P_SIXTHMOPOST4RVW is null then

        new_rec.SIXTHMOPOST4RVW:=0;

      else
        new_rec.SIXTHMOPOST4RVW:=P_SIXTHMOPOST4RVW;
      end if;
      cur_colname:='P_SIXTHMOPOST4RVWBY';
      cur_colvalue:=P_SIXTHMOPOST4RVWBY;
      if P_SIXTHMOPOST4RVWBY is null then

        new_rec.SIXTHMOPOST4RVWBY:=' ';

      else
        new_rec.SIXTHMOPOST4RVWBY:=P_SIXTHMOPOST4RVWBY;
      end if;
      cur_colname:='P_SIXTHMOPOST5RVW';
      cur_colvalue:=P_SIXTHMOPOST5RVW;
      if P_SIXTHMOPOST5RVW is null then

        new_rec.SIXTHMOPOST5RVW:=0;

      else
        new_rec.SIXTHMOPOST5RVW:=P_SIXTHMOPOST5RVW;
      end if;
      cur_colname:='P_SIXTHMOPOST5RVWBY';
      cur_colvalue:=P_SIXTHMOPOST5RVWBY;
      if P_SIXTHMOPOST5RVWBY is null then

        new_rec.SIXTHMOPOST5RVWBY:=' ';

      else
        new_rec.SIXTHMOPOST5RVWBY:=P_SIXTHMOPOST5RVWBY;
      end if;
      cur_colname:='P_THISTRVW';
      cur_colvalue:=P_THISTRVW;
      if P_THISTRVW is null then

        new_rec.THISTRVW:=0;

      else
        new_rec.THISTRVW:=P_THISTRVW;
      end if;
      cur_colname:='P_THISTRVWBY';
      cur_colvalue:=P_THISTRVWBY;
      if P_THISTRVWBY is null then

        new_rec.THISTRVWBY:=' ';

      else
        new_rec.THISTRVWBY:=P_THISTRVWBY;
      end if;
      cur_colname:='P_FEEVAL';
      cur_colvalue:=P_FEEVAL;
      if P_FEEVAL is null then

        new_rec.FEEVAL:=0;

      else
        new_rec.FEEVAL:=P_FEEVAL;
      end if;
      cur_colname:='P_FEEVALBY';
      cur_colvalue:=P_FEEVALBY;
      if P_FEEVALBY is null then

        new_rec.FEEVALBY:=' ';

      else
        new_rec.FEEVALBY:=P_FEEVALBY;
      end if;
      cur_colname:='P_RVWSBMT';
      cur_colvalue:=P_RVWSBMT;
      if P_RVWSBMT is null then

        new_rec.RVWSBMT:=0;

      else
        new_rec.RVWSBMT:=P_RVWSBMT;
      end if;
      cur_colname:='P_RVWSBMTBY';
      cur_colvalue:=P_RVWSBMTBY;
      if P_RVWSBMTBY is null then

        new_rec.RVWSBMTBY:=' ';

      else
        new_rec.RVWSBMTBY:=P_RVWSBMTBY;
      end if;
      cur_colname:='P_RVWSBMTDT';
      cur_colvalue:=P_RVWSBMTDT;
      if P_RVWSBMTDT is null then

        new_rec.RVWSBMTDT:=jan_1_1900;

      else
        new_rec.RVWSBMTDT:=P_RVWSBMTDT;
      end if;
      cur_colname:='P_WORKUPVAL';
      cur_colvalue:=P_WORKUPVAL;
      if P_WORKUPVAL is null then

        new_rec.WORKUPVAL:=0;

      else
        new_rec.WORKUPVAL:=P_WORKUPVAL;
      end if;
      cur_colname:='P_WORKUPVALBY';
      cur_colvalue:=P_WORKUPVALBY;
      if P_WORKUPVALBY is null then

        new_rec.WORKUPVALBY:=' ';

      else
        new_rec.WORKUPVALBY:=P_WORKUPVALBY;
      end if;
      cur_colname:='P_THISTVAL';
      cur_colvalue:=P_THISTVAL;
      if P_THISTVAL is null then

        new_rec.THISTVAL:=0;

      else
        new_rec.THISTVAL:=P_THISTVAL;
      end if;
      cur_colname:='P_THISTVALBY';
      cur_colvalue:=P_THISTVALBY;
      if P_THISTVALBY is null then

        new_rec.THISTVALBY:=' ';

      else
        new_rec.THISTVALBY:=P_THISTVALBY;
      end if;
      cur_colname:='P_VALSBMT';
      cur_colvalue:=P_VALSBMT;
      if P_VALSBMT is null then

        new_rec.VALSBMT:=0;

      else
        new_rec.VALSBMT:=P_VALSBMT;
      end if;
      cur_colname:='P_VALSBMTBY';
      cur_colvalue:=P_VALSBMTBY;
      if P_VALSBMTBY is null then

        new_rec.VALSBMTBY:=' ';

      else
        new_rec.VALSBMTBY:=P_VALSBMTBY;
      end if;
      cur_colname:='P_VALSBMTDT';
      cur_colvalue:=P_VALSBMTDT;
      if P_VALSBMTDT is null then

        new_rec.VALSBMTDT:=jan_1_1900;

      else
        new_rec.VALSBMTDT:=P_VALSBMTDT;
      end if;
      cur_colname:='P_PROOFVAL';
      cur_colvalue:=P_PROOFVAL;
      if P_PROOFVAL is null then

        new_rec.PROOFVAL:=0;

      else
        new_rec.PROOFVAL:=P_PROOFVAL;
      end if;
      cur_colname:='P_PROOFVALBY';
      cur_colvalue:=P_PROOFVALBY;
      if P_PROOFVALBY is null then

        new_rec.PROOFVALBY:=' ';

      else
        new_rec.PROOFVALBY:=P_PROOFVALBY;
      end if;
      cur_colname:='P_TIMESTAMPFLD';
      cur_colvalue:=P_TIMESTAMPFLD;
      if P_TIMESTAMPFLD is null then

        new_rec.TIMESTAMPFLD:=jan_1_1900;

      else
        new_rec.TIMESTAMPFLD:=P_TIMESTAMPFLD;
      end if;
    exception when others then
       p_errval:=sqlcode;
       p_errmsg:='Error - Oracle returned '||sqlerrm(sqlcode)||' using '
           ||cur_colname||' Value >'||cur_colvalue||'<';
    end;
  end if;
    keystouse:='(PRPRVWSTATID)=('||new_rec.PRPRVWSTATID||')';
    if p_errval=0 then
      begin
        insert into STGCSA.PRPRVWSTATTBL values new_rec;
        p_RETVAL := SQL%ROWCOUNT;
      exception
      when dup_val_on_index then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Insert caused dupe on unique IX: '||sqlerrm(sqlcode);
      when others then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<3 then maxsev:=3; end if;
        p_errmsg:='Inserting record caught SQL error '||sqlerrm(sqlcode);
      end;
    end if;
  else /* P_IDENTIFIER was unexpected value */
    p_errval:=-20002;
    if maxsev<3 then maxsev:=3; end if;
    p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier
      ||' inserting record with key '||keystouse;
  end if;
  if p_errval=0 then
    runtime.logger(logged_msgid_retval,'STDLOG',101,
      'End PRPRVWSTATINSTSP With 0 return, P_RETVAL='
      ||p_retval||' Record Key '||keystouse,p_userid,
      1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt,
      l_timestampfld,PROGRAM_NAME=>'PRPRVWSTATINSTSP');
  else
    p_errmsg:='Error: '||P_ERRMSG||' in PRPRVWSTATINSTSP Record Key '||keystouse
      ||' Build: 2018-11-07 11:35:01';
    ROLLBACK TO PRPRVWSTATINSTSP;
    runtime.logger(logged_msgid_retval,'STDERR',103,
      p_errmsg,p_userid,maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,
      l_entrydt, l_timestampfld,PROGRAM_NAME=>'PRPRVWSTATINSTSP',force_log_entry=>TRUE);
  end if;
EXCEPTION
WHEN OTHERS THEN
  p_errval:=SQLCODE;
  p_errmsg:=' In outer exception handler in PRPRVWSTATINSTSP caught exception '
    ||sqlerrm(sqlcode)||' Record Key '||keystouse||' build 2018-11-07 11:35:01';
  ROLLBACK TO PRPRVWSTATINSTSP;
  runtime.logger(logged_msgid_retval,'STDERR',20,p_errmsg,p_userid,3,
  PROGRAM_NAME=>'PRPRVWSTATINSTSP',force_log_entry=>TRUE);
--
END PRPRVWSTATINSTSP;
/


GRANT EXECUTE ON STGCSA.PRPRVWSTATINSTSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.PRPRVWSTATINSTSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.PRPRVWSTATINSTSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.PRPRVWSTATINSTSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.PRPRVWSTATINSTSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.PRPRVWSTATINSTSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.PRPRVWSTATINSTSP TO STGCSADEVROLE;

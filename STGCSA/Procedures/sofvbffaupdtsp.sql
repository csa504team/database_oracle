DROP PROCEDURE STGCSA.SOFVBFFAUPDTSP;

CREATE OR REPLACE PROCEDURE STGCSA.SOFVBFFAUPDTSP(
    p_retval out number,
    p_errval out number,
    p_errmsg  out varchar2,
    p_identifier number:=0,
    p_userid varchar2
   ,p_LOANNMB CHAR:=null
   ,p_ASSUMPTAXID CHAR:=null
   ,p_ASSUMPNM VARCHAR2:=null
   ,p_ASSUMPMAILADRSTR1NM VARCHAR2:=null
   ,p_ASSUMPMAILADRSTR2NM VARCHAR2:=null
   ,p_ASSUMPMAILADRCTYNM VARCHAR2:=null
   ,p_ASSUMPMAILADRSTCD CHAR:=null
   ,p_ASSUMPMAILADRZIPCD CHAR:=null
   ,p_ASSUMPMAILADRZIP4CD CHAR:=null
   ,p_ASSUMPOFC CHAR:=null
   ,p_ASSUMPPRGRM CHAR:=null
   ,p_ASSUMPSTATCDOFLOAN CHAR:=null
   ,p_ASSUMPVERIFICATIONIND CHAR:=null
   ,p_ASSUMPVERIFICATIONDT DATE:=null
   ,p_ASSUMPNMMAILADRCHNGIND CHAR:=null
   ,p_ASSUMPNMADRCHNGDT DATE:=null
   ,p_ASSUMPTAXFORMFLD CHAR:=null
   ,p_ASSUMPTAXMAINTNDT DATE:=null
   ,p_ASSUMPKEYLOANNMB CHAR:=null
   ,p_ASSUMPLASTEFFDT DATE:=null
) as
 /*
  Created on: 2018-11-07 11:35:18
  Created by: GENR
  Crerated from template stdtblupd_template v3.22 12 Oct 2018 on 2018-11-07 11:35:19
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  Procedure SOFVBFFAUPDTSP performs UPDATE on STGCSA.SOFVBFFATBL
    for P_IDENTIFIER=0 qualification is on Primary Key: ASSUMPKEYLOANNMB, ASSUMPLASTEFFDT
    for P_IDENTIFIER=1 qualification is on LOANNMB
    Updates all columns except the qualification keys and columns for which a
      null value is passed.
    SPECIAL secret update kludge!
      if p_identifier is set to -1 then a flag is set to use all input parm values
      and p_identifier is changed to 0 (update by PK).
      THEREFORE if using p_identifier=-1 you MUST pass intended value for all columns.
      This option allows setting colulmns to null!
*/
  dbrec STGCSA.SOFVBFFATBL%rowtype;
  logged_msg_id number;
  recnum number;
  rec_rowid rowid;
  keystouse varchar2(256);
  maxsev number:=0;
  pctsign char(1):='%';
  cur_colname varchar2(100):=null;
  cur_colvalue varchar2(4000);
  do_normal_field_checking char(1):='Y';
  overridden_p_identifier number;
  -- standard activity log capture vars
  l_LOANNMB char(10):=null;
  l_TRANSID number:=null;
  l_dbmdl varchar2(10):=null;
  l_entrydt date:=null;
  l_userid  varchar2(32):=null;
  l_usernm  varchar2(199):=null;
  l_timestampfld date:=null;
  crossover_not_active char(1);
procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
begin
  if p_errval=0 then
    begin
      select * into dbrec from STGCSA.SOFVBFFATBL where rowid=rowid_in;
    exception when others then
      p_errval:=sqlcode;
      maxsev:=3;
      p_retval:=0;
      p_errmsg:='Fetch of STGCSA.SOFVBFFATBL row by rowid '||rowid_in
        ||' orinally fetched using '||orig_keyset_values
        ||' failed with '||sqlerrm(sqlcode);
    end;
  end if;
  if p_errval=0 then
    -- set this as lastupdt
    DBrec.lastupdtuserid:=p_userid;
    DBrec.lastupdtdt:=sysdate;
    -- copy into DBrec all table column input parameters that are not null,
    -- saving column name/value if needed for error reporting
    --  (if p_identifier=-1 then move input even if null)
    begin
      if P_LOANNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_LOANNMB';
        cur_colvalue:=P_LOANNMB;
        DBrec.LOANNMB:=P_LOANNMB;
      end if;
      if P_ASSUMPTAXID is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_ASSUMPTAXID';
        cur_colvalue:=P_ASSUMPTAXID;
        DBrec.ASSUMPTAXID:=P_ASSUMPTAXID;
      end if;
      if P_ASSUMPNM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_ASSUMPNM';
        cur_colvalue:=P_ASSUMPNM;
        DBrec.ASSUMPNM:=P_ASSUMPNM;
      end if;
      if P_ASSUMPMAILADRSTR1NM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_ASSUMPMAILADRSTR1NM';
        cur_colvalue:=P_ASSUMPMAILADRSTR1NM;
        DBrec.ASSUMPMAILADRSTR1NM:=P_ASSUMPMAILADRSTR1NM;
      end if;
      if P_ASSUMPMAILADRSTR2NM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_ASSUMPMAILADRSTR2NM';
        cur_colvalue:=P_ASSUMPMAILADRSTR2NM;
        DBrec.ASSUMPMAILADRSTR2NM:=P_ASSUMPMAILADRSTR2NM;
      end if;
      if P_ASSUMPMAILADRCTYNM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_ASSUMPMAILADRCTYNM';
        cur_colvalue:=P_ASSUMPMAILADRCTYNM;
        DBrec.ASSUMPMAILADRCTYNM:=P_ASSUMPMAILADRCTYNM;
      end if;
      if P_ASSUMPMAILADRSTCD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_ASSUMPMAILADRSTCD';
        cur_colvalue:=P_ASSUMPMAILADRSTCD;
        DBrec.ASSUMPMAILADRSTCD:=P_ASSUMPMAILADRSTCD;
      end if;
      if P_ASSUMPMAILADRZIPCD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_ASSUMPMAILADRZIPCD';
        cur_colvalue:=P_ASSUMPMAILADRZIPCD;
        DBrec.ASSUMPMAILADRZIPCD:=P_ASSUMPMAILADRZIPCD;
      end if;
      if P_ASSUMPMAILADRZIP4CD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_ASSUMPMAILADRZIP4CD';
        cur_colvalue:=P_ASSUMPMAILADRZIP4CD;
        DBrec.ASSUMPMAILADRZIP4CD:=P_ASSUMPMAILADRZIP4CD;
      end if;
      if P_ASSUMPOFC is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_ASSUMPOFC';
        cur_colvalue:=P_ASSUMPOFC;
        DBrec.ASSUMPOFC:=P_ASSUMPOFC;
      end if;
      if P_ASSUMPPRGRM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_ASSUMPPRGRM';
        cur_colvalue:=P_ASSUMPPRGRM;
        DBrec.ASSUMPPRGRM:=P_ASSUMPPRGRM;
      end if;
      if P_ASSUMPSTATCDOFLOAN is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_ASSUMPSTATCDOFLOAN';
        cur_colvalue:=P_ASSUMPSTATCDOFLOAN;
        DBrec.ASSUMPSTATCDOFLOAN:=P_ASSUMPSTATCDOFLOAN;
      end if;
      if P_ASSUMPVERIFICATIONIND is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_ASSUMPVERIFICATIONIND';
        cur_colvalue:=P_ASSUMPVERIFICATIONIND;
        DBrec.ASSUMPVERIFICATIONIND:=P_ASSUMPVERIFICATIONIND;
      end if;
      if P_ASSUMPVERIFICATIONDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_ASSUMPVERIFICATIONDT';
        cur_colvalue:=P_ASSUMPVERIFICATIONDT;
        DBrec.ASSUMPVERIFICATIONDT:=P_ASSUMPVERIFICATIONDT;
      end if;
      if P_ASSUMPNMMAILADRCHNGIND is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_ASSUMPNMMAILADRCHNGIND';
        cur_colvalue:=P_ASSUMPNMMAILADRCHNGIND;
        DBrec.ASSUMPNMMAILADRCHNGIND:=P_ASSUMPNMMAILADRCHNGIND;
      end if;
      if P_ASSUMPNMADRCHNGDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_ASSUMPNMADRCHNGDT';
        cur_colvalue:=P_ASSUMPNMADRCHNGDT;
        DBrec.ASSUMPNMADRCHNGDT:=P_ASSUMPNMADRCHNGDT;
      end if;
      if P_ASSUMPTAXFORMFLD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_ASSUMPTAXFORMFLD';
        cur_colvalue:=P_ASSUMPTAXFORMFLD;
        DBrec.ASSUMPTAXFORMFLD:=P_ASSUMPTAXFORMFLD;
      end if;
      if P_ASSUMPTAXMAINTNDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_ASSUMPTAXMAINTNDT';
        cur_colvalue:=P_ASSUMPTAXMAINTNDT;
        DBrec.ASSUMPTAXMAINTNDT:=P_ASSUMPTAXMAINTNDT;
      end if;
      if P_ASSUMPKEYLOANNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_ASSUMPKEYLOANNMB';
        cur_colvalue:=P_ASSUMPKEYLOANNMB;
        DBrec.ASSUMPKEYLOANNMB:=P_ASSUMPKEYLOANNMB;
      end if;
      if P_ASSUMPLASTEFFDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_ASSUMPLASTEFFDT';
        cur_colvalue:=P_ASSUMPLASTEFFDT;
        DBrec.ASSUMPLASTEFFDT:=P_ASSUMPLASTEFFDT;
      end if;
    exception when others then
      p_errval:=sqlcode;
      if maxsev<3 then maxsev:=3; end if;
      p_errmsg:='Error using '||cur_colname||' Value >'||cur_colvalue||'<';
    end;
  end if;
  -- if all still ok do actual update
  if p_errval=0 then
    Begin
      update STGCSA.SOFVBFFATBL
        set lastupdtuserid=DBrec.lastupdtuserid,
          lastupdtdt=DBrec.lastupdtdt
          ,LOANNMB=DBrec.LOANNMB
          ,ASSUMPTAXID=DBrec.ASSUMPTAXID
          ,ASSUMPNM=DBrec.ASSUMPNM
          ,ASSUMPMAILADRSTR1NM=DBrec.ASSUMPMAILADRSTR1NM
          ,ASSUMPMAILADRSTR2NM=DBrec.ASSUMPMAILADRSTR2NM
          ,ASSUMPMAILADRCTYNM=DBrec.ASSUMPMAILADRCTYNM
          ,ASSUMPMAILADRSTCD=DBrec.ASSUMPMAILADRSTCD
          ,ASSUMPMAILADRZIPCD=DBrec.ASSUMPMAILADRZIPCD
          ,ASSUMPMAILADRZIP4CD=DBrec.ASSUMPMAILADRZIP4CD
          ,ASSUMPOFC=DBrec.ASSUMPOFC
          ,ASSUMPPRGRM=DBrec.ASSUMPPRGRM
          ,ASSUMPSTATCDOFLOAN=DBrec.ASSUMPSTATCDOFLOAN
          ,ASSUMPVERIFICATIONIND=DBrec.ASSUMPVERIFICATIONIND
          ,ASSUMPVERIFICATIONDT=DBrec.ASSUMPVERIFICATIONDT
          ,ASSUMPNMMAILADRCHNGIND=DBrec.ASSUMPNMMAILADRCHNGIND
          ,ASSUMPNMADRCHNGDT=DBrec.ASSUMPNMADRCHNGDT
          ,ASSUMPTAXFORMFLD=DBrec.ASSUMPTAXFORMFLD
          ,ASSUMPTAXMAINTNDT=DBrec.ASSUMPTAXMAINTNDT
          ,ASSUMPKEYLOANNMB=DBrec.ASSUMPKEYLOANNMB
          ,ASSUMPLASTEFFDT=DBrec.ASSUMPLASTEFFDT
        where rowid=rowid_in;
      recnum:=recnum+1;
    exception when others then
      p_errval:=sqlcode;
      p_retval:=0;
      if maxsev<3 then maxsev:=3; end if;
      p_errmsg:= 'Error while doing update on STGCSA.SOFVBFFATBL, on rec with rowid '
      ||rowid_in||' previously fetched using key: '||orig_keyset_values
      ||' got Oracle error '||sqlerrm(sqlcode);
    end;
  end if;
END;
begin
  -- setup
  savepoint SOFVBFFAUPDTSP;
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  recnum:=0;
  runtime.logger(logged_msg_id,'STDLOG',100,'Init SOFVBFFAUPDTSP',p_userid,4,
    logtxt1=>'SOFVBFFAUPDTSP',logtxt2=>'stdtblupd_template v3.22 12 Oct 2018',
    PROGRAM_NAME=>'SOFVBFFAUPDTSP');
  --
  l_LOANNMB:=P_LOANNMB;
  --
  -- Check to see if crossover is ongoing, in which case updates are prohibited
  SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
  IF crossover_not_active != 'Y' THEN
    p_errval := -20099;
    p_errmsg := 'Warning: Cannot update table SOFVBFFATBL at this time because crossover is running, please try later.';
  END IF;
  -- end of crossover_lockout_check
--
-- special p_identifier kludge
--  if p_identifier is set to -1 then it treated as 0 (update by PK)
--  but a flag is set use passed value EVEN IF NULL.  Since for option -1
--  every column is updated to what was passed, intended values MUST be passed
--  for all columns.  This option allows setting colulmns to null!
  overridden_p_identifier:=p_identifier;
  if p_identifier=-1 then
    do_normal_field_checking:='N';
    overridden_p_identifier:=0;
  end if;
  case overridden_p_identifier
  when 0 then
    -- case to update one row based on primary key
    -- p_identifier=0 (choice 0) is default action for a stored procedure
    -- get existing version of row
    if p_errval=0 then
      keystouse:='(ASSUMPKEYLOANNMB)=('||P_ASSUMPKEYLOANNMB||')'||' and '||'(ASSUMPLASTEFFDT)=('||P_ASSUMPLASTEFFDT||')';
      begin
        select rowid into rec_rowid from STGCSA.SOFVBFFATBL where 1=1
         and ASSUMPKEYLOANNMB=P_ASSUMPKEYLOANNMB         and ASSUMPLASTEFFDT=P_ASSUMPLASTEFFDT;
      exception when no_data_found then
        p_errval:=0;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Warning No STGCSA.SOFVBFFATBL row to update with key(s) '
          ||keystouse;
      end;
    end if;
    if p_errval=0 then
      fetch_and_update_by_rowid(rec_rowid, keystouse);
    end if;
  when 1 then
    -- case to update one or more rows based on keyset KEYS1
    if p_errval=0 then
      keystouse:='(LOANNMB)=('||P_LOANNMB||')';
      begin
        for r1 in (select rowid from STGCSA.SOFVBFFATBL a where 1=1
          and LOANNMB=P_LOANNMB
  )
        loop
          rec_rowid:=r1.rowid;
          if p_errval=0 then
            fetch_and_update_by_rowid(rec_rowid, keystouse);
          end if;
        end loop;
      exception when others then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<3 then maxsev:=3; end if;
        p_errmsg:='Error fetching STGCSA.SOFVBFFATBL row(s) with key(s) '
         ||keystouse||' Oracle returned '||sqlerrm(sqlcode);
      end;
      if recnum=0 then
        p_errval:=0;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Warning No STGCSA.SOFVBFFATBL row(s) to update with key(s) '
          ||keystouse;
      end if;
    end if;
  else /* P_IDENTIFIER was unexpected value */
    p_errval:=-20002;
    if maxsev<3 then maxsev:=3; end if;
    p_errmsg:='Error P_IDENTIFIER contained unexpected value: '||p_identifier;
  end case;
  P_RETVAL:=RECNUM;
  -- post to log
  if p_errval=0 then
    runtime.logger(logged_msg_id,'STDLOG',101,
      'End SOFVBFFAUPDTSP With 0 return, P_RETVAL='
      ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
      1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld
      ,PROGRAM_NAME=>'SOFVBFFAUPDTSP');
  else
    ROLLBACK TO SOFVBFFAUPDTSP;
    p_errmsg:=p_errmsg||' in SOFVBFFAUPDTSP build 2018-11-07 11:35:18';
    runtime.logger(logged_msg_id,'STDERR',103,p_errmsg,p_userid,
      maxsev, l_usernm, l_loannmb,l_transid, l_dbmdl, l_entrydt, l_timestampfld,
      PROGRAM_NAME=>'SOFVBFFAUPDTSP',force_log_entry=>true);
  end if;
EXCEPTION
WHEN OTHERS THEN
  ROLLBACK TO SOFVBFFAUPDTSP;
  p_errmsg:=' Error outer exception handler caught Oracle error:'||SQLERRM(SQLCODE)
    ||' in SOFVBFFAUPDTSP build 2018-11-07 11:35:18';
  runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
    3,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld,
    PROGRAM_NAME=>'SOFVBFFAUPDTSP',force_log_entry=>true);
--
END SOFVBFFAUPDTSP;
/


GRANT EXECUTE ON STGCSA.SOFVBFFAUPDTSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.SOFVBFFAUPDTSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.SOFVBFFAUPDTSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.SOFVBFFAUPDTSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.SOFVBFFAUPDTSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.SOFVBFFAUPDTSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.SOFVBFFAUPDTSP TO STGCSADEVROLE;

DROP PROCEDURE STGCSA.SEL_REFTRANSATTRTBL;

CREATE OR REPLACE PROCEDURE STGCSA.Sel_RefTransAttrTbl (
  arg_AttrID IN stgCSA.RefTransAttrTbl.REFTRANSAttrID%type,
  arg_AttrNm OUT stgcsa.RefTransAttrTbl.AttrNm%type
  )
AS
v_AttrNm stgcsa.RefTransAttrTbl.AttrNm%type ;

BEGIN
/******************************************************************************
   NAME:       Sel_CoreTransTbl
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0       10/27/2017    Victor T     1.  Return TransID, LoanNmb From
                    stgCSA.RefTransAttrTbl Table
     ******************************************************************************/

SELECT  AttrNm
       INTO v_AttrNm
FROM stgCSA.RefTransAttrTbl
WHERE REFTRANSAttrID =  arg_AttrID;

arg_AttrNm := v_AttrNm;

EXCEPTION
 WHEN NO_DATA_FOUND THEN
    arg_AttrNm := 'No Value';

END Sel_RefTransAttrTbl;
/


GRANT EXECUTE ON STGCSA.SEL_REFTRANSATTRTBL TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.SEL_REFTRANSATTRTBL TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.SEL_REFTRANSATTRTBL TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.SEL_REFTRANSATTRTBL TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.SEL_REFTRANSATTRTBL TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.SEL_REFTRANSATTRTBL TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.SEL_REFTRANSATTRTBL TO STGCSADEVROLE;

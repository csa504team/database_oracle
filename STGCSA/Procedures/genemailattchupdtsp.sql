DROP PROCEDURE STGCSA.GENEMAILATTCHUPDTSP;

CREATE OR REPLACE PROCEDURE STGCSA.GENEMAILATTCHUPDTSP(
    p_retval out number,
    p_errval out number,
    p_errmsg  out varchar2,
    p_identifier number:=0,
    p_userid varchar2
   ,p_EMAILATTCHID VARCHAR2:=null
   ,p_EMAILID VARCHAR2:=null
   ,p_ATTCHFILENM VARCHAR2:=null
   ,p_TIMESTAMPFLD DATE:=null
) as
 /*
  Created on: 2018-11-07 11:33:47
  Created by: GENR
  Crerated from template stdtblupd_template v3.22 12 Oct 2018 on 2018-11-07 11:33:47
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  Procedure GENEMAILATTCHUPDTSP performs UPDATE on STGCSA.GENEMAILATTCHTBL
    for P_IDENTIFIER=0 qualification is on Primary Key: EMAILATTCHID
    Updates all columns except the qualification keys and columns for which a
      null value is passed.
    SPECIAL secret update kludge!
      if p_identifier is set to -1 then a flag is set to use all input parm values
      and p_identifier is changed to 0 (update by PK).
      THEREFORE if using p_identifier=-1 you MUST pass intended value for all columns.
      This option allows setting colulmns to null!
*/
  dbrec STGCSA.GENEMAILATTCHTBL%rowtype;
  logged_msg_id number;
  recnum number;
  rec_rowid rowid;
  keystouse varchar2(256);
  maxsev number:=0;
  pctsign char(1):='%';
  cur_colname varchar2(100):=null;
  cur_colvalue varchar2(4000);
  do_normal_field_checking char(1):='Y';
  overridden_p_identifier number;
  -- standard activity log capture vars
  l_LOANNMB char(10):=null;
  l_TRANSID number:=null;
  l_dbmdl varchar2(10):=null;
  l_entrydt date:=null;
  l_userid  varchar2(32):=null;
  l_usernm  varchar2(199):=null;
  l_timestampfld date:=null;
procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
begin
  if p_errval=0 then
    begin
      select * into dbrec from STGCSA.GENEMAILATTCHTBL where rowid=rowid_in;
    exception when others then
      p_errval:=sqlcode;
      maxsev:=3;
      p_retval:=0;
      p_errmsg:='Fetch of STGCSA.GENEMAILATTCHTBL row by rowid '||rowid_in
        ||' orinally fetched using '||orig_keyset_values
        ||' failed with '||sqlerrm(sqlcode);
    end;
  end if;
  if p_errval=0 then
    -- set this as lastupdt
    DBrec.lastupdtuserid:=p_userid;
    DBrec.lastupdtdt:=sysdate;
    -- copy into DBrec all table column input parameters that are not null,
    -- saving column name/value if needed for error reporting
    --  (if p_identifier=-1 then move input even if null)
    begin
      if P_EMAILATTCHID is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_EMAILATTCHID';
        cur_colvalue:=P_EMAILATTCHID;
        DBrec.EMAILATTCHID:=P_EMAILATTCHID;
      end if;
      if P_EMAILID is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_EMAILID';
        cur_colvalue:=P_EMAILID;
        DBrec.EMAILID:=P_EMAILID;
      end if;
      if P_ATTCHFILENM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_ATTCHFILENM';
        cur_colvalue:=P_ATTCHFILENM;
        DBrec.ATTCHFILENM:=P_ATTCHFILENM;
      end if;
      if P_TIMESTAMPFLD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_TIMESTAMPFLD';
        cur_colvalue:=P_TIMESTAMPFLD;
        DBrec.TIMESTAMPFLD:=P_TIMESTAMPFLD;
      end if;
    exception when others then
      p_errval:=sqlcode;
      if maxsev<3 then maxsev:=3; end if;
      p_errmsg:='Error using '||cur_colname||' Value >'||cur_colvalue||'<';
    end;
  end if;
  -- if all still ok do actual update
  if p_errval=0 then
    Begin
      update STGCSA.GENEMAILATTCHTBL
        set lastupdtuserid=DBrec.lastupdtuserid,
          lastupdtdt=DBrec.lastupdtdt
          ,EMAILATTCHID=DBrec.EMAILATTCHID
          ,EMAILID=DBrec.EMAILID
          ,ATTCHFILENM=DBrec.ATTCHFILENM
          ,TIMESTAMPFLD=DBrec.TIMESTAMPFLD
        where rowid=rowid_in;
      recnum:=recnum+1;
    exception when others then
      p_errval:=sqlcode;
      p_retval:=0;
      if maxsev<3 then maxsev:=3; end if;
      p_errmsg:= 'Error while doing update on STGCSA.GENEMAILATTCHTBL, on rec with rowid '
      ||rowid_in||' previously fetched using key: '||orig_keyset_values
      ||' got Oracle error '||sqlerrm(sqlcode);
    end;
  end if;
END;
begin
  -- setup
  savepoint GENEMAILATTCHUPDTSP;
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  recnum:=0;
  runtime.logger(logged_msg_id,'STDLOG',100,'Init GENEMAILATTCHUPDTSP',p_userid,4,
    logtxt1=>'GENEMAILATTCHUPDTSP',logtxt2=>'stdtblupd_template v3.22 12 Oct 2018',
    PROGRAM_NAME=>'GENEMAILATTCHUPDTSP');
  --
  l_TIMESTAMPFLD:=P_TIMESTAMPFLD;
--
-- special p_identifier kludge
--  if p_identifier is set to -1 then it treated as 0 (update by PK)
--  but a flag is set use passed value EVEN IF NULL.  Since for option -1
--  every column is updated to what was passed, intended values MUST be passed
--  for all columns.  This option allows setting colulmns to null!
  overridden_p_identifier:=p_identifier;
  if p_identifier=-1 then
    do_normal_field_checking:='N';
    overridden_p_identifier:=0;
  end if;
  case overridden_p_identifier
  when 0 then
    -- case to update one row based on primary key
    -- p_identifier=0 (choice 0) is default action for a stored procedure
    -- get existing version of row
    if p_errval=0 then
      keystouse:='(EMAILATTCHID)=('||P_EMAILATTCHID||')';
      begin
        select rowid into rec_rowid from STGCSA.GENEMAILATTCHTBL where 1=1
         and EMAILATTCHID=P_EMAILATTCHID;
      exception when no_data_found then
        p_errval:=0;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Warning No STGCSA.GENEMAILATTCHTBL row to update with key(s) '
          ||keystouse;
      end;
    end if;
    if p_errval=0 then
      fetch_and_update_by_rowid(rec_rowid, keystouse);
    end if;
  else /* P_IDENTIFIER was unexpected value */
    p_errval:=-20002;
    if maxsev<3 then maxsev:=3; end if;
    p_errmsg:='Error P_IDENTIFIER contained unexpected value: '||p_identifier;
  end case;
  P_RETVAL:=RECNUM;
  -- post to log
  if p_errval=0 then
    runtime.logger(logged_msg_id,'STDLOG',101,
      'End GENEMAILATTCHUPDTSP With 0 return, P_RETVAL='
      ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
      1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld
      ,PROGRAM_NAME=>'GENEMAILATTCHUPDTSP');
  else
    ROLLBACK TO GENEMAILATTCHUPDTSP;
    p_errmsg:=p_errmsg||' in GENEMAILATTCHUPDTSP build 2018-11-07 11:33:47';
    runtime.logger(logged_msg_id,'STDERR',103,p_errmsg,p_userid,
      maxsev, l_usernm, l_loannmb,l_transid, l_dbmdl, l_entrydt, l_timestampfld,
      PROGRAM_NAME=>'GENEMAILATTCHUPDTSP',force_log_entry=>true);
  end if;
EXCEPTION
WHEN OTHERS THEN
  ROLLBACK TO GENEMAILATTCHUPDTSP;
  p_errmsg:=' Error outer exception handler caught Oracle error:'||SQLERRM(SQLCODE)
    ||' in GENEMAILATTCHUPDTSP build 2018-11-07 11:33:47';
  runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
    3,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld,
    PROGRAM_NAME=>'GENEMAILATTCHUPDTSP',force_log_entry=>true);
--
END GENEMAILATTCHUPDTSP;
/


GRANT EXECUTE ON STGCSA.GENEMAILATTCHUPDTSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.GENEMAILATTCHUPDTSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.GENEMAILATTCHUPDTSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.GENEMAILATTCHUPDTSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.GENEMAILATTCHUPDTSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.GENEMAILATTCHUPDTSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.GENEMAILATTCHUPDTSP TO STGCSADEVROLE;

DROP PROCEDURE STGCSA.PRPMFGNTYINSTSP;

CREATE OR REPLACE PROCEDURE STGCSA.PRPMFGNTYINSTSP(
    p_retval out number,
    p_errval out number,
    p_errmsg  out varchar2,
    p_identifier number:=0,
    p_userid varchar2
   ,p_PRPMFGNTYID VARCHAR2:=null
   ,p_LOANNMB CHAR:=null
   ,p_OPENGNTYDT DATE:=null
   ,p_OPENGNTYTYP VARCHAR2:=null
   ,p_GNTYAMT VARCHAR2:=null
   ,p_STATCD VARCHAR2:=null
   ,p_GNTYCLS VARCHAR2:=null
   ,p_PRPCMNT VARCHAR2:=null
   ,p_SEMIANDT DATE:=null
   ,p_PREPAYDT DATE:=null
   ,p_OPENGNTYDTCONVERTED DATE:=null
   ,p_OPENGNTYDTPREPAYDTDIFF VARCHAR2:=null
   ,p_GNTYIND VARCHAR2:=null
   ,p_SEMIANDTCONVERTED DATE:=null
   ,p_TIMESTAMPFLD DATE:=null
) as
 /*
  Created on: 2018-11-07 11:34:50
  Created by: GENR
  Crerated from template stdtblins_template v3.05 13 Sep 2018 on 2018-11-07 11:34:50
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  Procedure performs INSERT on STGCSA.PRPMFGNTYTBLfor P_IDENTIFIER=0
*/
  rundate date;
  gn global_name.global_name%type;
  new_rec STGCSA.PRPMFGNTYTBL%rowtype;
  logged_msgid_retval number;
  audretval number;
  crossover_active char(1);
  l_LOANNMB char(10):=null;
  l_TRANSID number:=null;
  l_dbmdl varchar2(10):=null;
  l_entrydt date:=null;
  l_userid  varchar2(32):=null;
  l_usernm  varchar2(199):=null;
  l_timestampfld date:=null;
  cur_col_name varchar2(100):=null;
  maxsev number:=0;
  holdstring varchar2(4000);
  keystouse varchar2(1000);
  -- std date for column defaulting
  JAN_1_1900  constant date:=to_date('JAN-01-1900','MON-DD-YYYY');
  cur_colvalue varchar2(4095);
  cur_colname varchar2(30);
-- Function used to initialize PRPMFGNTYID from sequence PRPMFGNTYIDSEQ
Function NEXTVAL_PRPMFGNTYIDSEQ return number is
  N number;
begin
  select PRPMFGNTYIDSEQ.nextval into n from dual;
  return n;
end;
-- Main body begins here
begin
  -- setup
  savepoint PRPMFGNTYINSTSP;
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  runtime.logger(logged_msgid_retval,'STDLOG',100,'Init PRPMFGNTYINSTSP',p_userid,4,
    logtxt1=>'PRPMFGNTYINSTSP',logtxt2=>'stdtblins_template v3.05 13 Sep 2018',
    PROGRAM_NAME=>'PRPMFGNTYINSTSP');
  select global_name, sysdate into gn, rundate from global_name;
  begin
    cur_colname:='LOANNMB';
    cur_colvalue:=P_LOANNMB;
    l_LOANNMB:=P_LOANNMB;    cur_colname:='TIMESTAMPFLD';
    cur_colvalue:=P_TIMESTAMPFLD;
    l_TIMESTAMPFLD:=P_TIMESTAMPFLD;
  exception when others then
    p_errval:=sqlcode;
    p_errmsg:='Error '||sqlerrm(sqlcode)||' using '||cur_colname||' Value >'||cur_colvalue||'<';
  end;
  new_rec.creatuserid:=p_userid;
  new_rec.creatdt:=sysdate;
  new_rec.lastupdtuserid:=p_userid;
  new_rec.lastupdtdt:=sysdate;
  if p_identifier=0 then
    -- p_identifier=0 this is default for a stored procedure, only action for insert
    runtime.errfound:=false;
    runtime.errstr:=' ';
    --
    -- Default columns as applicable
  if p_errval=0 then
    begin
      cur_colname:='P_PRPMFGNTYID';
      cur_colvalue:=P_PRPMFGNTYID;
      if P_PRPMFGNTYID is null then

        new_rec.PRPMFGNTYID:=NEXTVAL_PRPMFGNTYIDSEQ();

      else
        new_rec.PRPMFGNTYID:=P_PRPMFGNTYID;
      end if;
      cur_colname:='P_LOANNMB';
      cur_colvalue:=P_LOANNMB;
      if P_LOANNMB is null then

        new_rec.LOANNMB:=rpad(' ',10);

      else
        new_rec.LOANNMB:=P_LOANNMB;
      end if;
      cur_colname:='P_OPENGNTYDT';
      cur_colvalue:=P_OPENGNTYDT;
      if P_OPENGNTYDT is null then

        new_rec.OPENGNTYDT:=jan_1_1900;

      else
        new_rec.OPENGNTYDT:=P_OPENGNTYDT;
      end if;
      cur_colname:='P_OPENGNTYTYP';
      cur_colvalue:=P_OPENGNTYTYP;
      if P_OPENGNTYTYP is null then

        new_rec.OPENGNTYTYP:=0;

      else
        new_rec.OPENGNTYTYP:=P_OPENGNTYTYP;
      end if;
      cur_colname:='P_GNTYAMT';
      cur_colvalue:=P_GNTYAMT;
      if P_GNTYAMT is null then

        new_rec.GNTYAMT:=0;

      else
        new_rec.GNTYAMT:=P_GNTYAMT;
      end if;
      cur_colname:='P_STATCD';
      cur_colvalue:=P_STATCD;
      if P_STATCD is null then

        new_rec.STATCD:=0;

      else
        new_rec.STATCD:=P_STATCD;
      end if;
      cur_colname:='P_GNTYCLS';
      cur_colvalue:=P_GNTYCLS;
      if P_GNTYCLS is null then

        new_rec.GNTYCLS:=' ';

      else
        new_rec.GNTYCLS:=P_GNTYCLS;
      end if;
      cur_colname:='P_PRPCMNT';
      cur_colvalue:=P_PRPCMNT;
      if P_PRPCMNT is null then

        new_rec.PRPCMNT:=' ';

      else
        new_rec.PRPCMNT:=P_PRPCMNT;
      end if;
      cur_colname:='P_SEMIANDT';
      cur_colvalue:=P_SEMIANDT;
      if P_SEMIANDT is null then

        new_rec.SEMIANDT:=jan_1_1900;

      else
        new_rec.SEMIANDT:=P_SEMIANDT;
      end if;
      cur_colname:='P_PREPAYDT';
      cur_colvalue:=P_PREPAYDT;
      if P_PREPAYDT is null then

        new_rec.PREPAYDT:=jan_1_1900;

      else
        new_rec.PREPAYDT:=P_PREPAYDT;
      end if;
      cur_colname:='P_OPENGNTYDTCONVERTED';
      cur_colvalue:=P_OPENGNTYDTCONVERTED;
      if P_OPENGNTYDTCONVERTED is null then

        new_rec.OPENGNTYDTCONVERTED:=jan_1_1900;

      else
        new_rec.OPENGNTYDTCONVERTED:=P_OPENGNTYDTCONVERTED;
      end if;
      cur_colname:='P_OPENGNTYDTPREPAYDTDIFF';
      cur_colvalue:=P_OPENGNTYDTPREPAYDTDIFF;
      if P_OPENGNTYDTPREPAYDTDIFF is null then

        new_rec.OPENGNTYDTPREPAYDTDIFF:=0;

      else
        new_rec.OPENGNTYDTPREPAYDTDIFF:=P_OPENGNTYDTPREPAYDTDIFF;
      end if;
      cur_colname:='P_GNTYIND';
      cur_colvalue:=P_GNTYIND;
      if P_GNTYIND is null then

        new_rec.GNTYIND:=' ';

      else
        new_rec.GNTYIND:=P_GNTYIND;
      end if;
      cur_colname:='P_SEMIANDTCONVERTED';
      cur_colvalue:=P_SEMIANDTCONVERTED;
      if P_SEMIANDTCONVERTED is null then

        new_rec.SEMIANDTCONVERTED:=jan_1_1900;

      else
        new_rec.SEMIANDTCONVERTED:=P_SEMIANDTCONVERTED;
      end if;
      cur_colname:='P_TIMESTAMPFLD';
      cur_colvalue:=P_TIMESTAMPFLD;
      if P_TIMESTAMPFLD is null then

        new_rec.TIMESTAMPFLD:=jan_1_1900;

      else
        new_rec.TIMESTAMPFLD:=P_TIMESTAMPFLD;
      end if;
    exception when others then
       p_errval:=sqlcode;
       p_errmsg:='Error - Oracle returned '||sqlerrm(sqlcode)||' using '
           ||cur_colname||' Value >'||cur_colvalue||'<';
    end;
  end if;
    keystouse:='(PRPMFGNTYID)=('||new_rec.PRPMFGNTYID||')';
    if p_errval=0 then
      begin
        insert into STGCSA.PRPMFGNTYTBL values new_rec;
        p_RETVAL := SQL%ROWCOUNT;
      exception
      when dup_val_on_index then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Insert caused dupe on unique IX: '||sqlerrm(sqlcode);
      when others then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<3 then maxsev:=3; end if;
        p_errmsg:='Inserting record caught SQL error '||sqlerrm(sqlcode);
      end;
    end if;
  else /* P_IDENTIFIER was unexpected value */
    p_errval:=-20002;
    if maxsev<3 then maxsev:=3; end if;
    p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier
      ||' inserting record with key '||keystouse;
  end if;
  if p_errval=0 then
    runtime.logger(logged_msgid_retval,'STDLOG',101,
      'End PRPMFGNTYINSTSP With 0 return, P_RETVAL='
      ||p_retval||' Record Key '||keystouse,p_userid,
      1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt,
      l_timestampfld,PROGRAM_NAME=>'PRPMFGNTYINSTSP');
  else
    p_errmsg:='Error: '||P_ERRMSG||' in PRPMFGNTYINSTSP Record Key '||keystouse
      ||' Build: 2018-11-07 11:34:50';
    ROLLBACK TO PRPMFGNTYINSTSP;
    runtime.logger(logged_msgid_retval,'STDERR',103,
      p_errmsg,p_userid,maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,
      l_entrydt, l_timestampfld,PROGRAM_NAME=>'PRPMFGNTYINSTSP',force_log_entry=>TRUE);
  end if;
EXCEPTION
WHEN OTHERS THEN
  p_errval:=SQLCODE;
  p_errmsg:=' In outer exception handler in PRPMFGNTYINSTSP caught exception '
    ||sqlerrm(sqlcode)||' Record Key '||keystouse||' build 2018-11-07 11:34:50';
  ROLLBACK TO PRPMFGNTYINSTSP;
  runtime.logger(logged_msgid_retval,'STDERR',20,p_errmsg,p_userid,3,
  PROGRAM_NAME=>'PRPMFGNTYINSTSP',force_log_entry=>TRUE);
--
END PRPMFGNTYINSTSP;
/


GRANT EXECUTE ON STGCSA.PRPMFGNTYINSTSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.PRPMFGNTYINSTSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.PRPMFGNTYINSTSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.PRPMFGNTYINSTSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.PRPMFGNTYINSTSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.PRPMFGNTYINSTSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.PRPMFGNTYINSTSP TO STGCSADEVROLE;

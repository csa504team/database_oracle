DROP PROCEDURE STGCSA.PRPMFWIREPOSTINGINSTSP;

CREATE OR REPLACE PROCEDURE STGCSA.PRPMFWIREPOSTINGINSTSP(
    p_retval out number,
    p_errval out number,
    p_errmsg  out varchar2,
    p_identifier number:=0,
    p_userid varchar2
   ,p_LOANNMB CHAR:=null
   ,p_BORRFEEAMT VARCHAR2:=null
   ,p_BORRPAIDTHRUDT CHAR:=null
   ,p_CSAFEEAMT VARCHAR2:=null
   ,p_CSAPAIDTHRUDT CHAR:=null
   ,p_CDCFEEAPPLAMT VARCHAR2:=null
   ,p_CDCFEEDISBAMT VARCHAR2:=null
   ,p_CDCDISBAMT VARCHAR2:=null
   ,p_CDCPAIDTHRUDT CHAR:=null
   ,p_INTPAIDTHRUDT CHAR:=null
   ,p_ADJTOAPPLINTAMT VARCHAR2:=null
   ,p_ADJTOAPPLPRINAMT VARCHAR2:=null
   ,p_LATEFEEADJAMT VARCHAR2:=null
   ,p_UNALLOCAMT VARCHAR2:=null
   ,p_CURBALAMT VARCHAR2:=null
   ,p_PREPAYAMT VARCHAR2:=null
   ,p_WIRECMNT VARCHAR2:=null
   ,p_ADJTYP VARCHAR2:=null
   ,p_OVRWRITECD VARCHAR2:=null
   ,p_POSTINGTYP VARCHAR2:=null
   ,p_POSTINGSCHDLDT DATE:=null
   ,p_POSTINGACTUALDT DATE:=null
   ,p_WIREPOSTINGID VARCHAR2:=null
   ,p_TRANSID VARCHAR2:=null
   ,p_MFSTATCD VARCHAR2:=null
   ,p_ACHDEL VARCHAR2:=null
   ,p_OVRPAYAMT VARCHAR2:=null
   ,p_CDCREPDAMT VARCHAR2:=null
   ,p_CSAREPDAMT VARCHAR2:=null
   ,p_REPDAMT VARCHAR2:=null
   ,p_PREPAYDT DATE:=null
   ,p_SEMIANDT DATE:=null
   ,p_ZEROBALAMT VARCHAR2:=null
   ,p_SEMIANCDCNMB CHAR:=null
   ,p_SEMIANNOTEBALAMT VARCHAR2:=null
   ,p_SEMIANWIREAMT VARCHAR2:=null
   ,p_SEMIANPRINACCRAMT VARCHAR2:=null
   ,p_SEMIANINTACCRAMT VARCHAR2:=null
   ,p_SEMIANOPENGNTYAMT VARCHAR2:=null
   ,p_SEMIANOPENGNTYERRAMT VARCHAR2:=null
   ,p_SEMIANOPENGNTYREMAINAMT VARCHAR2:=null
   ,p_SEMIANPIPYMTAFT6MOAMT VARCHAR2:=null
   ,p_SEMIANPISHORTOVRAMT VARCHAR2:=null
   ,p_SEMIANESCROWPRIORPYMTAMT VARCHAR2:=null
   ,p_SEMIANDBENTRBALAMT VARCHAR2:=null
   ,p_SEMIANPREPAYPREMAMT VARCHAR2:=null
   ,p_SEMIANAMT VARCHAR2:=null
   ,p_SEMIANSBADUEFROMBORRAMT VARCHAR2:=null
   ,p_SEMIANSBABEHINDAMT VARCHAR2:=null
   ,p_SEMIANSBAACCRAMT VARCHAR2:=null
   ,p_SEMIANSBAPRJCTDAMT VARCHAR2:=null
   ,p_SEMIANCSADUEFROMBORRAMT VARCHAR2:=null
   ,p_SEMIANCSABEHINDAMT VARCHAR2:=null
   ,p_SEMIANCSAACCRAMT VARCHAR2:=null
   ,p_SEMIANCSAPRJCTDAMT VARCHAR2:=null
   ,p_SEMIANCDCDUEFROMBORRAMT VARCHAR2:=null
   ,p_SEMIANCDCBEHINDAMT VARCHAR2:=null
   ,p_SEMIANCDCACCRAMT VARCHAR2:=null
   ,p_SEMIANACCRINTONDELAMT VARCHAR2:=null
   ,p_SEMIANLATEFEEAMT VARCHAR2:=null
   ,p_SEMIANPROOFAMT VARCHAR2:=null
   ,p_TIMESTAMPFLD DATE:=null
) as
 /*
  Created on: 2018-11-07 11:34:55
  Created by: GENR
  Crerated from template stdtblins_template v3.05 13 Sep 2018 on 2018-11-07 11:34:55
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  Procedure performs INSERT on STGCSA.PRPMFWIREPOSTINGTBLfor P_IDENTIFIER=0
*/
  rundate date;
  gn global_name.global_name%type;
  new_rec STGCSA.PRPMFWIREPOSTINGTBL%rowtype;
  logged_msgid_retval number;
  audretval number;
  crossover_active char(1);
  l_LOANNMB char(10):=null;
  l_TRANSID number:=null;
  l_dbmdl varchar2(10):=null;
  l_entrydt date:=null;
  l_userid  varchar2(32):=null;
  l_usernm  varchar2(199):=null;
  l_timestampfld date:=null;
  cur_col_name varchar2(100):=null;
  maxsev number:=0;
  holdstring varchar2(4000);
  keystouse varchar2(1000);
  -- std date for column defaulting
  JAN_1_1900  constant date:=to_date('JAN-01-1900','MON-DD-YYYY');
  cur_colvalue varchar2(4095);
  cur_colname varchar2(30);
-- Function used to initialize WIREPOSTINGID from sequence WIREPOSTINGIDSEQ
Function NEXTVAL_WIREPOSTINGIDSEQ return number is
  N number;
begin
  select WIREPOSTINGIDSEQ.nextval into n from dual;
  return n;
end;
-- Main body begins here
begin
  -- setup
  savepoint PRPMFWIREPOSTINGINSTSP;
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  runtime.logger(logged_msgid_retval,'STDLOG',100,'Init PRPMFWIREPOSTINGINSTSP',p_userid,4,
    logtxt1=>'PRPMFWIREPOSTINGINSTSP',logtxt2=>'stdtblins_template v3.05 13 Sep 2018',
    PROGRAM_NAME=>'PRPMFWIREPOSTINGINSTSP');
  select global_name, sysdate into gn, rundate from global_name;
  begin
    cur_colname:='LOANNMB';
    cur_colvalue:=P_LOANNMB;
    l_LOANNMB:=P_LOANNMB;    cur_colname:='TRANSID';
    cur_colvalue:=P_TRANSID;
    l_TRANSID:=P_TRANSID;    cur_colname:='TIMESTAMPFLD';
    cur_colvalue:=P_TIMESTAMPFLD;
    l_TIMESTAMPFLD:=P_TIMESTAMPFLD;
  exception when others then
    p_errval:=sqlcode;
    p_errmsg:='Error '||sqlerrm(sqlcode)||' using '||cur_colname||' Value >'||cur_colvalue||'<';
  end;
  new_rec.creatuserid:=p_userid;
  new_rec.creatdt:=sysdate;
  new_rec.lastupdtuserid:=p_userid;
  new_rec.lastupdtdt:=sysdate;
  if p_identifier=0 then
    -- p_identifier=0 this is default for a stored procedure, only action for insert
    runtime.errfound:=false;
    runtime.errstr:=' ';
    --
    -- Default columns as applicable
  if p_errval=0 then
    begin
      cur_colname:='P_LOANNMB';
      cur_colvalue:=P_LOANNMB;
      if P_LOANNMB is null then

        new_rec.LOANNMB:=rpad(' ',10);

      else
        new_rec.LOANNMB:=P_LOANNMB;
      end if;
      cur_colname:='P_BORRFEEAMT';
      cur_colvalue:=P_BORRFEEAMT;
      if P_BORRFEEAMT is null then

        new_rec.BORRFEEAMT:=0;

      else
        new_rec.BORRFEEAMT:=P_BORRFEEAMT;
      end if;
      cur_colname:='P_BORRPAIDTHRUDT';
      cur_colvalue:=P_BORRPAIDTHRUDT;
      if P_BORRPAIDTHRUDT is null then

        new_rec.BORRPAIDTHRUDT:=rpad(' ',10);

      else
        new_rec.BORRPAIDTHRUDT:=P_BORRPAIDTHRUDT;
      end if;
      cur_colname:='P_CSAFEEAMT';
      cur_colvalue:=P_CSAFEEAMT;
      if P_CSAFEEAMT is null then

        new_rec.CSAFEEAMT:=0;

      else
        new_rec.CSAFEEAMT:=P_CSAFEEAMT;
      end if;
      cur_colname:='P_CSAPAIDTHRUDT';
      cur_colvalue:=P_CSAPAIDTHRUDT;
      if P_CSAPAIDTHRUDT is null then

        new_rec.CSAPAIDTHRUDT:=rpad(' ',10);

      else
        new_rec.CSAPAIDTHRUDT:=P_CSAPAIDTHRUDT;
      end if;
      cur_colname:='P_CDCFEEAPPLAMT';
      cur_colvalue:=P_CDCFEEAPPLAMT;
      if P_CDCFEEAPPLAMT is null then

        new_rec.CDCFEEAPPLAMT:=0;

      else
        new_rec.CDCFEEAPPLAMT:=P_CDCFEEAPPLAMT;
      end if;
      cur_colname:='P_CDCFEEDISBAMT';
      cur_colvalue:=P_CDCFEEDISBAMT;
      if P_CDCFEEDISBAMT is null then

        new_rec.CDCFEEDISBAMT:=0;

      else
        new_rec.CDCFEEDISBAMT:=P_CDCFEEDISBAMT;
      end if;
      cur_colname:='P_CDCDISBAMT';
      cur_colvalue:=P_CDCDISBAMT;
      if P_CDCDISBAMT is null then

        new_rec.CDCDISBAMT:=0;

      else
        new_rec.CDCDISBAMT:=P_CDCDISBAMT;
      end if;
      cur_colname:='P_CDCPAIDTHRUDT';
      cur_colvalue:=P_CDCPAIDTHRUDT;
      if P_CDCPAIDTHRUDT is null then

        new_rec.CDCPAIDTHRUDT:=rpad(' ',10);

      else
        new_rec.CDCPAIDTHRUDT:=P_CDCPAIDTHRUDT;
      end if;
      cur_colname:='P_INTPAIDTHRUDT';
      cur_colvalue:=P_INTPAIDTHRUDT;
      if P_INTPAIDTHRUDT is null then

        new_rec.INTPAIDTHRUDT:=rpad(' ',10);

      else
        new_rec.INTPAIDTHRUDT:=P_INTPAIDTHRUDT;
      end if;
      cur_colname:='P_ADJTOAPPLINTAMT';
      cur_colvalue:=P_ADJTOAPPLINTAMT;
      if P_ADJTOAPPLINTAMT is null then

        new_rec.ADJTOAPPLINTAMT:=0;

      else
        new_rec.ADJTOAPPLINTAMT:=P_ADJTOAPPLINTAMT;
      end if;
      cur_colname:='P_ADJTOAPPLPRINAMT';
      cur_colvalue:=P_ADJTOAPPLPRINAMT;
      if P_ADJTOAPPLPRINAMT is null then

        new_rec.ADJTOAPPLPRINAMT:=0;

      else
        new_rec.ADJTOAPPLPRINAMT:=P_ADJTOAPPLPRINAMT;
      end if;
      cur_colname:='P_LATEFEEADJAMT';
      cur_colvalue:=P_LATEFEEADJAMT;
      if P_LATEFEEADJAMT is null then

        new_rec.LATEFEEADJAMT:=0;

      else
        new_rec.LATEFEEADJAMT:=P_LATEFEEADJAMT;
      end if;
      cur_colname:='P_UNALLOCAMT';
      cur_colvalue:=P_UNALLOCAMT;
      if P_UNALLOCAMT is null then

        new_rec.UNALLOCAMT:=0;

      else
        new_rec.UNALLOCAMT:=P_UNALLOCAMT;
      end if;
      cur_colname:='P_CURBALAMT';
      cur_colvalue:=P_CURBALAMT;
      if P_CURBALAMT is null then

        new_rec.CURBALAMT:=0;

      else
        new_rec.CURBALAMT:=P_CURBALAMT;
      end if;
      cur_colname:='P_PREPAYAMT';
      cur_colvalue:=P_PREPAYAMT;
      if P_PREPAYAMT is null then

        new_rec.PREPAYAMT:=0;

      else
        new_rec.PREPAYAMT:=P_PREPAYAMT;
      end if;
      cur_colname:='P_WIRECMNT';
      cur_colvalue:=P_WIRECMNT;
      if P_WIRECMNT is null then

        new_rec.WIRECMNT:=' ';

      else
        new_rec.WIRECMNT:=P_WIRECMNT;
      end if;
      cur_colname:='P_ADJTYP';
      cur_colvalue:=P_ADJTYP;
      if P_ADJTYP is null then

        new_rec.ADJTYP:=' ';

      else
        new_rec.ADJTYP:=P_ADJTYP;
      end if;
      cur_colname:='P_OVRWRITECD';
      cur_colvalue:=P_OVRWRITECD;
      if P_OVRWRITECD is null then

        new_rec.OVRWRITECD:=' ';

      else
        new_rec.OVRWRITECD:=P_OVRWRITECD;
      end if;
      cur_colname:='P_POSTINGTYP';
      cur_colvalue:=P_POSTINGTYP;
      if P_POSTINGTYP is null then

        new_rec.POSTINGTYP:=' ';

      else
        new_rec.POSTINGTYP:=P_POSTINGTYP;
      end if;
      cur_colname:='P_POSTINGSCHDLDT';
      cur_colvalue:=P_POSTINGSCHDLDT;
      if P_POSTINGSCHDLDT is null then

        new_rec.POSTINGSCHDLDT:=jan_1_1900;

      else
        new_rec.POSTINGSCHDLDT:=P_POSTINGSCHDLDT;
      end if;
      cur_colname:='P_POSTINGACTUALDT';
      cur_colvalue:=P_POSTINGACTUALDT;
      if P_POSTINGACTUALDT is null then

        new_rec.POSTINGACTUALDT:=NULL;

      else
        new_rec.POSTINGACTUALDT:=P_POSTINGACTUALDT;
      end if;
      cur_colname:='P_WIREPOSTINGID';
      cur_colvalue:=P_WIREPOSTINGID;
      if P_WIREPOSTINGID is null then

        new_rec.WIREPOSTINGID:=NEXTVAL_WIREPOSTINGIDSEQ();

      else
        new_rec.WIREPOSTINGID:=P_WIREPOSTINGID;
      end if;
      cur_colname:='P_TRANSID';
      cur_colvalue:=P_TRANSID;
      if P_TRANSID is null then

        new_rec.TRANSID:=0;

      else
        new_rec.TRANSID:=P_TRANSID;
      end if;
      cur_colname:='P_MFSTATCD';
      cur_colvalue:=P_MFSTATCD;
      if P_MFSTATCD is null then

        new_rec.MFSTATCD:=' ';

      else
        new_rec.MFSTATCD:=P_MFSTATCD;
      end if;
      cur_colname:='P_ACHDEL';
      cur_colvalue:=P_ACHDEL;
      if P_ACHDEL is null then

        new_rec.ACHDEL:=0;

      else
        new_rec.ACHDEL:=P_ACHDEL;
      end if;
      cur_colname:='P_OVRPAYAMT';
      cur_colvalue:=P_OVRPAYAMT;
      if P_OVRPAYAMT is null then

        new_rec.OVRPAYAMT:=0;

      else
        new_rec.OVRPAYAMT:=P_OVRPAYAMT;
      end if;
      cur_colname:='P_CDCREPDAMT';
      cur_colvalue:=P_CDCREPDAMT;
      if P_CDCREPDAMT is null then

        new_rec.CDCREPDAMT:=0;

      else
        new_rec.CDCREPDAMT:=P_CDCREPDAMT;
      end if;
      cur_colname:='P_CSAREPDAMT';
      cur_colvalue:=P_CSAREPDAMT;
      if P_CSAREPDAMT is null then

        new_rec.CSAREPDAMT:=0;

      else
        new_rec.CSAREPDAMT:=P_CSAREPDAMT;
      end if;
      cur_colname:='P_REPDAMT';
      cur_colvalue:=P_REPDAMT;
      if P_REPDAMT is null then

        new_rec.REPDAMT:=0;

      else
        new_rec.REPDAMT:=P_REPDAMT;
      end if;
      cur_colname:='P_PREPAYDT';
      cur_colvalue:=P_PREPAYDT;
      if P_PREPAYDT is null then

        new_rec.PREPAYDT:=jan_1_1900;

      else
        new_rec.PREPAYDT:=P_PREPAYDT;
      end if;
      cur_colname:='P_SEMIANDT';
      cur_colvalue:=P_SEMIANDT;
      if P_SEMIANDT is null then

        new_rec.SEMIANDT:=jan_1_1900;

      else
        new_rec.SEMIANDT:=P_SEMIANDT;
      end if;
      cur_colname:='P_ZEROBALAMT';
      cur_colvalue:=P_ZEROBALAMT;
      if P_ZEROBALAMT is null then

        new_rec.ZEROBALAMT:=0;

      else
        new_rec.ZEROBALAMT:=P_ZEROBALAMT;
      end if;
      cur_colname:='P_SEMIANCDCNMB';
      cur_colvalue:=P_SEMIANCDCNMB;
      if P_SEMIANCDCNMB is null then

        new_rec.SEMIANCDCNMB:=rpad(' ',8);

      else
        new_rec.SEMIANCDCNMB:=P_SEMIANCDCNMB;
      end if;
      cur_colname:='P_SEMIANNOTEBALAMT';
      cur_colvalue:=P_SEMIANNOTEBALAMT;
      if P_SEMIANNOTEBALAMT is null then

        new_rec.SEMIANNOTEBALAMT:=0;

      else
        new_rec.SEMIANNOTEBALAMT:=P_SEMIANNOTEBALAMT;
      end if;
      cur_colname:='P_SEMIANWIREAMT';
      cur_colvalue:=P_SEMIANWIREAMT;
      if P_SEMIANWIREAMT is null then

        new_rec.SEMIANWIREAMT:=0;

      else
        new_rec.SEMIANWIREAMT:=P_SEMIANWIREAMT;
      end if;
      cur_colname:='P_SEMIANPRINACCRAMT';
      cur_colvalue:=P_SEMIANPRINACCRAMT;
      if P_SEMIANPRINACCRAMT is null then

        new_rec.SEMIANPRINACCRAMT:=0;

      else
        new_rec.SEMIANPRINACCRAMT:=P_SEMIANPRINACCRAMT;
      end if;
      cur_colname:='P_SEMIANINTACCRAMT';
      cur_colvalue:=P_SEMIANINTACCRAMT;
      if P_SEMIANINTACCRAMT is null then

        new_rec.SEMIANINTACCRAMT:=0;

      else
        new_rec.SEMIANINTACCRAMT:=P_SEMIANINTACCRAMT;
      end if;
      cur_colname:='P_SEMIANOPENGNTYAMT';
      cur_colvalue:=P_SEMIANOPENGNTYAMT;
      if P_SEMIANOPENGNTYAMT is null then

        new_rec.SEMIANOPENGNTYAMT:=0;

      else
        new_rec.SEMIANOPENGNTYAMT:=P_SEMIANOPENGNTYAMT;
      end if;
      cur_colname:='P_SEMIANOPENGNTYERRAMT';
      cur_colvalue:=P_SEMIANOPENGNTYERRAMT;
      if P_SEMIANOPENGNTYERRAMT is null then

        new_rec.SEMIANOPENGNTYERRAMT:=0;

      else
        new_rec.SEMIANOPENGNTYERRAMT:=P_SEMIANOPENGNTYERRAMT;
      end if;
      cur_colname:='P_SEMIANOPENGNTYREMAINAMT';
      cur_colvalue:=P_SEMIANOPENGNTYREMAINAMT;
      if P_SEMIANOPENGNTYREMAINAMT is null then

        new_rec.SEMIANOPENGNTYREMAINAMT:=0;

      else
        new_rec.SEMIANOPENGNTYREMAINAMT:=P_SEMIANOPENGNTYREMAINAMT;
      end if;
      cur_colname:='P_SEMIANPIPYMTAFT6MOAMT';
      cur_colvalue:=P_SEMIANPIPYMTAFT6MOAMT;
      if P_SEMIANPIPYMTAFT6MOAMT is null then

        new_rec.SEMIANPIPYMTAFT6MOAMT:=0;

      else
        new_rec.SEMIANPIPYMTAFT6MOAMT:=P_SEMIANPIPYMTAFT6MOAMT;
      end if;
      cur_colname:='P_SEMIANPISHORTOVRAMT';
      cur_colvalue:=P_SEMIANPISHORTOVRAMT;
      if P_SEMIANPISHORTOVRAMT is null then

        new_rec.SEMIANPISHORTOVRAMT:=0;

      else
        new_rec.SEMIANPISHORTOVRAMT:=P_SEMIANPISHORTOVRAMT;
      end if;
      cur_colname:='P_SEMIANESCROWPRIORPYMTAMT';
      cur_colvalue:=P_SEMIANESCROWPRIORPYMTAMT;
      if P_SEMIANESCROWPRIORPYMTAMT is null then

        new_rec.SEMIANESCROWPRIORPYMTAMT:=0;

      else
        new_rec.SEMIANESCROWPRIORPYMTAMT:=P_SEMIANESCROWPRIORPYMTAMT;
      end if;
      cur_colname:='P_SEMIANDBENTRBALAMT';
      cur_colvalue:=P_SEMIANDBENTRBALAMT;
      if P_SEMIANDBENTRBALAMT is null then

        new_rec.SEMIANDBENTRBALAMT:=0;

      else
        new_rec.SEMIANDBENTRBALAMT:=P_SEMIANDBENTRBALAMT;
      end if;
      cur_colname:='P_SEMIANPREPAYPREMAMT';
      cur_colvalue:=P_SEMIANPREPAYPREMAMT;
      if P_SEMIANPREPAYPREMAMT is null then

        new_rec.SEMIANPREPAYPREMAMT:=0;

      else
        new_rec.SEMIANPREPAYPREMAMT:=P_SEMIANPREPAYPREMAMT;
      end if;
      cur_colname:='P_SEMIANAMT';
      cur_colvalue:=P_SEMIANAMT;
      if P_SEMIANAMT is null then

        new_rec.SEMIANAMT:=0;

      else
        new_rec.SEMIANAMT:=P_SEMIANAMT;
      end if;
      cur_colname:='P_SEMIANSBADUEFROMBORRAMT';
      cur_colvalue:=P_SEMIANSBADUEFROMBORRAMT;
      if P_SEMIANSBADUEFROMBORRAMT is null then

        new_rec.SEMIANSBADUEFROMBORRAMT:=0;

      else
        new_rec.SEMIANSBADUEFROMBORRAMT:=P_SEMIANSBADUEFROMBORRAMT;
      end if;
      cur_colname:='P_SEMIANSBABEHINDAMT';
      cur_colvalue:=P_SEMIANSBABEHINDAMT;
      if P_SEMIANSBABEHINDAMT is null then

        new_rec.SEMIANSBABEHINDAMT:=0;

      else
        new_rec.SEMIANSBABEHINDAMT:=P_SEMIANSBABEHINDAMT;
      end if;
      cur_colname:='P_SEMIANSBAACCRAMT';
      cur_colvalue:=P_SEMIANSBAACCRAMT;
      if P_SEMIANSBAACCRAMT is null then

        new_rec.SEMIANSBAACCRAMT:=0;

      else
        new_rec.SEMIANSBAACCRAMT:=P_SEMIANSBAACCRAMT;
      end if;
      cur_colname:='P_SEMIANSBAPRJCTDAMT';
      cur_colvalue:=P_SEMIANSBAPRJCTDAMT;
      if P_SEMIANSBAPRJCTDAMT is null then

        new_rec.SEMIANSBAPRJCTDAMT:=0;

      else
        new_rec.SEMIANSBAPRJCTDAMT:=P_SEMIANSBAPRJCTDAMT;
      end if;
      cur_colname:='P_SEMIANCSADUEFROMBORRAMT';
      cur_colvalue:=P_SEMIANCSADUEFROMBORRAMT;
      if P_SEMIANCSADUEFROMBORRAMT is null then

        new_rec.SEMIANCSADUEFROMBORRAMT:=0;

      else
        new_rec.SEMIANCSADUEFROMBORRAMT:=P_SEMIANCSADUEFROMBORRAMT;
      end if;
      cur_colname:='P_SEMIANCSABEHINDAMT';
      cur_colvalue:=P_SEMIANCSABEHINDAMT;
      if P_SEMIANCSABEHINDAMT is null then

        new_rec.SEMIANCSABEHINDAMT:=0;

      else
        new_rec.SEMIANCSABEHINDAMT:=P_SEMIANCSABEHINDAMT;
      end if;
      cur_colname:='P_SEMIANCSAACCRAMT';
      cur_colvalue:=P_SEMIANCSAACCRAMT;
      if P_SEMIANCSAACCRAMT is null then

        new_rec.SEMIANCSAACCRAMT:=0;

      else
        new_rec.SEMIANCSAACCRAMT:=P_SEMIANCSAACCRAMT;
      end if;
      cur_colname:='P_SEMIANCSAPRJCTDAMT';
      cur_colvalue:=P_SEMIANCSAPRJCTDAMT;
      if P_SEMIANCSAPRJCTDAMT is null then

        new_rec.SEMIANCSAPRJCTDAMT:=0;

      else
        new_rec.SEMIANCSAPRJCTDAMT:=P_SEMIANCSAPRJCTDAMT;
      end if;
      cur_colname:='P_SEMIANCDCDUEFROMBORRAMT';
      cur_colvalue:=P_SEMIANCDCDUEFROMBORRAMT;
      if P_SEMIANCDCDUEFROMBORRAMT is null then

        new_rec.SEMIANCDCDUEFROMBORRAMT:=0;

      else
        new_rec.SEMIANCDCDUEFROMBORRAMT:=P_SEMIANCDCDUEFROMBORRAMT;
      end if;
      cur_colname:='P_SEMIANCDCBEHINDAMT';
      cur_colvalue:=P_SEMIANCDCBEHINDAMT;
      if P_SEMIANCDCBEHINDAMT is null then

        new_rec.SEMIANCDCBEHINDAMT:=0;

      else
        new_rec.SEMIANCDCBEHINDAMT:=P_SEMIANCDCBEHINDAMT;
      end if;
      cur_colname:='P_SEMIANCDCACCRAMT';
      cur_colvalue:=P_SEMIANCDCACCRAMT;
      if P_SEMIANCDCACCRAMT is null then

        new_rec.SEMIANCDCACCRAMT:=0;

      else
        new_rec.SEMIANCDCACCRAMT:=P_SEMIANCDCACCRAMT;
      end if;
      cur_colname:='P_SEMIANACCRINTONDELAMT';
      cur_colvalue:=P_SEMIANACCRINTONDELAMT;
      if P_SEMIANACCRINTONDELAMT is null then

        new_rec.SEMIANACCRINTONDELAMT:=0;

      else
        new_rec.SEMIANACCRINTONDELAMT:=P_SEMIANACCRINTONDELAMT;
      end if;
      cur_colname:='P_SEMIANLATEFEEAMT';
      cur_colvalue:=P_SEMIANLATEFEEAMT;
      if P_SEMIANLATEFEEAMT is null then

        new_rec.SEMIANLATEFEEAMT:=0;

      else
        new_rec.SEMIANLATEFEEAMT:=P_SEMIANLATEFEEAMT;
      end if;
      cur_colname:='P_SEMIANPROOFAMT';
      cur_colvalue:=P_SEMIANPROOFAMT;
      if P_SEMIANPROOFAMT is null then

        new_rec.SEMIANPROOFAMT:=0;

      else
        new_rec.SEMIANPROOFAMT:=P_SEMIANPROOFAMT;
      end if;
      cur_colname:='P_TIMESTAMPFLD';
      cur_colvalue:=P_TIMESTAMPFLD;
      if P_TIMESTAMPFLD is null then

        new_rec.TIMESTAMPFLD:=jan_1_1900;

      else
        new_rec.TIMESTAMPFLD:=P_TIMESTAMPFLD;
      end if;
    exception when others then
       p_errval:=sqlcode;
       p_errmsg:='Error - Oracle returned '||sqlerrm(sqlcode)||' using '
           ||cur_colname||' Value >'||cur_colvalue||'<';
    end;
  end if;
    keystouse:='(WIREPOSTINGID)=('||new_rec.WIREPOSTINGID||')';
    if p_errval=0 then
      begin
        insert into STGCSA.PRPMFWIREPOSTINGTBL values new_rec;
        p_RETVAL := SQL%ROWCOUNT;
      exception
      when dup_val_on_index then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Insert caused dupe on unique IX: '||sqlerrm(sqlcode);
      when others then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<3 then maxsev:=3; end if;
        p_errmsg:='Inserting record caught SQL error '||sqlerrm(sqlcode);
      end;
    end if;
  else /* P_IDENTIFIER was unexpected value */
    p_errval:=-20002;
    if maxsev<3 then maxsev:=3; end if;
    p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier
      ||' inserting record with key '||keystouse;
  end if;
  if p_errval=0 then
    runtime.logger(logged_msgid_retval,'STDLOG',101,
      'End PRPMFWIREPOSTINGINSTSP With 0 return, P_RETVAL='
      ||p_retval||' Record Key '||keystouse,p_userid,
      1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt,
      l_timestampfld,PROGRAM_NAME=>'PRPMFWIREPOSTINGINSTSP');
  else
    p_errmsg:='Error: '||P_ERRMSG||' in PRPMFWIREPOSTINGINSTSP Record Key '||keystouse
      ||' Build: 2018-11-07 11:34:55';
    ROLLBACK TO PRPMFWIREPOSTINGINSTSP;
    runtime.logger(logged_msgid_retval,'STDERR',103,
      p_errmsg,p_userid,maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,
      l_entrydt, l_timestampfld,PROGRAM_NAME=>'PRPMFWIREPOSTINGINSTSP',force_log_entry=>TRUE);
  end if;
EXCEPTION
WHEN OTHERS THEN
  p_errval:=SQLCODE;
  p_errmsg:=' In outer exception handler in PRPMFWIREPOSTINGINSTSP caught exception '
    ||sqlerrm(sqlcode)||' Record Key '||keystouse||' build 2018-11-07 11:34:55';
  ROLLBACK TO PRPMFWIREPOSTINGINSTSP;
  runtime.logger(logged_msgid_retval,'STDERR',20,p_errmsg,p_userid,3,
  PROGRAM_NAME=>'PRPMFWIREPOSTINGINSTSP',force_log_entry=>TRUE);
--
END PRPMFWIREPOSTINGINSTSP;
/


GRANT EXECUTE ON STGCSA.PRPMFWIREPOSTINGINSTSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.PRPMFWIREPOSTINGINSTSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.PRPMFWIREPOSTINGINSTSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.PRPMFWIREPOSTINGINSTSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.PRPMFWIREPOSTINGINSTSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.PRPMFWIREPOSTINGINSTSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.PRPMFWIREPOSTINGINSTSP TO STGCSADEVROLE;

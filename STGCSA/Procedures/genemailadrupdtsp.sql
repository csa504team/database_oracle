DROP PROCEDURE STGCSA.GENEMAILADRUPDTSP;

CREATE OR REPLACE PROCEDURE STGCSA.GENEMAILADRUPDTSP(
    p_retval out number,
    p_errval out number,
    p_errmsg  out varchar2,
    p_identifier number:=0,
    p_userid varchar2
   ,p_EMAILADRID VARCHAR2:=null
   ,p_DSPLYNM VARCHAR2:=null
   ,p_GREETINGNM VARCHAR2:=null
   ,p_EMAILADR VARCHAR2:=null
   ,p_EMAILACTV VARCHAR2:=null
   ,p_TIMESTAMPFLD DATE:=null
) as
 /*
  Created on: 2018-11-07 11:33:39
  Created by: GENR
  Crerated from template stdtblupd_template v3.22 12 Oct 2018 on 2018-11-07 11:33:39
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  Procedure GENEMAILADRUPDTSP performs UPDATE on STGCSA.GENEMAILADRTBL
    for P_IDENTIFIER=0 qualification is on Primary Key: EMAILADRID
    Updates all columns except the qualification keys and columns for which a
      null value is passed.
    SPECIAL secret update kludge!
      if p_identifier is set to -1 then a flag is set to use all input parm values
      and p_identifier is changed to 0 (update by PK).
      THEREFORE if using p_identifier=-1 you MUST pass intended value for all columns.
      This option allows setting colulmns to null!
*/
  dbrec STGCSA.GENEMAILADRTBL%rowtype;
  logged_msg_id number;
  recnum number;
  rec_rowid rowid;
  keystouse varchar2(256);
  maxsev number:=0;
  pctsign char(1):='%';
  cur_colname varchar2(100):=null;
  cur_colvalue varchar2(4000);
  do_normal_field_checking char(1):='Y';
  overridden_p_identifier number;
  -- standard activity log capture vars
  l_LOANNMB char(10):=null;
  l_TRANSID number:=null;
  l_dbmdl varchar2(10):=null;
  l_entrydt date:=null;
  l_userid  varchar2(32):=null;
  l_usernm  varchar2(199):=null;
  l_timestampfld date:=null;
procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
begin
  if p_errval=0 then
    begin
      select * into dbrec from STGCSA.GENEMAILADRTBL where rowid=rowid_in;
    exception when others then
      p_errval:=sqlcode;
      maxsev:=3;
      p_retval:=0;
      p_errmsg:='Fetch of STGCSA.GENEMAILADRTBL row by rowid '||rowid_in
        ||' orinally fetched using '||orig_keyset_values
        ||' failed with '||sqlerrm(sqlcode);
    end;
  end if;
  if p_errval=0 then
    -- set this as lastupdt
    DBrec.lastupdtuserid:=p_userid;
    DBrec.lastupdtdt:=sysdate;
    -- copy into DBrec all table column input parameters that are not null,
    -- saving column name/value if needed for error reporting
    --  (if p_identifier=-1 then move input even if null)
    begin
      if P_EMAILADRID is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_EMAILADRID';
        cur_colvalue:=P_EMAILADRID;
        DBrec.EMAILADRID:=P_EMAILADRID;
      end if;
      if P_DSPLYNM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_DSPLYNM';
        cur_colvalue:=P_DSPLYNM;
        DBrec.DSPLYNM:=P_DSPLYNM;
      end if;
      if P_GREETINGNM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_GREETINGNM';
        cur_colvalue:=P_GREETINGNM;
        DBrec.GREETINGNM:=P_GREETINGNM;
      end if;
      if P_EMAILADR is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_EMAILADR';
        cur_colvalue:=P_EMAILADR;
        DBrec.EMAILADR:=P_EMAILADR;
      end if;
      if P_EMAILACTV is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_EMAILACTV';
        cur_colvalue:=P_EMAILACTV;
        DBrec.EMAILACTV:=P_EMAILACTV;
      end if;
      if P_TIMESTAMPFLD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_TIMESTAMPFLD';
        cur_colvalue:=P_TIMESTAMPFLD;
        DBrec.TIMESTAMPFLD:=P_TIMESTAMPFLD;
      end if;
    exception when others then
      p_errval:=sqlcode;
      if maxsev<3 then maxsev:=3; end if;
      p_errmsg:='Error using '||cur_colname||' Value >'||cur_colvalue||'<';
    end;
  end if;
  -- if all still ok do actual update
  if p_errval=0 then
    Begin
      update STGCSA.GENEMAILADRTBL
        set lastupdtuserid=DBrec.lastupdtuserid,
          lastupdtdt=DBrec.lastupdtdt
          ,EMAILADRID=DBrec.EMAILADRID
          ,DSPLYNM=DBrec.DSPLYNM
          ,GREETINGNM=DBrec.GREETINGNM
          ,EMAILADR=DBrec.EMAILADR
          ,EMAILACTV=DBrec.EMAILACTV
          ,TIMESTAMPFLD=DBrec.TIMESTAMPFLD
        where rowid=rowid_in;
      recnum:=recnum+1;
    exception when others then
      p_errval:=sqlcode;
      p_retval:=0;
      if maxsev<3 then maxsev:=3; end if;
      p_errmsg:= 'Error while doing update on STGCSA.GENEMAILADRTBL, on rec with rowid '
      ||rowid_in||' previously fetched using key: '||orig_keyset_values
      ||' got Oracle error '||sqlerrm(sqlcode);
    end;
  end if;
END;
begin
  -- setup
  savepoint GENEMAILADRUPDTSP;
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  recnum:=0;
  runtime.logger(logged_msg_id,'STDLOG',100,'Init GENEMAILADRUPDTSP',p_userid,4,
    logtxt1=>'GENEMAILADRUPDTSP',logtxt2=>'stdtblupd_template v3.22 12 Oct 2018',
    PROGRAM_NAME=>'GENEMAILADRUPDTSP');
  --
  l_TIMESTAMPFLD:=P_TIMESTAMPFLD;
--
-- special p_identifier kludge
--  if p_identifier is set to -1 then it treated as 0 (update by PK)
--  but a flag is set use passed value EVEN IF NULL.  Since for option -1
--  every column is updated to what was passed, intended values MUST be passed
--  for all columns.  This option allows setting colulmns to null!
  overridden_p_identifier:=p_identifier;
  if p_identifier=-1 then
    do_normal_field_checking:='N';
    overridden_p_identifier:=0;
  end if;
  case overridden_p_identifier
  when 0 then
    -- case to update one row based on primary key
    -- p_identifier=0 (choice 0) is default action for a stored procedure
    -- get existing version of row
    if p_errval=0 then
      keystouse:='(EMAILADRID)=('||P_EMAILADRID||')';
      begin
        select rowid into rec_rowid from STGCSA.GENEMAILADRTBL where 1=1
         and EMAILADRID=P_EMAILADRID;
      exception when no_data_found then
        p_errval:=0;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Warning No STGCSA.GENEMAILADRTBL row to update with key(s) '
          ||keystouse;
      end;
    end if;
    if p_errval=0 then
      fetch_and_update_by_rowid(rec_rowid, keystouse);
    end if;
  else /* P_IDENTIFIER was unexpected value */
    p_errval:=-20002;
    if maxsev<3 then maxsev:=3; end if;
    p_errmsg:='Error P_IDENTIFIER contained unexpected value: '||p_identifier;
  end case;
  P_RETVAL:=RECNUM;
  -- post to log
  if p_errval=0 then
    runtime.logger(logged_msg_id,'STDLOG',101,
      'End GENEMAILADRUPDTSP With 0 return, P_RETVAL='
      ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
      1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld
      ,PROGRAM_NAME=>'GENEMAILADRUPDTSP');
  else
    ROLLBACK TO GENEMAILADRUPDTSP;
    p_errmsg:=p_errmsg||' in GENEMAILADRUPDTSP build 2018-11-07 11:33:39';
    runtime.logger(logged_msg_id,'STDERR',103,p_errmsg,p_userid,
      maxsev, l_usernm, l_loannmb,l_transid, l_dbmdl, l_entrydt, l_timestampfld,
      PROGRAM_NAME=>'GENEMAILADRUPDTSP',force_log_entry=>true);
  end if;
EXCEPTION
WHEN OTHERS THEN
  ROLLBACK TO GENEMAILADRUPDTSP;
  p_errmsg:=' Error outer exception handler caught Oracle error:'||SQLERRM(SQLCODE)
    ||' in GENEMAILADRUPDTSP build 2018-11-07 11:33:39';
  runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
    3,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld,
    PROGRAM_NAME=>'GENEMAILADRUPDTSP',force_log_entry=>true);
--
END GENEMAILADRUPDTSP;
/


GRANT EXECUTE ON STGCSA.GENEMAILADRUPDTSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.GENEMAILADRUPDTSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.GENEMAILADRUPDTSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.GENEMAILADRUPDTSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.GENEMAILADRUPDTSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.GENEMAILADRUPDTSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.GENEMAILADRUPDTSP TO STGCSADEVROLE;

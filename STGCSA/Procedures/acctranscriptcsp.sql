DROP PROCEDURE STGCSA.ACCTRANSCRIPTCSP;

CREATE OR REPLACE PROCEDURE STGCSA.ACCTRANSCRIPTCSP
    (p_retval out number, 
    p_errval out number,  
    p_errmsg out varchar2, 
    p_identifier number:=0, 
    p_userid varchar2, 
    p_rpt_start_dt_in date:=null, 
    p_rpt_end_dt_in date:=null) AS
  --
  -- J. Low rev v1.05 14 Apr 2018
  -- R. Jochum rev v1.06 04 JUN 2018 - corrected DUEDT mapping
  pgm varchar2(30):='ACCTRANSCRIPTCSP';
  ver varchar2(40):='V1.07 23 Jul 2018';  
  dtfmt varchar2(40):='YYYY-MM-DD HH24:MI:SS';
  V_ACCELERATNID number;
  V_SBAENTRYDT Date;
  log_LOANNMB char(10):=null;
  log_TRANSID number:=null;
  log_dbmdl varchar2(10):=null;
  log_entrydt date:=null;
  log_userid  varchar2(32):=null;
  log_usernm  varchar2(199):=null;
  log_timestampfld date:=null;  
  logged_msg_id number;
  p_rpt_start_dt date;
  p_rpt_end_dt date; 
  --
  ACC_UPD_CNT number:=0;
  ACC_NFD_CNT number:=0;
  Ret_msg varchar2(256);
  rundate date;
BEGIN
  savepoint ACCTRANSCRIPTCSP;
  p_errval:=0;
  p_retval:=0;
  p_errmsg:=pgm||' '||ver||': Normal completion';
  --
  -- default report dates if not passed
  if p_rpt_start_dt_in is null then
    p_rpt_start_dt:=to_date(to_char(trunc(sysdate,'MM'),'yyyymmdd'),'yyyymmdd');
  else 
    p_rpt_start_dt:=p_rpt_start_dt_in;  
  end if;
  if p_rpt_end_dt is null then  
    p_rpt_end_dt:=to_date(to_char(trunc(add_months(sysdate,1),'MM'),'yyyymmdd'),'yyyymmdd');
  else
    p_rpt_end_dt:=p_rpt_end_dt_in;
  end if;  
  stgcsa.runtime_logger(logged_msg_id,'STDLOG',100,
    pgm||' '||ver||' entered, P_ID='||P_IDENTIFIER 
    ||' Start_dt='||to_char(p_rpt_start_dt,dtfmt)||' End_dt='||to_char(p_rpt_end_dt,dtfmt),
    p_userid,1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld,
      logtxt1=>pgm, logtxt2=>ver);
  select sysdate into rundate from dual;  
  if p_identifier<> 0 then
    p_errval:=-20001;
    p_errmsg:=PGM||' '||ver||' called with invalid p_identifier: '||P_IDENTIFIER
      ||' Only supported value is 0';
  end if;
  if p_errval=0 then    
    -- call "transcript report" to stage data in temp table
    STGCSA.AcclTrnscrptGen2TBLCSP(p_retval, p_errval, p_errmsg,
      0, p_userid,p_rpt_start_dt, p_rpt_end_dt);
  end if;
  if p_errval=0 then
    -- sanity check... aggragation of temp data groups on many cols, But they 
    --    should all be determinind by loannmb... two result rows for a loannmb
    --    is a fatal error
    for e in (select loannmb, count(*) cnt from
        (select loannmb, DBENTRPRINBALAMT, DBENTRINTDUEAMT, AMORTBALAMT, DUEDT,
        NOTERTPCT, LASTPYMTDT, LASTBALAMT, 
-- added (nonsummed) columns:
        CSAFEEDUEAMT, CDCFeeOwedAmt, EscrowBalAmt, DueAmt, tIntAmt,
        sum(CSAFEEAMT) SUM_CSAFEEAMT, sum(ACCRINTAMT) sum_ACCRINTAMT, 
        sum(CDCFEEAMT) SUM_CDCFEEAMT, sum(UNALLOCAMT) SUM_UNALLOCAMT, 
        sum(PRINAMT) SUM_PRINAMT, sum(INTAMT) SUM_INTAMT
      from stgcsa.TTTRANSCRIPTRPT1
      group by loannmb, DBENTRPRINBALAMT, DBENTRINTDUEAMT, AMORTBALAMT, DUEDT,
        NOTERTPCT, LASTPYMTDT, LASTPYMTDT, LASTBALAMT, 
-- added (nonsummed) columns
        CSAFEEDUEAMT, CDCFeeOwedAmt, EscrowBalAmt, DueAmt, tIntAmt)
      group by loannmb having count(*)>1 order by loannmb)
    loop  
      p_errval:=-20001;
      p_errmsg:='Error - multiple rows in result set for LOANNMB='||e.loannmb
        ||' After calling CDCONLINE.AcclTrnscrptGen2TBLCSP to stage transcript data'
        ||' For  Start_dt='||to_char(p_rpt_start_dt,dtfmt)||' End_dt='||to_char(p_rpt_end_dt,dtfmt)
        ||'. There should be one aggragate row for a LOANNMB, logic error.';    
      stgcsa.runtime_logger(logged_msg_id,'STDERR',102,p_errmsg,
        p_userid,1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld);
      p_errmsg:=pgm||' '||ver||' terminated because Gen of TRANSCRIPT data did not produce distinct'
        ||' data for one or more LOANNMB.';
    end loop;   
  end if;   
  if p_errval=0 then
    -- process each aggragated loannmb in temp result    
    for r in (select loannmb, DBENTRPRINBALAMT, DBENTRINTDUEAMT, AMORTBALAMT, DUEDT,
        NOTERTPCT, LASTPYMTDT, LASTBALAMT, 
-- added (nonsummed) columns:
        CSAFEEDUEAMT, CDCFeeOwedAmt, EscrowBalAmt, DueAmt, tIntAmt,
        sum(CSAFEEAMT) SUM_CSAFEEAMT, sum(ACCRINTAMT) sum_ACCRINTAMT, 
        sum(CDCFEEAMT) SUM_CDCFEEAMT, sum(UNALLOCAMT) SUM_UNALLOCAMT, 
        sum(PRINAMT) SUM_PRINAMT, sum(INTAMT) SUM_INTAMT
      from stgcsa.TTTRANSCRIPTRPT1
      group by loannmb, DBENTRPRINBALAMT, DBENTRINTDUEAMT, AMORTBALAMT, DUEDT,
        NOTERTPCT, LASTPYMTDT, LASTPYMTDT, LASTBALAMT, 
-- added (nonsummed) columns
        CSAFEEDUEAMT, CDCFeeOwedAmt, EscrowBalAmt, DueAmt, tIntAmt 
        order by loannmb)
    loop
      -- first get ACCELERATNID key for loan LOANNMB as "active" in ACCACCELERATELOANTBL
      if p_errval<>0 then 
        exit;
      end if;  
      begin 
        select  ACCELERATNID, SBAENTRYDT into V_ACCELERATNID, V_SBAENTRYDT
          from ACCACCELERATELOANTBL where LOANNMB=r.loannmb 
          and statid=49 and trunc(PPAENTRYDT,'MM') =trunc(sysdate,'MM')
          and PRIORACCELERATN=0;
      exception  
      when no_data_found then
        V_ACCELERATNID:=0;   
        ACC_NFD_CNT:=ACC_NFD_CNT+1;   
      When too_many_rows then
        V_ACCELERATNID:=0;      
        p_errval:=sqlcode;
        p_errmsg:='Error - Looking up active ACCELERATNID for loan '||r.loannmb
          ||' in ACCACCELERATELOANTBL, got too many rows.  '||sqlerrm(sqlcode);
      when others then    
        V_ACCELERATNID:=0;      
        p_errval:=sqlcode;
        p_errmsg:='Error - Looking up active ACCELERATNID for loan '||r.loannmb
          ||' in ACCACCELERATELOANTBL, got '||sqlerrm(sqlcode);
      end;
      if p_errval=0  and V_ACCELERATNID<>0 then
        if V_SBAENTRYDT is null then 
          V_SBAENTRYDT:=rundate;  
        end if;
        ACCACCELERATELOANUPDTSP(
          p_retval, p_errval, p_errmsg, 0, p_userid
          ,p_ACCELERATNID=>v_ACCELERATNID
          ,p_SBAENTRYDT=>V_SBAENTRYDT
          ,p_SBALASTTRNSCRPTIMPTDT=>rundate
          ,p_SBADBENTRBALATSEMIANAMT=>r.DBENTRPRINBALAMT
          ,p_SBAINTDUEATSEMIANAMT=>r.DBENTRINTDUEAMT
--          ,p_SBACSAFEEAMT=>r.SUM_CSAFEEAMT
          ,p_SBACSAFEEAMT=>r.CSAFEEDUEAMT
--          ,p_SBACDCFEEAMT=>r.SUM_CDCFEEAMT
          ,p_SBACDCFEEAMT=>r.CDCFeeOwedAmt
--          ,p_SBAESCROWPRIORPYMTAMT=>(r.SUM_UNALLOCAMT+r.SUM_PRINAMT+r.SUM_INTAMT)
--          ,p_SBAESCROWPRIORPYMTAMT=>(r.EscrowBalAmt - r.CSAFeeDueAmt)
          ,p_SBAESCROWPRIORPYMTAMT=>(r.EscrowBalAmt)
--          ,p_SBAACCELAMT=>r.AMORTBALAMT
          ,p_SBAACCELAMT=>r.DueAmt
          ,p_SBADUEDT=>r.DUEDT
          ,p_SBANOTERTPCT=>r.NOTERTPCT
--          ,p_SBALASTNOTEBALAMT=>r.LASTBALAMT
          ,p_SBALASTNOTEBALAMT=>r.LASTBALAMT
--          ,p_SBALASTNOTEBALAMT=>r.AmortBalAmt
          ,p_SBALASTPYMTDT=>r.LASTPYMTDT
--          ,p_SBAACCRINTDUEAMT=>r.sum_ACCRINTAMT
          ,p_SBAACCRINTDUEAMT=>r.tIntAmt
          ,p_SBAEXCESSAMT=>r.SUM_UNALLOCAMT
           ,P_MFCSADUEFROMBORRAMT=>r.CDCFEEOWEDAMT);
        ACC_UPD_CNT:=ACC_UPD_CNT+1; 
/*  removed logging each loan...action and key is logged in std update proc
        stgcsa.runtime_logger(logged_msg_id,'STDLOG',114,   
          'Updated ACCACCELERATELOAN ACCELERATNID='||v_ACCELERATNID
          ||'LOANNMB='||r.loannmb, p_userid,1,log_usernm,r.loannmb,log_transid, 
          log_dbmdl,log_entrydt, log_timestampfld,lognmb1=>r.loannmb,
          lognmb2=>V_ACCELERATNID, logtxt1=>pgm, logtxt2=>ver);                  
*/        
      end if;  
    end loop;
  end if;
  -- 
  -- done all LOANNMB in transcript, but process may have encountered error
  if p_errval<>0 then 
    p_retval:=0;
    rollback to ACCTRANSCRIPTCSP;
    stgcsa.runtime_logger(logged_msg_id,'STDERR',112,   
      p_errmsg,
      p_userid,1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld,
      logtxt1=>pgm, logtxt2=>ver);    
    stgcsa.runtime_logger(logged_msg_id,'STDERR',113,   
      'ACCTRANSCRIPTCSP rolled back due to errors.',
      p_userid,1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld,
      logtxt1=>pgm, logtxt2=>ver);    
  else
    p_retval:=acc_upd_cnt;  
    p_errmsg:='ACCTRANSCRIPTCSP normal completion, Loans UPD: '||ACC_UPD_CNT
      ||' Loans in Transcript but not ACCACCELERATELOAN: '||ACC_NFD_CNT;
    stgcsa.runtime_logger(logged_msg_id,'STDLOG',111,   
      p_errmsg, p_userid,1,log_usernm,log_loannmb,log_transid, log_dbmdl,
      log_entrydt, log_timestampfld,logtxt1=>pgm, logtxt2=>ver);  
  end if;      
exception when others then
    rollback to ACCTRANSCRIPTCSP;
    p_errval:=sqlcode;
    p_retval:=0;
    stgcsa.runtime_logger(logged_msg_id,'STDERR',114,   
      'ACCTRANSCRIPTCSP caught exception in outer handler: '||sqlcode||' '||sqlerrm(sqlcode),
      p_userid,1,log_usernm,log_loannmb,log_transid, log_dbmdl,log_entrydt, log_timestampfld,
      logtxt1=>pgm, logtxt2=>ver);  
      raise;                
end ACCTRANSCRIPTCSP;
/


GRANT EXECUTE ON STGCSA.ACCTRANSCRIPTCSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.ACCTRANSCRIPTCSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.ACCTRANSCRIPTCSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.ACCTRANSCRIPTCSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.ACCTRANSCRIPTCSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.ACCTRANSCRIPTCSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.ACCTRANSCRIPTCSP TO STGCSADEVROLE;

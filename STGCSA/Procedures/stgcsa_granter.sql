DROP PROCEDURE STGCSA.STGCSA_GRANTER;

CREATE OR REPLACE PROCEDURE STGCSA.stgcsa_granter(days_new number:=1, env varchar2:='none') as
  sqltxt varchar2(4000);
  cnt number:=0;
  grantlist varchar2(1000);
begin
  dbms_output.enable(1000000);
  -- first procedure, function, pkg grants
  if env='DEV' then 
    grantlist:='LOANCSAADMINROLE,LOANCSAANALYSTROLE,LOANCSAREVIEWERROLE,STGCSADEVROLE,'  
      ||'LOANCSAREADALLROLE,CSAUPDTROLE,LOANCSAMANAGERROLE';
  elsif env='TEST' then     
    grantlist:='LOANCSAADMINROLE,LOANCSAANALYSTROLE,LOANCSAREVIEWERROLE,'  
      ||'LOANCSAREADALLROLE,CSAUPDTROLE,LOANCSAMANAGERROLE';
  else 
    raise_application_error(-20001,'ENV must be DEV or TEST, Aborting.');
  end if;      
  for r in 
    (select object_name from user_objects 
      where object_type in ('PROCEDURE','PACKAGE','FUNCTION')
        and last_ddl_time>(sysdate-days_new))
  loop
    sqltxt:='grant execute on '||r.object_name||' to '||grantlist;
--    ||'LOANCSAADMINROLE,LOANCSAANALYSTROLE,LOANCSAREADALLROLE,'
--    ||'LOANCSAREVIWERROLE,STGCSADEVROLE';  
    execute immediate sqltxt; 
    cnt:=cnt+1;
    dbms_output.put_line('Granted '||r.object_name); 
  end loop;
  -- now things that get select
  if env='DEV' then 
    grantlist:='LOANCSAADMINROLE,LOANCSAANALYSTROLE,LOANCSAREVIEWERROLE,STGCSADEVROLE,'  
    ||'LOANCSAREADALLROLE,LOANCSAMANAGERROLE';
  elsif env='TEST' then     
    grantlist:='LOANCSAADMINROLE,LOANCSAANALYSTROLE,LOANCSAREVIEWERROLE,'  
    ||'LOANCSAREADALLROLE,LOANCSAMANAGERROLE';
  else 
    raise_application_error(-20001,'ENV must be DEV or TEST, Aborting.');
  end if;         
  for r in 
    (select object_name from user_objects 
      where object_type in ('VIEW','SEQUENCE','TABLE')
        and last_ddl_time>(sysdate-days_new))
  loop
    sqltxt:='grant select on '||r.object_name||' to '
      ||'LOANCSAADMINROLE,LOANCSAANALYSTROLE,LOANCSAREVIEWERROLE,STGCSADEVROLE,'  
    ||'LOANCSAREADALLROLE,LOANCSAMANAGERROLE';
--    ||'LOANCSAADMINROLE,LOANCSAANALYSTROLE,LOANCSAREADALLROLE,'
--    ||'LOANCSAREVIWERROLE,STGCSADEVROLE';  
    execute immediate sqltxt; 
    cnt:=cnt+1;
    dbms_output.put_line('Granted '||r.object_name); 
  end loop;         
end;
/


GRANT EXECUTE ON STGCSA.STGCSA_GRANTER TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.STGCSA_GRANTER TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.STGCSA_GRANTER TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.STGCSA_GRANTER TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.STGCSA_GRANTER TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.STGCSA_GRANTER TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.STGCSA_GRANTER TO STGCSADEVROLE;

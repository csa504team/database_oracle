DROP PROCEDURE STGCSA.MYPROCEDURE1;

CREATE OR REPLACE PROCEDURE STGCSA.myprocedure1
is
   stmt varchar2(1000);
   stmt2 varchar2(1000);
begin
    stmt := 'create global temporary table stgcsa.temp(id number(10))';
    execute immediate stmt;
    stmt2 := 'insert into stgcsa.temp(id) values (10)';
    execute immediate stmt2;
    dbms_output.put_line(temp.id);
end;
/

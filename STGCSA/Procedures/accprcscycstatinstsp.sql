DROP PROCEDURE STGCSA.ACCPRCSCYCSTATINSTSP;

CREATE OR REPLACE PROCEDURE STGCSA.ACCPRCSCYCSTATINSTSP(
    p_retval out number,
    p_errval out number,
    p_errmsg  out varchar2,
    p_identifier number:=0,
    p_userid varchar2
   ,p_CYCPRCSID VARCHAR2:=null
   ,p_CYCID VARCHAR2:=null
   ,p_PRCSID VARCHAR2:=null
   ,p_SCHDLDT DATE:=null
   ,p_SCHDLUSER VARCHAR2:=null
   ,p_SETUPDT DATE:=null
   ,p_LASTREFRESHDT DATE:=null
   ,p_INITIALRVWCMPLT VARCHAR2:=null
   ,p_INITIALRVWUSER VARCHAR2:=null
   ,p_INITIALRVWDT DATE:=null
   ,p_PEERRVWCMPLT VARCHAR2:=null
   ,p_PEERRVWUSER VARCHAR2:=null
   ,p_PEERRVWDT DATE:=null
   ,p_UPDTCMPLT VARCHAR2:=null
   ,p_UPDTCMPLTUSER VARCHAR2:=null
   ,p_UPDTCMPLTDT DATE:=null
   ,p_MGRRVWCMPLT VARCHAR2:=null
   ,p_MGRRVWUSER VARCHAR2:=null
   ,p_MGRRVWDT DATE:=null
   ,p_UPDTRUNNING VARCHAR2:=null
   ,p_UPDTRUNNINGUSER VARCHAR2:=null
   ,p_PRCSCYCCMNT VARCHAR2:=null
   ,p_PROPOSEDDT DATE:=null
   ,p_TIMESTAMPFLD DATE:=null
) as
 /*
  Created on: 2018-11-07 11:32:12
  Created by: GENR
  Crerated from template stdtblins_template v3.05 13 Sep 2018 on 2018-11-07 11:32:12
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  Procedure performs INSERT on STGCSA.ACCPRCSCYCSTATTBLfor P_IDENTIFIER=0
*/
  rundate date;
  gn global_name.global_name%type;
  new_rec STGCSA.ACCPRCSCYCSTATTBL%rowtype;
  logged_msgid_retval number;
  audretval number;
  crossover_active char(1);
  l_LOANNMB char(10):=null;
  l_TRANSID number:=null;
  l_dbmdl varchar2(10):=null;
  l_entrydt date:=null;
  l_userid  varchar2(32):=null;
  l_usernm  varchar2(199):=null;
  l_timestampfld date:=null;
  cur_col_name varchar2(100):=null;
  maxsev number:=0;
  holdstring varchar2(4000);
  keystouse varchar2(1000);
  -- std date for column defaulting
  JAN_1_1900  constant date:=to_date('JAN-01-1900','MON-DD-YYYY');
  cur_colvalue varchar2(4095);
  cur_colname varchar2(30);
-- Function used to initialize CYCPRCSID from sequence CYCPRCSIDSEQ
Function NEXTVAL_CYCPRCSIDSEQ return number is
  N number;
begin
  select CYCPRCSIDSEQ.nextval into n from dual;
  return n;
end;
-- Main body begins here
begin
  -- setup
  savepoint ACCPRCSCYCSTATINSTSP;
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  runtime.logger(logged_msgid_retval,'STDLOG',100,'Init ACCPRCSCYCSTATINSTSP',p_userid,4,
    logtxt1=>'ACCPRCSCYCSTATINSTSP',logtxt2=>'stdtblins_template v3.05 13 Sep 2018',
    PROGRAM_NAME=>'ACCPRCSCYCSTATINSTSP');
  select global_name, sysdate into gn, rundate from global_name;
  begin
    cur_colname:='TIMESTAMPFLD';
    cur_colvalue:=P_TIMESTAMPFLD;
    l_TIMESTAMPFLD:=P_TIMESTAMPFLD;
  exception when others then
    p_errval:=sqlcode;
    p_errmsg:='Error '||sqlerrm(sqlcode)||' using '||cur_colname||' Value >'||cur_colvalue||'<';
  end;
  new_rec.creatuserid:=p_userid;
  new_rec.creatdt:=sysdate;
  new_rec.lastupdtuserid:=p_userid;
  new_rec.lastupdtdt:=sysdate;
  if p_identifier=0 then
    -- p_identifier=0 this is default for a stored procedure, only action for insert
    runtime.errfound:=false;
    runtime.errstr:=' ';
    --
    -- Default columns as applicable
  if p_errval=0 then
    begin
      cur_colname:='P_CYCPRCSID';
      cur_colvalue:=P_CYCPRCSID;
      if P_CYCPRCSID is null then

        new_rec.CYCPRCSID:=NEXTVAL_CYCPRCSIDSEQ();

      else
        new_rec.CYCPRCSID:=P_CYCPRCSID;
      end if;
      cur_colname:='P_CYCID';
      cur_colvalue:=P_CYCID;
      if P_CYCID is null then

        new_rec.CYCID:=0;

      else
        new_rec.CYCID:=P_CYCID;
      end if;
      cur_colname:='P_PRCSID';
      cur_colvalue:=P_PRCSID;
      if P_PRCSID is null then

        new_rec.PRCSID:=0;

      else
        new_rec.PRCSID:=P_PRCSID;
      end if;
      cur_colname:='P_SCHDLDT';
      cur_colvalue:=P_SCHDLDT;
      if P_SCHDLDT is null then

        new_rec.SCHDLDT:=jan_1_1900;

      else
        new_rec.SCHDLDT:=P_SCHDLDT;
      end if;
      cur_colname:='P_SCHDLUSER';
      cur_colvalue:=P_SCHDLUSER;
      if P_SCHDLUSER is null then

        new_rec.SCHDLUSER:=' ';

      else
        new_rec.SCHDLUSER:=P_SCHDLUSER;
      end if;
      cur_colname:='P_SETUPDT';
      cur_colvalue:=P_SETUPDT;
      if P_SETUPDT is null then

        new_rec.SETUPDT:=jan_1_1900;

      else
        new_rec.SETUPDT:=P_SETUPDT;
      end if;
      cur_colname:='P_LASTREFRESHDT';
      cur_colvalue:=P_LASTREFRESHDT;
      if P_LASTREFRESHDT is null then

        new_rec.LASTREFRESHDT:=jan_1_1900;

      else
        new_rec.LASTREFRESHDT:=P_LASTREFRESHDT;
      end if;
      cur_colname:='P_INITIALRVWCMPLT';
      cur_colvalue:=P_INITIALRVWCMPLT;
      if P_INITIALRVWCMPLT is null then

        new_rec.INITIALRVWCMPLT:=0;

      else
        new_rec.INITIALRVWCMPLT:=P_INITIALRVWCMPLT;
      end if;
      cur_colname:='P_INITIALRVWUSER';
      cur_colvalue:=P_INITIALRVWUSER;
      if P_INITIALRVWUSER is null then

        new_rec.INITIALRVWUSER:=' ';

      else
        new_rec.INITIALRVWUSER:=P_INITIALRVWUSER;
      end if;
      cur_colname:='P_INITIALRVWDT';
      cur_colvalue:=P_INITIALRVWDT;
      if P_INITIALRVWDT is null then

        new_rec.INITIALRVWDT:=jan_1_1900;

      else
        new_rec.INITIALRVWDT:=P_INITIALRVWDT;
      end if;
      cur_colname:='P_PEERRVWCMPLT';
      cur_colvalue:=P_PEERRVWCMPLT;
      if P_PEERRVWCMPLT is null then

        new_rec.PEERRVWCMPLT:=0;

      else
        new_rec.PEERRVWCMPLT:=P_PEERRVWCMPLT;
      end if;
      cur_colname:='P_PEERRVWUSER';
      cur_colvalue:=P_PEERRVWUSER;
      if P_PEERRVWUSER is null then

        new_rec.PEERRVWUSER:=' ';

      else
        new_rec.PEERRVWUSER:=P_PEERRVWUSER;
      end if;
      cur_colname:='P_PEERRVWDT';
      cur_colvalue:=P_PEERRVWDT;
      if P_PEERRVWDT is null then

        new_rec.PEERRVWDT:=jan_1_1900;

      else
        new_rec.PEERRVWDT:=P_PEERRVWDT;
      end if;
      cur_colname:='P_UPDTCMPLT';
      cur_colvalue:=P_UPDTCMPLT;
      if P_UPDTCMPLT is null then

        new_rec.UPDTCMPLT:=0;

      else
        new_rec.UPDTCMPLT:=P_UPDTCMPLT;
      end if;
      cur_colname:='P_UPDTCMPLTUSER';
      cur_colvalue:=P_UPDTCMPLTUSER;
      if P_UPDTCMPLTUSER is null then

        new_rec.UPDTCMPLTUSER:=' ';

      else
        new_rec.UPDTCMPLTUSER:=P_UPDTCMPLTUSER;
      end if;
      cur_colname:='P_UPDTCMPLTDT';
      cur_colvalue:=P_UPDTCMPLTDT;
      if P_UPDTCMPLTDT is null then

        new_rec.UPDTCMPLTDT:=jan_1_1900;

      else
        new_rec.UPDTCMPLTDT:=P_UPDTCMPLTDT;
      end if;
      cur_colname:='P_MGRRVWCMPLT';
      cur_colvalue:=P_MGRRVWCMPLT;
      if P_MGRRVWCMPLT is null then

        new_rec.MGRRVWCMPLT:=0;

      else
        new_rec.MGRRVWCMPLT:=P_MGRRVWCMPLT;
      end if;
      cur_colname:='P_MGRRVWUSER';
      cur_colvalue:=P_MGRRVWUSER;
      if P_MGRRVWUSER is null then

        new_rec.MGRRVWUSER:=' ';

      else
        new_rec.MGRRVWUSER:=P_MGRRVWUSER;
      end if;
      cur_colname:='P_MGRRVWDT';
      cur_colvalue:=P_MGRRVWDT;
      if P_MGRRVWDT is null then

        new_rec.MGRRVWDT:=jan_1_1900;

      else
        new_rec.MGRRVWDT:=P_MGRRVWDT;
      end if;
      cur_colname:='P_UPDTRUNNING';
      cur_colvalue:=P_UPDTRUNNING;
      if P_UPDTRUNNING is null then

        new_rec.UPDTRUNNING:=0;

      else
        new_rec.UPDTRUNNING:=P_UPDTRUNNING;
      end if;
      cur_colname:='P_UPDTRUNNINGUSER';
      cur_colvalue:=P_UPDTRUNNINGUSER;
      if P_UPDTRUNNINGUSER is null then

        new_rec.UPDTRUNNINGUSER:=' ';

      else
        new_rec.UPDTRUNNINGUSER:=P_UPDTRUNNINGUSER;
      end if;
      cur_colname:='P_PRCSCYCCMNT';
      cur_colvalue:=P_PRCSCYCCMNT;
      if P_PRCSCYCCMNT is null then

        new_rec.PRCSCYCCMNT:=' ';

      else
        new_rec.PRCSCYCCMNT:=P_PRCSCYCCMNT;
      end if;
      cur_colname:='P_PROPOSEDDT';
      cur_colvalue:=P_PROPOSEDDT;
      if P_PROPOSEDDT is null then

        new_rec.PROPOSEDDT:=jan_1_1900;

      else
        new_rec.PROPOSEDDT:=P_PROPOSEDDT;
      end if;
      cur_colname:='P_TIMESTAMPFLD';
      cur_colvalue:=P_TIMESTAMPFLD;
      if P_TIMESTAMPFLD is null then

        new_rec.TIMESTAMPFLD:=jan_1_1900;

      else
        new_rec.TIMESTAMPFLD:=P_TIMESTAMPFLD;
      end if;
    exception when others then
       p_errval:=sqlcode;
       p_errmsg:='Error - Oracle returned '||sqlerrm(sqlcode)||' using '
           ||cur_colname||' Value >'||cur_colvalue||'<';
    end;
  end if;
    keystouse:='(CYCPRCSID)=('||new_rec.CYCPRCSID||')';
    if p_errval=0 then
      begin
        insert into STGCSA.ACCPRCSCYCSTATTBL values new_rec;
        p_RETVAL := SQL%ROWCOUNT;
      exception
      when dup_val_on_index then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Insert caused dupe on unique IX: '||sqlerrm(sqlcode);
      when others then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<3 then maxsev:=3; end if;
        p_errmsg:='Inserting record caught SQL error '||sqlerrm(sqlcode);
      end;
    end if;
  else /* P_IDENTIFIER was unexpected value */
    p_errval:=-20002;
    if maxsev<3 then maxsev:=3; end if;
    p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier
      ||' inserting record with key '||keystouse;
  end if;
  if p_errval=0 then
    runtime.logger(logged_msgid_retval,'STDLOG',101,
      'End ACCPRCSCYCSTATINSTSP With 0 return, P_RETVAL='
      ||p_retval||' Record Key '||keystouse,p_userid,
      1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt,
      l_timestampfld,PROGRAM_NAME=>'ACCPRCSCYCSTATINSTSP');
  else
    p_errmsg:='Error: '||P_ERRMSG||' in ACCPRCSCYCSTATINSTSP Record Key '||keystouse
      ||' Build: 2018-11-07 11:32:11';
    ROLLBACK TO ACCPRCSCYCSTATINSTSP;
    runtime.logger(logged_msgid_retval,'STDERR',103,
      p_errmsg,p_userid,maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,
      l_entrydt, l_timestampfld,PROGRAM_NAME=>'ACCPRCSCYCSTATINSTSP',force_log_entry=>TRUE);
  end if;
EXCEPTION
WHEN OTHERS THEN
  p_errval:=SQLCODE;
  p_errmsg:=' In outer exception handler in ACCPRCSCYCSTATINSTSP caught exception '
    ||sqlerrm(sqlcode)||' Record Key '||keystouse||' build 2018-11-07 11:32:11';
  ROLLBACK TO ACCPRCSCYCSTATINSTSP;
  runtime.logger(logged_msgid_retval,'STDERR',20,p_errmsg,p_userid,3,
  PROGRAM_NAME=>'ACCPRCSCYCSTATINSTSP',force_log_entry=>TRUE);
--
END ACCPRCSCYCSTATINSTSP;
/


GRANT EXECUTE ON STGCSA.ACCPRCSCYCSTATINSTSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.ACCPRCSCYCSTATINSTSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.ACCPRCSCYCSTATINSTSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.ACCPRCSCYCSTATINSTSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.ACCPRCSCYCSTATINSTSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.ACCPRCSCYCSTATINSTSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.ACCPRCSCYCSTATINSTSP TO STGCSADEVROLE;

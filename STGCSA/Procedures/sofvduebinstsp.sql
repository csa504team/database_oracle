DROP PROCEDURE STGCSA.SOFVDUEBINSTSP;

CREATE OR REPLACE PROCEDURE STGCSA.SOFVDUEBINSTSP(
    p_retval out number,
    p_errval out number,
    p_errmsg  out varchar2,
    p_identifier number:=0,
    p_userid varchar2
   ,p_LOANNMB CHAR:=null
   ,p_DUEFROMBORRRECRDTYP VARCHAR2:=null
   ,p_DUEFROMBORRPOSTINGDT DATE:=null
   ,p_DUEFROMBORRDOLLRAMT VARCHAR2:=null
   ,p_DUEFROMBORRRJCTDT DATE:=null
   ,p_DUEFROMBORRRJCTCD CHAR:=null
   ,p_DUEFROMBORRRECRDSTATCD VARCHAR2:=null
   ,p_DUEFROMBORRCLSDT DATE:=null
   ,p_DUEFROMBORRCMNT1 VARCHAR2:=null
   ,p_DUEFROMBORRCMNT2 VARCHAR2:=null
   ,p_DUEFROMBORRRPTDAILY CHAR:=null
   ,p_DUEFROMBORRDISBIND CHAR:=null
   ,p_DUEFROMBORRDELACH CHAR:=null
) as
 /*
  Created on: 2018-11-07 11:35:47
  Created by: GENR
  Crerated from template stdtblins_template v3.05 13 Sep 2018 on 2018-11-07 11:35:47
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  Procedure performs INSERT on STGCSA.SOFVDUEBTBLfor P_IDENTIFIER=0
*/
  rundate date;
  gn global_name.global_name%type;
  new_rec STGCSA.SOFVDUEBTBL%rowtype;
  logged_msgid_retval number;
  audretval number;
  crossover_active char(1);
  l_LOANNMB char(10):=null;
  l_TRANSID number:=null;
  l_dbmdl varchar2(10):=null;
  l_entrydt date:=null;
  l_userid  varchar2(32):=null;
  l_usernm  varchar2(199):=null;
  l_timestampfld date:=null;
  cur_col_name varchar2(100):=null;
  maxsev number:=0;
  holdstring varchar2(4000);
  keystouse varchar2(1000);
  crossover_not_active char(1);
  -- vars for checking non-defaultable columns
  onespace varchar2(10):=' ';
  ndfltmsg varchar2(300);
  v_errfnd boolean:=false;
  -- std date for column defaulting
  JAN_1_1900  constant date:=to_date('JAN-01-1900','MON-DD-YYYY');
  cur_colvalue varchar2(4095);
  cur_colname varchar2(30);
-- Main body begins here
begin
  -- setup
  savepoint SOFVDUEBINSTSP;
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  runtime.logger(logged_msgid_retval,'STDLOG',100,'Init SOFVDUEBINSTSP',p_userid,4,
    logtxt1=>'SOFVDUEBINSTSP',logtxt2=>'stdtblins_template v3.05 13 Sep 2018',
    PROGRAM_NAME=>'SOFVDUEBINSTSP');
  select global_name, sysdate into gn, rundate from global_name;
  --
  -- Check to see if crossover is ongoing, in which case updates are prohibited
  SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
  IF crossover_not_active != 'Y' THEN
    p_errval := -20099;
    p_errmsg := 'Warning: Cannot update table SOFVDUEBTBL at this time because crossover is running, please try later.';
  END IF;
  -- end of crossover_lockout_check
  begin
    cur_colname:='LOANNMB';
    cur_colvalue:=P_LOANNMB;
    l_LOANNMB:=P_LOANNMB;
  exception when others then
    p_errval:=sqlcode;
    p_errmsg:='Error '||sqlerrm(sqlcode)||' using '||cur_colname||' Value >'||cur_colvalue||'<';
  end;
  new_rec.creatuserid:=p_userid;
  new_rec.creatdt:=sysdate;
  new_rec.lastupdtuserid:=p_userid;
  new_rec.lastupdtdt:=sysdate;
  if p_identifier=0 then
    -- p_identifier=0 this is default for a stored procedure, only action for insert
    runtime.errfound:=false;
    runtime.errstr:=' ';
    --
    -- Default columns as applicable
  if p_errval=0 then
    begin
      cur_colname:='P_LOANNMB';
      cur_colvalue:=P_LOANNMB;
      if P_LOANNMB is null then
        raise_application_error(-20001,cur_colname||' may not be null');
      else
        new_rec.LOANNMB:=P_LOANNMB;
      end if;
      cur_colname:='P_DUEFROMBORRRECRDTYP';
      cur_colvalue:=P_DUEFROMBORRRECRDTYP;
      if P_DUEFROMBORRRECRDTYP is null then

        new_rec.DUEFROMBORRRECRDTYP:=0;

      else
        new_rec.DUEFROMBORRRECRDTYP:=P_DUEFROMBORRRECRDTYP;
      end if;
      cur_colname:='P_DUEFROMBORRPOSTINGDT';
      cur_colvalue:=P_DUEFROMBORRPOSTINGDT;
      if P_DUEFROMBORRPOSTINGDT is null then

        new_rec.DUEFROMBORRPOSTINGDT:=jan_1_1900;

      else
        new_rec.DUEFROMBORRPOSTINGDT:=P_DUEFROMBORRPOSTINGDT;
      end if;
      cur_colname:='P_DUEFROMBORRDOLLRAMT';
      cur_colvalue:=P_DUEFROMBORRDOLLRAMT;
      if P_DUEFROMBORRDOLLRAMT is null then

        new_rec.DUEFROMBORRDOLLRAMT:=0;

      else
        new_rec.DUEFROMBORRDOLLRAMT:=P_DUEFROMBORRDOLLRAMT;
      end if;
      cur_colname:='P_DUEFROMBORRRJCTDT';
      cur_colvalue:=P_DUEFROMBORRRJCTDT;
      if P_DUEFROMBORRRJCTDT is null then

        new_rec.DUEFROMBORRRJCTDT:=jan_1_1900;

      else
        new_rec.DUEFROMBORRRJCTDT:=P_DUEFROMBORRRJCTDT;
      end if;
      cur_colname:='P_DUEFROMBORRRJCTCD';
      cur_colvalue:=P_DUEFROMBORRRJCTCD;
      if P_DUEFROMBORRRJCTCD is null then

        new_rec.DUEFROMBORRRJCTCD:=rpad(' ',3);

      else
        new_rec.DUEFROMBORRRJCTCD:=P_DUEFROMBORRRJCTCD;
      end if;
      cur_colname:='P_DUEFROMBORRRECRDSTATCD';
      cur_colvalue:=P_DUEFROMBORRRECRDSTATCD;
      if P_DUEFROMBORRRECRDSTATCD is null then

        new_rec.DUEFROMBORRRECRDSTATCD:=0;

      else
        new_rec.DUEFROMBORRRECRDSTATCD:=P_DUEFROMBORRRECRDSTATCD;
      end if;
      cur_colname:='P_DUEFROMBORRCLSDT';
      cur_colvalue:=P_DUEFROMBORRCLSDT;
      if P_DUEFROMBORRCLSDT is null then

        new_rec.DUEFROMBORRCLSDT:=jan_1_1900;

      else
        new_rec.DUEFROMBORRCLSDT:=P_DUEFROMBORRCLSDT;
      end if;
      cur_colname:='P_DUEFROMBORRCMNT1';
      cur_colvalue:=P_DUEFROMBORRCMNT1;
      if P_DUEFROMBORRCMNT1 is null then

        new_rec.DUEFROMBORRCMNT1:=' ';

      else
        new_rec.DUEFROMBORRCMNT1:=P_DUEFROMBORRCMNT1;
      end if;
      cur_colname:='P_DUEFROMBORRCMNT2';
      cur_colvalue:=P_DUEFROMBORRCMNT2;
      if P_DUEFROMBORRCMNT2 is null then

        new_rec.DUEFROMBORRCMNT2:=' ';

      else
        new_rec.DUEFROMBORRCMNT2:=P_DUEFROMBORRCMNT2;
      end if;
      cur_colname:='P_DUEFROMBORRRPTDAILY';
      cur_colvalue:=P_DUEFROMBORRRPTDAILY;
      if P_DUEFROMBORRRPTDAILY is null then

        new_rec.DUEFROMBORRRPTDAILY:=rpad(' ',1);

      else
        new_rec.DUEFROMBORRRPTDAILY:=P_DUEFROMBORRRPTDAILY;
      end if;
      cur_colname:='P_DUEFROMBORRDISBIND';
      cur_colvalue:=P_DUEFROMBORRDISBIND;
      if P_DUEFROMBORRDISBIND is null then

        new_rec.DUEFROMBORRDISBIND:=rpad(' ',1);

      else
        new_rec.DUEFROMBORRDISBIND:=P_DUEFROMBORRDISBIND;
      end if;
      cur_colname:='P_DUEFROMBORRDELACH';
      cur_colvalue:=P_DUEFROMBORRDELACH;
      if P_DUEFROMBORRDELACH is null then

        new_rec.DUEFROMBORRDELACH:=rpad(' ',1);

      else
        new_rec.DUEFROMBORRDELACH:=P_DUEFROMBORRDELACH;
      end if;
    exception when others then
       p_errval:=sqlcode;
       p_errmsg:='Error - Oracle returned '||sqlerrm(sqlcode)||' using '
           ||cur_colname||' Value >'||cur_colvalue||'<';
    end;
  end if;
    keystouse:='(LOANNMB)=('||new_rec.LOANNMB||')'||' and '||'(DUEFROMBORRRECRDTYP)=('||new_rec.DUEFROMBORRRECRDTYP||')'||' and '||'(DUEFROMBORRPOSTINGDT)=('||new_rec.DUEFROMBORRPOSTINGDT||')';
    if p_errval=0 then
      begin
        insert into STGCSA.SOFVDUEBTBL values new_rec;
        p_RETVAL := SQL%ROWCOUNT;
      exception
      when dup_val_on_index then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Insert caused dupe on unique IX: '||sqlerrm(sqlcode);
      when others then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<3 then maxsev:=3; end if;
        p_errmsg:='Inserting record caught SQL error '||sqlerrm(sqlcode);
      end;
    end if;
  else /* P_IDENTIFIER was unexpected value */
    p_errval:=-20002;
    if maxsev<3 then maxsev:=3; end if;
    p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier
      ||' inserting record with key '||keystouse;
  end if;
  if p_errval=0 then
    runtime.logger(logged_msgid_retval,'STDLOG',101,
      'End SOFVDUEBINSTSP With 0 return, P_RETVAL='
      ||p_retval||' Record Key '||keystouse,p_userid,
      1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt,
      l_timestampfld,PROGRAM_NAME=>'SOFVDUEBINSTSP');
  else
    p_errmsg:='Error: '||P_ERRMSG||' in SOFVDUEBINSTSP Record Key '||keystouse
      ||' Build: 2018-11-07 11:35:47';
    ROLLBACK TO SOFVDUEBINSTSP;
    runtime.logger(logged_msgid_retval,'STDERR',103,
      p_errmsg,p_userid,maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,
      l_entrydt, l_timestampfld,PROGRAM_NAME=>'SOFVDUEBINSTSP',force_log_entry=>TRUE);
  end if;
EXCEPTION
WHEN OTHERS THEN
  p_errval:=SQLCODE;
  p_errmsg:=' In outer exception handler in SOFVDUEBINSTSP caught exception '
    ||sqlerrm(sqlcode)||' Record Key '||keystouse||' build 2018-11-07 11:35:47';
  ROLLBACK TO SOFVDUEBINSTSP;
  runtime.logger(logged_msgid_retval,'STDERR',20,p_errmsg,p_userid,3,
  PROGRAM_NAME=>'SOFVDUEBINSTSP',force_log_entry=>TRUE);
--
END SOFVDUEBINSTSP;
/


GRANT EXECUTE ON STGCSA.SOFVDUEBINSTSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.SOFVDUEBINSTSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.SOFVDUEBINSTSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.SOFVDUEBINSTSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.SOFVDUEBINSTSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.SOFVDUEBINSTSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.SOFVDUEBINSTSP TO STGCSADEVROLE;

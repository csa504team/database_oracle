DROP PROCEDURE STGCSA.GENEMAILTEMPLATEINSTSP;

CREATE OR REPLACE PROCEDURE STGCSA.GENEMAILTEMPLATEINSTSP(
    p_retval out number,
    p_errval out number,
    p_errmsg  out varchar2,
    p_identifier number:=0,
    p_userid varchar2
   ,p_EMAILTEMPLATEID VARCHAR2:=null
   ,p_EMAILCATID VARCHAR2:=null
   ,p_DBMDL VARCHAR2:=null
   ,p_TEMPLATENM VARCHAR2:=null
   ,p_DISTROLISTID VARCHAR2:=null
   ,p_SUBJLINE VARCHAR2:=null
   ,p_TEMPLATEHTML VARCHAR2:=null
   ,p_TIMESTAMPFLD DATE:=null
) as
 /*
  Created on: 2018-11-07 11:34:07
  Created by: GENR
  Crerated from template stdtblins_template v3.05 13 Sep 2018 on 2018-11-07 11:34:07
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  Procedure performs INSERT on STGCSA.GENEMAILTEMPLATETBLfor P_IDENTIFIER=0
*/
  rundate date;
  gn global_name.global_name%type;
  new_rec STGCSA.GENEMAILTEMPLATETBL%rowtype;
  logged_msgid_retval number;
  audretval number;
  crossover_active char(1);
  l_LOANNMB char(10):=null;
  l_TRANSID number:=null;
  l_dbmdl varchar2(10):=null;
  l_entrydt date:=null;
  l_userid  varchar2(32):=null;
  l_usernm  varchar2(199):=null;
  l_timestampfld date:=null;
  cur_col_name varchar2(100):=null;
  maxsev number:=0;
  holdstring varchar2(4000);
  keystouse varchar2(1000);
  -- std date for column defaulting
  JAN_1_1900  constant date:=to_date('JAN-01-1900','MON-DD-YYYY');
  cur_colvalue varchar2(4095);
  cur_colname varchar2(30);
-- Function used to initialize EMAILTEMPLATEID from sequence EMAILTEMPLATEIDSEQ
Function NEXTVAL_EMAILTEMPLATEIDSEQ return number is
  N number;
begin
  select EMAILTEMPLATEIDSEQ.nextval into n from dual;
  return n;
end;
-- Main body begins here
begin
  -- setup
  savepoint GENEMAILTEMPLATEINSTSP;
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  runtime.logger(logged_msgid_retval,'STDLOG',100,'Init GENEMAILTEMPLATEINSTSP',p_userid,4,
    logtxt1=>'GENEMAILTEMPLATEINSTSP',logtxt2=>'stdtblins_template v3.05 13 Sep 2018',
    PROGRAM_NAME=>'GENEMAILTEMPLATEINSTSP');
  select global_name, sysdate into gn, rundate from global_name;
  begin
    cur_colname:='DBMDL';
    cur_colvalue:=P_DBMDL;
    l_DBMDL:=P_DBMDL;    cur_colname:='TIMESTAMPFLD';
    cur_colvalue:=P_TIMESTAMPFLD;
    l_TIMESTAMPFLD:=P_TIMESTAMPFLD;
  exception when others then
    p_errval:=sqlcode;
    p_errmsg:='Error '||sqlerrm(sqlcode)||' using '||cur_colname||' Value >'||cur_colvalue||'<';
  end;
  new_rec.creatuserid:=p_userid;
  new_rec.creatdt:=sysdate;
  new_rec.lastupdtuserid:=p_userid;
  new_rec.lastupdtdt:=sysdate;
  if p_identifier=0 then
    -- p_identifier=0 this is default for a stored procedure, only action for insert
    runtime.errfound:=false;
    runtime.errstr:=' ';
    --
    -- Default columns as applicable
  if p_errval=0 then
    begin
      cur_colname:='P_EMAILTEMPLATEID';
      cur_colvalue:=P_EMAILTEMPLATEID;
      if P_EMAILTEMPLATEID is null then

        new_rec.EMAILTEMPLATEID:=NEXTVAL_EMAILTEMPLATEIDSEQ();

      else
        new_rec.EMAILTEMPLATEID:=P_EMAILTEMPLATEID;
      end if;
      cur_colname:='P_EMAILCATID';
      cur_colvalue:=P_EMAILCATID;
      if P_EMAILCATID is null then

        new_rec.EMAILCATID:=0;

      else
        new_rec.EMAILCATID:=P_EMAILCATID;
      end if;
      cur_colname:='P_DBMDL';
      cur_colvalue:=P_DBMDL;
      if P_DBMDL is null then

        new_rec.DBMDL:=' ';

      else
        new_rec.DBMDL:=P_DBMDL;
      end if;
      cur_colname:='P_TEMPLATENM';
      cur_colvalue:=P_TEMPLATENM;
      if P_TEMPLATENM is null then

        new_rec.TEMPLATENM:=' ';

      else
        new_rec.TEMPLATENM:=P_TEMPLATENM;
      end if;
      cur_colname:='P_DISTROLISTID';
      cur_colvalue:=P_DISTROLISTID;
      if P_DISTROLISTID is null then

        new_rec.DISTROLISTID:=0;

      else
        new_rec.DISTROLISTID:=P_DISTROLISTID;
      end if;
      cur_colname:='P_SUBJLINE';
      cur_colvalue:=P_SUBJLINE;
      if P_SUBJLINE is null then

        new_rec.SUBJLINE:=' ';

      else
        new_rec.SUBJLINE:=P_SUBJLINE;
      end if;
      cur_colname:='P_TEMPLATEHTML';
      cur_colvalue:=P_TEMPLATEHTML;
      if P_TEMPLATEHTML is null then

        new_rec.TEMPLATEHTML:=' ';

      else
        new_rec.TEMPLATEHTML:=P_TEMPLATEHTML;
      end if;
      cur_colname:='P_TIMESTAMPFLD';
      cur_colvalue:=P_TIMESTAMPFLD;
      if P_TIMESTAMPFLD is null then

        new_rec.TIMESTAMPFLD:=jan_1_1900;

      else
        new_rec.TIMESTAMPFLD:=P_TIMESTAMPFLD;
      end if;
    exception when others then
       p_errval:=sqlcode;
       p_errmsg:='Error - Oracle returned '||sqlerrm(sqlcode)||' using '
           ||cur_colname||' Value >'||cur_colvalue||'<';
    end;
  end if;
    keystouse:='(EMAILTEMPLATEID)=('||new_rec.EMAILTEMPLATEID||')';
    if p_errval=0 then
      begin
        insert into STGCSA.GENEMAILTEMPLATETBL values new_rec;
        p_RETVAL := SQL%ROWCOUNT;
      exception
      when dup_val_on_index then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Insert caused dupe on unique IX: '||sqlerrm(sqlcode);
      when others then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<3 then maxsev:=3; end if;
        p_errmsg:='Inserting record caught SQL error '||sqlerrm(sqlcode);
      end;
    end if;
  else /* P_IDENTIFIER was unexpected value */
    p_errval:=-20002;
    if maxsev<3 then maxsev:=3; end if;
    p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier
      ||' inserting record with key '||keystouse;
  end if;
  if p_errval=0 then
    runtime.logger(logged_msgid_retval,'STDLOG',101,
      'End GENEMAILTEMPLATEINSTSP With 0 return, P_RETVAL='
      ||p_retval||' Record Key '||keystouse,p_userid,
      1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt,
      l_timestampfld,PROGRAM_NAME=>'GENEMAILTEMPLATEINSTSP');
  else
    p_errmsg:='Error: '||P_ERRMSG||' in GENEMAILTEMPLATEINSTSP Record Key '||keystouse
      ||' Build: 2018-11-07 11:34:07';
    ROLLBACK TO GENEMAILTEMPLATEINSTSP;
    runtime.logger(logged_msgid_retval,'STDERR',103,
      p_errmsg,p_userid,maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,
      l_entrydt, l_timestampfld,PROGRAM_NAME=>'GENEMAILTEMPLATEINSTSP',force_log_entry=>TRUE);
  end if;
EXCEPTION
WHEN OTHERS THEN
  p_errval:=SQLCODE;
  p_errmsg:=' In outer exception handler in GENEMAILTEMPLATEINSTSP caught exception '
    ||sqlerrm(sqlcode)||' Record Key '||keystouse||' build 2018-11-07 11:34:07';
  ROLLBACK TO GENEMAILTEMPLATEINSTSP;
  runtime.logger(logged_msgid_retval,'STDERR',20,p_errmsg,p_userid,3,
  PROGRAM_NAME=>'GENEMAILTEMPLATEINSTSP',force_log_entry=>TRUE);
--
END GENEMAILTEMPLATEINSTSP;
/


GRANT EXECUTE ON STGCSA.GENEMAILTEMPLATEINSTSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.GENEMAILTEMPLATEINSTSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.GENEMAILTEMPLATEINSTSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.GENEMAILTEMPLATEINSTSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.GENEMAILTEMPLATEINSTSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.GENEMAILTEMPLATEINSTSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.GENEMAILTEMPLATEINSTSP TO STGCSADEVROLE;

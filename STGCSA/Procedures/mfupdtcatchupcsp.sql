DROP PROCEDURE STGCSA.MFUPDTCATCHUPCSP;

CREATE OR REPLACE PROCEDURE STGCSA.MFUPDTCATCHUPCSP (
    p_taskname VARCHAR2 := '', /* CATCHUP */
    p_runmode CHAR := 'C', /* C: current month, N: next month */
    p_purpose VARCHAR2 := 'MFUPDATE', /* MFUPDATE or MFLIST */
    P_CREATUSERID VARCHAR2 :=NULL,
    p_retval out number,
    p_errval out number,
    p_errmsg out varchar2)
as
    ---p1 NUMBER; p2 NUMBER; p3 VARCHAR2(200);
    v_sysdate date := sysdate;
	v_nulldate date := TO_DATE('1900-01-01', 'YYYY-MM-DD');
    v_status char(1);
    v_curtask varchar(200);
    v_lastupdtdt date;
    v_taskisopen char(1) := 'N';
	v_indexdate date := TRUNC(sysdate, 'mm');
    v_sysmsg varchar(2000);
    v_rowcount number;
    v_monthmode varchar(20) := 'Current Month';
BEGIN
    p_errval := 0;
    p_retval := -1000;
    p_errmsg := 'Success';

    SELECT stgcsa.isProcessAllowed( p_taskname ) into v_status from dual;

    IF v_status != 'Y' THEN
        SELECT JOBTYPEDESC INTO v_curtask FROM (
            SELECT REF.JOBTYPEDESC FROM MFJOBDETAILTBL DTL JOIN REFJOBTYPETBL REF ON REF.JOBTYPEID = DTL.JOBTYPEFK
            --WHERE DTL.TaskEndDt IS NULL
            ORDER BY DTL.JOBDETAILID DESC FETCH FIRST 1 ROWS ONLY
        );        
        p_errval := -22000;
        p_errmsg := 'Warning: Cannot perform MF Update for [' || Upper(p_taskname) || '] at this time because a [' || v_curtask || '] task is currently running.';
        DBMS_OUTPUT.PUT_LINE('P_RETVAL = ' || p_retval || ' / P_ERRVAL = ' || p_errval || ' / P_ERRMSG = ' || p_errmsg);
        RETURN;
    END IF;

    --SELECT NVL(MAX(taskenddt), to_date('2018-01-01', 'yyyy-mm-dd')) into v_lastupdtdt from mfjobdetailtbl where jobtypefk = 40;
	v_lastupdtdt := v_indexdate; -- always use the beginning of this month

    taskstartcsp(p_taskname, p_creatuserid, p_retval => p_retval, p_errval => p_errval, p_errmsg => p_errmsg );
    v_taskisopen := 'Y';
    DBMS_OUTPUT.PUT_LINE('P_RETVAL = ' || p_retval || ' / P_ERRVAL = ' || p_errval || ' / P_ERRMSG = ' || p_errmsg);

	/* Step 1: Finding those Plans status of Pending Removal (43) or Cancelled (20). Remove those from the all SOFV table */

	EXECUTE IMMEDIATE 'TRUNCATE TABLE TempCUDPlanTBL';

	INSERT INTO TEMPCUDPLANTBL 
	SELECT DISTINCT LoanNmb, PlanTransID, PlanStatus, PlanStatID, PlanLnSts, PlanUpdtDt 
	FROM defctchvw WHERE planUpdtdt > v_lastupdtdt AND PlanStatID IN (43, 20) AND PeriodStatusID = 1;

	EXECUTE IMMEDIATE 'TRUNCATE TABLE TempCUDTbl';

	IF p_runmode = 'N' THEN
		v_indexdate := trunc(Add_Months(sysdate, 1), 'mm');
        v_monthmode := 'Next Month';
	END IF;

    /* now use this common routine shared with MFListCatchUpCSP and MFUpdtCatchUpCSP to get uniform result */
    MFCOMMONCATCHUPCSP(p_runmode, 'MFUPDATE');

    /* move those out of the way first */
	DELETE FROM SOFVBGDFTBL FV WHERE EXISTS (SELECT 1 FROM TempCUDTbl tmp WHERE fv.DEFRMNTGPKEY = tmp.LOANNMB AND tmp.PymtAmt IS NULL AND tmp.CUDPDTYPID IN (1, 2));   --- remove the def
    v_rowcount := sql%rowcount;
    IF v_rowcount > 0 THEN
        v_sysmsg := v_sysmsg || '<li>' || to_char(v_rowcount) || ' records were removed from BGDF</li>';
    END IF;

	DELETE FROM SOFVCTCHTBL FV WHERE EXISTS (SELECT 1 FROM TempCUDTbl tmp WHERE fv.CATCHUPPLANSBANMB = tmp.LOANNMB AND tmp.PymtAmt IS NULL AND tmp.CUDPDTYPID = 3);    --- remove the ctch
    v_rowcount := sql%rowcount;
    IF v_rowcount > 0 THEN
        v_sysmsg := v_sysmsg || '<li>' || to_char(v_rowcount) || ' records were removed from CTCH</li>';
    END IF;

	DELETE FROM SOFVDFPYTBL FV1 WHERE EXISTS (SELECT 1 FROM TempCUDTbl tmp WHERE fv1.LOANNMB = tmp.LOANNMB AND tmp.PymtAmt IS NULL);                                     --- remove the pymt
    v_rowcount := sql%rowcount;
    IF v_rowcount > 0 THEN
        v_sysmsg := v_sysmsg || '<li>' || to_char(v_rowcount) || ' records were removed from DFPY</li>';
    END IF;

    /* only set LND1 to '01' for Pending Removal and Cancelled loan numbers for now */
	UPDATE SOFVLND1TBL FV2 SET LOANDTLSTATCDOFLOAN = '01', LOANDTLSTATDT = v_sysdate, LASTUPDTDT = v_sysdate, LASTUPDTUSERID = 'MFUPDATE'
    WHERE EXISTS (SELECT 1 FROM TempCUDTbl tmp WHERE fv2.LOANNMB = tmp.LOANNMB AND tmp.PlanStatID IN (43, 20));                                                                                    --- clear out LND1 status (it will be Active)

    v_rowcount := sql%rowcount;
    IF v_rowcount > 0 THEN
        v_sysmsg := v_sysmsg || '<li>' || to_char(v_rowcount) || ' Pending Removal / Cancelled loans were updated to status 01 in LND1</li>';
    END IF;

	/* insert data into the catch up table. CTCH only add data when it is approved status, for both current and next month macro */
    MERGE INTO SOFVCTCHTBL XX USING (
        WITH pivot_data AS (
            select VW.LoanNMB, 
            TRUNC(VW.StartDT, 'MM') as StartDT, ADD_MONTHS(TRUNC(VW.EndDt,'MM'), 1) - 1 as EndDt, VW.PymtAmt, 
            row_number() over (PARTITION BY VW.LoanNMB ORDER BY VW.LoanNMB, StartDt) rowx  
            from TempCUDTbl VW 
            WHERE VW.CUDPDTYPID = 3 AND VW.UpdateInd = 'Y' AND VW.PlanStatID = 35
        )
        SELECT * 
        FROM pivot_data
        PIVOT (
            MAX(STARTDT) AS Start_DT,
            MAX(ENDDT) AS End_DT,
            MAX(PYMTAMT) AS AMT
            FOR ROWX IN (1 as p1, 2 as p2, 3 as p3, 4 as p4, 5 as p5, 6 as p6, 7 as p7, 8 as p8, 9 as p9, 10 as p10, 11 as p11, 12 as p12, 13 as p13, 14 as p14, 15 as p15)
        )
    ) PV ON (XX.CATCHUPPLANSBANMB = PV.LOANNMB)
    WHEN MATCHED THEN UPDATE SET 
        CATCHUPPLANSTRTDT1 = DECODE(CATCHUPPLANSTRTDT1, v_nulldate, NVL(p1_Start_DT, v_nulldate), CATCHUPPLANSTRTDT1), 
        CATCHUPPLANENDDT1  = DECODE(CATCHUPPLANENDDT1,  v_nulldate, NVL(p1_End_DT,   v_nulldate), CATCHUPPLANENDDT1), 
        CATCHUPPLANRQ1AMT  = DECODE(CATCHUPPLANRQ1AMT,  0,          NVL(p1_AMT,      0),          CATCHUPPLANRQ1AMT),
        CATCHUPPLANSTRTDT2 = DECODE(CATCHUPPLANSTRTDT2, v_nulldate, NVL(p2_Start_DT, v_nulldate), CATCHUPPLANSTRTDT2), 
        CATCHUPPLANENDDT2  = DECODE(CATCHUPPLANENDDT2,  v_nulldate, NVL(p2_End_DT,   v_nulldate), CATCHUPPLANENDDT2), 
        CATCHUPPLANRQ2AMT  = DECODE(CATCHUPPLANRQ2AMT,  0,          NVL(p2_AMT,      0),          CATCHUPPLANRQ2AMT),
        CATCHUPPLANSTRTDT3 = DECODE(CATCHUPPLANSTRTDT3, v_nulldate, NVL(p3_Start_DT, v_nulldate), CATCHUPPLANSTRTDT3), 
        CATCHUPPLANENDDT3  = DECODE(CATCHUPPLANENDDT3,  v_nulldate, NVL(p3_End_DT,   v_nulldate), CATCHUPPLANENDDT3), 
        CATCHUPPLANRQ3AMT  = DECODE(CATCHUPPLANRQ3AMT,  0,          NVL(p3_AMT,      0),          CATCHUPPLANRQ3AMT),
        CATCHUPPLANSTRTDT4 = DECODE(CATCHUPPLANSTRTDT4, v_nulldate, NVL(p4_Start_DT, v_nulldate), CATCHUPPLANSTRTDT4), 
        CATCHUPPLANENDDT4  = DECODE(CATCHUPPLANENDDT4,  v_nulldate, NVL(p4_End_DT,   v_nulldate), CATCHUPPLANENDDT4), 
        CATCHUPPLANRQ4AMT  = DECODE(CATCHUPPLANRQ4AMT,  0,          NVL(p4_AMT,      0),          CATCHUPPLANRQ4AMT),
        CATCHUPPLANSTRTDT5 = DECODE(CATCHUPPLANSTRTDT5, v_nulldate, NVL(p5_Start_DT, v_nulldate), CATCHUPPLANSTRTDT5), 
        CATCHUPPLANENDDT5  = DECODE(CATCHUPPLANENDDT5,  v_nulldate, NVL(p5_End_DT,   v_nulldate), CATCHUPPLANENDDT5), 
        CATCHUPPLANRQ5AMT  = DECODE(CATCHUPPLANRQ5AMT,  0,          NVL(p5_AMT,      0),          CATCHUPPLANRQ5AMT),
        CATCHUPPLANSTRTDT6 = DECODE(CATCHUPPLANSTRTDT6, v_nulldate, NVL(p6_Start_DT, v_nulldate), CATCHUPPLANSTRTDT6), 
        CATCHUPPLANENDDT6  = DECODE(CATCHUPPLANENDDT6,  v_nulldate, NVL(p6_End_DT,   v_nulldate), CATCHUPPLANENDDT6), 
        CATCHUPPLANRQ6AMT  = DECODE(CATCHUPPLANRQ6AMT,  0,          NVL(p6_AMT,      0),          CATCHUPPLANRQ6AMT),
        CATCHUPPLANSTRTDT7 = DECODE(CATCHUPPLANSTRTDT7, v_nulldate, NVL(p7_Start_DT, v_nulldate), CATCHUPPLANSTRTDT7), 
        CATCHUPPLANENDDT7  = DECODE(CATCHUPPLANENDDT7,  v_nulldate, NVL(p7_End_DT,   v_nulldate), CATCHUPPLANENDDT7), 
        CATCHUPPLANRQ7AMT  = DECODE(CATCHUPPLANRQ7AMT,  0,          NVL(p7_AMT,      0),          CATCHUPPLANRQ7AMT),
        CATCHUPPLANSTRTDT8 = DECODE(CATCHUPPLANSTRTDT8, v_nulldate, NVL(p8_Start_DT, v_nulldate), CATCHUPPLANSTRTDT8), 
        CATCHUPPLANENDDT8  = DECODE(CATCHUPPLANENDDT8,  v_nulldate, NVL(p8_End_DT,   v_nulldate), CATCHUPPLANENDDT8), 
        CATCHUPPLANRQ8AMT  = DECODE(CATCHUPPLANRQ8AMT,  0,          NVL(p8_AMT,      0),          CATCHUPPLANRQ8AMT),
        CATCHUPPLANSTRTDT9 = DECODE(CATCHUPPLANSTRTDT9, v_nulldate, NVL(p9_Start_DT, v_nulldate), CATCHUPPLANSTRTDT9), 
        CATCHUPPLANENDDT9  = DECODE(CATCHUPPLANENDDT9,  v_nulldate, NVL(p9_End_DT,   v_nulldate), CATCHUPPLANENDDT9), 
        CATCHUPPLANRQ9AMT  = DECODE(CATCHUPPLANRQ9AMT,  0,          NVL(p9_AMT,      0),          CATCHUPPLANRQ9AMT),
        CATCHUPPLANSTRTDT10 = DECODE(CATCHUPPLANSTRTDT10, v_nulldate, NVL(p10_Start_DT, v_nulldate), CATCHUPPLANSTRTDT10), 
        CATCHUPPLANENDDT10  = DECODE(CATCHUPPLANENDDT10,  v_nulldate, NVL(p10_End_DT,   v_nulldate), CATCHUPPLANENDDT10), 
        CATCHUPPLANRQ10AMT  = DECODE(CATCHUPPLANRQ10AMT,  0,          NVL(p10_AMT,      0),          CATCHUPPLANRQ10AMT),
        CATCHUPPLANSTRTDT11 = DECODE(CATCHUPPLANSTRTDT11, v_nulldate, NVL(p11_Start_DT, v_nulldate), CATCHUPPLANSTRTDT11), 
        CATCHUPPLANENDDT11  = DECODE(CATCHUPPLANENDDT11,  v_nulldate, NVL(p11_End_DT,   v_nulldate), CATCHUPPLANENDDT11), 
        CATCHUPPLANRQ11AMT  = DECODE(CATCHUPPLANRQ11AMT,  0,          NVL(p11_AMT,      0),          CATCHUPPLANRQ11AMT),
        CATCHUPPLANSTRTDT12 = DECODE(CATCHUPPLANSTRTDT12, v_nulldate, NVL(p12_Start_DT, v_nulldate), CATCHUPPLANSTRTDT12), 
        CATCHUPPLANENDDT12  = DECODE(CATCHUPPLANENDDT12,  v_nulldate, NVL(p12_End_DT,   v_nulldate), CATCHUPPLANENDDT12), 
        CATCHUPPLANRQ12AMT  = DECODE(CATCHUPPLANRQ12AMT,  0,          NVL(p12_AMT,      0),          CATCHUPPLANRQ12AMT),
        CATCHUPPLANSTRTDT13 = DECODE(CATCHUPPLANSTRTDT13, v_nulldate, NVL(p13_Start_DT, v_nulldate), CATCHUPPLANSTRTDT13), 
        CATCHUPPLANENDDT13  = DECODE(CATCHUPPLANENDDT13,  v_nulldate, NVL(p13_End_DT,   v_nulldate), CATCHUPPLANENDDT13), 
        CATCHUPPLANRQ13AMT  = DECODE(CATCHUPPLANRQ13AMT,  0,          NVL(p13_AMT,      0),          CATCHUPPLANRQ13AMT),
        CATCHUPPLANSTRTDT14 = DECODE(CATCHUPPLANSTRTDT14, v_nulldate, NVL(p14_Start_DT, v_nulldate), CATCHUPPLANSTRTDT14), 
        CATCHUPPLANENDDT14  = DECODE(CATCHUPPLANENDDT14,  v_nulldate, NVL(p14_End_DT,   v_nulldate), CATCHUPPLANENDDT14), 
        CATCHUPPLANRQ14AMT  = DECODE(CATCHUPPLANRQ14AMT,  0,          NVL(p14_AMT,      0),          CATCHUPPLANRQ14AMT),
        CATCHUPPLANSTRTDT15 = DECODE(CATCHUPPLANSTRTDT15, v_nulldate, NVL(p15_Start_DT, v_nulldate), CATCHUPPLANSTRTDT15), 
        CATCHUPPLANENDDT15  = DECODE(CATCHUPPLANENDDT15,  v_nulldate, NVL(p15_End_DT,   v_nulldate), CATCHUPPLANENDDT15), 
        CATCHUPPLANRQ15AMT  = DECODE(CATCHUPPLANRQ15AMT,  0,          NVL(p15_AMT,      0),          CATCHUPPLANRQ15AMT),
        CATCHUPPLANMODDT = v_sysdate, LASTUPDTUSERID = 'MFUPDATE', LASTUPDTDT = v_sysdate
    WHEN NOT MATCHED THEN 
        INSERT (CATCHUPPLANSBANMB, 
            CATCHUPPLANSTRTDT1, CATCHUPPLANENDDT1, CATCHUPPLANRQ1AMT,
            CATCHUPPLANSTRTDT2, CATCHUPPLANENDDT2, CATCHUPPLANRQ2AMT,
            CATCHUPPLANSTRTDT3, CATCHUPPLANENDDT3, CATCHUPPLANRQ3AMT,
            CATCHUPPLANSTRTDT4, CATCHUPPLANENDDT4, CATCHUPPLANRQ4AMT,
            CATCHUPPLANSTRTDT5, CATCHUPPLANENDDT5, CATCHUPPLANRQ5AMT,
            CATCHUPPLANSTRTDT6, CATCHUPPLANENDDT6, CATCHUPPLANRQ6AMT,
            CATCHUPPLANSTRTDT7, CATCHUPPLANENDDT7, CATCHUPPLANRQ7AMT,
            CATCHUPPLANSTRTDT8, CATCHUPPLANENDDT8, CATCHUPPLANRQ8AMT,
            CATCHUPPLANSTRTDT9, CATCHUPPLANENDDT9, CATCHUPPLANRQ9AMT,
            CATCHUPPLANSTRTDT10, CATCHUPPLANENDDT10, CATCHUPPLANRQ10AMT,
            CATCHUPPLANSTRTDT11, CATCHUPPLANENDDT11, CATCHUPPLANRQ11AMT,
            CATCHUPPLANSTRTDT12, CATCHUPPLANENDDT12, CATCHUPPLANRQ12AMT,
            CATCHUPPLANSTRTDT13, CATCHUPPLANENDDT13, CATCHUPPLANRQ13AMT,
            CATCHUPPLANSTRTDT14, CATCHUPPLANENDDT14, CATCHUPPLANRQ14AMT,
            CATCHUPPLANSTRTDT15, CATCHUPPLANENDDT15, CATCHUPPLANRQ15AMT,
            CATCHUPPLANCREDT, CATCHUPPLANMODDT, CREATUSERID, CREATDT, LASTUPDTUSERID, LASTUPDTDT)
        VALUES (LoanNMB, 
            NVL(p1_Start_DT, v_nulldate), NVL(p1_End_DT, v_nulldate), NVL(p1_AMT, 0),
            NVL(p2_Start_DT, v_nulldate), NVL(p2_End_DT, v_nulldate), NVL(p2_AMT, 0),
            NVL(p3_Start_DT, v_nulldate), NVL(p3_End_DT, v_nulldate), NVL(p3_AMT, 0),
            NVL(p4_Start_DT, v_nulldate), NVL(p4_End_DT, v_nulldate), NVL(p4_AMT, 0),
            NVL(p5_Start_DT, v_nulldate), NVL(p5_End_DT, v_nulldate), NVL(p5_AMT, 0),
            NVL(p6_Start_DT, v_nulldate), NVL(p6_End_DT, v_nulldate), NVL(p6_AMT, 0),
            NVL(p7_Start_DT, v_nulldate), NVL(p7_End_DT, v_nulldate), NVL(p7_AMT, 0),
            NVL(p8_Start_DT, v_nulldate), NVL(p8_End_DT, v_nulldate), NVL(p8_AMT, 0),
            NVL(p9_Start_DT, v_nulldate), NVL(p9_End_DT, v_nulldate), NVL(p9_AMT, 0),
            NVL(p10_Start_DT, v_nulldate), NVL(p10_End_DT, v_nulldate), NVL(p10_AMT, 0),
            NVL(p11_Start_DT, v_nulldate), NVL(p11_End_DT, v_nulldate), NVL(p11_AMT, 0),
            NVL(p12_Start_DT, v_nulldate), NVL(p12_End_DT, v_nulldate), NVL(p12_AMT, 0),
            NVL(p13_Start_DT, v_nulldate), NVL(p13_End_DT, v_nulldate), NVL(p13_AMT, 0),
            NVL(p14_Start_DT, v_nulldate), NVL(p14_End_DT, v_nulldate), NVL(p14_AMT, 0),
            NVL(p15_Start_DT, v_nulldate), NVL(p15_End_DT, v_nulldate), NVL(p15_AMT, 0),
            v_sysdate, v_sysdate, 'MFUPDATE', v_sysdate, 'MFUPDATE', v_sysdate );

        v_rowcount := sql%rowcount;
        IF v_rowcount > 0 THEN
            v_sysmsg := v_sysmsg || '<li>' || to_char(v_rowcount) || ' records were inserted/updated in CTCH</li>';
        END IF;

	/* insert data into the partial deferrment table */
    MERGE INTO SOFVBGDFTBL XX USING (
        WITH pivot_data AS (
            select VW.LoanNMB, 
            TRUNC(VW.StartDT, 'MM') as StartDT, ADD_MONTHS(TRUNC(VW.EndDt,'MM'), 1) - 1 as EndDt, VW.PymtAmt, 
            row_number() over (PARTITION BY VW.LoanNMB ORDER BY VW.LoanNMB, StartDt) rowx  
            from TempCUDTbl VW 
            WHERE VW.CUDPDTYPID = 2 AND VW.UpdateInd = 'Y' AND VW.PlanStatID IN (2, 35)
        )
        SELECT * 
        FROM pivot_data
        PIVOT (
            MAX(STARTDT) AS Start_DT,
            MAX(ENDDT) AS End_DT,
            MAX(PYMTAMT) AS AMT
            FOR ROWX IN (1 as p1, 2 as p2, 3 as p3, 4 as p4, 5 as p5, 6 as p6, 7 as p7, 8 as p8, 9 as p9, 10 as p10, 11 as p11, 12 as p12, 13 as p13, 14 as p14, 15 as p15)
        )
    ) PV ON (XX.DEFRMNTGPKEY = PV.LOANNMB)
    WHEN MATCHED THEN UPDATE SET 
        DEFRMNTSTRTDT1 = DECODE(DEFRMNTSTRTDT1, v_nulldate, NVL(p1_Start_DT, v_nulldate), DEFRMNTSTRTDT1), 
        DEFRMNTENDDT1  = DECODE(DEFRMNTENDDT1,  v_nulldate, NVL(p1_End_DT,   v_nulldate), DEFRMNTENDDT1), 
        DEFRMNTREQ1AMT = DECODE(DEFRMNTREQ1AMT, 0,          NVL(p1_AMT,      0),          DEFRMNTREQ1AMT),
        DEFRMNTSTRTDT2 = DECODE(DEFRMNTSTRTDT2, v_nulldate, NVL(p2_Start_DT, v_nulldate), DEFRMNTSTRTDT2), 
        DEFRMNTENDDT2  = DECODE(DEFRMNTENDDT2,  v_nulldate, NVL(p2_End_DT,   v_nulldate), DEFRMNTENDDT2), 
        DEFRMNTREQ2AMT = DECODE(DEFRMNTREQ2AMT, 0,          NVL(p2_AMT,      0),          DEFRMNTREQ2AMT),
        DEFRMNTSTRTDT3 = DECODE(DEFRMNTSTRTDT3, v_nulldate, NVL(p3_Start_DT, v_nulldate), DEFRMNTSTRTDT3), 
        DEFRMNTENDDT3  = DECODE(DEFRMNTENDDT3,  v_nulldate, NVL(p3_End_DT,   v_nulldate), DEFRMNTENDDT3), 
        DEFRMNTREQ3AMT = DECODE(DEFRMNTREQ3AMT, 0,          NVL(p3_AMT,      0),          DEFRMNTREQ3AMT),
        DEFRMNTSTRTDT4 = DECODE(DEFRMNTSTRTDT4, v_nulldate, NVL(p4_Start_DT, v_nulldate), DEFRMNTSTRTDT4), 
        DEFRMNTENDDT4  = DECODE(DEFRMNTENDDT4,  v_nulldate, NVL(p4_End_DT,   v_nulldate), DEFRMNTENDDT4), 
        DEFRMNTREQ4AMT = DECODE(DEFRMNTREQ4AMT, 0,          NVL(p4_AMT,      0),          DEFRMNTREQ4AMT),
        DEFRMNTSTRTDT5 = DECODE(DEFRMNTSTRTDT5, v_nulldate, NVL(p5_Start_DT, v_nulldate), DEFRMNTSTRTDT5), 
        DEFRMNTENDDT5  = DECODE(DEFRMNTENDDT5,  v_nulldate, NVL(p5_End_DT,   v_nulldate), DEFRMNTENDDT5), 
        DEFRMNTREQ5AMT = DECODE(DEFRMNTREQ5AMT, 0,          NVL(p5_AMT,      0),          DEFRMNTREQ5AMT),
        DEFRMNTSTRTDT6 = DECODE(DEFRMNTSTRTDT6, v_nulldate, NVL(p6_Start_DT, v_nulldate), DEFRMNTSTRTDT6), 
        DEFRMNTENDDT6  = DECODE(DEFRMNTENDDT6,  v_nulldate, NVL(p6_End_DT,   v_nulldate), DEFRMNTENDDT6), 
        DEFRMNTREQ6AMT = DECODE(DEFRMNTREQ6AMT, 0,          NVL(p6_AMT,      0),          DEFRMNTREQ6AMT),
        DEFRMNTSTRTDT7 = DECODE(DEFRMNTSTRTDT7, v_nulldate, NVL(p7_Start_DT, v_nulldate), DEFRMNTSTRTDT7), 
        DEFRMNTENDDT7  = DECODE(DEFRMNTENDDT7,  v_nulldate, NVL(p7_End_DT,   v_nulldate), DEFRMNTENDDT7), 
        DEFRMNTREQ7AMT = DECODE(DEFRMNTREQ7AMT, 0,          NVL(p7_AMT,      0),          DEFRMNTREQ7AMT),
        DEFRMNTSTRTDT8 = DECODE(DEFRMNTSTRTDT8, v_nulldate, NVL(p8_Start_DT, v_nulldate), DEFRMNTSTRTDT8), 
        DEFRMNTENDDT8  = DECODE(DEFRMNTENDDT8,  v_nulldate, NVL(p8_End_DT,   v_nulldate), DEFRMNTENDDT8), 
        DEFRMNTREQ8AMT = DECODE(DEFRMNTREQ8AMT, 0,          NVL(p8_AMT,      0),          DEFRMNTREQ8AMT),
        DEFRMNTSTRTDT9 = DECODE(DEFRMNTSTRTDT9, v_nulldate, NVL(p9_Start_DT, v_nulldate), DEFRMNTSTRTDT9), 
        DEFRMNTENDDT9  = DECODE(DEFRMNTENDDT9,  v_nulldate, NVL(p9_End_DT,   v_nulldate), DEFRMNTENDDT9), 
        DEFRMNTREQ9AMT = DECODE(DEFRMNTREQ9AMT, 0,          NVL(p9_AMT,      0),          DEFRMNTREQ9AMT),
        DEFRMNTSTRTDT10 = DECODE(DEFRMNTSTRTDT10, v_nulldate, NVL(p10_Start_DT, v_nulldate), DEFRMNTSTRTDT10), 
        DEFRMNTENDDT10  = DECODE(DEFRMNTENDDT10,  v_nulldate, NVL(p10_End_DT,   v_nulldate), DEFRMNTENDDT10), 
        DEFRMNTREQ10AMT = DECODE(DEFRMNTREQ10AMT, 0,          NVL(p10_AMT,      0),          DEFRMNTREQ10AMT),
        DEFRMNTSTRTDT11 = DECODE(DEFRMNTSTRTDT11, v_nulldate, NVL(p11_Start_DT, v_nulldate), DEFRMNTSTRTDT11), 
        DEFRMNTENDDT11  = DECODE(DEFRMNTENDDT11,  v_nulldate, NVL(p11_End_DT,   v_nulldate), DEFRMNTENDDT11), 
        DEFRMNTREQ11AMT = DECODE(DEFRMNTREQ11AMT, 0,          NVL(p11_AMT,      0),          DEFRMNTREQ11AMT),
        DEFRMNTSTRTDT12 = DECODE(DEFRMNTSTRTDT12, v_nulldate, NVL(p12_Start_DT, v_nulldate), DEFRMNTSTRTDT12), 
        DEFRMNTENDDT12  = DECODE(DEFRMNTENDDT12,  v_nulldate, NVL(p12_End_DT,   v_nulldate), DEFRMNTENDDT12), 
        DEFRMNTREQ12AMT = DECODE(DEFRMNTREQ12AMT, 0,          NVL(p12_AMT,      0),          DEFRMNTREQ12AMT),
        DEFRMNTSTRTDT13 = DECODE(DEFRMNTSTRTDT13, v_nulldate, NVL(p13_Start_DT, v_nulldate), DEFRMNTSTRTDT13), 
        DEFRMNTENDDT13  = DECODE(DEFRMNTENDDT13,  v_nulldate, NVL(p13_End_DT,   v_nulldate), DEFRMNTENDDT13), 
        DEFRMNTREQ13AMT = DECODE(DEFRMNTREQ13AMT, 0,          NVL(p13_AMT,      0),          DEFRMNTREQ13AMT),
        DEFRMNTSTRTDT14 = DECODE(DEFRMNTSTRTDT14, v_nulldate, NVL(p14_Start_DT, v_nulldate), DEFRMNTSTRTDT14), 
        DEFRMNTENDDT14  = DECODE(DEFRMNTENDDT14,  v_nulldate, NVL(p14_End_DT,   v_nulldate), DEFRMNTENDDT14), 
        DEFRMNTREQ14AMT = DECODE(DEFRMNTREQ14AMT, 0,          NVL(p14_AMT,      0),          DEFRMNTREQ14AMT),
        DEFRMNTSTRTDT15 = DECODE(DEFRMNTSTRTDT15, v_nulldate, NVL(p15_Start_DT, v_nulldate), DEFRMNTSTRTDT15), 
        DEFRMNTENDDT15  = DECODE(DEFRMNTENDDT15,  v_nulldate, NVL(p15_End_DT,   v_nulldate), DEFRMNTENDDT15), 
        DEFRMNTREQ15AMT = DECODE(DEFRMNTREQ15AMT, 0,          NVL(p15_AMT,      0),          DEFRMNTREQ15AMT),
        DEFRMNTDTMOD = v_sysdate, LASTUPDTUSERID = 'MFUPDATE', LASTUPDTDT = v_sysdate
    WHEN NOT MATCHED THEN 
        INSERT (DEFRMNTGPKEY, 
            DEFRMNTSTRTDT1, DEFRMNTENDDT1, DEFRMNTREQ1AMT,
            DEFRMNTSTRTDT2, DEFRMNTENDDT2, DEFRMNTREQ2AMT,
            DEFRMNTSTRTDT3, DEFRMNTENDDT3, DEFRMNTREQ3AMT,
            DEFRMNTSTRTDT4, DEFRMNTENDDT4, DEFRMNTREQ4AMT,
            DEFRMNTSTRTDT5, DEFRMNTENDDT5, DEFRMNTREQ5AMT,
            DEFRMNTSTRTDT6, DEFRMNTENDDT6, DEFRMNTREQ6AMT,
            DEFRMNTSTRTDT7, DEFRMNTENDDT7, DEFRMNTREQ7AMT,
            DEFRMNTSTRTDT8, DEFRMNTENDDT8, DEFRMNTREQ8AMT,
            DEFRMNTSTRTDT9, DEFRMNTENDDT9, DEFRMNTREQ9AMT,
            DEFRMNTSTRTDT10, DEFRMNTENDDT10, DEFRMNTREQ10AMT,
            DEFRMNTSTRTDT11, DEFRMNTENDDT11, DEFRMNTREQ11AMT,
            DEFRMNTSTRTDT12, DEFRMNTENDDT12, DEFRMNTREQ12AMT,
            DEFRMNTSTRTDT13, DEFRMNTENDDT13, DEFRMNTREQ13AMT,
            DEFRMNTSTRTDT14, DEFRMNTENDDT14, DEFRMNTREQ14AMT,
            DEFRMNTSTRTDT15, DEFRMNTENDDT15, DEFRMNTREQ15AMT,
            DEFRMNTDTCREAT, DEFRMNTDTMOD, CREATUSERID, CREATDT, LASTUPDTUSERID, LASTUPDTDT)
        VALUES (LoanNMB, 
            NVL(p1_Start_DT, v_nulldate), NVL(p1_End_DT, v_nulldate), NVL(p1_AMT, 0),
            NVL(p2_Start_DT, v_nulldate), NVL(p2_End_DT, v_nulldate), NVL(p2_AMT, 0),
            NVL(p3_Start_DT, v_nulldate), NVL(p3_End_DT, v_nulldate), NVL(p3_AMT, 0),
            NVL(p4_Start_DT, v_nulldate), NVL(p4_End_DT, v_nulldate), NVL(p4_AMT, 0),
            NVL(p5_Start_DT, v_nulldate), NVL(p5_End_DT, v_nulldate), NVL(p5_AMT, 0),
            NVL(p6_Start_DT, v_nulldate), NVL(p6_End_DT, v_nulldate), NVL(p6_AMT, 0),
            NVL(p7_Start_DT, v_nulldate), NVL(p7_End_DT, v_nulldate), NVL(p7_AMT, 0),
            NVL(p8_Start_DT, v_nulldate), NVL(p8_End_DT, v_nulldate), NVL(p8_AMT, 0),
            NVL(p9_Start_DT, v_nulldate), NVL(p9_End_DT, v_nulldate), NVL(p9_AMT, 0),
            NVL(p10_Start_DT, v_nulldate), NVL(p10_End_DT, v_nulldate), NVL(p10_AMT, 0),
            NVL(p11_Start_DT, v_nulldate), NVL(p11_End_DT, v_nulldate), NVL(p11_AMT, 0),
            NVL(p12_Start_DT, v_nulldate), NVL(p12_End_DT, v_nulldate), NVL(p12_AMT, 0),
            NVL(p13_Start_DT, v_nulldate), NVL(p13_End_DT, v_nulldate), NVL(p13_AMT, 0),
            NVL(p14_Start_DT, v_nulldate), NVL(p14_End_DT, v_nulldate), NVL(p14_AMT, 0),
            NVL(p15_Start_DT, v_nulldate), NVL(p15_End_DT, v_nulldate), NVL(p15_AMT, 0),
            v_sysdate, v_sysdate, 'MFUPDATE', v_sysdate, 'MFUPDATE', v_sysdate );

        v_rowcount := sql%rowcount;
        IF v_rowcount > 0 THEN
            v_sysmsg := v_sysmsg || '<li>' || to_char(v_rowcount) || ' records were inserted/updated in BFDG</li>';
        END IF;

	/* insert into the payment table */

	MERGE INTO SOFVDFPYTBL FV3 USING (
		SELECT LoanNmb as LnNbr, PymtAmt, StartDt, EndDt, Decode(cudpdtypid, 3, 'C-UP', 2, 'Part DEF', 1, 'DEF') || to_char(trunc(StartDt, 'mm'),' mm/yyyy') || to_char(trunc(StartDt, 'mm'),'-mm/yyyy') as cmn1
		FROM TempCUDTbl
		WHERE v_indexdate BETWEEN TRUNC(StartDt, 'mm') AND TRUNC(EndDt, 'mm') AND cudpdtypid <> 1 AND PymtAmt IS NOT NULL
	) TMP ON (FV3.LOANNMB = TMP.LnNbr)
	WHEN MATCHED THEN UPDATE SET
		DEFPLANNEWMOPYMTAMT = TMP.PymtAmt, DEFPLANOPNDT = TMP.StartDt, DEFPLANDTCLS = TMP.EndDt, DEFPLANLASTMAINTNDT = TMP.EndDt, DEFPLANCMNT1 = TMP.cmn1, DEFPLANCMNT2 = ' ', 
		LASTUPDTUSERID = 'MFUDDATE', LASTUPDTDT = v_sysdate
	WHEN NOT MATCHED THEN 
		INSERT (LOANNMB, DEFPLANNEWMOPYMTAMT, DEFPLANOPNDT, DEFPLANDTCLS, DEFPLANLASTMAINTNDT, DEFPLANCMNT1, DEFPLANCMNT2, CREATUSERID, CREATDT, LASTUPDTUSERID, LASTUPDTDT)
		VALUES (LnNbr, PymtAmt, StartDt, EndDt, EndDt, cmn1, ' ', 'MFUPDATE', v_sysdate, 'MFUPDATE', v_sysdate);
	/* end of the merge */

        v_rowcount := sql%rowcount;
        IF v_rowcount > 0 THEN
            v_sysmsg := v_sysmsg || '<li>' || to_char(v_rowcount) || ' records were inserted/updated in DFPY</li>';
        END IF;

	/* update status for CoreTransTbl, and insert new status into CoreTransStatTbl. This can include Pending Removal and Cancelled */
    -- For periods
	UPDATE CORETRANSTBL txns SET (STATIDCUR, LASTUPDTDT, LASTUPDTUSERID) = 
    (SELECT DISTINCT NewPeriodStatID, v_sysdate, 'MFUPDATE' FROM TempCUDTbl tmp WHERE txns.TRANSID = tmp.PeriodTransID AND NewPeriodStatID IS NOT NULL)
	WHERE TRANSTYPID = 6
	AND EXISTS (SELECT 1 FROM TempCUDTbl tmp WHERE txns.TRANSID = tmp.PeriodTransID AND NewPeriodStatID IS NOT NULL);

    v_rowcount := sql%rowcount;
    IF v_rowcount > 0 THEN
        v_sysmsg := v_sysmsg || '<li>' || to_char(v_rowcount) || ' Period status were updated</li>';
    END IF;

    INSERT INTO CORETRANSSTATTBL (TRANSID, STATID, TIMESTAMPFLD, CREATUSERID, CREATDT, LASTUPDTUSERID, LASTUPDTDT)
    SELECT DISTINCT PeriodTransID, NewPeriodStatID, v_sysdate, 'MFUPDATE', v_sysdate, 'MFUPDATE', v_sysdate
    FROM TempCUDTbl WHERE NewPeriodStatID IS NOT NULL;

    -- For plans. the DISTINCT is important here because some of them can have multiple periods
	UPDATE CORETRANSTBL txns SET (STATIDCUR, LASTUPDTDT, LASTUPDTUSERID) = 
    (SELECT DISTINCT NewPlanStatID, v_sysdate, 'MFUPDATE' FROM TempCUDTbl tmp WHERE txns.TRANSID = tmp.PlanTransID AND NewPlanStatID IS NOT NULL)
	WHERE TRANSTYPID = 7
	AND EXISTS (SELECT 1 FROM TempCUDTbl tmp WHERE txns.TRANSID = tmp.PlanTransID AND NewPlanStatID IS NOT NULL);

    v_rowcount := sql%rowcount;
    IF v_rowcount > 0 THEN
        v_sysmsg := v_sysmsg || '<li>' || to_char(v_rowcount) || ' Plan status were updated</li>';
    END IF;

    INSERT INTO CORETRANSSTATTBL (TRANSID, STATID, TIMESTAMPFLD, CREATUSERID, CREATDT, LASTUPDTUSERID, LASTUPDTDT)
    SELECT DISTINCT PlanTransID, NewPlanStatID, v_sysdate, 'MFUPDATE', v_sysdate, 'MFUPDATE', v_sysdate
    FROM TempCUDTbl WHERE NewPlanStatID IS NOT NULL;

    -- Update LND1 for all non Pending Removal and Cancellation. PR and C were already taken care of before the MERGE blocks
	UPDATE SOFVLND1TBL FV4 SET (LOANDTLSTATCDOFLOAN, LOANDTLSTATDT, LASTUPDTDT, LASTUPDTUSERID) = 
    (SELECT NewLND1Stat, v_sysdate, v_sysdate, 'MFUPDATE' FROM TempCUDTbl tmp 
		WHERE FV4.LOANNMB = tmp.LOANNMB AND v_indexdate BETWEEN TRUNC(StartDt, 'mm') AND TRUNC(EndDt, 'mm')
        AND tmp.PlanStatID NOT IN (43, 20)
        AND NewLND1Stat IS NOT NULL)
    WHERE EXISTS (SELECT 1 FROM TempCUDTbl tmp 
		WHERE FV4.LOANNMB = tmp.LOANNMB AND v_indexdate BETWEEN TRUNC(StartDt, 'mm') AND TRUNC(EndDt, 'mm')
        AND tmp.PlanStatID NOT IN (43, 20)
        AND NewLND1Stat IS NOT NULL);        
        
    v_rowcount := sql%rowcount;
    IF v_rowcount > 0 THEN
        v_sysmsg := v_sysmsg || '<li>' || to_char(v_rowcount) || ' Non-Pending Removal / Cancelled loans were updated in LND1</li>';
    END IF;
        
    -- Update LND1 to close plans when both plan and period status are 'complete'. This only happens for Next Month Macro 
	IF p_runmode = 'N' THEN 
        UPDATE SOFVLND1TBL FV5 SET (LOANDTLSTATCDOFLOAN, LOANDTLSTATDT, LASTUPDTDT, LASTUPDTUSERID) = 
        (SELECT DISTINCT NewLND1Stat, v_sysdate, v_sysdate, 'MFUPDATE' FROM TempCUDTbl tmp 
            WHERE FV5.LOANNMB = tmp.LOANNMB AND TRUNC(EndDt, 'mm') = Add_Months(v_indexdate, -1)
            AND tmp.NewPlanStatID = 4 AND tmp.NewPeriodStatID = 4
            AND tmp.PlanStatID NOT IN (43, 20)
            AND NewLND1Stat IS NOT NULL)
        WHERE EXISTS (SELECT 1 FROM TempCUDTbl tmp 
            WHERE FV5.LOANNMB = tmp.LOANNMB AND TRUNC(EndDt, 'mm') = Add_Months(v_indexdate, -1)
            AND tmp.NewPlanStatID = 4 AND tmp.NewPeriodStatID = 4
            AND tmp.PlanStatID NOT IN (43, 20)
            AND NewLND1Stat IS NOT NULL);  

        v_rowcount := sql%rowcount;
        IF v_rowcount > 0 THEN
            v_sysmsg := v_sysmsg || '<li>' || to_char(v_rowcount) || ' loans were close out and set to 01 in LND1</li>';
        END IF;

	END IF;       

    TaskEndCSP(p_taskname, p_retval => p_retval, p_errval => p_errval, p_errmsg => p_errmsg );
    v_taskisopen := 'N';
    DBMS_OUTPUT.PUT_LINE('P_RETVAL = ' || p_retval || ' / P_ERRVAL = ' || p_errval || ' / P_ERRMSG = ' || p_errmsg);
    
    IF length(trim(v_sysmsg)) IS NULL THEN
        v_sysmsg := '<p>There is no more un-processed plan for ' || v_monthmode || '.</p>';
    ELSE v_sysmsg := '<p>The following changes were made:</p><ul>' || v_sysmsg || '</ul>';
    END IF;
    
    p_errmsg := v_sysmsg;
    
EXCEPTION
WHEN OTHERS THEN
  p_errval:=SQLCODE;
  p_errmsg:=SQLERRM(SQLCODE);
  
  v_sysmsg := 'An Error happened: ' || p_errmsg || '<br>Other steps so far:<ul>' || v_sysmsg || '</ul>';
  
  IF v_taskisopen = 'Y' THEN
    TaskEndCSP(p_taskname, p_retval => p_retval, p_errval => p_errval, p_errmsg => p_errmsg );
  END IF;  
  
  p_errmsg := v_sysmsg;
  --RAISE;
END MFUPDTCATCHUPCSP;
/


GRANT EXECUTE ON STGCSA.MFUPDTCATCHUPCSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.MFUPDTCATCHUPCSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.MFUPDTCATCHUPCSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.MFUPDTCATCHUPCSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.MFUPDTCATCHUPCSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.MFUPDTCATCHUPCSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.MFUPDTCATCHUPCSP TO STGCSADEVROLE;

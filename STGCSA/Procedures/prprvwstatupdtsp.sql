DROP PROCEDURE STGCSA.PRPRVWSTATUPDTSP;

CREATE OR REPLACE PROCEDURE STGCSA.PRPRVWSTATUPDTSP(
    p_retval out number,
    p_errval out number,
    p_errmsg  out varchar2,
    p_identifier number:=0,
    p_userid varchar2
   ,p_PRPRVWSTATID VARCHAR2:=null
   ,p_LOANNMB CHAR:=null
   ,p_TRANSID VARCHAR2:=null
   ,p_TRANSTYPID VARCHAR2:=null
   ,p_INITIALPOSTRVW VARCHAR2:=null
   ,p_INITIALPOSTRVWBY VARCHAR2:=null
   ,p_SCNDPOSTRVW VARCHAR2:=null
   ,p_SCNDPOSTRVWBY VARCHAR2:=null
   ,p_THIRDPOSTRVW VARCHAR2:=null
   ,p_THIRDPOSTRVWBY VARCHAR2:=null
   ,p_FOURTHPOSTRVW VARCHAR2:=null
   ,p_FOURTHPOSTRVWBY VARCHAR2:=null
   ,p_FIFTHPOSTRVW VARCHAR2:=null
   ,p_FIFTHPOSTRVWBY VARCHAR2:=null
   ,p_SIXTHPOSTRVW VARCHAR2:=null
   ,p_SIXTHPOSTRVWBY VARCHAR2:=null
   ,p_SIXTHMOPOSTRVW VARCHAR2:=null
   ,p_SIXTHMOPOSTRVWBY VARCHAR2:=null
   ,p_SIXTHMOPOST2RVW VARCHAR2:=null
   ,p_SIXTHMOPOST2RVWBY VARCHAR2:=null
   ,p_SIXTHMOPOST3RVW VARCHAR2:=null
   ,p_SIXTHMOPOST3RVWBY VARCHAR2:=null
   ,p_SIXTHMOPOST4RVW VARCHAR2:=null
   ,p_SIXTHMOPOST4RVWBY VARCHAR2:=null
   ,p_SIXTHMOPOST5RVW VARCHAR2:=null
   ,p_SIXTHMOPOST5RVWBY VARCHAR2:=null
   ,p_THISTRVW VARCHAR2:=null
   ,p_THISTRVWBY VARCHAR2:=null
   ,p_FEEVAL VARCHAR2:=null
   ,p_FEEVALBY VARCHAR2:=null
   ,p_RVWSBMT VARCHAR2:=null
   ,p_RVWSBMTBY VARCHAR2:=null
   ,p_RVWSBMTDT DATE:=null
   ,p_WORKUPVAL VARCHAR2:=null
   ,p_WORKUPVALBY VARCHAR2:=null
   ,p_THISTVAL VARCHAR2:=null
   ,p_THISTVALBY VARCHAR2:=null
   ,p_VALSBMT VARCHAR2:=null
   ,p_VALSBMTBY VARCHAR2:=null
   ,p_VALSBMTDT DATE:=null
   ,p_PROOFVAL VARCHAR2:=null
   ,p_PROOFVALBY VARCHAR2:=null
   ,p_TIMESTAMPFLD DATE:=null
) as
 /*
  Created on: 2018-11-07 11:34:59
  Created by: GENR
  Crerated from template stdtblupd_template v3.22 12 Oct 2018 on 2018-11-07 11:34:59
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  Procedure PRPRVWSTATUPDTSP performs UPDATE on STGCSA.PRPRVWSTATTBL
    for P_IDENTIFIER=0 qualification is on Primary Key: PRPRVWSTATID
    for P_IDENTIFIER=1 qualification is on TRANSID
    for P_IDENTIFIER=2 qualification is on TRANSTYPID, TRANSID
    Updates all columns except the qualification keys and columns for which a
      null value is passed.
    SPECIAL secret update kludge!
      if p_identifier is set to -1 then a flag is set to use all input parm values
      and p_identifier is changed to 0 (update by PK).
      THEREFORE if using p_identifier=-1 you MUST pass intended value for all columns.
      This option allows setting colulmns to null!
*/
  dbrec STGCSA.PRPRVWSTATTBL%rowtype;
  logged_msg_id number;
  recnum number;
  rec_rowid rowid;
  keystouse varchar2(256);
  maxsev number:=0;
  pctsign char(1):='%';
  cur_colname varchar2(100):=null;
  cur_colvalue varchar2(4000);
  do_normal_field_checking char(1):='Y';
  overridden_p_identifier number;
  -- standard activity log capture vars
  l_LOANNMB char(10):=null;
  l_TRANSID number:=null;
  l_dbmdl varchar2(10):=null;
  l_entrydt date:=null;
  l_userid  varchar2(32):=null;
  l_usernm  varchar2(199):=null;
  l_timestampfld date:=null;
procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
begin
  if p_errval=0 then
    begin
      select * into dbrec from STGCSA.PRPRVWSTATTBL where rowid=rowid_in;
    exception when others then
      p_errval:=sqlcode;
      maxsev:=3;
      p_retval:=0;
      p_errmsg:='Fetch of STGCSA.PRPRVWSTATTBL row by rowid '||rowid_in
        ||' orinally fetched using '||orig_keyset_values
        ||' failed with '||sqlerrm(sqlcode);
    end;
  end if;
  if p_errval=0 then
    -- set this as lastupdt
    DBrec.lastupdtuserid:=p_userid;
    DBrec.lastupdtdt:=sysdate;
    -- copy into DBrec all table column input parameters that are not null,
    -- saving column name/value if needed for error reporting
    --  (if p_identifier=-1 then move input even if null)
    begin
      if P_PRPRVWSTATID is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PRPRVWSTATID';
        cur_colvalue:=P_PRPRVWSTATID;
        DBrec.PRPRVWSTATID:=P_PRPRVWSTATID;
      end if;
      if P_LOANNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_LOANNMB';
        cur_colvalue:=P_LOANNMB;
        DBrec.LOANNMB:=P_LOANNMB;
      end if;
      if P_TRANSID is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_TRANSID';
        cur_colvalue:=P_TRANSID;
        DBrec.TRANSID:=P_TRANSID;
      end if;
      if P_TRANSTYPID is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_TRANSTYPID';
        cur_colvalue:=P_TRANSTYPID;
        DBrec.TRANSTYPID:=P_TRANSTYPID;
      end if;
      if P_INITIALPOSTRVW is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_INITIALPOSTRVW';
        cur_colvalue:=P_INITIALPOSTRVW;
        DBrec.INITIALPOSTRVW:=P_INITIALPOSTRVW;
      end if;
      if P_INITIALPOSTRVWBY is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_INITIALPOSTRVWBY';
        cur_colvalue:=P_INITIALPOSTRVWBY;
        DBrec.INITIALPOSTRVWBY:=P_INITIALPOSTRVWBY;
      end if;
      if P_SCNDPOSTRVW is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SCNDPOSTRVW';
        cur_colvalue:=P_SCNDPOSTRVW;
        DBrec.SCNDPOSTRVW:=P_SCNDPOSTRVW;
      end if;
      if P_SCNDPOSTRVWBY is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SCNDPOSTRVWBY';
        cur_colvalue:=P_SCNDPOSTRVWBY;
        DBrec.SCNDPOSTRVWBY:=P_SCNDPOSTRVWBY;
      end if;
      if P_THIRDPOSTRVW is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_THIRDPOSTRVW';
        cur_colvalue:=P_THIRDPOSTRVW;
        DBrec.THIRDPOSTRVW:=P_THIRDPOSTRVW;
      end if;
      if P_THIRDPOSTRVWBY is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_THIRDPOSTRVWBY';
        cur_colvalue:=P_THIRDPOSTRVWBY;
        DBrec.THIRDPOSTRVWBY:=P_THIRDPOSTRVWBY;
      end if;
      if P_FOURTHPOSTRVW is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_FOURTHPOSTRVW';
        cur_colvalue:=P_FOURTHPOSTRVW;
        DBrec.FOURTHPOSTRVW:=P_FOURTHPOSTRVW;
      end if;
      if P_FOURTHPOSTRVWBY is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_FOURTHPOSTRVWBY';
        cur_colvalue:=P_FOURTHPOSTRVWBY;
        DBrec.FOURTHPOSTRVWBY:=P_FOURTHPOSTRVWBY;
      end if;
      if P_FIFTHPOSTRVW is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_FIFTHPOSTRVW';
        cur_colvalue:=P_FIFTHPOSTRVW;
        DBrec.FIFTHPOSTRVW:=P_FIFTHPOSTRVW;
      end if;
      if P_FIFTHPOSTRVWBY is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_FIFTHPOSTRVWBY';
        cur_colvalue:=P_FIFTHPOSTRVWBY;
        DBrec.FIFTHPOSTRVWBY:=P_FIFTHPOSTRVWBY;
      end if;
      if P_SIXTHPOSTRVW is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SIXTHPOSTRVW';
        cur_colvalue:=P_SIXTHPOSTRVW;
        DBrec.SIXTHPOSTRVW:=P_SIXTHPOSTRVW;
      end if;
      if P_SIXTHPOSTRVWBY is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SIXTHPOSTRVWBY';
        cur_colvalue:=P_SIXTHPOSTRVWBY;
        DBrec.SIXTHPOSTRVWBY:=P_SIXTHPOSTRVWBY;
      end if;
      if P_SIXTHMOPOSTRVW is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SIXTHMOPOSTRVW';
        cur_colvalue:=P_SIXTHMOPOSTRVW;
        DBrec.SIXTHMOPOSTRVW:=P_SIXTHMOPOSTRVW;
      end if;
      if P_SIXTHMOPOSTRVWBY is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SIXTHMOPOSTRVWBY';
        cur_colvalue:=P_SIXTHMOPOSTRVWBY;
        DBrec.SIXTHMOPOSTRVWBY:=P_SIXTHMOPOSTRVWBY;
      end if;
      if P_SIXTHMOPOST2RVW is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SIXTHMOPOST2RVW';
        cur_colvalue:=P_SIXTHMOPOST2RVW;
        DBrec.SIXTHMOPOST2RVW:=P_SIXTHMOPOST2RVW;
      end if;
      if P_SIXTHMOPOST2RVWBY is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SIXTHMOPOST2RVWBY';
        cur_colvalue:=P_SIXTHMOPOST2RVWBY;
        DBrec.SIXTHMOPOST2RVWBY:=P_SIXTHMOPOST2RVWBY;
      end if;
      if P_SIXTHMOPOST3RVW is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SIXTHMOPOST3RVW';
        cur_colvalue:=P_SIXTHMOPOST3RVW;
        DBrec.SIXTHMOPOST3RVW:=P_SIXTHMOPOST3RVW;
      end if;
      if P_SIXTHMOPOST3RVWBY is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SIXTHMOPOST3RVWBY';
        cur_colvalue:=P_SIXTHMOPOST3RVWBY;
        DBrec.SIXTHMOPOST3RVWBY:=P_SIXTHMOPOST3RVWBY;
      end if;
      if P_SIXTHMOPOST4RVW is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SIXTHMOPOST4RVW';
        cur_colvalue:=P_SIXTHMOPOST4RVW;
        DBrec.SIXTHMOPOST4RVW:=P_SIXTHMOPOST4RVW;
      end if;
      if P_SIXTHMOPOST4RVWBY is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SIXTHMOPOST4RVWBY';
        cur_colvalue:=P_SIXTHMOPOST4RVWBY;
        DBrec.SIXTHMOPOST4RVWBY:=P_SIXTHMOPOST4RVWBY;
      end if;
      if P_SIXTHMOPOST5RVW is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SIXTHMOPOST5RVW';
        cur_colvalue:=P_SIXTHMOPOST5RVW;
        DBrec.SIXTHMOPOST5RVW:=P_SIXTHMOPOST5RVW;
      end if;
      if P_SIXTHMOPOST5RVWBY is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SIXTHMOPOST5RVWBY';
        cur_colvalue:=P_SIXTHMOPOST5RVWBY;
        DBrec.SIXTHMOPOST5RVWBY:=P_SIXTHMOPOST5RVWBY;
      end if;
      if P_THISTRVW is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_THISTRVW';
        cur_colvalue:=P_THISTRVW;
        DBrec.THISTRVW:=P_THISTRVW;
      end if;
      if P_THISTRVWBY is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_THISTRVWBY';
        cur_colvalue:=P_THISTRVWBY;
        DBrec.THISTRVWBY:=P_THISTRVWBY;
      end if;
      if P_FEEVAL is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_FEEVAL';
        cur_colvalue:=P_FEEVAL;
        DBrec.FEEVAL:=P_FEEVAL;
      end if;
      if P_FEEVALBY is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_FEEVALBY';
        cur_colvalue:=P_FEEVALBY;
        DBrec.FEEVALBY:=P_FEEVALBY;
      end if;
      if P_RVWSBMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_RVWSBMT';
        cur_colvalue:=P_RVWSBMT;
        DBrec.RVWSBMT:=P_RVWSBMT;
      end if;
      if P_RVWSBMTBY is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_RVWSBMTBY';
        cur_colvalue:=P_RVWSBMTBY;
        DBrec.RVWSBMTBY:=P_RVWSBMTBY;
      end if;
      if P_RVWSBMTDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_RVWSBMTDT';
        cur_colvalue:=P_RVWSBMTDT;
        DBrec.RVWSBMTDT:=P_RVWSBMTDT;
      end if;
      if P_WORKUPVAL is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_WORKUPVAL';
        cur_colvalue:=P_WORKUPVAL;
        DBrec.WORKUPVAL:=P_WORKUPVAL;
      end if;
      if P_WORKUPVALBY is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_WORKUPVALBY';
        cur_colvalue:=P_WORKUPVALBY;
        DBrec.WORKUPVALBY:=P_WORKUPVALBY;
      end if;
      if P_THISTVAL is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_THISTVAL';
        cur_colvalue:=P_THISTVAL;
        DBrec.THISTVAL:=P_THISTVAL;
      end if;
      if P_THISTVALBY is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_THISTVALBY';
        cur_colvalue:=P_THISTVALBY;
        DBrec.THISTVALBY:=P_THISTVALBY;
      end if;
      if P_VALSBMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_VALSBMT';
        cur_colvalue:=P_VALSBMT;
        DBrec.VALSBMT:=P_VALSBMT;
      end if;
      if P_VALSBMTBY is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_VALSBMTBY';
        cur_colvalue:=P_VALSBMTBY;
        DBrec.VALSBMTBY:=P_VALSBMTBY;
      end if;
      if P_VALSBMTDT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_VALSBMTDT';
        cur_colvalue:=P_VALSBMTDT;
        DBrec.VALSBMTDT:=P_VALSBMTDT;
      end if;
      if P_PROOFVAL is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PROOFVAL';
        cur_colvalue:=P_PROOFVAL;
        DBrec.PROOFVAL:=P_PROOFVAL;
      end if;
      if P_PROOFVALBY is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PROOFVALBY';
        cur_colvalue:=P_PROOFVALBY;
        DBrec.PROOFVALBY:=P_PROOFVALBY;
      end if;
      if P_TIMESTAMPFLD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_TIMESTAMPFLD';
        cur_colvalue:=P_TIMESTAMPFLD;
        DBrec.TIMESTAMPFLD:=P_TIMESTAMPFLD;
      end if;
    exception when others then
      p_errval:=sqlcode;
      if maxsev<3 then maxsev:=3; end if;
      p_errmsg:='Error using '||cur_colname||' Value >'||cur_colvalue||'<';
    end;
  end if;
  -- if all still ok do actual update
  if p_errval=0 then
    Begin
      update STGCSA.PRPRVWSTATTBL
        set lastupdtuserid=DBrec.lastupdtuserid,
          lastupdtdt=DBrec.lastupdtdt
          ,PRPRVWSTATID=DBrec.PRPRVWSTATID
          ,LOANNMB=DBrec.LOANNMB
          ,TRANSID=DBrec.TRANSID
          ,TRANSTYPID=DBrec.TRANSTYPID
          ,INITIALPOSTRVW=DBrec.INITIALPOSTRVW
          ,INITIALPOSTRVWBY=DBrec.INITIALPOSTRVWBY
          ,SCNDPOSTRVW=DBrec.SCNDPOSTRVW
          ,SCNDPOSTRVWBY=DBrec.SCNDPOSTRVWBY
          ,THIRDPOSTRVW=DBrec.THIRDPOSTRVW
          ,THIRDPOSTRVWBY=DBrec.THIRDPOSTRVWBY
          ,FOURTHPOSTRVW=DBrec.FOURTHPOSTRVW
          ,FOURTHPOSTRVWBY=DBrec.FOURTHPOSTRVWBY
          ,FIFTHPOSTRVW=DBrec.FIFTHPOSTRVW
          ,FIFTHPOSTRVWBY=DBrec.FIFTHPOSTRVWBY
          ,SIXTHPOSTRVW=DBrec.SIXTHPOSTRVW
          ,SIXTHPOSTRVWBY=DBrec.SIXTHPOSTRVWBY
          ,SIXTHMOPOSTRVW=DBrec.SIXTHMOPOSTRVW
          ,SIXTHMOPOSTRVWBY=DBrec.SIXTHMOPOSTRVWBY
          ,SIXTHMOPOST2RVW=DBrec.SIXTHMOPOST2RVW
          ,SIXTHMOPOST2RVWBY=DBrec.SIXTHMOPOST2RVWBY
          ,SIXTHMOPOST3RVW=DBrec.SIXTHMOPOST3RVW
          ,SIXTHMOPOST3RVWBY=DBrec.SIXTHMOPOST3RVWBY
          ,SIXTHMOPOST4RVW=DBrec.SIXTHMOPOST4RVW
          ,SIXTHMOPOST4RVWBY=DBrec.SIXTHMOPOST4RVWBY
          ,SIXTHMOPOST5RVW=DBrec.SIXTHMOPOST5RVW
          ,SIXTHMOPOST5RVWBY=DBrec.SIXTHMOPOST5RVWBY
          ,THISTRVW=DBrec.THISTRVW
          ,THISTRVWBY=DBrec.THISTRVWBY
          ,FEEVAL=DBrec.FEEVAL
          ,FEEVALBY=DBrec.FEEVALBY
          ,RVWSBMT=DBrec.RVWSBMT
          ,RVWSBMTBY=DBrec.RVWSBMTBY
          ,RVWSBMTDT=DBrec.RVWSBMTDT
          ,WORKUPVAL=DBrec.WORKUPVAL
          ,WORKUPVALBY=DBrec.WORKUPVALBY
          ,THISTVAL=DBrec.THISTVAL
          ,THISTVALBY=DBrec.THISTVALBY
          ,VALSBMT=DBrec.VALSBMT
          ,VALSBMTBY=DBrec.VALSBMTBY
          ,VALSBMTDT=DBrec.VALSBMTDT
          ,PROOFVAL=DBrec.PROOFVAL
          ,PROOFVALBY=DBrec.PROOFVALBY
          ,TIMESTAMPFLD=DBrec.TIMESTAMPFLD
        where rowid=rowid_in;
      recnum:=recnum+1;
    exception when others then
      p_errval:=sqlcode;
      p_retval:=0;
      if maxsev<3 then maxsev:=3; end if;
      p_errmsg:= 'Error while doing update on STGCSA.PRPRVWSTATTBL, on rec with rowid '
      ||rowid_in||' previously fetched using key: '||orig_keyset_values
      ||' got Oracle error '||sqlerrm(sqlcode);
    end;
  end if;
END;
begin
  -- setup
  savepoint PRPRVWSTATUPDTSP;
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  recnum:=0;
  runtime.logger(logged_msg_id,'STDLOG',100,'Init PRPRVWSTATUPDTSP',p_userid,4,
    logtxt1=>'PRPRVWSTATUPDTSP',logtxt2=>'stdtblupd_template v3.22 12 Oct 2018',
    PROGRAM_NAME=>'PRPRVWSTATUPDTSP');
  --
  l_LOANNMB:=P_LOANNMB;  l_TRANSID:=P_TRANSID;  l_TIMESTAMPFLD:=P_TIMESTAMPFLD;
--
-- special p_identifier kludge
--  if p_identifier is set to -1 then it treated as 0 (update by PK)
--  but a flag is set use passed value EVEN IF NULL.  Since for option -1
--  every column is updated to what was passed, intended values MUST be passed
--  for all columns.  This option allows setting colulmns to null!
  overridden_p_identifier:=p_identifier;
  if p_identifier=-1 then
    do_normal_field_checking:='N';
    overridden_p_identifier:=0;
  end if;
  case overridden_p_identifier
  when 0 then
    -- case to update one row based on primary key
    -- p_identifier=0 (choice 0) is default action for a stored procedure
    -- get existing version of row
    if p_errval=0 then
      keystouse:='(PRPRVWSTATID)=('||P_PRPRVWSTATID||')';
      begin
        select rowid into rec_rowid from STGCSA.PRPRVWSTATTBL where 1=1
         and PRPRVWSTATID=P_PRPRVWSTATID;
      exception when no_data_found then
        p_errval:=0;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Warning No STGCSA.PRPRVWSTATTBL row to update with key(s) '
          ||keystouse;
      end;
    end if;
    if p_errval=0 then
      fetch_and_update_by_rowid(rec_rowid, keystouse);
    end if;
  when 1 then
    -- case to update one or more rows based on keyset KEYS1
    if p_errval=0 then
      keystouse:='(TRANSID)=('||P_TRANSID||')';
      begin
        for r1 in (select rowid from STGCSA.PRPRVWSTATTBL a where 1=1
          and TRANSID=P_TRANSID
  )
        loop
          rec_rowid:=r1.rowid;
          if p_errval=0 then
            fetch_and_update_by_rowid(rec_rowid, keystouse);
          end if;
        end loop;
      exception when others then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<3 then maxsev:=3; end if;
        p_errmsg:='Error fetching STGCSA.PRPRVWSTATTBL row(s) with key(s) '
         ||keystouse||' Oracle returned '||sqlerrm(sqlcode);
      end;
      if recnum=0 then
        p_errval:=0;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Warning No STGCSA.PRPRVWSTATTBL row(s) to update with key(s) '
          ||keystouse;
      end if;
    end if;
  when 2 then
    -- case to update one or more rows based on keyset KEYS2
    if p_errval=0 then
      keystouse:='(TRANSID)=('||P_TRANSID||')'||' and '||'(TRANSTYPID)=('||P_TRANSTYPID||')';
      begin
        for r2 in (select rowid from STGCSA.PRPRVWSTATTBL a where 1=1
          and TRANSID=P_TRANSID
            and TRANSTYPID=P_TRANSTYPID
  )
        loop
          rec_rowid:=r2.rowid;
          if p_errval=0 then
            fetch_and_update_by_rowid(rec_rowid, keystouse);
          end if;
        end loop;
      exception when others then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<3 then maxsev:=3; end if;
        p_errmsg:='Error fetching STGCSA.PRPRVWSTATTBL row(s) with key(s) '
         ||keystouse||' Oracle returned '||sqlerrm(sqlcode);
      end;
      if recnum=0 then
        p_errval:=0;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Warning No STGCSA.PRPRVWSTATTBL row(s) to update with key(s) '
          ||keystouse;
      end if;
    end if;
  else /* P_IDENTIFIER was unexpected value */
    p_errval:=-20002;
    if maxsev<3 then maxsev:=3; end if;
    p_errmsg:='Error P_IDENTIFIER contained unexpected value: '||p_identifier;
  end case;
  P_RETVAL:=RECNUM;
  -- post to log
  if p_errval=0 then
    runtime.logger(logged_msg_id,'STDLOG',101,
      'End PRPRVWSTATUPDTSP With 0 return, P_RETVAL='
      ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
      1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld
      ,PROGRAM_NAME=>'PRPRVWSTATUPDTSP');
  else
    ROLLBACK TO PRPRVWSTATUPDTSP;
    p_errmsg:=p_errmsg||' in PRPRVWSTATUPDTSP build 2018-11-07 11:34:59';
    runtime.logger(logged_msg_id,'STDERR',103,p_errmsg,p_userid,
      maxsev, l_usernm, l_loannmb,l_transid, l_dbmdl, l_entrydt, l_timestampfld,
      PROGRAM_NAME=>'PRPRVWSTATUPDTSP',force_log_entry=>true);
  end if;
EXCEPTION
WHEN OTHERS THEN
  ROLLBACK TO PRPRVWSTATUPDTSP;
  p_errmsg:=' Error outer exception handler caught Oracle error:'||SQLERRM(SQLCODE)
    ||' in PRPRVWSTATUPDTSP build 2018-11-07 11:34:59';
  runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
    3,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld,
    PROGRAM_NAME=>'PRPRVWSTATUPDTSP',force_log_entry=>true);
--
END PRPRVWSTATUPDTSP;
/


GRANT EXECUTE ON STGCSA.PRPRVWSTATUPDTSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.PRPRVWSTATUPDTSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.PRPRVWSTATUPDTSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.PRPRVWSTATUPDTSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.PRPRVWSTATUPDTSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.PRPRVWSTATUPDTSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.PRPRVWSTATUPDTSP TO STGCSADEVROLE;

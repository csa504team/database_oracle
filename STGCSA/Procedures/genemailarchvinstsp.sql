DROP PROCEDURE STGCSA.GENEMAILARCHVINSTSP;

CREATE OR REPLACE PROCEDURE STGCSA.GENEMAILARCHVINSTSP(
    p_retval out number,
    p_errval out number,
    p_errmsg  out varchar2,
    p_identifier number:=0,
    p_userid varchar2
   ,p_EMAILID VARCHAR2:=null
   ,p_DBMDL VARCHAR2:=null
   ,p_RELTDTBLNM VARCHAR2:=null
   ,p_RELTDTBLREF VARCHAR2:=null
   ,p_LOANNMB CHAR:=null
   ,p_EMAILCATID VARCHAR2:=null
   ,p_EMAILTO VARCHAR2:=null
   ,p_EMAILFROMNM VARCHAR2:=null
   ,p_EMAILFROMADR VARCHAR2:=null
   ,p_EMAILCC VARCHAR2:=null
   ,p_EMAILSUBJ VARCHAR2:=null
   ,p_EMAILBODY VARCHAR2:=null
   ,p_EMAILSENTDT DATE:=null
   ,p_MSGFILENM VARCHAR2:=null
   ,p_TIMESTAMPFLD DATE:=null
) as
 /*
  Created on: 2018-11-07 11:33:45
  Created by: GENR
  Crerated from template stdtblins_template v3.05 13 Sep 2018 on 2018-11-07 11:33:45
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  Procedure performs INSERT on STGCSA.GENEMAILARCHVTBLfor P_IDENTIFIER=0
*/
  rundate date;
  gn global_name.global_name%type;
  new_rec STGCSA.GENEMAILARCHVTBL%rowtype;
  logged_msgid_retval number;
  audretval number;
  crossover_active char(1);
  l_LOANNMB char(10):=null;
  l_TRANSID number:=null;
  l_dbmdl varchar2(10):=null;
  l_entrydt date:=null;
  l_userid  varchar2(32):=null;
  l_usernm  varchar2(199):=null;
  l_timestampfld date:=null;
  cur_col_name varchar2(100):=null;
  maxsev number:=0;
  holdstring varchar2(4000);
  keystouse varchar2(1000);
  -- std date for column defaulting
  JAN_1_1900  constant date:=to_date('JAN-01-1900','MON-DD-YYYY');
  cur_colvalue varchar2(4095);
  cur_colname varchar2(30);
-- Function used to initialize EMAILID from sequence EMAILIDSEQ
Function NEXTVAL_EMAILIDSEQ return number is
  N number;
begin
  select EMAILIDSEQ.nextval into n from dual;
  return n;
end;
-- Main body begins here
begin
  -- setup
  savepoint GENEMAILARCHVINSTSP;
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  runtime.logger(logged_msgid_retval,'STDLOG',100,'Init GENEMAILARCHVINSTSP',p_userid,4,
    logtxt1=>'GENEMAILARCHVINSTSP',logtxt2=>'stdtblins_template v3.05 13 Sep 2018',
    PROGRAM_NAME=>'GENEMAILARCHVINSTSP');
  select global_name, sysdate into gn, rundate from global_name;
  begin
    cur_colname:='DBMDL';
    cur_colvalue:=P_DBMDL;
    l_DBMDL:=P_DBMDL;    cur_colname:='LOANNMB';
    cur_colvalue:=P_LOANNMB;
    l_LOANNMB:=P_LOANNMB;    cur_colname:='TIMESTAMPFLD';
    cur_colvalue:=P_TIMESTAMPFLD;
    l_TIMESTAMPFLD:=P_TIMESTAMPFLD;
  exception when others then
    p_errval:=sqlcode;
    p_errmsg:='Error '||sqlerrm(sqlcode)||' using '||cur_colname||' Value >'||cur_colvalue||'<';
  end;
  new_rec.creatuserid:=p_userid;
  new_rec.creatdt:=sysdate;
  new_rec.lastupdtuserid:=p_userid;
  new_rec.lastupdtdt:=sysdate;
  if p_identifier=0 then
    -- p_identifier=0 this is default for a stored procedure, only action for insert
    runtime.errfound:=false;
    runtime.errstr:=' ';
    --
    -- Default columns as applicable
  if p_errval=0 then
    begin
      cur_colname:='P_EMAILID';
      cur_colvalue:=P_EMAILID;
      if P_EMAILID is null then

        new_rec.EMAILID:=NEXTVAL_EMAILIDSEQ();

      else
        new_rec.EMAILID:=P_EMAILID;
      end if;
      cur_colname:='P_DBMDL';
      cur_colvalue:=P_DBMDL;
      if P_DBMDL is null then

        new_rec.DBMDL:=' ';

      else
        new_rec.DBMDL:=P_DBMDL;
      end if;
      cur_colname:='P_RELTDTBLNM';
      cur_colvalue:=P_RELTDTBLNM;
      if P_RELTDTBLNM is null then

        new_rec.RELTDTBLNM:=' ';

      else
        new_rec.RELTDTBLNM:=P_RELTDTBLNM;
      end if;
      cur_colname:='P_RELTDTBLREF';
      cur_colvalue:=P_RELTDTBLREF;
      if P_RELTDTBLREF is null then

        new_rec.RELTDTBLREF:=0;

      else
        new_rec.RELTDTBLREF:=P_RELTDTBLREF;
      end if;
      cur_colname:='P_LOANNMB';
      cur_colvalue:=P_LOANNMB;
      if P_LOANNMB is null then

        new_rec.LOANNMB:=rpad(' ',10);

      else
        new_rec.LOANNMB:=P_LOANNMB;
      end if;
      cur_colname:='P_EMAILCATID';
      cur_colvalue:=P_EMAILCATID;
      if P_EMAILCATID is null then

        new_rec.EMAILCATID:=0;

      else
        new_rec.EMAILCATID:=P_EMAILCATID;
      end if;
      cur_colname:='P_EMAILTO';
      cur_colvalue:=P_EMAILTO;
      if P_EMAILTO is null then

        new_rec.EMAILTO:=' ';

      else
        new_rec.EMAILTO:=P_EMAILTO;
      end if;
      cur_colname:='P_EMAILFROMNM';
      cur_colvalue:=P_EMAILFROMNM;
      if P_EMAILFROMNM is null then

        new_rec.EMAILFROMNM:=' ';

      else
        new_rec.EMAILFROMNM:=P_EMAILFROMNM;
      end if;
      cur_colname:='P_EMAILFROMADR';
      cur_colvalue:=P_EMAILFROMADR;
      if P_EMAILFROMADR is null then

        new_rec.EMAILFROMADR:=' ';

      else
        new_rec.EMAILFROMADR:=P_EMAILFROMADR;
      end if;
      cur_colname:='P_EMAILCC';
      cur_colvalue:=P_EMAILCC;
      if P_EMAILCC is null then

        new_rec.EMAILCC:=' ';

      else
        new_rec.EMAILCC:=P_EMAILCC;
      end if;
      cur_colname:='P_EMAILSUBJ';
      cur_colvalue:=P_EMAILSUBJ;
      if P_EMAILSUBJ is null then

        new_rec.EMAILSUBJ:=' ';

      else
        new_rec.EMAILSUBJ:=P_EMAILSUBJ;
      end if;
      cur_colname:='P_EMAILBODY';
      cur_colvalue:=P_EMAILBODY;
      if P_EMAILBODY is null then

        new_rec.EMAILBODY:=' ';

      else
        new_rec.EMAILBODY:=P_EMAILBODY;
      end if;
      cur_colname:='P_EMAILSENTDT';
      cur_colvalue:=P_EMAILSENTDT;
      if P_EMAILSENTDT is null then

        new_rec.EMAILSENTDT:=jan_1_1900;

      else
        new_rec.EMAILSENTDT:=P_EMAILSENTDT;
      end if;
      cur_colname:='P_MSGFILENM';
      cur_colvalue:=P_MSGFILENM;
      if P_MSGFILENM is null then

        new_rec.MSGFILENM:=' ';

      else
        new_rec.MSGFILENM:=P_MSGFILENM;
      end if;
      cur_colname:='P_TIMESTAMPFLD';
      cur_colvalue:=P_TIMESTAMPFLD;
      if P_TIMESTAMPFLD is null then

        new_rec.TIMESTAMPFLD:=jan_1_1900;

      else
        new_rec.TIMESTAMPFLD:=P_TIMESTAMPFLD;
      end if;
    exception when others then
       p_errval:=sqlcode;
       p_errmsg:='Error - Oracle returned '||sqlerrm(sqlcode)||' using '
           ||cur_colname||' Value >'||cur_colvalue||'<';
    end;
  end if;
    keystouse:='(EMAILID)=('||new_rec.EMAILID||')';
    if p_errval=0 then
      begin
        insert into STGCSA.GENEMAILARCHVTBL values new_rec;
        p_RETVAL := SQL%ROWCOUNT;
      exception
      when dup_val_on_index then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Insert caused dupe on unique IX: '||sqlerrm(sqlcode);
      when others then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<3 then maxsev:=3; end if;
        p_errmsg:='Inserting record caught SQL error '||sqlerrm(sqlcode);
      end;
    end if;
  else /* P_IDENTIFIER was unexpected value */
    p_errval:=-20002;
    if maxsev<3 then maxsev:=3; end if;
    p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier
      ||' inserting record with key '||keystouse;
  end if;
  if p_errval=0 then
    runtime.logger(logged_msgid_retval,'STDLOG',101,
      'End GENEMAILARCHVINSTSP With 0 return, P_RETVAL='
      ||p_retval||' Record Key '||keystouse,p_userid,
      1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt,
      l_timestampfld,PROGRAM_NAME=>'GENEMAILARCHVINSTSP');
  else
    p_errmsg:='Error: '||P_ERRMSG||' in GENEMAILARCHVINSTSP Record Key '||keystouse
      ||' Build: 2018-11-07 11:33:45';
    ROLLBACK TO GENEMAILARCHVINSTSP;
    runtime.logger(logged_msgid_retval,'STDERR',103,
      p_errmsg,p_userid,maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,
      l_entrydt, l_timestampfld,PROGRAM_NAME=>'GENEMAILARCHVINSTSP',force_log_entry=>TRUE);
  end if;
EXCEPTION
WHEN OTHERS THEN
  p_errval:=SQLCODE;
  p_errmsg:=' In outer exception handler in GENEMAILARCHVINSTSP caught exception '
    ||sqlerrm(sqlcode)||' Record Key '||keystouse||' build 2018-11-07 11:33:45';
  ROLLBACK TO GENEMAILARCHVINSTSP;
  runtime.logger(logged_msgid_retval,'STDERR',20,p_errmsg,p_userid,3,
  PROGRAM_NAME=>'GENEMAILARCHVINSTSP',force_log_entry=>TRUE);
--
END GENEMAILARCHVINSTSP;
/


GRANT EXECUTE ON STGCSA.GENEMAILARCHVINSTSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.GENEMAILARCHVINSTSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.GENEMAILARCHVINSTSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.GENEMAILARCHVINSTSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.GENEMAILARCHVINSTSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.GENEMAILARCHVINSTSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.GENEMAILARCHVINSTSP TO STGCSADEVROLE;

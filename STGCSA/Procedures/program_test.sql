DROP PROCEDURE STGCSA.PROGRAM_TEST;

CREATE OR REPLACE PROCEDURE STGCSA.program_test
   (p_retval out number,
    p_errval out number,
    p_errmsg out varchar2,
    p_identifier number:=0,
	p_what_to_do varchar2:='',      
    p_userid varchar2,
    p_resetdt timestamp
	) as
  prog varchar2(40):='TEST_PROGRAM';
  ver  varchar2(40):='V1 21 Mar 2019, JG'; 
  logentryid number;
  msgtxt varchar2(4000);
begin
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  if lower(p_what_to_do)='start_program' then
    msgtxt:='Starting program...';
    runtime2.logger(logentryid,'STDXVR',235,msgtxt,user,4,
    entrydt=>cast(p_resetdt as date),
    program_name=>prog||' '||ver,force_log_entry=>true);
    dbms_output.put_line(MSGTXT);
  elsif lower(p_what_to_do)='end_program' then
		MSGTXT:='Ending program...';
		runtime2.logger(logentryid,'STDXVR',236,MSGTXT,p_userid,5,
		program_name=>prog||' '||ver);
		dbms_output.put_line(MSGTXT);
  else
    --dbms_output.put_line(p_what_to_do);
    p_errmsg:= 'Unexpected value entered for p_what_to_do. 
    It can only take values from start_program and end_program';
    runtime2.logger(logentryid,'STDERR',104,p_errmsg,user,3,
    entrydt=>cast(p_resetdt as date),
    program_name=>prog||' '||ver,force_log_entry=>true);
    --dbms_output.put_line(p_errmsg); 
   end if;
end;
/

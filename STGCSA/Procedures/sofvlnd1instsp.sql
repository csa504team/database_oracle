DROP PROCEDURE STGCSA.SOFVLND1INSTSP;

CREATE OR REPLACE PROCEDURE STGCSA.SOFVLND1INSTSP(
    p_retval out number,
    p_errval out number,
    p_errmsg  out varchar2,
    p_identifier number:=0,
    p_userid varchar2
   ,p_LOANNMB CHAR:=null
   ,p_LOANDTLSBAOFC CHAR:=null
   ,p_LOANDTLBORRNM VARCHAR2:=null
   ,p_LOANDTLBORRMAILADRSTR1NM VARCHAR2:=null
   ,p_LOANDTLBORRMAILADRSTR2NM VARCHAR2:=null
   ,p_LOANDTLBORRMAILADRCTYNM VARCHAR2:=null
   ,p_LOANDTLBORRMAILADRSTCD CHAR:=null
   ,p_LOANDTLBORRMAILADRZIPCD CHAR:=null
   ,p_LOANDTLBORRMAILADRZIP4CD CHAR:=null
   ,p_LOANDTLSBCNM VARCHAR2:=null
   ,p_LOANDTLSBCMAILADRSTR1NM VARCHAR2:=null
   ,p_LOANDTLSBCMAILADRSTR2NM VARCHAR2:=null
   ,p_LOANDTLSBCMAILADRCTYNM VARCHAR2:=null
   ,p_LOANDTLSBCMAILADRSTCD CHAR:=null
   ,p_LOANDTLSBCMAILADRZIPCD CHAR:=null
   ,p_LOANDTLSBCMAILADRZIP4CD CHAR:=null
   ,p_LOANDTLTAXID CHAR:=null
   ,p_LOANDTLCDCREGNCD CHAR:=null
   ,p_LOANDTLCDCCERT CHAR:=null
   ,p_LOANDTLDEFPYMT CHAR:=null
   ,p_LOANDTLDBENTRPRINAMT VARCHAR2:=null
   ,p_LOANDTLDBENTRCURBALAMT VARCHAR2:=null
   ,p_LOANDTLDBENTRINTRTPCT VARCHAR2:=null
   ,p_LOANDTLISSDT DATE:=null
   ,p_LOANDTLDEFPYMTDTX CHAR:=null
   ,p_LOANDTLDBENTRPYMTDTTRM CHAR:=null
   ,p_LOANDTLDBENTRPROCDSAMT VARCHAR2:=null
   ,p_LOANDTLDBENTRPROCDSESCROWAMT VARCHAR2:=null
   ,p_LOANDTLSEMIANNPYMTAMT VARCHAR2:=null
   ,p_LOANDTLNOTEPRINAMT VARCHAR2:=null
   ,p_LOANDTLNOTECURBALAMT VARCHAR2:=null
   ,p_LOANDTLNOTEINTRTPCT VARCHAR2:=null
   ,p_LOANDTLNOTEDT DATE:=null
   ,p_LOANDTLACCTCD VARCHAR2:=null
   ,p_LOANDTLACCTCHNGCD CHAR:=null
   ,p_LOANDTLREAMIND CHAR:=null
   ,p_LOANDTLNOTEPYMTDTTRM CHAR:=null
   ,p_LOANDTLRESRVDEPAMT VARCHAR2:=null
   ,p_LOANDTLUNDRWTRFEEPCT VARCHAR2:=null
   ,p_LOANDTLUNDRWTRFEEMTDAMT VARCHAR2:=null
   ,p_LOANDTLUNDRWTRFEEYTDAMT VARCHAR2:=null
   ,p_LOANDTLUNDRWTRFEEPTDAMT VARCHAR2:=null
   ,p_LOANDTLCDCFEEPCT VARCHAR2:=null
   ,p_LOANDTLCDCFEEMTDAMT VARCHAR2:=null
   ,p_LOANDTLCDCFEEYTDAMT VARCHAR2:=null
   ,p_LOANDTLCDCFEEPTDAMT VARCHAR2:=null
   ,p_LOANDTLCSAFEEPCT VARCHAR2:=null
   ,p_LOANDTLCSAFEEMTDAMT VARCHAR2:=null
   ,p_LOANDTLCSAFEEYTDAMT VARCHAR2:=null
   ,p_LOANDTLCSAFEEPTDAMT VARCHAR2:=null
   ,p_LOANDTLATTORNEYFEEAMT VARCHAR2:=null
   ,p_LOANDTLINITFEEPCT VARCHAR2:=null
   ,p_LOANDTLINITFEEDOLLRAMT VARCHAR2:=null
   ,p_LOANDTLFUNDFEEPCT VARCHAR2:=null
   ,p_LOANDTLFUNDFEEDOLLRAMT VARCHAR2:=null
   ,p_LOANDTLCBNKNM VARCHAR2:=null
   ,p_LOANDTLCBNKMAILADRSTR1NM VARCHAR2:=null
   ,p_LOANDTLCBNKMAILADRSTR2NM VARCHAR2:=null
   ,p_LOANDTLCBNKMAILADRCTYNM VARCHAR2:=null
   ,p_LOANDTLCBNKMAILADRSTCD CHAR:=null
   ,p_LOANDTLCBNKMAILADRZIPCD CHAR:=null
   ,p_LOANDTLCBNKMAILADRZIP4CD CHAR:=null
   ,p_LOANDTLCACCTNM VARCHAR2:=null
   ,p_LOANDTLCOLOANDTLACCT VARCHAR2:=null
   ,p_LOANDTLCROUTSYM CHAR:=null
   ,p_LOANDTLCTRANSCD CHAR:=null
   ,p_LOANDTLCATTEN VARCHAR2:=null
   ,p_LOANDTLRBNKNM VARCHAR2:=null
   ,p_LOANDTLRBNKMAILADRSTR1NM VARCHAR2:=null
   ,p_LOANDTLRBNKMAILADRSTR2NM VARCHAR2:=null
   ,p_LOANDTLRBNKMAILADRCTYNM VARCHAR2:=null
   ,p_LOANDTLRBNKMAILADRSTCD CHAR:=null
   ,p_LOANDTLRBNKMAILADRZIPCD CHAR:=null
   ,p_LOANDTLRBNKMAILADRZIP4CD CHAR:=null
   ,p_LOANDTLRACCTNM VARCHAR2:=null
   ,p_LOANDTLROLOANDTLACCT VARCHAR2:=null
   ,p_LOANDTLRROUTSYM CHAR:=null
   ,p_LOANDTLTRANSCD CHAR:=null
   ,p_LOANDTLRATTEN VARCHAR2:=null
   ,p_LOANDTLPYMT1AMT VARCHAR2:=null
   ,p_LOANDTLPYMTDT1X CHAR:=null
   ,p_LOANDTLPRIN1AMT VARCHAR2:=null
   ,p_LOANDTLINT1AMT VARCHAR2:=null
   ,p_LOANDTLCSA1PCT VARCHAR2:=null
   ,p_LOANDTLCSADOLLR1AMT VARCHAR2:=null
   ,p_LOANDTLCDC1PCT VARCHAR2:=null
   ,p_LOANDTLCDCDOLLR1AMT VARCHAR2:=null
   ,p_LOANDTLSTMTNM VARCHAR2:=null
   ,p_LOANDTLCURCSAFEEAMT VARCHAR2:=null
   ,p_LOANDTLCURCDCFEEAMT VARCHAR2:=null
   ,p_LOANDTLCURRESRVAMT VARCHAR2:=null
   ,p_LOANDTLCURFEEBALAMT VARCHAR2:=null
   ,p_LOANDTLAMPRINLEFTAMT VARCHAR2:=null
   ,p_LOANDTLAMTHRUDTX CHAR:=null
   ,p_LOANDTLAPPIND CHAR:=null
   ,p_LOANDTLSPPIND CHAR:=null
   ,p_LOANDTLSPPLQDAMT VARCHAR2:=null
   ,p_LOANDTLSPPAUTOPYMTAMT VARCHAR2:=null
   ,p_LOANDTLSPPAUTOPCT VARCHAR2:=null
   ,p_LOANDTLSTMTINT1AMT VARCHAR2:=null
   ,p_LOANDTLSTMTINT2AMT VARCHAR2:=null
   ,p_LOANDTLSTMTINT3AMT VARCHAR2:=null
   ,p_LOANDTLSTMTINT4AMT VARCHAR2:=null
   ,p_LOANDTLSTMTINT5AMT VARCHAR2:=null
   ,p_LOANDTLSTMTINT6AMT VARCHAR2:=null
   ,p_LOANDTLSTMTINT7AMT VARCHAR2:=null
   ,p_LOANDTLSTMTINT8AMT VARCHAR2:=null
   ,p_LOANDTLSTMTINT9AMT VARCHAR2:=null
   ,p_LOANDTLSTMTINT10AMT VARCHAR2:=null
   ,p_LOANDTLSTMTINT11AMT VARCHAR2:=null
   ,p_LOANDTLSTMTINT12AMT VARCHAR2:=null
   ,p_LOANDTLSTMTINT13AMT VARCHAR2:=null
   ,p_LOANDTLSTMTAVGMOBALAMT VARCHAR2:=null
   ,p_LOANDTLAMPRINAMT VARCHAR2:=null
   ,p_LOANDTLAMINTAMT VARCHAR2:=null
   ,p_LOANDTLREFIIND CHAR:=null
   ,p_LOANDTLSETUPIND CHAR:=null
   ,p_LOANDTLCREATDT DATE:=null
   ,p_LOANDTLLMAINTNDT DATE:=null
   ,p_LOANDTLCONVDT DATE:=null
   ,p_LOANDTLPRGRM CHAR:=null
   ,p_LOANDTLNOTEMOPYMTAMT VARCHAR2:=null
   ,p_LOANDTLDBENTRMATDT DATE:=null
   ,p_LOANDTLNOTEMATDT DATE:=null
   ,p_LOANDTLRESRVDEPPCT VARCHAR2:=null
   ,p_LOANDTLCDCPFEEPCT VARCHAR2:=null
   ,p_LOANDTLCDCPFEEDOLLRAMT VARCHAR2:=null
   ,p_LOANDTLDUEBORRAMT VARCHAR2:=null
   ,p_LOANDTLAPPVDT DATE:=null
   ,p_LOANDTLLTFEEIND CHAR:=null
   ,p_LOANDTLACHPRENTIND CHAR:=null
   ,p_LOANDTLAMSCHDLTYP CHAR:=null
   ,p_LOANDTLACHBNKNM VARCHAR2:=null
   ,p_LOANDTLACHBNKMAILADRSTR1NM VARCHAR2:=null
   ,p_LOANDTLACHBNKMAILADRSTR2NM VARCHAR2:=null
   ,p_LOANDTLACHBNKMAILADRCTYNM VARCHAR2:=null
   ,p_LOANDTLACHBNKMAILADRSTCD CHAR:=null
   ,p_LOANDTLACHBNKMAILADRZIPCD CHAR:=null
   ,p_LOANDTLACHBNKMAILADRZIP4CD CHAR:=null
   ,p_LOANDTLACHBNKBR CHAR:=null
   ,p_LOANDTLACHBNKACTYP CHAR:=null
   ,p_LOANDTLACHBNKACCT VARCHAR2:=null
   ,p_LOANDTLACHBNKROUTNMB CHAR:=null
   ,p_LOANDTLACHBNKTRANS CHAR:=null
   ,p_LOANDTLACHBNKIDNOORATTEN VARCHAR2:=null
   ,p_LOANDTLACHDEPNM VARCHAR2:=null
   ,p_LOANDTLACHSIGNDT DATE:=null
   ,p_LOANDTLBORRNM2 VARCHAR2:=null
   ,p_LOANDTLSBCNM2 VARCHAR2:=null
   ,p_LOANDTLCACCTNMB VARCHAR2:=null
   ,p_LOANDTLRACCTNMB VARCHAR2:=null
   ,p_LOANDTLINTLENDRIND CHAR:=null
   ,p_LOANDTLINTRMLENDR VARCHAR2:=null
   ,p_LOANDTLPYMTRECV VARCHAR2:=null
   ,p_LOANDTLTRMYR VARCHAR2:=null
   ,p_LOANDTLLOANTYP CHAR:=null
   ,p_LOANDTLMLACCTNMB VARCHAR2:=null
   ,p_LOANDTLEXCESSDUEBORRAMT VARCHAR2:=null
   ,p_LOANDTL503SUBTYP CHAR:=null
   ,p_LOANDTLLPYMTDT DATE:=null
   ,p_LOANDTLPOOLSERS CHAR:=null
   ,p_LOANDTLSBAFEEPCT VARCHAR2:=null
   ,p_LOANDTLSBAPAIDTHRUDTX CHAR:=null
   ,p_LOANDTLSBAFEEAMT VARCHAR2:=null
   ,p_LOANDTLPRMAMT VARCHAR2:=null
   ,p_LOANDTLLENDRSBAFEEAMT VARCHAR2:=null
   ,p_LOANDTLWITHHELOANDTLIND CHAR:=null
   ,p_LOANDTLPRGRMTYP CHAR:=null
   ,p_LOANDTLLATEIND CHAR:=null
   ,p_LOANDTLCMNT1 VARCHAR2:=null
   ,p_LOANDTLCMNT2 VARCHAR2:=null
   ,p_LOANDTLSOD CHAR:=null
   ,p_LOANDTLTOBECURAMT VARCHAR2:=null
   ,p_LOANDTLGNTYREPAYPTDAMT VARCHAR2:=null
   ,p_LOANDTLCDCPAIDTHRUDTX CHAR:=null
   ,p_LOANDTLCSAPAIDTHRUDTX CHAR:=null
   ,p_LOANDTLINTPAIDTHRUDTX CHAR:=null
   ,p_LOANDTLSTRTBAL1AMT VARCHAR2:=null
   ,p_LOANDTLSTRTBAL2AMT VARCHAR2:=null
   ,p_LOANDTLSTRTBAL3AMT VARCHAR2:=null
   ,p_LOANDTLSTRTBAL4AMT VARCHAR2:=null
   ,p_LOANDTLSTRTBAL5AMT VARCHAR2:=null
   ,p_LOANDTLSBCIDNMB CHAR:=null
   ,p_LOANDTLACHLASTCHNGDT DATE:=null
   ,p_LOANDTLPRENOTETESTDT DATE:=null
   ,p_LOANDTLPYMTNMBDUE VARCHAR2:=null
   ,p_LOANDTLRESRVAMT VARCHAR2:=null
   ,p_LOANDTLFEEBASEAMT VARCHAR2:=null
   ,p_LOANDTLSTATCDOFLOAN CHAR:=null
   ,p_LOANDTLACTUALCSAINITFEEAMT VARCHAR2:=null
   ,p_LOANDTLSTATDT DATE:=null
   ,p_LOANDTLSTMTCLSBALAMT VARCHAR2:=null
   ,p_LOANDTLSTMTCLSDT DATE:=null
   ,p_LOANDTLLASTDBPAYOUTAMT VARCHAR2:=null
   ,p_LOANDTLCHEMICALBASISDAYS VARCHAR2:=null
) as
 /*
  Created on: 2018-11-07 11:36:05
  Created by: GENR
  Crerated from template stdtblins_template v3.05 13 Sep 2018 on 2018-11-07 11:36:05
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  Procedure performs INSERT on STGCSA.SOFVLND1TBLfor P_IDENTIFIER=0
*/
  rundate date;
  gn global_name.global_name%type;
  new_rec STGCSA.SOFVLND1TBL%rowtype;
  logged_msgid_retval number;
  audretval number;
  crossover_active char(1);
  l_LOANNMB char(10):=null;
  l_TRANSID number:=null;
  l_dbmdl varchar2(10):=null;
  l_entrydt date:=null;
  l_userid  varchar2(32):=null;
  l_usernm  varchar2(199):=null;
  l_timestampfld date:=null;
  cur_col_name varchar2(100):=null;
  maxsev number:=0;
  holdstring varchar2(4000);
  keystouse varchar2(1000);
  crossover_not_active char(1);
  -- vars for checking non-defaultable columns
  onespace varchar2(10):=' ';
  ndfltmsg varchar2(300);
  v_errfnd boolean:=false;
  -- std date for column defaulting
  JAN_1_1900  constant date:=to_date('JAN-01-1900','MON-DD-YYYY');
  cur_colvalue varchar2(4095);
  cur_colname varchar2(30);
-- Main body begins here
begin
  -- setup
  savepoint SOFVLND1INSTSP;
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  runtime.logger(logged_msgid_retval,'STDLOG',100,'Init SOFVLND1INSTSP',p_userid,4,
    logtxt1=>'SOFVLND1INSTSP',logtxt2=>'stdtblins_template v3.05 13 Sep 2018',
    PROGRAM_NAME=>'SOFVLND1INSTSP');
  select global_name, sysdate into gn, rundate from global_name;
  --
  -- Check to see if crossover is ongoing, in which case updates are prohibited
  SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
  IF crossover_not_active != 'Y' THEN
    p_errval := -20099;
    p_errmsg := 'Warning: Cannot update table SOFVLND1TBL at this time because crossover is running, please try later.';
  END IF;
  -- end of crossover_lockout_check
  begin
    cur_colname:='LOANNMB';
    cur_colvalue:=P_LOANNMB;
    l_LOANNMB:=P_LOANNMB;
  exception when others then
    p_errval:=sqlcode;
    p_errmsg:='Error '||sqlerrm(sqlcode)||' using '||cur_colname||' Value >'||cur_colvalue||'<';
  end;
  new_rec.creatuserid:=p_userid;
  new_rec.creatdt:=sysdate;
  new_rec.lastupdtuserid:=p_userid;
  new_rec.lastupdtdt:=sysdate;
  if p_identifier=0 then
    -- p_identifier=0 this is default for a stored procedure, only action for insert
    runtime.errfound:=false;
    runtime.errstr:=' ';
    --
    -- Default columns as applicable
  if p_errval=0 then
    begin
      cur_colname:='P_LOANNMB';
      cur_colvalue:=P_LOANNMB;
      if P_LOANNMB is null then
        raise_application_error(-20001,cur_colname||' may not be null');
      else
        new_rec.LOANNMB:=P_LOANNMB;
      end if;
      cur_colname:='P_LOANDTLSBAOFC';
      cur_colvalue:=P_LOANDTLSBAOFC;
      if P_LOANDTLSBAOFC is null then

        new_rec.LOANDTLSBAOFC:=rpad(' ',5);

      else
        new_rec.LOANDTLSBAOFC:=P_LOANDTLSBAOFC;
      end if;
      cur_colname:='P_LOANDTLBORRNM';
      cur_colvalue:=P_LOANDTLBORRNM;
      if P_LOANDTLBORRNM is null then

        new_rec.LOANDTLBORRNM:=' ';

      else
        new_rec.LOANDTLBORRNM:=P_LOANDTLBORRNM;
      end if;
      cur_colname:='P_LOANDTLBORRMAILADRSTR1NM';
      cur_colvalue:=P_LOANDTLBORRMAILADRSTR1NM;
      if P_LOANDTLBORRMAILADRSTR1NM is null then

        new_rec.LOANDTLBORRMAILADRSTR1NM:=' ';

      else
        new_rec.LOANDTLBORRMAILADRSTR1NM:=P_LOANDTLBORRMAILADRSTR1NM;
      end if;
      cur_colname:='P_LOANDTLBORRMAILADRSTR2NM';
      cur_colvalue:=P_LOANDTLBORRMAILADRSTR2NM;
      if P_LOANDTLBORRMAILADRSTR2NM is null then

        new_rec.LOANDTLBORRMAILADRSTR2NM:=' ';

      else
        new_rec.LOANDTLBORRMAILADRSTR2NM:=P_LOANDTLBORRMAILADRSTR2NM;
      end if;
      cur_colname:='P_LOANDTLBORRMAILADRCTYNM';
      cur_colvalue:=P_LOANDTLBORRMAILADRCTYNM;
      if P_LOANDTLBORRMAILADRCTYNM is null then

        new_rec.LOANDTLBORRMAILADRCTYNM:=' ';

      else
        new_rec.LOANDTLBORRMAILADRCTYNM:=P_LOANDTLBORRMAILADRCTYNM;
      end if;
      cur_colname:='P_LOANDTLBORRMAILADRSTCD';
      cur_colvalue:=P_LOANDTLBORRMAILADRSTCD;
      if P_LOANDTLBORRMAILADRSTCD is null then

        new_rec.LOANDTLBORRMAILADRSTCD:=rpad(' ',2);

      else
        new_rec.LOANDTLBORRMAILADRSTCD:=P_LOANDTLBORRMAILADRSTCD;
      end if;
      cur_colname:='P_LOANDTLBORRMAILADRZIPCD';
      cur_colvalue:=P_LOANDTLBORRMAILADRZIPCD;
      if P_LOANDTLBORRMAILADRZIPCD is null then

        new_rec.LOANDTLBORRMAILADRZIPCD:=rpad(' ',5);

      else
        new_rec.LOANDTLBORRMAILADRZIPCD:=P_LOANDTLBORRMAILADRZIPCD;
      end if;
      cur_colname:='P_LOANDTLBORRMAILADRZIP4CD';
      cur_colvalue:=P_LOANDTLBORRMAILADRZIP4CD;
      if P_LOANDTLBORRMAILADRZIP4CD is null then

        new_rec.LOANDTLBORRMAILADRZIP4CD:=rpad(' ',4);

      else
        new_rec.LOANDTLBORRMAILADRZIP4CD:=P_LOANDTLBORRMAILADRZIP4CD;
      end if;
      cur_colname:='P_LOANDTLSBCNM';
      cur_colvalue:=P_LOANDTLSBCNM;
      if P_LOANDTLSBCNM is null then

        new_rec.LOANDTLSBCNM:=' ';

      else
        new_rec.LOANDTLSBCNM:=P_LOANDTLSBCNM;
      end if;
      cur_colname:='P_LOANDTLSBCMAILADRSTR1NM';
      cur_colvalue:=P_LOANDTLSBCMAILADRSTR1NM;
      if P_LOANDTLSBCMAILADRSTR1NM is null then

        new_rec.LOANDTLSBCMAILADRSTR1NM:=' ';

      else
        new_rec.LOANDTLSBCMAILADRSTR1NM:=P_LOANDTLSBCMAILADRSTR1NM;
      end if;
      cur_colname:='P_LOANDTLSBCMAILADRSTR2NM';
      cur_colvalue:=P_LOANDTLSBCMAILADRSTR2NM;
      if P_LOANDTLSBCMAILADRSTR2NM is null then

        new_rec.LOANDTLSBCMAILADRSTR2NM:=' ';

      else
        new_rec.LOANDTLSBCMAILADRSTR2NM:=P_LOANDTLSBCMAILADRSTR2NM;
      end if;
      cur_colname:='P_LOANDTLSBCMAILADRCTYNM';
      cur_colvalue:=P_LOANDTLSBCMAILADRCTYNM;
      if P_LOANDTLSBCMAILADRCTYNM is null then

        new_rec.LOANDTLSBCMAILADRCTYNM:=' ';

      else
        new_rec.LOANDTLSBCMAILADRCTYNM:=P_LOANDTLSBCMAILADRCTYNM;
      end if;
      cur_colname:='P_LOANDTLSBCMAILADRSTCD';
      cur_colvalue:=P_LOANDTLSBCMAILADRSTCD;
      if P_LOANDTLSBCMAILADRSTCD is null then

        new_rec.LOANDTLSBCMAILADRSTCD:=rpad(' ',2);

      else
        new_rec.LOANDTLSBCMAILADRSTCD:=P_LOANDTLSBCMAILADRSTCD;
      end if;
      cur_colname:='P_LOANDTLSBCMAILADRZIPCD';
      cur_colvalue:=P_LOANDTLSBCMAILADRZIPCD;
      if P_LOANDTLSBCMAILADRZIPCD is null then

        new_rec.LOANDTLSBCMAILADRZIPCD:=rpad(' ',5);

      else
        new_rec.LOANDTLSBCMAILADRZIPCD:=P_LOANDTLSBCMAILADRZIPCD;
      end if;
      cur_colname:='P_LOANDTLSBCMAILADRZIP4CD';
      cur_colvalue:=P_LOANDTLSBCMAILADRZIP4CD;
      if P_LOANDTLSBCMAILADRZIP4CD is null then

        new_rec.LOANDTLSBCMAILADRZIP4CD:=rpad(' ',4);

      else
        new_rec.LOANDTLSBCMAILADRZIP4CD:=P_LOANDTLSBCMAILADRZIP4CD;
      end if;
      cur_colname:='P_LOANDTLTAXID';
      cur_colvalue:=P_LOANDTLTAXID;
      if P_LOANDTLTAXID is null then

        new_rec.LOANDTLTAXID:=rpad(' ',10);

      else
        new_rec.LOANDTLTAXID:=P_LOANDTLTAXID;
      end if;
      cur_colname:='P_LOANDTLCDCREGNCD';
      cur_colvalue:=P_LOANDTLCDCREGNCD;
      if P_LOANDTLCDCREGNCD is null then

        new_rec.LOANDTLCDCREGNCD:=rpad(' ',2);

      else
        new_rec.LOANDTLCDCREGNCD:=P_LOANDTLCDCREGNCD;
      end if;
      cur_colname:='P_LOANDTLCDCCERT';
      cur_colvalue:=P_LOANDTLCDCCERT;
      if P_LOANDTLCDCCERT is null then

        new_rec.LOANDTLCDCCERT:=rpad(' ',4);

      else
        new_rec.LOANDTLCDCCERT:=P_LOANDTLCDCCERT;
      end if;
      cur_colname:='P_LOANDTLDEFPYMT';
      cur_colvalue:=P_LOANDTLDEFPYMT;
      if P_LOANDTLDEFPYMT is null then

        new_rec.LOANDTLDEFPYMT:=rpad(' ',1);

      else
        new_rec.LOANDTLDEFPYMT:=P_LOANDTLDEFPYMT;
      end if;
      cur_colname:='P_LOANDTLDBENTRPRINAMT';
      cur_colvalue:=P_LOANDTLDBENTRPRINAMT;
      if P_LOANDTLDBENTRPRINAMT is null then

        new_rec.LOANDTLDBENTRPRINAMT:=0;

      else
        new_rec.LOANDTLDBENTRPRINAMT:=P_LOANDTLDBENTRPRINAMT;
      end if;
      cur_colname:='P_LOANDTLDBENTRCURBALAMT';
      cur_colvalue:=P_LOANDTLDBENTRCURBALAMT;
      if P_LOANDTLDBENTRCURBALAMT is null then

        new_rec.LOANDTLDBENTRCURBALAMT:=0;

      else
        new_rec.LOANDTLDBENTRCURBALAMT:=P_LOANDTLDBENTRCURBALAMT;
      end if;
      cur_colname:='P_LOANDTLDBENTRINTRTPCT';
      cur_colvalue:=P_LOANDTLDBENTRINTRTPCT;
      if P_LOANDTLDBENTRINTRTPCT is null then

        new_rec.LOANDTLDBENTRINTRTPCT:=0;

      else
        new_rec.LOANDTLDBENTRINTRTPCT:=P_LOANDTLDBENTRINTRTPCT;
      end if;
      cur_colname:='P_LOANDTLISSDT';
      cur_colvalue:=P_LOANDTLISSDT;
      if P_LOANDTLISSDT is null then

        new_rec.LOANDTLISSDT:=jan_1_1900;

      else
        new_rec.LOANDTLISSDT:=P_LOANDTLISSDT;
      end if;
      cur_colname:='P_LOANDTLDEFPYMTDTX';
      cur_colvalue:=P_LOANDTLDEFPYMTDTX;
      if P_LOANDTLDEFPYMTDTX is null then

        new_rec.LOANDTLDEFPYMTDTX:=rpad(' ',8);

      else
        new_rec.LOANDTLDEFPYMTDTX:=P_LOANDTLDEFPYMTDTX;
      end if;
      cur_colname:='P_LOANDTLDBENTRPYMTDTTRM';
      cur_colvalue:=P_LOANDTLDBENTRPYMTDTTRM;
      if P_LOANDTLDBENTRPYMTDTTRM is null then

        new_rec.LOANDTLDBENTRPYMTDTTRM:=rpad(' ',15);

      else
        new_rec.LOANDTLDBENTRPYMTDTTRM:=P_LOANDTLDBENTRPYMTDTTRM;
      end if;
      cur_colname:='P_LOANDTLDBENTRPROCDSAMT';
      cur_colvalue:=P_LOANDTLDBENTRPROCDSAMT;
      if P_LOANDTLDBENTRPROCDSAMT is null then

        new_rec.LOANDTLDBENTRPROCDSAMT:=0;

      else
        new_rec.LOANDTLDBENTRPROCDSAMT:=P_LOANDTLDBENTRPROCDSAMT;
      end if;
      cur_colname:='P_LOANDTLDBENTRPROCDSESCROWAMT';
      cur_colvalue:=P_LOANDTLDBENTRPROCDSESCROWAMT;
      if P_LOANDTLDBENTRPROCDSESCROWAMT is null then

        new_rec.LOANDTLDBENTRPROCDSESCROWAMT:=0;

      else
        new_rec.LOANDTLDBENTRPROCDSESCROWAMT:=P_LOANDTLDBENTRPROCDSESCROWAMT;
      end if;
      cur_colname:='P_LOANDTLSEMIANNPYMTAMT';
      cur_colvalue:=P_LOANDTLSEMIANNPYMTAMT;
      if P_LOANDTLSEMIANNPYMTAMT is null then

        new_rec.LOANDTLSEMIANNPYMTAMT:=0;

      else
        new_rec.LOANDTLSEMIANNPYMTAMT:=P_LOANDTLSEMIANNPYMTAMT;
      end if;
      cur_colname:='P_LOANDTLNOTEPRINAMT';
      cur_colvalue:=P_LOANDTLNOTEPRINAMT;
      if P_LOANDTLNOTEPRINAMT is null then

        new_rec.LOANDTLNOTEPRINAMT:=0;

      else
        new_rec.LOANDTLNOTEPRINAMT:=P_LOANDTLNOTEPRINAMT;
      end if;
      cur_colname:='P_LOANDTLNOTECURBALAMT';
      cur_colvalue:=P_LOANDTLNOTECURBALAMT;
      if P_LOANDTLNOTECURBALAMT is null then

        new_rec.LOANDTLNOTECURBALAMT:=0;

      else
        new_rec.LOANDTLNOTECURBALAMT:=P_LOANDTLNOTECURBALAMT;
      end if;
      cur_colname:='P_LOANDTLNOTEINTRTPCT';
      cur_colvalue:=P_LOANDTLNOTEINTRTPCT;
      if P_LOANDTLNOTEINTRTPCT is null then

        new_rec.LOANDTLNOTEINTRTPCT:=0;

      else
        new_rec.LOANDTLNOTEINTRTPCT:=P_LOANDTLNOTEINTRTPCT;
      end if;
      cur_colname:='P_LOANDTLNOTEDT';
      cur_colvalue:=P_LOANDTLNOTEDT;
      if P_LOANDTLNOTEDT is null then

        new_rec.LOANDTLNOTEDT:=jan_1_1900;

      else
        new_rec.LOANDTLNOTEDT:=P_LOANDTLNOTEDT;
      end if;
      cur_colname:='P_LOANDTLACCTCD';
      cur_colvalue:=P_LOANDTLACCTCD;
      if P_LOANDTLACCTCD is null then

        new_rec.LOANDTLACCTCD:=0;

      else
        new_rec.LOANDTLACCTCD:=P_LOANDTLACCTCD;
      end if;
      cur_colname:='P_LOANDTLACCTCHNGCD';
      cur_colvalue:=P_LOANDTLACCTCHNGCD;
      if P_LOANDTLACCTCHNGCD is null then

        new_rec.LOANDTLACCTCHNGCD:=rpad(' ',3);

      else
        new_rec.LOANDTLACCTCHNGCD:=P_LOANDTLACCTCHNGCD;
      end if;
      cur_colname:='P_LOANDTLREAMIND';
      cur_colvalue:=P_LOANDTLREAMIND;
      if P_LOANDTLREAMIND is null then

        new_rec.LOANDTLREAMIND:=rpad(' ',1);

      else
        new_rec.LOANDTLREAMIND:=P_LOANDTLREAMIND;
      end if;
      cur_colname:='P_LOANDTLNOTEPYMTDTTRM';
      cur_colvalue:=P_LOANDTLNOTEPYMTDTTRM;
      if P_LOANDTLNOTEPYMTDTTRM is null then

        new_rec.LOANDTLNOTEPYMTDTTRM:=rpad(' ',15);

      else
        new_rec.LOANDTLNOTEPYMTDTTRM:=P_LOANDTLNOTEPYMTDTTRM;
      end if;
      cur_colname:='P_LOANDTLRESRVDEPAMT';
      cur_colvalue:=P_LOANDTLRESRVDEPAMT;
      if P_LOANDTLRESRVDEPAMT is null then

        new_rec.LOANDTLRESRVDEPAMT:=0;

      else
        new_rec.LOANDTLRESRVDEPAMT:=P_LOANDTLRESRVDEPAMT;
      end if;
      cur_colname:='P_LOANDTLUNDRWTRFEEPCT';
      cur_colvalue:=P_LOANDTLUNDRWTRFEEPCT;
      if P_LOANDTLUNDRWTRFEEPCT is null then

        new_rec.LOANDTLUNDRWTRFEEPCT:=0;

      else
        new_rec.LOANDTLUNDRWTRFEEPCT:=P_LOANDTLUNDRWTRFEEPCT;
      end if;
      cur_colname:='P_LOANDTLUNDRWTRFEEMTDAMT';
      cur_colvalue:=P_LOANDTLUNDRWTRFEEMTDAMT;
      if P_LOANDTLUNDRWTRFEEMTDAMT is null then

        new_rec.LOANDTLUNDRWTRFEEMTDAMT:=0;

      else
        new_rec.LOANDTLUNDRWTRFEEMTDAMT:=P_LOANDTLUNDRWTRFEEMTDAMT;
      end if;
      cur_colname:='P_LOANDTLUNDRWTRFEEYTDAMT';
      cur_colvalue:=P_LOANDTLUNDRWTRFEEYTDAMT;
      if P_LOANDTLUNDRWTRFEEYTDAMT is null then

        new_rec.LOANDTLUNDRWTRFEEYTDAMT:=0;

      else
        new_rec.LOANDTLUNDRWTRFEEYTDAMT:=P_LOANDTLUNDRWTRFEEYTDAMT;
      end if;
      cur_colname:='P_LOANDTLUNDRWTRFEEPTDAMT';
      cur_colvalue:=P_LOANDTLUNDRWTRFEEPTDAMT;
      if P_LOANDTLUNDRWTRFEEPTDAMT is null then

        new_rec.LOANDTLUNDRWTRFEEPTDAMT:=0;

      else
        new_rec.LOANDTLUNDRWTRFEEPTDAMT:=P_LOANDTLUNDRWTRFEEPTDAMT;
      end if;
      cur_colname:='P_LOANDTLCDCFEEPCT';
      cur_colvalue:=P_LOANDTLCDCFEEPCT;
      if P_LOANDTLCDCFEEPCT is null then

        new_rec.LOANDTLCDCFEEPCT:=0;

      else
        new_rec.LOANDTLCDCFEEPCT:=P_LOANDTLCDCFEEPCT;
      end if;
      cur_colname:='P_LOANDTLCDCFEEMTDAMT';
      cur_colvalue:=P_LOANDTLCDCFEEMTDAMT;
      if P_LOANDTLCDCFEEMTDAMT is null then

        new_rec.LOANDTLCDCFEEMTDAMT:=0;

      else
        new_rec.LOANDTLCDCFEEMTDAMT:=P_LOANDTLCDCFEEMTDAMT;
      end if;
      cur_colname:='P_LOANDTLCDCFEEYTDAMT';
      cur_colvalue:=P_LOANDTLCDCFEEYTDAMT;
      if P_LOANDTLCDCFEEYTDAMT is null then

        new_rec.LOANDTLCDCFEEYTDAMT:=0;

      else
        new_rec.LOANDTLCDCFEEYTDAMT:=P_LOANDTLCDCFEEYTDAMT;
      end if;
      cur_colname:='P_LOANDTLCDCFEEPTDAMT';
      cur_colvalue:=P_LOANDTLCDCFEEPTDAMT;
      if P_LOANDTLCDCFEEPTDAMT is null then

        new_rec.LOANDTLCDCFEEPTDAMT:=0;

      else
        new_rec.LOANDTLCDCFEEPTDAMT:=P_LOANDTLCDCFEEPTDAMT;
      end if;
      cur_colname:='P_LOANDTLCSAFEEPCT';
      cur_colvalue:=P_LOANDTLCSAFEEPCT;
      if P_LOANDTLCSAFEEPCT is null then

        new_rec.LOANDTLCSAFEEPCT:=0;

      else
        new_rec.LOANDTLCSAFEEPCT:=P_LOANDTLCSAFEEPCT;
      end if;
      cur_colname:='P_LOANDTLCSAFEEMTDAMT';
      cur_colvalue:=P_LOANDTLCSAFEEMTDAMT;
      if P_LOANDTLCSAFEEMTDAMT is null then

        new_rec.LOANDTLCSAFEEMTDAMT:=0;

      else
        new_rec.LOANDTLCSAFEEMTDAMT:=P_LOANDTLCSAFEEMTDAMT;
      end if;
      cur_colname:='P_LOANDTLCSAFEEYTDAMT';
      cur_colvalue:=P_LOANDTLCSAFEEYTDAMT;
      if P_LOANDTLCSAFEEYTDAMT is null then

        new_rec.LOANDTLCSAFEEYTDAMT:=0;

      else
        new_rec.LOANDTLCSAFEEYTDAMT:=P_LOANDTLCSAFEEYTDAMT;
      end if;
      cur_colname:='P_LOANDTLCSAFEEPTDAMT';
      cur_colvalue:=P_LOANDTLCSAFEEPTDAMT;
      if P_LOANDTLCSAFEEPTDAMT is null then

        new_rec.LOANDTLCSAFEEPTDAMT:=0;

      else
        new_rec.LOANDTLCSAFEEPTDAMT:=P_LOANDTLCSAFEEPTDAMT;
      end if;
      cur_colname:='P_LOANDTLATTORNEYFEEAMT';
      cur_colvalue:=P_LOANDTLATTORNEYFEEAMT;
      if P_LOANDTLATTORNEYFEEAMT is null then

        new_rec.LOANDTLATTORNEYFEEAMT:=0;

      else
        new_rec.LOANDTLATTORNEYFEEAMT:=P_LOANDTLATTORNEYFEEAMT;
      end if;
      cur_colname:='P_LOANDTLINITFEEPCT';
      cur_colvalue:=P_LOANDTLINITFEEPCT;
      if P_LOANDTLINITFEEPCT is null then

        new_rec.LOANDTLINITFEEPCT:=0;

      else
        new_rec.LOANDTLINITFEEPCT:=P_LOANDTLINITFEEPCT;
      end if;
      cur_colname:='P_LOANDTLINITFEEDOLLRAMT';
      cur_colvalue:=P_LOANDTLINITFEEDOLLRAMT;
      if P_LOANDTLINITFEEDOLLRAMT is null then

        new_rec.LOANDTLINITFEEDOLLRAMT:=0;

      else
        new_rec.LOANDTLINITFEEDOLLRAMT:=P_LOANDTLINITFEEDOLLRAMT;
      end if;
      cur_colname:='P_LOANDTLFUNDFEEPCT';
      cur_colvalue:=P_LOANDTLFUNDFEEPCT;
      if P_LOANDTLFUNDFEEPCT is null then

        new_rec.LOANDTLFUNDFEEPCT:=0;

      else
        new_rec.LOANDTLFUNDFEEPCT:=P_LOANDTLFUNDFEEPCT;
      end if;
      cur_colname:='P_LOANDTLFUNDFEEDOLLRAMT';
      cur_colvalue:=P_LOANDTLFUNDFEEDOLLRAMT;
      if P_LOANDTLFUNDFEEDOLLRAMT is null then

        new_rec.LOANDTLFUNDFEEDOLLRAMT:=0;

      else
        new_rec.LOANDTLFUNDFEEDOLLRAMT:=P_LOANDTLFUNDFEEDOLLRAMT;
      end if;
      cur_colname:='P_LOANDTLCBNKNM';
      cur_colvalue:=P_LOANDTLCBNKNM;
      if P_LOANDTLCBNKNM is null then

        new_rec.LOANDTLCBNKNM:=' ';

      else
        new_rec.LOANDTLCBNKNM:=P_LOANDTLCBNKNM;
      end if;
      cur_colname:='P_LOANDTLCBNKMAILADRSTR1NM';
      cur_colvalue:=P_LOANDTLCBNKMAILADRSTR1NM;
      if P_LOANDTLCBNKMAILADRSTR1NM is null then

        new_rec.LOANDTLCBNKMAILADRSTR1NM:=' ';

      else
        new_rec.LOANDTLCBNKMAILADRSTR1NM:=P_LOANDTLCBNKMAILADRSTR1NM;
      end if;
      cur_colname:='P_LOANDTLCBNKMAILADRSTR2NM';
      cur_colvalue:=P_LOANDTLCBNKMAILADRSTR2NM;
      if P_LOANDTLCBNKMAILADRSTR2NM is null then

        new_rec.LOANDTLCBNKMAILADRSTR2NM:=' ';

      else
        new_rec.LOANDTLCBNKMAILADRSTR2NM:=P_LOANDTLCBNKMAILADRSTR2NM;
      end if;
      cur_colname:='P_LOANDTLCBNKMAILADRCTYNM';
      cur_colvalue:=P_LOANDTLCBNKMAILADRCTYNM;
      if P_LOANDTLCBNKMAILADRCTYNM is null then

        new_rec.LOANDTLCBNKMAILADRCTYNM:=' ';

      else
        new_rec.LOANDTLCBNKMAILADRCTYNM:=P_LOANDTLCBNKMAILADRCTYNM;
      end if;
      cur_colname:='P_LOANDTLCBNKMAILADRSTCD';
      cur_colvalue:=P_LOANDTLCBNKMAILADRSTCD;
      if P_LOANDTLCBNKMAILADRSTCD is null then

        new_rec.LOANDTLCBNKMAILADRSTCD:=rpad(' ',2);

      else
        new_rec.LOANDTLCBNKMAILADRSTCD:=P_LOANDTLCBNKMAILADRSTCD;
      end if;
      cur_colname:='P_LOANDTLCBNKMAILADRZIPCD';
      cur_colvalue:=P_LOANDTLCBNKMAILADRZIPCD;
      if P_LOANDTLCBNKMAILADRZIPCD is null then

        new_rec.LOANDTLCBNKMAILADRZIPCD:=rpad(' ',5);

      else
        new_rec.LOANDTLCBNKMAILADRZIPCD:=P_LOANDTLCBNKMAILADRZIPCD;
      end if;
      cur_colname:='P_LOANDTLCBNKMAILADRZIP4CD';
      cur_colvalue:=P_LOANDTLCBNKMAILADRZIP4CD;
      if P_LOANDTLCBNKMAILADRZIP4CD is null then

        new_rec.LOANDTLCBNKMAILADRZIP4CD:=rpad(' ',4);

      else
        new_rec.LOANDTLCBNKMAILADRZIP4CD:=P_LOANDTLCBNKMAILADRZIP4CD;
      end if;
      cur_colname:='P_LOANDTLCACCTNM';
      cur_colvalue:=P_LOANDTLCACCTNM;
      if P_LOANDTLCACCTNM is null then

        new_rec.LOANDTLCACCTNM:=' ';

      else
        new_rec.LOANDTLCACCTNM:=P_LOANDTLCACCTNM;
      end if;
      cur_colname:='P_LOANDTLCOLOANDTLACCT';
      cur_colvalue:=P_LOANDTLCOLOANDTLACCT;
      if P_LOANDTLCOLOANDTLACCT is null then

        new_rec.LOANDTLCOLOANDTLACCT:=0;

      else
        new_rec.LOANDTLCOLOANDTLACCT:=P_LOANDTLCOLOANDTLACCT;
      end if;
      cur_colname:='P_LOANDTLCROUTSYM';
      cur_colvalue:=P_LOANDTLCROUTSYM;
      if P_LOANDTLCROUTSYM is null then

        new_rec.LOANDTLCROUTSYM:=rpad(' ',15);

      else
        new_rec.LOANDTLCROUTSYM:=P_LOANDTLCROUTSYM;
      end if;
      cur_colname:='P_LOANDTLCTRANSCD';
      cur_colvalue:=P_LOANDTLCTRANSCD;
      if P_LOANDTLCTRANSCD is null then

        new_rec.LOANDTLCTRANSCD:=rpad(' ',15);

      else
        new_rec.LOANDTLCTRANSCD:=P_LOANDTLCTRANSCD;
      end if;
      cur_colname:='P_LOANDTLCATTEN';
      cur_colvalue:=P_LOANDTLCATTEN;
      if P_LOANDTLCATTEN is null then

        new_rec.LOANDTLCATTEN:=' ';

      else
        new_rec.LOANDTLCATTEN:=P_LOANDTLCATTEN;
      end if;
      cur_colname:='P_LOANDTLRBNKNM';
      cur_colvalue:=P_LOANDTLRBNKNM;
      if P_LOANDTLRBNKNM is null then

        new_rec.LOANDTLRBNKNM:=' ';

      else
        new_rec.LOANDTLRBNKNM:=P_LOANDTLRBNKNM;
      end if;
      cur_colname:='P_LOANDTLRBNKMAILADRSTR1NM';
      cur_colvalue:=P_LOANDTLRBNKMAILADRSTR1NM;
      if P_LOANDTLRBNKMAILADRSTR1NM is null then

        new_rec.LOANDTLRBNKMAILADRSTR1NM:=' ';

      else
        new_rec.LOANDTLRBNKMAILADRSTR1NM:=P_LOANDTLRBNKMAILADRSTR1NM;
      end if;
      cur_colname:='P_LOANDTLRBNKMAILADRSTR2NM';
      cur_colvalue:=P_LOANDTLRBNKMAILADRSTR2NM;
      if P_LOANDTLRBNKMAILADRSTR2NM is null then

        new_rec.LOANDTLRBNKMAILADRSTR2NM:=' ';

      else
        new_rec.LOANDTLRBNKMAILADRSTR2NM:=P_LOANDTLRBNKMAILADRSTR2NM;
      end if;
      cur_colname:='P_LOANDTLRBNKMAILADRCTYNM';
      cur_colvalue:=P_LOANDTLRBNKMAILADRCTYNM;
      if P_LOANDTLRBNKMAILADRCTYNM is null then

        new_rec.LOANDTLRBNKMAILADRCTYNM:=' ';

      else
        new_rec.LOANDTLRBNKMAILADRCTYNM:=P_LOANDTLRBNKMAILADRCTYNM;
      end if;
      cur_colname:='P_LOANDTLRBNKMAILADRSTCD';
      cur_colvalue:=P_LOANDTLRBNKMAILADRSTCD;
      if P_LOANDTLRBNKMAILADRSTCD is null then

        new_rec.LOANDTLRBNKMAILADRSTCD:=rpad(' ',2);

      else
        new_rec.LOANDTLRBNKMAILADRSTCD:=P_LOANDTLRBNKMAILADRSTCD;
      end if;
      cur_colname:='P_LOANDTLRBNKMAILADRZIPCD';
      cur_colvalue:=P_LOANDTLRBNKMAILADRZIPCD;
      if P_LOANDTLRBNKMAILADRZIPCD is null then

        new_rec.LOANDTLRBNKMAILADRZIPCD:=rpad(' ',5);

      else
        new_rec.LOANDTLRBNKMAILADRZIPCD:=P_LOANDTLRBNKMAILADRZIPCD;
      end if;
      cur_colname:='P_LOANDTLRBNKMAILADRZIP4CD';
      cur_colvalue:=P_LOANDTLRBNKMAILADRZIP4CD;
      if P_LOANDTLRBNKMAILADRZIP4CD is null then

        new_rec.LOANDTLRBNKMAILADRZIP4CD:=rpad(' ',4);

      else
        new_rec.LOANDTLRBNKMAILADRZIP4CD:=P_LOANDTLRBNKMAILADRZIP4CD;
      end if;
      cur_colname:='P_LOANDTLRACCTNM';
      cur_colvalue:=P_LOANDTLRACCTNM;
      if P_LOANDTLRACCTNM is null then

        new_rec.LOANDTLRACCTNM:=' ';

      else
        new_rec.LOANDTLRACCTNM:=P_LOANDTLRACCTNM;
      end if;
      cur_colname:='P_LOANDTLROLOANDTLACCT';
      cur_colvalue:=P_LOANDTLROLOANDTLACCT;
      if P_LOANDTLROLOANDTLACCT is null then

        new_rec.LOANDTLROLOANDTLACCT:=0;

      else
        new_rec.LOANDTLROLOANDTLACCT:=P_LOANDTLROLOANDTLACCT;
      end if;
      cur_colname:='P_LOANDTLRROUTSYM';
      cur_colvalue:=P_LOANDTLRROUTSYM;
      if P_LOANDTLRROUTSYM is null then

        new_rec.LOANDTLRROUTSYM:=rpad(' ',15);

      else
        new_rec.LOANDTLRROUTSYM:=P_LOANDTLRROUTSYM;
      end if;
      cur_colname:='P_LOANDTLTRANSCD';
      cur_colvalue:=P_LOANDTLTRANSCD;
      if P_LOANDTLTRANSCD is null then

        new_rec.LOANDTLTRANSCD:=rpad(' ',15);

      else
        new_rec.LOANDTLTRANSCD:=P_LOANDTLTRANSCD;
      end if;
      cur_colname:='P_LOANDTLRATTEN';
      cur_colvalue:=P_LOANDTLRATTEN;
      if P_LOANDTLRATTEN is null then

        new_rec.LOANDTLRATTEN:=' ';

      else
        new_rec.LOANDTLRATTEN:=P_LOANDTLRATTEN;
      end if;
      cur_colname:='P_LOANDTLPYMT1AMT';
      cur_colvalue:=P_LOANDTLPYMT1AMT;
      if P_LOANDTLPYMT1AMT is null then

        new_rec.LOANDTLPYMT1AMT:=0;

      else
        new_rec.LOANDTLPYMT1AMT:=P_LOANDTLPYMT1AMT;
      end if;
      cur_colname:='P_LOANDTLPYMTDT1X';
      cur_colvalue:=P_LOANDTLPYMTDT1X;
      if P_LOANDTLPYMTDT1X is null then

        new_rec.LOANDTLPYMTDT1X:=rpad(' ',8);

      else
        new_rec.LOANDTLPYMTDT1X:=P_LOANDTLPYMTDT1X;
      end if;
      cur_colname:='P_LOANDTLPRIN1AMT';
      cur_colvalue:=P_LOANDTLPRIN1AMT;
      if P_LOANDTLPRIN1AMT is null then

        new_rec.LOANDTLPRIN1AMT:=0;

      else
        new_rec.LOANDTLPRIN1AMT:=P_LOANDTLPRIN1AMT;
      end if;
      cur_colname:='P_LOANDTLINT1AMT';
      cur_colvalue:=P_LOANDTLINT1AMT;
      if P_LOANDTLINT1AMT is null then

        new_rec.LOANDTLINT1AMT:=0;

      else
        new_rec.LOANDTLINT1AMT:=P_LOANDTLINT1AMT;
      end if;
      cur_colname:='P_LOANDTLCSA1PCT';
      cur_colvalue:=P_LOANDTLCSA1PCT;
      if P_LOANDTLCSA1PCT is null then

        new_rec.LOANDTLCSA1PCT:=0;

      else
        new_rec.LOANDTLCSA1PCT:=P_LOANDTLCSA1PCT;
      end if;
      cur_colname:='P_LOANDTLCSADOLLR1AMT';
      cur_colvalue:=P_LOANDTLCSADOLLR1AMT;
      if P_LOANDTLCSADOLLR1AMT is null then

        new_rec.LOANDTLCSADOLLR1AMT:=0;

      else
        new_rec.LOANDTLCSADOLLR1AMT:=P_LOANDTLCSADOLLR1AMT;
      end if;
      cur_colname:='P_LOANDTLCDC1PCT';
      cur_colvalue:=P_LOANDTLCDC1PCT;
      if P_LOANDTLCDC1PCT is null then

        new_rec.LOANDTLCDC1PCT:=0;

      else
        new_rec.LOANDTLCDC1PCT:=P_LOANDTLCDC1PCT;
      end if;
      cur_colname:='P_LOANDTLCDCDOLLR1AMT';
      cur_colvalue:=P_LOANDTLCDCDOLLR1AMT;
      if P_LOANDTLCDCDOLLR1AMT is null then

        new_rec.LOANDTLCDCDOLLR1AMT:=0;

      else
        new_rec.LOANDTLCDCDOLLR1AMT:=P_LOANDTLCDCDOLLR1AMT;
      end if;
      cur_colname:='P_LOANDTLSTMTNM';
      cur_colvalue:=P_LOANDTLSTMTNM;
      if P_LOANDTLSTMTNM is null then

        new_rec.LOANDTLSTMTNM:=' ';

      else
        new_rec.LOANDTLSTMTNM:=P_LOANDTLSTMTNM;
      end if;
      cur_colname:='P_LOANDTLCURCSAFEEAMT';
      cur_colvalue:=P_LOANDTLCURCSAFEEAMT;
      if P_LOANDTLCURCSAFEEAMT is null then

        new_rec.LOANDTLCURCSAFEEAMT:=0;

      else
        new_rec.LOANDTLCURCSAFEEAMT:=P_LOANDTLCURCSAFEEAMT;
      end if;
      cur_colname:='P_LOANDTLCURCDCFEEAMT';
      cur_colvalue:=P_LOANDTLCURCDCFEEAMT;
      if P_LOANDTLCURCDCFEEAMT is null then

        new_rec.LOANDTLCURCDCFEEAMT:=0;

      else
        new_rec.LOANDTLCURCDCFEEAMT:=P_LOANDTLCURCDCFEEAMT;
      end if;
      cur_colname:='P_LOANDTLCURRESRVAMT';
      cur_colvalue:=P_LOANDTLCURRESRVAMT;
      if P_LOANDTLCURRESRVAMT is null then

        new_rec.LOANDTLCURRESRVAMT:=0;

      else
        new_rec.LOANDTLCURRESRVAMT:=P_LOANDTLCURRESRVAMT;
      end if;
      cur_colname:='P_LOANDTLCURFEEBALAMT';
      cur_colvalue:=P_LOANDTLCURFEEBALAMT;
      if P_LOANDTLCURFEEBALAMT is null then

        new_rec.LOANDTLCURFEEBALAMT:=0;

      else
        new_rec.LOANDTLCURFEEBALAMT:=P_LOANDTLCURFEEBALAMT;
      end if;
      cur_colname:='P_LOANDTLAMPRINLEFTAMT';
      cur_colvalue:=P_LOANDTLAMPRINLEFTAMT;
      if P_LOANDTLAMPRINLEFTAMT is null then

        new_rec.LOANDTLAMPRINLEFTAMT:=0;

      else
        new_rec.LOANDTLAMPRINLEFTAMT:=P_LOANDTLAMPRINLEFTAMT;
      end if;
      cur_colname:='P_LOANDTLAMTHRUDTX';
      cur_colvalue:=P_LOANDTLAMTHRUDTX;
      if P_LOANDTLAMTHRUDTX is null then

        new_rec.LOANDTLAMTHRUDTX:=rpad(' ',8);

      else
        new_rec.LOANDTLAMTHRUDTX:=P_LOANDTLAMTHRUDTX;
      end if;
      cur_colname:='P_LOANDTLAPPIND';
      cur_colvalue:=P_LOANDTLAPPIND;
      if P_LOANDTLAPPIND is null then

        new_rec.LOANDTLAPPIND:=rpad(' ',1);

      else
        new_rec.LOANDTLAPPIND:=P_LOANDTLAPPIND;
      end if;
      cur_colname:='P_LOANDTLSPPIND';
      cur_colvalue:=P_LOANDTLSPPIND;
      if P_LOANDTLSPPIND is null then

        new_rec.LOANDTLSPPIND:=rpad(' ',1);

      else
        new_rec.LOANDTLSPPIND:=P_LOANDTLSPPIND;
      end if;
      cur_colname:='P_LOANDTLSPPLQDAMT';
      cur_colvalue:=P_LOANDTLSPPLQDAMT;
      if P_LOANDTLSPPLQDAMT is null then

        new_rec.LOANDTLSPPLQDAMT:=0;

      else
        new_rec.LOANDTLSPPLQDAMT:=P_LOANDTLSPPLQDAMT;
      end if;
      cur_colname:='P_LOANDTLSPPAUTOPYMTAMT';
      cur_colvalue:=P_LOANDTLSPPAUTOPYMTAMT;
      if P_LOANDTLSPPAUTOPYMTAMT is null then

        new_rec.LOANDTLSPPAUTOPYMTAMT:=0;

      else
        new_rec.LOANDTLSPPAUTOPYMTAMT:=P_LOANDTLSPPAUTOPYMTAMT;
      end if;
      cur_colname:='P_LOANDTLSPPAUTOPCT';
      cur_colvalue:=P_LOANDTLSPPAUTOPCT;
      if P_LOANDTLSPPAUTOPCT is null then

        new_rec.LOANDTLSPPAUTOPCT:=0;

      else
        new_rec.LOANDTLSPPAUTOPCT:=P_LOANDTLSPPAUTOPCT;
      end if;
      cur_colname:='P_LOANDTLSTMTINT1AMT';
      cur_colvalue:=P_LOANDTLSTMTINT1AMT;
      if P_LOANDTLSTMTINT1AMT is null then

        new_rec.LOANDTLSTMTINT1AMT:=0;

      else
        new_rec.LOANDTLSTMTINT1AMT:=P_LOANDTLSTMTINT1AMT;
      end if;
      cur_colname:='P_LOANDTLSTMTINT2AMT';
      cur_colvalue:=P_LOANDTLSTMTINT2AMT;
      if P_LOANDTLSTMTINT2AMT is null then

        new_rec.LOANDTLSTMTINT2AMT:=0;

      else
        new_rec.LOANDTLSTMTINT2AMT:=P_LOANDTLSTMTINT2AMT;
      end if;
      cur_colname:='P_LOANDTLSTMTINT3AMT';
      cur_colvalue:=P_LOANDTLSTMTINT3AMT;
      if P_LOANDTLSTMTINT3AMT is null then

        new_rec.LOANDTLSTMTINT3AMT:=0;

      else
        new_rec.LOANDTLSTMTINT3AMT:=P_LOANDTLSTMTINT3AMT;
      end if;
      cur_colname:='P_LOANDTLSTMTINT4AMT';
      cur_colvalue:=P_LOANDTLSTMTINT4AMT;
      if P_LOANDTLSTMTINT4AMT is null then

        new_rec.LOANDTLSTMTINT4AMT:=0;

      else
        new_rec.LOANDTLSTMTINT4AMT:=P_LOANDTLSTMTINT4AMT;
      end if;
      cur_colname:='P_LOANDTLSTMTINT5AMT';
      cur_colvalue:=P_LOANDTLSTMTINT5AMT;
      if P_LOANDTLSTMTINT5AMT is null then

        new_rec.LOANDTLSTMTINT5AMT:=0;

      else
        new_rec.LOANDTLSTMTINT5AMT:=P_LOANDTLSTMTINT5AMT;
      end if;
      cur_colname:='P_LOANDTLSTMTINT6AMT';
      cur_colvalue:=P_LOANDTLSTMTINT6AMT;
      if P_LOANDTLSTMTINT6AMT is null then

        new_rec.LOANDTLSTMTINT6AMT:=0;

      else
        new_rec.LOANDTLSTMTINT6AMT:=P_LOANDTLSTMTINT6AMT;
      end if;
      cur_colname:='P_LOANDTLSTMTINT7AMT';
      cur_colvalue:=P_LOANDTLSTMTINT7AMT;
      if P_LOANDTLSTMTINT7AMT is null then

        new_rec.LOANDTLSTMTINT7AMT:=0;

      else
        new_rec.LOANDTLSTMTINT7AMT:=P_LOANDTLSTMTINT7AMT;
      end if;
      cur_colname:='P_LOANDTLSTMTINT8AMT';
      cur_colvalue:=P_LOANDTLSTMTINT8AMT;
      if P_LOANDTLSTMTINT8AMT is null then

        new_rec.LOANDTLSTMTINT8AMT:=0;

      else
        new_rec.LOANDTLSTMTINT8AMT:=P_LOANDTLSTMTINT8AMT;
      end if;
      cur_colname:='P_LOANDTLSTMTINT9AMT';
      cur_colvalue:=P_LOANDTLSTMTINT9AMT;
      if P_LOANDTLSTMTINT9AMT is null then

        new_rec.LOANDTLSTMTINT9AMT:=0;

      else
        new_rec.LOANDTLSTMTINT9AMT:=P_LOANDTLSTMTINT9AMT;
      end if;
      cur_colname:='P_LOANDTLSTMTINT10AMT';
      cur_colvalue:=P_LOANDTLSTMTINT10AMT;
      if P_LOANDTLSTMTINT10AMT is null then

        new_rec.LOANDTLSTMTINT10AMT:=0;

      else
        new_rec.LOANDTLSTMTINT10AMT:=P_LOANDTLSTMTINT10AMT;
      end if;
      cur_colname:='P_LOANDTLSTMTINT11AMT';
      cur_colvalue:=P_LOANDTLSTMTINT11AMT;
      if P_LOANDTLSTMTINT11AMT is null then

        new_rec.LOANDTLSTMTINT11AMT:=0;

      else
        new_rec.LOANDTLSTMTINT11AMT:=P_LOANDTLSTMTINT11AMT;
      end if;
      cur_colname:='P_LOANDTLSTMTINT12AMT';
      cur_colvalue:=P_LOANDTLSTMTINT12AMT;
      if P_LOANDTLSTMTINT12AMT is null then

        new_rec.LOANDTLSTMTINT12AMT:=0;

      else
        new_rec.LOANDTLSTMTINT12AMT:=P_LOANDTLSTMTINT12AMT;
      end if;
      cur_colname:='P_LOANDTLSTMTINT13AMT';
      cur_colvalue:=P_LOANDTLSTMTINT13AMT;
      if P_LOANDTLSTMTINT13AMT is null then

        new_rec.LOANDTLSTMTINT13AMT:=0;

      else
        new_rec.LOANDTLSTMTINT13AMT:=P_LOANDTLSTMTINT13AMT;
      end if;
      cur_colname:='P_LOANDTLSTMTAVGMOBALAMT';
      cur_colvalue:=P_LOANDTLSTMTAVGMOBALAMT;
      if P_LOANDTLSTMTAVGMOBALAMT is null then

        new_rec.LOANDTLSTMTAVGMOBALAMT:=0;

      else
        new_rec.LOANDTLSTMTAVGMOBALAMT:=P_LOANDTLSTMTAVGMOBALAMT;
      end if;
      cur_colname:='P_LOANDTLAMPRINAMT';
      cur_colvalue:=P_LOANDTLAMPRINAMT;
      if P_LOANDTLAMPRINAMT is null then

        new_rec.LOANDTLAMPRINAMT:=0;

      else
        new_rec.LOANDTLAMPRINAMT:=P_LOANDTLAMPRINAMT;
      end if;
      cur_colname:='P_LOANDTLAMINTAMT';
      cur_colvalue:=P_LOANDTLAMINTAMT;
      if P_LOANDTLAMINTAMT is null then

        new_rec.LOANDTLAMINTAMT:=0;

      else
        new_rec.LOANDTLAMINTAMT:=P_LOANDTLAMINTAMT;
      end if;
      cur_colname:='P_LOANDTLREFIIND';
      cur_colvalue:=P_LOANDTLREFIIND;
      if P_LOANDTLREFIIND is null then

        new_rec.LOANDTLREFIIND:=rpad(' ',1);

      else
        new_rec.LOANDTLREFIIND:=P_LOANDTLREFIIND;
      end if;
      cur_colname:='P_LOANDTLSETUPIND';
      cur_colvalue:=P_LOANDTLSETUPIND;
      if P_LOANDTLSETUPIND is null then

        new_rec.LOANDTLSETUPIND:=rpad(' ',1);

      else
        new_rec.LOANDTLSETUPIND:=P_LOANDTLSETUPIND;
      end if;
      cur_colname:='P_LOANDTLCREATDT';
      cur_colvalue:=P_LOANDTLCREATDT;
      if P_LOANDTLCREATDT is null then

        new_rec.LOANDTLCREATDT:=jan_1_1900;

      else
        new_rec.LOANDTLCREATDT:=P_LOANDTLCREATDT;
      end if;
      cur_colname:='P_LOANDTLLMAINTNDT';
      cur_colvalue:=P_LOANDTLLMAINTNDT;
      if P_LOANDTLLMAINTNDT is null then

        new_rec.LOANDTLLMAINTNDT:=jan_1_1900;

      else
        new_rec.LOANDTLLMAINTNDT:=P_LOANDTLLMAINTNDT;
      end if;
      cur_colname:='P_LOANDTLCONVDT';
      cur_colvalue:=P_LOANDTLCONVDT;
      if P_LOANDTLCONVDT is null then

        new_rec.LOANDTLCONVDT:=jan_1_1900;

      else
        new_rec.LOANDTLCONVDT:=P_LOANDTLCONVDT;
      end if;
      cur_colname:='P_LOANDTLPRGRM';
      cur_colvalue:=P_LOANDTLPRGRM;
      if P_LOANDTLPRGRM is null then

        new_rec.LOANDTLPRGRM:=rpad(' ',3);

      else
        new_rec.LOANDTLPRGRM:=P_LOANDTLPRGRM;
      end if;
      cur_colname:='P_LOANDTLNOTEMOPYMTAMT';
      cur_colvalue:=P_LOANDTLNOTEMOPYMTAMT;
      if P_LOANDTLNOTEMOPYMTAMT is null then

        new_rec.LOANDTLNOTEMOPYMTAMT:=0;

      else
        new_rec.LOANDTLNOTEMOPYMTAMT:=P_LOANDTLNOTEMOPYMTAMT;
      end if;
      cur_colname:='P_LOANDTLDBENTRMATDT';
      cur_colvalue:=P_LOANDTLDBENTRMATDT;
      if P_LOANDTLDBENTRMATDT is null then

        new_rec.LOANDTLDBENTRMATDT:=jan_1_1900;

      else
        new_rec.LOANDTLDBENTRMATDT:=P_LOANDTLDBENTRMATDT;
      end if;
      cur_colname:='P_LOANDTLNOTEMATDT';
      cur_colvalue:=P_LOANDTLNOTEMATDT;
      if P_LOANDTLNOTEMATDT is null then

        new_rec.LOANDTLNOTEMATDT:=jan_1_1900;

      else
        new_rec.LOANDTLNOTEMATDT:=P_LOANDTLNOTEMATDT;
      end if;
      cur_colname:='P_LOANDTLRESRVDEPPCT';
      cur_colvalue:=P_LOANDTLRESRVDEPPCT;
      if P_LOANDTLRESRVDEPPCT is null then

        new_rec.LOANDTLRESRVDEPPCT:=0;

      else
        new_rec.LOANDTLRESRVDEPPCT:=P_LOANDTLRESRVDEPPCT;
      end if;
      cur_colname:='P_LOANDTLCDCPFEEPCT';
      cur_colvalue:=P_LOANDTLCDCPFEEPCT;
      if P_LOANDTLCDCPFEEPCT is null then

        new_rec.LOANDTLCDCPFEEPCT:=0;

      else
        new_rec.LOANDTLCDCPFEEPCT:=P_LOANDTLCDCPFEEPCT;
      end if;
      cur_colname:='P_LOANDTLCDCPFEEDOLLRAMT';
      cur_colvalue:=P_LOANDTLCDCPFEEDOLLRAMT;
      if P_LOANDTLCDCPFEEDOLLRAMT is null then

        new_rec.LOANDTLCDCPFEEDOLLRAMT:=0;

      else
        new_rec.LOANDTLCDCPFEEDOLLRAMT:=P_LOANDTLCDCPFEEDOLLRAMT;
      end if;
      cur_colname:='P_LOANDTLDUEBORRAMT';
      cur_colvalue:=P_LOANDTLDUEBORRAMT;
      if P_LOANDTLDUEBORRAMT is null then

        new_rec.LOANDTLDUEBORRAMT:=0;

      else
        new_rec.LOANDTLDUEBORRAMT:=P_LOANDTLDUEBORRAMT;
      end if;
      cur_colname:='P_LOANDTLAPPVDT';
      cur_colvalue:=P_LOANDTLAPPVDT;
      if P_LOANDTLAPPVDT is null then

        new_rec.LOANDTLAPPVDT:=jan_1_1900;

      else
        new_rec.LOANDTLAPPVDT:=P_LOANDTLAPPVDT;
      end if;
      cur_colname:='P_LOANDTLLTFEEIND';
      cur_colvalue:=P_LOANDTLLTFEEIND;
      if P_LOANDTLLTFEEIND is null then

        new_rec.LOANDTLLTFEEIND:=rpad(' ',1);

      else
        new_rec.LOANDTLLTFEEIND:=P_LOANDTLLTFEEIND;
      end if;
      cur_colname:='P_LOANDTLACHPRENTIND';
      cur_colvalue:=P_LOANDTLACHPRENTIND;
      if P_LOANDTLACHPRENTIND is null then

        new_rec.LOANDTLACHPRENTIND:=rpad(' ',1);

      else
        new_rec.LOANDTLACHPRENTIND:=P_LOANDTLACHPRENTIND;
      end if;
      cur_colname:='P_LOANDTLAMSCHDLTYP';
      cur_colvalue:=P_LOANDTLAMSCHDLTYP;
      if P_LOANDTLAMSCHDLTYP is null then

        new_rec.LOANDTLAMSCHDLTYP:=rpad(' ',2);

      else
        new_rec.LOANDTLAMSCHDLTYP:=P_LOANDTLAMSCHDLTYP;
      end if;
      cur_colname:='P_LOANDTLACHBNKNM';
      cur_colvalue:=P_LOANDTLACHBNKNM;
      if P_LOANDTLACHBNKNM is null then

        new_rec.LOANDTLACHBNKNM:=' ';

      else
        new_rec.LOANDTLACHBNKNM:=P_LOANDTLACHBNKNM;
      end if;
      cur_colname:='P_LOANDTLACHBNKMAILADRSTR1NM';
      cur_colvalue:=P_LOANDTLACHBNKMAILADRSTR1NM;
      if P_LOANDTLACHBNKMAILADRSTR1NM is null then

        new_rec.LOANDTLACHBNKMAILADRSTR1NM:=' ';

      else
        new_rec.LOANDTLACHBNKMAILADRSTR1NM:=P_LOANDTLACHBNKMAILADRSTR1NM;
      end if;
      cur_colname:='P_LOANDTLACHBNKMAILADRSTR2NM';
      cur_colvalue:=P_LOANDTLACHBNKMAILADRSTR2NM;
      if P_LOANDTLACHBNKMAILADRSTR2NM is null then

        new_rec.LOANDTLACHBNKMAILADRSTR2NM:=' ';

      else
        new_rec.LOANDTLACHBNKMAILADRSTR2NM:=P_LOANDTLACHBNKMAILADRSTR2NM;
      end if;
      cur_colname:='P_LOANDTLACHBNKMAILADRCTYNM';
      cur_colvalue:=P_LOANDTLACHBNKMAILADRCTYNM;
      if P_LOANDTLACHBNKMAILADRCTYNM is null then

        new_rec.LOANDTLACHBNKMAILADRCTYNM:=' ';

      else
        new_rec.LOANDTLACHBNKMAILADRCTYNM:=P_LOANDTLACHBNKMAILADRCTYNM;
      end if;
      cur_colname:='P_LOANDTLACHBNKMAILADRSTCD';
      cur_colvalue:=P_LOANDTLACHBNKMAILADRSTCD;
      if P_LOANDTLACHBNKMAILADRSTCD is null then

        new_rec.LOANDTLACHBNKMAILADRSTCD:=rpad(' ',2);

      else
        new_rec.LOANDTLACHBNKMAILADRSTCD:=P_LOANDTLACHBNKMAILADRSTCD;
      end if;
      cur_colname:='P_LOANDTLACHBNKMAILADRZIPCD';
      cur_colvalue:=P_LOANDTLACHBNKMAILADRZIPCD;
      if P_LOANDTLACHBNKMAILADRZIPCD is null then

        new_rec.LOANDTLACHBNKMAILADRZIPCD:=rpad(' ',5);

      else
        new_rec.LOANDTLACHBNKMAILADRZIPCD:=P_LOANDTLACHBNKMAILADRZIPCD;
      end if;
      cur_colname:='P_LOANDTLACHBNKMAILADRZIP4CD';
      cur_colvalue:=P_LOANDTLACHBNKMAILADRZIP4CD;
      if P_LOANDTLACHBNKMAILADRZIP4CD is null then

        new_rec.LOANDTLACHBNKMAILADRZIP4CD:=rpad(' ',4);

      else
        new_rec.LOANDTLACHBNKMAILADRZIP4CD:=P_LOANDTLACHBNKMAILADRZIP4CD;
      end if;
      cur_colname:='P_LOANDTLACHBNKBR';
      cur_colvalue:=P_LOANDTLACHBNKBR;
      if P_LOANDTLACHBNKBR is null then

        new_rec.LOANDTLACHBNKBR:=rpad(' ',15);

      else
        new_rec.LOANDTLACHBNKBR:=P_LOANDTLACHBNKBR;
      end if;
      cur_colname:='P_LOANDTLACHBNKACTYP';
      cur_colvalue:=P_LOANDTLACHBNKACTYP;
      if P_LOANDTLACHBNKACTYP is null then

        new_rec.LOANDTLACHBNKACTYP:=rpad(' ',1);

      else
        new_rec.LOANDTLACHBNKACTYP:=P_LOANDTLACHBNKACTYP;
      end if;
      cur_colname:='P_LOANDTLACHBNKACCT';
      cur_colvalue:=P_LOANDTLACHBNKACCT;
      if P_LOANDTLACHBNKACCT is null then

        new_rec.LOANDTLACHBNKACCT:=' ';

      else
        new_rec.LOANDTLACHBNKACCT:=P_LOANDTLACHBNKACCT;
      end if;
      cur_colname:='P_LOANDTLACHBNKROUTNMB';
      cur_colvalue:=P_LOANDTLACHBNKROUTNMB;
      if P_LOANDTLACHBNKROUTNMB is null then

        new_rec.LOANDTLACHBNKROUTNMB:=rpad(' ',15);

      else
        new_rec.LOANDTLACHBNKROUTNMB:=P_LOANDTLACHBNKROUTNMB;
      end if;
      cur_colname:='P_LOANDTLACHBNKTRANS';
      cur_colvalue:=P_LOANDTLACHBNKTRANS;
      if P_LOANDTLACHBNKTRANS is null then

        new_rec.LOANDTLACHBNKTRANS:=rpad(' ',15);

      else
        new_rec.LOANDTLACHBNKTRANS:=P_LOANDTLACHBNKTRANS;
      end if;
      cur_colname:='P_LOANDTLACHBNKIDNOORATTEN';
      cur_colvalue:=P_LOANDTLACHBNKIDNOORATTEN;
      if P_LOANDTLACHBNKIDNOORATTEN is null then

        new_rec.LOANDTLACHBNKIDNOORATTEN:=' ';

      else
        new_rec.LOANDTLACHBNKIDNOORATTEN:=P_LOANDTLACHBNKIDNOORATTEN;
      end if;
      cur_colname:='P_LOANDTLACHDEPNM';
      cur_colvalue:=P_LOANDTLACHDEPNM;
      if P_LOANDTLACHDEPNM is null then

        new_rec.LOANDTLACHDEPNM:=' ';

      else
        new_rec.LOANDTLACHDEPNM:=P_LOANDTLACHDEPNM;
      end if;
      cur_colname:='P_LOANDTLACHSIGNDT';
      cur_colvalue:=P_LOANDTLACHSIGNDT;
      if P_LOANDTLACHSIGNDT is null then

        new_rec.LOANDTLACHSIGNDT:=jan_1_1900;

      else
        new_rec.LOANDTLACHSIGNDT:=P_LOANDTLACHSIGNDT;
      end if;
      cur_colname:='P_LOANDTLBORRNM2';
      cur_colvalue:=P_LOANDTLBORRNM2;
      if P_LOANDTLBORRNM2 is null then

        new_rec.LOANDTLBORRNM2:=' ';

      else
        new_rec.LOANDTLBORRNM2:=P_LOANDTLBORRNM2;
      end if;
      cur_colname:='P_LOANDTLSBCNM2';
      cur_colvalue:=P_LOANDTLSBCNM2;
      if P_LOANDTLSBCNM2 is null then

        new_rec.LOANDTLSBCNM2:=' ';

      else
        new_rec.LOANDTLSBCNM2:=P_LOANDTLSBCNM2;
      end if;
      cur_colname:='P_LOANDTLCACCTNMB';
      cur_colvalue:=P_LOANDTLCACCTNMB;
      if P_LOANDTLCACCTNMB is null then

        new_rec.LOANDTLCACCTNMB:=' ';

      else
        new_rec.LOANDTLCACCTNMB:=P_LOANDTLCACCTNMB;
      end if;
      cur_colname:='P_LOANDTLRACCTNMB';
      cur_colvalue:=P_LOANDTLRACCTNMB;
      if P_LOANDTLRACCTNMB is null then

        new_rec.LOANDTLRACCTNMB:=' ';

      else
        new_rec.LOANDTLRACCTNMB:=P_LOANDTLRACCTNMB;
      end if;
      cur_colname:='P_LOANDTLINTLENDRIND';
      cur_colvalue:=P_LOANDTLINTLENDRIND;
      if P_LOANDTLINTLENDRIND is null then

        new_rec.LOANDTLINTLENDRIND:=rpad(' ',1);

      else
        new_rec.LOANDTLINTLENDRIND:=P_LOANDTLINTLENDRIND;
      end if;
      cur_colname:='P_LOANDTLINTRMLENDR';
      cur_colvalue:=P_LOANDTLINTRMLENDR;
      if P_LOANDTLINTRMLENDR is null then

        new_rec.LOANDTLINTRMLENDR:=0;

      else
        new_rec.LOANDTLINTRMLENDR:=P_LOANDTLINTRMLENDR;
      end if;
      cur_colname:='P_LOANDTLPYMTRECV';
      cur_colvalue:=P_LOANDTLPYMTRECV;
      if P_LOANDTLPYMTRECV is null then

        new_rec.LOANDTLPYMTRECV:=0;

      else
        new_rec.LOANDTLPYMTRECV:=P_LOANDTLPYMTRECV;
      end if;
      cur_colname:='P_LOANDTLTRMYR';
      cur_colvalue:=P_LOANDTLTRMYR;
      if P_LOANDTLTRMYR is null then

        new_rec.LOANDTLTRMYR:=0;

      else
        new_rec.LOANDTLTRMYR:=P_LOANDTLTRMYR;
      end if;
      cur_colname:='P_LOANDTLLOANTYP';
      cur_colvalue:=P_LOANDTLLOANTYP;
      if P_LOANDTLLOANTYP is null then

        new_rec.LOANDTLLOANTYP:=rpad(' ',1);

      else
        new_rec.LOANDTLLOANTYP:=P_LOANDTLLOANTYP;
      end if;
      cur_colname:='P_LOANDTLMLACCTNMB';
      cur_colvalue:=P_LOANDTLMLACCTNMB;
      if P_LOANDTLMLACCTNMB is null then

        new_rec.LOANDTLMLACCTNMB:=' ';

      else
        new_rec.LOANDTLMLACCTNMB:=P_LOANDTLMLACCTNMB;
      end if;
      cur_colname:='P_LOANDTLEXCESSDUEBORRAMT';
      cur_colvalue:=P_LOANDTLEXCESSDUEBORRAMT;
      if P_LOANDTLEXCESSDUEBORRAMT is null then

        new_rec.LOANDTLEXCESSDUEBORRAMT:=0;

      else
        new_rec.LOANDTLEXCESSDUEBORRAMT:=P_LOANDTLEXCESSDUEBORRAMT;
      end if;
      cur_colname:='P_LOANDTL503SUBTYP';
      cur_colvalue:=P_LOANDTL503SUBTYP;
      if P_LOANDTL503SUBTYP is null then

        new_rec.LOANDTL503SUBTYP:=rpad(' ',1);

      else
        new_rec.LOANDTL503SUBTYP:=P_LOANDTL503SUBTYP;
      end if;
      cur_colname:='P_LOANDTLLPYMTDT';
      cur_colvalue:=P_LOANDTLLPYMTDT;
      if P_LOANDTLLPYMTDT is null then

        new_rec.LOANDTLLPYMTDT:=jan_1_1900;

      else
        new_rec.LOANDTLLPYMTDT:=P_LOANDTLLPYMTDT;
      end if;
      cur_colname:='P_LOANDTLPOOLSERS';
      cur_colvalue:=P_LOANDTLPOOLSERS;
      if P_LOANDTLPOOLSERS is null then

        new_rec.LOANDTLPOOLSERS:=rpad(' ',7);

      else
        new_rec.LOANDTLPOOLSERS:=P_LOANDTLPOOLSERS;
      end if;
      cur_colname:='P_LOANDTLSBAFEEPCT';
      cur_colvalue:=P_LOANDTLSBAFEEPCT;
      if P_LOANDTLSBAFEEPCT is null then

        new_rec.LOANDTLSBAFEEPCT:=0;

      else
        new_rec.LOANDTLSBAFEEPCT:=P_LOANDTLSBAFEEPCT;
      end if;
      cur_colname:='P_LOANDTLSBAPAIDTHRUDTX';
      cur_colvalue:=P_LOANDTLSBAPAIDTHRUDTX;
      if P_LOANDTLSBAPAIDTHRUDTX is null then

        new_rec.LOANDTLSBAPAIDTHRUDTX:=rpad(' ',8);

      else
        new_rec.LOANDTLSBAPAIDTHRUDTX:=P_LOANDTLSBAPAIDTHRUDTX;
      end if;
      cur_colname:='P_LOANDTLSBAFEEAMT';
      cur_colvalue:=P_LOANDTLSBAFEEAMT;
      if P_LOANDTLSBAFEEAMT is null then

        new_rec.LOANDTLSBAFEEAMT:=0;

      else
        new_rec.LOANDTLSBAFEEAMT:=P_LOANDTLSBAFEEAMT;
      end if;
      cur_colname:='P_LOANDTLPRMAMT';
      cur_colvalue:=P_LOANDTLPRMAMT;
      if P_LOANDTLPRMAMT is null then

        new_rec.LOANDTLPRMAMT:=0;

      else
        new_rec.LOANDTLPRMAMT:=P_LOANDTLPRMAMT;
      end if;
      cur_colname:='P_LOANDTLLENDRSBAFEEAMT';
      cur_colvalue:=P_LOANDTLLENDRSBAFEEAMT;
      if P_LOANDTLLENDRSBAFEEAMT is null then

        new_rec.LOANDTLLENDRSBAFEEAMT:=0;

      else
        new_rec.LOANDTLLENDRSBAFEEAMT:=P_LOANDTLLENDRSBAFEEAMT;
      end if;
      cur_colname:='P_LOANDTLWITHHELOANDTLIND';
      cur_colvalue:=P_LOANDTLWITHHELOANDTLIND;
      if P_LOANDTLWITHHELOANDTLIND is null then

        new_rec.LOANDTLWITHHELOANDTLIND:=rpad(' ',1);

      else
        new_rec.LOANDTLWITHHELOANDTLIND:=P_LOANDTLWITHHELOANDTLIND;
      end if;
      cur_colname:='P_LOANDTLPRGRMTYP';
      cur_colvalue:=P_LOANDTLPRGRMTYP;
      if P_LOANDTLPRGRMTYP is null then

        new_rec.LOANDTLPRGRMTYP:=rpad(' ',3);

      else
        new_rec.LOANDTLPRGRMTYP:=P_LOANDTLPRGRMTYP;
      end if;
      cur_colname:='P_LOANDTLLATEIND';
      cur_colvalue:=P_LOANDTLLATEIND;
      if P_LOANDTLLATEIND is null then

        new_rec.LOANDTLLATEIND:=rpad(' ',1);

      else
        new_rec.LOANDTLLATEIND:=P_LOANDTLLATEIND;
      end if;
      cur_colname:='P_LOANDTLCMNT1';
      cur_colvalue:=P_LOANDTLCMNT1;
      if P_LOANDTLCMNT1 is null then

        new_rec.LOANDTLCMNT1:=' ';

      else
        new_rec.LOANDTLCMNT1:=P_LOANDTLCMNT1;
      end if;
      cur_colname:='P_LOANDTLCMNT2';
      cur_colvalue:=P_LOANDTLCMNT2;
      if P_LOANDTLCMNT2 is null then

        new_rec.LOANDTLCMNT2:=' ';

      else
        new_rec.LOANDTLCMNT2:=P_LOANDTLCMNT2;
      end if;
      cur_colname:='P_LOANDTLSOD';
      cur_colvalue:=P_LOANDTLSOD;
      if P_LOANDTLSOD is null then

        new_rec.LOANDTLSOD:=rpad(' ',4);

      else
        new_rec.LOANDTLSOD:=P_LOANDTLSOD;
      end if;
      cur_colname:='P_LOANDTLTOBECURAMT';
      cur_colvalue:=P_LOANDTLTOBECURAMT;
      if P_LOANDTLTOBECURAMT is null then

        new_rec.LOANDTLTOBECURAMT:=0;

      else
        new_rec.LOANDTLTOBECURAMT:=P_LOANDTLTOBECURAMT;
      end if;
      cur_colname:='P_LOANDTLGNTYREPAYPTDAMT';
      cur_colvalue:=P_LOANDTLGNTYREPAYPTDAMT;
      if P_LOANDTLGNTYREPAYPTDAMT is null then

        new_rec.LOANDTLGNTYREPAYPTDAMT:=0;

      else
        new_rec.LOANDTLGNTYREPAYPTDAMT:=P_LOANDTLGNTYREPAYPTDAMT;
      end if;
      cur_colname:='P_LOANDTLCDCPAIDTHRUDTX';
      cur_colvalue:=P_LOANDTLCDCPAIDTHRUDTX;
      if P_LOANDTLCDCPAIDTHRUDTX is null then

        new_rec.LOANDTLCDCPAIDTHRUDTX:=rpad(' ',8);

      else
        new_rec.LOANDTLCDCPAIDTHRUDTX:=P_LOANDTLCDCPAIDTHRUDTX;
      end if;
      cur_colname:='P_LOANDTLCSAPAIDTHRUDTX';
      cur_colvalue:=P_LOANDTLCSAPAIDTHRUDTX;
      if P_LOANDTLCSAPAIDTHRUDTX is null then

        new_rec.LOANDTLCSAPAIDTHRUDTX:=rpad(' ',8);

      else
        new_rec.LOANDTLCSAPAIDTHRUDTX:=P_LOANDTLCSAPAIDTHRUDTX;
      end if;
      cur_colname:='P_LOANDTLINTPAIDTHRUDTX';
      cur_colvalue:=P_LOANDTLINTPAIDTHRUDTX;
      if P_LOANDTLINTPAIDTHRUDTX is null then

        new_rec.LOANDTLINTPAIDTHRUDTX:=rpad(' ',8);

      else
        new_rec.LOANDTLINTPAIDTHRUDTX:=P_LOANDTLINTPAIDTHRUDTX;
      end if;
      cur_colname:='P_LOANDTLSTRTBAL1AMT';
      cur_colvalue:=P_LOANDTLSTRTBAL1AMT;
      if P_LOANDTLSTRTBAL1AMT is null then

        new_rec.LOANDTLSTRTBAL1AMT:=0;

      else
        new_rec.LOANDTLSTRTBAL1AMT:=P_LOANDTLSTRTBAL1AMT;
      end if;
      cur_colname:='P_LOANDTLSTRTBAL2AMT';
      cur_colvalue:=P_LOANDTLSTRTBAL2AMT;
      if P_LOANDTLSTRTBAL2AMT is null then

        new_rec.LOANDTLSTRTBAL2AMT:=0;

      else
        new_rec.LOANDTLSTRTBAL2AMT:=P_LOANDTLSTRTBAL2AMT;
      end if;
      cur_colname:='P_LOANDTLSTRTBAL3AMT';
      cur_colvalue:=P_LOANDTLSTRTBAL3AMT;
      if P_LOANDTLSTRTBAL3AMT is null then

        new_rec.LOANDTLSTRTBAL3AMT:=0;

      else
        new_rec.LOANDTLSTRTBAL3AMT:=P_LOANDTLSTRTBAL3AMT;
      end if;
      cur_colname:='P_LOANDTLSTRTBAL4AMT';
      cur_colvalue:=P_LOANDTLSTRTBAL4AMT;
      if P_LOANDTLSTRTBAL4AMT is null then

        new_rec.LOANDTLSTRTBAL4AMT:=0;

      else
        new_rec.LOANDTLSTRTBAL4AMT:=P_LOANDTLSTRTBAL4AMT;
      end if;
      cur_colname:='P_LOANDTLSTRTBAL5AMT';
      cur_colvalue:=P_LOANDTLSTRTBAL5AMT;
      if P_LOANDTLSTRTBAL5AMT is null then

        new_rec.LOANDTLSTRTBAL5AMT:=0;

      else
        new_rec.LOANDTLSTRTBAL5AMT:=P_LOANDTLSTRTBAL5AMT;
      end if;
      cur_colname:='P_LOANDTLSBCIDNMB';
      cur_colvalue:=P_LOANDTLSBCIDNMB;
      if P_LOANDTLSBCIDNMB is null then

        new_rec.LOANDTLSBCIDNMB:=rpad(' ',5);

      else
        new_rec.LOANDTLSBCIDNMB:=P_LOANDTLSBCIDNMB;
      end if;
      cur_colname:='P_LOANDTLACHLASTCHNGDT';
      cur_colvalue:=P_LOANDTLACHLASTCHNGDT;
      if P_LOANDTLACHLASTCHNGDT is null then

        new_rec.LOANDTLACHLASTCHNGDT:=jan_1_1900;

      else
        new_rec.LOANDTLACHLASTCHNGDT:=P_LOANDTLACHLASTCHNGDT;
      end if;
      cur_colname:='P_LOANDTLPRENOTETESTDT';
      cur_colvalue:=P_LOANDTLPRENOTETESTDT;
      if P_LOANDTLPRENOTETESTDT is null then

        new_rec.LOANDTLPRENOTETESTDT:=jan_1_1900;

      else
        new_rec.LOANDTLPRENOTETESTDT:=P_LOANDTLPRENOTETESTDT;
      end if;
      cur_colname:='P_LOANDTLPYMTNMBDUE';
      cur_colvalue:=P_LOANDTLPYMTNMBDUE;
      if P_LOANDTLPYMTNMBDUE is null then

        new_rec.LOANDTLPYMTNMBDUE:=0;

      else
        new_rec.LOANDTLPYMTNMBDUE:=P_LOANDTLPYMTNMBDUE;
      end if;
      cur_colname:='P_LOANDTLRESRVAMT';
      cur_colvalue:=P_LOANDTLRESRVAMT;
      if P_LOANDTLRESRVAMT is null then

        new_rec.LOANDTLRESRVAMT:=0;

      else
        new_rec.LOANDTLRESRVAMT:=P_LOANDTLRESRVAMT;
      end if;
      cur_colname:='P_LOANDTLFEEBASEAMT';
      cur_colvalue:=P_LOANDTLFEEBASEAMT;
      if P_LOANDTLFEEBASEAMT is null then

        new_rec.LOANDTLFEEBASEAMT:=0;

      else
        new_rec.LOANDTLFEEBASEAMT:=P_LOANDTLFEEBASEAMT;
      end if;
      cur_colname:='P_LOANDTLSTATCDOFLOAN';
      cur_colvalue:=P_LOANDTLSTATCDOFLOAN;
      if P_LOANDTLSTATCDOFLOAN is null then

        new_rec.LOANDTLSTATCDOFLOAN:=rpad(' ',2);

      else
        new_rec.LOANDTLSTATCDOFLOAN:=P_LOANDTLSTATCDOFLOAN;
      end if;
      cur_colname:='P_LOANDTLACTUALCSAINITFEEAMT';
      cur_colvalue:=P_LOANDTLACTUALCSAINITFEEAMT;
      if P_LOANDTLACTUALCSAINITFEEAMT is null then

        new_rec.LOANDTLACTUALCSAINITFEEAMT:=0;

      else
        new_rec.LOANDTLACTUALCSAINITFEEAMT:=P_LOANDTLACTUALCSAINITFEEAMT;
      end if;
      cur_colname:='P_LOANDTLSTATDT';
      cur_colvalue:=P_LOANDTLSTATDT;
      if P_LOANDTLSTATDT is null then

        new_rec.LOANDTLSTATDT:=jan_1_1900;

      else
        new_rec.LOANDTLSTATDT:=P_LOANDTLSTATDT;
      end if;
      cur_colname:='P_LOANDTLSTMTCLSBALAMT';
      cur_colvalue:=P_LOANDTLSTMTCLSBALAMT;
      if P_LOANDTLSTMTCLSBALAMT is null then

        new_rec.LOANDTLSTMTCLSBALAMT:=0;

      else
        new_rec.LOANDTLSTMTCLSBALAMT:=P_LOANDTLSTMTCLSBALAMT;
      end if;
      cur_colname:='P_LOANDTLSTMTCLSDT';
      cur_colvalue:=P_LOANDTLSTMTCLSDT;
      if P_LOANDTLSTMTCLSDT is null then

        new_rec.LOANDTLSTMTCLSDT:=jan_1_1900;

      else
        new_rec.LOANDTLSTMTCLSDT:=P_LOANDTLSTMTCLSDT;
      end if;
      cur_colname:='P_LOANDTLLASTDBPAYOUTAMT';
      cur_colvalue:=P_LOANDTLLASTDBPAYOUTAMT;
      if P_LOANDTLLASTDBPAYOUTAMT is null then

        new_rec.LOANDTLLASTDBPAYOUTAMT:=0;

      else
        new_rec.LOANDTLLASTDBPAYOUTAMT:=P_LOANDTLLASTDBPAYOUTAMT;
      end if;
      cur_colname:='P_LOANDTLCHEMICALBASISDAYS';
      cur_colvalue:=P_LOANDTLCHEMICALBASISDAYS;
      if P_LOANDTLCHEMICALBASISDAYS is null then

        new_rec.LOANDTLCHEMICALBASISDAYS:=0;

      else
        new_rec.LOANDTLCHEMICALBASISDAYS:=P_LOANDTLCHEMICALBASISDAYS;
      end if;
    exception when others then
       p_errval:=sqlcode;
       p_errmsg:='Error - Oracle returned '||sqlerrm(sqlcode)||' using '
           ||cur_colname||' Value >'||cur_colvalue||'<';
    end;
  end if;
    keystouse:='(LOANNMB)=('||new_rec.LOANNMB||')';
    new_rec.LoanDtlCreatDt:=sysdate;
    if p_errval=0 then
      begin
        insert into STGCSA.SOFVLND1TBL values new_rec;
        p_RETVAL := SQL%ROWCOUNT;
      exception
      when dup_val_on_index then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Insert caused dupe on unique IX: '||sqlerrm(sqlcode);
      when others then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<3 then maxsev:=3; end if;
        p_errmsg:='Inserting record caught SQL error '||sqlerrm(sqlcode);
      end;
    end if;
  else /* P_IDENTIFIER was unexpected value */
    p_errval:=-20002;
    if maxsev<3 then maxsev:=3; end if;
    p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier
      ||' inserting record with key '||keystouse;
  end if;
  if p_errval=0 then
    runtime.logger(logged_msgid_retval,'STDLOG',101,
      'End SOFVLND1INSTSP With 0 return, P_RETVAL='
      ||p_retval||' Record Key '||keystouse,p_userid,
      1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt,
      l_timestampfld,PROGRAM_NAME=>'SOFVLND1INSTSP');
  else
    p_errmsg:='Error: '||P_ERRMSG||' in SOFVLND1INSTSP Record Key '||keystouse
      ||' Build: 2018-11-07 11:36:05';
    ROLLBACK TO SOFVLND1INSTSP;
    runtime.logger(logged_msgid_retval,'STDERR',103,
      p_errmsg,p_userid,maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,
      l_entrydt, l_timestampfld,PROGRAM_NAME=>'SOFVLND1INSTSP',force_log_entry=>TRUE);
  end if;
EXCEPTION
WHEN OTHERS THEN
  p_errval:=SQLCODE;
  p_errmsg:=' In outer exception handler in SOFVLND1INSTSP caught exception '
    ||sqlerrm(sqlcode)||' Record Key '||keystouse||' build 2018-11-07 11:36:05';
  ROLLBACK TO SOFVLND1INSTSP;
  runtime.logger(logged_msgid_retval,'STDERR',20,p_errmsg,p_userid,3,
  PROGRAM_NAME=>'SOFVLND1INSTSP',force_log_entry=>TRUE);
--
END SOFVLND1INSTSP;
/


GRANT EXECUTE ON STGCSA.SOFVLND1INSTSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.SOFVLND1INSTSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.SOFVLND1INSTSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.SOFVLND1INSTSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.SOFVLND1INSTSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.SOFVLND1INSTSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.SOFVLND1INSTSP TO STGCSADEVROLE;

DROP PROCEDURE STGCSA.PYMT110RPTUPDTSP;

CREATE OR REPLACE PROCEDURE STGCSA.PYMT110RPTUPDTSP(
    p_retval out number,
    p_errval out number,
    p_errmsg  out varchar2,
    p_identifier number:=0,
    p_userid varchar2
   ,p_PYMT110ID VARCHAR2:=null
   ,p_TRANSID VARCHAR2:=null
   ,p_LOANNMB CHAR:=null
   ,p_POSTAMT VARCHAR2:=null
   ,p_PRINAMT VARCHAR2:=null
   ,p_INTAMT VARCHAR2:=null
   ,p_UNALLOCAMT VARCHAR2:=null
   ,p_CSAAMT VARCHAR2:=null
   ,p_CDCAMT VARCHAR2:=null
   ,p_CSARAMT VARCHAR2:=null
   ,p_CDCRAMT VARCHAR2:=null
   ,p_LFAMT VARCHAR2:=null
   ,p_SBAAMT VARCHAR2:=null
   ,p_TIMESTAMPFLD DATE:=null
) as
 /*
  Created on: 2018-11-07 11:35:04
  Created by: GENR
  Crerated from template stdtblupd_template v3.22 12 Oct 2018 on 2018-11-07 11:35:04
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  Procedure PYMT110RPTUPDTSP performs UPDATE on STGCSA.PYMT110RPTTBL
    for P_IDENTIFIER=0 qualification is on Primary Key: PYMT110ID
    for P_IDENTIFIER=2 qualification is on PYMT110ID =0 or PYMT110ID<>0
    Updates all columns except the qualification keys and columns for which a
      null value is passed.
    SPECIAL secret update kludge!
      if p_identifier is set to -1 then a flag is set to use all input parm values
      and p_identifier is changed to 0 (update by PK).
      THEREFORE if using p_identifier=-1 you MUST pass intended value for all columns.
      This option allows setting colulmns to null!
*/
  dbrec STGCSA.PYMT110RPTTBL%rowtype;
  logged_msg_id number;
  recnum number;
  rec_rowid rowid;
  keystouse varchar2(256);
  maxsev number:=0;
  pctsign char(1):='%';
  cur_colname varchar2(100):=null;
  cur_colvalue varchar2(4000);
  do_normal_field_checking char(1):='Y';
  overridden_p_identifier number;
  -- standard activity log capture vars
  l_LOANNMB char(10):=null;
  l_TRANSID number:=null;
  l_dbmdl varchar2(10):=null;
  l_entrydt date:=null;
  l_userid  varchar2(32):=null;
  l_usernm  varchar2(199):=null;
  l_timestampfld date:=null;
procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
begin
  if p_errval=0 then
    begin
      select * into dbrec from STGCSA.PYMT110RPTTBL where rowid=rowid_in;
    exception when others then
      p_errval:=sqlcode;
      maxsev:=3;
      p_retval:=0;
      p_errmsg:='Fetch of STGCSA.PYMT110RPTTBL row by rowid '||rowid_in
        ||' orinally fetched using '||orig_keyset_values
        ||' failed with '||sqlerrm(sqlcode);
    end;
  end if;
  if p_errval=0 then
    -- set this as lastupdt
    DBrec.lastupdtuserid:=p_userid;
    DBrec.lastupdtdt:=sysdate;
    -- copy into DBrec all table column input parameters that are not null,
    -- saving column name/value if needed for error reporting
    --  (if p_identifier=-1 then move input even if null)
    begin
      if P_PYMT110ID is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PYMT110ID';
        cur_colvalue:=P_PYMT110ID;
        DBrec.PYMT110ID:=P_PYMT110ID;
      end if;
      if P_TRANSID is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_TRANSID';
        cur_colvalue:=P_TRANSID;
        DBrec.TRANSID:=P_TRANSID;
      end if;
      if P_LOANNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_LOANNMB';
        cur_colvalue:=P_LOANNMB;
        DBrec.LOANNMB:=P_LOANNMB;
      end if;
      if P_POSTAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_POSTAMT';
        cur_colvalue:=P_POSTAMT;
        DBrec.POSTAMT:=P_POSTAMT;
      end if;
      if P_PRINAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PRINAMT';
        cur_colvalue:=P_PRINAMT;
        DBrec.PRINAMT:=P_PRINAMT;
      end if;
      if P_INTAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_INTAMT';
        cur_colvalue:=P_INTAMT;
        DBrec.INTAMT:=P_INTAMT;
      end if;
      if P_UNALLOCAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_UNALLOCAMT';
        cur_colvalue:=P_UNALLOCAMT;
        DBrec.UNALLOCAMT:=P_UNALLOCAMT;
      end if;
      if P_CSAAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CSAAMT';
        cur_colvalue:=P_CSAAMT;
        DBrec.CSAAMT:=P_CSAAMT;
      end if;
      if P_CDCAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCAMT';
        cur_colvalue:=P_CDCAMT;
        DBrec.CDCAMT:=P_CDCAMT;
      end if;
      if P_CSARAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CSARAMT';
        cur_colvalue:=P_CSARAMT;
        DBrec.CSARAMT:=P_CSARAMT;
      end if;
      if P_CDCRAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_CDCRAMT';
        cur_colvalue:=P_CDCRAMT;
        DBrec.CDCRAMT:=P_CDCRAMT;
      end if;
      if P_LFAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_LFAMT';
        cur_colvalue:=P_LFAMT;
        DBrec.LFAMT:=P_LFAMT;
      end if;
      if P_SBAAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_SBAAMT';
        cur_colvalue:=P_SBAAMT;
        DBrec.SBAAMT:=P_SBAAMT;
      end if;
      if P_TIMESTAMPFLD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_TIMESTAMPFLD';
        cur_colvalue:=P_TIMESTAMPFLD;
        DBrec.TIMESTAMPFLD:=P_TIMESTAMPFLD;
      end if;
    exception when others then
      p_errval:=sqlcode;
      if maxsev<3 then maxsev:=3; end if;
      p_errmsg:='Error using '||cur_colname||' Value >'||cur_colvalue||'<';
    end;
  end if;
  -- if all still ok do actual update
  if p_errval=0 then
    Begin
      update STGCSA.PYMT110RPTTBL
        set lastupdtuserid=DBrec.lastupdtuserid,
          lastupdtdt=DBrec.lastupdtdt
          ,PYMT110ID=DBrec.PYMT110ID
          ,TRANSID=DBrec.TRANSID
          ,LOANNMB=DBrec.LOANNMB
          ,POSTAMT=DBrec.POSTAMT
          ,PRINAMT=DBrec.PRINAMT
          ,INTAMT=DBrec.INTAMT
          ,UNALLOCAMT=DBrec.UNALLOCAMT
          ,CSAAMT=DBrec.CSAAMT
          ,CDCAMT=DBrec.CDCAMT
          ,CSARAMT=DBrec.CSARAMT
          ,CDCRAMT=DBrec.CDCRAMT
          ,LFAMT=DBrec.LFAMT
          ,SBAAMT=DBrec.SBAAMT
          ,TIMESTAMPFLD=DBrec.TIMESTAMPFLD
        where rowid=rowid_in;
      recnum:=recnum+1;
    exception when others then
      p_errval:=sqlcode;
      p_retval:=0;
      if maxsev<3 then maxsev:=3; end if;
      p_errmsg:= 'Error while doing update on STGCSA.PYMT110RPTTBL, on rec with rowid '
      ||rowid_in||' previously fetched using key: '||orig_keyset_values
      ||' got Oracle error '||sqlerrm(sqlcode);
    end;
  end if;
END;
begin
  -- setup
  savepoint PYMT110RPTUPDTSP;
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  recnum:=0;
  runtime.logger(logged_msg_id,'STDLOG',100,'Init PYMT110RPTUPDTSP',p_userid,4,
    logtxt1=>'PYMT110RPTUPDTSP',logtxt2=>'stdtblupd_template v3.22 12 Oct 2018',
    PROGRAM_NAME=>'PYMT110RPTUPDTSP');
  --
  l_TRANSID:=P_TRANSID;  l_LOANNMB:=P_LOANNMB;  l_TIMESTAMPFLD:=P_TIMESTAMPFLD;
--
-- special p_identifier kludge
--  if p_identifier is set to -1 then it treated as 0 (update by PK)
--  but a flag is set use passed value EVEN IF NULL.  Since for option -1
--  every column is updated to what was passed, intended values MUST be passed
--  for all columns.  This option allows setting colulmns to null!
  overridden_p_identifier:=p_identifier;
  if p_identifier=-1 then
    do_normal_field_checking:='N';
    overridden_p_identifier:=0;
  end if;
  case overridden_p_identifier
  when 0 then
    -- case to update one row based on primary key
    -- p_identifier=0 (choice 0) is default action for a stored procedure
    -- get existing version of row
    if p_errval=0 then
      keystouse:='(PYMT110ID)=('||P_PYMT110ID||')';
      begin
        select rowid into rec_rowid from STGCSA.PYMT110RPTTBL where 1=1
         and PYMT110ID=P_PYMT110ID;
      exception when no_data_found then
        p_errval:=0;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Warning No STGCSA.PYMT110RPTTBL row to update with key(s) '
          ||keystouse;
      end;
    end if;
    if p_errval=0 then
      fetch_and_update_by_rowid(rec_rowid, keystouse);
    end if;
  when 2 then
    -- case to update one or more rows based on keyset KEYS2
    if p_errval=0 then
      keystouse:='(PYMT110ID)=0 or PYMT110ID<>0';
      begin
        for r2 in (select rowid from STGCSA.PYMT110RPTTBL a where 1=1

            and PYMT110ID =0 or PYMT110ID<>0
  )
        loop
          rec_rowid:=r2.rowid;
          if p_errval=0 then
            fetch_and_update_by_rowid(rec_rowid, keystouse);
          end if;
        end loop;
      exception when others then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<3 then maxsev:=3; end if;
        p_errmsg:='Error fetching STGCSA.PYMT110RPTTBL row(s) with key(s) '
         ||keystouse||' Oracle returned '||sqlerrm(sqlcode);
      end;
      if recnum=0 then
        p_errval:=0;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Warning No STGCSA.PYMT110RPTTBL row(s) to update with key(s) '
          ||keystouse;
      end if;
    end if;
  else /* P_IDENTIFIER was unexpected value */
    p_errval:=-20002;
    if maxsev<3 then maxsev:=3; end if;
    p_errmsg:='Error P_IDENTIFIER contained unexpected value: '||p_identifier;
  end case;
  P_RETVAL:=RECNUM;
  -- post to log
  if p_errval=0 then
    runtime.logger(logged_msg_id,'STDLOG',101,
      'End PYMT110RPTUPDTSP With 0 return, P_RETVAL='
      ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
      1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld
      ,PROGRAM_NAME=>'PYMT110RPTUPDTSP');
  else
    ROLLBACK TO PYMT110RPTUPDTSP;
    p_errmsg:=p_errmsg||' in PYMT110RPTUPDTSP build 2018-11-07 11:35:04';
    runtime.logger(logged_msg_id,'STDERR',103,p_errmsg,p_userid,
      maxsev, l_usernm, l_loannmb,l_transid, l_dbmdl, l_entrydt, l_timestampfld,
      PROGRAM_NAME=>'PYMT110RPTUPDTSP',force_log_entry=>true);
  end if;
EXCEPTION
WHEN OTHERS THEN
  ROLLBACK TO PYMT110RPTUPDTSP;
  p_errmsg:=' Error outer exception handler caught Oracle error:'||SQLERRM(SQLCODE)
    ||' in PYMT110RPTUPDTSP build 2018-11-07 11:35:04';
  runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
    3,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld,
    PROGRAM_NAME=>'PYMT110RPTUPDTSP',force_log_entry=>true);
--
END PYMT110RPTUPDTSP;
/


GRANT EXECUTE ON STGCSA.PYMT110RPTUPDTSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.PYMT110RPTUPDTSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.PYMT110RPTUPDTSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.PYMT110RPTUPDTSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.PYMT110RPTUPDTSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.PYMT110RPTUPDTSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.PYMT110RPTUPDTSP TO STGCSADEVROLE;

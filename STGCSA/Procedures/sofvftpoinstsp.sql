DROP PROCEDURE STGCSA.SOFVFTPOINSTSP;

CREATE OR REPLACE PROCEDURE STGCSA.SOFVFTPOINSTSP(
    p_retval out number,
    p_errval out number,
    p_errmsg  out varchar2,
    p_identifier number:=0,
    p_userid varchar2
   ,p_PREPOSTRVWRECRDTYPCD CHAR:=null
   ,p_PREPOSTRVWTRANSCD CHAR:=null
   ,p_PREPOSTRVWTRANSITROUTINGNMB CHAR:=null
   ,p_PREPOSTRVWTRANSITROUTCHKDGT CHAR:=null
   ,p_PREPOSTRVWBNKACCTNMB VARCHAR2:=null
   ,p_PREPOSTRVWPYMTAMT VARCHAR2:=null
   ,p_LOANNMB CHAR:=null
   ,p_PREPOSTRVWINDVLNM VARCHAR2:=null
   ,p_PREPOSTRVWCOMPBNKDISC CHAR:=null
   ,p_PREPOSTRVWADDENDRECIND VARCHAR2:=null
   ,p_PREPOSTRVWTRACENMB VARCHAR2:=null
   ,p_PREPOSTRVWCMNT VARCHAR2:=null
) as
 /*
  Created on: 2018-11-07 11:35:51
  Created by: GENR
  Crerated from template stdtblins_template v3.05 13 Sep 2018 on 2018-11-07 11:35:51
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  Procedure performs INSERT on STGCSA.SOFVFTPOTBLfor P_IDENTIFIER=0
*/
  rundate date;
  gn global_name.global_name%type;
  new_rec STGCSA.SOFVFTPOTBL%rowtype;
  logged_msgid_retval number;
  audretval number;
  crossover_active char(1);
  l_LOANNMB char(10):=null;
  l_TRANSID number:=null;
  l_dbmdl varchar2(10):=null;
  l_entrydt date:=null;
  l_userid  varchar2(32):=null;
  l_usernm  varchar2(199):=null;
  l_timestampfld date:=null;
  cur_col_name varchar2(100):=null;
  maxsev number:=0;
  holdstring varchar2(4000);
  keystouse varchar2(1000);
  crossover_not_active char(1);
  -- vars for checking non-defaultable columns
  onespace varchar2(10):=' ';
  ndfltmsg varchar2(300);
  v_errfnd boolean:=false;
  -- std date for column defaulting
  JAN_1_1900  constant date:=to_date('JAN-01-1900','MON-DD-YYYY');
  cur_colvalue varchar2(4095);
  cur_colname varchar2(30);
-- Main body begins here
begin
  -- setup
  savepoint SOFVFTPOINSTSP;
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  runtime.logger(logged_msgid_retval,'STDLOG',100,'Init SOFVFTPOINSTSP',p_userid,4,
    logtxt1=>'SOFVFTPOINSTSP',logtxt2=>'stdtblins_template v3.05 13 Sep 2018',
    PROGRAM_NAME=>'SOFVFTPOINSTSP');
  select global_name, sysdate into gn, rundate from global_name;
  --
  -- Check to see if crossover is ongoing, in which case updates are prohibited
  SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
  IF crossover_not_active != 'Y' THEN
    p_errval := -20099;
    p_errmsg := 'Warning: Cannot update table SOFVFTPOTBL at this time because crossover is running, please try later.';
  END IF;
  -- end of crossover_lockout_check
  begin
    cur_colname:='LOANNMB';
    cur_colvalue:=P_LOANNMB;
    l_LOANNMB:=P_LOANNMB;
  exception when others then
    p_errval:=sqlcode;
    p_errmsg:='Error '||sqlerrm(sqlcode)||' using '||cur_colname||' Value >'||cur_colvalue||'<';
  end;
  new_rec.creatuserid:=p_userid;
  new_rec.creatdt:=sysdate;
  new_rec.lastupdtuserid:=p_userid;
  new_rec.lastupdtdt:=sysdate;
  if p_identifier=0 then
    -- p_identifier=0 this is default for a stored procedure, only action for insert
    runtime.errfound:=false;
    runtime.errstr:=' ';
    --
    -- Default columns as applicable
  if p_errval=0 then
    begin
      cur_colname:='P_PREPOSTRVWRECRDTYPCD';
      cur_colvalue:=P_PREPOSTRVWRECRDTYPCD;
      if P_PREPOSTRVWRECRDTYPCD is null then

        new_rec.PREPOSTRVWRECRDTYPCD:=rpad(' ',1);

      else
        new_rec.PREPOSTRVWRECRDTYPCD:=P_PREPOSTRVWRECRDTYPCD;
      end if;
      cur_colname:='P_PREPOSTRVWTRANSCD';
      cur_colvalue:=P_PREPOSTRVWTRANSCD;
      if P_PREPOSTRVWTRANSCD is null then

        new_rec.PREPOSTRVWTRANSCD:=rpad(' ',15);

      else
        new_rec.PREPOSTRVWTRANSCD:=P_PREPOSTRVWTRANSCD;
      end if;
      cur_colname:='P_PREPOSTRVWTRANSITROUTINGNMB';
      cur_colvalue:=P_PREPOSTRVWTRANSITROUTINGNMB;
      if P_PREPOSTRVWTRANSITROUTINGNMB is null then

        new_rec.PREPOSTRVWTRANSITROUTINGNMB:=rpad(' ',15);

      else
        new_rec.PREPOSTRVWTRANSITROUTINGNMB:=P_PREPOSTRVWTRANSITROUTINGNMB;
      end if;
      cur_colname:='P_PREPOSTRVWTRANSITROUTCHKDGT';
      cur_colvalue:=P_PREPOSTRVWTRANSITROUTCHKDGT;
      if P_PREPOSTRVWTRANSITROUTCHKDGT is null then

        new_rec.PREPOSTRVWTRANSITROUTINGCHKDGT:=rpad(' ',1);

      else
        new_rec.PREPOSTRVWTRANSITROUTINGCHKDGT:=P_PREPOSTRVWTRANSITROUTCHKDGT;
      end if;
      cur_colname:='P_PREPOSTRVWBNKACCTNMB';
      cur_colvalue:=P_PREPOSTRVWBNKACCTNMB;
      if P_PREPOSTRVWBNKACCTNMB is null then

        new_rec.PREPOSTRVWBNKACCTNMB:=' ';

      else
        new_rec.PREPOSTRVWBNKACCTNMB:=P_PREPOSTRVWBNKACCTNMB;
      end if;
      cur_colname:='P_PREPOSTRVWPYMTAMT';
      cur_colvalue:=P_PREPOSTRVWPYMTAMT;
      if P_PREPOSTRVWPYMTAMT is null then

        new_rec.PREPOSTRVWPYMTAMT:=0;

      else
        new_rec.PREPOSTRVWPYMTAMT:=P_PREPOSTRVWPYMTAMT;
      end if;
      cur_colname:='P_LOANNMB';
      cur_colvalue:=P_LOANNMB;
      if P_LOANNMB is null then
        raise_application_error(-20001,cur_colname||' may not be null');
      else
        new_rec.LOANNMB:=P_LOANNMB;
      end if;
      cur_colname:='P_PREPOSTRVWINDVLNM';
      cur_colvalue:=P_PREPOSTRVWINDVLNM;
      if P_PREPOSTRVWINDVLNM is null then

        new_rec.PREPOSTRVWINDVLNM:=' ';

      else
        new_rec.PREPOSTRVWINDVLNM:=P_PREPOSTRVWINDVLNM;
      end if;
      cur_colname:='P_PREPOSTRVWCOMPBNKDISC';
      cur_colvalue:=P_PREPOSTRVWCOMPBNKDISC;
      if P_PREPOSTRVWCOMPBNKDISC is null then

        new_rec.PREPOSTRVWCOMPBNKDISC:=rpad(' ',2);

      else
        new_rec.PREPOSTRVWCOMPBNKDISC:=P_PREPOSTRVWCOMPBNKDISC;
      end if;
      cur_colname:='P_PREPOSTRVWADDENDRECIND';
      cur_colvalue:=P_PREPOSTRVWADDENDRECIND;
      if P_PREPOSTRVWADDENDRECIND is null then

        new_rec.PREPOSTRVWADDENDRECIND:=0;

      else
        new_rec.PREPOSTRVWADDENDRECIND:=P_PREPOSTRVWADDENDRECIND;
      end if;
      cur_colname:='P_PREPOSTRVWTRACENMB';
      cur_colvalue:=P_PREPOSTRVWTRACENMB;
      if P_PREPOSTRVWTRACENMB is null then

        new_rec.PREPOSTRVWTRACENMB:=0;

      else
        new_rec.PREPOSTRVWTRACENMB:=P_PREPOSTRVWTRACENMB;
      end if;
      cur_colname:='P_PREPOSTRVWCMNT';
      cur_colvalue:=P_PREPOSTRVWCMNT;
      if P_PREPOSTRVWCMNT is null then

        new_rec.PREPOSTRVWCMNT:=' ';

      else
        new_rec.PREPOSTRVWCMNT:=P_PREPOSTRVWCMNT;
      end if;
    exception when others then
       p_errval:=sqlcode;
       p_errmsg:='Error - Oracle returned '||sqlerrm(sqlcode)||' using '
           ||cur_colname||' Value >'||cur_colvalue||'<';
    end;
  end if;
    keystouse:='(LOANNMB)=('||new_rec.LOANNMB||')';
    if p_errval=0 then
      begin
        insert into STGCSA.SOFVFTPOTBL values new_rec;
        p_RETVAL := SQL%ROWCOUNT;
      exception
      when dup_val_on_index then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Insert caused dupe on unique IX: '||sqlerrm(sqlcode);
      when others then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<3 then maxsev:=3; end if;
        p_errmsg:='Inserting record caught SQL error '||sqlerrm(sqlcode);
      end;
    end if;
  else /* P_IDENTIFIER was unexpected value */
    p_errval:=-20002;
    if maxsev<3 then maxsev:=3; end if;
    p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier
      ||' inserting record with key '||keystouse;
  end if;
  if p_errval=0 then
    runtime.logger(logged_msgid_retval,'STDLOG',101,
      'End SOFVFTPOINSTSP With 0 return, P_RETVAL='
      ||p_retval||' Record Key '||keystouse,p_userid,
      1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt,
      l_timestampfld,PROGRAM_NAME=>'SOFVFTPOINSTSP');
  else
    p_errmsg:='Error: '||P_ERRMSG||' in SOFVFTPOINSTSP Record Key '||keystouse
      ||' Build: 2018-11-07 11:35:51';
    ROLLBACK TO SOFVFTPOINSTSP;
    runtime.logger(logged_msgid_retval,'STDERR',103,
      p_errmsg,p_userid,maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,
      l_entrydt, l_timestampfld,PROGRAM_NAME=>'SOFVFTPOINSTSP',force_log_entry=>TRUE);
  end if;
EXCEPTION
WHEN OTHERS THEN
  p_errval:=SQLCODE;
  p_errmsg:=' In outer exception handler in SOFVFTPOINSTSP caught exception '
    ||sqlerrm(sqlcode)||' Record Key '||keystouse||' build 2018-11-07 11:35:51';
  ROLLBACK TO SOFVFTPOINSTSP;
  runtime.logger(logged_msgid_retval,'STDERR',20,p_errmsg,p_userid,3,
  PROGRAM_NAME=>'SOFVFTPOINSTSP',force_log_entry=>TRUE);
--
END SOFVFTPOINSTSP;
/


GRANT EXECUTE ON STGCSA.SOFVFTPOINSTSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.SOFVFTPOINSTSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.SOFVFTPOINSTSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.SOFVFTPOINSTSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.SOFVFTPOINSTSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.SOFVFTPOINSTSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.SOFVFTPOINSTSP TO STGCSADEVROLE;

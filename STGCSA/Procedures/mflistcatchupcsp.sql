DROP PROCEDURE STGCSA.MFLISTCATCHUPCSP;

CREATE OR REPLACE PROCEDURE STGCSA.MFLISTCATCHUPCSP (
    p_taskname VARCHAR2 := '', /* CATCHUP */
    p_runmode CHAR := 'C', /* C: current month, N: next month */
    p_purpose VARCHAR2 := 'MFLIST', /* MFUPDATE or MFLIST */
    P_CREATUSERID VARCHAR2 :=NULL,
    p_retval out number,
    p_errval out number,
    p_errmsg out varchar2,
	recordset1 OUT SYS_REFCURSOR,
    recordset2 OUT SYS_REFCURSOR,
    recordset3 OUT SYS_REFCURSOR)
as
    ---p1 NUMBER; p2 NUMBER; p3 VARCHAR2(200);
    v_sysdate date := sysdate;
	v_nulldate date := TO_DATE('1900-01-01', 'YYYY-MM-DD');
    v_status char(1);
    v_curtask varchar(200);
    v_lastupdtdt date;
    v_taskisopen char(1) := 'N';
	v_indexdate date := TRUNC(sysdate, 'mm');
    v_sysmsg varchar(2000);
    v_rowcount number;
    v_monthmode varchar(20) := 'Current Month';
BEGIN
    p_errval := 0;
    p_retval := -1000;
    p_errmsg := 'Success';

    SELECT stgcsa.isProcessAllowed( p_taskname ) into v_status from dual;

    IF v_status != 'Y' THEN
        SELECT JOBTYPEDESC INTO v_curtask FROM (
            SELECT REF.JOBTYPEDESC FROM MFJOBDETAILTBL DTL JOIN REFJOBTYPETBL REF ON REF.JOBTYPEID = DTL.JOBTYPEFK
            --WHERE DTL.TaskEndDt IS NULL
            ORDER BY DTL.JOBDETAILID DESC FETCH FIRST 1 ROWS ONLY
        );        
        p_errval := -22000;
        p_errmsg := 'Warning: Cannot generate MF List at this time because a [' || v_curtask || '] task is currently running.';
        DBMS_OUTPUT.PUT_LINE('P_RETVAL = ' || p_retval || ' / P_ERRVAL = ' || p_errval || ' / P_ERRMSG = ' || p_errmsg);
        RETURN;
    END IF;

	v_lastupdtdt := v_indexdate; -- always use the beginning of this month

    taskstartcsp(p_taskname, p_creatuserid, p_retval => p_retval, p_errval => p_errval, p_errmsg => p_errmsg );
    v_taskisopen := 'Y';
    DBMS_OUTPUT.PUT_LINE('P_RETVAL = ' || p_retval || ' / P_ERRVAL = ' || p_errval || ' / P_ERRMSG = ' || p_errmsg);

	EXECUTE IMMEDIATE 'TRUNCATE TABLE TempCUDPlanTBL';

	EXECUTE IMMEDIATE 'TRUNCATE TABLE TempCUDTbl';

	IF p_runmode = 'N' THEN
		v_indexdate := trunc(Add_Months(sysdate, 1), 'mm');
        v_monthmode := 'Next Month';
	END IF;

    /* now use this common routine shared with MFListCatchUpCSP and MFUpdtCatchUpCSP to get uniform result */
    MFCOMMONCATCHUPCSP(p_runmode, 'MFLIST');
    
    --OPEN recordset1 FOR SELECT * FROM TempCUDTbl ORDER BY LOANNMB, StartDt;
    --OPEN recordset1 FOR SELECT * FROM TempCUDTbl WHERE v_indexdate BETWEEN TRUNC(StartDt, 'mm') AND TRUNC(EndDt, 'mm')     ORDER BY LOANNMB;
    OPEN recordset1 FOR SELECT * FROM (
        SELECT * FROM TempCUDTbl WHERE v_indexdate BETWEEN TRUNC(StartDt, 'mm') AND TRUNC(EndDt, 'mm') OR PlanStatID IN (43,20)
        UNION
        SELECT * FROM TempCUDTbl t1 WHERE p_runmode = 'N'
        AND TRUNC(t1.EndDt, 'mm') = Add_Months(v_indexdate, -1) 
        AND t1.NewPlanStatID = 4 AND t1.NewPeriodStatID = 4 
        --AND NOT EXISTS (SELECT 1 FROM TempCUDTbl t2 WHERE t2.PlanTransID = t1.PlanTransID AND TRUNC(t2.StartDt, 'mm') > TRUNC(t1.EndDt, 'mm'))
    ) bulkset    
    ORDER BY LOANNMB, PLANTRANSID, STARTDT;
    
    v_rowcount := sql%rowcount;
    
    IF v_rowcount > 0 THEN
        v_sysmsg := v_rowcount || ' records are selected from the MFLIST task.';
    ELSE
        v_sysmsg := 'There is no more un-processed plan for ' || v_monthmode || '.';
    END IF;

    OPEN recordset2 FOR SELECT * FROM (
        WITH pivot_data AS (
            select VW.LoanNMB, VW.PlanStatus, 
            TRUNC(VW.StartDT, 'MM') as StartDT, ADD_MONTHS(TRUNC(VW.EndDt,'MM'), 1) - 1 as EndDt, VW.PymtAmt, 
            row_number() over (PARTITION BY VW.LoanNMB ORDER BY VW.LoanNMB, StartDt) rowx  
            from TempCUDTbl VW 
            WHERE VW.CUDPDTYPID = 3 AND VW.UpdateInd = 'Y' AND VW.PlanStatID = 35
        )
        SELECT *
        FROM pivot_data pd1
        JOIN (SELECT LoanNMB AS LoanNBR, MAX(rowx) As MaxGroup FROM pivot_data pd2 GROUP BY LoanNMB) pd3
        ON pd1.LoanNMB = pd3.LoanNBR
        PIVOT (
            MAX(STARTDT) AS Start_DT,
            MAX(ENDDT) AS End_DT,
            MAX(PYMTAMT) AS AMT
            FOR ROWX IN (1 as p1, 2 as p2, 3 as p3, 4 as p4, 5 as p5, 6 as p6, 7 as p7, 8 as p8, 9 as p9, 10 as p10, 11 as p11, 12 as p12, 13 as p13, 14 as p14, 15 as p15)
        )
    ) bulkset
    ORDER BY LOANNMB;

    OPEN recordset3 FOR SELECT * FROM (
        WITH pivot_data AS (
            select VW.LoanNMB, VW.PlanStatus,
            TRUNC(VW.StartDT, 'MM') as StartDT, ADD_MONTHS(TRUNC(VW.EndDt,'MM'), 1) - 1 as EndDt, VW.PymtAmt, 
            row_number() over (PARTITION BY VW.LoanNMB ORDER BY VW.LoanNMB, StartDt) rowx  
            from TempCUDTbl VW 
            WHERE VW.CUDPDTYPID = 2 AND VW.UpdateInd = 'Y' AND VW.PlanStatID IN (2, 35)
        )
        SELECT * 
        FROM pivot_data pd1
        JOIN (SELECT LoanNMB AS LoanNBR, MAX(rowx) As MaxGroup FROM pivot_data pd2 GROUP BY LoanNMB) pd3
        ON pd1.LoanNMB = pd3.LoanNBR
        PIVOT (
            MAX(STARTDT) AS Start_DT,
            MAX(ENDDT) AS End_DT,
            MAX(PYMTAMT) AS AMT
            FOR ROWX IN (1 as p1, 2 as p2, 3 as p3, 4 as p4, 5 as p5, 6 as p6, 7 as p7, 8 as p8, 9 as p9, 10 as p10, 11 as p11, 12 as p12, 13 as p13, 14 as p14, 15 as p15)
        )
    ) bulkset
    ORDER BY LOANNMB;

    TaskEndCSP(p_taskname, p_retval => p_retval, p_errval => p_errval, p_errmsg => p_errmsg );
    v_taskisopen := 'N';
    DBMS_OUTPUT.PUT_LINE('P_RETVAL = ' || p_retval || ' / P_ERRVAL = ' || p_errval || ' / P_ERRMSG = ' || p_errmsg);

    p_errmsg := v_sysmsg;
    
EXCEPTION
WHEN OTHERS THEN
  p_errval:=SQLCODE;
  p_errmsg:=SQLERRM(SQLCODE);
  IF v_taskisopen = 'Y' THEN
    TaskEndCSP(p_taskname, p_retval => p_retval, p_errval => p_errval, p_errmsg => p_errmsg );
  END IF;  
  
  --RAISE;
END MFLISTCATCHUPCSP;
/


GRANT EXECUTE ON STGCSA.MFLISTCATCHUPCSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.MFLISTCATCHUPCSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.MFLISTCATCHUPCSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.MFLISTCATCHUPCSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.MFLISTCATCHUPCSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.MFLISTCATCHUPCSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.MFLISTCATCHUPCSP TO STGCSADEVROLE;

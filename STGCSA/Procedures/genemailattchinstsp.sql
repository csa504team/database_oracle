DROP PROCEDURE STGCSA.GENEMAILATTCHINSTSP;

CREATE OR REPLACE PROCEDURE STGCSA.GENEMAILATTCHINSTSP(
    p_retval out number,
    p_errval out number,
    p_errmsg  out varchar2,
    p_identifier number:=0,
    p_userid varchar2
   ,p_EMAILATTCHID VARCHAR2:=null
   ,p_EMAILID VARCHAR2:=null
   ,p_ATTCHFILENM VARCHAR2:=null
   ,p_TIMESTAMPFLD DATE:=null
) as
 /*
  Created on: 2018-11-07 11:33:49
  Created by: GENR
  Crerated from template stdtblins_template v3.05 13 Sep 2018 on 2018-11-07 11:33:49
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  Procedure performs INSERT on STGCSA.GENEMAILATTCHTBLfor P_IDENTIFIER=0
*/
  rundate date;
  gn global_name.global_name%type;
  new_rec STGCSA.GENEMAILATTCHTBL%rowtype;
  logged_msgid_retval number;
  audretval number;
  crossover_active char(1);
  l_LOANNMB char(10):=null;
  l_TRANSID number:=null;
  l_dbmdl varchar2(10):=null;
  l_entrydt date:=null;
  l_userid  varchar2(32):=null;
  l_usernm  varchar2(199):=null;
  l_timestampfld date:=null;
  cur_col_name varchar2(100):=null;
  maxsev number:=0;
  holdstring varchar2(4000);
  keystouse varchar2(1000);
  -- std date for column defaulting
  JAN_1_1900  constant date:=to_date('JAN-01-1900','MON-DD-YYYY');
  cur_colvalue varchar2(4095);
  cur_colname varchar2(30);
-- Function used to initialize EMAILATTCHID from sequence EMAILATTCHIDSEQ
Function NEXTVAL_EMAILATTCHIDSEQ return number is
  N number;
begin
  select EMAILATTCHIDSEQ.nextval into n from dual;
  return n;
end;
-- Main body begins here
begin
  -- setup
  savepoint GENEMAILATTCHINSTSP;
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  runtime.logger(logged_msgid_retval,'STDLOG',100,'Init GENEMAILATTCHINSTSP',p_userid,4,
    logtxt1=>'GENEMAILATTCHINSTSP',logtxt2=>'stdtblins_template v3.05 13 Sep 2018',
    PROGRAM_NAME=>'GENEMAILATTCHINSTSP');
  select global_name, sysdate into gn, rundate from global_name;
  begin
    cur_colname:='TIMESTAMPFLD';
    cur_colvalue:=P_TIMESTAMPFLD;
    l_TIMESTAMPFLD:=P_TIMESTAMPFLD;
  exception when others then
    p_errval:=sqlcode;
    p_errmsg:='Error '||sqlerrm(sqlcode)||' using '||cur_colname||' Value >'||cur_colvalue||'<';
  end;
  new_rec.creatuserid:=p_userid;
  new_rec.creatdt:=sysdate;
  new_rec.lastupdtuserid:=p_userid;
  new_rec.lastupdtdt:=sysdate;
  if p_identifier=0 then
    -- p_identifier=0 this is default for a stored procedure, only action for insert
    runtime.errfound:=false;
    runtime.errstr:=' ';
    --
    -- Default columns as applicable
  if p_errval=0 then
    begin
      cur_colname:='P_EMAILATTCHID';
      cur_colvalue:=P_EMAILATTCHID;
      if P_EMAILATTCHID is null then

        new_rec.EMAILATTCHID:=NEXTVAL_EMAILATTCHIDSEQ();

      else
        new_rec.EMAILATTCHID:=P_EMAILATTCHID;
      end if;
      cur_colname:='P_EMAILID';
      cur_colvalue:=P_EMAILID;
      if P_EMAILID is null then

        new_rec.EMAILID:=0;

      else
        new_rec.EMAILID:=P_EMAILID;
      end if;
      cur_colname:='P_ATTCHFILENM';
      cur_colvalue:=P_ATTCHFILENM;
      if P_ATTCHFILENM is null then

        new_rec.ATTCHFILENM:=' ';

      else
        new_rec.ATTCHFILENM:=P_ATTCHFILENM;
      end if;
      cur_colname:='P_TIMESTAMPFLD';
      cur_colvalue:=P_TIMESTAMPFLD;
      if P_TIMESTAMPFLD is null then

        new_rec.TIMESTAMPFLD:=jan_1_1900;

      else
        new_rec.TIMESTAMPFLD:=P_TIMESTAMPFLD;
      end if;
    exception when others then
       p_errval:=sqlcode;
       p_errmsg:='Error - Oracle returned '||sqlerrm(sqlcode)||' using '
           ||cur_colname||' Value >'||cur_colvalue||'<';
    end;
  end if;
    keystouse:='(EMAILATTCHID)=('||new_rec.EMAILATTCHID||')';
    if p_errval=0 then
      begin
        insert into STGCSA.GENEMAILATTCHTBL values new_rec;
        p_RETVAL := SQL%ROWCOUNT;
      exception
      when dup_val_on_index then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Insert caused dupe on unique IX: '||sqlerrm(sqlcode);
      when others then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<3 then maxsev:=3; end if;
        p_errmsg:='Inserting record caught SQL error '||sqlerrm(sqlcode);
      end;
    end if;
  else /* P_IDENTIFIER was unexpected value */
    p_errval:=-20002;
    if maxsev<3 then maxsev:=3; end if;
    p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier
      ||' inserting record with key '||keystouse;
  end if;
  if p_errval=0 then
    runtime.logger(logged_msgid_retval,'STDLOG',101,
      'End GENEMAILATTCHINSTSP With 0 return, P_RETVAL='
      ||p_retval||' Record Key '||keystouse,p_userid,
      1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt,
      l_timestampfld,PROGRAM_NAME=>'GENEMAILATTCHINSTSP');
  else
    p_errmsg:='Error: '||P_ERRMSG||' in GENEMAILATTCHINSTSP Record Key '||keystouse
      ||' Build: 2018-11-07 11:33:48';
    ROLLBACK TO GENEMAILATTCHINSTSP;
    runtime.logger(logged_msgid_retval,'STDERR',103,
      p_errmsg,p_userid,maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,
      l_entrydt, l_timestampfld,PROGRAM_NAME=>'GENEMAILATTCHINSTSP',force_log_entry=>TRUE);
  end if;
EXCEPTION
WHEN OTHERS THEN
  p_errval:=SQLCODE;
  p_errmsg:=' In outer exception handler in GENEMAILATTCHINSTSP caught exception '
    ||sqlerrm(sqlcode)||' Record Key '||keystouse||' build 2018-11-07 11:33:48';
  ROLLBACK TO GENEMAILATTCHINSTSP;
  runtime.logger(logged_msgid_retval,'STDERR',20,p_errmsg,p_userid,3,
  PROGRAM_NAME=>'GENEMAILATTCHINSTSP',force_log_entry=>TRUE);
--
END GENEMAILATTCHINSTSP;
/


GRANT EXECUTE ON STGCSA.GENEMAILATTCHINSTSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.GENEMAILATTCHINSTSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.GENEMAILATTCHINSTSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.GENEMAILATTCHINSTSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.GENEMAILATTCHINSTSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.GENEMAILATTCHINSTSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.GENEMAILATTCHINSTSP TO STGCSADEVROLE;

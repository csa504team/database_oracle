DROP PROCEDURE STGCSA.RUNTIME_LOGGER;

CREATE OR REPLACE PROCEDURE STGCSA.runtime_logger(
  logentryid out number,
  msg_cat varchar2,
  msg_id number,
  msg_txt varchar2,
  userid varchar2,
  log_sev number:=1,
  usernm varchar2:=null,
  loannmb char:=null,
  transid number:=null,
  dbmdl varchar:=null,
  entrydt date:=null,
  timestampfield date:=null,
  logtxt1 varchar2:=null,
  logtxt2 varchar2:=null,  
  logtxt3 varchar2:=null,  
  logtxt4 varchar2:=null,  
  logtxt5 varchar2:=null,  
  lognmb1 number:=null,
  lognmb2 number:=null,  
  lognmb3 number:=null,  
  lognmb4 number:=null,  
  lognmb5 number:=null,  
  logdt1 date:=null,
  logdt2 date:=null,  
  logdt3 date:=null,  
  logdt4 date:=null,  
  logdt5 date:=null) as
begin
  runtime.logger(logentryid, msg_cat, msg_id, msg_txt, userid, log_sev, usernm, 
      loannmb, transid, dbmdl, entrydt, timestampfield, 
      logtxt1, logtxt2, logtxt3, logtxt4, logtxt5,
      lognmb1, lognmb2, lognmb3, lognmb4, lognmb5,
      logdt1, logdt2, logdt3, logdt4, logdt5);
  return;
end;
/


GRANT EXECUTE ON STGCSA.RUNTIME_LOGGER TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.RUNTIME_LOGGER TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.RUNTIME_LOGGER TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.RUNTIME_LOGGER TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.RUNTIME_LOGGER TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.RUNTIME_LOGGER TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.RUNTIME_LOGGER TO STGCSADEVROLE;

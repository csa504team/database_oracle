DROP PROCEDURE STGCSA.GENPACSIMPTDTLINSTSP;

CREATE OR REPLACE PROCEDURE STGCSA.GENPACSIMPTDTLINSTSP(
    p_retval out number,
    p_errval out number,
    p_errmsg  out varchar2,
    p_identifier number:=0,
    p_userid varchar2
   ,p_PACSIMPTDTLID VARCHAR2:=null
   ,p_PACSIMPTID VARCHAR2:=null
   ,p_RQSTID VARCHAR2:=null
   ,p_RELTDTBLNM VARCHAR2:=null
   ,p_RELTDTBLREF VARCHAR2:=null
   ,p_RQSTTYP VARCHAR2:=null
   ,p_PYMTTYP VARCHAR2:=null
   ,p_TRANSID VARCHAR2:=null
   ,p_TRANSAMT VARCHAR2:=null
   ,p_TRANSTYP VARCHAR2:=null
   ,p_PACSACCT VARCHAR2:=null
   ,p_ACCTNM VARCHAR2:=null
   ,p_INTRNLEXTRNLACCT VARCHAR2:=null
   ,p_TRANAU VARCHAR2:=null
   ,p_ABABNKNMB VARCHAR2:=null
   ,p_ABABNKNM VARCHAR2:=null
   ,p_SWIFTBNK VARCHAR2:=null
   ,p_INTRMEDRYABANMB VARCHAR2:=null
   ,p_INTRMEDRYSWIFT VARCHAR2:=null
   ,p_BNKIRC VARCHAR2:=null
   ,p_BNKIBAN VARCHAR2:=null
   ,p_TRANSINFO VARCHAR2:=null
   ,p_STATCD VARCHAR2:=null
   ,p_TRANSDT DATE:=null
   ,p_POSTEDDT DATE:=null
   ,p_POSTERLOC VARCHAR2:=null
   ,p_PACSIMPTPTIC VARCHAR2:=null
   ,p_RCPTCD VARCHAR2:=null
   ,p_DISBCD VARCHAR2:=null
   ,p_PRININC VARCHAR2:=null
   ,p_SEIBTCHID VARCHAR2:=null
   ,p_SEITRANTYP VARCHAR2:=null
   ,p_RQSTAU VARCHAR2:=null
   ,p_INITBY VARCHAR2:=null
   ,p_APPVBY VARCHAR2:=null
   ,p_ACHADDENDA VARCHAR2:=null
   ,p_TIMESTAMPFLD DATE:=null
) as
 /*
  Created on: 2018-11-07 11:34:11
  Created by: GENR
  Crerated from template stdtblins_template v3.05 13 Sep 2018 on 2018-11-07 11:34:11
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  Procedure performs INSERT on STGCSA.GENPACSIMPTDTLTBLfor P_IDENTIFIER=0
*/
  rundate date;
  gn global_name.global_name%type;
  new_rec STGCSA.GENPACSIMPTDTLTBL%rowtype;
  logged_msgid_retval number;
  audretval number;
  crossover_active char(1);
  l_LOANNMB char(10):=null;
  l_TRANSID number:=null;
  l_dbmdl varchar2(10):=null;
  l_entrydt date:=null;
  l_userid  varchar2(32):=null;
  l_usernm  varchar2(199):=null;
  l_timestampfld date:=null;
  cur_col_name varchar2(100):=null;
  maxsev number:=0;
  holdstring varchar2(4000);
  keystouse varchar2(1000);
  -- std date for column defaulting
  JAN_1_1900  constant date:=to_date('JAN-01-1900','MON-DD-YYYY');
  cur_colvalue varchar2(4095);
  cur_colname varchar2(30);
-- Function used to initialize PACSIMPTDTLID from sequence PACSIMPTDTLIDSEQ
Function NEXTVAL_PACSIMPTDTLIDSEQ return number is
  N number;
begin
  select PACSIMPTDTLIDSEQ.nextval into n from dual;
  return n;
end;
-- Main body begins here
begin
  -- setup
  savepoint GENPACSIMPTDTLINSTSP;
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  runtime.logger(logged_msgid_retval,'STDLOG',100,'Init GENPACSIMPTDTLINSTSP',p_userid,4,
    logtxt1=>'GENPACSIMPTDTLINSTSP',logtxt2=>'stdtblins_template v3.05 13 Sep 2018',
    PROGRAM_NAME=>'GENPACSIMPTDTLINSTSP');
  select global_name, sysdate into gn, rundate from global_name;
  begin
    cur_colname:='TRANSID';
    cur_colvalue:=P_TRANSID;
    l_TRANSID:=P_TRANSID;    cur_colname:='TIMESTAMPFLD';
    cur_colvalue:=P_TIMESTAMPFLD;
    l_TIMESTAMPFLD:=P_TIMESTAMPFLD;
  exception when others then
    p_errval:=sqlcode;
    p_errmsg:='Error '||sqlerrm(sqlcode)||' using '||cur_colname||' Value >'||cur_colvalue||'<';
  end;
  new_rec.creatuserid:=p_userid;
  new_rec.creatdt:=sysdate;
  new_rec.lastupdtuserid:=p_userid;
  new_rec.lastupdtdt:=sysdate;
  if p_identifier=0 then
    -- p_identifier=0 this is default for a stored procedure, only action for insert
    runtime.errfound:=false;
    runtime.errstr:=' ';
    --
    -- Default columns as applicable
  if p_errval=0 then
    begin
      cur_colname:='P_PACSIMPTDTLID';
      cur_colvalue:=P_PACSIMPTDTLID;
      if P_PACSIMPTDTLID is null then

        new_rec.PACSIMPTDTLID:=NEXTVAL_PACSIMPTDTLIDSEQ();

      else
        new_rec.PACSIMPTDTLID:=P_PACSIMPTDTLID;
      end if;
      cur_colname:='P_PACSIMPTID';
      cur_colvalue:=P_PACSIMPTID;
      if P_PACSIMPTID is null then

        new_rec.PACSIMPTID:=0;

      else
        new_rec.PACSIMPTID:=P_PACSIMPTID;
      end if;
      cur_colname:='P_RQSTID';
      cur_colvalue:=P_RQSTID;
      if P_RQSTID is null then

        new_rec.RQSTID:=' ';

      else
        new_rec.RQSTID:=P_RQSTID;
      end if;
      cur_colname:='P_RELTDTBLNM';
      cur_colvalue:=P_RELTDTBLNM;
      if P_RELTDTBLNM is null then

        new_rec.RELTDTBLNM:=' ';

      else
        new_rec.RELTDTBLNM:=P_RELTDTBLNM;
      end if;
      cur_colname:='P_RELTDTBLREF';
      cur_colvalue:=P_RELTDTBLREF;
      if P_RELTDTBLREF is null then

        new_rec.RELTDTBLREF:=0;

      else
        new_rec.RELTDTBLREF:=P_RELTDTBLREF;
      end if;
      cur_colname:='P_RQSTTYP';
      cur_colvalue:=P_RQSTTYP;
      if P_RQSTTYP is null then

        new_rec.RQSTTYP:=' ';

      else
        new_rec.RQSTTYP:=P_RQSTTYP;
      end if;
      cur_colname:='P_PYMTTYP';
      cur_colvalue:=P_PYMTTYP;
      if P_PYMTTYP is null then

        new_rec.PYMTTYP:=' ';

      else
        new_rec.PYMTTYP:=P_PYMTTYP;
      end if;
      cur_colname:='P_TRANSID';
      cur_colvalue:=P_TRANSID;
      if P_TRANSID is null then

        new_rec.TRANSID:=' ';

      else
        new_rec.TRANSID:=P_TRANSID;
      end if;
      cur_colname:='P_TRANSAMT';
      cur_colvalue:=P_TRANSAMT;
      if P_TRANSAMT is null then

        new_rec.TRANSAMT:=' ';

      else
        new_rec.TRANSAMT:=P_TRANSAMT;
      end if;
      cur_colname:='P_TRANSTYP';
      cur_colvalue:=P_TRANSTYP;
      if P_TRANSTYP is null then

        new_rec.TRANSTYP:=' ';

      else
        new_rec.TRANSTYP:=P_TRANSTYP;
      end if;
      cur_colname:='P_PACSACCT';
      cur_colvalue:=P_PACSACCT;
      if P_PACSACCT is null then

        new_rec.PACSACCT:=' ';

      else
        new_rec.PACSACCT:=P_PACSACCT;
      end if;
      cur_colname:='P_ACCTNM';
      cur_colvalue:=P_ACCTNM;
      if P_ACCTNM is null then

        new_rec.ACCTNM:=' ';

      else
        new_rec.ACCTNM:=P_ACCTNM;
      end if;
      cur_colname:='P_INTRNLEXTRNLACCT';
      cur_colvalue:=P_INTRNLEXTRNLACCT;
      if P_INTRNLEXTRNLACCT is null then

        new_rec.INTRNLEXTRNLACCT:=' ';

      else
        new_rec.INTRNLEXTRNLACCT:=P_INTRNLEXTRNLACCT;
      end if;
      cur_colname:='P_TRANAU';
      cur_colvalue:=P_TRANAU;
      if P_TRANAU is null then

        new_rec.TRANAU:=' ';

      else
        new_rec.TRANAU:=P_TRANAU;
      end if;
      cur_colname:='P_ABABNKNMB';
      cur_colvalue:=P_ABABNKNMB;
      if P_ABABNKNMB is null then

        new_rec.ABABNKNMB:=' ';

      else
        new_rec.ABABNKNMB:=P_ABABNKNMB;
      end if;
      cur_colname:='P_ABABNKNM';
      cur_colvalue:=P_ABABNKNM;
      if P_ABABNKNM is null then

        new_rec.ABABNKNM:=' ';

      else
        new_rec.ABABNKNM:=P_ABABNKNM;
      end if;
      cur_colname:='P_SWIFTBNK';
      cur_colvalue:=P_SWIFTBNK;
      if P_SWIFTBNK is null then

        new_rec.SWIFTBNK:=' ';

      else
        new_rec.SWIFTBNK:=P_SWIFTBNK;
      end if;
      cur_colname:='P_INTRMEDRYABANMB';
      cur_colvalue:=P_INTRMEDRYABANMB;
      if P_INTRMEDRYABANMB is null then

        new_rec.INTRMEDRYABANMB:=' ';

      else
        new_rec.INTRMEDRYABANMB:=P_INTRMEDRYABANMB;
      end if;
      cur_colname:='P_INTRMEDRYSWIFT';
      cur_colvalue:=P_INTRMEDRYSWIFT;
      if P_INTRMEDRYSWIFT is null then

        new_rec.INTRMEDRYSWIFT:=' ';

      else
        new_rec.INTRMEDRYSWIFT:=P_INTRMEDRYSWIFT;
      end if;
      cur_colname:='P_BNKIRC';
      cur_colvalue:=P_BNKIRC;
      if P_BNKIRC is null then

        new_rec.BNKIRC:=' ';

      else
        new_rec.BNKIRC:=P_BNKIRC;
      end if;
      cur_colname:='P_BNKIBAN';
      cur_colvalue:=P_BNKIBAN;
      if P_BNKIBAN is null then

        new_rec.BNKIBAN:=' ';

      else
        new_rec.BNKIBAN:=P_BNKIBAN;
      end if;
      cur_colname:='P_TRANSINFO';
      cur_colvalue:=P_TRANSINFO;
      if P_TRANSINFO is null then

        new_rec.TRANSINFO:=' ';

      else
        new_rec.TRANSINFO:=P_TRANSINFO;
      end if;
      cur_colname:='P_STATCD';
      cur_colvalue:=P_STATCD;
      if P_STATCD is null then

        new_rec.STATCD:=' ';

      else
        new_rec.STATCD:=P_STATCD;
      end if;
      cur_colname:='P_TRANSDT';
      cur_colvalue:=P_TRANSDT;
      if P_TRANSDT is null then

        new_rec.TRANSDT:=jan_1_1900;

      else
        new_rec.TRANSDT:=P_TRANSDT;
      end if;
      cur_colname:='P_POSTEDDT';
      cur_colvalue:=P_POSTEDDT;
      if P_POSTEDDT is null then

        new_rec.POSTEDDT:=jan_1_1900;

      else
        new_rec.POSTEDDT:=P_POSTEDDT;
      end if;
      cur_colname:='P_POSTERLOC';
      cur_colvalue:=P_POSTERLOC;
      if P_POSTERLOC is null then

        new_rec.POSTERLOC:=' ';

      else
        new_rec.POSTERLOC:=P_POSTERLOC;
      end if;
      cur_colname:='P_PACSIMPTPTIC';
      cur_colvalue:=P_PACSIMPTPTIC;
      if P_PACSIMPTPTIC is null then

        new_rec.PACSIMPTPTIC:=' ';

      else
        new_rec.PACSIMPTPTIC:=P_PACSIMPTPTIC;
      end if;
      cur_colname:='P_RCPTCD';
      cur_colvalue:=P_RCPTCD;
      if P_RCPTCD is null then

        new_rec.RCPTCD:=' ';

      else
        new_rec.RCPTCD:=P_RCPTCD;
      end if;
      cur_colname:='P_DISBCD';
      cur_colvalue:=P_DISBCD;
      if P_DISBCD is null then

        new_rec.DISBCD:=' ';

      else
        new_rec.DISBCD:=P_DISBCD;
      end if;
      cur_colname:='P_PRININC';
      cur_colvalue:=P_PRININC;
      if P_PRININC is null then

        new_rec.PRININC:=' ';

      else
        new_rec.PRININC:=P_PRININC;
      end if;
      cur_colname:='P_SEIBTCHID';
      cur_colvalue:=P_SEIBTCHID;
      if P_SEIBTCHID is null then

        new_rec.SEIBTCHID:=' ';

      else
        new_rec.SEIBTCHID:=P_SEIBTCHID;
      end if;
      cur_colname:='P_SEITRANTYP';
      cur_colvalue:=P_SEITRANTYP;
      if P_SEITRANTYP is null then

        new_rec.SEITRANTYP:=' ';

      else
        new_rec.SEITRANTYP:=P_SEITRANTYP;
      end if;
      cur_colname:='P_RQSTAU';
      cur_colvalue:=P_RQSTAU;
      if P_RQSTAU is null then

        new_rec.RQSTAU:=' ';

      else
        new_rec.RQSTAU:=P_RQSTAU;
      end if;
      cur_colname:='P_INITBY';
      cur_colvalue:=P_INITBY;
      if P_INITBY is null then

        new_rec.INITBY:=' ';

      else
        new_rec.INITBY:=P_INITBY;
      end if;
      cur_colname:='P_APPVBY';
      cur_colvalue:=P_APPVBY;
      if P_APPVBY is null then

        new_rec.APPVBY:=' ';

      else
        new_rec.APPVBY:=P_APPVBY;
      end if;
      cur_colname:='P_ACHADDENDA';
      cur_colvalue:=P_ACHADDENDA;
      if P_ACHADDENDA is null then

        new_rec.ACHADDENDA:=' ';

      else
        new_rec.ACHADDENDA:=P_ACHADDENDA;
      end if;
      cur_colname:='P_TIMESTAMPFLD';
      cur_colvalue:=P_TIMESTAMPFLD;
      if P_TIMESTAMPFLD is null then

        new_rec.TIMESTAMPFLD:=jan_1_1900;

      else
        new_rec.TIMESTAMPFLD:=P_TIMESTAMPFLD;
      end if;
    exception when others then
       p_errval:=sqlcode;
       p_errmsg:='Error - Oracle returned '||sqlerrm(sqlcode)||' using '
           ||cur_colname||' Value >'||cur_colvalue||'<';
    end;
  end if;
    keystouse:='(PACSIMPTDTLID)=('||new_rec.PACSIMPTDTLID||')';
    if p_errval=0 then
      begin
        insert into STGCSA.GENPACSIMPTDTLTBL values new_rec;
        p_RETVAL := SQL%ROWCOUNT;
      exception
      when dup_val_on_index then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Insert caused dupe on unique IX: '||sqlerrm(sqlcode);
      when others then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<3 then maxsev:=3; end if;
        p_errmsg:='Inserting record caught SQL error '||sqlerrm(sqlcode);
      end;
    end if;
  else /* P_IDENTIFIER was unexpected value */
    p_errval:=-20002;
    if maxsev<3 then maxsev:=3; end if;
    p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier
      ||' inserting record with key '||keystouse;
  end if;
  if p_errval=0 then
    runtime.logger(logged_msgid_retval,'STDLOG',101,
      'End GENPACSIMPTDTLINSTSP With 0 return, P_RETVAL='
      ||p_retval||' Record Key '||keystouse,p_userid,
      1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt,
      l_timestampfld,PROGRAM_NAME=>'GENPACSIMPTDTLINSTSP');
  else
    p_errmsg:='Error: '||P_ERRMSG||' in GENPACSIMPTDTLINSTSP Record Key '||keystouse
      ||' Build: 2018-11-07 11:34:11';
    ROLLBACK TO GENPACSIMPTDTLINSTSP;
    runtime.logger(logged_msgid_retval,'STDERR',103,
      p_errmsg,p_userid,maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,
      l_entrydt, l_timestampfld,PROGRAM_NAME=>'GENPACSIMPTDTLINSTSP',force_log_entry=>TRUE);
  end if;
EXCEPTION
WHEN OTHERS THEN
  p_errval:=SQLCODE;
  p_errmsg:=' In outer exception handler in GENPACSIMPTDTLINSTSP caught exception '
    ||sqlerrm(sqlcode)||' Record Key '||keystouse||' build 2018-11-07 11:34:11';
  ROLLBACK TO GENPACSIMPTDTLINSTSP;
  runtime.logger(logged_msgid_retval,'STDERR',20,p_errmsg,p_userid,3,
  PROGRAM_NAME=>'GENPACSIMPTDTLINSTSP',force_log_entry=>TRUE);
--
END GENPACSIMPTDTLINSTSP;
/


GRANT EXECUTE ON STGCSA.GENPACSIMPTDTLINSTSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.GENPACSIMPTDTLINSTSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.GENPACSIMPTDTLINSTSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.GENPACSIMPTDTLINSTSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.GENPACSIMPTDTLINSTSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.GENPACSIMPTDTLINSTSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.GENPACSIMPTDTLINSTSP TO STGCSADEVROLE;

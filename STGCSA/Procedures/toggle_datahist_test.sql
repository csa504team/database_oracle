DROP PROCEDURE STGCSA.TOGGLE_DATAHIST_TEST;

CREATE OR REPLACE PROCEDURE STGCSA.toggle_datahist_test as
begin
  dbms_output.put_line('DATAHIST: '||runtime.datahist_logging_on_YN);
end;
/

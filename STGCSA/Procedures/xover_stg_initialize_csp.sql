DROP PROCEDURE STGCSA.XOVER_STG_INITIALIZE_CSP;

CREATE OR REPLACE PROCEDURE STGCSA.xover_stg_initialize_csp as
  -- V1.5 11 Feb 2019 - JL - first tested version
  -- V1.6 14 Mar 2019 - JL - Add outer exception handler to record error in XOVER_STATUS
  prog varchar2(40):='XOVER_STG_INITIALIZE_CSP';
  ver  varchar2(40):='V1.6 14 Mar 2019, JL';
  Reset_date date:=to_date('19000101','yyyymmdd');
  logentryid number;
  msgtxt varchar2(4000);
  statusrec stgcsa.xover_status%rowtype;
begin
  select * into statusrec from stgcsa.xover_status;
  msgtxt:='Xover init.  Last Event: '||statusrec.xover_last_event
    ||' Recd prior events:'||' CI:'||to_char(statusrec.CSA_INIT_dt,'mm-dd hh24:mi:ss')
    ||' CM:'||to_char(statusrec.CSA_MERGE_end_dt,'mm-dd hh24:mi:ss')
    ||' CX:'||to_char(statusrec.CSA_EXPORT_START_dt,'mm-dd hh24:mi:ss')
    ||' SI:'||to_char(statusrec.STGCSA_INIT_dt,'mm-dd hh24:mi:ss')
    ||' SM:'||to_char(statusrec. STGCSA_MERGE_end_dt,'mm-dd hh24:mi:ss')
    ||' SX:'||to_char(statusrec.STGCSA_EXPORT_START_dt,'mm-dd hh24:mi:ss');
  runtime.logger(logentryid,'STDXVR',230,msgtxt,user,4,
    program_name=>prog||' '||ver);
  If statusrec.xover_last_event<>'SM' then
    msgtxt:='Error - Crossover status last event is: '||statusrec.xover_last_event
    ||' is not correct to initiate crossover. '
      ||' Wait for current crossover to complete or run a Crossover RESET.';
    update stgcsa.xover_status
      set err_step='SI', err_dt=sysdate, err_msg=msgtxt;
    runtime.logger(logentryid,'STDXVR',233,msgtxt,user,3,
      program_name=>prog||' '||ver);
    runtime.logger(logentryid,'STDXVR',232,prog||' ended with error',user,5,
      program_name=>prog||' '||ver);
    commit;
    raise_application_error(-20233,'STGCSA '||msgtxt);
  end if;
  update stgcsa.xover_status set xover_last_event='SI',
    stgcsa_init_dt=sysdate,err_step=null,err_dt=null,err_msg=null;
  commit;
  runtime.logger(logentryid,'STDXVR',231,'Normal Completion',user,5,
    program_name=>prog||' '||ver,force_log_entry=>true);
exception when others then
  -- outer exception handler for uncaught errors
  msgtxt:=sqlerrm(sqlcode)||' caught in '||prog;
  update stgcsa.xover_status set
    err_step='SI',err_dt=sysdate,
    err_msg=msgtxt;
  commit;
  raise;
end;
/

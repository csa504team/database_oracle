DROP PROCEDURE STGCSA.JOBQUEUPDTSP;

CREATE OR REPLACE PROCEDURE STGCSA.JobQueUpdTSP
/*  Date         : 11/01/2017
    Programmer   : Prasad Mente
    Updated      : Rick Wu (11/14/2017)
    Remarks      :
    Parameters   :  No.   Name        Type     Description
*********************************************************************************
Revised By      Date        Commments
*********************************************************************************
*********************************************************************************
*/
( p_JobTypCd varchar2 := NULL,
  p_StatCd varchar2 := NULL,
  p_CommTxt varchar2 := NULL,
  p_CreatUserId varchar2 := NULL
)
AS
    v_Cnt number(3);
BEGIN
    select count(*) into v_Cnt from stgcsa.JobQueTbl
    where JobTypCd in ('02','04')
    and   StatCd = 'I'
    and   StrtDt IS NULL;

    IF v_Cnt > 0 and p_StatCd = 'P'
    
    THEN
       UPDATE stgcsa.JobQueTbl
       SET StatCd = p_StatCd,
           StrtDt = sysdate,
           CommTxt = p_CommTxt
       WHERE StrtDt IS NULL
       AND   StatCd = 'I'
       AND   JobTypCd = p_JobTypCd;
    ELSIF p_JobTypCd in ('02','04') and p_StatCd in ('F','S')
    THEN
       UPDATE stgcsa.JobQueTbl
       SET StatCd = p_StatCd,
           EndDt = sysdate,
           CommTxt = p_CommTxt
       WHERE EndDt IS NULL
       AND   StatCd = 'P'
       AND   JobTypCd = p_JobTypCd;
    ELSE
       RAISE_APPLICATION_ERROR( -20001, 'Inappropriate update');
    END IF;

 END;
/


GRANT EXECUTE ON STGCSA.JOBQUEUPDTSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.JOBQUEUPDTSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.JOBQUEUPDTSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.JOBQUEUPDTSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.JOBQUEUPDTSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.JOBQUEUPDTSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.JOBQUEUPDTSP TO STGCSADEVROLE;

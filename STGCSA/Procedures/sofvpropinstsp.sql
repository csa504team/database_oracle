DROP PROCEDURE STGCSA.SOFVPROPINSTSP;

CREATE OR REPLACE PROCEDURE STGCSA.SOFVPROPINSTSP(
    p_retval out number,
    p_errval out number,
    p_errmsg  out varchar2,
    p_identifier number:=0,
    p_userid varchar2
   ,p_LOANNMB CHAR:=null
   ,p_PROPLOANSECURBYREALPROP CHAR:=null
   ,p_PROPSECURMAILADRSTR1NM VARCHAR2:=null
   ,p_PROPSECURMAILADRSTR2NM VARCHAR2:=null
   ,p_PROPSECURMAILADRCTYNM VARCHAR2:=null
   ,p_PROPSECURMAILADRSTCD CHAR:=null
   ,p_PROPSECURZIPCD CHAR:=null
   ,p_PROPSECURZIP4CD CHAR:=null
   ,p_PROPDESCCNT VARCHAR2:=null
   ,p_PROPDESCMAILADRSTCD CHAR:=null
   ,p_PROPDESCAPN VARCHAR2:=null
) as
 /*
  Created on: 2018-11-28 18:24:38
  Created by: GENR
  Crerated from template stdtblins_template v3.05 13 Sep 2018 on 2018-11-28 18:24:38
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  Procedure performs INSERT on STGCSA.SOFVPROPTBLfor P_IDENTIFIER=0
*/
  rundate date;
  gn global_name.global_name%type;
  new_rec STGCSA.SOFVPROPTBL%rowtype;
  logged_msgid_retval number;
  audretval number;
  crossover_active char(1);
  l_LOANNMB char(10):=null;
  l_TRANSID number:=null;
  l_dbmdl varchar2(10):=null;
  l_entrydt date:=null;
  l_userid  varchar2(32):=null;
  l_usernm  varchar2(199):=null;
  l_timestampfld date:=null;
  cur_col_name varchar2(100):=null;
  maxsev number:=0;
  holdstring varchar2(4000);
  keystouse varchar2(1000);
  crossover_not_active char(1);
  -- std date for column defaulting
  JAN_1_1900  constant date:=to_date('JAN-01-1900','MON-DD-YYYY');
  cur_colvalue varchar2(4095);
  cur_colname varchar2(30);
-- Main body begins here
begin
  -- setup
  savepoint SOFVPROPINSTSP;
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  runtime.logger(logged_msgid_retval,'STDLOG',100,'Init SOFVPROPINSTSP',p_userid,4,
    logtxt1=>'SOFVPROPINSTSP',logtxt2=>'stdtblins_template v3.05 13 Sep 2018',
    PROGRAM_NAME=>'SOFVPROPINSTSP');
  select global_name, sysdate into gn, rundate from global_name;
  --
  -- Check to see if crossover is ongoing, in which case updates are prohibited
  SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
  IF crossover_not_active != 'Y' THEN
    p_errval := -20099;
    p_errmsg := 'Warning: Cannot update table SOFVPROPTBL at this time because crossover is running, please try later.';
  END IF;
  -- end of crossover_lockout_check
  begin
    cur_colname:='LOANNMB';
    cur_colvalue:=P_LOANNMB;
    l_LOANNMB:=P_LOANNMB;
  exception when others then
    p_errval:=sqlcode;
    p_errmsg:='Error '||sqlerrm(sqlcode)||' using '||cur_colname||' Value >'||cur_colvalue||'<';
  end;
  new_rec.creatuserid:=p_userid;
  new_rec.creatdt:=sysdate;
  new_rec.lastupdtuserid:=p_userid;
  new_rec.lastupdtdt:=sysdate;
  if p_identifier=0 then
    -- p_identifier=0 this is default for a stored procedure, only action for insert
    runtime.errfound:=false;
    runtime.errstr:=' ';
    --
    -- Default columns as applicable
  if p_errval=0 then
    begin
      cur_colname:='P_LOANNMB';
      cur_colvalue:=P_LOANNMB;
      if P_LOANNMB is null then

        new_rec.LOANNMB:=rpad(' ',10);

      else
        new_rec.LOANNMB:=P_LOANNMB;
      end if;
      cur_colname:='P_PROPLOANSECURBYREALPROP';
      cur_colvalue:=P_PROPLOANSECURBYREALPROP;
      if P_PROPLOANSECURBYREALPROP is null then

        new_rec.PROPLOANSECURBYREALPROP:=rpad(' ',1);

      else
        new_rec.PROPLOANSECURBYREALPROP:=P_PROPLOANSECURBYREALPROP;
      end if;
      cur_colname:='P_PROPSECURMAILADRSTR1NM';
      cur_colvalue:=P_PROPSECURMAILADRSTR1NM;
      if P_PROPSECURMAILADRSTR1NM is null then

        new_rec.PROPSECURMAILADRSTR1NM:=' ';

      else
        new_rec.PROPSECURMAILADRSTR1NM:=P_PROPSECURMAILADRSTR1NM;
      end if;
      cur_colname:='P_PROPSECURMAILADRSTR2NM';
      cur_colvalue:=P_PROPSECURMAILADRSTR2NM;
      if P_PROPSECURMAILADRSTR2NM is null then

        new_rec.PROPSECURMAILADRSTR2NM:=' ';

      else
        new_rec.PROPSECURMAILADRSTR2NM:=P_PROPSECURMAILADRSTR2NM;
      end if;
      cur_colname:='P_PROPSECURMAILADRCTYNM';
      cur_colvalue:=P_PROPSECURMAILADRCTYNM;
      if P_PROPSECURMAILADRCTYNM is null then

        new_rec.PROPSECURMAILADRCTYNM:=' ';

      else
        new_rec.PROPSECURMAILADRCTYNM:=P_PROPSECURMAILADRCTYNM;
      end if;
      cur_colname:='P_PROPSECURMAILADRSTCD';
      cur_colvalue:=P_PROPSECURMAILADRSTCD;
      if P_PROPSECURMAILADRSTCD is null then

        new_rec.PROPSECURMAILADRSTCD:=rpad(' ',2);

      else
        new_rec.PROPSECURMAILADRSTCD:=P_PROPSECURMAILADRSTCD;
      end if;
      cur_colname:='P_PROPSECURZIPCD';
      cur_colvalue:=P_PROPSECURZIPCD;
      if P_PROPSECURZIPCD is null then

        new_rec.PROPSECURZIPCD:=rpad(' ',5);

      else
        new_rec.PROPSECURZIPCD:=P_PROPSECURZIPCD;
      end if;
      cur_colname:='P_PROPSECURZIP4CD';
      cur_colvalue:=P_PROPSECURZIP4CD;
      if P_PROPSECURZIP4CD is null then

        new_rec.PROPSECURZIP4CD:=rpad(' ',4);

      else
        new_rec.PROPSECURZIP4CD:=P_PROPSECURZIP4CD;
      end if;
      cur_colname:='P_PROPDESCCNT';
      cur_colvalue:=P_PROPDESCCNT;
      if P_PROPDESCCNT is null then

        new_rec.PROPDESCCNT:=' ';

      else
        new_rec.PROPDESCCNT:=P_PROPDESCCNT;
      end if;
      cur_colname:='P_PROPDESCMAILADRSTCD';
      cur_colvalue:=P_PROPDESCMAILADRSTCD;
      if P_PROPDESCMAILADRSTCD is null then

        new_rec.PROPDESCMAILADRSTCD:=rpad(' ',2);

      else
        new_rec.PROPDESCMAILADRSTCD:=P_PROPDESCMAILADRSTCD;
      end if;
      cur_colname:='P_PROPDESCAPN';
      cur_colvalue:=P_PROPDESCAPN;
      if P_PROPDESCAPN is null then

        new_rec.PROPDESCAPN:=' ';

      else
        new_rec.PROPDESCAPN:=P_PROPDESCAPN;
      end if;
    exception when others then
       p_errval:=sqlcode;
       p_errmsg:='Error - Oracle returned '||sqlerrm(sqlcode)||' using '
           ||cur_colname||' Value >'||cur_colvalue||'<';
    end;
  end if;
    keystouse:='(LOANNMB)=('||new_rec.LOANNMB||')';
    if p_errval=0 then
      begin
        insert into STGCSA.SOFVPROPTBL values new_rec;
        p_RETVAL := SQL%ROWCOUNT;
      exception
      when dup_val_on_index then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Insert caused dupe on unique IX: '||sqlerrm(sqlcode);
      when others then
        p_errval:=sqlcode;
        p_retval:=0;
        if maxsev<3 then maxsev:=3; end if;
        p_errmsg:='Inserting record caught SQL error '||sqlerrm(sqlcode);
      end;
    end if;
  else /* P_IDENTIFIER was unexpected value */
    p_errval:=-20002;
    if maxsev<3 then maxsev:=3; end if;
    p_errmsg:='P_IDENTIFIER contained unexpected value: '||p_identifier
      ||' inserting record with key '||keystouse;
  end if;
  if p_errval=0 then
    runtime.logger(logged_msgid_retval,'STDLOG',101,
      'End SOFVPROPINSTSP With 0 return, P_RETVAL='
      ||p_retval||' Record Key '||keystouse,p_userid,
      1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt,
      l_timestampfld,PROGRAM_NAME=>'SOFVPROPINSTSP');
  else
    p_errmsg:='Error: '||P_ERRMSG||' in SOFVPROPINSTSP Record Key '||keystouse
      ||' Build: 2018-11-28 18:24:37';
    ROLLBACK TO SOFVPROPINSTSP;
    runtime.logger(logged_msgid_retval,'STDERR',103,
      p_errmsg,p_userid,maxsev,l_usernm,l_loannmb,l_transid, l_dbmdl,
      l_entrydt, l_timestampfld,PROGRAM_NAME=>'SOFVPROPINSTSP',force_log_entry=>TRUE);
  end if;
EXCEPTION
WHEN OTHERS THEN
  p_errval:=SQLCODE;
  p_errmsg:=' In outer exception handler in SOFVPROPINSTSP caught exception '
    ||sqlerrm(sqlcode)||' Record Key '||keystouse||' build 2018-11-28 18:24:37';
  ROLLBACK TO SOFVPROPINSTSP;
  runtime.logger(logged_msgid_retval,'STDERR',20,p_errmsg,p_userid,3,
  PROGRAM_NAME=>'SOFVPROPINSTSP',force_log_entry=>TRUE);
--
END SOFVPROPINSTSP;
/

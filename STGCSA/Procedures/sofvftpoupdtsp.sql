DROP PROCEDURE STGCSA.SOFVFTPOUPDTSP;

CREATE OR REPLACE PROCEDURE STGCSA.SOFVFTPOUPDTSP(
    p_retval out number,
    p_errval out number,
    p_errmsg  out varchar2,
    p_identifier number:=0,
    p_userid varchar2
   ,p_PREPOSTRVWRECRDTYPCD CHAR:=null
   ,p_PREPOSTRVWTRANSCD CHAR:=null
   ,p_PREPOSTRVWTRANSITROUTINGNMB CHAR:=null
   ,p_PREPOSTRVWTRANSITROUTCHKDGT CHAR:=null
   ,p_PREPOSTRVWBNKACCTNMB VARCHAR2:=null
   ,p_PREPOSTRVWPYMTAMT VARCHAR2:=null
   ,p_LOANNMB CHAR:=null
   ,p_PREPOSTRVWINDVLNM VARCHAR2:=null
   ,p_PREPOSTRVWCOMPBNKDISC CHAR:=null
   ,p_PREPOSTRVWADDENDRECIND VARCHAR2:=null
   ,p_PREPOSTRVWTRACENMB VARCHAR2:=null
   ,p_PREPOSTRVWCMNT VARCHAR2:=null
) as
 /*
  Created on: 2018-11-07 11:35:49
  Created by: GENR
  Crerated from template stdtblupd_template v3.22 12 Oct 2018 on 2018-11-07 11:35:50
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  Procedure SOFVFTPOUPDTSP performs UPDATE on STGCSA.SOFVFTPOTBL
    for P_IDENTIFIER=0 qualification is on Primary Key: LOANNMB
    Updates all columns except the qualification keys and columns for which a
      null value is passed.
    SPECIAL secret update kludge!
      if p_identifier is set to -1 then a flag is set to use all input parm values
      and p_identifier is changed to 0 (update by PK).
      THEREFORE if using p_identifier=-1 you MUST pass intended value for all columns.
      This option allows setting colulmns to null!
*/
  dbrec STGCSA.SOFVFTPOTBL%rowtype;
  logged_msg_id number;
  recnum number;
  rec_rowid rowid;
  keystouse varchar2(256);
  maxsev number:=0;
  pctsign char(1):='%';
  cur_colname varchar2(100):=null;
  cur_colvalue varchar2(4000);
  do_normal_field_checking char(1):='Y';
  overridden_p_identifier number;
  -- standard activity log capture vars
  l_LOANNMB char(10):=null;
  l_TRANSID number:=null;
  l_dbmdl varchar2(10):=null;
  l_entrydt date:=null;
  l_userid  varchar2(32):=null;
  l_usernm  varchar2(199):=null;
  l_timestampfld date:=null;
  crossover_not_active char(1);
procedure fetch_and_update_by_rowid(rowid_in rowid, orig_keyset_values varchar2) is
begin
  if p_errval=0 then
    begin
      select * into dbrec from STGCSA.SOFVFTPOTBL where rowid=rowid_in;
    exception when others then
      p_errval:=sqlcode;
      maxsev:=3;
      p_retval:=0;
      p_errmsg:='Fetch of STGCSA.SOFVFTPOTBL row by rowid '||rowid_in
        ||' orinally fetched using '||orig_keyset_values
        ||' failed with '||sqlerrm(sqlcode);
    end;
  end if;
  if p_errval=0 then
    -- set this as lastupdt
    DBrec.lastupdtuserid:=p_userid;
    DBrec.lastupdtdt:=sysdate;
    -- copy into DBrec all table column input parameters that are not null,
    -- saving column name/value if needed for error reporting
    --  (if p_identifier=-1 then move input even if null)
    begin
      if P_PREPOSTRVWRECRDTYPCD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PREPOSTRVWRECRDTYPCD';
        cur_colvalue:=P_PREPOSTRVWRECRDTYPCD;
        DBrec.PREPOSTRVWRECRDTYPCD:=P_PREPOSTRVWRECRDTYPCD;
      end if;
      if P_PREPOSTRVWTRANSCD is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PREPOSTRVWTRANSCD';
        cur_colvalue:=P_PREPOSTRVWTRANSCD;
        DBrec.PREPOSTRVWTRANSCD:=P_PREPOSTRVWTRANSCD;
      end if;
      if P_PREPOSTRVWTRANSITROUTINGNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PREPOSTRVWTRANSITROUTINGNMB';
        cur_colvalue:=P_PREPOSTRVWTRANSITROUTINGNMB;
        DBrec.PREPOSTRVWTRANSITROUTINGNMB:=P_PREPOSTRVWTRANSITROUTINGNMB;
      end if;
      if P_PREPOSTRVWTRANSITROUTCHKDGT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PREPOSTRVWTRANSITROUTCHKDGT';
        cur_colvalue:=P_PREPOSTRVWTRANSITROUTCHKDGT;
        DBrec.PREPOSTRVWTRANSITROUTINGCHKDGT:=P_PREPOSTRVWTRANSITROUTCHKDGT;
      end if;
      if P_PREPOSTRVWBNKACCTNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PREPOSTRVWBNKACCTNMB';
        cur_colvalue:=P_PREPOSTRVWBNKACCTNMB;
        DBrec.PREPOSTRVWBNKACCTNMB:=P_PREPOSTRVWBNKACCTNMB;
      end if;
      if P_PREPOSTRVWPYMTAMT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PREPOSTRVWPYMTAMT';
        cur_colvalue:=P_PREPOSTRVWPYMTAMT;
        DBrec.PREPOSTRVWPYMTAMT:=P_PREPOSTRVWPYMTAMT;
      end if;
      if P_LOANNMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_LOANNMB';
        cur_colvalue:=P_LOANNMB;
        DBrec.LOANNMB:=P_LOANNMB;
      end if;
      if P_PREPOSTRVWINDVLNM is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PREPOSTRVWINDVLNM';
        cur_colvalue:=P_PREPOSTRVWINDVLNM;
        DBrec.PREPOSTRVWINDVLNM:=P_PREPOSTRVWINDVLNM;
      end if;
      if P_PREPOSTRVWCOMPBNKDISC is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PREPOSTRVWCOMPBNKDISC';
        cur_colvalue:=P_PREPOSTRVWCOMPBNKDISC;
        DBrec.PREPOSTRVWCOMPBNKDISC:=P_PREPOSTRVWCOMPBNKDISC;
      end if;
      if P_PREPOSTRVWADDENDRECIND is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PREPOSTRVWADDENDRECIND';
        cur_colvalue:=P_PREPOSTRVWADDENDRECIND;
        DBrec.PREPOSTRVWADDENDRECIND:=P_PREPOSTRVWADDENDRECIND;
      end if;
      if P_PREPOSTRVWTRACENMB is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PREPOSTRVWTRACENMB';
        cur_colvalue:=P_PREPOSTRVWTRACENMB;
        DBrec.PREPOSTRVWTRACENMB:=P_PREPOSTRVWTRACENMB;
      end if;
      if P_PREPOSTRVWCMNT is not null or DO_NORMAL_FIELD_CHECKING='N' then
        cur_colname:='P_PREPOSTRVWCMNT';
        cur_colvalue:=P_PREPOSTRVWCMNT;
        DBrec.PREPOSTRVWCMNT:=P_PREPOSTRVWCMNT;
      end if;
    exception when others then
      p_errval:=sqlcode;
      if maxsev<3 then maxsev:=3; end if;
      p_errmsg:='Error using '||cur_colname||' Value >'||cur_colvalue||'<';
    end;
  end if;
  -- if all still ok do actual update
  if p_errval=0 then
    Begin
      update STGCSA.SOFVFTPOTBL
        set lastupdtuserid=DBrec.lastupdtuserid,
          lastupdtdt=DBrec.lastupdtdt
          ,PREPOSTRVWRECRDTYPCD=DBrec.PREPOSTRVWRECRDTYPCD
          ,PREPOSTRVWTRANSCD=DBrec.PREPOSTRVWTRANSCD
          ,PREPOSTRVWTRANSITROUTINGNMB=DBrec.PREPOSTRVWTRANSITROUTINGNMB
          ,PREPOSTRVWTRANSITROUTINGCHKDGT=DBrec.PREPOSTRVWTRANSITROUTINGCHKDGT
          ,PREPOSTRVWBNKACCTNMB=DBrec.PREPOSTRVWBNKACCTNMB
          ,PREPOSTRVWPYMTAMT=DBrec.PREPOSTRVWPYMTAMT
          ,LOANNMB=DBrec.LOANNMB
          ,PREPOSTRVWINDVLNM=DBrec.PREPOSTRVWINDVLNM
          ,PREPOSTRVWCOMPBNKDISC=DBrec.PREPOSTRVWCOMPBNKDISC
          ,PREPOSTRVWADDENDRECIND=DBrec.PREPOSTRVWADDENDRECIND
          ,PREPOSTRVWTRACENMB=DBrec.PREPOSTRVWTRACENMB
          ,PREPOSTRVWCMNT=DBrec.PREPOSTRVWCMNT
        where rowid=rowid_in;
      recnum:=recnum+1;
    exception when others then
      p_errval:=sqlcode;
      p_retval:=0;
      if maxsev<3 then maxsev:=3; end if;
      p_errmsg:= 'Error while doing update on STGCSA.SOFVFTPOTBL, on rec with rowid '
      ||rowid_in||' previously fetched using key: '||orig_keyset_values
      ||' got Oracle error '||sqlerrm(sqlcode);
    end;
  end if;
END;
begin
  -- setup
  savepoint SOFVFTPOUPDTSP;
  p_retval:=0;
  p_errval:=0;
  p_errmsg:=null;
  recnum:=0;
  runtime.logger(logged_msg_id,'STDLOG',100,'Init SOFVFTPOUPDTSP',p_userid,4,
    logtxt1=>'SOFVFTPOUPDTSP',logtxt2=>'stdtblupd_template v3.22 12 Oct 2018',
    PROGRAM_NAME=>'SOFVFTPOUPDTSP');
  --
  l_LOANNMB:=P_LOANNMB;
  --
  -- Check to see if crossover is ongoing, in which case updates are prohibited
  SELECT stgcsa.isUpdateAllowed( ) into crossover_not_active from dual;
  IF crossover_not_active != 'Y' THEN
    p_errval := -20099;
    p_errmsg := 'Warning: Cannot update table SOFVFTPOTBL at this time because crossover is running, please try later.';
  END IF;
  -- end of crossover_lockout_check
--
-- special p_identifier kludge
--  if p_identifier is set to -1 then it treated as 0 (update by PK)
--  but a flag is set use passed value EVEN IF NULL.  Since for option -1
--  every column is updated to what was passed, intended values MUST be passed
--  for all columns.  This option allows setting colulmns to null!
  overridden_p_identifier:=p_identifier;
  if p_identifier=-1 then
    do_normal_field_checking:='N';
    overridden_p_identifier:=0;
  end if;
  case overridden_p_identifier
  when 0 then
    -- case to update one row based on primary key
    -- p_identifier=0 (choice 0) is default action for a stored procedure
    -- get existing version of row
    if p_errval=0 then
      keystouse:='(LOANNMB)=('||P_LOANNMB||')';
      begin
        select rowid into rec_rowid from STGCSA.SOFVFTPOTBL where 1=1
         and LOANNMB=P_LOANNMB;
      exception when no_data_found then
        p_errval:=0;
        p_retval:=0;
        if maxsev<2 then maxsev:=2; end if;
        p_errmsg:='Warning No STGCSA.SOFVFTPOTBL row to update with key(s) '
          ||keystouse;
      end;
    end if;
    if p_errval=0 then
      fetch_and_update_by_rowid(rec_rowid, keystouse);
    end if;
  else /* P_IDENTIFIER was unexpected value */
    p_errval:=-20002;
    if maxsev<3 then maxsev:=3; end if;
    p_errmsg:='Error P_IDENTIFIER contained unexpected value: '||p_identifier;
  end case;
  P_RETVAL:=RECNUM;
  -- post to log
  if p_errval=0 then
    runtime.logger(logged_msg_id,'STDLOG',101,
      'End SOFVFTPOUPDTSP With 0 return, P_RETVAL='
      ||p_retval||' MSG='||p_errmsg||' key used:'||keystouse,p_userid,
      1,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld
      ,PROGRAM_NAME=>'SOFVFTPOUPDTSP');
  else
    ROLLBACK TO SOFVFTPOUPDTSP;
    p_errmsg:=p_errmsg||' in SOFVFTPOUPDTSP build 2018-11-07 11:35:49';
    runtime.logger(logged_msg_id,'STDERR',103,p_errmsg,p_userid,
      maxsev, l_usernm, l_loannmb,l_transid, l_dbmdl, l_entrydt, l_timestampfld,
      PROGRAM_NAME=>'SOFVFTPOUPDTSP',force_log_entry=>true);
  end if;
EXCEPTION
WHEN OTHERS THEN
  ROLLBACK TO SOFVFTPOUPDTSP;
  p_errmsg:=' Error outer exception handler caught Oracle error:'||SQLERRM(SQLCODE)
    ||' in SOFVFTPOUPDTSP build 2018-11-07 11:35:49';
  runtime.logger(logged_msg_id,'STDERR',104,p_errmsg,p_userid,
    3,l_usernm,l_loannmb,l_transid, l_dbmdl,l_entrydt, l_timestampfld,
    PROGRAM_NAME=>'SOFVFTPOUPDTSP',force_log_entry=>true);
--
END SOFVFTPOUPDTSP;
/


GRANT EXECUTE ON STGCSA.SOFVFTPOUPDTSP TO CSAUPDTROLE;

GRANT EXECUTE ON STGCSA.SOFVFTPOUPDTSP TO LOANCSAADMINROLE;

GRANT EXECUTE ON STGCSA.SOFVFTPOUPDTSP TO LOANCSAANALYSTROLE;

GRANT EXECUTE ON STGCSA.SOFVFTPOUPDTSP TO LOANCSAMANAGERROLE;

GRANT EXECUTE ON STGCSA.SOFVFTPOUPDTSP TO LOANCSAREADALLROLE;

GRANT EXECUTE ON STGCSA.SOFVFTPOUPDTSP TO LOANCSAREVIEWERROLE;

GRANT EXECUTE ON STGCSA.SOFVFTPOUPDTSP TO STGCSADEVROLE;

DROP PROCEDURE STGCSA.LOANBORRADDRUPDEXPCSP;

CREATE OR REPLACE PROCEDURE STGCSA.LOANBORRADDRUPDEXPCSP( 
  p_retval out number,
  p_errval out number,
  p_errmsg  out varchar2,
  p_identifier number:=0,
  p_userid varchar2,
  p_LOANNMB CHAR:=null,
  p_LOANDTLBORRNM varchar2,
  p_LoanDtlBorrMailAdrStr1Nm varchar2,
  p_LoanDtlBorrMailAdrStr2Nm varchar2:=' ',
  p_LoanDtlBorrMailAdrCtyNm varchar2,
  p_LoanDtlBorrMailAdrStCd varchar2,
  p_LoanDtlBorrMailAdrZipCd varchar2,
  p_LoanDtlBorrMailAdrZip4Cd varchar2:=' ')
as
--
-- fixed 9 Oct 18 jl - interim version pendind ID of "blankable" columns
  progver varchar2(30):='1.0 10/04/2018';
  progname varchar2(30):='LOANBORRADDRUPDEXP';
  logged_msg_id number;
  w_LoanDtlBorrMailAdrStr2Nm varchar2(80);
  w_LoanDtlBorrMailAdrZip4Cd varchar2(4);
begin
  p_errval:=0;
  p_errmsg:='';
  p_retval:=0;
  --
  --add any data validation logic here 
  -- sample specific validation logic:
  if p_LoanDtlBorrMailAdrZipCd='abcde' then 
      p_errval:=1;
      p_errmsg:='Procedure '||progname||' passed invalid zipcode '
          ||p_LoanDtlBorrMailAdrZipCd;
  end if;
  --
  -- verify p_identifier has value 0
  if p_errval=0 then
      if p_identifier<>0 then 
          p_errval:=1;
          p_errmsg:='Procedure '||progname||' passed invalid p_identifier '
             ||p_identifier;
      end if;
  end if;

  w_LoanDtlBorrMailAdrStr2Nm:=p_LoanDtlBorrMailAdrStr2Nm;
  if w_LoanDtlBorrMailAdrStr2Nm is NULL then 
    w_LoanDtlBorrMailAdrStr2Nm := ' ';
  end if;
  w_LoanDtlBorrMailAdrZip4Cd:=p_LoanDtlBorrMailAdrZip4Cd;
  if w_LoanDtlBorrMailAdrZip4Cd is NULL then 
    w_LoanDtlBorrMailAdrZip4Cd := ' ';
  end if;


 --
  -- all good, perform update
  if p_errval=0 then
    SOFVLND1UPDTSP(p_retval, p_errval, p_errmsg, p_identifier, p_userid, 
       p_loannmb=>p_loannmb,p_LOANDTLBORRNM=>p_LOANDTLBORRNM,
       p_LoanDtlBorrMailAdrStr1Nm=>p_LoanDtlBorrMailAdrStr1Nm,
       p_LoanDtlBorrMailAdrStr2Nm=>w_loandtlBorrMailAdrStr2Nm,
       p_LoanDtlBorrMailAdrCtyNm=>p_LoanDtlBorrMailAdrCtyNm,
       p_LoanDtlBorrMailAdrStCd=>p_LoanDtlBorrMailAdrStCd,
       p_LoanDtlBorrMailAdrZipCd=>p_LoanDtlBorrMailAdrZipCd,
       p_LoanDtlBorrMailAdrZip4Cd=>w_loandtlBorrMailAdrZip4Cd);
  end if;
  -- 
  -- regardless of return from update proc, just return the result to caller
  runtime.logger(logged_msg_id,'STDLOG',101,
    'End LOANBORRADDRUPDEXP With '||p_errval||' return, P_RETVAL='
    ||p_retval||' MSG='||p_errmsg,p_userid,1    
    --,PROGRAM_NAME=>progname||' '||progver
    );
exception when others then
  p_errval:=sqlcode;
  p_errmsg:='Procedure '||progname||' outer exception handler '
    ||'caught error '||sqlerrm(sqlcode); 
  runtime.logger(logged_msg_id,'STDLOG',101,
    p_errmsg,p_userid,1
    --,PROGRAM_NAME=>progname||' '||progver
    );
end LOANBORRADDRUPDEXPCSP;
/

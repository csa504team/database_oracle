/* Formatted on 6/8/2020 6:13:17 PM (QP5 v5.300) */
CREATE OR REPLACE FORCE VIEW STGCSA.XOVER_SOFVWIREKEYTOTTBL_R2V
(
    WIREKEYDT,
    WIREKEYFILENM,
    WIREKEYTOTAMT,
    CREATUSERID,
    CREATDT,
    LASTUPDTUSERID,
    LASTUPDTDT,
    STGCSA_MERGE_END_DT
)
    BEQUEATH DEFINER
AS
    WITH merge_end AS (SELECT STGCSA_MERGE_END_DT FROM stgcsa.xover_status)
    SELECT "WIREKEYDT",
           "WIREKEYFILENM",
           "WIREKEYTOTAMT",
           "CREATUSERID",
           "CREATDT",
           "LASTUPDTUSERID",
           "LASTUPDTDT",
           "STGCSA_MERGE_END_DT"
      FROM stgcsa.SOFVWIREKEYTOTTBL, merge_end
     -- special case... updates on stgcsa should not go to csa
     WHERE lastupdtdt >=
               DECODE (STGCSA_MERGE_END_DT,
                       TO_DATE ('19000101', 'yyyymmdd'), STGCSA_MERGE_END_DT,
                       TO_DATE ('29990101', 'yyyymmdd'));



GRANT SELECT ON STGCSA.XOVER_SOFVWIREKEYTOTTBL_R2V TO STGCSAREADALLROLE;

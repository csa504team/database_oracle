create or replace view STGCSA.SC_SOFVDRTBL
  as select 
  -- Scrubbing View - Definition generated 2019-06-24 11:59:44
  -- Using SNAP ver SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  --   and template SCRUB_VIEWS V1.2 25 Mar 2019, J. Low, J. Gorowada, Select Computing
  * from STGCSA.SOFVDRTBL;

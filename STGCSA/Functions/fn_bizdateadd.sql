create or replace FUNCTION           STGCSA.FN_BIZDATEADD 
--drop function        fn_BizDateAdd


(
  v_in_date IN DATE,
  v_biz_day IN NUMBER
)
RETURN DATE
AS
   v_out_date DATE;
   v_weekday NUMBER(3,0);
   v_interim_date DATE;
   v_interval NUMBER(5,0);
   v_days_added NUMBER(10,0);
/*
        select dbo.fn_BizDateAdd('2/1/2008', -7)
        select dbo.fn_BizDateAdd('2/1/2008', 0)
        select dbo.fn_BizDateAdd('2/1/2008', 1)
*/


BEGIN
   v_days_added := 0 ;
   IF v_biz_day > 0 THEN
    v_interval := 1 ;
   ELSE
      IF v_biz_day < 0 THEN
       v_interval := -1 ;
      ELSE
         v_interval := 0 ;
      END IF;
   END IF;
   v_interim_date := v_in_date ;
   WHILE v_days_added < ABS(v_biz_day) 
   LOOP 
      DECLARE
         v_temp NUMBER(1, 0) := 0;
      
      BEGIN
         v_interim_date := v_interim_date + v_interval;
         v_weekday := TO_CHAR(v_interim_date, 'D');
         -- if not a businiess day, keep searching for the next business day
         BEGIN
            SELECT 1 INTO v_temp
              FROM DUAL
             WHERE NOT ( v_weekday IN ( 1,7 )


           OR v_interim_date IN ( SELECT HOLIDAYDT 
                                  FROM STGCSA.REFCALNDRHOLIDAYTBL  )
          );
         EXCEPTION
            WHEN OTHERS THEN
               NULL;
         END;
            
         IF v_temp = 1 THEN
          v_days_added := v_days_added + 1 ;
         END IF;
      
      END;
   END LOOP;
   v_out_date := v_interim_date ;
   RETURN (v_out_date);


EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
END;
/

GRANT EXECUTE ON STGCSA.FN_BIZDATEADD TO STGCSADEVROLE;
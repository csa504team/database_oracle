/* first statement will not find a row to update                             
   it is intended to verify finding statements that do not match a row              
   when manually reviewing the log fog file.   */                                   
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112' where LOANNMB='NONESUCH  ' 
  and PYMTPOSTPYMTDT=to_DATE('02-APR-20','dd-MON-yy')  AND PYMTPOSTPYMTTYP='Z' and PYMTPOSTREMITTEDAMT=-9999;    
/* all following statements should update 1 row */                                  
                                                                                       
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2488497003'                        
 and PYMTPOSTPYMTDT=to_DATE('10-Mar-21','mm/dd/yyyy')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=9000;                                          
-- 1                                                                                                     
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3188346000'                        
 and PYMTPOSTPYMTDT=to_DATE('10-Mar-21','mm/dd/yyyy')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=9000;                                          
-- 2                                                                                                     
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4046977007'                        
 and PYMTPOSTPYMTDT=to_DATE('10-Mar-21','mm/dd/yyyy')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4293.08;                                       
-- 3                                                                                                     
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6108365004'                        
 and PYMTPOSTPYMTDT=to_DATE('10-Mar-21','mm/dd/yyyy')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5826.68;                                       
-- 4                                                                                                     
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6549815003'                        
 and PYMTPOSTPYMTDT=to_DATE('10-Mar-21','mm/dd/yyyy')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=8159.82;                                       
-- 5                                                                                                     
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7918675010'                        
 and PYMTPOSTPYMTDT=to_DATE('10-Mar-21','mm/dd/yyyy')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=9000;                                          
-- 6                                                                                                     
                                                                                                                                                      
COMMIT;                                                                           
Select 'END of update statements' from dual;                                      
                                                                                                                                                      

/* first statement will not find a row to update                             
   it is intended to verify finding statements that do not match a row              
   when manually reviewing the log fog file.   */                                   
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112' where LOANNMB='NONESUCH  ' 
  and PYMTPOSTPYMTDT=to_DATE('02-APR-20','dd-MON-yy')  AND PYMTPOSTPYMTTYP='Z' and PYMTPOSTREMITTEDAMT=-9999;    
/* all following statements should update 1 row */                                  
                                                                                       
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='1929427006'                        
 and PYMTPOSTPYMTDT=to_DATE('10/22/2020','mm/dd/yyyy')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2638.73;                                      
-- 1                                                                                                     
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3526157002'                        
 and PYMTPOSTPYMTDT=to_DATE('10/22/2020','mm/dd/yyyy')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6555.69;                                      
-- 2                                                                                                     
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4182817010'                        
 and PYMTPOSTPYMTDT=to_DATE('10/22/2020','mm/dd/yyyy')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4662.5;                                       
-- 3                                                                                                     
                                                                                                                                                      
COMMIT;                                                                           
Select 'END of update statements' from dual;                                      
                                                                                                                                                      

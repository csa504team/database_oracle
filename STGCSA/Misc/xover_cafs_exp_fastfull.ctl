directory=dp2
dumpfile=xovertest
reuse_dumpfiles=yes
exclude=grant,trigger,index
tables=
  STGCSA.SOFVPYMTtbl,  
  stgcsa.SOFVAUDTTBL,                   
  stgcsa.SOFVBFFATBL,                   
  stgcsa.SOFVBGDFTBL,                   
  stgcsa.SOFVBGW9TBL,                   
  stgcsa.SOFVCDCMSTRTBL,                
  stgcsa.SOFVCTCHTBL,                   
  stgcsa.SOFVDFPYTBL,                   
  stgcsa.SOFVDUEBTBL,                   
  stgcsa.SOFVFTPOTBL,                   
  stgcsa.SOFVGNTYTBL,                   
  stgcsa.SOFVGRGCTBL,                   
  stgcsa.SOFVLND1TBL,                   
  stgcsa.SOFVOTRNTBL,                   
  stgcsa.SOFVRTSCTBL,
  stgcsa.SOFVDRTBL,
  stgcsa.SOFVWIREKEYTOTTBL,
  stgcsa.PYMT110RPTARCHVTBL
Views_as_tables=STGCSA.XOVER_STATUS_STG_V
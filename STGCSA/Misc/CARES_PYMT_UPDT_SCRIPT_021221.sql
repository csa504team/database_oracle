/* first statement will not find a row to update                             
   it is intended to verify finding statements that do not match a row              
   when manually reviewing the log fog file.   */                                   
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112' where LOANNMB='NONESUCH  ' 
  and PYMTPOSTPYMTDT=to_DATE('02-APR-20','dd-MON-yy')  AND PYMTPOSTPYMTTYP='Z' and PYMTPOSTREMITTEDAMT=-9999;    
/* all following statements should update 1 row */                                  
                                                                                       
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2947827006'                        
 and PYMTPOSTPYMTDT=to_DATE('2/11/2021','mm/dd/yyyy')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=631.77;                                        
-- 1                                                                                                     
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4892785005'                        
 and PYMTPOSTPYMTDT=to_DATE('2/11/2021','mm/dd/yyyy')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=9000;                                          
-- 2                                                                                                     
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5204485000'                        
 and PYMTPOSTPYMTDT=to_DATE('2/11/2021','mm/dd/yyyy')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=9000;                                          
-- 3                                                                                                     
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5616405005'                        
 and PYMTPOSTPYMTDT=to_DATE('2/11/2021','mm/dd/yyyy')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=545.29;                                        
-- 4                                                                                                     
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6444095005'                        
 and PYMTPOSTPYMTDT=to_DATE('2/11/2021','mm/dd/yyyy')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=9000;                                          
-- 5                                                                                                     
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7039225008'                        
 and PYMTPOSTPYMTDT=to_DATE('2/11/2021','mm/dd/yyyy')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2686.42;                                       
-- 6                                                                                                     
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9167215009'                        
 and PYMTPOSTPYMTDT=to_DATE('2/11/2021','mm/dd/yyyy')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=8131.31;                                       
-- 7                                                                                                     
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9387815003'                        
 and PYMTPOSTPYMTDT=to_DATE('2/11/2021','mm/dd/yyyy')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=9000;                                          
-- 8                                                                                                     
                                                                                                                                                      
COMMIT;                                                                           
Select 'END of update statements' from dual;                                      
                                                                                                                                                      

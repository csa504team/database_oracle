set pagesize 0
select
'alter trigger stgcsa.'||substr(oname,1,length(oname)-3)||'trg disable;',
'create table stgcsa.newrows_'||oname,
'  as select * from stgcsa.'||oname||' where 0=1;',    
'declare                                                              ',
'  rec stgcsa.'||oname||'%rowtype;                  ',                                                    
'begin                                                                ',
'  for x in (select rowid from stgcsa.'||oname||' where rownum<5) loop     ',
'    select * into rec from stgcsa.'||oname||' where rowid=x.rowid;',
'    insert into stgcsa.newrows_'||oname||' values rec;' ,
'    delete from  stgcsa.'||oname||' where rowid=x.rowid; ',
'  end loop;                                                      ',
'end;                                                             ',  
'alter trigger stgcsa.'||substr(oname,1,length(oname)-3)||'trg enable;',
'/*  ---------------------------------------------------------- */'
from stgcsa.reftblstbl where one_of_16='Y'
  and oname not in ('SOFVAUDTTBL');
  
/*
  declare 
  x stgcsa.sofvotrntbl%rowtype;
begin
  select * into x from stgcsa.sofvotrntbl;
  for i in 1..50 loop
    x.ONLNTRANSTRANSDT:=x.ONLNTRANSTRANSDT+i/1440;
    insert into stgcsa.sofvotrntbl values x;
  end loop;
end;   
*/

/*
alter trigger stgcsa.sofvftpotrg enable;
alter session set nls_date_format='yyyy-mm-dd hh24:mi:ss';
define lnnmb=9907041776
define puserid=TESTER
set verify off
declare
    p_identifier number:=0;
    p_retval number;
    p_errval number;
    p_errmsg varchar2(4000);
    p_userid varchar2(80):='&&puserid';
    wasdef boolean;
    myloannmb char(10);
begin
  dbms_output.enable(10000);
  for l in (select loannmb from stgcsa.sofvlnd1tbl where rownum<51) loop  
  STGCSA.SOFVftpoINSTSP(
     p_retval, p_errval, p_errmsg, p_identifier, p_userid,
       p_loannmb=>l.loannmb);
  dbms_output.put_line('done ins, p_retval='||p_retval
      ||' p_errval='||p_errval||' p_errmsg='||p_errmsg);
  end loop;    
end;
select count(*) from stgcsa.sofvftpotbl;
*/
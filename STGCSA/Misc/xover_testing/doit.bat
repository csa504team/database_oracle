echo off
set usr=john
set pwd=R0adTr1p!
set sqldir=c:/csa/database_oracle/STGCSA/Misc/xover_testing
rem set dt=%Date:~10,4%-%Date:~7,2%-%Date:~4,2%_%time:~0,2%-%time:~3,2%-%time:~6,2%
set dt=%Date:~10,4%-%Date:~7,2%-%Date:~4,2%_%time:~1,1%-%time:~3,2%-%time:~6,2%
set ofile=c:/csa/database_oracle/STGCSA/Misc/xover_testing/out/run_%1%
set ofile=%ofile%_%dt%.lst
echo $$$0 starting now>>%ofile%
set sqlparm=%sqldir%/%1%.sql
set sqlparmstg=%sqldir%/%1%stgwork.sql
set sqlparmcsa=%sqldir%/%1%csawork.sql
echo sqlparm %sqlparm%
rem 
rem use sqlplus to show key status info from both DBs
sqlplus /nolog @%sqldir%\teststart %ofile%
rem
rem run test case sql script to simulate work on stgcsa
echo $$$1 run STGCSA work before crossover ...stgwork script >>%ofile%
set sqlfile=%sqldir%\%1%stgwork
sqlplus /nolog @%sqlfile% %ofile%
rem
rem run xover_init on stgcsa side
echo $$$2 run xover_init on stgcsa>>%ofile% 
sqlplus /nolog @%sqldir%\runstgcsaxoverinit %ofile% 
rem
rem run expdp/impdp from stgcsa to csa
echo $$$3 run STGCSA export>>%ofile%
expdp john/R0adTr1p!@stgcsa3 parfile=c:\csa\database_oracle\STGCSA\Misc\xover_cafs_exp.ctl
type c:\oracle\admin\mstr\dpdump\export.log>>%ofile%
echo $$$4 run CSA import>>%ofile%
rem
impdp john/R0adTr1p!@csa1 parfile=c:\csa\database_oracle\STGCSA\Misc\xover_cala_imp.ctl
type c:\oracle\admin\mstr\dpdump\import.log>>%ofile%
rem
rem show state info on csa after import
echo $$$5 query state info on csa after import before merge >>%ofile% 
sqlplus /nolog @%sqldir%\qrycsastate %ofile% 
rem
rem run merge on csa side
echo $$$6 run merge on csa>>%ofile% 
sqlplus /nolog @%sqldir%\runcsamerge %ofile% 
rem
rem show state info on csa after import
echo $$$7 query state info on csa after merge >>%ofile% 
sqlplus /nolog @%sqldir%\qrycsastate %ofile% 
rem
rem run test case sql script to simulate work on csa
echo $$$8 run CSA work before crossover ...csawork script>>%ofile%
set sqlfile=%sqldir%\%1%csawork
sqlplus /nolog @%sqlfile% %ofile%
rem
rem run xover_init on csa side
echo $$$9 run xover_init on csa>>%ofile% 
sqlplus /nolog @%sqldir%\runcsaxoverinit %ofile% 
rem
rem exp/imp crossover back to stgcsa
echo $$$10 run CSA export>>%ofile%
expdp john/R0adTr1p!@csa1 parfile=c:\csa\database_oracle\STGCSA\Misc\xover_cala_exp.ctl
type c:\oracle\admin\mstr\dpdump\export.log>>%ofile%
echo $$$11 run STGCSA import>>%ofile%
rem
impdp john/R0adTr1p!@stgcsa3 parfile=c:\csa\database_oracle\STGCSA\Misc\xover_cafs_imp.ctl
type c:\oracle\admin\mstr\dpdump\import.log>>%ofile%

rem
rem show state info on stgcsa after import
echo $$$12 query state info on stg after import before merge>>%ofile% 
sqlplus /nolog @%sqldir%\qrystgstate2 %ofile% 
rem
rem run merge on stgcsa side
echo $$$13 run merge on stgcsa >>%ofile% 
sqlplus /nolog @%sqldir%\runstgmerge %ofile% 
rem
rem show state info on stgcsa after merge
echo $$$14 query state info on stgcsa after merge>>%ofile% 
sqlplus /nolog @%sqldir%\qrystgstate %ofile% 

rem
rem run testcase specific queries to show changes on STGCSA
echo $$$15 run testcase specific queries to show changes on STGCSA>>%ofile% 
set sqlfile=%sqldir%\%1%stgverify
sqlplus /nolog @%sqlfile% %ofile%


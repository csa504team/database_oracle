connect john/R0adTr1p!@stgcsa3
set pagesize 1000
alter session set nls_date_format='yyyy-mm-dd hh24:mi:ss';
set pagesize 1000
set echo on
spool &1 append
prompt $$$ validating queries on STGCSA after merge
set pagesize 100
alter session set nls_date_format='yyyy-mm-dd hh24:mi:ss';
select 'SOFVAUDTTBL', count(*) from stgcsa.SOFVAUDTTBL                          
  where lastupdtdt>                           
    (select csa_merge_end_dt from stgcsa.xover_status);
                                                                                
select 'SOFVBFFATBL', count(*) from stgcsa.SOFVBFFATBL                          
  where lastupdtdt>                           
    (select csa_merge_end_dt from stgcsa.xover_status);
                                                                                
select 'SOFVBGDFTBL', count(*) from stgcsa.SOFVBGDFTBL                          
  where lastupdtdt>                           
    (select csa_merge_end_dt from stgcsa.xover_status);
                                                                                
select 'SOFVBGW9TBL', count(*) from stgcsa.SOFVBGW9TBL                          
  where lastupdtdt>                           
    (select csa_merge_end_dt from stgcsa.xover_status);
                                                                                
select 'SOFVCDCMSTRTBL', count(*) from stgcsa.SOFVCDCMSTRTBL                    
  where lastupdtdt>                           
    (select csa_merge_end_dt from stgcsa.xover_status);
                                                                                
select 'SOFVCTCHTBL', count(*) from stgcsa.SOFVCTCHTBL                          
  where lastupdtdt>                           
    (select csa_merge_end_dt from stgcsa.xover_status);
                                                                                
select 'SOFVDFPYTBL', count(*) from stgcsa.SOFVDFPYTBL                          
  where lastupdtdt>                           
    (select csa_merge_end_dt from stgcsa.xover_status);
                                                                                
select 'SOFVDUEBTBL', count(*) from stgcsa.SOFVDUEBTBL                          
  where lastupdtdt>                           
    (select csa_merge_end_dt from stgcsa.xover_status);
                                                                                
select 'SOFVFTPOTBL', count(*) from stgcsa.SOFVFTPOTBL                          
  where lastupdtdt>                           
    (select csa_merge_end_dt from stgcsa.xover_status);
                                                                                
select 'SOFVGNTYTBL', count(*) from stgcsa.SOFVGNTYTBL                          
  where lastupdtdt>                           
    (select csa_merge_end_dt from stgcsa.xover_status);
                                                                                
select 'SOFVGRGCTBL', count(*) from stgcsa.SOFVGRGCTBL                          
  where lastupdtdt>                           
    (select csa_merge_end_dt from stgcsa.xover_status);
                                                                                
select 'SOFVLND1TBL', count(*) from stgcsa.SOFVLND1TBL                          
  where lastupdtdt>                           
    (select csa_merge_end_dt from stgcsa.xover_status);
                                                                                
select 'SOFVLND2TBL', count(*) from stgcsa.SOFVLND2TBL                          
  where lastupdtdt>                           
    (select csa_merge_end_dt from stgcsa.xover_status);
                                                                                
select 'SOFVOTRNTBL', count(*) from stgcsa.SOFVOTRNTBL                          
  where lastupdtdt>                           
    (select csa_merge_end_dt from stgcsa.xover_status);
                                                                                
select 'SOFVPYMTTBL', count(*) from stgcsa.SOFVPYMTTBL                          
  where lastupdtdt>                           
    (select csa_merge_end_dt from stgcsa.xover_status);
                                                                                
select 'SOFVRTSCTBL', count(*) from stgcsa.SOFVRTSCTBL                          
  where lastupdtdt>                           
    (select csa_merge_end_dt from stgcsa.xover_status);
                                                              
exit
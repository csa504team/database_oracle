connect john/R0adTr1p!@stgcsa3
set pagesize 1000
spool &1 append
prompt $$$ stgcsa run merge
set pagesize 100
set serveroutput on
set echo on
exec stgcsa.xover_stg_merge_csp;
exit
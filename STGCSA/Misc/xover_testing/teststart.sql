define usr=john
define pwd=R0adTr1p!
define odb=stgcsa3
define bdb=csa1
connect john/R0adTr1p!@stgcsa3
set pagesize 1000
spool &1 append
prompt $$$ STGCSA2 before test
@@stgcsa_state_queries
prompt CSA1 before test
connect john/R0adTr1p!@csa1
@@csa_state_queries
exit
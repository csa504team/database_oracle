connect john/R0adTr1p!@stgcsa3
set pagesize 1000
spool &1 append
prompt $$$ STGCSA2 simulated work before crossover
prompt update 2 rows in each crossover table
prompt include columns that force sofvaudttbl logging

update stgcsa.SOFVBGW9TBL                   
 set lastupdtuserid='ME',W9NM='ME', 
lastupdtdt=sysdate where rownum<3;
                                                                                
update stgcsa.SOFVCDCMSTRTBL                
 set lastupdtuserid='ME', CDCSTATCD='xx',
lastupdtdt=sysdate where rownum<2;
update stgcsa.SOFVCDCMSTRTBL                
 set lastupdtuserid='ME', CDCSTATCD='xx',
lastupdtdt=sysdate where rownum<2
and lastupdtdt<sysdate-2/(1440*60);

                                                                                
update stgcsa.SOFVCTCHTBL                   
 set lastupdtuserid='ME', lastupdtdt=sysdate where rownum<3;
                                                                                
update stgcsa.SOFVDFPYTBL                   
 set lastupdtuserid='ME', lastupdtdt=sysdate where rownum<3;
                                                                                
update stgcsa.SOFVDUEBTBL                   
 set lastupdtuserid='ME', lastupdtdt=sysdate where rownum<3;
                                                                                
update stgcsa.SOFVFTPOTBL                   
 set lastupdtuserid='ME', lastupdtdt=sysdate where rownum<3;
                                                                                
update stgcsa.SOFVGNTYTBL                   
 set lastupdtuserid='ME', lastupdtdt=sysdate where rownum<3;
                                                                                
update stgcsa.SOFVGRGCTBL                   
 set lastupdtuserid='ME', lastupdtdt=sysdate where rownum<3;
                                                                                
update stgcsa.SOFVLND1TBL                   
 set lastupdtuserid='ME', LOANDTLBORRMAILADRZIPCD='99999',
lastupdtdt=sysdate where rownum<3;
                                                                                
update stgcsa.SOFVLND2TBL                   
 set lastupdtuserid='ME', lastupdtdt=sysdate where rownum<3;
                                                                                
update stgcsa.SOFVOTRNTBL                   
 set lastupdtuserid='ME', lastupdtdt=sysdate where rownum<3;
                                                                                
update stgcsa.SOFVPYMTTBL                   
 set lastupdtuserid='ME', lastupdtdt=sysdate where rownum<3;
                                                                                
update stgcsa.SOFVRTSCTBL                   
 set lastupdtuserid='ME', lastupdtdt=sysdate where rownum<3;
                                                                                
update stgcsa.SOFVBFFATBL                   
 set lastupdtuserid='ME',ASSUMPNM='CHANGED',
lastupdtdt=sysdate where rownum<3;
                                                                                
update stgcsa.SOFVBGDFTBL                   
 set lastupdtuserid='ME', lastupdtdt=sysdate where rownum<3;
exit
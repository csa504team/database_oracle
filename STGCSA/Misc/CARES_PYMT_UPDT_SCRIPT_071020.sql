/* first statement will not find a row to update                             
   it is intended to verify finding statements that do not match a row              
   when manually reviewing the log fog file.   */                                   
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112' where LOANNMB='NONESUCH  ' 
  and PYMTPOSTPYMTDT=to_DATE('02-APR-20','dd-MON-yy')  AND PYMTPOSTPYMTTYP='Z' and PYMTPOSTREMITTEDAMT=-9999;    
/* all following statements should update 1 row */                                  
                                                                                       
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='1005696010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1316.08;                                          
-- 1                                                                                                     
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='1015146002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2650.41;                                          
-- 2                                                                                                     
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='1015216010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4945.02;                                          
-- 3                                                                                                     
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='1016107010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=8898.65;                                          
-- 4                                                                                                     
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='1049476005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2358.86;                                          
-- 5                                                                                                     
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='1053617009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=10520.65;                                         
-- 6                                                                                                     
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='1068547004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4186.89;                                          
-- 7                                                                                                     
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='1078167001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=11325.69;                                         
-- 8                                                                                                     
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='1177897003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1898.88;                                          
-- 9                                                                                                     
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='1198706006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2697.79;                                          
-- 10                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='1213606010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=13158.97;                                         
-- 11                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='1241146000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1404.59;                                          
-- 12                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='1271246007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4159.42;                                          
-- 13                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='1282107002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2965.12;                                          
-- 14                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='1286146004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5021.09;                                          
-- 15                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='1295837008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5965.81;                                          
-- 16                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='1325246003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2247.44;                                          
-- 17                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='1341984010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1164.24;                                          
-- 18                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='1399297006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=8435.58;                                          
-- 19                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='1505266006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=8762.55;                                          
-- 20                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='1514046001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1312.31;                                          
-- 21                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='1519056003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1816;                                             
-- 22                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='1521617000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=8894.24;                                          
-- 23                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='1533706004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4030.73;                                          
-- 24                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='1559496000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5335.79;                                          
-- 25                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='1624607000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3890.37;                                          
-- 26                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='1743557001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5111.16;                                          
-- 27                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='1818967001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=19609.6;                                          
-- 28                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='1852427007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4177;                                             
-- 29                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='1900026004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1713.89;                                          
-- 30                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='1907896010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3638.34;                                          
-- 31                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='1932777007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5664.9;                                           
-- 32                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2043756010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3051;                                             
-- 33                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2060627004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=10450.13;                                         
-- 34                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2096126002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2886.43;                                          
-- 35                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2099367006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5271.6;                                           
-- 36                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2101457009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=7692.02;                                          
-- 37                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2128946007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=9667.97;                                          
-- 38                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2139457004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1317.77;                                          
-- 39                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2146947007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=9387.35;                                          
-- 40                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2162596002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1973.85;                                          
-- 41                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2166747003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2850.23;                                          
-- 42                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2171376008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3477.28;                                          
-- 43                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2183666009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2102.12;                                          
-- 44                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2197297002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4915.94;                                          
-- 45                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2209346006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4188.77;                                          
-- 46                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2218617002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=7411.87;                                          
-- 47                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2220746009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1103.3;                                           
-- 48                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2229656002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1315.65;                                          
-- 49                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2236926009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=906.57;                                           
-- 50                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2242156010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3791.3;                                           
-- 51                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2266406007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1384.18;                                          
-- 52                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2273186000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5806.41;                                          
-- 53                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2277477008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1571.2;                                           
-- 54                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2306036009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1703.35;                                          
-- 55                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2306596002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=837.25;                                           
-- 56                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2306876003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4443.11;                                          
-- 57                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2316317010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2758.33;                                          
-- 58                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2333707000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5568.48;                                          
-- 59                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2350646001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3120.97;                                          
-- 60                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2368467000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1075.31;                                          
-- 61                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2369106003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1403.69;                                          
-- 62                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2382976000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1144.33;                                          
-- 63                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2386096004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1739.19;                                          
-- 64                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2391856001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2898.64;                                          
-- 65                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2395096006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1965.59;                                          
-- 66                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2406476009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2431.01;                                          
-- 67                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2411356002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5270.89;                                          
-- 68                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2415996003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2547.78;                                          
-- 69                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2426606008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3363.55;                                          
-- 70                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2433356003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=9191.95;                                          
-- 71                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2439186001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2886.09;                                          
-- 72                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2442116003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3486.36;                                          
-- 73                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2444796001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=930.46;                                           
-- 74                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2449676010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1172.15;                                          
-- 75                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2450487005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=24428.17;                                         
-- 76                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2458116008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2979.49;                                          
-- 77                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2472427010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1333.5;                                           
-- 78                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2487346006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2922.46;                                          
-- 79                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2509177001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3220.74;                                          
-- 80                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2509506008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6831.62;                                          
-- 81                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2511096004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4994.5;                                           
-- 82                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2511156009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1595.36;                                          
-- 83                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2524517005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2286.13;                                          
-- 84                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2536976004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=940.25;                                           
-- 85                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2538636000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=711.84;                                           
-- 86                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2542767003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4852.34;                                          
-- 87                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2549966000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1556.18;                                          
-- 88                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2550947009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1978.87;                                          
-- 89                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2555866001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5925.65;                                          
-- 90                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2560247004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=20876.24;                                         
-- 91                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2566266004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6897.14;                                          
-- 92                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2574546005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4212.46;                                          
-- 93                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2576306007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1384.95;                                          
-- 94                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2577946002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=8273.09;                                          
-- 95                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2588696005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=10129.78;                                         
-- 96                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2590236006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=9562.94;                                          
-- 97                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2592617002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2728.14;                                          
-- 98                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2596917006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=9824.49;                                          
-- 99                                                                                                    
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2600196001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3636.84;                                          
-- 100                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2602956008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6145.89;                                          
-- 101                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2607457006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3591.84;                                          
-- 102                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2619106007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2191.79;                                          
-- 103                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2624307009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1945.57;                                          
-- 104                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2626826009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2004.61;                                          
-- 105                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2633956000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=9794.21;                                          
-- 106                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2634826009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6746.47;                                          
-- 107                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2648776008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4158.08;                                          
-- 108                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2650396000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2941.69;                                          
-- 109                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2654227010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=19133.46;                                         
-- 110                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2659136010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4524.35;                                          
-- 111                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2660327001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5680.15;                                          
-- 112                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2660946003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4205.21;                                          
-- 113                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2663267002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3090.89;                                          
-- 114                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2670806000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=480.81;                                           
-- 115                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2677706008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1359.11;                                          
-- 116                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2681936010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2819;                                             
-- 117                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2693466008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3206.51;                                          
-- 118                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2694866001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=806.12;                                           
-- 119                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2702436002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2846.04;                                          
-- 120                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2709166007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3720.95;                                          
-- 121                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2711136008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=972.84;                                           
-- 122                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2712106001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=8001.35;                                          
-- 123                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2713726001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1467.75;                                          
-- 124                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2715477009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2884.24;                                          
-- 125                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2720766000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1663.07;                                          
-- 126                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2721676010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6221.92;                                          
-- 127                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2727176003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4373.92;                                          
-- 128                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2730416004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6080.53;                                          
-- 129                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2742927008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3457.13;                                          
-- 130                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2743996002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3088.87;                                          
-- 131                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2746326006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5706.01;                                          
-- 132                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2763247004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5047.94;                                          
-- 133                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2766956004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=518.14;                                           
-- 134                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2767106009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5859.37;                                          
-- 135                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2767686008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1978.83;                                          
-- 136                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2768926010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3064.31;                                          
-- 137                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2770096009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=8376.85;                                          
-- 138                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2775786003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1227.16;                                          
-- 139                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2776976003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2061.19;                                          
-- 140                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2777837005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6173.11;                                          
-- 141                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2780416002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6711.19;                                          
-- 142                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2780636009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2067.64;                                          
-- 143                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2780746007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3097.32;                                          
-- 144                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2794436009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1171.71;                                          
-- 145                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2801756010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1865.56;                                          
-- 146                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2804237009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6724.31;                                          
-- 147                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2806477004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3341.25;                                          
-- 148                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2808386009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3026.89;                                          
-- 149                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2809767003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4585;                                             
-- 150                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2813186002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1309.7;                                           
-- 151                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2813234001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2363.97;                                          
-- 152                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2813446008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=796.94;                                           
-- 153                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2823276009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3319.44;                                          
-- 154                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2829607009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4519.49;                                          
-- 155                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2831586008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=10612.69;                                         
-- 156                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2832907006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1358.28;                                          
-- 157                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2836436008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4961.32;                                          
-- 158                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2839127000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=17213.5;                                          
-- 159                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2840216004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5856.38;                                          
-- 160                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2858706007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=10105.48;                                         
-- 161                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2874636010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4391.39;                                          
-- 162                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2884466000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3085.12;                                          
-- 163                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2886326008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3545.71;                                          
-- 164                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2891466009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2062.84;                                          
-- 165                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2895636009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2502.52;                                          
-- 166                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2896356010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5608.06;                                          
-- 167                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2898166005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5101.97;                                          
-- 168                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2899666004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4550.7;                                           
-- 169                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2901616000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2081.77;                                          
-- 170                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2906086006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1102;                                             
-- 171                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2918796004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=569.21;                                           
-- 172                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2919896001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=7867.16;                                          
-- 173                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2922236003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1820.49;                                          
-- 174                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2931386004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=585.84;                                           
-- 175                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2933716000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3840.6;                                           
-- 176                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2936506002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=7064.87;                                          
-- 177                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2943446006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3214.38;                                          
-- 178                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2945856004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3589.38;                                          
-- 179                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2946656005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=13276.84;                                         
-- 180                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2947167009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=9157.51;                                          
-- 181                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2948596004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1478.49;                                          
-- 182                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2949196004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=939.36;                                           
-- 183                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2950284001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2761.82;                                          
-- 184                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2953586006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3686.88;                                          
-- 185                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2959286000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4488.24;                                          
-- 186                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2961016010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6994.95;                                          
-- 187                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2964856010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4972.41;                                          
-- 188                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2968266007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3090.67;                                          
-- 189                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2968456005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=13773.78;                                         
-- 190                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2972386000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3587.58;                                          
-- 191                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2980496009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2068.25;                                          
-- 192                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2981986005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=8105.05;                                          
-- 193                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2982487006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2984.42;                                          
-- 194                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2987816001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4956.27;                                          
-- 195                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2989966004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5916.26;                                          
-- 196                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2990596008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3999.08;                                          
-- 197                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='2999736009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5112.06;                                          
-- 198                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3000024005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2036.71;                                          
-- 199                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3000986003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2797.72;                                          
-- 200                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3001567004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3160.31;                                          
-- 201                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3005647001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=32590.24;                                         
-- 202                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3009216002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2424.01;                                          
-- 203                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3013467006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1179.99;                                          
-- 204                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3020006010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3052.76;                                          
-- 205                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3021526004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=14450.81;                                         
-- 206                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3024686001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1171.89;                                          
-- 207                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3026176005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5856.44;                                          
-- 208                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3035776010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2281.82;                                          
-- 209                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3039716000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2739.52;                                          
-- 210                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3042914010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=821.16;                                           
-- 211                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3044566008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1329.59;                                          
-- 212                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3051207010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200710','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5083.62;                                          
-- 213                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3059536002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5322.08;                                          
-- 214                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3062737000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=13493.95;                                         
-- 215                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3071686004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2659.19;                                          
-- 216                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3074936002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=8964.82;                                          
-- 217                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3075636008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1569.34;                                          
-- 218                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3090136009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3194.68;                                          
-- 219                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3098146006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=10919.84;                                         
-- 220                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3098176004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=9250.19;                                          
-- 221                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3098226006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=13416.46;                                         
-- 222                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3101866001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2593.99;                                          
-- 223                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3104136000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1112.33;                                          
-- 224                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3108236003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=16579.07;                                         
-- 225                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3111086007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=8731.06;                                          
-- 226                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3112197003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3554.63;                                          
-- 227                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3114986001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1549.15;                                          
-- 228                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3117717003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=12019.17;                                         
-- 229                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3119696007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=8648.09;                                          
-- 230                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3131556003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=12023.59;                                         
-- 231                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3136866001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5558.52;                                          
-- 232                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3142674007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2508.34;                                          
-- 233                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3145366006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1431.11;                                          
-- 234                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3148776006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=13010.39;                                         
-- 235                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3161286006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2904.39;                                          
-- 236                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3161806007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5884.07;                                          
-- 237                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3163446010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3114.12;                                          
-- 238                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3169386006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5199.25;                                          
-- 239                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3176946006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2540.86;                                          
-- 240                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3180236008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1306.84;                                          
-- 241                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3181966006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2152.07;                                          
-- 242                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3182846007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3171.59;                                          
-- 243                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3187756003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1298.46;                                          
-- 244                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3191326006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=11192.96;                                         
-- 245                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3192266003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2366.65;                                          
-- 246                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3192276006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4639.64;                                          
-- 247                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3194767000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=9611.84;                                          
-- 248                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3195756003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=857.71;                                           
-- 249                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3203337003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=28975.68;                                         
-- 250                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3205056010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1497.1;                                           
-- 251                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3207086001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1419.58;                                          
-- 252                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3211666001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2993.1;                                           
-- 253                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3215146006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=453.41;                                           
-- 254                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3218086007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5469.63;                                          
-- 255                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3222566001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3302.57;                                          
-- 256                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3225186000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6964.95;                                          
-- 257                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3226466003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6364.74;                                          
-- 258                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3265256002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2823.35;                                          
-- 259                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3269546003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2528.23;                                          
-- 260                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3269606008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1386.99;                                          
-- 261                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3273336002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=711.23;                                           
-- 262                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3276446006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1128.67;                                          
-- 263                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3279596000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2398.04;                                          
-- 264                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3282816006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3033.01;                                          
-- 265                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3292866003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=841.54;                                           
-- 266                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3318526005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=7326.27;                                          
-- 267                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3320666000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1903.07;                                          
-- 268                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3321726007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4194.07;                                          
-- 269                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3322046006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3184.54;                                          
-- 270                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3327574008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5451.67;                                          
-- 271                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3338856007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1367.21;                                          
-- 272                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3343477001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3408.4;                                           
-- 273                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3346076009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1999.59;                                          
-- 274                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3346705001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=8638.08;                                          
-- 275                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3350295001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=10191.44;                                         
-- 276                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3352386004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=988.66;                                           
-- 277                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3355865005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5776.65;                                          
-- 278                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3355935002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4512.2;                                           
-- 279                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3359497001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=7940.45;                                          
-- 280                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3364946003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1018.44;                                          
-- 281                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3373987002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1327.79;                                          
-- 282                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3379066002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=470.53;                                           
-- 283                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3379106001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=783.03;                                           
-- 284                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3384635005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=9976.22;                                          
-- 285                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3389176004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3704.32;                                          
-- 286                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3390185008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1708.39;                                          
-- 287                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3403935008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=839.48;                                           
-- 288                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3404086000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2198.2;                                           
-- 289                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3407056008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2883.82;                                          
-- 290                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3410815005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1671.31;                                          
-- 291                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3411855008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=570.02;                                           
-- 292                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3413085006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1503.86;                                          
-- 293                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3415726008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1087.13;                                          
-- 294                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3420576005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2736.5;                                           
-- 295                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3422346010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=709.17;                                           
-- 296                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3424885005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2810.51;                                          
-- 297                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3426706008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2758.5;                                           
-- 298                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3432766000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1195.14;                                          
-- 299                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3433236007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1508.31;                                          
-- 300                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3435355005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5657.11;                                          
-- 301                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3444465005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=12809.87;                                         
-- 302                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3445825008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1333.74;                                          
-- 303                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3474537006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=7019.21;                                          
-- 304                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3475656002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=8744.96;                                          
-- 305                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3487277010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1614.13;                                          
-- 306                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3488535004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2878.03;                                          
-- 307                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3500187002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=7814.54;                                          
-- 308                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3500967000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=961.87;                                           
-- 309                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3512676008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=9274.89;                                          
-- 310                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3526737010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3924.38;                                          
-- 311                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3529535001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3695.44;                                          
-- 312                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3535005002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2320.4;                                           
-- 313                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3540235006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1941.76;                                          
-- 314                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3541886000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=357.93;                                           
-- 315                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3552066007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1872.4;                                           
-- 316                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3558335010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=7222.69;                                          
-- 317                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3562545006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=11856.44;                                         
-- 318                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3565146006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=10739.56;                                         
-- 319                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3569425008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=832;                                              
-- 320                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3570505005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=11737.57;                                         
-- 321                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3573465001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1274.86;                                          
-- 322                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3574137001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6064.79;                                          
-- 323                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3575085009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3175.6;                                           
-- 324                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3578806002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1636.83;                                          
-- 325                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3581396000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2180.42;                                          
-- 326                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3591275007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=22927.32;                                         
-- 327                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3598577009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200710','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2413.35;                                          
-- 328                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3601415006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4386.93;                                          
-- 329                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3605376000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4212.49;                                          
-- 330                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3617474000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3194.12;                                          
-- 331                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3627166008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=13444.3;                                          
-- 332                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3628177009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200710','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3172.43;                                          
-- 333                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3628335006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2950.62;                                          
-- 334                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3628446000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2229.46;                                          
-- 335                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3628975010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3440.93;                                          
-- 336                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3630127004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5892.79;                                          
-- 337                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3632434008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5753.24;                                          
-- 338                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3637735010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2295.76;                                          
-- 339                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3639956006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2562.86;                                          
-- 340                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3647375002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2333.23;                                          
-- 341                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3651195010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2207.73;                                          
-- 342                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3656277001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1398.48;                                          
-- 343                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3659696008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4292.08;                                          
-- 344                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3660967010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=430.57;                                           
-- 345                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3661137010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200710','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=11027.26;                                         
-- 346                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3671395008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6243.07;                                          
-- 347                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3674256003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1270.76;                                          
-- 348                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3675786000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1961.84;                                          
-- 349                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3680886000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3542.79;                                          
-- 350                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3680947001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=27523.56;                                         
-- 351                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3681486000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4930.57;                                          
-- 352                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3682375008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1329.65;                                          
-- 353                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3686206007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1798.33;                                          
-- 354                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3687587003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200710','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1205.75;                                          
-- 355                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3690796001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4908.36;                                          
-- 356                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3691026006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1310.85;                                          
-- 357                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3691844002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2122.72;                                          
-- 358                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3698357010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2141.76;                                          
-- 359                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3698486007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2586.85;                                          
-- 360                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3701885004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1418.66;                                          
-- 361                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3716195001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4470.85;                                          
-- 362                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3724905000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1608.14;                                          
-- 363                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3737495007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=681.1;                                            
-- 364                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3745825010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1951.76;                                          
-- 365                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3760535004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3247.77;                                          
-- 366                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3785855002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=465.19;                                           
-- 367                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3785895003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1616.56;                                          
-- 368                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3793505002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1350.79;                                          
-- 369                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3808095003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=998.38;                                           
-- 370                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3820225008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1101.34;                                          
-- 371                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3821275003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=9104.19;                                          
-- 372                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3821285006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1872.84;                                          
-- 373                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3832774010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2017.02;                                          
-- 374                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3837535003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2449.11;                                          
-- 375                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3840575005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=697.08;                                           
-- 376                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3842485006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1470.22;                                          
-- 377                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3845335002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1164.75;                                          
-- 378                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3853597002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200710','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1397.39;                                          
-- 379                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3853885003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3913.86;                                          
-- 380                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3856694004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3774.09;                                          
-- 381                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3902817004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5329.4;                                           
-- 382                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3910737004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=791.79;                                           
-- 383                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3912475010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2423.03;                                          
-- 384                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3934545008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3254.52;                                          
-- 385                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3935935009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2879.83;                                          
-- 386                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3940675008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=689.09;                                           
-- 387                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3964725004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2146.61;                                          
-- 388                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3965005002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1785.21;                                          
-- 389                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3970685001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4531.26;                                          
-- 390                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3985544001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3165.17;                                          
-- 391                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3989987000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4520.68;                                          
-- 392                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3996627006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200710','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1131.25;                                          
-- 393                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4005655006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=856.21;                                           
-- 394                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4006027010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2894.24;                                          
-- 395                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4007397007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1035.03;                                          
-- 396                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4008685010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1565.25;                                          
-- 397                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4021727006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2342.01;                                          
-- 398                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4022485005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4059.24;                                          
-- 399                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4025037008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200710','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1346.59;                                          
-- 400                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4036984007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2296.31;                                          
-- 401                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4061835006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=965.04;                                           
-- 402                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4062437009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1155.15;                                          
-- 403                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4067627006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=7712.14;                                          
-- 404                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4072485003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=10457.39;                                         
-- 405                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4086014007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1095.72;                                          
-- 406                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4115257008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1787.02;                                          
-- 407                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4121395008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5737.49;                                          
-- 408                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4121935004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1008.54;                                          
-- 409                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4130695006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2467.13;                                          
-- 410                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4149605001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1347.77;                                          
-- 411                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4150655002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=691.1;                                            
-- 412                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4152055003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4173.33;                                          
-- 413                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4153437004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200710','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1777.83;                                          
-- 414                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4160364006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1025.21;                                          
-- 415                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4193287001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3462.29;                                          
-- 416                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4193855003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2171.26;                                          
-- 417                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4199257004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200710','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2936.9;                                           
-- 418                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4201964006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2863.58;                                          
-- 419                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4217837006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3198.65;                                          
-- 420                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4218545001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2333.95;                                          
-- 421                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4237965004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1980.76;                                          
-- 422                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4240175004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2078.94;                                          
-- 423                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4248595006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6478.48;                                          
-- 424                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4256425001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=7378.13;                                          
-- 425                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4269785003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2636.55;                                          
-- 426                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4274785008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3760.78;                                          
-- 427                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4274925002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5886.45;                                          
-- 428                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4275385008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4974.96;                                          
-- 429                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4276105007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1101.93;                                          
-- 430                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4276565005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1797.86;                                          
-- 431                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4277135007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4418.96;                                          
-- 432                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4277265000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=8494.3;                                           
-- 433                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4278305001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2399.2;                                           
-- 434                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4278675007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3084.91;                                          
-- 435                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4279775004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4877.32;                                          
-- 436                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4279815003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1846.93;                                          
-- 437                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4297254009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1492.55;                                          
-- 438                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4320725003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5754.78;                                          
-- 439                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4331155004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4830.38;                                          
-- 440                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4339037000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200710','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=9846.46;                                          
-- 441                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4350454010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2277.51;                                          
-- 442                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4351805006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5825.71;                                          
-- 443                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4352895002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1615.06;                                          
-- 444                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4358055009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4814.63;                                          
-- 445                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4368475010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2493.67;                                          
-- 446                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4372414010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2276.6;                                           
-- 447                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4375415001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4959.22;                                          
-- 448                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4381535009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2436.01;                                          
-- 449                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4381935000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=8651.34;                                          
-- 450                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4402984008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=10192.22;                                         
-- 451                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4436135005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=946.16;                                           
-- 452                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4444725005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3571.49;                                          
-- 453                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4456955001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=7068.81;                                          
-- 454                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4457185008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4798.19;                                          
-- 455                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4460525004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1498.03;                                          
-- 456                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4479484009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1325.84;                                          
-- 457                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4484745005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4304.45;                                          
-- 458                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4488465001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3397.39;                                          
-- 459                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4501155008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6067.36;                                          
-- 460                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4504045005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1018.66;                                          
-- 461                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4506725001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=856.72;                                           
-- 462                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4511305009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1060.05;                                          
-- 463                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4511455008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1111.07;                                          
-- 464                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4516575008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1341.86;                                          
-- 465                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4529925005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1365.98;                                          
-- 466                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4533115001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3355.71;                                          
-- 467                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4555165006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1905.52;                                          
-- 468                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4556295001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2404.58;                                          
-- 469                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4558255004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4184.89;                                          
-- 470                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4569705003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=19650.86;                                         
-- 471                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4572335000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4107.57;                                          
-- 472                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4572485010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6427.74;                                          
-- 473                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4573775005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4939.43;                                          
-- 474                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4574465008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6935.13;                                          
-- 475                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4581505005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2263.59;                                          
-- 476                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4590065006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2946.91;                                          
-- 477                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4599495002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4439.94;                                          
-- 478                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4605584006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4728.73;                                          
-- 479                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4607554001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4820.08;                                          
-- 480                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4639635002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=14235.37;                                         
-- 481                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4647294000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3677.36;                                          
-- 482                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4647705010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=842.86;                                           
-- 483                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4647775009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=9910.35;                                          
-- 484                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4655995005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2943.59;                                          
-- 485                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4668345008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5255.21;                                          
-- 486                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4673675007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2880.27;                                          
-- 487                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4686675006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1367.5;                                           
-- 488                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4708205007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2097.26;                                          
-- 489                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4708495002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1935.88;                                          
-- 490                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4723005004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2405.37;                                          
-- 491                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4724735002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1519.44;                                          
-- 492                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4729215009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=12223.78;                                         
-- 493                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4732265003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1642.49;                                          
-- 494                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4737075004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1716.09;                                          
-- 495                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4741615003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=743.97;                                           
-- 496                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4751304001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3237.12;                                          
-- 497                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4759615001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4498.75;                                          
-- 498                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4770225003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1919.89;                                          
-- 499                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4772645004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3886;                                             
-- 500                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4778395002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1043.22;                                          
-- 501                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4780245002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1495.05;                                          
-- 502                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4783595008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6902.24;                                          
-- 503                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4783965001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=570.6;                                            
-- 504                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4792835010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=353.44;                                           
-- 505                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4795225010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=10758.89;                                         
-- 506                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4796355005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=852.59;                                           
-- 507                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4799345008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1076.25;                                          
-- 508                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4820905005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=9570.61;                                          
-- 509                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4828555001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4005.67;                                          
-- 510                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4831305008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=27655.82;                                         
-- 511                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4838385002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1462.6;                                           
-- 512                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4857885005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1562.1;                                           
-- 513                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4862795007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3178.18;                                          
-- 514                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4863595008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1813.04;                                          
-- 515                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4865295005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1329.1;                                           
-- 516                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4882665000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3477.15;                                          
-- 517                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4898045007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=24123.37;                                         
-- 518                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4898085008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=10946.01;                                         
-- 519                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4898865006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=27959.75;                                         
-- 520                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4902095004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1221.96;                                          
-- 521                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4903775009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=8594.33;                                          
-- 522                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4907895007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3060.15;                                          
-- 523                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4908205001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1552.26;                                          
-- 524                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4910275010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1340.68;                                          
-- 525                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4911565005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1135.86;                                          
-- 526                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4927755008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3357.1;                                           
-- 527                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4934964003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5576.8;                                           
-- 528                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4937345007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4053.22;                                          
-- 529                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4937754005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1208.42;                                          
-- 530                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4941044007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1069.44;                                          
-- 531                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4941464004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=9667.64;                                          
-- 532                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4942605007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200710','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1535.71;                                          
-- 533                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4947005003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1216.72;                                          
-- 534                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4947445006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1820.84;                                          
-- 535                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4950715005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2556.1;                                           
-- 536                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4950775001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=9857.06;                                          
-- 537                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4952875000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2791.93;                                          
-- 538                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4962835003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=7203.03;                                          
-- 539                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4968565006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1957.63;                                          
-- 540                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4969195004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3193.31;                                          
-- 541                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4980385003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1647.34;                                          
-- 542                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4986065002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3606.34;                                          
-- 543                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4991015003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4109.87;                                          
-- 544                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4993365007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1051.99;                                          
-- 545                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4998415008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3649.9;                                           
-- 546                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5004425010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1513.19;                                          
-- 547                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5012735009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2925.68;                                          
-- 548                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5017655008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4257.58;                                          
-- 549                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5020415007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=19128.44;                                         
-- 550                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5024595001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=17969.94;                                         
-- 551                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5030045007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5302.45;                                          
-- 552                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5038494000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3066.5;                                           
-- 553                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5054485004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3203.41;                                          
-- 554                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5054585010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2529.79;                                          
-- 555                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5060465005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4990.17;                                          
-- 556                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5062095005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=14070.81;                                         
-- 557                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5067745009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3433.67;                                          
-- 558                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5071475003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=12955.53;                                         
-- 559                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5074374007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=828.1;                                            
-- 560                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5100035000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=649.07;                                           
-- 561                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5106615009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2266.66;                                          
-- 562                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5110425003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5006.14;                                          
-- 563                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5111305004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=23655.35;                                         
-- 564                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5116475008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=8414.94;                                          
-- 565                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5125535004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1205.01;                                          
-- 566                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5129975004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3275.82;                                          
-- 567                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5134965006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2144.21;                                          
-- 568                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5137925000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3997.81;                                          
-- 569                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5141395010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1822.22;                                          
-- 570                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5147565003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2402.56;                                          
-- 571                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5148975010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6593.96;                                          
-- 572                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5150755002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6462.9;                                           
-- 573                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5151655009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5350.45;                                          
-- 574                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5158715006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=898.42;                                           
-- 575                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5162395009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3600.48;                                          
-- 576                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5165665002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=20422.13;                                         
-- 577                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5165925008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=962.13;                                           
-- 578                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5189695005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4327.71;                                          
-- 579                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5191945007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3498.5;                                           
-- 580                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5194895000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2074.36;                                          
-- 581                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5195475005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=924.85;                                           
-- 582                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5199525004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3130.08;                                          
-- 583                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5202975001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=7001.01;                                          
-- 584                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5211865005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4586.08;                                          
-- 585                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5220035005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5842.31;                                          
-- 586                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5222105006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2715.61;                                          
-- 587                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5223175007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4630.9;                                           
-- 588                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5225035004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=10034.78;                                         
-- 589                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5227885005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=9295.73;                                          
-- 590                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5228345009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4269.36;                                          
-- 591                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5244155000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3229.44;                                          
-- 592                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5249245002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=10376;                                            
-- 593                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5255365010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3221.86;                                          
-- 594                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5257075010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=837.69;                                           
-- 595                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5268135010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=7765.9;                                           
-- 596                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5270595007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6629.74;                                          
-- 597                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5274715003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=7018.74;                                          
-- 598                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5276754001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1209.62;                                          
-- 599                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5278215003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1461.69;                                          
-- 600                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5280155008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2526.02;                                          
-- 601                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5291155003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=11442.6;                                          
-- 602                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5294735006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2377.14;                                          
-- 603                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5296865003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3821.47;                                          
-- 604                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5300755009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1590.93;                                          
-- 605                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5310815007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6747.93;                                          
-- 606                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5313504007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=9577.83;                                          
-- 607                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5315804007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=967.84;                                           
-- 608                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5317225010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3269.55;                                          
-- 609                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5323685004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=7843.92;                                          
-- 610                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5324365004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=20319.54;                                         
-- 611                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5328565002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=15421.51;                                         
-- 612                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5332935009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5518.17;                                          
-- 613                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5335455002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1537.86;                                          
-- 614                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5337495007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=18232.66;                                         
-- 615                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5338845007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6874.37;                                          
-- 616                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5340305008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=10537.14;                                         
-- 617                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5343185004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4847.89;                                          
-- 618                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5344195009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=766.04;                                           
-- 619                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5354915004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1152.75;                                          
-- 620                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5363185001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=456.61;                                           
-- 621                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5365935005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3065.22;                                          
-- 622                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5389305009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2521.95;                                          
-- 623                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5389855010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=832.56;                                           
-- 624                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5391185009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1787.89;                                          
-- 625                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5393115003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=28559.06;                                         
-- 626                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5394895005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5085.55;                                          
-- 627                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5394975005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2070.12;                                          
-- 628                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5398905003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1164.12;                                          
-- 629                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5400144004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5612.72;                                          
-- 630                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5405905002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5690.34;                                          
-- 631                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5413755005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3512.33;                                          
-- 632                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5421745002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6653.83;                                          
-- 633                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5429165010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1282.83;                                          
-- 634                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5431544009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1724.37;                                          
-- 635                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5436225002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1299.76;                                          
-- 636                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5445455003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=12467.59;                                         
-- 637                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5445515008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=13020.82;                                         
-- 638                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5458805000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1819.27;                                          
-- 639                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5467335003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1037.83;                                          
-- 640                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5479955009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=17888.6;                                          
-- 641                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5483555010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2448.09;                                          
-- 642                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5499725007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3622.77;                                          
-- 643                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5507675006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=7206.75;                                          
-- 644                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5522105008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3951.75;                                          
-- 645                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5523655000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2139.19;                                          
-- 646                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5528235002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=642.51;                                           
-- 647                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5540924004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1096.71;                                          
-- 648                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5553435005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6415.69;                                          
-- 649                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5559675008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6016.03;                                          
-- 650                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5560935000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=993.16;                                           
-- 651                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5573355002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5219.16;                                          
-- 652                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5584275008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=794.89;                                           
-- 653                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5597215000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=935.5;                                            
-- 654                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5622195010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6325.23;                                          
-- 655                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5647645010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4309.85;                                          
-- 656                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5648555009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=799.67;                                           
-- 657                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5650914002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1633.9;                                           
-- 658                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5671385004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=17443.57;                                         
-- 659                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5678245000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1339.22;                                          
-- 660                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5696685007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1239.31;                                          
-- 661                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5711165003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=21549.22;                                         
-- 662                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5720015006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=9493.7;                                           
-- 663                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5729644007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4111.39;                                          
-- 664                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5744875000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1202.82;                                          
-- 665                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5745715000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1963.39;                                          
-- 666                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5748264006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1404.76;                                          
-- 667                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5785365007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2603.85;                                          
-- 668                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5787465006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=496.06;                                           
-- 669                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5839975009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=11395.09;                                         
-- 670                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5845584010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2820.11;                                          
-- 671                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5847634005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=836.47;                                           
-- 672                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5868255004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=7893.62;                                          
-- 673                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5884624001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=373.14;                                           
-- 674                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5906884000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4191.94;                                          
-- 675                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5912834003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2261.82;                                          
-- 676                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5919084006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=360.74;                                           
-- 677                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5939994005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2451.58;                                          
-- 678                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5952635003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2246.15;                                          
-- 679                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5968195007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=8223.45;                                          
-- 680                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5979454001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2820.39;                                          
-- 681                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5988165006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1808.62;                                          
-- 682                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5988824007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1737.83;                                          
-- 683                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5992625005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2895.99;                                          
-- 684                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5993675000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=517.92;                                           
-- 685                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='5999244005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=923.17;                                           
-- 686                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6000945010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3565.78;                                          
-- 687                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6018995001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=9689.18;                                          
-- 688                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6024434008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3437.23;                                          
-- 689                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6044915003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3225.51;                                          
-- 690                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6049135004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=10342.45;                                         
-- 691                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6051085001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2885.29;                                          
-- 692                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6066505010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2801.28;                                          
-- 693                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6067155003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3953.25;                                          
-- 694                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6067895002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5028.55;                                          
-- 695                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6068625004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4712.66;                                          
-- 696                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6095865001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=7563.75;                                          
-- 697                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6104094002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4324.89;                                          
-- 698                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6108865001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4193.94;                                          
-- 699                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6111735009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4454.24;                                          
-- 700                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6112584007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=962.18;                                           
-- 701                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6113894008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2281.95;                                          
-- 702                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6145245002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=775.35;                                           
-- 703                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6147145000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=984;                                              
-- 704                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6148925000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1152.51;                                          
-- 705                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6163585003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=12046.93;                                         
-- 706                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6163785004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1522.56;                                          
-- 707                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6168395004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3486.09;                                          
-- 708                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6175075000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=21403.55;                                         
-- 709                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6195665008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1624.21;                                          
-- 710                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6196974002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1132.23;                                          
-- 711                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6196995004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=620.17;                                           
-- 712                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6198744007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1227.42;                                          
-- 713                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6204885007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1813.8;                                           
-- 714                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6210955000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1255.84;                                          
-- 715                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6217045001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=486.93;                                           
-- 716                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6226305009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2847.11;                                          
-- 717                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6227664007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3241.51;                                          
-- 718                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6233675002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=887.66;                                           
-- 719                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6237705006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1370.59;                                          
-- 720                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6238485003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2321.88;                                          
-- 721                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6239385010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4480.46;                                          
-- 722                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6248245005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=8560.84;                                          
-- 723                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6255944005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1292.77;                                          
-- 724                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6257744008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3053.16;                                          
-- 725                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6275195009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=981.79;                                           
-- 726                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6281035003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3786.84;                                          
-- 727                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6287925000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6379.98;                                          
-- 728                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6289634004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=805.59;                                           
-- 729                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6290125008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=13505.66;                                         
-- 730                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6306725006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2289.1;                                           
-- 731                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6310835007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4785.54;                                          
-- 732                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6318415004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=23073.24;                                         
-- 733                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6333975006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=707.84;                                           
-- 734                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6335995005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2103.17;                                          
-- 735                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6346454006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2838.36;                                          
-- 736                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6349645006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1367.89;                                          
-- 737                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6350815006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1946.25;                                          
-- 738                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6355315008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=9180.63;                                          
-- 739                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6360295009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1080.35;                                          
-- 740                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6362655003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6286.34;                                          
-- 741                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6364385009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3677.33;                                          
-- 742                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6365165004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=8723.36;                                          
-- 743                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6367475007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4862.05;                                          
-- 744                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6370255001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=943.66;                                           
-- 745                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6382424005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=927.74;                                           
-- 746                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6402445005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2229.23;                                          
-- 747                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6412965001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=10742.12;                                         
-- 748                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6415405003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5314.14;                                          
-- 749                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6422685004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2591.28;                                          
-- 750                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6425045006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=18750.88;                                         
-- 751                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6425095010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=9971.07;                                          
-- 752                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6432705001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=23072.77;                                         
-- 753                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6438455010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2704.78;                                          
-- 754                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6444064000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=878.32;                                           
-- 755                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6445855010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=825.68;                                           
-- 756                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6450205008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1163.68;                                          
-- 757                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6451005009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2254.13;                                          
-- 758                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6453475003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=8536.21;                                          
-- 759                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6454564001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=7971.52;                                          
-- 760                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6456355008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=18958.1;                                          
-- 761                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6456665007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=10773.82;                                         
-- 762                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6474204006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1908.8;                                           
-- 763                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6486364005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1756.44;                                          
-- 764                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6492974007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1537.29;                                          
-- 765                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6521235004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=455.63;                                           
-- 766                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6524005000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4159.85;                                          
-- 767                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6529545008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=50380.5;                                          
-- 768                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6529705008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4968.84;                                          
-- 769                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6544355008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=783.43;                                           
-- 770                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6547855000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4197.35;                                          
-- 771                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6548625003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2039.89;                                          
-- 772                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6553405001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=26305.28;                                         
-- 773                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6555914009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1454.01;                                          
-- 774                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6568465000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=29729.01;                                         
-- 775                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6570925006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1489.47;                                          
-- 776                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6573484004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3002.44;                                          
-- 777                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6574235008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=534.22;                                           
-- 778                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6578325008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=7619.02;                                          
-- 779                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6585635005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=11616.4;                                          
-- 780                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6601175010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2862.36;                                          
-- 781                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6601915007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2604.07;                                          
-- 782                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6604275000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=7630.33;                                          
-- 783                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6605235001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=8378.98;                                          
-- 784                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6610565000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5475.79;                                          
-- 785                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6631135010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=418.02;                                           
-- 786                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6634745000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=363.3;                                            
-- 787                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6646794005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1576.05;                                          
-- 788                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6653515007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1030.88;                                          
-- 789                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6662415003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1560.87;                                          
-- 790                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6665435004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3663.81;                                          
-- 791                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6670335003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4629.92;                                          
-- 792                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6676015002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3669.31;                                          
-- 793                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6677434005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4032.37;                                          
-- 794                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6680355002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5552.63;                                          
-- 795                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6684615005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=18963.72;                                         
-- 796                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6688695004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=889.61;                                           
-- 797                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6693914007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=898.79;                                           
-- 798                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6698525003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2672.72;                                          
-- 799                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6699075001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4707.84;                                          
-- 800                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6701145009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5835.08;                                          
-- 801                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6704395009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1213.21;                                          
-- 802                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6704665007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=913.76;                                           
-- 803                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6713145006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=18016.77;                                         
-- 804                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6722175006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2297.69;                                          
-- 805                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6726485002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2649.53;                                          
-- 806                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6733535002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=909.96;                                           
-- 807                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6733794003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4011.01;                                          
-- 808                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6735245002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1314.68;                                          
-- 809                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6735365003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4252.64;                                          
-- 810                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6737425001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4366.47;                                          
-- 811                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6745565008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1144.41;                                          
-- 812                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6749785001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6970.07;                                          
-- 813                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6753805008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2910.2;                                           
-- 814                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6756845004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3679.06;                                          
-- 815                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6771505005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=13784.36;                                         
-- 816                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6783275005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=690.53;                                           
-- 817                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6813935004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2131.91;                                          
-- 818                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6822035007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=12247.55;                                         
-- 819                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6824894004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6577.63;                                          
-- 820                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6829045002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=512.77;                                           
-- 821                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6840245004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1629.47;                                          
-- 822                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6887515010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1719.47;                                          
-- 823                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6889955006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=998.58;                                           
-- 824                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6890175005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2528.06;                                          
-- 825                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6891795005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=801.18;                                           
-- 826                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6906465004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6240.17;                                          
-- 827                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6916495006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=8375.23;                                          
-- 828                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6917115010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1121.14;                                          
-- 829                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6924325001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=8062.8;                                           
-- 830                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6925235000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2262.18;                                          
-- 831                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6929544000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4472.63;                                          
-- 832                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6938374010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1816.92;                                          
-- 833                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6942735010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3589.74;                                          
-- 834                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6945905008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2171.51;                                          
-- 835                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6948895002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3753.92;                                          
-- 836                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6955035000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4167.45;                                          
-- 837                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6956534003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1589.67;                                          
-- 838                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6973485010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4918.24;                                          
-- 839                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6978515005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1882.7;                                           
-- 840                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6984025007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3776.21;                                          
-- 841                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6986065001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=12504.79;                                         
-- 842                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6987265004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1593.02;                                          
-- 843                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6988965004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=7031.21;                                          
-- 844                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6989045001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2119.26;                                          
-- 845                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='6994065001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=19980.41;                                         
-- 846                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7001084001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4030.09;                                          
-- 847                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7004855009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1233.67;                                          
-- 848                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7005435003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=473.49;                                           
-- 849                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7008985010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4775.16;                                          
-- 850                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7016955001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=8801.94;                                          
-- 851                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7024235008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2655.71;                                          
-- 852                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7024335003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=11715.59;                                         
-- 853                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7026665001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4267.43;                                          
-- 854                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7027455010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=10824.46;                                         
-- 855                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7030875008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2113.86;                                          
-- 856                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7036355006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1308.76;                                          
-- 857                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7036985007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1649.49;                                          
-- 858                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7043774005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1760.42;                                          
-- 859                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7056555004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=26145.42;                                         
-- 860                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7061005008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1872.55;                                          
-- 861                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7074735003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=11446.21;                                         
-- 862                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7078645008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2183.91;                                          
-- 863                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7079705004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5377.66;                                          
-- 864                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7085545000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1033.86;                                          
-- 865                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7087575002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2252.06;                                          
-- 866                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7095505003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5343.28;                                          
-- 867                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7108385004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5806.93;                                          
-- 868                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7118985000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4947.03;                                          
-- 869                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7119264002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1329.1;                                           
-- 870                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7121495007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5514.77;                                          
-- 871                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7124505003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5226.68;                                          
-- 872                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7125545006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3448.93;                                          
-- 873                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7125735004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2355.01;                                          
-- 874                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7135275001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4141.68;                                          
-- 875                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7138235006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=842.9;                                            
-- 876                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7149255007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=19718.42;                                         
-- 877                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7158215008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=15601.96;                                         
-- 878                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7172864004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3612.72;                                          
-- 879                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7180765005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3663.02;                                          
-- 880                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7188394010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1603.31;                                          
-- 881                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7189894009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=809.23;                                           
-- 882                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7191625004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1742.23;                                          
-- 883                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7199024010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1472.25;                                          
-- 884                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7199315010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1283.85;                                          
-- 885                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7204295001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3946.92;                                          
-- 886                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7223754007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2640.62;                                          
-- 887                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7227024008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2649.44;                                          
-- 888                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7230075009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1420.45;                                          
-- 889                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7240225010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=14056.95;                                         
-- 890                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7244225007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=8488.75;                                          
-- 891                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7246834010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3657.94;                                          
-- 892                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7250664010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3381.84;                                          
-- 893                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7258854002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3388.77;                                          
-- 894                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7259384005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1004.83;                                          
-- 895                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7259704005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2208.1;                                           
-- 896                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7278755000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=894.43;                                           
-- 897                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7293925001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6684.16;                                          
-- 898                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7314095001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1795.65;                                          
-- 899                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7314665006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1915.48;                                          
-- 900                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7323594004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4077.07;                                          
-- 901                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7354644009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1277.71;                                          
-- 902                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7361775007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4133.48;                                          
-- 903                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7393795007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=7027.92;                                          
-- 904                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7395275008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=731.8;                                            
-- 905                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7404425008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3533.28;                                          
-- 906                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7412784004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2855.96;                                          
-- 907                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7414195004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=9858.94;                                          
-- 908                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7414475005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=7684.33;                                          
-- 909                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7435415008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2155.39;                                          
-- 910                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7446934010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2935.21;                                          
-- 911                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7448974004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=358.21;                                           
-- 912                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7450495008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4131.42;                                          
-- 913                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7457645008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5444.55;                                          
-- 914                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7483094006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2155.24;                                          
-- 915                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7490844004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4600.37;                                          
-- 916                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7500454010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2415;                                             
-- 917                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7505495006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=18311.09;                                         
-- 918                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7525034009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6364.87;                                          
-- 919                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7534094007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1105.79;                                          
-- 920                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7548534009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3868.22;                                          
-- 921                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7549245003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2623.49;                                          
-- 922                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7559875008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4468.68;                                          
-- 923                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7560114004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1115.85;                                          
-- 924                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7561425001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1148.02;                                          
-- 925                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7564855007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5914.68;                                          
-- 926                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7570124000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1982.21;                                          
-- 927                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7571565001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1826.38;                                          
-- 928                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7585825008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1290.73;                                          
-- 929                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7586174009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1863.27;                                          
-- 930                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7587785002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1944.6;                                           
-- 931                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7596555005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4198.87;                                          
-- 932                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7599065006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3953.52;                                          
-- 933                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7601905009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1706.9;                                           
-- 934                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7605035005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3016.83;                                          
-- 935                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7605465005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5277.51;                                          
-- 936                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7615105006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2371.41;                                          
-- 937                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7615195000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=23670.27;                                         
-- 938                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7615985001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1567.51;                                          
-- 939                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7627225004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1743.85;                                          
-- 940                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7632745001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200710','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2649.18;                                          
-- 941                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7636784003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=939.94;                                           
-- 942                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7650995010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3011.75;                                          
-- 943                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7666794007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3307.16;                                          
-- 944                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7670925008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=12565.65;                                         
-- 945                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7689925008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1238.11;                                          
-- 946                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7737874005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=7615.44;                                          
-- 947                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7740115008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1310.11;                                          
-- 948                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7741395002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=16021.91;                                         
-- 949                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7745905008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4794.25;                                          
-- 950                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7759034001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6397.69;                                          
-- 951                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7762255005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1401.3;                                           
-- 952                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7779974009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2299.87;                                          
-- 953                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7797175005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=379.56;                                           
-- 954                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7805865001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3106.62;                                          
-- 955                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7808805000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=848.59;                                           
-- 956                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7816015010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=7133.99;                                          
-- 957                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7818255005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2130.63;                                          
-- 958                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7821355001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5929.56;                                          
-- 959                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7854764006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4400.6;                                           
-- 960                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7855175004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4707.45;                                          
-- 961                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7858035003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3825.12;                                          
-- 962                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7861395007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=7627.38;                                          
-- 963                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7867365010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=10762.33;                                         
-- 964                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7870314000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5184.38;                                          
-- 965                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7874865005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=910.62;                                           
-- 966                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7878245004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2811.12;                                          
-- 967                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7889255002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4663.68;                                          
-- 968                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7913995002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2811.3;                                           
-- 969                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7932495000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=11561.62;                                         
-- 970                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7937995007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=11068.07;                                         
-- 971                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7943405001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4796.67;                                          
-- 972                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7943475000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6754.2;                                           
-- 973                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7944465010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=37696.99;                                         
-- 974                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7963215000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=461.31;                                           
-- 975                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7971765001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=29465.43;                                         
-- 976                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='7989594000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=561.58;                                           
-- 977                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8005595010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1727.29;                                          
-- 978                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8007425009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1878.71;                                          
-- 979                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8013135001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6384.79;                                          
-- 980                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8020885001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6883.03;                                          
-- 981                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8032725007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=24218.85;                                         
-- 982                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8038885010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=848.34;                                           
-- 983                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8057235009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2233.67;                                          
-- 984                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8059995007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3675.67;                                          
-- 985                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8062885010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=7512.24;                                          
-- 986                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8063585005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2148.56;                                          
-- 987                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8083305010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1950.61;                                          
-- 988                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8090525004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1568.72;                                          
-- 989                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8111955004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6426.52;                                          
-- 990                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8114525010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4335.71;                                          
-- 991                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8129765004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=8002.3;                                           
-- 992                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8130975005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2516.17;                                          
-- 993                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8140925005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=11697.74;                                         
-- 994                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8159145007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2752.41;                                          
-- 995                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8165095001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1254.81;                                          
-- 996                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8168075001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=8920.4;                                           
-- 997                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8168945002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2621.6;                                           
-- 998                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8169955007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3610.22;                                          
-- 999                                                                                                   
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8170305008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=9637.93;                                          
-- 1000                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8182475010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1664.6;                                           
-- 1001                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8182485002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=649.85;                                           
-- 1002                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8182785009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5986.46;                                          
-- 1003                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8188495006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3489.34;                                          
-- 1004                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8199834000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1341.06;                                          
-- 1005                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8209344000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2745.28;                                          
-- 1006                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8210335001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=10190.9;                                          
-- 1007                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8212985001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=483.15;                                           
-- 1008                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8239495007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=37377.49;                                         
-- 1009                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8240965003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1048.78;                                          
-- 1010                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8258475007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4257.61;                                          
-- 1011                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8265915006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=697.31;                                           
-- 1012                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8294104002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=794.74;                                           
-- 1013                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8296164002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=8961.09;                                          
-- 1014                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8302305000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4985.29;                                          
-- 1015                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8304335002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=490.51;                                           
-- 1016                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8319585010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6884.96;                                          
-- 1017                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8330565005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2066.08;                                          
-- 1018                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8333445010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=35414.52;                                         
-- 1019                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8345075003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2296.67;                                          
-- 1020                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8353105010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1654.17;                                          
-- 1021                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8360115000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=7705.98;                                          
-- 1022                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8385975007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=24930.95;                                         
-- 1023                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8422105004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=20843.37;                                         
-- 1024                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8431585010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2403.24;                                          
-- 1025                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8440445005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2410.48;                                          
-- 1026                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8443805001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4413.25;                                          
-- 1027                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8457455004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=13376.52;                                         
-- 1028                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8459385000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=36045.9;                                          
-- 1029                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8466405002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=8259.26;                                          
-- 1030                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8470265000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6388.73;                                          
-- 1031                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8509654001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=12140.2;                                          
-- 1032                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8529785009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1107.34;                                          
-- 1033                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8530434010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1517.24;                                          
-- 1034                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8534415008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6779.32;                                          
-- 1035                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8542534002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1003.19;                                          
-- 1036                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8542834009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=9687.37;                                          
-- 1037                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8556035002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3697.32;                                          
-- 1038                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8558045009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=7445.46;                                          
-- 1039                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8561915000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=8376.05;                                          
-- 1040                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8577485007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=870.33;                                           
-- 1041                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8588415003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5325.39;                                          
-- 1042                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8593015006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=9460.2;                                           
-- 1043                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8594495001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5240.04;                                          
-- 1044                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8599905003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5026.54;                                          
-- 1045                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8601735000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=7585.33;                                          
-- 1046                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8605665000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6925.97;                                          
-- 1047                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8609085000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=10891.97;                                         
-- 1048                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8620884009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1835.81;                                          
-- 1049                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8631825004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=7288.73;                                          
-- 1050                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8638235007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=8904.29;                                          
-- 1051                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8640654007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=9439.38;                                          
-- 1052                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8646265005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5770.16;                                          
-- 1053                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8649664006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=707.92;                                           
-- 1054                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8653004010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1773.74;                                          
-- 1055                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8656135005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4579.72;                                          
-- 1056                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8685855008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=9884.43;                                          
-- 1057                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8689675010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=19121.61;                                         
-- 1058                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8690614004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3306.56;                                          
-- 1059                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8694145009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4990.94;                                          
-- 1060                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8702074006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3309.64;                                          
-- 1061                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8703414003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=7751.9;                                           
-- 1062                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8713985010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2895.39;                                          
-- 1063                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8726675010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1942.42;                                          
-- 1064                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8766755004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2141.48;                                          
-- 1065                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8788104002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3020.05;                                          
-- 1066                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8788775000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2646.47;                                          
-- 1067                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8793154000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1198.57;                                          
-- 1068                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8801284005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4376.33;                                          
-- 1069                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8804255009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=14669.91;                                         
-- 1070                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8807884009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=7806.84;                                          
-- 1071                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8826055009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3644.54;                                          
-- 1072                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8829384002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6945.61;                                          
-- 1073                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8850185002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=14715.59;                                         
-- 1074                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8870624004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1499.11;                                          
-- 1075                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8872885001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4079.31;                                          
-- 1076                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8876264004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3260.65;                                          
-- 1077                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8882134007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1491.93;                                          
-- 1078                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8891404007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2430.81;                                          
-- 1079                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8894884006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2519.1;                                           
-- 1080                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8896494000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1130.11;                                          
-- 1081                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8902555009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=10533.3;                                          
-- 1082                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8919174002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1090.3;                                           
-- 1083                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8928225002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4364.35;                                          
-- 1084                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8939305008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4777.98;                                          
-- 1085                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8939734001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1571.58;                                          
-- 1086                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8942995006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5700;                                             
-- 1087                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8953904000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1153.49;                                          
-- 1088                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8954704001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4634.4;                                           
-- 1089                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8958554001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=929.29;                                           
-- 1090                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8964774004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=14572.76;                                         
-- 1091                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8966155006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=11395.26;                                         
-- 1092                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='8982754002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3671.3;                                           
-- 1093                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9009235009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=564.3;                                            
-- 1094                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9009385008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3575.98;                                          
-- 1095                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9017675001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3540.74;                                          
-- 1096                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9021714007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2021.25;                                          
-- 1097                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9021954009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4296.85;                                          
-- 1098                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9031315005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=10035.84;                                         
-- 1099                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9036445008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=10887.39;                                         
-- 1100                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9042025007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1673.35;                                          
-- 1101                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9046214006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1270.52;                                          
-- 1102                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9047535006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=7446.3;                                           
-- 1103                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9050724009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1149.56;                                          
-- 1104                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9051435003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6091.7;                                           
-- 1105                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9054124003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3955.47;                                          
-- 1106                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9061225003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2349.27;                                          
-- 1107                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9063005000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3376.19;                                          
-- 1108                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9074835008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2174.01;                                          
-- 1109                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9086294002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2612.79;                                          
-- 1110                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9087184006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1745.81;                                          
-- 1111                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9087484002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4003.98;                                          
-- 1112                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9088605010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3912.73;                                          
-- 1113                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9092304010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4462.9;                                           
-- 1114                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9092935007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=12399.36;                                         
-- 1115                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9093545010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4377.02;                                          
-- 1116                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9095074008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3964.55;                                          
-- 1117                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9097095003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=35133.73;                                         
-- 1118                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9118475010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2378.78;                                          
-- 1119                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9120274001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2109.69;                                          
-- 1120                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9122034003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3665.78;                                          
-- 1121                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9122705010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6926.53;                                          
-- 1122                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9124494005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=801.38;                                           
-- 1123                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9128014009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=471.76;                                           
-- 1124                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9128554007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=8501.3;                                           
-- 1125                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9128994010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1883.1;                                           
-- 1126                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9130445004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=17429.11;                                         
-- 1127                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9132375000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1043.4;                                           
-- 1128                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9154744009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1854.59;                                          
-- 1129                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9154954002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4058.02;                                          
-- 1130                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9167275005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4719.82;                                          
-- 1131                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9168814007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1220.54;                                          
-- 1132                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9174054000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=9157.42;                                          
-- 1133                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9182865003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2954.19;                                          
-- 1134                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9183205006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=10953.28;                                         
-- 1135                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9197285009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=19269;                                            
-- 1136                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9205984001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4631.12;                                          
-- 1137                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9209704006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1785.96;                                          
-- 1138                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9213595009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4962.77;                                          
-- 1139                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9220895003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4594.87;                                          
-- 1140                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9221864000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=593.69;                                           
-- 1141                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9223145007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=8593.62;                                          
-- 1142                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9228145006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1019.52;                                          
-- 1143                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9229485005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=17872.7;                                          
-- 1144                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9235554002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=10534;                                            
-- 1145                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9244615005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1351.62;                                          
-- 1146                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9245134009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=887.48;                                           
-- 1147                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9245574001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2542.74;                                          
-- 1148                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9255245007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5948.61;                                          
-- 1149                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9262525006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=10834.23;                                         
-- 1150                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9269314009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2869.27;                                          
-- 1151                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9281335003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5028.77;                                          
-- 1152                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9287335004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=9970.74;                                          
-- 1153                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9310465007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3591.68;                                          
-- 1154                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9316895008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=38651.98;                                         
-- 1155                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9320535008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4833.33;                                          
-- 1156                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9321584007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1607.71;                                          
-- 1157                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9321704006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1285.41;                                          
-- 1158                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9332734010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=931.05;                                           
-- 1159                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9340675001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1216.57;                                          
-- 1160                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9342554008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1142.35;                                          
-- 1161                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9343735001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1467.9;                                           
-- 1162                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9345105004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6890.53;                                          
-- 1163                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9353834004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2356.32;                                          
-- 1164                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9354084006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2280.26;                                          
-- 1165                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9354134008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3520.85;                                          
-- 1166                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9376195001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1503.94;                                          
-- 1167                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9376605004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1497.29;                                          
-- 1168                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9394764003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=783.52;                                           
-- 1169                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9422955006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3884.56;                                          
-- 1170                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9423105000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1276.34;                                          
-- 1171                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9436125005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1983.15;                                          
-- 1172                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9446065004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1648.64;                                          
-- 1173                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9450004004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=7581.07;                                          
-- 1174                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9450224000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4211.69;                                          
-- 1175                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9455945010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=13186.98;                                         
-- 1176                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9457685008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3331.75;                                          
-- 1177                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9461485010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=7033.36;                                          
-- 1178                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9483324002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2761.61;                                          
-- 1179                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9487615010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6789.32;                                          
-- 1180                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9489004001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2243.31;                                          
-- 1181                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9490795008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4194.31;                                          
-- 1182                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9494995006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1726.63;                                          
-- 1183                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9519405001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=7565.57;                                          
-- 1184                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9519565003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=10210.93;                                         
-- 1185                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9519814010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2175.35;                                          
-- 1186                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9523185010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=18324.32;                                         
-- 1187                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9534305004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3678.79;                                          
-- 1188                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9548975007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2762.17;                                          
-- 1189                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9559095009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5113.42;                                          
-- 1190                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9565195000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6057.79;                                          
-- 1191                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9580095003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2615.61;                                          
-- 1192                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9582704004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2973.43;                                          
-- 1193                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9582715003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1182.64;                                          
-- 1194                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9585675010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3476.02;                                          
-- 1195                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9591895002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2709.79;                                          
-- 1196                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9616475002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4698.92;                                          
-- 1197                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9622885003'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2928.23;                                          
-- 1198                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9657285000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=10512.2;                                          
-- 1199                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9658435010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1661.16;                                          
-- 1200                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9659244007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2744;                                             
-- 1201                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9659964000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2141.65;                                          
-- 1202                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9671124009'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2634.36;                                          
-- 1203                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9769185010'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=10065.86;                                         
-- 1204                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9801075002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=14989.17;                                         
-- 1205                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9855295004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6265.43;                                          
-- 1206                                                                                                  
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9893844008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200706','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1782.46;                                          
-- 1207                                                                                                  
                                                                                                                                                      
COMMIT;                                                                           
Select 'END of update statements' from dual;                                      
                                                                                                                                                      

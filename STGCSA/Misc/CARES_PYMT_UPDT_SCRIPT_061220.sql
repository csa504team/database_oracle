/* first statement will not find a row to update                             
   it is intended to verify finding statements that do not match a row              
   when manually reviewing the log fog file.   */                                   
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112' where LOANNMB='NONESUCH  ' 
  and PYMTPOSTPYMTDT=to_DATE('02-APR-20','dd-MON-yy')  AND PYMTPOSTPYMTTYP='Z' and PYMTPOSTREMITTEDAMT=-9999;    
/* all following statements should update 1 row */                                  
                                                                                       
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3652017007'                        
 and PYMTPOSTPYMTDT=to_DATE('20200612','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2302.99;                                          
-- 1                                                                                                     
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3661037004'                        
 and PYMTPOSTPYMTDT=to_DATE('20200612','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3044.51;                                          
-- 2                                                                                                     
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3800237006'                        
 and PYMTPOSTPYMTDT=to_DATE('20200612','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1839.52;                                          
-- 3                                                                                                     
                                                                                                                                                      
COMMIT;                                                                           
Select 'END of update statements' from dual;                                      
                                                                                                                                                      

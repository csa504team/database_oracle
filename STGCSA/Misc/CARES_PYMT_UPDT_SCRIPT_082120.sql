/* first statement will not find a row to update                             
   it is intended to verify finding statements that do not match a row              
   when manually reviewing the log fog file.   */                                   
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112' where LOANNMB='NONESUCH  ' 
  and PYMTPOSTPYMTDT=to_DATE('02-APR-20','dd-MON-yy')  AND PYMTPOSTPYMTTYP='Z' and PYMTPOSTREMITTEDAMT=-9999;    
/* all following statements should update 1 row */                                  
                                                                                       
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3495967005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200821','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5026.99;                                          
-- 1                                                                                                     
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3619306002'                        
 and PYMTPOSTPYMTDT=to_DATE('20200821','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=1189.8;                                           
-- 2                                                                                                     
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3673777000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200821','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=3042.6;                                           
-- 3                                                                                                     
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9653925005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200821','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=4219.81;                                          
-- 4                                                                                                     
                                                                                                                                                      
COMMIT;                                                                           
Select 'END of update statements' from dual;                                      
                                                                                                                                                      

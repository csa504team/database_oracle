/* first statement will not find a row to update                             
   it is intended to verify finding statements that do not match a row              
   when manually reviewing the log fog file.   */                                   
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112' where LOANNMB='NONESUCH  ' 
  and PYMTPOSTPYMTDT=to_DATE('02-APR-20','dd-MON-yy')  AND PYMTPOSTPYMTTYP='Z' and PYMTPOSTREMITTEDAMT=-9999;    
/* all following statements should update 1 row */                                  
                                                                                       
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3361657207'                        
 and PYMTPOSTPYMTDT=to_DATE('11/19/2020','mm/dd/yyyy')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=7699.44;                                      
-- 1                                                                                                     
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4138557007'                        
 and PYMTPOSTPYMTDT=to_DATE('11/19/2020','mm/dd/yyyy')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2866.34;                                      
-- 2                                                                                                     
                                                                                                                                                      
COMMIT;                                                                           
Select 'END of update statements' from dual;                                      
                                                                                                                                                      

-- stgcsa_merge_end_dt (last value)... 
--   1900=full, sysdate=in-sync as of now, 2900=in-sync whenever next run
insert into stgcsa.xover_status 
  (xover_last_event,
   STGCSA_INIT_dt,
   STGCSA_EXPORT_START_dt,
   CSA_MERGE_end_dt,  
   CSA_INIT_dt,
   CSA_EXPORT_START_dt,
   STGCSA_MERGE_end_dt)
 values('SM', 
   to_date('29000101','yyyymmdd'), to_date('29000101','yyyymmdd'), to_date('29000101','yyyymmdd'), 
   to_date('29000101','yyyymmdd'), to_date('29000101','yyyymmdd'), 
   --
   sysdate);
commit;

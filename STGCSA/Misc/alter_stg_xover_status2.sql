alter table stgcsa.xover_status add (
  stgcnt_pymt110rptarchvtbl number,
  csacnt_pymt110rptarchvtbl number);
update stgcsa.xover_status set
  stgcnt_pymt110rptarchvtbl=0, csacnt_pymt110rptarchvtbl=0;
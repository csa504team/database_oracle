/* first statement will not find a row to update                             
   it is intended to verify finding statements that do not match a row              
   when manually reviewing the log fog file.   */                                                           
UPDATE STGCSA.CORETRANSTBL set LOANNMB='NONESUCH'  ,STATIDCUR=23, CREATUSERID='U801545CSA',   
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=00;                                
/* all following statements should update 1 row */                                  
                                                                                       
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='1013187006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268548;                                                                                        
                                                                                                                                                      
-- 1                                                                                                     
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='1015146002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268139;                                                                                        
                                                                                                                                                      
-- 2                                                                                                     
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='1073777008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269880;                                                                                        
                                                                                                                                                      
-- 3                                                                                                     
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='1079887007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269663;                                                                                        
                                                                                                                                                      
-- 4                                                                                                     
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='1119187004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268416;                                                                                        
                                                                                                                                                      
-- 5                                                                                                     
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='1134347002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268360;                                                                                        
                                                                                                                                                      
-- 6                                                                                                     
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='1135387005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269104;                                                                                        
                                                                                                                                                      
-- 7                                                                                                     
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='1251187001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268880;                                                                                        
                                                                                                                                                      
-- 8                                                                                                     
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='1281567009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268898;                                                                                        
                                                                                                                                                      
-- 9                                                                                                     
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='1282107002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268574;                                                                                        
                                                                                                                                                      
-- 10                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='1393428210',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268845;                                                                                        
                                                                                                                                                      
-- 11                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='1427587008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268390;                                                                                        
                                                                                                                                                      
-- 12                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='1435948201',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268578;                                                                                        
                                                                                                                                                      
-- 13                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='1437348006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268680;                                                                                        
                                                                                                                                                      
-- 14                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='1485268207',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268939;                                                                                        
                                                                                                                                                      
-- 15                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='1521388010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269620;                                                                                        
                                                                                                                                                      
-- 16                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='1609196003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268850;                                                                                        
                                                                                                                                                      
-- 17                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='1654467007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268027;                                                                                        
                                                                                                                                                      
-- 18                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='1669177006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268722;                                                                                        
                                                                                                                                                      
-- 19                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='1700277000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268582;                                                                                        
                                                                                                                                                      
-- 20                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='1702747003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268959;                                                                                        
                                                                                                                                                      
-- 21                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='1798156000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269996;                                                                                        
                                                                                                                                                      
-- 22                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='1852427007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269764;                                                                                        
                                                                                                                                                      
-- 23                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='1858877003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268083;                                                                                        
                                                                                                                                                      
-- 24                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='1895868202',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269492;                                                                                        
                                                                                                                                                      
-- 25                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='1917817008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268613;                                                                                        
                                                                                                                                                      
-- 26                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='1988157008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269850;                                                                                        
                                                                                                                                                      
-- 27                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2043756010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268668;                                                                                        
                                                                                                                                                      
-- 28                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2051057008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268238;                                                                                        
                                                                                                                                                      
-- 29                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2064396008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268105;                                                                                        
                                                                                                                                                      
-- 30                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2066437802',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268556;                                                                                        
                                                                                                                                                      
-- 31                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2073126010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269084;                                                                                        
                                                                                                                                                      
-- 32                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2096126002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268466;                                                                                        
                                                                                                                                                      
-- 33                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2141757010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269488;                                                                                        
                                                                                                                                                      
-- 34                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2166747003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268409;                                                                                        
                                                                                                                                                      
-- 35                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2171376008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269112;                                                                                        
                                                                                                                                                      
-- 36                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2186737008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=270002;                                                                                        
                                                                                                                                                      
-- 37                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2190026003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269241;                                                                                        
                                                                                                                                                      
-- 38                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2196417010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269064;                                                                                        
                                                                                                                                                      
-- 39                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2200147103',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269754;                                                                                        
                                                                                                                                                      
-- 40                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2200387007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269768;                                                                                        
                                                                                                                                                      
-- 41                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2209346006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269778;                                                                                        
                                                                                                                                                      
-- 42                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2227157803',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269842;                                                                                        
                                                                                                                                                      
-- 43                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2236287101',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269264;                                                                                        
                                                                                                                                                      
-- 44                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2242156010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269444;                                                                                        
                                                                                                                                                      
-- 45                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2258327003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269392;                                                                                        
                                                                                                                                                      
-- 46                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2270347110',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269446;                                                                                        
                                                                                                                                                      
-- 47                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2296997006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=270021;                                                                                        
                                                                                                                                                      
-- 48                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2300447009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268664;                                                                                        
                                                                                                                                                      
-- 49                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2306876003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269956;                                                                                        
                                                                                                                                                      
-- 50                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2316317010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268276;                                                                                        
                                                                                                                                                      
-- 51                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2328147908',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268746;                                                                                        
                                                                                                                                                      
-- 52                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2336366004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269296;                                                                                        
                                                                                                                                                      
-- 53                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2342856005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268014;                                                                                        
                                                                                                                                                      
-- 54                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2350387002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269948;                                                                                        
                                                                                                                                                      
-- 55                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2350646001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268765;                                                                                        
                                                                                                                                                      
-- 56                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2361247001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268915;                                                                                        
                                                                                                                                                      
-- 57                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2365757009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=270014;                                                                                        
                                                                                                                                                      
-- 58                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2365797010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269618;                                                                                        
                                                                                                                                                      
-- 59                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2389967007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269364;                                                                                        
                                                                                                                                                      
-- 60                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2391856001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268486;                                                                                        
                                                                                                                                                      
-- 61                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2404347008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269418;                                                                                        
                                                                                                                                                      
-- 62                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2426606008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269001;                                                                                        
                                                                                                                                                      
-- 63                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2439186001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268464;                                                                                        
                                                                                                                                                      
-- 64                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2440898209',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269683;                                                                                        
                                                                                                                                                      
-- 65                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2442116003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269120;                                                                                        
                                                                                                                                                      
-- 66                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2448397003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268309;                                                                                        
                                                                                                                                                      
-- 67                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2458116008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268584;                                                                                        
                                                                                                                                                      
-- 68                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2492517010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268568;                                                                                        
                                                                                                                                                      
-- 69                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2494897001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268937;                                                                                        
                                                                                                                                                      
-- 70                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2502446000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269862;                                                                                        
                                                                                                                                                      
-- 71                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2509177001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268884;                                                                                        
                                                                                                                                                      
-- 72                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2522237000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268356;                                                                                        
                                                                                                                                                      
-- 73                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2533366003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268977;                                                                                        
                                                                                                                                                      
-- 74                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2538766004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268537;                                                                                        
                                                                                                                                                      
-- 75                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2552917004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268682;                                                                                        
                                                                                                                                                      
-- 76                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2574546005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269804;                                                                                        
                                                                                                                                                      
-- 77                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2597827005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268586;                                                                                        
                                                                                                                                                      
-- 78                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2599847004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268955;                                                                                        
                                                                                                                                                      
-- 79                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2600196001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269278;                                                                                        
                                                                                                                                                      
-- 80                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2607457006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269229;                                                                                        
                                                                                                                                                      
-- 81                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2607576000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268053;                                                                                        
                                                                                                                                                      
-- 82                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2609807008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268947;                                                                                        
                                                                                                                                                      
-- 83                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2619647001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269832;                                                                                        
                                                                                                                                                      
-- 84                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2622546000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269016;                                                                                        
                                                                                                                                                      
-- 85                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2624357002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269920;                                                                                        
                                                                                                                                                      
-- 86                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2627617003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269094;                                                                                        
                                                                                                                                                      
-- 87                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2629577008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269661;                                                                                        
                                                                                                                                                      
-- 88                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2635517008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268173;                                                                                        
                                                                                                                                                      
-- 89                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2638166009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268913;                                                                                        
                                                                                                                                                      
-- 90                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2648776008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269750;                                                                                        
                                                                                                                                                      
-- 91                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2655477006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269766;                                                                                        
                                                                                                                                                      
-- 92                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2658187008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268205;                                                                                        
                                                                                                                                                      
-- 93                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2658457006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268348;                                                                                        
                                                                                                                                                      
-- 94                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2667987003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268496;                                                                                        
                                                                                                                                                      
-- 95                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2676807008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268558;                                                                                        
                                                                                                                                                      
-- 96                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2681936010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268364;                                                                                        
                                                                                                                                                      
-- 97                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2694407010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269645;                                                                                        
                                                                                                                                                      
-- 98                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2694608203',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269070;                                                                                        
                                                                                                                                                      
-- 99                                                                                                    
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2702297004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269960;                                                                                        
                                                                                                                                                      
-- 100                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2702436002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268398;                                                                                        
                                                                                                                                                      
-- 101                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2702797001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268540;                                                                                        
                                                                                                                                                      
-- 102                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2709166007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269378;                                                                                        
                                                                                                                                                      
-- 103                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2715477009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268458;                                                                                        
                                                                                                                                                      
-- 104                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2718137007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269651;                                                                                        
                                                                                                                                                      
-- 105                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2727176003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269908;                                                                                        
                                                                                                                                                      
-- 106                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2729896000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268251;                                                                                        
                                                                                                                                                      
-- 107                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2736968107',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269165;                                                                                        
                                                                                                                                                      
-- 108                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2743996002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268724;                                                                                        
                                                                                                                                                      
-- 109                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2767947010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268542;                                                                                        
                                                                                                                                                      
-- 110                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2768926010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268694;                                                                                        
                                                                                                                                                      
-- 111                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2770376010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268121;                                                                                        
                                                                                                                                                      
-- 112                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2770626002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269896;                                                                                        
                                                                                                                                                      
-- 113                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2788047010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268855;                                                                                        
                                                                                                                                                      
-- 114                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2794657001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268979;                                                                                        
                                                                                                                                                      
-- 115                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2808386009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268648;                                                                                        
                                                                                                                                                      
-- 116                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2821337006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269900;                                                                                        
                                                                                                                                                      
-- 117                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2826627009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269052;                                                                                        
                                                                                                                                                      
-- 118                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2828007004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269382;                                                                                        
                                                                                                                                                      
-- 119                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2829197006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268622;                                                                                        
                                                                                                                                                      
-- 120                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2833167000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269707;                                                                                        
                                                                                                                                                      
-- 121                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2841077008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269733;                                                                                        
                                                                                                                                                      
-- 122                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2842977009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269348;                                                                                        
                                                                                                                                                      
-- 123                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2843147009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269022;                                                                                        
                                                                                                                                                      
-- 124                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2850287003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269922;                                                                                        
                                                                                                                                                      
-- 125                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2853787006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268476;                                                                                        
                                                                                                                                                      
-- 126                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2863386001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268079;                                                                                        
                                                                                                                                                      
-- 127                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2874636010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269918;                                                                                        
                                                                                                                                                      
-- 128                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2876817005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269122;                                                                                        
                                                                                                                                                      
-- 129                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2881007406',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269438;                                                                                        
                                                                                                                                                      
-- 130                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2882457000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268628;                                                                                        
                                                                                                                                                      
-- 131                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2886326008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269183;                                                                                        
                                                                                                                                                      
-- 132                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2888407008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269157;                                                                                        
                                                                                                                                                      
-- 133                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2888947006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268492;                                                                                        
                                                                                                                                                      
-- 134                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2889487001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268878;                                                                                        
                                                                                                                                                      
-- 135                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2890847010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268777;                                                                                        
                                                                                                                                                      
-- 136                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2891037005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268008;                                                                                        
                                                                                                                                                      
-- 137                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2893246006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269562;                                                                                        
                                                                                                                                                      
-- 138                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2896287009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268763;                                                                                        
                                                                                                                                                      
-- 139                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2901957004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268296;                                                                                        
                                                                                                                                                      
-- 140                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2922997004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268480;                                                                                        
                                                                                                                                                      
-- 141                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2933716000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269480;                                                                                        
                                                                                                                                                      
-- 142                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2934857005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269727;                                                                                        
                                                                                                                                                      
-- 143                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2942297003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268147;                                                                                        
                                                                                                                                                      
-- 144                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2943177004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268790;                                                                                        
                                                                                                                                                      
-- 145                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2943446006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268876;                                                                                        
                                                                                                                                                      
-- 146                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2948427006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268869;                                                                                        
                                                                                                                                                      
-- 147                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2953586006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269344;                                                                                        
                                                                                                                                                      
-- 148                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2957337005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269368;                                                                                        
                                                                                                                                                      
-- 149                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2959286000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269984;                                                                                        
                                                                                                                                                      
-- 150                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2960237000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268757;                                                                                        
                                                                                                                                                      
-- 151                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2961106002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268454;                                                                                        
                                                                                                                                                      
-- 152                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2968266007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268728;                                                                                        
                                                                                                                                                      
-- 153                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2973407002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268778;                                                                                        
                                                                                                                                                      
-- 154                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2977216005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269424;                                                                                        
                                                                                                                                                      
-- 155                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2983126009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=270004;                                                                                        
                                                                                                                                                      
-- 156                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2987037010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269456;                                                                                        
                                                                                                                                                      
-- 157                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2987138110',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268406;                                                                                        
                                                                                                                                                      
-- 158                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2990596008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269628;                                                                                        
                                                                                                                                                      
-- 159                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='2991597006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268867;                                                                                        
                                                                                                                                                      
-- 160                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3000986003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268321;                                                                                        
                                                                                                                                                      
-- 161                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3001567004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268806;                                                                                        
                                                                                                                                                      
-- 162                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3001617006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269005;                                                                                        
                                                                                                                                                      
-- 163                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3003267001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269276;                                                                                        
                                                                                                                                                      
-- 164                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3009736005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268051;                                                                                        
                                                                                                                                                      
-- 165                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3020006010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268672;                                                                                        
                                                                                                                                                      
-- 166                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3024877006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268249;                                                                                        
                                                                                                                                                      
-- 167                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3033347002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268422;                                                                                        
                                                                                                                                                      
-- 168                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3042328005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268323;                                                                                        
                                                                                                                                                      
-- 169                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3046636009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268800;                                                                                        
                                                                                                                                                      
-- 170                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3047297001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268786;                                                                                        
                                                                                                                                                      
-- 171                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3068106004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269221;                                                                                        
                                                                                                                                                      
-- 172                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3087247002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269058;                                                                                        
                                                                                                                                                      
-- 173                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3091757005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269169;                                                                                        
                                                                                                                                                      
-- 174                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3094167000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268061;                                                                                        
                                                                                                                                                      
-- 175                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3097777001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269932;                                                                                        
                                                                                                                                                      
-- 176                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3104177008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269670;                                                                                        
                                                                                                                                                      
-- 177                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3104307010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269671;                                                                                        
                                                                                                                                                      
-- 178                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3119346007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269177;                                                                                        
                                                                                                                                                      
-- 179                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3120906006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268993;                                                                                        
                                                                                                                                                      
-- 180                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3121897003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269701;                                                                                        
                                                                                                                                                      
-- 181                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3124957003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268245;                                                                                        
                                                                                                                                                      
-- 182                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3140397208',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269560;                                                                                        
                                                                                                                                                      
-- 183                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3140427008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268932;                                                                                        
                                                                                                                                                      
-- 184                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3140667206',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268510;                                                                                        
                                                                                                                                                      
-- 185                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3142717005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269201;                                                                                        
                                                                                                                                                      
-- 186                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3161286006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268494;                                                                                        
                                                                                                                                                      
-- 187                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3163446010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268753;                                                                                        
                                                                                                                                                      
-- 188                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3182846007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268821;                                                                                        
                                                                                                                                                      
-- 189                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3188267007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269249;                                                                                        
                                                                                                                                                      
-- 190                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3203687003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268280;                                                                                        
                                                                                                                                                      
-- 191                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3206926002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268262;                                                                                        
                                                                                                                                                      
-- 192                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3211666001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268592;                                                                                        
                                                                                                                                                      
-- 193                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3220557001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269432;                                                                                        
                                                                                                                                                      
-- 194                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3222566001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268961;                                                                                        
                                                                                                                                                      
-- 195                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3239256009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268825;                                                                                        
                                                                                                                                                      
-- 196                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3246277009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269762;                                                                                        
                                                                                                                                                      
-- 197                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3251167408',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269978;                                                                                        
                                                                                                                                                      
-- 198                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3253606003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269888;                                                                                        
                                                                                                                                                      
-- 199                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3257457206',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268161;                                                                                        
                                                                                                                                                      
-- 200                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3258907005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269721;                                                                                        
                                                                                                                                                      
-- 201                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3263046005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268055;                                                                                        
                                                                                                                                                      
-- 202                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3265256002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268374;                                                                                        
                                                                                                                                                      
-- 203                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3272478010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269167;                                                                                        
                                                                                                                                                      
-- 204                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3276217003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269223;                                                                                        
                                                                                                                                                      
-- 205                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3280567006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269944;                                                                                        
                                                                                                                                                      
-- 206                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3282816006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268656;                                                                                        
                                                                                                                                                      
-- 207                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3283876004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268957;                                                                                        
                                                                                                                                                      
-- 208                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3287467003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268755;                                                                                        
                                                                                                                                                      
-- 209                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3299217006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269655;                                                                                        
                                                                                                                                                      
-- 210                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3302967805',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=270010;                                                                                        
                                                                                                                                                      
-- 211                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3306417202',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268114;                                                                                        
                                                                                                                                                      
-- 212                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3308387210',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268288;                                                                                        
                                                                                                                                                      
-- 213                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3308687010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269460;                                                                                        
                                                                                                                                                      
-- 214                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3311397203',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269940;                                                                                        
                                                                                                                                                      
-- 215                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3330526008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268442;                                                                                        
                                                                                                                                                      
-- 216                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3337637005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268865;                                                                                        
                                                                                                                                                      
-- 217                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3339466010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269719;                                                                                        
                                                                                                                                                      
-- 218                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3341057000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269144;                                                                                        
                                                                                                                                                      
-- 219                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3343477001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269040;                                                                                        
                                                                                                                                                      
-- 220                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3350336007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268093;                                                                                        
                                                                                                                                                      
-- 221                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3355935002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=270000;                                                                                        
                                                                                                                                                      
-- 222                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3359007005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269808;                                                                                        
                                                                                                                                                      
-- 223                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3359977003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268325;                                                                                        
                                                                                                                                                      
-- 224                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3381517208',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268518;                                                                                        
                                                                                                                                                      
-- 225                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3386007000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269412;                                                                                        
                                                                                                                                                      
-- 226                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3389176004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269211;                                                                                        
                                                                                                                                                      
-- 227                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3389296005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269098;                                                                                        
                                                                                                                                                      
-- 228                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3392147003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268294;                                                                                        
                                                                                                                                                      
-- 229                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3399577202',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269092;                                                                                        
                                                                                                                                                      
-- 230                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3404866009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268456;                                                                                        
                                                                                                                                                      
-- 231                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3417337009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268873;                                                                                        
                                                                                                                                                      
-- 232                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3418606002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269810;                                                                                        
                                                                                                                                                      
-- 233                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3418616005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269578;                                                                                        
                                                                                                                                                      
-- 234                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3421086002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268490;                                                                                        
                                                                                                                                                      
-- 235                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3424885005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268346;                                                                                        
                                                                                                                                                      
-- 236                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3447647205',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268839;                                                                                        
                                                                                                                                                      
-- 237                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3474965003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269272;                                                                                        
                                                                                                                                                      
-- 238                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3476617010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268535;                                                                                        
                                                                                                                                                      
-- 239                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3484697001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269616;                                                                                        
                                                                                                                                                      
-- 240                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3485757204',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268436;                                                                                        
                                                                                                                                                      
-- 241                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3488535004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268444;                                                                                        
                                                                                                                                                      
-- 242                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3491237001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269100;                                                                                        
                                                                                                                                                      
-- 243                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3494277008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269308;                                                                                        
                                                                                                                                                      
-- 244                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3500557006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269717;                                                                                        
                                                                                                                                                      
-- 245                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3506397007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268474;                                                                                        
                                                                                                                                                      
-- 246                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3511897009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269159;                                                                                        
                                                                                                                                                      
-- 247                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3516087001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269786;                                                                                        
                                                                                                                                                      
-- 248                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3519526001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269606;                                                                                        
                                                                                                                                                      
-- 249                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3520667001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268049;                                                                                        
                                                                                                                                                      
-- 250                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3521417009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269434;                                                                                        
                                                                                                                                                      
-- 251                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3521477005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269798;                                                                                        
                                                                                                                                                      
-- 252                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3526147010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268896;                                                                                        
                                                                                                                                                      
-- 253                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3526537009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268572;                                                                                        
                                                                                                                                                      
-- 254                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3526737010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269554;                                                                                        
                                                                                                                                                      
-- 255                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3528777004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268334;                                                                                        
                                                                                                                                                      
-- 256                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3540047000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269326;                                                                                        
                                                                                                                                                      
-- 257                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3541907000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268953;                                                                                        
                                                                                                                                                      
-- 258                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3550827002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269282;                                                                                        
                                                                                                                                                      
-- 259                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3550997007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269800;                                                                                        
                                                                                                                                                      
-- 260                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3556297010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269140;                                                                                        
                                                                                                                                                      
-- 261                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3556827003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268175;                                                                                        
                                                                                                                                                      
-- 262                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3560647000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269747;                                                                                        
                                                                                                                                                      
-- 263                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3561427006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268933;                                                                                        
                                                                                                                                                      
-- 264                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3564327006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268642;                                                                                        
                                                                                                                                                      
-- 265                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3565398003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268057;                                                                                        
                                                                                                                                                      
-- 266                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3568127002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269464;                                                                                        
                                                                                                                                                      
-- 267                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3570377006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268292;                                                                                        
                                                                                                                                                      
-- 268                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3572677006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269729;                                                                                        
                                                                                                                                                      
-- 269                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3574597010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269032;                                                                                        
                                                                                                                                                      
-- 270                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3575085009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268829;                                                                                        
                                                                                                                                                      
-- 271                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3576217005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268274;                                                                                        
                                                                                                                                                      
-- 272                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3576477002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269966;                                                                                        
                                                                                                                                                      
-- 273                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3578597007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269106;                                                                                        
                                                                                                                                                      
-- 274                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3580887010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268841;                                                                                        
                                                                                                                                                      
-- 275                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3582495001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269030;                                                                                        
                                                                                                                                                      
-- 276                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3590766006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269830;                                                                                        
                                                                                                                                                      
-- 277                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3593747002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269498;                                                                                        
                                                                                                                                                      
-- 278                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3594167007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269564;                                                                                        
                                                                                                                                                      
-- 279                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3594217009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269394;                                                                                        
                                                                                                                                                      
-- 280                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3599947004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269342;                                                                                        
                                                                                                                                                      
-- 281                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3601415006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269916;                                                                                        
                                                                                                                                                      
-- 282                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3603405007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268229;                                                                                        
                                                                                                                                                      
-- 283                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3603447000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268975;                                                                                        
                                                                                                                                                      
-- 284                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3605077000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269354;                                                                                        
                                                                                                                                                      
-- 285                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3608037005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269179;                                                                                        
                                                                                                                                                      
-- 286                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3608997000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268603;                                                                                        
                                                                                                                                                      
-- 287                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3609546000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269868;                                                                                        
                                                                                                                                                      
-- 288                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3612547008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268732;                                                                                        
                                                                                                                                                      
-- 289                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3612657006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269792;                                                                                        
                                                                                                                                                      
-- 290                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3617474000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268853;                                                                                        
                                                                                                                                                      
-- 291                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3625437009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268378;                                                                                        
                                                                                                                                                      
-- 292                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3628335006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268560;                                                                                        
                                                                                                                                                      
-- 293                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3639497006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268676;                                                                                        
                                                                                                                                                      
-- 294                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3639956006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268025;                                                                                        
                                                                                                                                                      
-- 295                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3645857003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268670;                                                                                        
                                                                                                                                                      
-- 296                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3646016004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269187;                                                                                        
                                                                                                                                                      
-- 297                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3648596007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268983;                                                                                        
                                                                                                                                                      
-- 298                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3657657010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269573;                                                                                        
                                                                                                                                                      
-- 299                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3659696008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269846;                                                                                        
                                                                                                                                                      
-- 300                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3660414010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269635;                                                                                        
                                                                                                                                                      
-- 301                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3663245009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268123;                                                                                        
                                                                                                                                                      
-- 302                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3666577001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268191;                                                                                        
                                                                                                                                                      
-- 303                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3671787010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268508;                                                                                        
                                                                                                                                                      
-- 304                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3674917007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268109;                                                                                        
                                                                                                                                                      
-- 305                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3674957008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268111;                                                                                        
                                                                                                                                                      
-- 306                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3680557002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269540;                                                                                        
                                                                                                                                                      
-- 307                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3680886000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269102;                                                                                        
                                                                                                                                                      
-- 308                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3680997005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269758;                                                                                        
                                                                                                                                                      
-- 309                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3681697000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269816;                                                                                        
                                                                                                                                                      
-- 310                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3685277000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268440;                                                                                        
                                                                                                                                                      
-- 311                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3685877003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268943;                                                                                        
                                                                                                                                                      
-- 312                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3686317001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268967;                                                                                        
                                                                                                                                                      
-- 313                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3697776009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269588;                                                                                        
                                                                                                                                                      
-- 314                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3701865009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269677;                                                                                        
                                                                                                                                                      
-- 315                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3705277009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269924;                                                                                        
                                                                                                                                                      
-- 316                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3716195001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269972;                                                                                        
                                                                                                                                                      
-- 317                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3726107003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269876;                                                                                        
                                                                                                                                                      
-- 318                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3735797002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269231;                                                                                        
                                                                                                                                                      
-- 319                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3738627003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268452;                                                                                        
                                                                                                                                                      
-- 320                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3740037002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269639;                                                                                        
                                                                                                                                                      
-- 321                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3740457010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268143;                                                                                        
                                                                                                                                                      
-- 322                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3744387010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268780;                                                                                        
                                                                                                                                                      
-- 323                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3750645009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269314;                                                                                        
                                                                                                                                                      
-- 324                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3751937007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269968;                                                                                        
                                                                                                                                                      
-- 325                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3752907000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269152;                                                                                        
                                                                                                                                                      
-- 326                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3754767010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269128;                                                                                        
                                                                                                                                                      
-- 327                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3755587006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268125;                                                                                        
                                                                                                                                                      
-- 328                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3760045010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268708;                                                                                        
                                                                                                                                                      
-- 329                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3764808205',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268660;                                                                                        
                                                                                                                                                      
-- 330                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3765157010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269964;                                                                                        
                                                                                                                                                      
-- 331                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3767467002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268169;                                                                                        
                                                                                                                                                      
-- 332                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3768337000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269998;                                                                                        
                                                                                                                                                      
-- 333                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3771507004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268863;                                                                                        
                                                                                                                                                      
-- 334                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3773187008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269062;                                                                                        
                                                                                                                                                      
-- 335                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3776017009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269318;                                                                                        
                                                                                                                                                      
-- 336                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3780347006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269072;                                                                                        
                                                                                                                                                      
-- 337                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3782617008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268802;                                                                                        
                                                                                                                                                      
-- 338                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3784337000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268270;                                                                                        
                                                                                                                                                      
-- 339                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3787597003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268412;                                                                                        
                                                                                                                                                      
-- 340                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3787887007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268811;                                                                                        
                                                                                                                                                      
-- 341                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3791317003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269108;                                                                                        
                                                                                                                                                      
-- 342                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3795417006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269500;                                                                                        
                                                                                                                                                      
-- 343                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3798657003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268301;                                                                                        
                                                                                                                                                      
-- 344                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3801847003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268095;                                                                                        
                                                                                                                                                      
-- 345                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3805277006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269675;                                                                                        
                                                                                                                                                      
-- 346                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3805397007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269468;                                                                                        
                                                                                                                                                      
-- 347                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3806067004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269316;                                                                                        
                                                                                                                                                      
-- 348                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3811597004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269770;                                                                                        
                                                                                                                                                      
-- 349                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3813057010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268235;                                                                                        
                                                                                                                                                      
-- 350                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3813777003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268350;                                                                                        
                                                                                                                                                      
-- 351                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3820617010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269912;                                                                                        
                                                                                                                                                      
-- 352                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3822087010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269011;                                                                                        
                                                                                                                                                      
-- 353                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3824547010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268213;                                                                                        
                                                                                                                                                      
-- 354                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3826067001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268317;                                                                                        
                                                                                                                                                      
-- 355                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3826857002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269689;                                                                                        
                                                                                                                                                      
-- 356                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3828267006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269262;                                                                                        
                                                                                                                                                      
-- 357                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3828837000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269506;                                                                                        
                                                                                                                                                      
-- 358                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3828927003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269878;                                                                                        
                                                                                                                                                      
-- 359                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3832057005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268784;                                                                                        
                                                                                                                                                      
-- 360                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3832517001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269679;                                                                                        
                                                                                                                                                      
-- 361                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3837277000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269584;                                                                                        
                                                                                                                                                      
-- 362                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3837587010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268354;                                                                                        
                                                                                                                                                      
-- 363                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3844787009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269410;                                                                                        
                                                                                                                                                      
-- 364                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3845227007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=270008;                                                                                        
                                                                                                                                                      
-- 365                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3847837006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269532;                                                                                        
                                                                                                                                                      
-- 366                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3849997001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268081;                                                                                        
                                                                                                                                                      
-- 367                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3853885003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269542;                                                                                        
                                                                                                                                                      
-- 368                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3854157001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269332;                                                                                        
                                                                                                                                                      
-- 369                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3856694004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269426;                                                                                        
                                                                                                                                                      
-- 370                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3861687005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268910;                                                                                        
                                                                                                                                                      
-- 371                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3866087001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268227;                                                                                        
                                                                                                                                                      
-- 372                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3868187000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268358;                                                                                        
                                                                                                                                                      
-- 373                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3875187009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269864;                                                                                        
                                                                                                                                                      
-- 374                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3877737001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268921;                                                                                        
                                                                                                                                                      
-- 375                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3880487010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268258;                                                                                        
                                                                                                                                                      
-- 376                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3885787005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268684;                                                                                        
                                                                                                                                                      
-- 377                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3891677003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268882;                                                                                        
                                                                                                                                                      
-- 378                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3892107009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268151;                                                                                        
                                                                                                                                                      
-- 379                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3893367008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269574;                                                                                        
                                                                                                                                                      
-- 380                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3896787000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268484;                                                                                        
                                                                                                                                                      
-- 381                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3897197002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269420;                                                                                        
                                                                                                                                                      
-- 382                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3902597010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269604;                                                                                        
                                                                                                                                                      
-- 383                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3902687002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268236;                                                                                        
                                                                                                                                                      
-- 384                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3909777008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269142;                                                                                        
                                                                                                                                                      
-- 385                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3918177007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269612;                                                                                        
                                                                                                                                                      
-- 386                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3918937010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269235;                                                                                        
                                                                                                                                                      
-- 387                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3928575010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268650;                                                                                        
                                                                                                                                                      
-- 388                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3934545008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268917;                                                                                        
                                                                                                                                                      
-- 389                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3935367007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268201;                                                                                        
                                                                                                                                                      
-- 390                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3935857001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269328;                                                                                        
                                                                                                                                                      
-- 391                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3937957000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269346;                                                                                        
                                                                                                                                                      
-- 392                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3942977000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268430;                                                                                        
                                                                                                                                                      
-- 393                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3943037002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268299;                                                                                        
                                                                                                                                                      
-- 394                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3943297010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269858;                                                                                        
                                                                                                                                                      
-- 395                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3944687000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269245;                                                                                        
                                                                                                                                                      
-- 396                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3945517008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268386;                                                                                        
                                                                                                                                                      
-- 397                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3948397004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269824;                                                                                        
                                                                                                                                                      
-- 398                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3950057006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268514;                                                                                        
                                                                                                                                                      
-- 399                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3952177000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268159;                                                                                        
                                                                                                                                                      
-- 400                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3956477004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268570;                                                                                        
                                                                                                                                                      
-- 401                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3957237004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268788;                                                                                        
                                                                                                                                                      
-- 402                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3958115002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268767;                                                                                        
                                                                                                                                                      
-- 403                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3962997003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268388;                                                                                        
                                                                                                                                                      
-- 404                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3965247007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268630;                                                                                        
                                                                                                                                                      
-- 405                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3969127003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268332;                                                                                        
                                                                                                                                                      
-- 406                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3969507010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268177;                                                                                        
                                                                                                                                                      
-- 407                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3970607002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268438;                                                                                        
                                                                                                                                                      
-- 408                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3971827000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269522;                                                                                        
                                                                                                                                                      
-- 409                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3972747002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269362;                                                                                        
                                                                                                                                                      
-- 410                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3985544001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268815;                                                                                        
                                                                                                                                                      
-- 411                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3986387808',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268223;                                                                                        
                                                                                                                                                      
-- 412                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3988627006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268141;                                                                                        
                                                                                                                                                      
-- 413                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3991005006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269474;                                                                                        
                                                                                                                                                      
-- 414                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3991937006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268742;                                                                                        
                                                                                                                                                      
-- 415                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3992237010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269288;                                                                                        
                                                                                                                                                      
-- 416                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3992277000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268904;                                                                                        
                                                                                                                                                      
-- 417                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3996677010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268981;                                                                                        
                                                                                                                                                      
-- 418                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='3999307010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268808;                                                                                        
                                                                                                                                                      
-- 419                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4001437005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268394;                                                                                        
                                                                                                                                                      
-- 420                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4002047008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269866;                                                                                        
                                                                                                                                                      
-- 421                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4006215005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269715;                                                                                        
                                                                                                                                                      
-- 422                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4006517004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268342;                                                                                        
                                                                                                                                                      
-- 423                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4012647004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268253;                                                                                        
                                                                                                                                                      
-- 424                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4013037000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268835;                                                                                        
                                                                                                                                                      
-- 425                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4013217006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269814;                                                                                        
                                                                                                                                                      
-- 426                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4015817002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269548;                                                                                        
                                                                                                                                                      
-- 427                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4017627008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269928;                                                                                        
                                                                                                                                                      
-- 428                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4018565002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269828;                                                                                        
                                                                                                                                                      
-- 429                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4020177005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268087;                                                                                        
                                                                                                                                                      
-- 430                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4021987003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268370;                                                                                        
                                                                                                                                                      
-- 431                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4025577006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268837;                                                                                        
                                                                                                                                                      
-- 432                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4027597005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268704;                                                                                        
                                                                                                                                                      
-- 433                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4029487000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268908;                                                                                        
                                                                                                                                                      
-- 434                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4030627002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268989;                                                                                        
                                                                                                                                                      
-- 435                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4030877007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269044;                                                                                        
                                                                                                                                                      
-- 436                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4033507007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268091;                                                                                        
                                                                                                                                                      
-- 437                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4034657008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269050;                                                                                        
                                                                                                                                                      
-- 438                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4034757003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268004;                                                                                        
                                                                                                                                                      
-- 439                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4040695002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268077;                                                                                        
                                                                                                                                                      
-- 440                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4043147010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269086;                                                                                        
                                                                                                                                                      
-- 441                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4045165006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268290;                                                                                        
                                                                                                                                                      
-- 442                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4046637010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269854;                                                                                        
                                                                                                                                                      
-- 443                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4050217005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268284;                                                                                        
                                                                                                                                                      
-- 444                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4052827004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269126;                                                                                        
                                                                                                                                                      
-- 445                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4053437007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268550;                                                                                        
                                                                                                                                                      
-- 446                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4055167002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269508;                                                                                        
                                                                                                                                                      
-- 447                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4055667010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269243;                                                                                        
                                                                                                                                                      
-- 448                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4057397005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269080;                                                                                        
                                                                                                                                                      
-- 449                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4057437004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268021;                                                                                        
                                                                                                                                                      
-- 450                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4062597000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268951;                                                                                        
                                                                                                                                                      
-- 451                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4066427003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268740;                                                                                        
                                                                                                                                                      
-- 452                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4068317009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268590;                                                                                        
                                                                                                                                                      
-- 453                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4069397002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268468;                                                                                        
                                                                                                                                                      
-- 454                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4070437009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269872;                                                                                        
                                                                                                                                                      
-- 455                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4074757008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269370;                                                                                        
                                                                                                                                                      
-- 456                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4077835000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269665;                                                                                        
                                                                                                                                                      
-- 457                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4077837003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269254;                                                                                        
                                                                                                                                                      
-- 458                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4081297010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268414;                                                                                        
                                                                                                                                                      
-- 459                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4082807805',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268472;                                                                                        
                                                                                                                                                      
-- 460                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4084757001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268063;                                                                                        
                                                                                                                                                      
-- 461                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4085627010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268995;                                                                                        
                                                                                                                                                      
-- 462                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4087587004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268382;                                                                                        
                                                                                                                                                      
-- 463                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4090897004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269173;                                                                                        
                                                                                                                                                      
-- 464                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4092597001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268446;                                                                                        
                                                                                                                                                      
-- 465                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4094457009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268482;                                                                                        
                                                                                                                                                      
-- 466                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4097887004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268260;                                                                                        
                                                                                                                                                      
-- 467                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4098137004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268031;                                                                                        
                                                                                                                                                      
-- 468                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4099517002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269195;                                                                                        
                                                                                                                                                      
-- 469                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4099727801',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269358;                                                                                        
                                                                                                                                                      
-- 470                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4101767007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269484;                                                                                        
                                                                                                                                                      
-- 471                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4103747005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269637;                                                                                        
                                                                                                                                                      
-- 472                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4106577008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268751;                                                                                        
                                                                                                                                                      
-- 473                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4107577010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268115;                                                                                        
                                                                                                                                                      
-- 474                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4107677005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269580;                                                                                        
                                                                                                                                                      
-- 475                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4107817010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268153;                                                                                        
                                                                                                                                                      
-- 476                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4110757006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268662;                                                                                        
                                                                                                                                                      
-- 477                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4112577004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268185;                                                                                        
                                                                                                                                                      
-- 478                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4113017002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268197;                                                                                        
                                                                                                                                                      
-- 479                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4113567003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268678;                                                                                        
                                                                                                                                                      
-- 480                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4113587009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269874;                                                                                        
                                                                                                                                                      
-- 481                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4116327007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269695;                                                                                        
                                                                                                                                                      
-- 482                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4122477002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269470;                                                                                        
                                                                                                                                                      
-- 483                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4125307003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269970;                                                                                        
                                                                                                                                                      
-- 484                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4128877005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269217;                                                                                        
                                                                                                                                                      
-- 485                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4129847009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269516;                                                                                        
                                                                                                                                                      
-- 486                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4131647007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268761;                                                                                        
                                                                                                                                                      
-- 487                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4132707003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269193;                                                                                        
                                                                                                                                                      
-- 488                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4134085008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269894;                                                                                        
                                                                                                                                                      
-- 489                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4134847003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268774;                                                                                        
                                                                                                                                                      
-- 490                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4134917000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268103;                                                                                        
                                                                                                                                                      
-- 491                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4137237003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269336;                                                                                        
                                                                                                                                                      
-- 492                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4139017000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269380;                                                                                        
                                                                                                                                                      
-- 493                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4141837009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269280;                                                                                        
                                                                                                                                                      
-- 494                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4141857004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269213;                                                                                        
                                                                                                                                                      
-- 495                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4141997000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269300;                                                                                        
                                                                                                                                                      
-- 496                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4146847000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268706;                                                                                        
                                                                                                                                                      
-- 497                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4147577004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268886;                                                                                        
                                                                                                                                                      
-- 498                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4148337004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268769;                                                                                        
                                                                                                                                                      
-- 499                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4151287003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269054;                                                                                        
                                                                                                                                                      
-- 500                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4152055003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269687;                                                                                        
                                                                                                                                                      
-- 501                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4152585009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268171;                                                                                        
                                                                                                                                                      
-- 502                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4154617404',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268041;                                                                                        
                                                                                                                                                      
-- 503                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4157537007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268478;                                                                                        
                                                                                                                                                      
-- 504                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4158337008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269988;                                                                                        
                                                                                                                                                      
-- 505                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4158577010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268949;                                                                                        
                                                                                                                                                      
-- 506                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4161977002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268718;                                                                                        
                                                                                                                                                      
-- 507                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4163577004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268037;                                                                                        
                                                                                                                                                      
-- 508                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4164197010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269036;                                                                                        
                                                                                                                                                      
-- 509                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4166547001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268016;                                                                                        
                                                                                                                                                      
-- 510                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4167037003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268470;                                                                                        
                                                                                                                                                      
-- 511                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4173177006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269338;                                                                                        
                                                                                                                                                      
-- 512                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4173207002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268686;                                                                                        
                                                                                                                                                      
-- 513                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4174527006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269512;                                                                                        
                                                                                                                                                      
-- 514                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4174587002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268157;                                                                                        
                                                                                                                                                      
-- 515                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4175667004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269526;                                                                                        
                                                                                                                                                      
-- 516                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4176457002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269454;                                                                                        
                                                                                                                                                      
-- 517                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4176647000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268562;                                                                                        
                                                                                                                                                      
-- 518                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4177467007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269514;                                                                                        
                                                                                                                                                      
-- 519                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4179717003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268404;                                                                                        
                                                                                                                                                      
-- 520                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4180507007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269215;                                                                                        
                                                                                                                                                      
-- 521                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4181347009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269772;                                                                                        
                                                                                                                                                      
-- 522                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4184917009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=270023;                                                                                        
                                                                                                                                                      
-- 523                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4185257003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269227;                                                                                        
                                                                                                                                                      
-- 524                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4185907008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269582;                                                                                        
                                                                                                                                                      
-- 525                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4186037009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269042;                                                                                        
                                                                                                                                                      
-- 526                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4186187008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268985;                                                                                        
                                                                                                                                                      
-- 527                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4187117000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269390;                                                                                        
                                                                                                                                                      
-- 528                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4187587001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268646;                                                                                        
                                                                                                                                                      
-- 529                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4188537010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269812;                                                                                        
                                                                                                                                                      
-- 530                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4188577000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269233;                                                                                        
                                                                                                                                                      
-- 531                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4189577002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268644;                                                                                        
                                                                                                                                                      
-- 532                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4190297009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268796;                                                                                        
                                                                                                                                                      
-- 533                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4190347000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269096;                                                                                        
                                                                                                                                                      
-- 534                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4192387005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269852;                                                                                        
                                                                                                                                                      
-- 535                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4194077010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268620;                                                                                        
                                                                                                                                                      
-- 536                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4195677004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268923;                                                                                        
                                                                                                                                                      
-- 537                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4195907001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269737;                                                                                        
                                                                                                                                                      
-- 538                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4196257009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268219;                                                                                        
                                                                                                                                                      
-- 539                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4196487008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269268;                                                                                        
                                                                                                                                                      
-- 540                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4197037004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269820;                                                                                        
                                                                                                                                                      
-- 541                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4197367009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268861;                                                                                        
                                                                                                                                                      
-- 542                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4197967001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268716;                                                                                        
                                                                                                                                                      
-- 543                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4198017000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268117;                                                                                        
                                                                                                                                                      
-- 544                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4198447000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268216;                                                                                        
                                                                                                                                                      
-- 545                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4199257004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268546;                                                                                        
                                                                                                                                                      
-- 546                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4200467006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269350;                                                                                        
                                                                                                                                                      
-- 547                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4201597001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269502;                                                                                        
                                                                                                                                                      
-- 548                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4201964006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268428;                                                                                        
                                                                                                                                                      
-- 549                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4204347002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268217;                                                                                        
                                                                                                                                                      
-- 550                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4204597007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269436;                                                                                        
                                                                                                                                                      
-- 551                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4205627005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268165;                                                                                        
                                                                                                                                                      
-- 552                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4207077010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269476;                                                                                        
                                                                                                                                                      
-- 553                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4214047010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269914;                                                                                        
                                                                                                                                                      
-- 554                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4215967006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268207;                                                                                        
                                                                                                                                                      
-- 555                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4216207003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268420;                                                                                        
                                                                                                                                                      
-- 556                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4216687007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269274;                                                                                        
                                                                                                                                                      
-- 557                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4221117005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269148;                                                                                        
                                                                                                                                                      
-- 558                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4223157010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269396;                                                                                        
                                                                                                                                                      
-- 559                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4223697008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268522;                                                                                        
                                                                                                                                                      
-- 560                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4226127007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268392;                                                                                        
                                                                                                                                                      
-- 561                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4226737002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269992;                                                                                        
                                                                                                                                                      
-- 562                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4227187005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268991;                                                                                        
                                                                                                                                                      
-- 563                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4228097004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269530;                                                                                        
                                                                                                                                                      
-- 564                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4230097003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269886;                                                                                        
                                                                                                                                                      
-- 565                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4231607003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269745;                                                                                        
                                                                                                                                                      
-- 566                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4232707000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269653;                                                                                        
                                                                                                                                                      
-- 567                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4233967010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269360;                                                                                        
                                                                                                                                                      
-- 568                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4236297005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268971;                                                                                        
                                                                                                                                                      
-- 569                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4236347007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268632;                                                                                        
                                                                                                                                                      
-- 570                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4236967005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269576;                                                                                        
                                                                                                                                                      
-- 571                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4238007003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269136;                                                                                        
                                                                                                                                                      
-- 572                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4240737009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268127;                                                                                        
                                                                                                                                                      
-- 573                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4240777010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269416;                                                                                        
                                                                                                                                                      
-- 574                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4243727001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268069;                                                                                        
                                                                                                                                                      
-- 575                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4246447006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268199;                                                                                        
                                                                                                                                                      
-- 576                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4247527008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268963;                                                                                        
                                                                                                                                                      
-- 577                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4250397007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268609;                                                                                        
                                                                                                                                                      
-- 578                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4253767006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269938;                                                                                        
                                                                                                                                                      
-- 579                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4254577010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268666;                                                                                        
                                                                                                                                                      
-- 580                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4256717008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269007;                                                                                        
                                                                                                                                                      
-- 581                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4257767003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268817;                                                                                        
                                                                                                                                                      
-- 582                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4259687007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268520;                                                                                        
                                                                                                                                                      
-- 583                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4260307006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268987;                                                                                        
                                                                                                                                                      
-- 584                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4263527008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269290;                                                                                        
                                                                                                                                                      
-- 585                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4263707003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268029;                                                                                        
                                                                                                                                                      
-- 586                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4264457307',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269568;                                                                                        
                                                                                                                                                      
-- 587                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4269747005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268888;                                                                                        
                                                                                                                                                      
-- 588                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4269785003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268010;                                                                                        
                                                                                                                                                      
-- 589                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4269947006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269013;                                                                                        
                                                                                                                                                      
-- 590                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4270097008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269608;                                                                                        
                                                                                                                                                      
-- 591                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4271207006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269836;                                                                                        
                                                                                                                                                      
-- 592                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4272917009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269258;                                                                                        
                                                                                                                                                      
-- 593                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4273067005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268700;                                                                                        
                                                                                                                                                      
-- 594                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4274785008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269284;                                                                                        
                                                                                                                                                      
-- 595                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4274895006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268819;                                                                                        
                                                                                                                                                      
-- 596                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4276567008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269306;                                                                                        
                                                                                                                                                      
-- 597                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4277135007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269942;                                                                                        
                                                                                                                                                      
-- 598                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4278675007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268597;                                                                                        
                                                                                                                                                      
-- 599                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4282887006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268340;                                                                                        
                                                                                                                                                      
-- 600                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4284467002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269020;                                                                                        
                                                                                                                                                      
-- 601                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4286727001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269697;                                                                                        
                                                                                                                                                      
-- 602                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4291627000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268231;                                                                                        
                                                                                                                                                      
-- 603                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4292357004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269486;                                                                                        
                                                                                                                                                      
-- 604                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4294877000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268653;                                                                                        
                                                                                                                                                      
-- 605                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4296257006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269610;                                                                                        
                                                                                                                                                      
-- 606                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4298337010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269982;                                                                                        
                                                                                                                                                      
-- 607                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4299107002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269191;                                                                                        
                                                                                                                                                      
-- 608                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4303137004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269185;                                                                                        
                                                                                                                                                      
-- 609                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4312857010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269209;                                                                                        
                                                                                                                                                      
-- 610                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4316537005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268618;                                                                                        
                                                                                                                                                      
-- 611                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4316867010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269902;                                                                                        
                                                                                                                                                      
-- 612                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4318627001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269884;                                                                                        
                                                                                                                                                      
-- 613                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4320477003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268823;                                                                                        
                                                                                                                                                      
-- 614                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4329297004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269239;                                                                                        
                                                                                                                                                      
-- 615                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4329367001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268099;                                                                                        
                                                                                                                                                      
-- 616                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4333937009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268344;                                                                                        
                                                                                                                                                      
-- 617                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4340537005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268526;                                                                                        
                                                                                                                                                      
-- 618                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4341497008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269673;                                                                                        
                                                                                                                                                      
-- 619                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4348775009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268736;                                                                                        
                                                                                                                                                      
-- 620                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4361615001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268023;                                                                                        
                                                                                                                                                      
-- 621                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4368805002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268264;                                                                                        
                                                                                                                                                      
-- 622                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4380435001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268071;                                                                                        
                                                                                                                                                      
-- 623                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4382767002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268307;                                                                                        
                                                                                                                                                      
-- 624                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4444725005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269199;                                                                                        
                                                                                                                                                      
-- 625                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4488465001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269026;                                                                                        
                                                                                                                                                      
-- 626                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4504015007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268163;                                                                                        
                                                                                                                                                      
-- 627                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4515977806',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269406;                                                                                        
                                                                                                                                                      
-- 628                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4543654007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269794;                                                                                        
                                                                                                                                                      
-- 629                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4554057107',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268595;                                                                                        
                                                                                                                                                      
-- 630                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4558255004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269776;                                                                                        
                                                                                                                                                      
-- 631                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4572335000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269723;                                                                                        
                                                                                                                                                      
-- 632                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4590065006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268554;                                                                                        
                                                                                                                                                      
-- 633                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4599495002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269954;                                                                                        
                                                                                                                                                      
-- 634                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4623568206',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269298;                                                                                        
                                                                                                                                                      
-- 635                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4655995005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268552;                                                                                        
                                                                                                                                                      
-- 636                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4673675007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268450;                                                                                        
                                                                                                                                                      
-- 637                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4686145000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268203;                                                                                        
                                                                                                                                                      
-- 638                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4713945000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269844;                                                                                        
                                                                                                                                                      
-- 639                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4718984004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268638;                                                                                        
                                                                                                                                                      
-- 640                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4732815002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269496;                                                                                        
                                                                                                                                                      
-- 641                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4743088100',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268155;                                                                                        
                                                                                                                                                      
-- 642                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4747784002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269520;                                                                                        
                                                                                                                                                      
-- 643                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4751304001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268902;                                                                                        
                                                                                                                                                      
-- 644                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4759615001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269990;                                                                                        
                                                                                                                                                      
-- 645                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4772645004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269518;                                                                                        
                                                                                                                                                      
-- 646                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4796965000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268730;                                                                                        
                                                                                                                                                      
-- 647                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4813765002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268636;                                                                                        
                                                                                                                                                      
-- 648                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4828555001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269633;                                                                                        
                                                                                                                                                      
-- 649                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4846395005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268794;                                                                                        
                                                                                                                                                      
-- 650                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4862795007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268833;                                                                                        
                                                                                                                                                      
-- 651                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4868014001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269452;                                                                                        
                                                                                                                                                      
-- 652                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4869797100',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268601;                                                                                        
                                                                                                                                                      
-- 653                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4882665000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269110;                                                                                        
                                                                                                                                                      
-- 654                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4892144005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268929;                                                                                        
                                                                                                                                                      
-- 655                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4898605009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269558;                                                                                        
                                                                                                                                                      
-- 656                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4903898107',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268410;                                                                                        
                                                                                                                                                      
-- 657                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4905238003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269088;                                                                                        
                                                                                                                                                      
-- 658                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4907895007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268690;                                                                                        
                                                                                                                                                      
-- 659                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4926895002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268892;                                                                                        
                                                                                                                                                      
-- 660                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4927755008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268997;                                                                                        
                                                                                                                                                      
-- 661                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4937345007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269681;                                                                                        
                                                                                                                                                      
-- 662                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4945115008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269552;                                                                                        
                                                                                                                                                      
-- 663                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4950715005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268012;                                                                                        
                                                                                                                                                      
-- 664                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4951235005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268720;                                                                                        
                                                                                                                                                      
-- 665                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4952875000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268313;                                                                                        
                                                                                                                                                      
-- 666                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4963667408',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269756;                                                                                        
                                                                                                                                                      
-- 667                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4969195004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268847;                                                                                        
                                                                                                                                                      
-- 668                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4989375007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269685;                                                                                        
                                                                                                                                                      
-- 669                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='4998415008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269294;                                                                                        
                                                                                                                                                      
-- 670                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5001235006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268999;                                                                                        
                                                                                                                                                      
-- 671                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5005185001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268286;                                                                                        
                                                                                                                                                      
-- 672                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5009865001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269018;                                                                                        
                                                                                                                                                      
-- 673                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5012735009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268524;                                                                                        
                                                                                                                                                      
-- 674                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5026685008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=270016;                                                                                        
                                                                                                                                                      
-- 675                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5026855000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269048;                                                                                        
                                                                                                                                                      
-- 676                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5030635007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268039;                                                                                        
                                                                                                                                                      
-- 677                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5035208005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269117;                                                                                        
                                                                                                                                                      
-- 678                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5038494000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268698;                                                                                        
                                                                                                                                                      
-- 679                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5051215009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268272;                                                                                        
                                                                                                                                                      
-- 680                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5051798007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269118;                                                                                        
                                                                                                                                                      
-- 681                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5060317804',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268871;                                                                                        
                                                                                                                                                      
-- 682                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5071195002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268900;                                                                                        
                                                                                                                                                      
-- 683                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5089775008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269524;                                                                                        
                                                                                                                                                      
-- 684                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5096435009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269528;                                                                                        
                                                                                                                                                      
-- 685                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5098165004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269090;                                                                                        
                                                                                                                                                      
-- 686                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5107755007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268798;                                                                                        
                                                                                                                                                      
-- 687                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5109525001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268352;                                                                                        
                                                                                                                                                      
-- 688                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5114477802',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268067;                                                                                        
                                                                                                                                                      
-- 689                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5119255007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269796;                                                                                        
                                                                                                                                                      
-- 690                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5123595007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269400;                                                                                        
                                                                                                                                                      
-- 691                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5123837903',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268303;                                                                                        
                                                                                                                                                      
-- 692                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5125045010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268626;                                                                                        
                                                                                                                                                      
-- 693                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5128075003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268744;                                                                                        
                                                                                                                                                      
-- 694                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5129975004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268945;                                                                                        
                                                                                                                                                      
-- 695                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5132908103',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269602;                                                                                        
                                                                                                                                                      
-- 696                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5137925000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269624;                                                                                        
                                                                                                                                                      
-- 697                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5144075003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268035;                                                                                        
                                                                                                                                                      
-- 698                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5157705001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269056;                                                                                        
                                                                                                                                                      
-- 699                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5159035005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268045;                                                                                        
                                                                                                                                                      
-- 700                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5162395009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269237;                                                                                        
                                                                                                                                                      
-- 701                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5182915007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268384;                                                                                        
                                                                                                                                                      
-- 702                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5191945007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269134;                                                                                        
                                                                                                                                                      
-- 703                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5192165000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268043;                                                                                        
                                                                                                                                                      
-- 704                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5199715002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=270025;                                                                                        
                                                                                                                                                      
-- 705                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5222105006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268225;                                                                                        
                                                                                                                                                      
-- 706                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5244155000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268894;                                                                                        
                                                                                                                                                      
-- 707                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5261695000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269060;                                                                                        
                                                                                                                                                      
-- 708                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5264185006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269466;                                                                                        
                                                                                                                                                      
-- 709                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5296865003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269462;                                                                                        
                                                                                                                                                      
-- 710                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5303435002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269774;                                                                                        
                                                                                                                                                      
-- 711                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5317125004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268019;                                                                                        
                                                                                                                                                      
-- 712                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5317435003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268544;                                                                                        
                                                                                                                                                      
-- 713                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5324518109',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269442;                                                                                        
                                                                                                                                                      
-- 714                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5339905003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268221;                                                                                        
                                                                                                                                                      
-- 715                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5349185005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269994;                                                                                        
                                                                                                                                                      
-- 716                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5365935005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268696;                                                                                        
                                                                                                                                                      
-- 717                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5391565005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269404;                                                                                        
                                                                                                                                                      
-- 718                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5395995002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269478;                                                                                        
                                                                                                                                                      
-- 719                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5418885008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269826;                                                                                        
                                                                                                                                                      
-- 720                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5421865003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268211;                                                                                        
                                                                                                                                                      
-- 721                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5447705010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268941;                                                                                        
                                                                                                                                                      
-- 722                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5499725007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269266;                                                                                        
                                                                                                                                                      
-- 723                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5509275008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268702;                                                                                        
                                                                                                                                                      
-- 724                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5522105008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269458;                                                                                        
                                                                                                                                                      
-- 725                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5540395007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269544;                                                                                        
                                                                                                                                                      
-- 726                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5588055009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269705;                                                                                        
                                                                                                                                                      
-- 727                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5703954008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268533;                                                                                        
                                                                                                                                                      
-- 728                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5785365007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268073;                                                                                        
                                                                                                                                                      
-- 729                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5787415002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269132;                                                                                        
                                                                                                                                                      
-- 730                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5809838109',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269595;                                                                                        
                                                                                                                                                      
-- 731                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5836735001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269709;                                                                                        
                                                                                                                                                      
-- 732                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5845584010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268366;                                                                                        
                                                                                                                                                      
-- 733                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5873895002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268843;                                                                                        
                                                                                                                                                      
-- 734                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5906884000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269782;                                                                                        
                                                                                                                                                      
-- 735                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='5948088109',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268101;                                                                                        
                                                                                                                                                      
-- 736                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6000945010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269197;                                                                                        
                                                                                                                                                      
-- 737                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6003325007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269219;                                                                                        
                                                                                                                                                      
-- 738                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6018977107',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268926;                                                                                        
                                                                                                                                                      
-- 739                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6024434008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269074;                                                                                        
                                                                                                                                                      
-- 740                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6044915003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268890;                                                                                        
                                                                                                                                                      
-- 741                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6048068004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269340;                                                                                        
                                                                                                                                                      
-- 742                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6051085001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268462;                                                                                        
                                                                                                                                                      
-- 743                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6066505010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268327;                                                                                        
                                                                                                                                                      
-- 744                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6067155003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269590;                                                                                        
                                                                                                                                                      
-- 745                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6067585003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268857;                                                                                        
                                                                                                                                                      
-- 746                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6108865001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269784;                                                                                        
                                                                                                                                                      
-- 747                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6111735009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269962;                                                                                        
                                                                                                                                                      
-- 748                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6119235006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269068;                                                                                        
                                                                                                                                                      
-- 749                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6128725002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268240;                                                                                        
                                                                                                                                                      
-- 750                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6133287303',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268305;                                                                                        
                                                                                                                                                      
-- 751                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6151965002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268167;                                                                                        
                                                                                                                                                      
-- 752                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6186547008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269171;                                                                                        
                                                                                                                                                      
-- 753                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6186785007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269657;                                                                                        
                                                                                                                                                      
-- 754                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6202667402',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269586;                                                                                        
                                                                                                                                                      
-- 755                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6226305009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268400;                                                                                        
                                                                                                                                                      
-- 756                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6227195004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268319;                                                                                        
                                                                                                                                                      
-- 757                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6227664007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268782;                                                                                        
                                                                                                                                                      
-- 758                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6228788201',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269596;                                                                                        
                                                                                                                                                      
-- 759                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6245215001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269372;                                                                                        
                                                                                                                                                      
-- 760                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6246635000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269534;                                                                                        
                                                                                                                                                      
-- 761                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6257744008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268674;                                                                                        
                                                                                                                                                      
-- 762                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6262765004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269570;                                                                                        
                                                                                                                                                      
-- 763                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6346647101',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268268;                                                                                        
                                                                                                                                                      
-- 764                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6346825006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269952;                                                                                        
                                                                                                                                                      
-- 765                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6364385009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269334;                                                                                        
                                                                                                                                                      
-- 766                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6377087908',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268376;                                                                                        
                                                                                                                                                      
-- 767                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6381955005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268714;                                                                                        
                                                                                                                                                      
-- 768                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6412365009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268256;                                                                                        
                                                                                                                                                      
-- 769                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6418605010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268065;                                                                                        
                                                                                                                                                      
-- 770                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6422685004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268059;                                                                                        
                                                                                                                                                      
-- 771                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6424218202',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269130;                                                                                        
                                                                                                                                                      
-- 772                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6427814009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268424;                                                                                        
                                                                                                                                                      
-- 773                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6438455010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268209;                                                                                        
                                                                                                                                                      
-- 774                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6524005000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269752;                                                                                        
                                                                                                                                                      
-- 775                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6547855000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269649;                                                                                        
                                                                                                                                                      
-- 776                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6561984004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268564;                                                                                        
                                                                                                                                                      
-- 777                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6573484004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268607;                                                                                        
                                                                                                                                                      
-- 778                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6601915007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268075;                                                                                        
                                                                                                                                                      
-- 779                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6626305008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268033;                                                                                        
                                                                                                                                                      
-- 780                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6665435004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269312;                                                                                        
                                                                                                                                                      
-- 781                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6676015002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269322;                                                                                        
                                                                                                                                                      
-- 782                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6679515005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268531;                                                                                        
                                                                                                                                                      
-- 783                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6698525003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268179;                                                                                        
                                                                                                                                                      
-- 784                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6711185003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269622;                                                                                        
                                                                                                                                                      
-- 785                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6716477804',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268624;                                                                                        
                                                                                                                                                      
-- 786                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6721158008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269713;                                                                                        
                                                                                                                                                      
-- 787                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6726485002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268137;                                                                                        
                                                                                                                                                      
-- 788                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6732015008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268338;                                                                                        
                                                                                                                                                      
-- 789                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6733794003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269643;                                                                                        
                                                                                                                                                      
-- 790                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6737425001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269818;                                                                                        
                                                                                                                                                      
-- 791                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6753025010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269386;                                                                                        
                                                                                                                                                      
-- 792                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6753805008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268504;                                                                                        
                                                                                                                                                      
-- 793                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6756845004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269150;                                                                                        
                                                                                                                                                      
-- 794                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6772725003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269741;                                                                                        
                                                                                                                                                      
-- 795                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6795208100',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268859;                                                                                        
                                                                                                                                                      
-- 796                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6819235007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268500;                                                                                        
                                                                                                                                                      
-- 797                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6828605002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268402;                                                                                        
                                                                                                                                                      
-- 798                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6841245006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269860;                                                                                        
                                                                                                                                                      
-- 799                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6866908107',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268935;                                                                                        
                                                                                                                                                      
-- 800                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6882455006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268247;                                                                                        
                                                                                                                                                      
-- 801                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6891034004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268969;                                                                                        
                                                                                                                                                      
-- 802                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6929455004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269450;                                                                                        
                                                                                                                                                      
-- 803                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6929544000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269974;                                                                                        
                                                                                                                                                      
-- 804                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6935465003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269448;                                                                                        
                                                                                                                                                      
-- 805                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6942735010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269066;                                                                                        
                                                                                                                                                      
-- 806                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6943147407',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268654;                                                                                        
                                                                                                                                                      
-- 807                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6943705003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269838;                                                                                        
                                                                                                                                                      
-- 808                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6948895002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269402;                                                                                        
                                                                                                                                                      
-- 809                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6967405001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=270018;                                                                                        
                                                                                                                                                      
-- 810                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='6984025007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269430;                                                                                        
                                                                                                                                                      
-- 811                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7017027107',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268640;                                                                                        
                                                                                                                                                      
-- 812                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7017135004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269374;                                                                                        
                                                                                                                                                      
-- 813                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7024235008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268149;                                                                                        
                                                                                                                                                      
-- 814                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7034918101',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269647;                                                                                        
                                                                                                                                                      
-- 815                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7039225008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268195;                                                                                        
                                                                                                                                                      
-- 816                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7043854005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268611;                                                                                        
                                                                                                                                                      
-- 817                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7076477407',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269906;                                                                                        
                                                                                                                                                      
-- 818                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7125545006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269082;                                                                                        
                                                                                                                                                      
-- 819                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7144245005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269161;                                                                                        
                                                                                                                                                      
-- 820                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7172864004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269260;                                                                                        
                                                                                                                                                      
-- 821                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7175035004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268688;                                                                                        
                                                                                                                                                      
-- 822                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7180765005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269310;                                                                                        
                                                                                                                                                      
-- 823                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7204295001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269422;                                                                                        
                                                                                                                                                      
-- 824                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7223754007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268119;                                                                                        
                                                                                                                                                      
-- 825                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7246834010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269302;                                                                                        
                                                                                                                                                      
-- 826                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7275278209',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268919;                                                                                        
                                                                                                                                                      
-- 827                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7275714008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269936;                                                                                        
                                                                                                                                                      
-- 828                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7304917008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268129;                                                                                        
                                                                                                                                                      
-- 829                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7323355009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269440;                                                                                        
                                                                                                                                                      
-- 830                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7323594004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269699;                                                                                        
                                                                                                                                                      
-- 831                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7361775007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269739;                                                                                        
                                                                                                                                                      
-- 832                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7379065004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269693;                                                                                        
                                                                                                                                                      
-- 833                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7379735004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269950;                                                                                        
                                                                                                                                                      
-- 834                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7404425008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269163;                                                                                        
                                                                                                                                                      
-- 835                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7407065002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269550;                                                                                        
                                                                                                                                                      
-- 836                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7412784004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268418;                                                                                        
                                                                                                                                                      
-- 837                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7444165007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269356;                                                                                        
                                                                                                                                                      
-- 838                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7450115010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268692;                                                                                        
                                                                                                                                                      
-- 839                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7450495008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269735;                                                                                        
                                                                                                                                                      
-- 840                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7479585004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268266;                                                                                        
                                                                                                                                                      
-- 841                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7479745004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269743;                                                                                        
                                                                                                                                                      
-- 842                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7480294001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269376;                                                                                        
                                                                                                                                                      
-- 843                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7488815003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269504;                                                                                        
                                                                                                                                                      
-- 844                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7495085007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269806;                                                                                        
                                                                                                                                                      
-- 845                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7517074010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269046;                                                                                        
                                                                                                                                                      
-- 846                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7523315002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269780;                                                                                        
                                                                                                                                                      
-- 847                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7537688106',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269028;                                                                                        
                                                                                                                                                      
-- 848                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7556405001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269703;                                                                                        
                                                                                                                                                      
-- 849                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7556815006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269667;                                                                                        
                                                                                                                                                      
-- 850                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7559875008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269856;                                                                                        
                                                                                                                                                      
-- 851                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7582145010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268749;                                                                                        
                                                                                                                                                      
-- 852                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7585165000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269760;                                                                                        
                                                                                                                                                      
-- 853                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7596555005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269788;                                                                                        
                                                                                                                                                      
-- 854                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7597015009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=270012;                                                                                        
                                                                                                                                                      
-- 855                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7597375001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268512;                                                                                        
                                                                                                                                                      
-- 856                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7599065006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269592;                                                                                        
                                                                                                                                                      
-- 857                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7605035005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268488;                                                                                        
                                                                                                                                                      
-- 858                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7605125008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268329;                                                                                        
                                                                                                                                                      
-- 859                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7616054010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268311;                                                                                        
                                                                                                                                                      
-- 860                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7616185010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268792;                                                                                        
                                                                                                                                                      
-- 861                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7632745001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268133;                                                                                        
                                                                                                                                                      
-- 862                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7650995010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268460;                                                                                        
                                                                                                                                                      
-- 863                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7658275000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268336;                                                                                        
                                                                                                                                                      
-- 864                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7666794007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268851;                                                                                        
                                                                                                                                                      
-- 865                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7683227306',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269976;                                                                                        
                                                                                                                                                      
-- 866                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7689434007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268282;                                                                                        
                                                                                                                                                      
-- 867                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7729574009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269388;                                                                                        
                                                                                                                                                      
-- 868                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7766625006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269207;                                                                                        
                                                                                                                                                      
-- 869                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7787588103',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268181;                                                                                        
                                                                                                                                                      
-- 870                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7800478209',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269790;                                                                                        
                                                                                                                                                      
-- 871                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7822217805',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269292;                                                                                        
                                                                                                                                                      
-- 872                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7829994003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269882;                                                                                        
                                                                                                                                                      
-- 873                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7842615006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268183;                                                                                        
                                                                                                                                                      
-- 874                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7845245008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269566;                                                                                        
                                                                                                                                                      
-- 875                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7851915004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269384;                                                                                        
                                                                                                                                                      
-- 876                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7852258106',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269034;                                                                                        
                                                                                                                                                      
-- 877                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7854764006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269926;                                                                                        
                                                                                                                                                      
-- 878                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7876285001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269890;                                                                                        
                                                                                                                                                      
-- 879                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7889255002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269958;                                                                                        
                                                                                                                                                      
-- 880                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7913995002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268193;                                                                                        
                                                                                                                                                      
-- 881                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7918615003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268576;                                                                                        
                                                                                                                                                      
-- 882                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7937695000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269003;                                                                                        
                                                                                                                                                      
-- 883                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7953185000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269536;                                                                                        
                                                                                                                                                      
-- 884                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='7995815008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269725;                                                                                        
                                                                                                                                                      
-- 885                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8056665007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268658;                                                                                        
                                                                                                                                                      
-- 886                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8108085002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268712;                                                                                        
                                                                                                                                                      
-- 887                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8114525010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269870;                                                                                        
                                                                                                                                                      
-- 888                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8114705005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269138;                                                                                        
                                                                                                                                                      
-- 889                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8133945002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269078;                                                                                        
                                                                                                                                                      
-- 890                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8169955007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269251;                                                                                        
                                                                                                                                                      
-- 891                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8187045009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268362;                                                                                        
                                                                                                                                                      
-- 892                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8188495006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269124;                                                                                        
                                                                                                                                                      
-- 893                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8209344000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268131;                                                                                        
                                                                                                                                                      
-- 894                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8227738205',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268506;                                                                                        
                                                                                                                                                      
-- 895                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8247405002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269614;                                                                                        
                                                                                                                                                      
-- 896                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8248775010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269731;                                                                                        
                                                                                                                                                      
-- 897                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8258475007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269834;                                                                                        
                                                                                                                                                      
-- 898                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8280935010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268516;                                                                                        
                                                                                                                                                      
-- 899                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8283525000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268599;                                                                                        
                                                                                                                                                      
-- 900                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8284697402',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268588;                                                                                        
                                                                                                                                                      
-- 901                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8288295002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269181;                                                                                        
                                                                                                                                                      
-- 902                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8363607308',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269330;                                                                                        
                                                                                                                                                      
-- 903                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8370824009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268097;                                                                                        
                                                                                                                                                      
-- 904                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8405695004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268710;                                                                                        
                                                                                                                                                      
-- 905                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8423055004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268372;                                                                                        
                                                                                                                                                      
-- 906                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8440415007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268498;                                                                                        
                                                                                                                                                      
-- 907                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8443805001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269934;                                                                                        
                                                                                                                                                      
-- 908                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8467655009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269076;                                                                                        
                                                                                                                                                      
-- 909                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8471395006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268396;                                                                                        
                                                                                                                                                      
-- 910                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8473945009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269494;                                                                                        
                                                                                                                                                      
-- 911                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8494615003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269203;                                                                                        
                                                                                                                                                      
-- 912                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8556035002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269352;                                                                                        
                                                                                                                                                      
-- 913                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8572535010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268243;                                                                                        
                                                                                                                                                      
-- 914                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8587895005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269038;                                                                                        
                                                                                                                                                      
-- 915                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8603665007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268187;                                                                                        
                                                                                                                                                      
-- 916                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8606347308',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268927;                                                                                        
                                                                                                                                                      
-- 917                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8633495005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269904;                                                                                        
                                                                                                                                                      
-- 918                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8634905010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268772;                                                                                        
                                                                                                                                                      
-- 919                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8649035001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269538;                                                                                        
                                                                                                                                                      
-- 920                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8657225010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269840;                                                                                        
                                                                                                                                                      
-- 921                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8669845005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268738;                                                                                        
                                                                                                                                                      
-- 922                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8689295003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268502;                                                                                        
                                                                                                                                                      
-- 923                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8696065002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268906;                                                                                        
                                                                                                                                                      
-- 924                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8698855007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269659;                                                                                        
                                                                                                                                                      
-- 925                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8702074006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268965;                                                                                        
                                                                                                                                                      
-- 926                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8711798105',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268189;                                                                                        
                                                                                                                                                      
-- 927                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8718305004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269428;                                                                                        
                                                                                                                                                      
-- 928                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8781435000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268831;                                                                                        
                                                                                                                                                      
-- 929                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8788104002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268634;                                                                                        
                                                                                                                                                      
-- 930                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8801284005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269822;                                                                                        
                                                                                                                                                      
-- 931                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8804715005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268368;                                                                                        
                                                                                                                                                      
-- 932                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8876264004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268804;                                                                                        
                                                                                                                                                      
-- 933                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8904234004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269225;                                                                                        
                                                                                                                                                      
-- 934                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8915537310',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269598;                                                                                        
                                                                                                                                                      
-- 935                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8928225002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269892;                                                                                        
                                                                                                                                                      
-- 936                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8954704001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269986;                                                                                        
                                                                                                                                                      
-- 937                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8982754002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269324;                                                                                        
                                                                                                                                                      
-- 938                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8995445009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269490;                                                                                        
                                                                                                                                                      
-- 939                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8999565007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269711;                                                                                        
                                                                                                                                                      
-- 940                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='8999625001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269546;                                                                                        
                                                                                                                                                      
-- 941                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9009385008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269205;                                                                                        
                                                                                                                                                      
-- 942                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9017675001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269175;                                                                                        
                                                                                                                                                      
-- 943                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9021954009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269848;                                                                                        
                                                                                                                                                      
-- 944                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9039915002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269641;                                                                                        
                                                                                                                                                      
-- 945                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9054124003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269472;                                                                                        
                                                                                                                                                      
-- 946                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9054784002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269482;                                                                                        
                                                                                                                                                      
-- 947                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9063005000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269009;                                                                                        
                                                                                                                                                      
-- 948                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9074754001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268089;                                                                                        
                                                                                                                                                      
-- 949                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9093545010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269910;                                                                                        
                                                                                                                                                      
-- 950                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9095074008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269600;                                                                                        
                                                                                                                                                      
-- 951                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9098734000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=270006;                                                                                        
                                                                                                                                                      
-- 952                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9101017803',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268605;                                                                                        
                                                                                                                                                      
-- 953                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9115305010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269980;                                                                                        
                                                                                                                                                      
-- 954                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9118114000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269189;                                                                                        
                                                                                                                                                      
-- 955                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9154954002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269691;                                                                                        
                                                                                                                                                      
-- 956                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9171325010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269304;                                                                                        
                                                                                                                                                      
-- 957                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9182865003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268566;                                                                                        
                                                                                                                                                      
-- 958                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9189095000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269256;                                                                                        
                                                                                                                                                      
-- 959                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9192245009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268426;                                                                                        
                                                                                                                                                      
-- 960                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9195374001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269024;                                                                                        
                                                                                                                                                      
-- 961                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9236645003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268107;                                                                                        
                                                                                                                                                      
-- 962                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9269314009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268434;                                                                                        
                                                                                                                                                      
-- 963                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9272635008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268448;                                                                                        
                                                                                                                                                      
-- 964                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9272865007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269398;                                                                                        
                                                                                                                                                      
-- 965                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9327325007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268827;                                                                                        
                                                                                                                                                      
-- 966                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9344205008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269286;                                                                                        
                                                                                                                                                      
-- 967                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9354134008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269155;                                                                                        
                                                                                                                                                      
-- 968                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9361215002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269366;                                                                                        
                                                                                                                                                      
-- 969                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9370935008',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268615;                                                                                        
                                                                                                                                                      
-- 970                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9370945000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269556;                                                                                        
                                                                                                                                                      
-- 971                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9387515007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268085;                                                                                        
                                                                                                                                                      
-- 972                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9407025000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268734;                                                                                        
                                                                                                                                                      
-- 973                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9422955006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269510;                                                                                        
                                                                                                                                                      
-- 974                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9426685005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269146;                                                                                        
                                                                                                                                                      
-- 975                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9450224000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269802;                                                                                        
                                                                                                                                                      
-- 976                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9454685002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269114;                                                                                        
                                                                                                                                                      
-- 977                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9484715010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269320;                                                                                        
                                                                                                                                                      
-- 978                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9496135010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269408;                                                                                        
                                                                                                                                                      
-- 979                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9500237807',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269270;                                                                                        
                                                                                                                                                      
-- 980                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9507785002',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268315;                                                                                        
                                                                                                                                                      
-- 981                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9548975007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268278;                                                                                        
                                                                                                                                                      
-- 982                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9575164010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269414;                                                                                        
                                                                                                                                                      
-- 983                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9583925009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268973;                                                                                        
                                                                                                                                                      
-- 984                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9585895006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269930;                                                                                        
                                                                                                                                                      
-- 985                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9590085004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268432;                                                                                        
                                                                                                                                                      
-- 986                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9595945001',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268047;                                                                                        
                                                                                                                                                      
-- 987                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9635035005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268529;                                                                                        
                                                                                                                                                      
-- 988                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9640845006',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268759;                                                                                        
                                                                                                                                                      
-- 989                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9653785000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268580;                                                                                        
                                                                                                                                                      
-- 990                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9706415010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268135;                                                                                        
                                                                                                                                                      
-- 991                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9707705005',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268726;                                                                                        
                                                                                                                                                      
-- 992                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9727175009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268380;                                                                                        
                                                                                                                                                      
-- 993                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9761404003',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269247;                                                                                        
                                                                                                                                                      
-- 994                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9786267004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269898;                                                                                        
                                                                                                                                                      
-- 995                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9846915009',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269626;                                                                                        
                                                                                                                                                      
-- 996                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9873925007',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269946;                                                                                        
                                                                                                                                                      
-- 997                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9886605004',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268813;                                                                                        
                                                                                                                                                      
-- 998                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9904335000',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=268145;                                                                                        
                                                                                                                                                      
-- 999                                                                                                   
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='9939215010',STATIDCUR=23, CREATUSERID='U801545CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=269631;                                                                                        
                                                                                                                                                      
-- 1000                                                                                                  
                                                                                                                                                      
COMMIT;                                                                           
Select 'END of update statements' from dual;                                      
                                                                                                                                                      

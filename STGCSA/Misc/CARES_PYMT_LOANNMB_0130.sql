/* first statement will not find a row to update                             
   it is intended to verify finding statements that do not match a row              
   when manually reviewing the log fog file.   */                                                           
UPDATE STGCSA.CORETRANSTBL set LOANNMB='NONESUCH'  ,STATIDCUR=22, CREATUSERID='U269783CSA',   
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID=00;                                
/* all following statements should update 1 row */                                  
                                                                                       
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='1002577010',STATIDCUR=22, CREATUSERID='U269783CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID='231690';                                                                                      
                                                                                                                                                      
-- 1                                                                                                     
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='1031467000',STATIDCUR=22, CREATUSERID='U269783CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID='231691';                                                                                      
                                                                                                                                                      
-- 2                                                                                                     
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='1074257007',STATIDCUR=22, CREATUSERID='U269783CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID='231692';                                                                                      
                                                                                                                                                      
-- 3                                                                                                     
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='1074487006',STATIDCUR=22, CREATUSERID='U269783CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID='231693';                                                                                      
                                                                                                                                                      
-- 4                                                                                                     
                                                                                                                                                      
UPDATE STGCSA.CORETRANSTBL set LOANNMB='1078387008',STATIDCUR=22, CREATUSERID='U269783CSA', 
LASTUPDTDT=sysdate, LASTUPDTUSERID=user where TRANSID='231694';                                                                                      
                                                                                                                                                      
-- 5                                                                                                     
                                                                                                                                                      
COMMIT;                                                                           
Select 'END of update statements' from dual;                                      
                                                                                                                                                      

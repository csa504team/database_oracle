/* first statement will not find a row to update                             
   it is intended to verify finding statements that do not match a row              
   when manually reviewing the log fog file.   */                                   
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112' where LOANNMB='NONESUCH  ' 
  and PYMTPOSTPYMTDT=to_DATE('02-APR-20','dd-MON-yy')  AND PYMTPOSTPYMTTYP='Z' and PYMTPOSTREMITTEDAMT=-9999;    
/* all following statements should update 1 row */                                  
                                                                                       
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3576787001'                        
 and PYMTPOSTPYMTDT=to_DATE('20200716','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2440.54;                                          
-- 1                                                                                                     
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='3593777000'                        
 and PYMTPOSTPYMTDT=to_DATE('20200716','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=5428.26;                                          
-- 2                                                                                                     
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='4119257005'                        
 and PYMTPOSTPYMTDT=to_DATE('20200716','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=2479.72;                                          
-- 3                                                                                                     
                                                                                                                                                      
UPDATE stgcsa.sofvpymttbl set creatuserid='CARES Act 1112', LASTUPDTDT=sysdate, LASTUPDTUSERID=user where LOANNMB='9193075008'                        
 and PYMTPOSTPYMTDT=to_DATE('20200716','yyyymmdd')  AND PYMTPOSTPYMTTYP='C' and PYMTPOSTREMITTEDAMT=6109.85;                                          
-- 4                                                                                                     
                                                                                                                                                      
COMMIT;                                                                           
Select 'END of update statements' from dual;                                      
                                                                                                                                                      

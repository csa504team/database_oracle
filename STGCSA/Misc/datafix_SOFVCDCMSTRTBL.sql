-- verify online DB is active
ON SQLERROR EXIT
select 1/(select count(*) from stgcsa.xover_status
   where xover_last_event='SM') from dual;

---resultset before data fix
select cdcsetupdt, cdcregncd, cdccertnmb, creatdt, lastupdtdt from stgcsa.sofvcdcmstrtbl where cdcsetupdt=to_date('01011900','ddmmyyyy');


--- data fix steps
update STGCSA.SOFVCDCMSTRTBL set CDCSetupDt=TO_DATE('3/4/2020', 'MM/DD/YYYY'),
                                 LASTUPDTDT=sysdate,
                                 LASTUPDTUSERID=USER 
                             where CDCREGNCD='09' and CDCCERTNMB='713S';
                             
update STGCSA.SOFVCDCMSTRTBL set CDCSetupDt=CREATDT,
                                 LASTUPDTDT=sysdate,
                                 LASTUPDTUSERID=USER 
                             where CDCREGNCD='05' and CDCCERTNMB='038';
                             

update STGCSA.SOFVCDCMSTRTBL set CDCSetupDt=CREATDT,
                                 LASTUPDTDT=sysdate,
                                 LASTUPDTUSERID=USER 
                             where CDCREGNCD='03'and CDCCERTNMB='718';
commit;
                             
--resultset after data fix
select cdcsetupdt, cdcregncd, cdccertnmb, creatdt, lastupdtdt from stgcsa.sofvcdcmstrtbl where cdcsetupdt=to_date('01011900','ddmmyyyy');
select cdcsetupdt, cdcregncd, cdccertnmb, creatdt, lastupdtdt from stgcsa.sofvcdcmstrtbl where CDCREGNCD='09' and CDCCERTNMB ='713S';
select cdcsetupdt, cdcregncd, cdccertnmb, creatdt, lastupdtdt from stgcsa.sofvcdcmstrtbl where CDCREGNCD='05' and CDCCERTNMB ='038';
select cdcsetupdt, cdcregncd, cdccertnmb, creatdt, lastupdtdt from stgcsa.sofvcdcmstrtbl where CDCREGNCD='03' and CDCCERTNMB ='718';


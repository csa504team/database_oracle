CREATE TABLE PYMTBTCHTBL
(
  BTCHID          NUMBER,
  BTCHNMB         NUMBER,
  BTCHTOTAMT      NUMBER(15,2),
  ATTCHLOC        VARCHAR2(1000 BYTE),
  ATTCHNM         VARCHAR2(1000 BYTE),
  TIMESTAMPFLD    DATE,
  CREATUSERID     VARCHAR2(32 BYTE),
  CREATDT         DATE                          DEFAULT sysdate,
  LASTUPDTUSERID  VARCHAR2(32 BYTE),
  LASTUPDTDT      DATE                          DEFAULT sysdate
)
NOLOGGING 
NOCOMPRESS 
NO INMEMORY
NOCACHE
RESULT_CACHE (MODE DEFAULT)
NOPARALLEL
MONITORING;


GRANT DELETE, INSERT, SELECT, UPDATE ON PYMTBTCHTBL TO CSAUPDTROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON PYMTBTCHTBL TO LOANCSAADMINROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON PYMTBTCHTBL TO LOANCSAANALYSTROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON PYMTBTCHTBL TO LOANCSAMANAGERROLE;

GRANT SELECT ON PYMTBTCHTBL TO LOANCSAREADALLROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON PYMTBTCHTBL TO LOANCSAREVIEWERROLE;

GRANT SELECT ON PYMTBTCHTBL TO STGCSAREADALLROLE;

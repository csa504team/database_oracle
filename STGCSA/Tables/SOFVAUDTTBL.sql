CREATE TABLE SOFVAUDTTBL
(
  AUDTRECRDKEY      CHAR(15 BYTE),
  AUDTTRANSDT       TIMESTAMP(6),
  AUDTSEQNMB        NUMBER,
  AUDTOPRTRINITIAL  CHAR(2 BYTE),
  AUDTTRMNLID       CHAR(4 BYTE),
  AUDTCHNGFILE      CHAR(8 BYTE),
  AUDTACTNCD        CHAR(1 BYTE),
  AUDTBEFCNTNTS     VARCHAR2(80 BYTE),
  AUDTAFTCNTNTS     VARCHAR2(80 BYTE),
  AUDTFLDNM         CHAR(8 BYTE),
  AUDTEDTCD         CHAR(1 BYTE),
  AUDTUSERID        VARCHAR2(32 BYTE),
  CREATUSERID       VARCHAR2(32 BYTE),
  CREATDT           DATE                        DEFAULT sysdate,
  LASTUPDTUSERID    VARCHAR2(32 BYTE),
  LASTUPDTDT        DATE                        DEFAULT sysdate
)
NOLOGGING 
NOCOMPRESS 
NO INMEMORY
NOCACHE
RESULT_CACHE (MODE DEFAULT)
NOPARALLEL
MONITORING;


GRANT DELETE, INSERT, SELECT, UPDATE ON SOFVAUDTTBL TO CSAUPDTROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON SOFVAUDTTBL TO LOANCSAADMINROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON SOFVAUDTTBL TO LOANCSAANALYSTROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON SOFVAUDTTBL TO LOANCSAMANAGERROLE;

GRANT SELECT ON SOFVAUDTTBL TO LOANCSAREADALLROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON SOFVAUDTTBL TO LOANCSAREVIEWERROLE;

GRANT SELECT ON SOFVAUDTTBL TO STGCSAREADALLROLE;

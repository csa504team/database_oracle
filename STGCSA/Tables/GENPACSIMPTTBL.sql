CREATE TABLE GENPACSIMPTTBL
(
  PACSIMPTID      NUMBER,
  IMPTDT          DATE,
  IMPTUSERID      VARCHAR2(32 BYTE),
  PACSACCT        VARCHAR2(50 BYTE),
  TIMESTAMPFLD    DATE,
  CREATUSERID     VARCHAR2(32 BYTE),
  CREATDT         DATE                          DEFAULT sysdate,
  LASTUPDTUSERID  VARCHAR2(32 BYTE),
  LASTUPDTDT      DATE                          DEFAULT sysdate
)
NOLOGGING 
NOCOMPRESS 
NO INMEMORY
NOCACHE
RESULT_CACHE (MODE DEFAULT)
NOPARALLEL
MONITORING;


GRANT DELETE, INSERT, SELECT, UPDATE ON GENPACSIMPTTBL TO CSAUPDTROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON GENPACSIMPTTBL TO LOANCSAADMINROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON GENPACSIMPTTBL TO LOANCSAANALYSTROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON GENPACSIMPTTBL TO LOANCSAMANAGERROLE;

GRANT SELECT ON GENPACSIMPTTBL TO LOANCSAREADALLROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON GENPACSIMPTTBL TO LOANCSAREVIEWERROLE;

GRANT SELECT ON GENPACSIMPTTBL TO STGCSAREADALLROLE;

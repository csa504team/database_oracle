CREATE TABLE REFACHACCTCDTBL
(
  ACCTCD          VARCHAR2(10 BYTE),
  TIMESTAMPFLD    DATE,
  CREATUSERID     VARCHAR2(32 BYTE),
  CREATDT         DATE                          DEFAULT sysdate,
  LASTUPDTUSERID  VARCHAR2(32 BYTE),
  LASTUPDTDT      DATE                          DEFAULT sysdate
)
NOLOGGING 
NOCOMPRESS 
NO INMEMORY
NOCACHE
RESULT_CACHE (MODE DEFAULT)
NOPARALLEL
MONITORING;


GRANT DELETE, INSERT, SELECT, UPDATE ON REFACHACCTCDTBL TO CSAUPDTROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON REFACHACCTCDTBL TO LOANCSAADMINROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON REFACHACCTCDTBL TO LOANCSAANALYSTROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON REFACHACCTCDTBL TO LOANCSAMANAGERROLE;

GRANT SELECT ON REFACHACCTCDTBL TO LOANCSAREADALLROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON REFACHACCTCDTBL TO LOANCSAREVIEWERROLE;

GRANT SELECT ON REFACHACCTCDTBL TO STGCSAREADALLROLE;

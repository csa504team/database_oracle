CREATE TABLE MACROTBL
(
  MACROID         NUMBER,
  MACRONM         VARCHAR2(80 BYTE),
  MACRO           VARCHAR2(4000 BYTE),
  MACRODESC       VARCHAR2(255 BYTE),
  MACRODTLDESC    VARCHAR2(4000 BYTE),
  MACROINSTRCTN   VARCHAR2(4000 BYTE),
  MACROSCN        VARCHAR2(255 BYTE),
  MACROOUTPUTTBL  VARCHAR2(255 BYTE),
  CREATUSERID     VARCHAR2(32 BYTE),
  CREATDT         DATE                          DEFAULT sysdate,
  LASTUPDTUSERID  VARCHAR2(32 BYTE),
  LASTUPDTDT      DATE                          DEFAULT sysdate
)
NOLOGGING 
NOCOMPRESS 
NO INMEMORY
NOCACHE
RESULT_CACHE (MODE DEFAULT)
NOPARALLEL
MONITORING;


GRANT DELETE, INSERT, SELECT, UPDATE ON MACROTBL TO CSAUPDTROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON MACROTBL TO LOANCSAADMINROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON MACROTBL TO LOANCSAANALYSTROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON MACROTBL TO LOANCSAMANAGERROLE;

GRANT SELECT ON MACROTBL TO LOANCSAREADALLROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON MACROTBL TO LOANCSAREVIEWERROLE;

GRANT SELECT ON MACROTBL TO STGCSAREADALLROLE;

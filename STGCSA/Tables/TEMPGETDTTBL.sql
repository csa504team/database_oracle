CREATE GLOBAL TEMPORARY TABLE TEMPGETDTTBL
(
  LOANNMB         CHAR(10 BYTE),
  PREPAYDT        DATE,
  PREPAYMO        NUMBER,
  PREPAYYRNMB     CHAR(4 BYTE),
  SEMIANNDT       DATE,
  CREATUSERID     VARCHAR2(32 BYTE),
  CREATDT         DATE                          DEFAULT SYSDATE,
  LASTUPDTUSERID  VARCHAR2(32 BYTE),
  LASTUPDTDT      DATE                          DEFAULT SYSDATE
)
ON COMMIT DELETE ROWS
NOCACHE;


GRANT DELETE, INSERT, SELECT, UPDATE ON TEMPGETDTTBL TO LOANCSAADMINROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON TEMPGETDTTBL TO LOANCSAANALYSTROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON TEMPGETDTTBL TO LOANCSAMANAGERROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON TEMPGETDTTBL TO LOANCSAREADALLROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON TEMPGETDTTBL TO LOANCSAREVIEWERROLE;

GRANT SELECT ON TEMPGETDTTBL TO STGCSAREADALLROLE;

CREATE TABLE REFENTATTRTBL
(
  REFENTATTRID    NUMBER,
  ATTRNM          VARCHAR2(80 BYTE),
  ATTRTYP         VARCHAR2(255 BYTE),
  DSPLYNM         VARCHAR2(80 BYTE),
  CREATBY         NUMBER,
  CREATUSERID     VARCHAR2(32 BYTE),
  CREATDT         DATE                          DEFAULT sysdate,
  LASTUPDTUSERID  VARCHAR2(32 BYTE),
  LASTUPDTDT      DATE                          DEFAULT sysdate
)
NOLOGGING 
NOCOMPRESS 
NO INMEMORY
NOCACHE
RESULT_CACHE (MODE DEFAULT)
NOPARALLEL
MONITORING;


GRANT DELETE, INSERT, SELECT, UPDATE ON REFENTATTRTBL TO CSAUPDTROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON REFENTATTRTBL TO LOANCSAADMINROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON REFENTATTRTBL TO LOANCSAANALYSTROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON REFENTATTRTBL TO LOANCSAMANAGERROLE;

GRANT SELECT ON REFENTATTRTBL TO LOANCSAREADALLROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON REFENTATTRTBL TO LOANCSAREVIEWERROLE;

GRANT SELECT ON REFENTATTRTBL TO STGCSAREADALLROLE;

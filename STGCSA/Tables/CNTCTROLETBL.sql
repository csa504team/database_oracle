CREATE TABLE CNTCTROLETBL
(
  CDCCNTCTROLEID  NUMBER,
  CNTCTID         NUMBER,
  ROLEID          NUMBER,
  CNTCTACTV       NUMBER,
  CREATBY         NUMBER,
  CREATUSERID     VARCHAR2(32 BYTE),
  CREATDT         DATE                          DEFAULT sysdate,
  LASTUPDTUSERID  VARCHAR2(32 BYTE),
  LASTUPDTDT      DATE                          DEFAULT sysdate
)
NOLOGGING 
NOCOMPRESS 
NO INMEMORY
NOCACHE
RESULT_CACHE (MODE DEFAULT)
NOPARALLEL
MONITORING;


GRANT DELETE, INSERT, SELECT, UPDATE ON CNTCTROLETBL TO CSAUPDTROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON CNTCTROLETBL TO LOANCSAADMINROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON CNTCTROLETBL TO LOANCSAANALYSTROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON CNTCTROLETBL TO LOANCSAMANAGERROLE;

GRANT SELECT ON CNTCTROLETBL TO LOANCSAREADALLROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON CNTCTROLETBL TO LOANCSAREVIEWERROLE;

GRANT SELECT ON CNTCTROLETBL TO STGCSAREADALLROLE;

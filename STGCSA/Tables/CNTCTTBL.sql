CREATE TABLE CNTCTTBL
(
  CNTCTID             NUMBER,
  CNTCTFIRSTNM        VARCHAR2(80 BYTE),
  CNTCTLASTNM         VARCHAR2(80 BYTE),
  CNTCTEMAILADR       VARCHAR2(80 BYTE),
  CNTCTPHNNMB         VARCHAR2(30 BYTE),
  CNTCTPHNXTN         CHAR(10 BYTE),
  CNTCTFAXNMB         VARCHAR2(20 BYTE),
  CNTCTMAILADRSTR1NM  VARCHAR2(80 BYTE),
  CNTCTMAILADRSTR2NM  VARCHAR2(80 BYTE),
  CNTCTMAILADRCTYNM   VARCHAR2(80 BYTE),
  CNTCTMAILADRSTCD    CHAR(2 BYTE),
  CNTCTMAILADRZIPCD   CHAR(5 BYTE),
  CNTCTMAILADRZIP4CD  CHAR(4 BYTE),
  CNTCTTITL           VARCHAR2(255 BYTE),
  CDCREF              NUMBER,
  CREATBY             NUMBER,
  UPDTBY              NUMBER,
  ENTID               NUMBER,
  CREATUSERID         VARCHAR2(32 BYTE),
  CREATDT             DATE                      DEFAULT sysdate,
  LASTUPDTUSERID      VARCHAR2(32 BYTE),
  LASTUPDTDT          DATE                      DEFAULT sysdate
)
NOLOGGING 
NOCOMPRESS 
NO INMEMORY
NOCACHE
RESULT_CACHE (MODE DEFAULT)
NOPARALLEL
MONITORING;


GRANT DELETE, INSERT, SELECT, UPDATE ON CNTCTTBL TO CSAUPDTROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON CNTCTTBL TO LOANCSAADMINROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON CNTCTTBL TO LOANCSAANALYSTROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON CNTCTTBL TO LOANCSAMANAGERROLE;

GRANT SELECT ON CNTCTTBL TO LOANCSAREADALLROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON CNTCTTBL TO LOANCSAREVIEWERROLE;

GRANT SELECT ON CNTCTTBL TO STGCSAREADALLROLE;

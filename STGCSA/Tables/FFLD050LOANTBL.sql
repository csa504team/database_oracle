CREATE TABLE FFLD050LOANTBL
(
  FFLD050ID                 NUMBER,
  LOANDTLNMB                VARCHAR2(255 BYTE),
  LOANSTATCD                CHAR(2 BYTE),
  STATDT                    DATE,
  LOANDTLCDCNMB             CHAR(8 BYTE),
  LOANDTLBORRNM             VARCHAR2(80 BYTE),
  LOANDTLSBCNM              VARCHAR2(255 BYTE),
  FULLBORRNM                VARCHAR2(80 BYTE),
  BORRANDSBCNM              VARCHAR2(80 BYTE),
  LOANDTLBORRMAILADRSTR1NM  VARCHAR2(80 BYTE),
  LOANDTLBORRMAILADRSTR2NM  VARCHAR2(80 BYTE),
  LOANDTLBORRMAILADRCTYNM   VARCHAR2(80 BYTE),
  LOANDTLBORRMAILADRSTCD    CHAR(2 BYTE),
  LOANDTLBORRMAILADRZIPCD   CHAR(5 BYTE),
  LOANDTLBORRMAILADRZIP4CD  CHAR(4 BYTE),
  LOANDTLSBCMAILADRSTR1NM   VARCHAR2(80 BYTE),
  LOANDTLSBCMAILADRSTR2NM   VARCHAR2(80 BYTE),
  LOANDTLSBCMAILADRCTYNM    VARCHAR2(80 BYTE),
  LOANDTLSBCMAILADRSTCD     CHAR(2 BYTE),
  LOANDTLSBCMAILADRZIPCD    CHAR(5 BYTE),
  LOANDTLSBCMAILADRZIP4CD   CHAR(4 BYTE),
  CDCFEEAMT                 NUMBER(15,2),
  FEEAMT                    NUMBER(15,2),
  APPVDT                    DATE,
  ISSDT                     DATE,
  SEMIANPYMTAMT             NUMBER(15,2),
  PRINLEFTAMT               NUMBER(15,2),
  CSAFEEPCT                 NUMBER(7,5),
  MATDT                     DATE,
  NOTEINTRTPCT              NUMBER(7,5),
  DBENTRINTRTPCT            NUMBER(7,5),
  LOANDTLCMNT1              VARCHAR2(255 BYTE),
  LOANDTLCMNT2              VARCHAR2(255 BYTE),
  NETDBENTRPROCDSAMT        NUMBER(15,2),
  ORGLPRINBALAMT            NUMBER(15,2),
  LOANDTLREFIIND            CHAR(1 BYTE),
  LOANDTLPRGRMTYP           VARCHAR2(255 BYTE),
  LASTUPDT                  DATE,
  CREATUSERID               VARCHAR2(32 BYTE),
  CREATDT                   DATE                DEFAULT sysdate,
  LASTUPDTUSERID            VARCHAR2(32 BYTE),
  LASTUPDTDT                DATE                DEFAULT sysdate
)
NOLOGGING 
NOCOMPRESS 
NO INMEMORY
NOCACHE
RESULT_CACHE (MODE DEFAULT)
NOPARALLEL
MONITORING;


GRANT DELETE, INSERT, SELECT, UPDATE ON FFLD050LOANTBL TO CSAUPDTROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON FFLD050LOANTBL TO LOANCSAADMINROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON FFLD050LOANTBL TO LOANCSAANALYSTROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON FFLD050LOANTBL TO LOANCSAMANAGERROLE;

GRANT SELECT ON FFLD050LOANTBL TO LOANCSAREADALLROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON FFLD050LOANTBL TO LOANCSAREVIEWERROLE;

GRANT SELECT ON FFLD050LOANTBL TO STGCSAREADALLROLE;

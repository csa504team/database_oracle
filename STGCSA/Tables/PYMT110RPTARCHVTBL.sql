CREATE TABLE stgCSA.PYMT110RPTARCHVTBL
(
  POSTDT          DATE                          NOT NULL, 
  PRININTTOTAMT   NUMBER(15,2),
  UNALLOCAMT      NUMBER(15,2),
  REPDAMT         NUMBER(15,2),
  FEEPDAMT        NUMBER(15,2),
  LATEFEEAMT      NUMBER(15,2),
  LENDRFEEAMT     NUMBER(15,2),
  PREPAYAMT       NUMBER(15,2),
  PRCSIND         CHAR(1)                       DEFAULT 'N',
  CREATUSERID     VARCHAR2(32 BYTE),
  CREATDT         DATE                          DEFAULT sysdate,
  LASTUPDTUSERID  VARCHAR2(32 BYTE),
  LASTUPDTDT      DATE                          DEFAULT sysdate
) tablespace stgcsadatatbs;

create unique index stgcsa.pymtrpt_ux1 
  on stgCSA.PYMT110RPTARCHVTBL(postdt)
  tablespace stgcsaindtbs;

ALTER TABLE stgCSA.Pymt110RptArchvTbl
    ADD CONSTRAINT Pymt110RptArchTbl_PK PRIMARY KEY
    (postDt) 
USING INDEX stgcsa.pymtrpt_ux1;

-- grant select, insert, update, delete on stgcsa.PYMT110RPTARCHVTBL to stgCSADEVROLE;
grant select, insert, update, delete on stgcsa.PYMT110RPTARCHVTBL to stgCSAREADALLROLE;
grant select, insert, update, delete on stgcsa.PYMT110RPTARCHVTBL to LOANCSAMANAGERROLE;
grant select, insert, update, delete on stgcsa.PYMT110RPTARCHVTBL to LOANCSAREADALLROLE;
grant select, insert, update, delete on stgcsa.PYMT110RPTARCHVTBL to LOANCSAADMINROLE;
grant select, insert, update, delete on stgcsa.PYMT110RPTARCHVTBL to LOANCSAANALYSTROLE;
grant select, insert, update, delete on stgcsa.PYMT110RPTARCHVTBL to LOANCSAREVIEWERROLE;

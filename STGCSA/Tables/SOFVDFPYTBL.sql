CREATE TABLE SOFVDFPYTBL
(
  LOANNMB              CHAR(10 BYTE),
  DEFPLANNEWMOPYMTAMT  NUMBER(15,2),
  DEFPLANOPNDT         DATE,
  DEFPLANDTCLS         DATE,
  DEFPLANLASTMAINTNDT  DATE,
  DEFPLANCMNT1         VARCHAR2(80 BYTE),
  DEFPLANCMNT2         VARCHAR2(80 BYTE),
  CREATUSERID          VARCHAR2(32 BYTE),
  CREATDT              DATE                     DEFAULT sysdate,
  LASTUPDTUSERID       VARCHAR2(32 BYTE),
  LASTUPDTDT           DATE                     DEFAULT sysdate
)
NOLOGGING 
NOCOMPRESS 
NO INMEMORY
NOCACHE
RESULT_CACHE (MODE DEFAULT)
NOPARALLEL
MONITORING;


GRANT DELETE, INSERT, SELECT, UPDATE ON SOFVDFPYTBL TO CSAUPDTROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON SOFVDFPYTBL TO LOANCSAADMINROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON SOFVDFPYTBL TO LOANCSAANALYSTROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON SOFVDFPYTBL TO LOANCSAMANAGERROLE;

GRANT SELECT ON SOFVDFPYTBL TO LOANCSAREADALLROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON SOFVDFPYTBL TO LOANCSAREVIEWERROLE;

GRANT SELECT ON SOFVDFPYTBL TO STGCSAREADALLROLE;

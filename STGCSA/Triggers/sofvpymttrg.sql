create or replace trigger STGCSA.SOFVPYMTTRG
    after insert or update or delete ON STGCSA.SOFVPYMTTBL
    REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE
 /*
  Created on: 2019-05-04 08:25:41
  Created by: GENR
  Crerated from template csatrig_template v3.4 3 May 2019 on 2019-05-04 08:25:41
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
    Trigger handles errors by not updating and raising error -20100.
    Caller should handle failing SQL statement, including recording.
*/
  calling_module varchar2(80);
  tabowner varchar2(32):='STGCSA';
  tabname varchar2(32):='SOFVPYMTTBL';
  -- vars for history logging
  hst stgupdtlogtbl%rowtype;
  TYPE logdtl_typ is table of STGCSA.stgupdtlogdtltbl%rowtype;
  logdtl logdtl_typ:=logdtl_typ();
  logdtl_ctr number:=0;
procedure histlog_col_if_changed(colnam varchar2, oldval varchar2, newval varchar2) is
begin
  If (inserting and newval is not null)
    or (deleting and oldval is not null)
    or (updating and 
         (oldval<>newval 
          or (oldval is null and newval is not null)
          or (oldval is not null and newval is null)
         )
       )                  
  then 
    logdtl.extend;
    logdtl_ctr:=logdtl_ctr+1;
    logdtl(logdtl_ctr):=logdtl(1);
    logdtl(logdtl_ctr).colname:=colnam;
    if inserting or updating then 
      logdtl(logdtl_ctr).newvalue:=newval; 
    else
      logdtl(logdtl_ctr).newvalue:=null;       
    end if;  
    if updating or deleting then 
      logdtl(logdtl_ctr).oldvalue:=oldval;
    else
      logdtl(logdtl_ctr).oldvalue:=null;      
    end if;
  end if;
end;
--
--
begin
  select SYS_CONTEXT ('USERENV', 'MODULE') into calling_module from dual;
  if calling_module='Data Pump Worker' then return; end if;
  --
  -- History logging (HST)... initialize 
  -- Skip if datahist logging is explicitly turned fo via switch in runtime
  if runtime.datahist_logging_on_YN='Y' then
    SELECT LOGENTRYIDSEQ.NEXTVAL, sysdate 
      into hst.updtlogid, hst.updtdt FROM DUAL;
    -- set aside table pk, set managed variables
    hst.csa_sid:=runtime.csa_session_id;
    hst.loannmb:=null;
    hst.tablenm:='SOFVPYMTTBL';
    hst.loannmb:=nvl(:old.loannmb,:new.loannmb);
      
    hst.updtuserid:=:new.lastupdtuserid;
    if deleting=true then hst.updtuserid:=runtime.csa_userid; end if;
    if hst.updtuserid is null then hst.updtuserid:=user; end if;
    case
      when inserting=true then hst.upd_action:='I'; 
      when updating=true then hst.upd_action:='U';
      when deleting=true then hst.upd_action:='D';
    end case; 
    -- now set composit PK for row being updated  
    hst.pk:=null         
      ||nvl(:new.LOANNMB,:old.LOANNMB)
      ||to_char(nvl(:new.PYMTPOSTPYMTDT,:old.PYMTPOSTPYMTDT), 'YYYY-MM-DD HH24:MI:SS')
;  
    -- insert log main row
    INSERT INTO stgupdtlogtbl values HST;
    --
    -- Now column value logging for changed columns
    --   Initialize logdtl collection.
    logdtl.extend;
    logdtl_ctr:=1;
    logdtl(logdtl_ctr).updtlogfk:=hst.updtlogid;
    logdtl(logdtl_ctr).oldvalue:=null;
    logdtl(logdtl_ctr).newvalue:=null;
    histlog_col_if_changed('LOANNMB',
      :old.LOANNMB,:new.LOANNMB);
    histlog_col_if_changed('PYMTPOSTPYMTDT',
      to_char(:old.PYMTPOSTPYMTDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.PYMTPOSTPYMTDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('PYMTPOSTPYMTTYP',
      :old.PYMTPOSTPYMTTYP,:new.PYMTPOSTPYMTTYP);
    histlog_col_if_changed('PYMTPOSTDUEAMT',
      :old.PYMTPOSTDUEAMT,:new.PYMTPOSTDUEAMT);
    histlog_col_if_changed('PYMTPOSTREMITTEDAMT',
      :old.PYMTPOSTREMITTEDAMT,:new.PYMTPOSTREMITTEDAMT);
    histlog_col_if_changed('PYMTPOSTPYMTNMB',
      :old.PYMTPOSTPYMTNMB,:new.PYMTPOSTPYMTNMB);
    histlog_col_if_changed('PYMTPOSTCSAPAIDSTARTDTX',
      :old.PYMTPOSTCSAPAIDSTARTDTX,:new.PYMTPOSTCSAPAIDSTARTDTX);
    histlog_col_if_changed('PYMTPOSTCSAPAIDTHRUDTX',
      :old.PYMTPOSTCSAPAIDTHRUDTX,:new.PYMTPOSTCSAPAIDTHRUDTX);
    histlog_col_if_changed('PYMTPOSTCSADUEAMT',
      :old.PYMTPOSTCSADUEAMT,:new.PYMTPOSTCSADUEAMT);
    histlog_col_if_changed('PYMTPOSTCSAAPPLAMT',
      :old.PYMTPOSTCSAAPPLAMT,:new.PYMTPOSTCSAAPPLAMT);
    histlog_col_if_changed('PYMTPOSTCSAACTUALINITFEEAMT',
      :old.PYMTPOSTCSAACTUALINITFEEAMT,:new.PYMTPOSTCSAACTUALINITFEEAMT);
    histlog_col_if_changed('PYMTPOSTCSAREPDAMT',
      :old.PYMTPOSTCSAREPDAMT,:new.PYMTPOSTCSAREPDAMT);
    histlog_col_if_changed('PYMTPOSTSBAFEEREPDAMT',
      :old.PYMTPOSTSBAFEEREPDAMT,:new.PYMTPOSTSBAFEEREPDAMT);
    histlog_col_if_changed('PYMTPOSTCDCPAIDSTARTDTX',
      :old.PYMTPOSTCDCPAIDSTARTDTX,:new.PYMTPOSTCDCPAIDSTARTDTX);
    histlog_col_if_changed('PYMTPOSTCDCPAIDTHRUDTX',
      :old.PYMTPOSTCDCPAIDTHRUDTX,:new.PYMTPOSTCDCPAIDTHRUDTX);
    histlog_col_if_changed('PYMTPOSTCDCDUEAMT',
      :old.PYMTPOSTCDCDUEAMT,:new.PYMTPOSTCDCDUEAMT);
    histlog_col_if_changed('PYMTPOSTCDCAPPLAMT',
      :old.PYMTPOSTCDCAPPLAMT,:new.PYMTPOSTCDCAPPLAMT);
    histlog_col_if_changed('PYMTPOSTCDCREPDAMT',
      :old.PYMTPOSTCDCREPDAMT,:new.PYMTPOSTCDCREPDAMT);
    histlog_col_if_changed('PYMTPOSTSBAFEEAMT',
      :old.PYMTPOSTSBAFEEAMT,:new.PYMTPOSTSBAFEEAMT);
    histlog_col_if_changed('PYMTPOSTSBAFEEAPPLAMT',
      :old.PYMTPOSTSBAFEEAPPLAMT,:new.PYMTPOSTSBAFEEAPPLAMT);
    histlog_col_if_changed('PYMTPOSTINTPAIDSTARTDTX',
      :old.PYMTPOSTINTPAIDSTARTDTX,:new.PYMTPOSTINTPAIDSTARTDTX);
    histlog_col_if_changed('PYMTPOSTINTPAIDTHRUDTX',
      :old.PYMTPOSTINTPAIDTHRUDTX,:new.PYMTPOSTINTPAIDTHRUDTX);
    histlog_col_if_changed('PYMTPOSTINTBASISCALC',
      :old.PYMTPOSTINTBASISCALC,:new.PYMTPOSTINTBASISCALC);
    histlog_col_if_changed('PYMTPOSTINTRTUSEDPCT',
      :old.PYMTPOSTINTRTUSEDPCT,:new.PYMTPOSTINTRTUSEDPCT);
    histlog_col_if_changed('PYMTPOSTINTBALUSEDAMT',
      :old.PYMTPOSTINTBALUSEDAMT,:new.PYMTPOSTINTBALUSEDAMT);
    histlog_col_if_changed('PYMTPOSTINTDUEAMT',
      :old.PYMTPOSTINTDUEAMT,:new.PYMTPOSTINTDUEAMT);
    histlog_col_if_changed('PYMTPOSTINTAPPLAMT',
      :old.PYMTPOSTINTAPPLAMT,:new.PYMTPOSTINTAPPLAMT);
    histlog_col_if_changed('PYMTPOSTINTACTUALBASISDAYS',
      :old.PYMTPOSTINTACTUALBASISDAYS,:new.PYMTPOSTINTACTUALBASISDAYS);
    histlog_col_if_changed('PYMTPOSTINTACCRADJAMT',
      :old.PYMTPOSTINTACCRADJAMT,:new.PYMTPOSTINTACCRADJAMT);
    histlog_col_if_changed('PYMTPOSTINTRJCTAMT',
      :old.PYMTPOSTINTRJCTAMT,:new.PYMTPOSTINTRJCTAMT);
    histlog_col_if_changed('PYMTPOSTSBAFEESTARTDTX',
      :old.PYMTPOSTSBAFEESTARTDTX,:new.PYMTPOSTSBAFEESTARTDTX);
    histlog_col_if_changed('PYMTPOSTPRINDUEAMT',
      :old.PYMTPOSTPRINDUEAMT,:new.PYMTPOSTPRINDUEAMT);
    histlog_col_if_changed('PYMTPOSTPRINBALUSEDAMT',
      :old.PYMTPOSTPRINBALUSEDAMT,:new.PYMTPOSTPRINBALUSEDAMT);
    histlog_col_if_changed('PYMTPOSTPRINAPPLAMT',
      :old.PYMTPOSTPRINAPPLAMT,:new.PYMTPOSTPRINAPPLAMT);
    histlog_col_if_changed('PYMTPOSTPRINBALLEFTAMT',
      :old.PYMTPOSTPRINBALLEFTAMT,:new.PYMTPOSTPRINBALLEFTAMT);
    histlog_col_if_changed('PYMTPOSTPRINRJCTAMT',
      :old.PYMTPOSTPRINRJCTAMT,:new.PYMTPOSTPRINRJCTAMT);
    histlog_col_if_changed('PYMTPOSTGNTYREPAYAMT',
      :old.PYMTPOSTGNTYREPAYAMT,:new.PYMTPOSTGNTYREPAYAMT);
    histlog_col_if_changed('PYMTPOSTSBAFEEENDDTX',
      :old.PYMTPOSTSBAFEEENDDTX,:new.PYMTPOSTSBAFEEENDDTX);
    histlog_col_if_changed('PYMTPOSTPAIDBYCOLSONDTX',
      :old.PYMTPOSTPAIDBYCOLSONDTX,:new.PYMTPOSTPAIDBYCOLSONDTX);
    histlog_col_if_changed('PYMTPOSTLPYMTAMT',
      :old.PYMTPOSTLPYMTAMT,:new.PYMTPOSTLPYMTAMT);
    histlog_col_if_changed('PYMTPOSTESCRCPTSTDAMT',
      :old.PYMTPOSTESCRCPTSTDAMT,:new.PYMTPOSTESCRCPTSTDAMT);
    histlog_col_if_changed('PYMTPOSTLATEFEEASSESSAMT',
      :old.PYMTPOSTLATEFEEASSESSAMT,:new.PYMTPOSTLATEFEEASSESSAMT);
    histlog_col_if_changed('PYMTPOSTCDCDISBAMT',
      :old.PYMTPOSTCDCDISBAMT,:new.PYMTPOSTCDCDISBAMT);
    histlog_col_if_changed('PYMTPOSTCDCSBADISBAMT',
      :old.PYMTPOSTCDCSBADISBAMT,:new.PYMTPOSTCDCSBADISBAMT);
    histlog_col_if_changed('PYMTPOSTXADJCD',
      :old.PYMTPOSTXADJCD,:new.PYMTPOSTXADJCD);
    histlog_col_if_changed('PYMTPOSTLEFTOVRAMT',
      :old.PYMTPOSTLEFTOVRAMT,:new.PYMTPOSTLEFTOVRAMT);
    histlog_col_if_changed('PYMTPOSTRJCTCD',
      :old.PYMTPOSTRJCTCD,:new.PYMTPOSTRJCTCD);
    histlog_col_if_changed('PYMTPOSTCMNT',
      :old.PYMTPOSTCMNT,:new.PYMTPOSTCMNT);
    histlog_col_if_changed('PYMTPOSTREMITTEDRJCTAMT',
      :old.PYMTPOSTREMITTEDRJCTAMT,:new.PYMTPOSTREMITTEDRJCTAMT);
    histlog_col_if_changed('PYMTPOSTLEFTOVRRJCTAMT',
      :old.PYMTPOSTLEFTOVRRJCTAMT,:new.PYMTPOSTLEFTOVRRJCTAMT);
    histlog_col_if_changed('PYMTPOSTRJCTDT',
      to_char(:old.PYMTPOSTRJCTDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.PYMTPOSTRJCTDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('PYMTPOSTLATEFEEPAIDAMT',
      :old.PYMTPOSTLATEFEEPAIDAMT,:new.PYMTPOSTLATEFEEPAIDAMT);
    histlog_col_if_changed('PYMTPOSTINTWAIVEDAMT',
      :old.PYMTPOSTINTWAIVEDAMT,:new.PYMTPOSTINTWAIVEDAMT);
    histlog_col_if_changed('PYMTPOSTPREPAYAMT',
      :old.PYMTPOSTPREPAYAMT,:new.PYMTPOSTPREPAYAMT);
    histlog_col_if_changed('CREATUSERID',
      :old.CREATUSERID,:new.CREATUSERID);
    histlog_col_if_changed('CREATDT',
      to_char(:old.CREATDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CREATDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('LASTUPDTUSERID',
      :old.LASTUPDTUSERID,:new.LASTUPDTUSERID);
    histlog_col_if_changed('LASTUPDTDT',
      to_char(:old.LASTUPDTDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.LASTUPDTDT,'YYYY-MM-DD HH24:MI:SS'));
    -- write out any accumulated logdtl recs with bulk insert
    if logdtl_ctr>1 then
      forall r in 2..logdtl_ctr insert into stgcsa.stgupdtlogdtltbl values logdtl(r);
    end if;
  end if;  
  -- end of history logging section
end;

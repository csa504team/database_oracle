create or replace trigger STGCSA.IMPTCHKMSTRTRG
    after insert or update or delete ON STGCSA.IMPTCHKMSTRTBL
    REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE
 /*
  Created on: 2019-05-04 08:25:04
  Created by: GENR
  Crerated from template csatrig_template v3.4 3 May 2019 on 2019-05-04 08:25:05
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
    Trigger handles errors by not updating and raising error -20100.
    Caller should handle failing SQL statement, including recording.
*/
  calling_module varchar2(80);
  tabowner varchar2(32):='STGCSA';
  tabname varchar2(32):='IMPTCHKMSTRTBL';
  -- vars for history logging
  hst stgupdtlogtbl%rowtype;
  TYPE logdtl_typ is table of STGCSA.stgupdtlogdtltbl%rowtype;
  logdtl logdtl_typ:=logdtl_typ();
  logdtl_ctr number:=0;
procedure histlog_col_if_changed(colnam varchar2, oldval varchar2, newval varchar2) is
begin
  If (inserting and newval is not null)
    or (deleting and oldval is not null)
    or (updating and 
         (oldval<>newval 
          or (oldval is null and newval is not null)
          or (oldval is not null and newval is null)
         )
       )                  
  then 
    logdtl.extend;
    logdtl_ctr:=logdtl_ctr+1;
    logdtl(logdtl_ctr):=logdtl(1);
    logdtl(logdtl_ctr).colname:=colnam;
    if inserting or updating then 
      logdtl(logdtl_ctr).newvalue:=newval; 
    else
      logdtl(logdtl_ctr).newvalue:=null;       
    end if;  
    if updating or deleting then 
      logdtl(logdtl_ctr).oldvalue:=oldval;
    else
      logdtl(logdtl_ctr).oldvalue:=null;      
    end if;
  end if;
end;
--
--
begin
  select SYS_CONTEXT ('USERENV', 'MODULE') into calling_module from dual;
  if calling_module='Data Pump Worker' then return; end if;
  --
  -- History logging (HST)... initialize 
  -- Skip if datahist logging is explicitly turned fo via switch in runtime
  if runtime.datahist_logging_on_YN='Y' then
    SELECT LOGENTRYIDSEQ.NEXTVAL, sysdate 
      into hst.updtlogid, hst.updtdt FROM DUAL;
    -- set aside table pk, set managed variables
    hst.csa_sid:=runtime.csa_session_id;
    hst.loannmb:=null;
    hst.tablenm:='IMPTCHKMSTRTBL';
    hst.loannmb:=null;      
    hst.updtuserid:=:new.lastupdtuserid;
    if deleting=true then hst.updtuserid:=runtime.csa_userid; end if;
    if hst.updtuserid is null then hst.updtuserid:=user; end if;
    case
      when inserting=true then hst.upd_action:='I'; 
      when updating=true then hst.upd_action:='U';
      when deleting=true then hst.upd_action:='D';
    end case; 
    -- now set composit PK for row being updated  
    hst.pk:=null         
      ||rpad(to_char(nvl(:new.CHKIMPTCHKMSTRID,:old.CHKIMPTCHKMSTRID)),22)
;  
    -- insert log main row
    INSERT INTO stgupdtlogtbl values HST;
    --
    -- Now column value logging for changed columns
    --   Initialize logdtl collection.
    logdtl.extend;
    logdtl_ctr:=1;
    logdtl(logdtl_ctr).updtlogfk:=hst.updtlogid;
    logdtl(logdtl_ctr).oldvalue:=null;
    logdtl(logdtl_ctr).newvalue:=null;
    histlog_col_if_changed('CHKIMPTCHKMSTRID',
      :old.CHKIMPTCHKMSTRID,:new.CHKIMPTCHKMSTRID);
    histlog_col_if_changed('CHKPAYMTHD',
      :old.CHKPAYMTHD,:new.CHKPAYMTHD);
    histlog_col_if_changed('CHKCRDDBTFL',
      :old.CHKCRDDBTFL,:new.CHKCRDDBTFL);
    histlog_col_if_changed('CHKTRANSNMB',
      :old.CHKTRANSNMB,:new.CHKTRANSNMB);
    histlog_col_if_changed('CHKVALDT',
      to_char(:old.CHKVALDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CHKVALDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CHKPYMTAMT',
      :old.CHKPYMTAMT,:new.CHKPYMTAMT);
    histlog_col_if_changed('CHKPYMTFMTCD',
      :old.CHKPYMTFMTCD,:new.CHKPYMTFMTCD);
    histlog_col_if_changed('CHKIMPTCHKMSTRCUR',
      :old.CHKIMPTCHKMSTRCUR,:new.CHKIMPTCHKMSTRCUR);
    histlog_col_if_changed('CHKFXCONNMB',
      :old.CHKFXCONNMB,:new.CHKFXCONNMB);
    histlog_col_if_changed('CHKCEOCMPID',
      :old.CHKCEOCMPID,:new.CHKCEOCMPID);
    histlog_col_if_changed('CHKEDDHNDLCD',
      :old.CHKEDDHNDLCD,:new.CHKEDDHNDLCD);
    histlog_col_if_changed('CHKPDPHNDLCD',
      :old.CHKPDPHNDLCD,:new.CHKPDPHNDLCD);
    histlog_col_if_changed('CHKEDDBILLID',
      :old.CHKEDDBILLID,:new.CHKEDDBILLID);
    histlog_col_if_changed('CHKORGLACCTTY',
      :old.CHKORGLACCTTY,:new.CHKORGLACCTTY);
    histlog_col_if_changed('CHKORGLACCT',
      :old.CHKORGLACCT,:new.CHKORGLACCT);
    histlog_col_if_changed('CHKORGLACCTCUR',
      :old.CHKORGLACCTCUR,:new.CHKORGLACCTCUR);
    histlog_col_if_changed('CHKORGLBNKIDTY',
      :old.CHKORGLBNKIDTY,:new.CHKORGLBNKIDTY);
    histlog_col_if_changed('CHKORGLBNKID',
      :old.CHKORGLBNKID,:new.CHKORGLBNKID);
    histlog_col_if_changed('CHKRECVPARTYACCTTY',
      :old.CHKRECVPARTYACCTTY,:new.CHKRECVPARTYACCTTY);
    histlog_col_if_changed('CHKRECVPARTYACCT',
      :old.CHKRECVPARTYACCT,:new.CHKRECVPARTYACCT);
    histlog_col_if_changed('CHKRECVACCTCUR',
      :old.CHKRECVACCTCUR,:new.CHKRECVACCTCUR);
    histlog_col_if_changed('CHKRECVBNKIDTY',
      :old.CHKRECVBNKIDTY,:new.CHKRECVBNKIDTY);
    histlog_col_if_changed('CHKRECVBNKID',
      :old.CHKRECVBNKID,:new.CHKRECVBNKID);
    histlog_col_if_changed('CHKRECVBNKSECID',
      :old.CHKRECVBNKSECID,:new.CHKRECVBNKSECID);
    histlog_col_if_changed('CHKORGLTORECVPARTYINFO',
      :old.CHKORGLTORECVPARTYINFO,:new.CHKORGLTORECVPARTYINFO);
    histlog_col_if_changed('CHKEFFDT',
      to_char(:old.CHKEFFDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CHKEFFDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CHKPROCDT',
      to_char(:old.CHKPROCDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CHKPROCDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CHKORGLPARTYNM',
      :old.CHKORGLPARTYNM,:new.CHKORGLPARTYNM);
    histlog_col_if_changed('CHKORGLPARTYADDNM',
      :old.CHKORGLPARTYADDNM,:new.CHKORGLPARTYADDNM);
    histlog_col_if_changed('CHKORGLPARTYID',
      :old.CHKORGLPARTYID,:new.CHKORGLPARTYID);
    histlog_col_if_changed('CHKORGLPARTYMAILADRSTR1NM',
      :old.CHKORGLPARTYMAILADRSTR1NM,:new.CHKORGLPARTYMAILADRSTR1NM);
    histlog_col_if_changed('CHKORGLPARTYMAILADRSTR2NM',
      :old.CHKORGLPARTYMAILADRSTR2NM,:new.CHKORGLPARTYMAILADRSTR2NM);
    histlog_col_if_changed('CHKORGLPARTYMAILADRSTR3NM',
      :old.CHKORGLPARTYMAILADRSTR3NM,:new.CHKORGLPARTYMAILADRSTR3NM);
    histlog_col_if_changed('CHKORGLPARTYMAILADRCTYNM',
      :old.CHKORGLPARTYMAILADRCTYNM,:new.CHKORGLPARTYMAILADRCTYNM);
    histlog_col_if_changed('CHKORGLPARTYMAILADRSTPRO',
      :old.CHKORGLPARTYMAILADRSTPRO,:new.CHKORGLPARTYMAILADRSTPRO);
    histlog_col_if_changed('CHKORGLPARTYPMAILADRSTCD',
      :old.CHKORGLPARTYPMAILADRSTCD,:new.CHKORGLPARTYPMAILADRSTCD);
    histlog_col_if_changed('CHKORGLPARTYMAILADRCNTRYCD',
      :old.CHKORGLPARTYMAILADRCNTRYCD,:new.CHKORGLPARTYMAILADRCNTRYCD);
    histlog_col_if_changed('CHKORGLPARTYMAILADRCNTRYNM',
      :old.CHKORGLPARTYMAILADRCNTRYNM,:new.CHKORGLPARTYMAILADRCNTRYNM);
    histlog_col_if_changed('CHKPHNNMB',
      :old.CHKPHNNMB,:new.CHKPHNNMB);
    histlog_col_if_changed('CHKORGLBNKNM',
      :old.CHKORGLBNKNM,:new.CHKORGLBNKNM);
    histlog_col_if_changed('CHKORGLBNKMAILADRSTR1NM',
      :old.CHKORGLBNKMAILADRSTR1NM,:new.CHKORGLBNKMAILADRSTR1NM);
    histlog_col_if_changed('CHKORGLBNKMAILADRSTR2NM',
      :old.CHKORGLBNKMAILADRSTR2NM,:new.CHKORGLBNKMAILADRSTR2NM);
    histlog_col_if_changed('CHKORGLBNKMAILADRCTYNM',
      :old.CHKORGLBNKMAILADRCTYNM,:new.CHKORGLBNKMAILADRCTYNM);
    histlog_col_if_changed('CHKORGLBNKMAILADRSTPRO',
      :old.CHKORGLBNKMAILADRSTPRO,:new.CHKORGLBNKMAILADRSTPRO);
    histlog_col_if_changed('CHKORGLBNKPMAILADRSTCD',
      :old.CHKORGLBNKPMAILADRSTCD,:new.CHKORGLBNKPMAILADRSTCD);
    histlog_col_if_changed('CHKORGLBNKMAILADRCNTRYCD',
      :old.CHKORGLBNKMAILADRCNTRYCD,:new.CHKORGLBNKMAILADRCNTRYCD);
    histlog_col_if_changed('CHKRECVPARTYNM',
      :old.CHKRECVPARTYNM,:new.CHKRECVPARTYNM);
    histlog_col_if_changed('CHKRECVPARTYADDNM',
      :old.CHKRECVPARTYADDNM,:new.CHKRECVPARTYADDNM);
    histlog_col_if_changed('CHKRECVPARTYID',
      :old.CHKRECVPARTYID,:new.CHKRECVPARTYID);
    histlog_col_if_changed('CHKRECVPARTYMAILADRSTR1NM',
      :old.CHKRECVPARTYMAILADRSTR1NM,:new.CHKRECVPARTYMAILADRSTR1NM);
    histlog_col_if_changed('CHKRECVPARTYMAILADRSTR2NM',
      :old.CHKRECVPARTYMAILADRSTR2NM,:new.CHKRECVPARTYMAILADRSTR2NM);
    histlog_col_if_changed('CHKRECVPARTYMAILADRSTR3NM',
      :old.CHKRECVPARTYMAILADRSTR3NM,:new.CHKRECVPARTYMAILADRSTR3NM);
    histlog_col_if_changed('CHKRECVPARTYMAILADRCTYNM',
      :old.CHKRECVPARTYMAILADRCTYNM,:new.CHKRECVPARTYMAILADRCTYNM);
    histlog_col_if_changed('CHKRECVPARTYMAILADRSTPRO',
      :old.CHKRECVPARTYMAILADRSTPRO,:new.CHKRECVPARTYMAILADRSTPRO);
    histlog_col_if_changed('CHKRECVPARTYPMAILADRSTCD',
      :old.CHKRECVPARTYPMAILADRSTCD,:new.CHKRECVPARTYPMAILADRSTCD);
    histlog_col_if_changed('CHKRECVPARTYMAILADRCNTRYCD',
      :old.CHKRECVPARTYMAILADRCNTRYCD,:new.CHKRECVPARTYMAILADRCNTRYCD);
    histlog_col_if_changed('CHKRECVPARTYMAILADRCNTRYNM',
      :old.CHKRECVPARTYMAILADRCNTRYNM,:new.CHKRECVPARTYMAILADRCNTRYNM);
    histlog_col_if_changed('CHKRECVBNKNM',
      :old.CHKRECVBNKNM,:new.CHKRECVBNKNM);
    histlog_col_if_changed('CHKRECVBNKMAILADRSTR1NM',
      :old.CHKRECVBNKMAILADRSTR1NM,:new.CHKRECVBNKMAILADRSTR1NM);
    histlog_col_if_changed('CHKRECVBNKMAILADRSTR2NM',
      :old.CHKRECVBNKMAILADRSTR2NM,:new.CHKRECVBNKMAILADRSTR2NM);
    histlog_col_if_changed('CHKRECVBNKMAILADRCTYNM',
      :old.CHKRECVBNKMAILADRCTYNM,:new.CHKRECVBNKMAILADRCTYNM);
    histlog_col_if_changed('CHKRECVBNKMAILADRSTPRO',
      :old.CHKRECVBNKMAILADRSTPRO,:new.CHKRECVBNKMAILADRSTPRO);
    histlog_col_if_changed('CHKRECVBNKPMAILADRSTCD',
      :old.CHKRECVBNKPMAILADRSTCD,:new.CHKRECVBNKPMAILADRSTCD);
    histlog_col_if_changed('CHKRECVBNKMAILADRCNTRYCD',
      :old.CHKRECVBNKMAILADRCNTRYCD,:new.CHKRECVBNKMAILADRCNTRYCD);
    histlog_col_if_changed('CHKINTBNKNM',
      :old.CHKINTBNKNM,:new.CHKINTBNKNM);
    histlog_col_if_changed('CHKINTBNKIDTYP1',
      :old.CHKINTBNKIDTYP1,:new.CHKINTBNKIDTYP1);
    histlog_col_if_changed('CHKINTBNKID1',
      :old.CHKINTBNKID1,:new.CHKINTBNKID1);
    histlog_col_if_changed('CHKINTBNKMAILADRSTR1NM',
      :old.CHKINTBNKMAILADRSTR1NM,:new.CHKINTBNKMAILADRSTR1NM);
    histlog_col_if_changed('CHKINTBNKMAILADRCTY1NM',
      :old.CHKINTBNKMAILADRCTY1NM,:new.CHKINTBNKMAILADRCTY1NM);
    histlog_col_if_changed('CHKINTBNKMAILADRSTPRO1',
      :old.CHKINTBNKMAILADRSTPRO1,:new.CHKINTBNKMAILADRSTPRO1);
    histlog_col_if_changed('CHKINTBNKPMAILADRSTCD1',
      :old.CHKINTBNKPMAILADRSTCD1,:new.CHKINTBNKPMAILADRSTCD1);
    histlog_col_if_changed('CHKINTBNKMAILADRCNTRYCD1',
      :old.CHKINTBNKMAILADRCNTRYCD1,:new.CHKINTBNKMAILADRCNTRYCD1);
    histlog_col_if_changed('CHKINTBNKNM2',
      :old.CHKINTBNKNM2,:new.CHKINTBNKNM2);
    histlog_col_if_changed('CHKINTBNKIDTYP2',
      :old.CHKINTBNKIDTYP2,:new.CHKINTBNKIDTYP2);
    histlog_col_if_changed('CHKINTBNKID2',
      :old.CHKINTBNKID2,:new.CHKINTBNKID2);
    histlog_col_if_changed('CHKINTBNKMAILADRSTR2NM',
      :old.CHKINTBNKMAILADRSTR2NM,:new.CHKINTBNKMAILADRSTR2NM);
    histlog_col_if_changed('CHKINTBNKMAILADRCTY2NM',
      :old.CHKINTBNKMAILADRCTY2NM,:new.CHKINTBNKMAILADRCTY2NM);
    histlog_col_if_changed('CHKINTBNKMAILADRSTPRO2',
      :old.CHKINTBNKMAILADRSTPRO2,:new.CHKINTBNKMAILADRSTPRO2);
    histlog_col_if_changed('CHKINTBNKPMAILADRSTCD2',
      :old.CHKINTBNKPMAILADRSTCD2,:new.CHKINTBNKPMAILADRSTCD2);
    histlog_col_if_changed('CHKINTBNKMAILADRCNTRYCD2',
      :old.CHKINTBNKMAILADRCNTRYCD2,:new.CHKINTBNKMAILADRCNTRYCD2);
    histlog_col_if_changed('CHKORDPARTYNM',
      :old.CHKORDPARTYNM,:new.CHKORDPARTYNM);
    histlog_col_if_changed('CHKORDPARTYID',
      :old.CHKORDPARTYID,:new.CHKORDPARTYID);
    histlog_col_if_changed('CHKORDPARTYMAILADRSTR1NM',
      :old.CHKORDPARTYMAILADRSTR1NM,:new.CHKORDPARTYMAILADRSTR1NM);
    histlog_col_if_changed('CHKORDPARTYMAILADRSTR2NM',
      :old.CHKORDPARTYMAILADRSTR2NM,:new.CHKORDPARTYMAILADRSTR2NM);
    histlog_col_if_changed('CHKORDPARTYMAILADRCTYNM',
      :old.CHKORDPARTYMAILADRCTYNM,:new.CHKORDPARTYMAILADRCTYNM);
    histlog_col_if_changed('CHKORDPARTYMAILADRSTPRO',
      :old.CHKORDPARTYMAILADRSTPRO,:new.CHKORDPARTYMAILADRSTPRO);
    histlog_col_if_changed('CHKORDPARTYPMAILADRSTCD',
      :old.CHKORDPARTYPMAILADRSTCD,:new.CHKORDPARTYPMAILADRSTCD);
    histlog_col_if_changed('CHKORDPARTYMAILADRCNTRYCD',
      :old.CHKORDPARTYMAILADRCNTRYCD,:new.CHKORDPARTYMAILADRCNTRYCD);
    histlog_col_if_changed('CHKORDPARTYMAILADRCNTRYNM',
      :old.CHKORDPARTYMAILADRCNTRYNM,:new.CHKORDPARTYMAILADRCNTRYNM);
    histlog_col_if_changed('CHKDELPARTYNM',
      :old.CHKDELPARTYNM,:new.CHKDELPARTYNM);
    histlog_col_if_changed('CHKDELPARTYADDNM',
      :old.CHKDELPARTYADDNM,:new.CHKDELPARTYADDNM);
    histlog_col_if_changed('CHKDELPARTYID',
      :old.CHKDELPARTYID,:new.CHKDELPARTYID);
    histlog_col_if_changed('CHKDELPARTYMAILADRSTR1NM',
      :old.CHKDELPARTYMAILADRSTR1NM,:new.CHKDELPARTYMAILADRSTR1NM);
    histlog_col_if_changed('CHKDELPARTYMAILADRSTR2NM',
      :old.CHKDELPARTYMAILADRSTR2NM,:new.CHKDELPARTYMAILADRSTR2NM);
    histlog_col_if_changed('CHKDELPARTYMAILADRSTR3NM',
      :old.CHKDELPARTYMAILADRSTR3NM,:new.CHKDELPARTYMAILADRSTR3NM);
    histlog_col_if_changed('CHKDELPARTYMAILADRCTYNM',
      :old.CHKDELPARTYMAILADRCTYNM,:new.CHKDELPARTYMAILADRCTYNM);
    histlog_col_if_changed('CHKDELPARTYMAILADRSTPRO',
      :old.CHKDELPARTYMAILADRSTPRO,:new.CHKDELPARTYMAILADRSTPRO);
    histlog_col_if_changed('CHKDELPARTYPMAILADRSTCD',
      :old.CHKDELPARTYPMAILADRSTCD,:new.CHKDELPARTYPMAILADRSTCD);
    histlog_col_if_changed('CHKDELPARTYMAILADRCNTRYCD',
      :old.CHKDELPARTYMAILADRCNTRYCD,:new.CHKDELPARTYMAILADRCNTRYCD);
    histlog_col_if_changed('CHKDELPARTYMAILADRCNTRYNM',
      :old.CHKDELPARTYMAILADRCNTRYNM,:new.CHKDELPARTYMAILADRCNTRYNM);
    histlog_col_if_changed('CHKNMB',
      :old.CHKNMB,:new.CHKNMB);
    histlog_col_if_changed('CHKDOCTMPLNMB',
      :old.CHKDOCTMPLNMB,:new.CHKDOCTMPLNMB);
    histlog_col_if_changed('CHKDELCD',
      :old.CHKDELCD,:new.CHKDELCD);
    histlog_col_if_changed('CHKCOURNM',
      :old.CHKCOURNM,:new.CHKCOURNM);
    histlog_col_if_changed('CHKCOURACCT',
      :old.CHKCOURACCT,:new.CHKCOURACCT);
    histlog_col_if_changed('CHKDELRTNLOCCD',
      :old.CHKDELRTNLOCCD,:new.CHKDELRTNLOCCD);
    histlog_col_if_changed('CHKCHKIMGID',
      :old.CHKCHKIMGID,:new.CHKCHKIMGID);
    histlog_col_if_changed('CHKCHKIMGDESC',
      :old.CHKCHKIMGDESC,:new.CHKCHKIMGDESC);
    histlog_col_if_changed('CHKRTNPARTYNM',
      :old.CHKRTNPARTYNM,:new.CHKRTNPARTYNM);
    histlog_col_if_changed('CHKRTNPARTYADDNM',
      :old.CHKRTNPARTYADDNM,:new.CHKRTNPARTYADDNM);
    histlog_col_if_changed('CHKRTNPARTYID',
      :old.CHKRTNPARTYID,:new.CHKRTNPARTYID);
    histlog_col_if_changed('CHKRTNPARTYMAILADRSTR1NM',
      :old.CHKRTNPARTYMAILADRSTR1NM,:new.CHKRTNPARTYMAILADRSTR1NM);
    histlog_col_if_changed('CHKRTNPARTYMAILADRSTR2NM',
      :old.CHKRTNPARTYMAILADRSTR2NM,:new.CHKRTNPARTYMAILADRSTR2NM);
    histlog_col_if_changed('CHKRTNPARTYMAILADRSTR3NM',
      :old.CHKRTNPARTYMAILADRSTR3NM,:new.CHKRTNPARTYMAILADRSTR3NM);
    histlog_col_if_changed('CHKRTNPARTYMAILADRCTYNM',
      :old.CHKRTNPARTYMAILADRCTYNM,:new.CHKRTNPARTYMAILADRCTYNM);
    histlog_col_if_changed('CHKRTNPARTYMAILADRSTPRO',
      :old.CHKRTNPARTYMAILADRSTPRO,:new.CHKRTNPARTYMAILADRSTPRO);
    histlog_col_if_changed('CHKRTNPARTYPMAILADRSTCD',
      :old.CHKRTNPARTYPMAILADRSTCD,:new.CHKRTNPARTYPMAILADRSTCD);
    histlog_col_if_changed('CHKRTNPARTYMAILADRCNTRYCD',
      :old.CHKRTNPARTYMAILADRCNTRYCD,:new.CHKRTNPARTYMAILADRCNTRYCD);
    histlog_col_if_changed('CHKRTNPARTYMAILADRCNTRYNM',
      :old.CHKRTNPARTYMAILADRCNTRYNM,:new.CHKRTNPARTYMAILADRCNTRYNM);
    histlog_col_if_changed('CHKACHCMPID',
      :old.CHKACHCMPID,:new.CHKACHCMPID);
    histlog_col_if_changed('CHKFXTYP',
      :old.CHKFXTYP,:new.CHKFXTYP);
    histlog_col_if_changed('CHKACHINTFMTCD',
      :old.CHKACHINTFMTCD,:new.CHKACHINTFMTCD);
    histlog_col_if_changed('CHKSEPAREFID',
      :old.CHKSEPAREFID,:new.CHKSEPAREFID);
    histlog_col_if_changed('CHKWIRECHGIND',
      :old.CHKWIRECHGIND,:new.CHKWIRECHGIND);
    histlog_col_if_changed('CHKCEOTMPLID',
      :old.CHKCEOTMPLID,:new.CHKCEOTMPLID);
    histlog_col_if_changed('CHKBATID',
      :old.CHKBATID,:new.CHKBATID);
    histlog_col_if_changed('CHKOPRID',
      :old.CHKOPRID,:new.CHKOPRID);
    histlog_col_if_changed('CHKBNKTOBNKINFO',
      :old.CHKBNKTOBNKINFO,:new.CHKBNKTOBNKINFO);
    histlog_col_if_changed('CHKCCREXPDT',
      to_char(:old.CHKCCREXPDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CHKCCREXPDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CHKCCRPAYEETYP',
      :old.CHKCCRPAYEETYP,:new.CHKCCRPAYEETYP);
    histlog_col_if_changed('CHKCCRMERCHNTID',
      :old.CHKCCRMERCHNTID,:new.CHKCCRMERCHNTID);
    histlog_col_if_changed('CHKCCRMCCCD',
      :old.CHKCCRMCCCD,:new.CHKCCRMCCCD);
    histlog_col_if_changed('CHKPAYEEEMAILADR',
      :old.CHKPAYEEEMAILADR,:new.CHKPAYEEEMAILADR);
    histlog_col_if_changed('CHKCCRDIVISN',
      :old.CHKCCRDIVISN,:new.CHKCCRDIVISN);
    histlog_col_if_changed('CHKFILEFRMT',
      :old.CHKFILEFRMT,:new.CHKFILEFRMT);
    histlog_col_if_changed('CHKDELTYP1',
      :old.CHKDELTYP1,:new.CHKDELTYP1);
    histlog_col_if_changed('CHKDELTYP2',
      :old.CHKDELTYP2,:new.CHKDELTYP2);
    histlog_col_if_changed('CHKDELTYP3',
      :old.CHKDELTYP3,:new.CHKDELTYP3);
    histlog_col_if_changed('CHKDELCNM',
      :old.CHKDELCNM,:new.CHKDELCNM);
    histlog_col_if_changed('CHKDELNM2',
      :old.CHKDELNM2,:new.CHKDELNM2);
    histlog_col_if_changed('CHKDELNM3',
      :old.CHKDELNM3,:new.CHKDELNM3);
    histlog_col_if_changed('CHKDEL1FAXNMB',
      :old.CHKDEL1FAXNMB,:new.CHKDEL1FAXNMB);
    histlog_col_if_changed('CHKDEL2FAXNMB',
      :old.CHKDEL2FAXNMB,:new.CHKDEL2FAXNMB);
    histlog_col_if_changed('CHKDEL3FAXNMB',
      :old.CHKDEL3FAXNMB,:new.CHKDEL3FAXNMB);
    histlog_col_if_changed('CHKDELEMAILADR1',
      :old.CHKDELEMAILADR1,:new.CHKDELEMAILADR1);
    histlog_col_if_changed('CHKDELEMAILADR2',
      :old.CHKDELEMAILADR2,:new.CHKDELEMAILADR2);
    histlog_col_if_changed('CHKDELEMAILADR3',
      :old.CHKDELEMAILADR3,:new.CHKDELEMAILADR3);
    histlog_col_if_changed('CHKDELUSERID1',
      :old.CHKDELUSERID1,:new.CHKDELUSERID1);
    histlog_col_if_changed('CHKDELUSERID2',
      :old.CHKDELUSERID2,:new.CHKDELUSERID2);
    histlog_col_if_changed('CHKDELUSERID3',
      :old.CHKDELUSERID3,:new.CHKDELUSERID3);
    histlog_col_if_changed('CHKDELCOID1',
      :old.CHKDELCOID1,:new.CHKDELCOID1);
    histlog_col_if_changed('CHKDELCOID2',
      :old.CHKDELCOID2,:new.CHKDELCOID2);
    histlog_col_if_changed('CHKDELCOID3',
      :old.CHKDELCOID3,:new.CHKDELCOID3);
    histlog_col_if_changed('CHKSECTYP1',
      :old.CHKSECTYP1,:new.CHKSECTYP1);
    histlog_col_if_changed('CHKSECTYP2',
      :old.CHKSECTYP2,:new.CHKSECTYP2);
    histlog_col_if_changed('CHKSECTYP3',
      :old.CHKSECTYP3,:new.CHKSECTYP3);
    histlog_col_if_changed('CHKSECQ11',
      :old.CHKSECQ11,:new.CHKSECQ11);
    histlog_col_if_changed('CHKSECQ12',
      :old.CHKSECQ12,:new.CHKSECQ12);
    histlog_col_if_changed('CHKSECQ13',
      :old.CHKSECQ13,:new.CHKSECQ13);
    histlog_col_if_changed('CHKSECQ21',
      :old.CHKSECQ21,:new.CHKSECQ21);
    histlog_col_if_changed('CHKSECQ22',
      :old.CHKSECQ22,:new.CHKSECQ22);
    histlog_col_if_changed('CHKSECQ23',
      :old.CHKSECQ23,:new.CHKSECQ23);
    histlog_col_if_changed('CHKSECPWD11',
      :old.CHKSECPWD11,:new.CHKSECPWD11);
    histlog_col_if_changed('CHKSECPWD12',
      :old.CHKSECPWD12,:new.CHKSECPWD12);
    histlog_col_if_changed('CHKSECPWD13',
      :old.CHKSECPWD13,:new.CHKSECPWD13);
    histlog_col_if_changed('CHKSECPWD21',
      :old.CHKSECPWD21,:new.CHKSECPWD21);
    histlog_col_if_changed('CHKSECPWD22',
      :old.CHKSECPWD22,:new.CHKSECPWD22);
    histlog_col_if_changed('CHKSECPWD23',
      :old.CHKSECPWD23,:new.CHKSECPWD23);
    histlog_col_if_changed('CHKEDDRECVMAILADRSTR11NM',
      :old.CHKEDDRECVMAILADRSTR11NM,:new.CHKEDDRECVMAILADRSTR11NM);
    histlog_col_if_changed('CHKEDDRECVMAILADRSTR12NM',
      :old.CHKEDDRECVMAILADRSTR12NM,:new.CHKEDDRECVMAILADRSTR12NM);
    histlog_col_if_changed('CHKEDDRECVMAILADRSTR13NM',
      :old.CHKEDDRECVMAILADRSTR13NM,:new.CHKEDDRECVMAILADRSTR13NM);
    histlog_col_if_changed('CHKEDDRECVMAILADRSTR21NM',
      :old.CHKEDDRECVMAILADRSTR21NM,:new.CHKEDDRECVMAILADRSTR21NM);
    histlog_col_if_changed('CHKEDDRECVMAILADRSTR22NM',
      :old.CHKEDDRECVMAILADRSTR22NM,:new.CHKEDDRECVMAILADRSTR22NM);
    histlog_col_if_changed('CHKEDDRECVMAILADRSTR23NM',
      :old.CHKEDDRECVMAILADRSTR23NM,:new.CHKEDDRECVMAILADRSTR23NM);
    histlog_col_if_changed('CHKEDDRECVMAILADRCTY1NM',
      :old.CHKEDDRECVMAILADRCTY1NM,:new.CHKEDDRECVMAILADRCTY1NM);
    histlog_col_if_changed('CHKEDDRECVMAILADRCTY2NM',
      :old.CHKEDDRECVMAILADRCTY2NM,:new.CHKEDDRECVMAILADRCTY2NM);
    histlog_col_if_changed('CHKEDDRECVMAILADRCTY3NM',
      :old.CHKEDDRECVMAILADRCTY3NM,:new.CHKEDDRECVMAILADRCTY3NM);
    histlog_col_if_changed('CHKEDDRECVMAILADRST1',
      :old.CHKEDDRECVMAILADRST1,:new.CHKEDDRECVMAILADRST1);
    histlog_col_if_changed('CHKEDDRECVMAILADRST2',
      :old.CHKEDDRECVMAILADRST2,:new.CHKEDDRECVMAILADRST2);
    histlog_col_if_changed('CHKEDDRECVMAILADRST3',
      :old.CHKEDDRECVMAILADRST3,:new.CHKEDDRECVMAILADRST3);
    histlog_col_if_changed('CHKEDDRECVPMAILADRSTCD1',
      :old.CHKEDDRECVPMAILADRSTCD1,:new.CHKEDDRECVPMAILADRSTCD1);
    histlog_col_if_changed('CHKEDDRECVPMAILADRSTCD2',
      :old.CHKEDDRECVPMAILADRSTCD2,:new.CHKEDDRECVPMAILADRSTCD2);
    histlog_col_if_changed('CHKEDDRECVPMAILADRSTCD3',
      :old.CHKEDDRECVPMAILADRSTCD3,:new.CHKEDDRECVPMAILADRSTCD3);
    histlog_col_if_changed('CHKEDDRECVMAILADRCTRY1NM',
      :old.CHKEDDRECVMAILADRCTRY1NM,:new.CHKEDDRECVMAILADRCTRY1NM);
    histlog_col_if_changed('CHKEDDRECVMAILADRCTRY2NM',
      :old.CHKEDDRECVMAILADRCTRY2NM,:new.CHKEDDRECVMAILADRCTRY2NM);
    histlog_col_if_changed('CHKEDDRECVMAILADRCTRY3NM',
      :old.CHKEDDRECVMAILADRCTRY3NM,:new.CHKEDDRECVMAILADRCTRY3NM);
    histlog_col_if_changed('CHKEDDDLVRCD1',
      :old.CHKEDDDLVRCD1,:new.CHKEDDDLVRCD1);
    histlog_col_if_changed('CHKEDDDLVRCD2',
      :old.CHKEDDDLVRCD2,:new.CHKEDDDLVRCD2);
    histlog_col_if_changed('CHKEDDDLVRCD3',
      :old.CHKEDDDLVRCD3,:new.CHKEDDDLVRCD3);
    histlog_col_if_changed('CHKEDDCOURNMB1',
      :old.CHKEDDCOURNMB1,:new.CHKEDDCOURNMB1);
    histlog_col_if_changed('CHKEDDCOURNMB2',
      :old.CHKEDDCOURNMB2,:new.CHKEDDCOURNMB2);
    histlog_col_if_changed('CHKEDDCOURNMB3',
      :old.CHKEDDCOURNMB3,:new.CHKEDDCOURNMB3);
    histlog_col_if_changed('CHKINVNMB',
      :old.CHKINVNMB,:new.CHKINVNMB);
    histlog_col_if_changed('CHKINVDT',
      to_char(:old.CHKINVDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CHKINVDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CHKINVDESC',
      :old.CHKINVDESC,:new.CHKINVDESC);
    histlog_col_if_changed('CHKINVNET',
      :old.CHKINVNET,:new.CHKINVNET);
    histlog_col_if_changed('CHKINVGRS',
      :old.CHKINVGRS,:new.CHKINVGRS);
    histlog_col_if_changed('CHKINVDIST',
      :old.CHKINVDIST,:new.CHKINVDIST);
    histlog_col_if_changed('CHKPONMB',
      :old.CHKPONMB,:new.CHKPONMB);
    histlog_col_if_changed('CHKINVTYP',
      :old.CHKINVTYP,:new.CHKINVTYP);
    histlog_col_if_changed('CHKFLXNISTYP1',
      :old.CHKFLXNISTYP1,:new.CHKFLXNISTYP1);
    histlog_col_if_changed('CHKFLXNISTYP2',
      :old.CHKFLXNISTYP2,:new.CHKFLXNISTYP2);
    histlog_col_if_changed('CHKFLXINSTYP3',
      :old.CHKFLXINSTYP3,:new.CHKFLXINSTYP3);
    histlog_col_if_changed('CHKFLXINSID1',
      :old.CHKFLXINSID1,:new.CHKFLXINSID1);
    histlog_col_if_changed('CHKFLXINSID2',
      :old.CHKFLXINSID2,:new.CHKFLXINSID2);
    histlog_col_if_changed('CHKFLXINSID3',
      :old.CHKFLXINSID3,:new.CHKFLXINSID3);
    histlog_col_if_changed('CHKCCRFACNM',
      :old.CHKCCRFACNM,:new.CHKCCRFACNM);
    histlog_col_if_changed('CHKPODESC',
      :old.CHKPODESC,:new.CHKPODESC);
    histlog_col_if_changed('CREATUSERID',
      :old.CREATUSERID,:new.CREATUSERID);
    histlog_col_if_changed('CREATDT',
      to_char(:old.CREATDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CREATDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('LASTUPDTUSERID',
      :old.LASTUPDTUSERID,:new.LASTUPDTUSERID);
    histlog_col_if_changed('LASTUPDTDT',
      to_char(:old.LASTUPDTDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.LASTUPDTDT,'YYYY-MM-DD HH24:MI:SS'));
    -- write out any accumulated logdtl recs with bulk insert
    if logdtl_ctr>1 then
      forall r in 2..logdtl_ctr insert into stgcsa.stgupdtlogdtltbl values logdtl(r);
    end if;
  end if;  
  -- end of history logging section
end;

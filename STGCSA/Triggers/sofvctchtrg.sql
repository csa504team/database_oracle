create or replace trigger STGCSA.SOFVCTCHTRG
    after insert or update or delete ON STGCSA.SOFVCTCHTBL
    REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE
 /*
  Created on: 2019-05-04 08:25:26
  Created by: GENR
  Crerated from template csatrig_template v3.4 3 May 2019 on 2019-05-04 08:25:26
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
    Trigger handles errors by not updating and raising error -20100.
    Caller should handle failing SQL statement, including recording.
*/
  calling_module varchar2(80);
  tabowner varchar2(32):='STGCSA';
  tabname varchar2(32):='SOFVCTCHTBL';
  -- vars for history logging
  hst stgupdtlogtbl%rowtype;
  TYPE logdtl_typ is table of STGCSA.stgupdtlogdtltbl%rowtype;
  logdtl logdtl_typ:=logdtl_typ();
  logdtl_ctr number:=0;
procedure histlog_col_if_changed(colnam varchar2, oldval varchar2, newval varchar2) is
begin
  If (inserting and newval is not null)
    or (deleting and oldval is not null)
    or (updating and 
         (oldval<>newval 
          or (oldval is null and newval is not null)
          or (oldval is not null and newval is null)
         )
       )                  
  then 
    logdtl.extend;
    logdtl_ctr:=logdtl_ctr+1;
    logdtl(logdtl_ctr):=logdtl(1);
    logdtl(logdtl_ctr).colname:=colnam;
    if inserting or updating then 
      logdtl(logdtl_ctr).newvalue:=newval; 
    else
      logdtl(logdtl_ctr).newvalue:=null;       
    end if;  
    if updating or deleting then 
      logdtl(logdtl_ctr).oldvalue:=oldval;
    else
      logdtl(logdtl_ctr).oldvalue:=null;      
    end if;
  end if;
end;
--
--
begin
  select SYS_CONTEXT ('USERENV', 'MODULE') into calling_module from dual;
  if calling_module='Data Pump Worker' then return; end if;
  --
  -- History logging (HST)... initialize 
  -- Skip if datahist logging is explicitly turned fo via switch in runtime
  if runtime.datahist_logging_on_YN='Y' then
    SELECT LOGENTRYIDSEQ.NEXTVAL, sysdate 
      into hst.updtlogid, hst.updtdt FROM DUAL;
    -- set aside table pk, set managed variables
    hst.csa_sid:=runtime.csa_session_id;
    hst.loannmb:=null;
    hst.tablenm:='SOFVCTCHTBL';
    hst.loannmb:=null;      
    hst.updtuserid:=:new.lastupdtuserid;
    if deleting=true then hst.updtuserid:=runtime.csa_userid; end if;
    if hst.updtuserid is null then hst.updtuserid:=user; end if;
    case
      when inserting=true then hst.upd_action:='I'; 
      when updating=true then hst.upd_action:='U';
      when deleting=true then hst.upd_action:='D';
    end case; 
    -- now set composit PK for row being updated  
    hst.pk:=null         
      ||nvl(:new.CATCHUPPLANSBANMB,:old.CATCHUPPLANSBANMB)
;  
    -- insert log main row
    INSERT INTO stgupdtlogtbl values HST;
    --
    -- Now column value logging for changed columns
    --   Initialize logdtl collection.
    logdtl.extend;
    logdtl_ctr:=1;
    logdtl(logdtl_ctr).updtlogfk:=hst.updtlogid;
    logdtl(logdtl_ctr).oldvalue:=null;
    logdtl(logdtl_ctr).newvalue:=null;
    histlog_col_if_changed('CATCHUPPLANSBANMB',
      :old.CATCHUPPLANSBANMB,:new.CATCHUPPLANSBANMB);
    histlog_col_if_changed('CATCHUPPLANSTRTDT1',
      to_char(:old.CATCHUPPLANSTRTDT1,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CATCHUPPLANSTRTDT1,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CATCHUPPLANENDDT1',
      to_char(:old.CATCHUPPLANENDDT1,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CATCHUPPLANENDDT1,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CATCHUPPLANRQ1AMT',
      :old.CATCHUPPLANRQ1AMT,:new.CATCHUPPLANRQ1AMT);
    histlog_col_if_changed('CATCHUPPLANSTRTDT2',
      to_char(:old.CATCHUPPLANSTRTDT2,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CATCHUPPLANSTRTDT2,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CATCHUPPLANENDDT2',
      to_char(:old.CATCHUPPLANENDDT2,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CATCHUPPLANENDDT2,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CATCHUPPLANRQ2AMT',
      :old.CATCHUPPLANRQ2AMT,:new.CATCHUPPLANRQ2AMT);
    histlog_col_if_changed('CATCHUPPLANSTRTDT3',
      to_char(:old.CATCHUPPLANSTRTDT3,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CATCHUPPLANSTRTDT3,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CATCHUPPLANENDDT3',
      to_char(:old.CATCHUPPLANENDDT3,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CATCHUPPLANENDDT3,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CATCHUPPLANRQ3AMT',
      :old.CATCHUPPLANRQ3AMT,:new.CATCHUPPLANRQ3AMT);
    histlog_col_if_changed('CATCHUPPLANSTRTDT4',
      to_char(:old.CATCHUPPLANSTRTDT4,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CATCHUPPLANSTRTDT4,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CATCHUPPLANENDDT4',
      to_char(:old.CATCHUPPLANENDDT4,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CATCHUPPLANENDDT4,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CATCHUPPLANRQ4AMT',
      :old.CATCHUPPLANRQ4AMT,:new.CATCHUPPLANRQ4AMT);
    histlog_col_if_changed('CATCHUPPLANSTRTDT5',
      to_char(:old.CATCHUPPLANSTRTDT5,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CATCHUPPLANSTRTDT5,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CATCHUPPLANENDDT5',
      to_char(:old.CATCHUPPLANENDDT5,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CATCHUPPLANENDDT5,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CATCHUPPLANRQ5AMT',
      :old.CATCHUPPLANRQ5AMT,:new.CATCHUPPLANRQ5AMT);
    histlog_col_if_changed('CATCHUPPLANSTRTDT6',
      to_char(:old.CATCHUPPLANSTRTDT6,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CATCHUPPLANSTRTDT6,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CATCHUPPLANENDDT6',
      to_char(:old.CATCHUPPLANENDDT6,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CATCHUPPLANENDDT6,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CATCHUPPLANRQ6AMT',
      :old.CATCHUPPLANRQ6AMT,:new.CATCHUPPLANRQ6AMT);
    histlog_col_if_changed('CATCHUPPLANSTRTDT7',
      to_char(:old.CATCHUPPLANSTRTDT7,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CATCHUPPLANSTRTDT7,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CATCHUPPLANENDDT7',
      to_char(:old.CATCHUPPLANENDDT7,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CATCHUPPLANENDDT7,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CATCHUPPLANRQ7AMT',
      :old.CATCHUPPLANRQ7AMT,:new.CATCHUPPLANRQ7AMT);
    histlog_col_if_changed('CATCHUPPLANSTRTDT8',
      to_char(:old.CATCHUPPLANSTRTDT8,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CATCHUPPLANSTRTDT8,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CATCHUPPLANENDDT8',
      to_char(:old.CATCHUPPLANENDDT8,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CATCHUPPLANENDDT8,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CATCHUPPLANRQ8AMT',
      :old.CATCHUPPLANRQ8AMT,:new.CATCHUPPLANRQ8AMT);
    histlog_col_if_changed('CATCHUPPLANSTRTDT9',
      to_char(:old.CATCHUPPLANSTRTDT9,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CATCHUPPLANSTRTDT9,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CATCHUPPLANENDDT9',
      to_char(:old.CATCHUPPLANENDDT9,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CATCHUPPLANENDDT9,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CATCHUPPLANRQ9AMT',
      :old.CATCHUPPLANRQ9AMT,:new.CATCHUPPLANRQ9AMT);
    histlog_col_if_changed('CATCHUPPLANSTRTDT10',
      to_char(:old.CATCHUPPLANSTRTDT10,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CATCHUPPLANSTRTDT10,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CATCHUPPLANENDDT10',
      to_char(:old.CATCHUPPLANENDDT10,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CATCHUPPLANENDDT10,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CATCHUPPLANRQ10AMT',
      :old.CATCHUPPLANRQ10AMT,:new.CATCHUPPLANRQ10AMT);
    histlog_col_if_changed('CATCHUPPLANSTRTDT11',
      to_char(:old.CATCHUPPLANSTRTDT11,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CATCHUPPLANSTRTDT11,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CATCHUPPLANENDDT11',
      to_char(:old.CATCHUPPLANENDDT11,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CATCHUPPLANENDDT11,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CATCHUPPLANRQ11AMT',
      :old.CATCHUPPLANRQ11AMT,:new.CATCHUPPLANRQ11AMT);
    histlog_col_if_changed('CATCHUPPLANSTRTDT12',
      to_char(:old.CATCHUPPLANSTRTDT12,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CATCHUPPLANSTRTDT12,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CATCHUPPLANENDDT12',
      to_char(:old.CATCHUPPLANENDDT12,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CATCHUPPLANENDDT12,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CATCHUPPLANRQ12AMT',
      :old.CATCHUPPLANRQ12AMT,:new.CATCHUPPLANRQ12AMT);
    histlog_col_if_changed('CATCHUPPLANSTRTDT13',
      to_char(:old.CATCHUPPLANSTRTDT13,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CATCHUPPLANSTRTDT13,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CATCHUPPLANENDDT13',
      to_char(:old.CATCHUPPLANENDDT13,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CATCHUPPLANENDDT13,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CATCHUPPLANRQ13AMT',
      :old.CATCHUPPLANRQ13AMT,:new.CATCHUPPLANRQ13AMT);
    histlog_col_if_changed('CATCHUPPLANSTRTDT14',
      to_char(:old.CATCHUPPLANSTRTDT14,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CATCHUPPLANSTRTDT14,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CATCHUPPLANENDDT14',
      to_char(:old.CATCHUPPLANENDDT14,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CATCHUPPLANENDDT14,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CATCHUPPLANRQ14AMT',
      :old.CATCHUPPLANRQ14AMT,:new.CATCHUPPLANRQ14AMT);
    histlog_col_if_changed('CATCHUPPLANSTRTDT15',
      to_char(:old.CATCHUPPLANSTRTDT15,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CATCHUPPLANSTRTDT15,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CATCHUPPLANENDDT15',
      to_char(:old.CATCHUPPLANENDDT15,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CATCHUPPLANENDDT15,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CATCHUPPLANRQ15AMT',
      :old.CATCHUPPLANRQ15AMT,:new.CATCHUPPLANRQ15AMT);
    histlog_col_if_changed('CATCHUPPLANCREDT',
      to_char(:old.CATCHUPPLANCREDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CATCHUPPLANCREDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CATCHUPPLANMODDT',
      to_char(:old.CATCHUPPLANMODDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CATCHUPPLANMODDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CREATUSERID',
      :old.CREATUSERID,:new.CREATUSERID);
    histlog_col_if_changed('CREATDT',
      to_char(:old.CREATDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CREATDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('LASTUPDTUSERID',
      :old.LASTUPDTUSERID,:new.LASTUPDTUSERID);
    histlog_col_if_changed('LASTUPDTDT',
      to_char(:old.LASTUPDTDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.LASTUPDTDT,'YYYY-MM-DD HH24:MI:SS'));
    -- write out any accumulated logdtl recs with bulk insert
    if logdtl_ctr>1 then
      forall r in 2..logdtl_ctr insert into stgcsa.stgupdtlogdtltbl values logdtl(r);
    end if;
  end if;  
  -- end of history logging section
end;

create or replace trigger STGCSA.REFSBAOFCTRG
    after insert or update or delete ON STGCSA.REFSBAOFCTBL
    REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE
 /*
  Created on: 2019-05-04 08:25:18
  Created by: GENR
  Crerated from template csatrig_template v3.4 3 May 2019 on 2019-05-04 08:25:18
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
    Trigger handles errors by not updating and raising error -20100.
    Caller should handle failing SQL statement, including recording.
*/
  calling_module varchar2(80);
  tabowner varchar2(32):='STGCSA';
  tabname varchar2(32):='REFSBAOFCTBL';
  -- vars for history logging
  hst stgupdtlogtbl%rowtype;
  TYPE logdtl_typ is table of STGCSA.stgupdtlogdtltbl%rowtype;
  logdtl logdtl_typ:=logdtl_typ();
  logdtl_ctr number:=0;
procedure histlog_col_if_changed(colnam varchar2, oldval varchar2, newval varchar2) is
begin
  If (inserting and newval is not null)
    or (deleting and oldval is not null)
    or (updating and 
         (oldval<>newval 
          or (oldval is null and newval is not null)
          or (oldval is not null and newval is null)
         )
       )                  
  then 
    logdtl.extend;
    logdtl_ctr:=logdtl_ctr+1;
    logdtl(logdtl_ctr):=logdtl(1);
    logdtl(logdtl_ctr).colname:=colnam;
    if inserting or updating then 
      logdtl(logdtl_ctr).newvalue:=newval; 
    else
      logdtl(logdtl_ctr).newvalue:=null;       
    end if;  
    if updating or deleting then 
      logdtl(logdtl_ctr).oldvalue:=oldval;
    else
      logdtl(logdtl_ctr).oldvalue:=null;      
    end if;
  end if;
end;
--
--
begin
  select SYS_CONTEXT ('USERENV', 'MODULE') into calling_module from dual;
  if calling_module='Data Pump Worker' then return; end if;
  --
  -- History logging (HST)... initialize 
  -- Skip if datahist logging is explicitly turned fo via switch in runtime
  if runtime.datahist_logging_on_YN='Y' then
    SELECT LOGENTRYIDSEQ.NEXTVAL, sysdate 
      into hst.updtlogid, hst.updtdt FROM DUAL;
    -- set aside table pk, set managed variables
    hst.csa_sid:=runtime.csa_session_id;
    hst.loannmb:=null;
    hst.tablenm:='REFSBAOFCTBL';
    hst.loannmb:=null;      
    hst.updtuserid:=:new.lastupdtuserid;
    if deleting=true then hst.updtuserid:=runtime.csa_userid; end if;
    if hst.updtuserid is null then hst.updtuserid:=user; end if;
    case
      when inserting=true then hst.upd_action:='I'; 
      when updating=true then hst.upd_action:='U';
      when deleting=true then hst.upd_action:='D';
    end case; 
    -- now set composit PK for row being updated  
    hst.pk:=null         
      ||nvl(:new.SBAOFCCD,:old.SBAOFCCD)
;  
    -- insert log main row
    INSERT INTO stgupdtlogtbl values HST;
    --
    -- Now column value logging for changed columns
    --   Initialize logdtl collection.
    logdtl.extend;
    logdtl_ctr:=1;
    logdtl(logdtl_ctr).updtlogfk:=hst.updtlogid;
    logdtl(logdtl_ctr).oldvalue:=null;
    logdtl(logdtl_ctr).newvalue:=null;
    histlog_col_if_changed('SBAOFCCD',
      :old.SBAOFCCD,:new.SBAOFCCD);
    histlog_col_if_changed('SBAOFCNM',
      :old.SBAOFCNM,:new.SBAOFCNM);
    histlog_col_if_changed('SBAOFCADRSTR1NM',
      :old.SBAOFCADRSTR1NM,:new.SBAOFCADRSTR1NM);
    histlog_col_if_changed('SBAOFCADRSTR2NM',
      :old.SBAOFCADRSTR2NM,:new.SBAOFCADRSTR2NM);
    histlog_col_if_changed('SBAOFCADRCTYNM',
      :old.SBAOFCADRCTYNM,:new.SBAOFCADRCTYNM);
    histlog_col_if_changed('SBAOFCADRSTCD',
      :old.SBAOFCADRSTCD,:new.SBAOFCADRSTCD);
    histlog_col_if_changed('SBAOFCADRZIPCD',
      :old.SBAOFCADRZIPCD,:new.SBAOFCADRZIPCD);
    histlog_col_if_changed('SBAOFCADRZIP4CD',
      :old.SBAOFCADRZIP4CD,:new.SBAOFCADRZIP4CD);
    histlog_col_if_changed('SBAOFCPHNNMB',
      :old.SBAOFCPHNNMB,:new.SBAOFCPHNNMB);
    histlog_col_if_changed('LASTUPDTUSERID',
      :old.LASTUPDTUSERID,:new.LASTUPDTUSERID);
    histlog_col_if_changed('LASTUPDTDT',
      to_char(:old.LASTUPDTDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.LASTUPDTDT,'YYYY-MM-DD HH24:MI:SS'));
    -- write out any accumulated logdtl recs with bulk insert
    if logdtl_ctr>1 then
      forall r in 2..logdtl_ctr insert into stgcsa.stgupdtlogdtltbl values logdtl(r);
    end if;
  end if;  
  -- end of history logging section
end;

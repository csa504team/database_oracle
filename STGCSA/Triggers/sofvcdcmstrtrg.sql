create or replace trigger STGCSA.SOFVCDCMSTRTRG
    after insert or update or delete ON STGCSA.SOFVCDCMSTRTBL
    REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE
 /*
  Created on: 2019-05-04 08:25:24
  Created by: GENR
  Crerated from template csatrig_template v3.4 3 May 2019 on 2019-05-04 08:25:24
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
    Trigger handles errors by not updating and raising error -20100.
    Caller should handle failing SQL statement, including recording.
*/
  calling_module varchar2(80);
  tabowner varchar2(32):='STGCSA';
  tabname varchar2(32):='SOFVCDCMSTRTBL';
  -- vars for sofvaudt support
  TYPE audrecs_typ is table of STGCSA.SOFVAUDTTBL%rowtype;
  audrecs audrecs_typ:=audrecs_typ();
  audrecs_ctr number:=0;
  -- vars for history logging
  hst stgupdtlogtbl%rowtype;
  TYPE logdtl_typ is table of STGCSA.stgupdtlogdtltbl%rowtype;
  logdtl logdtl_typ:=logdtl_typ();
  logdtl_ctr number:=0;
procedure histlog_col_if_changed(colnam varchar2, oldval varchar2, newval varchar2) is
begin
  If (inserting and newval is not null)
    or (deleting and oldval is not null)
    or (updating and 
         (oldval<>newval 
          or (oldval is null and newval is not null)
          or (oldval is not null and newval is null)
         )
       )                  
  then 
    logdtl.extend;
    logdtl_ctr:=logdtl_ctr+1;
    logdtl(logdtl_ctr):=logdtl(1);
    logdtl(logdtl_ctr).colname:=colnam;
    if inserting or updating then 
      logdtl(logdtl_ctr).newvalue:=newval; 
    else
      logdtl(logdtl_ctr).newvalue:=null;       
    end if;  
    if updating or deleting then 
      logdtl(logdtl_ctr).oldvalue:=oldval;
    else
      logdtl(logdtl_ctr).oldvalue:=null;      
    end if;
  end if;
end;
--
--
begin
  select SYS_CONTEXT ('USERENV', 'MODULE') into calling_module from dual;
  if calling_module='Data Pump Worker' then return; end if;
    --
  -- code to record changes to SOFVAUDTTBL
  -- initialize audrecs collection and first rec 
  -- first rec holds defaults and is not inserted
  -- Skip if audit logging is explicitly turned fo via switch in runtime
  if runtime.audit_logging_on_YN='Y' then
    audrecs.extend;
    select systimestamp,sysdate,sys_context('userenv','session_user')
      into audrecs(1).audttransdt, audrecs(1).CREATDT, audrecs(1).creatuserid from dual;
    audrecs_ctr:=1;   
    audrecs(1).LASTUPDTDT:=audrecs(audrecs_ctr).CREATDT;
    audrecs(1).lastupdtuserid:=audrecs(audrecs_ctr).creatuserid;
    
     
    if deleting then
      audrecs(1).audtrecrdkey:=:old.CDCREGNCD;
    else 
      audrecs(1).audtrecrdkey:=:new.CDCREGNCD;
    end if;     
    audrecs(1).audtseqnmb:=0;
    audrecs(1).AUDTOPRTRINITIAL:='  ';
    audrecs(1).AUDTTRMNLID:='    ';
    audrecs(1).AUDTEDTCD:='?';
    audrecs(1).AUDTUSERID:=nvl(:new.lastupdtuserid,:old.lastupdtuserid);
    if audrecs(1).AUDTUSERID is null then audrecs(audrecs_ctr).AUDTUSERID:=runtime.csa_userid; end if;
    if audrecs(1).AUDTUSERID is null then audrecs(audrecs_ctr).AUDTUSERID:=user; end if;  
    if inserting or updating then
      --
      -- record specific changed values to sofvaudttbl

      if inserting then  
        audrecs(audrecs_ctr).AUDTACTNCD:='A';
      else  
        audrecs(audrecs_ctr).AUDTACTNCD:='C';
      end if;
    if not (:new.CDCMAILADRCTYNM=:old.CDCMAILADRCTYNM 
                or (:new.CDCMAILADRCTYNM is null and :old.CDCMAILADRCTYNM is null)) 
      then  
        audrecs.extend;
        audrecs_ctr:=audrecs_ctr+1;
        audrecs(audrecs_ctr):=audrecs(1);
        audrecs(audrecs_ctr).audtseqnmb:=audrecs(audrecs_ctr-1).audtseqnmb+1;
        audrecs(audrecs_ctr).AUDTFLDNM:='CCCITY  ';
        audrecs(audrecs_ctr).audtchngfile:='BGCDCMST';
        if updating then
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:=:old.CDCMAILADRCTYNM;
        else  
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:='$NULL$';
        end if;  
        audrecs(audrecs_ctr).AUDTAFTCNTNTS:=:new.CDCMAILADRCTYNM;
      end if;
    if not (:new.CDCMAILADRSTCD=:old.CDCMAILADRSTCD 
                or (:new.CDCMAILADRSTCD is null and :old.CDCMAILADRSTCD is null)) 
      then  
        audrecs.extend;
        audrecs_ctr:=audrecs_ctr+1;
        audrecs(audrecs_ctr):=audrecs(1);
        audrecs(audrecs_ctr).audtseqnmb:=audrecs(audrecs_ctr-1).audtseqnmb+1;
        audrecs(audrecs_ctr).AUDTFLDNM:='CCSTATE ';
        audrecs(audrecs_ctr).audtchngfile:='BGCDCMST';
        if updating then
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:=:old.CDCMAILADRSTCD;
        else  
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:='$NULL$';
        end if;  
        audrecs(audrecs_ctr).AUDTAFTCNTNTS:=:new.CDCMAILADRSTCD;
      end if;
    if not (:new.CDCMAILADRZIPCD=:old.CDCMAILADRZIPCD 
                or (:new.CDCMAILADRZIPCD is null and :old.CDCMAILADRZIPCD is null)) 
      then  
        audrecs.extend;
        audrecs_ctr:=audrecs_ctr+1;
        audrecs(audrecs_ctr):=audrecs(1);
        audrecs(audrecs_ctr).audtseqnmb:=audrecs(audrecs_ctr-1).audtseqnmb+1;
        audrecs(audrecs_ctr).AUDTFLDNM:='CCZIP   ';
        audrecs(audrecs_ctr).audtchngfile:='BGCDCMST';
        if updating then
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:=:old.CDCMAILADRZIPCD;
        else  
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:='$NULL$';
        end if;  
        audrecs(audrecs_ctr).AUDTAFTCNTNTS:=:new.CDCMAILADRZIPCD;
      end if;
    if not (:new.CDCSTATCD=:old.CDCSTATCD 
                or (:new.CDCSTATCD is null and :old.CDCSTATCD is null)) 
      then  
        audrecs.extend;
        audrecs_ctr:=audrecs_ctr+1;
        audrecs(audrecs_ctr):=audrecs(1);
        audrecs(audrecs_ctr).audtseqnmb:=audrecs(audrecs_ctr-1).audtseqnmb+1;
        audrecs(audrecs_ctr).AUDTFLDNM:='CCSTATUS';
        audrecs(audrecs_ctr).audtchngfile:='BGCDCMST';
        if updating then
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:=:old.CDCSTATCD;
        else  
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:='$NULL$';
        end if;  
        audrecs(audrecs_ctr).AUDTAFTCNTNTS:=:new.CDCSTATCD;
      end if;
    if not (:new.CDCACHMAILADRSTR1NM=:old.CDCACHMAILADRSTR1NM 
                or (:new.CDCACHMAILADRSTR1NM is null and :old.CDCACHMAILADRSTR1NM is null)) 
      then  
        audrecs.extend;
        audrecs_ctr:=audrecs_ctr+1;
        audrecs(audrecs_ctr):=audrecs(1);
        audrecs(audrecs_ctr).audtseqnmb:=audrecs(audrecs_ctr-1).audtseqnmb+1;
        audrecs(audrecs_ctr).AUDTFLDNM:='CCSTREET';
        audrecs(audrecs_ctr).audtchngfile:='BGCDCMST';
        if updating then
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:=:old.CDCACHMAILADRSTR1NM;
        else  
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:='$NULL$';
        end if;  
        audrecs(audrecs_ctr).AUDTAFTCNTNTS:=:new.CDCACHMAILADRSTR1NM;
      end if;
    if not (:new.CDCACHMAILADRSTR2NM=:old.CDCACHMAILADRSTR2NM 
                or (:new.CDCACHMAILADRSTR2NM is null and :old.CDCACHMAILADRSTR2NM is null)) 
      then  
        audrecs.extend;
        audrecs_ctr:=audrecs_ctr+1;
        audrecs(audrecs_ctr):=audrecs(1);
        audrecs(audrecs_ctr).audtseqnmb:=audrecs(audrecs_ctr-1).audtseqnmb+1;
        audrecs(audrecs_ctr).AUDTFLDNM:='CCSTRET2';
        audrecs(audrecs_ctr).audtchngfile:='BGCDCMST';
        if updating then
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:=:old.CDCACHMAILADRSTR2NM;
        else  
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:='$NULL$';
        end if;  
        audrecs(audrecs_ctr).AUDTAFTCNTNTS:=:new.CDCACHMAILADRSTR2NM;
      end if;
    if not (:new.CDCNETPREPAYAMT=:old.CDCNETPREPAYAMT 
                or (:new.CDCNETPREPAYAMT is null and :old.CDCNETPREPAYAMT is null)) 
      then  
        audrecs.extend;
        audrecs_ctr:=audrecs_ctr+1;
        audrecs(audrecs_ctr):=audrecs(1);
        audrecs(audrecs_ctr).audtseqnmb:=audrecs(audrecs_ctr-1).audtseqnmb+1;
        audrecs(audrecs_ctr).AUDTFLDNM:='CCNETPPM';
        audrecs(audrecs_ctr).audtchngfile:='BGCDCMST';
        if updating then
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:=:old.CDCNETPREPAYAMT;
        else  
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:='$NULL$';
        end if;  
        audrecs(audrecs_ctr).AUDTAFTCNTNTS:=:new.CDCNETPREPAYAMT;
      end if;
  -- write out any accumulated audit recs with bulk insert
      if audrecs_ctr>1 then
        forall r in 2..audrecs_ctr insert into stgcsa.sofvaudttbl values audrecs(r);
      end if;  
    end if;
  end if;
  -- end of logging to sofvaudttbl
  --
  -- History logging (HST)... initialize 
  -- Skip if datahist logging is explicitly turned fo via switch in runtime
  if runtime.datahist_logging_on_YN='Y' then
    SELECT LOGENTRYIDSEQ.NEXTVAL, sysdate 
      into hst.updtlogid, hst.updtdt FROM DUAL;
    -- set aside table pk, set managed variables
    hst.csa_sid:=runtime.csa_session_id;
    hst.loannmb:=null;
    hst.tablenm:='SOFVCDCMSTRTBL';
    hst.loannmb:=null;      
    hst.updtuserid:=:new.lastupdtuserid;
    if deleting=true then hst.updtuserid:=runtime.csa_userid; end if;
    if hst.updtuserid is null then hst.updtuserid:=user; end if;
    case
      when inserting=true then hst.upd_action:='I'; 
      when updating=true then hst.upd_action:='U';
      when deleting=true then hst.upd_action:='D';
    end case; 
    -- now set composit PK for row being updated  
    hst.pk:=null         
      ||nvl(:new.CDCREGNCD,:old.CDCREGNCD)
      ||nvl(:new.CDCCERTNMB,:old.CDCCERTNMB)
;  
    -- insert log main row
    INSERT INTO stgupdtlogtbl values HST;
    --
    -- Now column value logging for changed columns
    --   Initialize logdtl collection.
    logdtl.extend;
    logdtl_ctr:=1;
    logdtl(logdtl_ctr).updtlogfk:=hst.updtlogid;
    logdtl(logdtl_ctr).oldvalue:=null;
    logdtl(logdtl_ctr).newvalue:=null;
    histlog_col_if_changed('CDCREGNCD',
      :old.CDCREGNCD,:new.CDCREGNCD);
    histlog_col_if_changed('CDCCERTNMB',
      :old.CDCCERTNMB,:new.CDCCERTNMB);
    histlog_col_if_changed('CDCPOLKNMB',
      :old.CDCPOLKNMB,:new.CDCPOLKNMB);
    histlog_col_if_changed('CDCNM',
      :old.CDCNM,:new.CDCNM);
    histlog_col_if_changed('CDCMAILADRSTR1NM',
      :old.CDCMAILADRSTR1NM,:new.CDCMAILADRSTR1NM);
    histlog_col_if_changed('CDCMAILADRCTYNM',
      :old.CDCMAILADRCTYNM,:new.CDCMAILADRCTYNM);
    histlog_col_if_changed('CDCMAILADRSTCD',
      :old.CDCMAILADRSTCD,:new.CDCMAILADRSTCD);
    histlog_col_if_changed('CDCMAILADRZIPCD',
      :old.CDCMAILADRZIPCD,:new.CDCMAILADRZIPCD);
    histlog_col_if_changed('CDCMAILADRZIP4CD',
      :old.CDCMAILADRZIP4CD,:new.CDCMAILADRZIP4CD);
    histlog_col_if_changed('CDCCNTCT',
      :old.CDCCNTCT,:new.CDCCNTCT);
    histlog_col_if_changed('CDCAREACD',
      :old.CDCAREACD,:new.CDCAREACD);
    histlog_col_if_changed('CDCEXCH',
      :old.CDCEXCH,:new.CDCEXCH);
    histlog_col_if_changed('CDCEXTN',
      :old.CDCEXTN,:new.CDCEXTN);
    histlog_col_if_changed('CDCSTATCD',
      :old.CDCSTATCD,:new.CDCSTATCD);
    histlog_col_if_changed('CDCTAXID',
      :old.CDCTAXID,:new.CDCTAXID);
    histlog_col_if_changed('CDCSETUPDT',
      to_char(:old.CDCSETUPDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CDCSETUPDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CDCLMAINTNDT',
      to_char(:old.CDCLMAINTNDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CDCLMAINTNDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CDCTOTDOLLRDBENTRAMT',
      :old.CDCTOTDOLLRDBENTRAMT,:new.CDCTOTDOLLRDBENTRAMT);
    histlog_col_if_changed('CDCTOTNMBPART',
      :old.CDCTOTNMBPART,:new.CDCTOTNMBPART);
    histlog_col_if_changed('CDCTOTPAIDDBENTRAMT',
      :old.CDCTOTPAIDDBENTRAMT,:new.CDCTOTPAIDDBENTRAMT);
    histlog_col_if_changed('CDCHLDPYMT',
      :old.CDCHLDPYMT,:new.CDCHLDPYMT);
    histlog_col_if_changed('CDCDIST',
      :old.CDCDIST,:new.CDCDIST);
    histlog_col_if_changed('CDCMAILADRSTR2NM',
      :old.CDCMAILADRSTR2NM,:new.CDCMAILADRSTR2NM);
    histlog_col_if_changed('CDCACHBNK',
      :old.CDCACHBNK,:new.CDCACHBNK);
    histlog_col_if_changed('CDCACHMAILADRSTR1NM',
      :old.CDCACHMAILADRSTR1NM,:new.CDCACHMAILADRSTR1NM);
    histlog_col_if_changed('CDCACHMAILADRSTR2NM',
      :old.CDCACHMAILADRSTR2NM,:new.CDCACHMAILADRSTR2NM);
    histlog_col_if_changed('CDCACHMAILADRCTYNM',
      :old.CDCACHMAILADRCTYNM,:new.CDCACHMAILADRCTYNM);
    histlog_col_if_changed('CDCACHMAILADRSTCD',
      :old.CDCACHMAILADRSTCD,:new.CDCACHMAILADRSTCD);
    histlog_col_if_changed('CDCACHMAILADRZIPCD',
      :old.CDCACHMAILADRZIPCD,:new.CDCACHMAILADRZIPCD);
    histlog_col_if_changed('CDCACHMAILADRZIP4CD',
      :old.CDCACHMAILADRZIP4CD,:new.CDCACHMAILADRZIP4CD);
    histlog_col_if_changed('CDCACHBR',
      :old.CDCACHBR,:new.CDCACHBR);
    histlog_col_if_changed('CDCACHACCTTYP',
      :old.CDCACHACCTTYP,:new.CDCACHACCTTYP);
    histlog_col_if_changed('CDCACHACCTNMB',
      :old.CDCACHACCTNMB,:new.CDCACHACCTNMB);
    histlog_col_if_changed('CDCACHROUTSYM',
      :old.CDCACHROUTSYM,:new.CDCACHROUTSYM);
    histlog_col_if_changed('CDCACHTRANSCD',
      :old.CDCACHTRANSCD,:new.CDCACHTRANSCD);
    histlog_col_if_changed('CDCACHBNKIDNMB',
      :old.CDCACHBNKIDNMB,:new.CDCACHBNKIDNMB);
    histlog_col_if_changed('CDCACHDEPSTR',
      :old.CDCACHDEPSTR,:new.CDCACHDEPSTR);
    histlog_col_if_changed('CDCACHSIGNDT',
      to_char(:old.CDCACHSIGNDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CDCACHSIGNDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CDCPRENOTETESTDT',
      to_char(:old.CDCPRENOTETESTDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CDCPRENOTETESTDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CDCACHPRENTIND',
      :old.CDCACHPRENTIND,:new.CDCACHPRENTIND);
    histlog_col_if_changed('CDCCLSBALAMT',
      :old.CDCCLSBALAMT,:new.CDCCLSBALAMT);
    histlog_col_if_changed('CDCCLSBALDT',
      to_char(:old.CDCCLSBALDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CDCCLSBALDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CDCNETPREPAYAMT',
      :old.CDCNETPREPAYAMT,:new.CDCNETPREPAYAMT);
    histlog_col_if_changed('CDCFAXAREACD',
      :old.CDCFAXAREACD,:new.CDCFAXAREACD);
    histlog_col_if_changed('CDCFAXEXCH',
      :old.CDCFAXEXCH,:new.CDCFAXEXCH);
    histlog_col_if_changed('CDCFAXEXTN',
      :old.CDCFAXEXTN,:new.CDCFAXEXTN);
    histlog_col_if_changed('CDCPREPAYCLSBALAMT',
      :old.CDCPREPAYCLSBALAMT,:new.CDCPREPAYCLSBALAMT);
    histlog_col_if_changed('CDCNAMADDRID',
      :old.CDCNAMADDRID,:new.CDCNAMADDRID);
    histlog_col_if_changed('CREATUSERID',
      :old.CREATUSERID,:new.CREATUSERID);
    histlog_col_if_changed('CREATDT',
      to_char(:old.CREATDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CREATDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('LASTUPDTUSERID',
      :old.LASTUPDTUSERID,:new.LASTUPDTUSERID);
    histlog_col_if_changed('LASTUPDTDT',
      to_char(:old.LASTUPDTDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.LASTUPDTDT,'YYYY-MM-DD HH24:MI:SS'));
    -- write out any accumulated logdtl recs with bulk insert
    if logdtl_ctr>1 then
      forall r in 2..logdtl_ctr insert into stgcsa.stgupdtlogdtltbl values logdtl(r);
    end if;
  end if;  
  -- end of history logging section
end;

create or replace trigger STGCSA.ACCACCELERATELOANTRG
    after insert or update or delete ON STGCSA.ACCACCELERATELOANTBL
    REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE
 /*
  Created on: 2019-05-04 08:24:31
  Created by: GENR
  Crerated from template csatrig_template v3.4 3 May 2019 on 2019-05-04 08:24:32
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
    Trigger handles errors by not updating and raising error -20100.
    Caller should handle failing SQL statement, including recording.
*/
  calling_module varchar2(80);
  tabowner varchar2(32):='STGCSA';
  tabname varchar2(32):='ACCACCELERATELOANTBL';
  -- vars for history logging
  hst stgupdtlogtbl%rowtype;
  TYPE logdtl_typ is table of STGCSA.stgupdtlogdtltbl%rowtype;
  logdtl logdtl_typ:=logdtl_typ();
  logdtl_ctr number:=0;
procedure histlog_col_if_changed(colnam varchar2, oldval varchar2, newval varchar2) is
begin
  If (inserting and newval is not null)
    or (deleting and oldval is not null)
    or (updating and 
         (oldval<>newval 
          or (oldval is null and newval is not null)
          or (oldval is not null and newval is null)
         )
       )                  
  then 
    logdtl.extend;
    logdtl_ctr:=logdtl_ctr+1;
    logdtl(logdtl_ctr):=logdtl(1);
    logdtl(logdtl_ctr).colname:=colnam;
    if inserting or updating then 
      logdtl(logdtl_ctr).newvalue:=newval; 
    else
      logdtl(logdtl_ctr).newvalue:=null;       
    end if;  
    if updating or deleting then 
      logdtl(logdtl_ctr).oldvalue:=oldval;
    else
      logdtl(logdtl_ctr).oldvalue:=null;      
    end if;
  end if;
end;
--
--
begin
  select SYS_CONTEXT ('USERENV', 'MODULE') into calling_module from dual;
  if calling_module='Data Pump Worker' then return; end if;
  --
  -- History logging (HST)... initialize 
  -- Skip if datahist logging is explicitly turned fo via switch in runtime
  if runtime.datahist_logging_on_YN='Y' then
    SELECT LOGENTRYIDSEQ.NEXTVAL, sysdate 
      into hst.updtlogid, hst.updtdt FROM DUAL;
    -- set aside table pk, set managed variables
    hst.csa_sid:=runtime.csa_session_id;
    hst.loannmb:=null;
    hst.tablenm:='ACCACCELERATELOANTBL';
    hst.loannmb:=nvl(:old.loannmb,:new.loannmb);
      
    hst.updtuserid:=:new.lastupdtuserid;
    if deleting=true then hst.updtuserid:=runtime.csa_userid; end if;
    if hst.updtuserid is null then hst.updtuserid:=user; end if;
    case
      when inserting=true then hst.upd_action:='I'; 
      when updating=true then hst.upd_action:='U';
      when deleting=true then hst.upd_action:='D';
    end case; 
    -- now set composit PK for row being updated  
    hst.pk:=null         
      ||rpad(to_char(nvl(:new.ACCELERATNID,:old.ACCELERATNID)),22)
;  
    -- insert log main row
    INSERT INTO stgupdtlogtbl values HST;
    --
    -- Now column value logging for changed columns
    --   Initialize logdtl collection.
    logdtl.extend;
    logdtl_ctr:=1;
    logdtl(logdtl_ctr).updtlogfk:=hst.updtlogid;
    logdtl(logdtl_ctr).oldvalue:=null;
    logdtl(logdtl_ctr).newvalue:=null;
    histlog_col_if_changed('ACCELERATNID',
      :old.ACCELERATNID,:new.ACCELERATNID);
    histlog_col_if_changed('LOANNMB',
      :old.LOANNMB,:new.LOANNMB);
    histlog_col_if_changed('TRANSID',
      :old.TRANSID,:new.TRANSID);
    histlog_col_if_changed('PRIORACCELERATN',
      :old.PRIORACCELERATN,:new.PRIORACCELERATN);
    histlog_col_if_changed('STATID',
      :old.STATID,:new.STATID);
    histlog_col_if_changed('ACCELERATECMNT',
      :old.ACCELERATECMNT,:new.ACCELERATECMNT);
    histlog_col_if_changed('SEMIANDT',
      to_char(:old.SEMIANDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.SEMIANDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('MFINITIALSCRAPEDT',
      to_char(:old.MFINITIALSCRAPEDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.MFINITIALSCRAPEDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('MFLASTSCRAPEDT',
      to_char(:old.MFLASTSCRAPEDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.MFLASTSCRAPEDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('MFCURSTATID',
      :old.MFCURSTATID,:new.MFCURSTATID);
    histlog_col_if_changed('MFCURCMNT',
      :old.MFCURCMNT,:new.MFCURCMNT);
    histlog_col_if_changed('MFINITIALSTATID',
      :old.MFINITIALSTATID,:new.MFINITIALSTATID);
    histlog_col_if_changed('MFINITIALCMNT',
      :old.MFINITIALCMNT,:new.MFINITIALCMNT);
    histlog_col_if_changed('MFLASTUPDTDT',
      to_char(:old.MFLASTUPDTDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.MFLASTUPDTDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('MFISSDT',
      to_char(:old.MFISSDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.MFISSDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('MFLOANMATDT',
      to_char(:old.MFLOANMATDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.MFLOANMATDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('MFORGLPRINAMT',
      :old.MFORGLPRINAMT,:new.MFORGLPRINAMT);
    histlog_col_if_changed('MFINTRTPCT',
      :old.MFINTRTPCT,:new.MFINTRTPCT);
    histlog_col_if_changed('MFDBENTRAMT',
      :old.MFDBENTRAMT,:new.MFDBENTRAMT);
    histlog_col_if_changed('MFTOBECURAMT',
      :old.MFTOBECURAMT,:new.MFTOBECURAMT);
    histlog_col_if_changed('MFLASTPYMTDT',
      to_char(:old.MFLASTPYMTDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.MFLASTPYMTDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('MFESCROWPRIORPYMTAMT',
      :old.MFESCROWPRIORPYMTAMT,:new.MFESCROWPRIORPYMTAMT);
    histlog_col_if_changed('MFCDCPAIDTHRUDT',
      :old.MFCDCPAIDTHRUDT,:new.MFCDCPAIDTHRUDT);
    histlog_col_if_changed('MFCDCMODELNQ',
      :old.MFCDCMODELNQ,:new.MFCDCMODELNQ);
    histlog_col_if_changed('MFCDCMOFEEAMT',
      :old.MFCDCMOFEEAMT,:new.MFCDCMOFEEAMT);
    histlog_col_if_changed('MFCDCFEEAMT',
      :old.MFCDCFEEAMT,:new.MFCDCFEEAMT);
    histlog_col_if_changed('MFCDCDUEFROMBORRAMT',
      :old.MFCDCDUEFROMBORRAMT,:new.MFCDCDUEFROMBORRAMT);
    histlog_col_if_changed('MFCSAPAIDTHRUDT',
      :old.MFCSAPAIDTHRUDT,:new.MFCSAPAIDTHRUDT);
    histlog_col_if_changed('MFCSAMODELNQ',
      :old.MFCSAMODELNQ,:new.MFCSAMODELNQ);
    histlog_col_if_changed('MFCSAMOFEEAMT',
      :old.MFCSAMOFEEAMT,:new.MFCSAMOFEEAMT);
    histlog_col_if_changed('MFCSAFEEAMT',
      :old.MFCSAFEEAMT,:new.MFCSAFEEAMT);
    histlog_col_if_changed('MFCSADUEFROMBORRAMT',
      :old.MFCSADUEFROMBORRAMT,:new.MFCSADUEFROMBORRAMT);
    histlog_col_if_changed('PPAENTRYDT',
      to_char(:old.PPAENTRYDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.PPAENTRYDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('PPAGROUP',
      :old.PPAGROUP,:new.PPAGROUP);
    histlog_col_if_changed('PPACDCNMB',
      :old.PPACDCNMB,:new.PPACDCNMB);
    histlog_col_if_changed('PPAORGLPRINAMT',
      :old.PPAORGLPRINAMT,:new.PPAORGLPRINAMT);
    histlog_col_if_changed('PPADBENTRBALATSEMIANAMT',
      :old.PPADBENTRBALATSEMIANAMT,:new.PPADBENTRBALATSEMIANAMT);
    histlog_col_if_changed('PPAINTDUEATSEMIANAMT',
      :old.PPAINTDUEATSEMIANAMT,:new.PPAINTDUEATSEMIANAMT);
    histlog_col_if_changed('PPADUEATSEMIANAMT',
      :old.PPADUEATSEMIANAMT,:new.PPADUEATSEMIANAMT);
    histlog_col_if_changed('SBAENTRYDT',
      to_char(:old.SBAENTRYDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.SBAENTRYDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('SBALASTTRNSCRPTIMPTDT',
      to_char(:old.SBALASTTRNSCRPTIMPTDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.SBALASTTRNSCRPTIMPTDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('SBADBENTRBALATSEMIANAMT',
      :old.SBADBENTRBALATSEMIANAMT,:new.SBADBENTRBALATSEMIANAMT);
    histlog_col_if_changed('SBAINTDUEATSEMIANAMT',
      :old.SBAINTDUEATSEMIANAMT,:new.SBAINTDUEATSEMIANAMT);
    histlog_col_if_changed('SBACSAFEEAMT',
      :old.SBACSAFEEAMT,:new.SBACSAFEEAMT);
    histlog_col_if_changed('SBACDCFEEAMT',
      :old.SBACDCFEEAMT,:new.SBACDCFEEAMT);
    histlog_col_if_changed('SBAESCROWPRIORPYMTAMT',
      :old.SBAESCROWPRIORPYMTAMT,:new.SBAESCROWPRIORPYMTAMT);
    histlog_col_if_changed('SBAACCELAMT',
      :old.SBAACCELAMT,:new.SBAACCELAMT);
    histlog_col_if_changed('SBADUEDT',
      to_char(:old.SBADUEDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.SBADUEDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('SBANOTERTPCT',
      :old.SBANOTERTPCT,:new.SBANOTERTPCT);
    histlog_col_if_changed('SBALASTNOTEBALAMT',
      :old.SBALASTNOTEBALAMT,:new.SBALASTNOTEBALAMT);
    histlog_col_if_changed('SBALASTPYMTDT',
      to_char(:old.SBALASTPYMTDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.SBALASTPYMTDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('SBAACCRINTDUEAMT',
      :old.SBAACCRINTDUEAMT,:new.SBAACCRINTDUEAMT);
    histlog_col_if_changed('SBAEXCESSAMT',
      :old.SBAEXCESSAMT,:new.SBAEXCESSAMT);
    histlog_col_if_changed('MFPREVSTATID',
      :old.MFPREVSTATID,:new.MFPREVSTATID);
    histlog_col_if_changed('MFPREVCMNT',
      :old.MFPREVCMNT,:new.MFPREVCMNT);
    histlog_col_if_changed('CDCNMB',
      :old.CDCNMB,:new.CDCNMB);
    histlog_col_if_changed('SERVCNTRID',
      :old.SERVCNTRID,:new.SERVCNTRID);
    histlog_col_if_changed('TIMESTAMPFLD',
      to_char(:old.TIMESTAMPFLD,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.TIMESTAMPFLD,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CREATUSERID',
      :old.CREATUSERID,:new.CREATUSERID);
    histlog_col_if_changed('CREATDT',
      to_char(:old.CREATDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CREATDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('LASTUPDTUSERID',
      :old.LASTUPDTUSERID,:new.LASTUPDTUSERID);
    histlog_col_if_changed('LASTUPDTDT',
      to_char(:old.LASTUPDTDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.LASTUPDTDT,'YYYY-MM-DD HH24:MI:SS'));
    -- write out any accumulated logdtl recs with bulk insert
    if logdtl_ctr>1 then
      forall r in 2..logdtl_ctr insert into stgcsa.stgupdtlogdtltbl values logdtl(r);
    end if;
  end if;  
  -- end of history logging section
end;

create or replace trigger STGCSA.SOFVBGW9TRG
    after insert or update or delete ON STGCSA.SOFVBGW9TBL
    REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE
 /*
  Created on: 2019-05-04 08:25:23
  Created by: GENR
  Crerated from template csatrig_template v3.4 3 May 2019 on 2019-05-04 08:25:23
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
    Trigger handles errors by not updating and raising error -20100.
    Caller should handle failing SQL statement, including recording.
*/
  calling_module varchar2(80);
  tabowner varchar2(32):='STGCSA';
  tabname varchar2(32):='SOFVBGW9TBL';
  -- vars for sofvaudt support
  TYPE audrecs_typ is table of STGCSA.SOFVAUDTTBL%rowtype;
  audrecs audrecs_typ:=audrecs_typ();
  audrecs_ctr number:=0;
  -- vars for history logging
  hst stgupdtlogtbl%rowtype;
  TYPE logdtl_typ is table of STGCSA.stgupdtlogdtltbl%rowtype;
  logdtl logdtl_typ:=logdtl_typ();
  logdtl_ctr number:=0;
procedure histlog_col_if_changed(colnam varchar2, oldval varchar2, newval varchar2) is
begin
  If (inserting and newval is not null)
    or (deleting and oldval is not null)
    or (updating and 
         (oldval<>newval 
          or (oldval is null and newval is not null)
          or (oldval is not null and newval is null)
         )
       )                  
  then 
    logdtl.extend;
    logdtl_ctr:=logdtl_ctr+1;
    logdtl(logdtl_ctr):=logdtl(1);
    logdtl(logdtl_ctr).colname:=colnam;
    if inserting or updating then 
      logdtl(logdtl_ctr).newvalue:=newval; 
    else
      logdtl(logdtl_ctr).newvalue:=null;       
    end if;  
    if updating or deleting then 
      logdtl(logdtl_ctr).oldvalue:=oldval;
    else
      logdtl(logdtl_ctr).oldvalue:=null;      
    end if;
  end if;
end;
--
--
begin
  select SYS_CONTEXT ('USERENV', 'MODULE') into calling_module from dual;
  if calling_module='Data Pump Worker' then return; end if;
    --
  -- code to record changes to SOFVAUDTTBL
  -- initialize audrecs collection and first rec 
  -- first rec holds defaults and is not inserted
  -- Skip if audit logging is explicitly turned fo via switch in runtime
  if runtime.audit_logging_on_YN='Y' then
    audrecs.extend;
    select systimestamp,sysdate,sys_context('userenv','session_user')
      into audrecs(1).audttransdt, audrecs(1).CREATDT, audrecs(1).creatuserid from dual;
    audrecs_ctr:=1;   
    audrecs(1).LASTUPDTDT:=audrecs(audrecs_ctr).CREATDT;
    audrecs(1).lastupdtuserid:=audrecs(audrecs_ctr).creatuserid;
    
     
    if deleting then
      audrecs(1).audtrecrdkey:=:old.LOANNMB;
    else 
      audrecs(1).audtrecrdkey:=:new.LOANNMB;
    end if;     
    audrecs(1).audtseqnmb:=0;
    audrecs(1).AUDTOPRTRINITIAL:='  ';
    audrecs(1).AUDTTRMNLID:='    ';
    audrecs(1).AUDTEDTCD:='?';
    audrecs(1).AUDTUSERID:=nvl(:new.lastupdtuserid,:old.lastupdtuserid);
    if audrecs(1).AUDTUSERID is null then audrecs(audrecs_ctr).AUDTUSERID:=runtime.csa_userid; end if;
    if audrecs(1).AUDTUSERID is null then audrecs(audrecs_ctr).AUDTUSERID:=user; end if;  
    if inserting or updating then
      --
      -- record specific changed values to sofvaudttbl

      if inserting then  
        audrecs(audrecs_ctr).AUDTACTNCD:='A';
      else  
        audrecs(audrecs_ctr).AUDTACTNCD:='C';
      end if;
    if not (:new.W9NM=:old.W9NM 
                or (:new.W9NM is null and :old.W9NM is null)) 
      then  
        audrecs.extend;
        audrecs_ctr:=audrecs_ctr+1;
        audrecs(audrecs_ctr):=audrecs(1);
        audrecs(audrecs_ctr).audtseqnmb:=audrecs(audrecs_ctr-1).audtseqnmb+1;
        audrecs(audrecs_ctr).AUDTFLDNM:='W9NAME1 ';
        audrecs(audrecs_ctr).audtchngfile:='BGW9FILE';
        if updating then
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:=:old.W9NM;
        else  
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:='$NULL$';
        end if;  
        audrecs(audrecs_ctr).AUDTAFTCNTNTS:=:new.W9NM;
      end if;
    if not (:new.W9MAILADRSTR1NM=:old.W9MAILADRSTR1NM 
                or (:new.W9MAILADRSTR1NM is null and :old.W9MAILADRSTR1NM is null)) 
      then  
        audrecs.extend;
        audrecs_ctr:=audrecs_ctr+1;
        audrecs(audrecs_ctr):=audrecs(1);
        audrecs(audrecs_ctr).audtseqnmb:=audrecs(audrecs_ctr-1).audtseqnmb+1;
        audrecs(audrecs_ctr).AUDTFLDNM:='W9STREET';
        audrecs(audrecs_ctr).audtchngfile:='BGW9FILE';
        if updating then
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:=:old.W9MAILADRSTR1NM;
        else  
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:='$NULL$';
        end if;  
        audrecs(audrecs_ctr).AUDTAFTCNTNTS:=:new.W9MAILADRSTR1NM;
      end if;
    if not (:new.W9MAILADRCTYNM=:old.W9MAILADRCTYNM 
                or (:new.W9MAILADRCTYNM is null and :old.W9MAILADRCTYNM is null)) 
      then  
        audrecs.extend;
        audrecs_ctr:=audrecs_ctr+1;
        audrecs(audrecs_ctr):=audrecs(1);
        audrecs(audrecs_ctr).audtseqnmb:=audrecs(audrecs_ctr-1).audtseqnmb+1;
        audrecs(audrecs_ctr).AUDTFLDNM:='W9CITY  ';
        audrecs(audrecs_ctr).audtchngfile:='BGW9FILE';
        if updating then
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:=:old.W9MAILADRCTYNM;
        else  
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:='$NULL$';
        end if;  
        audrecs(audrecs_ctr).AUDTAFTCNTNTS:=:new.W9MAILADRCTYNM;
      end if;
    if not (:new.W9MAILADRSTCD=:old.W9MAILADRSTCD 
                or (:new.W9MAILADRSTCD is null and :old.W9MAILADRSTCD is null)) 
      then  
        audrecs.extend;
        audrecs_ctr:=audrecs_ctr+1;
        audrecs(audrecs_ctr):=audrecs(1);
        audrecs(audrecs_ctr).audtseqnmb:=audrecs(audrecs_ctr-1).audtseqnmb+1;
        audrecs(audrecs_ctr).AUDTFLDNM:='W9STATE ';
        audrecs(audrecs_ctr).audtchngfile:='BGW9FILE';
        if updating then
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:=:old.W9MAILADRSTCD;
        else  
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:='$NULL$';
        end if;  
        audrecs(audrecs_ctr).AUDTAFTCNTNTS:=:new.W9MAILADRSTCD;
      end if;
    if not (:new.W9MAILADRZIPCD=:old.W9MAILADRZIPCD 
                or (:new.W9MAILADRZIPCD is null and :old.W9MAILADRZIPCD is null)) 
      then  
        audrecs.extend;
        audrecs_ctr:=audrecs_ctr+1;
        audrecs(audrecs_ctr):=audrecs(1);
        audrecs(audrecs_ctr).audtseqnmb:=audrecs(audrecs_ctr-1).audtseqnmb+1;
        audrecs(audrecs_ctr).AUDTFLDNM:='W9ZIP1  ';
        audrecs(audrecs_ctr).audtchngfile:='BGW9FILE';
        if updating then
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:=:old.W9MAILADRZIPCD;
        else  
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:='$NULL$';
        end if;  
        audrecs(audrecs_ctr).AUDTAFTCNTNTS:=:new.W9MAILADRZIPCD;
      end if;
    if not (:new.W9MAILADRZIP4CD=:old.W9MAILADRZIP4CD 
                or (:new.W9MAILADRZIP4CD is null and :old.W9MAILADRZIP4CD is null)) 
      then  
        audrecs.extend;
        audrecs_ctr:=audrecs_ctr+1;
        audrecs(audrecs_ctr):=audrecs(1);
        audrecs(audrecs_ctr).audtseqnmb:=audrecs(audrecs_ctr-1).audtseqnmb+1;
        audrecs(audrecs_ctr).AUDTFLDNM:='W9ZIP2  ';
        audrecs(audrecs_ctr).audtchngfile:='BGW9FILE';
        if updating then
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:=:old.W9MAILADRZIP4CD;
        else  
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:='$NULL$';
        end if;  
        audrecs(audrecs_ctr).AUDTAFTCNTNTS:=:new.W9MAILADRZIP4CD;
      end if;
  -- write out any accumulated audit recs with bulk insert
      if audrecs_ctr>1 then
        forall r in 2..audrecs_ctr insert into stgcsa.sofvaudttbl values audrecs(r);
      end if;  
    end if;
  end if;
  -- end of logging to sofvaudttbl
  --
  -- History logging (HST)... initialize 
  -- Skip if datahist logging is explicitly turned fo via switch in runtime
  if runtime.datahist_logging_on_YN='Y' then
    SELECT LOGENTRYIDSEQ.NEXTVAL, sysdate 
      into hst.updtlogid, hst.updtdt FROM DUAL;
    -- set aside table pk, set managed variables
    hst.csa_sid:=runtime.csa_session_id;
    hst.loannmb:=null;
    hst.tablenm:='SOFVBGW9TBL';
    hst.loannmb:=nvl(:old.loannmb,:new.loannmb);
      
    hst.updtuserid:=:new.lastupdtuserid;
    if deleting=true then hst.updtuserid:=runtime.csa_userid; end if;
    if hst.updtuserid is null then hst.updtuserid:=user; end if;
    case
      when inserting=true then hst.upd_action:='I'; 
      when updating=true then hst.upd_action:='U';
      when deleting=true then hst.upd_action:='D';
    end case; 
    -- now set composit PK for row being updated  
    hst.pk:=null         
      ||nvl(:new.LOANNMB,:old.LOANNMB)
;  
    -- insert log main row
    INSERT INTO stgupdtlogtbl values HST;
    --
    -- Now column value logging for changed columns
    --   Initialize logdtl collection.
    logdtl.extend;
    logdtl_ctr:=1;
    logdtl(logdtl_ctr).updtlogfk:=hst.updtlogid;
    logdtl(logdtl_ctr).oldvalue:=null;
    logdtl(logdtl_ctr).newvalue:=null;
    histlog_col_if_changed('LOANNMB',
      :old.LOANNMB,:new.LOANNMB);
    histlog_col_if_changed('W9TAXID',
      :old.W9TAXID,:new.W9TAXID);
    histlog_col_if_changed('W9NM',
      :old.W9NM,:new.W9NM);
    histlog_col_if_changed('W9MAILADRSTR1NM',
      :old.W9MAILADRSTR1NM,:new.W9MAILADRSTR1NM);
    histlog_col_if_changed('W9MAILADRSTR2NM',
      :old.W9MAILADRSTR2NM,:new.W9MAILADRSTR2NM);
    histlog_col_if_changed('W9MAILADRCTYNM',
      :old.W9MAILADRCTYNM,:new.W9MAILADRCTYNM);
    histlog_col_if_changed('W9MAILADRSTCD',
      :old.W9MAILADRSTCD,:new.W9MAILADRSTCD);
    histlog_col_if_changed('W9MAILADRZIPCD',
      :old.W9MAILADRZIPCD,:new.W9MAILADRZIPCD);
    histlog_col_if_changed('W9MAILADRZIP4CD',
      :old.W9MAILADRZIP4CD,:new.W9MAILADRZIP4CD);
    histlog_col_if_changed('W9VERIFICATIONIND',
      :old.W9VERIFICATIONIND,:new.W9VERIFICATIONIND);
    histlog_col_if_changed('W9VERIFICATIONDT',
      to_char(:old.W9VERIFICATIONDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.W9VERIFICATIONDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('W9NMMAILADRCHNGIND',
      :old.W9NMMAILADRCHNGIND,:new.W9NMMAILADRCHNGIND);
    histlog_col_if_changed('W9NMADRCHNGDT',
      to_char(:old.W9NMADRCHNGDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.W9NMADRCHNGDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('W9TAXFORMFLD',
      :old.W9TAXFORMFLD,:new.W9TAXFORMFLD);
    histlog_col_if_changed('W9TAXMAINTNDT',
      to_char(:old.W9TAXMAINTNDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.W9TAXMAINTNDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CREATUSERID',
      :old.CREATUSERID,:new.CREATUSERID);
    histlog_col_if_changed('CREATDT',
      to_char(:old.CREATDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CREATDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('LASTUPDTUSERID',
      :old.LASTUPDTUSERID,:new.LASTUPDTUSERID);
    histlog_col_if_changed('LASTUPDTDT',
      to_char(:old.LASTUPDTDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.LASTUPDTDT,'YYYY-MM-DD HH24:MI:SS'));
    -- write out any accumulated logdtl recs with bulk insert
    if logdtl_ctr>1 then
      forall r in 2..logdtl_ctr insert into stgcsa.stgupdtlogdtltbl values logdtl(r);
    end if;
  end if;  
  -- end of history logging section
end;

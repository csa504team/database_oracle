create or replace trigger STGCSA.PRPLOANRQSTTRG
    after insert or update or delete ON STGCSA.PRPLOANRQSTTBL
    REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE
 /*
  Created on: 2019-05-04 08:25:07
  Created by: GENR
  Crerated from template csatrig_template v3.4 3 May 2019 on 2019-05-04 08:25:07
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
    Trigger handles errors by not updating and raising error -20100.
    Caller should handle failing SQL statement, including recording.
*/
  calling_module varchar2(80);
  tabowner varchar2(32):='STGCSA';
  tabname varchar2(32):='PRPLOANRQSTTBL';
  -- vars for history logging
  hst stgupdtlogtbl%rowtype;
  TYPE logdtl_typ is table of STGCSA.stgupdtlogdtltbl%rowtype;
  logdtl logdtl_typ:=logdtl_typ();
  logdtl_ctr number:=0;
procedure histlog_col_if_changed(colnam varchar2, oldval varchar2, newval varchar2) is
begin
  If (inserting and newval is not null)
    or (deleting and oldval is not null)
    or (updating and 
         (oldval<>newval 
          or (oldval is null and newval is not null)
          or (oldval is not null and newval is null)
         )
       )                  
  then 
    logdtl.extend;
    logdtl_ctr:=logdtl_ctr+1;
    logdtl(logdtl_ctr):=logdtl(1);
    logdtl(logdtl_ctr).colname:=colnam;
    if inserting or updating then 
      logdtl(logdtl_ctr).newvalue:=newval; 
    else
      logdtl(logdtl_ctr).newvalue:=null;       
    end if;  
    if updating or deleting then 
      logdtl(logdtl_ctr).oldvalue:=oldval;
    else
      logdtl(logdtl_ctr).oldvalue:=null;      
    end if;
  end if;
end;
--
--
begin
  select SYS_CONTEXT ('USERENV', 'MODULE') into calling_module from dual;
  if calling_module='Data Pump Worker' then return; end if;
  --
  -- History logging (HST)... initialize 
  -- Skip if datahist logging is explicitly turned fo via switch in runtime
  if runtime.datahist_logging_on_YN='Y' then
    SELECT LOGENTRYIDSEQ.NEXTVAL, sysdate 
      into hst.updtlogid, hst.updtdt FROM DUAL;
    -- set aside table pk, set managed variables
    hst.csa_sid:=runtime.csa_session_id;
    hst.loannmb:=null;
    hst.tablenm:='PRPLOANRQSTTBL';
    hst.loannmb:=nvl(:old.loannmb,:new.loannmb);
      
    hst.updtuserid:=:new.lastupdtuserid;
    if deleting=true then hst.updtuserid:=runtime.csa_userid; end if;
    if hst.updtuserid is null then hst.updtuserid:=user; end if;
    case
      when inserting=true then hst.upd_action:='I'; 
      when updating=true then hst.upd_action:='U';
      when deleting=true then hst.upd_action:='D';
    end case; 
    -- now set composit PK for row being updated  
    hst.pk:=null         
      ||nvl(:new.LOANNMB,:old.LOANNMB)
;  
    -- insert log main row
    INSERT INTO stgupdtlogtbl values HST;
    --
    -- Now column value logging for changed columns
    --   Initialize logdtl collection.
    logdtl.extend;
    logdtl_ctr:=1;
    logdtl(logdtl_ctr).updtlogfk:=hst.updtlogid;
    logdtl(logdtl_ctr).oldvalue:=null;
    logdtl(logdtl_ctr).newvalue:=null;
    histlog_col_if_changed('LOANNMB',
      :old.LOANNMB,:new.LOANNMB);
    histlog_col_if_changed('RQSTREF',
      :old.RQSTREF,:new.RQSTREF);
    histlog_col_if_changed('RQSTCD',
      :old.RQSTCD,:new.RQSTCD);
    histlog_col_if_changed('CDCREGNCD',
      :old.CDCREGNCD,:new.CDCREGNCD);
    histlog_col_if_changed('CDCNMB',
      :old.CDCNMB,:new.CDCNMB);
    histlog_col_if_changed('LOANSTAT',
      :old.LOANSTAT,:new.LOANSTAT);
    histlog_col_if_changed('BORRNM',
      :old.BORRNM,:new.BORRNM);
    histlog_col_if_changed('PREPAYAMT',
      :old.PREPAYAMT,:new.PREPAYAMT);
    histlog_col_if_changed('PREPAYDT',
      to_char(:old.PREPAYDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.PREPAYDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('PREPAYMO',
      :old.PREPAYMO,:new.PREPAYMO);
    histlog_col_if_changed('PREPAYYRNMB',
      :old.PREPAYYRNMB,:new.PREPAYYRNMB);
    histlog_col_if_changed('PREPAYCALCDT',
      to_char(:old.PREPAYCALCDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.PREPAYCALCDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('SEMIANNDT',
      to_char(:old.SEMIANNDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.SEMIANNDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('SEMIANMO',
      :old.SEMIANMO,:new.SEMIANMO);
    histlog_col_if_changed('SEMIANNAMT',
      :old.SEMIANNAMT,:new.SEMIANNAMT);
    histlog_col_if_changed('SEMIANNPYMTAMT',
      :old.SEMIANNPYMTAMT,:new.SEMIANNPYMTAMT);
    histlog_col_if_changed('PRINCURAMT',
      :old.PRINCURAMT,:new.PRINCURAMT);
    histlog_col_if_changed('BALAMT',
      :old.BALAMT,:new.BALAMT);
    histlog_col_if_changed('BALCALCAMT',
      :old.BALCALCAMT,:new.BALCALCAMT);
    histlog_col_if_changed('NOTEBALCALCAMT',
      :old.NOTEBALCALCAMT,:new.NOTEBALCALCAMT);
    histlog_col_if_changed('PIAMT',
      :old.PIAMT,:new.PIAMT);
    histlog_col_if_changed('NOTERTPCT',
      :old.NOTERTPCT,:new.NOTERTPCT);
    histlog_col_if_changed('ISSDT',
      to_char(:old.ISSDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.ISSDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('DBENTRBALAMT',
      :old.DBENTRBALAMT,:new.DBENTRBALAMT);
    histlog_col_if_changed('PREPAYPREMAMT',
      :old.PREPAYPREMAMT,:new.PREPAYPREMAMT);
    histlog_col_if_changed('POSTINGDT',
      to_char(:old.POSTINGDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.POSTINGDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('POSTINGMO',
      :old.POSTINGMO,:new.POSTINGMO);
    histlog_col_if_changed('POSTINGYRNMB',
      :old.POSTINGYRNMB,:new.POSTINGYRNMB);
    histlog_col_if_changed('BUSDAY6DT',
      to_char(:old.BUSDAY6DT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.BUSDAY6DT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('THIRDTHURSDAY',
      to_char(:old.THIRDTHURSDAY,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.THIRDTHURSDAY,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('ACTUALRQST',
      :old.ACTUALRQST,:new.ACTUALRQST);
    histlog_col_if_changed('CSAPAIDTHRUDT',
      :old.CSAPAIDTHRUDT,:new.CSAPAIDTHRUDT);
    histlog_col_if_changed('CDCPAIDTHRUDT',
      :old.CDCPAIDTHRUDT,:new.CDCPAIDTHRUDT);
    histlog_col_if_changed('BORRPAIDTHRUDT',
      :old.BORRPAIDTHRUDT,:new.BORRPAIDTHRUDT);
    histlog_col_if_changed('INTPAIDTHRUDT',
      :old.INTPAIDTHRUDT,:new.INTPAIDTHRUDT);
    histlog_col_if_changed('APPVDT',
      to_char(:old.APPVDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.APPVDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('PRINNEEDEDAMT',
      :old.PRINNEEDEDAMT,:new.PRINNEEDEDAMT);
    histlog_col_if_changed('INTNEEDEDAMT',
      :old.INTNEEDEDAMT,:new.INTNEEDEDAMT);
    histlog_col_if_changed('MODELNQ',
      :old.MODELNQ,:new.MODELNQ);
    histlog_col_if_changed('CURRFEEBALAMT',
      :old.CURRFEEBALAMT,:new.CURRFEEBALAMT);
    histlog_col_if_changed('CSAFEENEEDEDAMT',
      :old.CSAFEENEEDEDAMT,:new.CSAFEENEEDEDAMT);
    histlog_col_if_changed('CDCFEENEEDEDAMT',
      :old.CDCFEENEEDEDAMT,:new.CDCFEENEEDEDAMT);
    histlog_col_if_changed('FEENEEDEDAMT',
      :old.FEENEEDEDAMT,:new.FEENEEDEDAMT);
    histlog_col_if_changed('LATEFEENEEDEDAMT',
      :old.LATEFEENEEDEDAMT,:new.LATEFEENEEDEDAMT);
    histlog_col_if_changed('CSADUEFROMBORRAMT',
      :old.CSADUEFROMBORRAMT,:new.CSADUEFROMBORRAMT);
    histlog_col_if_changed('CDCDUEFROMBORRAMT',
      :old.CDCDUEFROMBORRAMT,:new.CDCDUEFROMBORRAMT);
    histlog_col_if_changed('DUEFROMBORRAMT',
      :old.DUEFROMBORRAMT,:new.DUEFROMBORRAMT);
    histlog_col_if_changed('CSABEHINDAMT',
      :old.CSABEHINDAMT,:new.CSABEHINDAMT);
    histlog_col_if_changed('CDCBEHINDAMT',
      :old.CDCBEHINDAMT,:new.CDCBEHINDAMT);
    histlog_col_if_changed('BEHINDAMT',
      :old.BEHINDAMT,:new.BEHINDAMT);
    histlog_col_if_changed('BORRFEEAMT',
      :old.BORRFEEAMT,:new.BORRFEEAMT);
    histlog_col_if_changed('CDCFEEDISBAMT',
      :old.CDCFEEDISBAMT,:new.CDCFEEDISBAMT);
    histlog_col_if_changed('AMORTCDCAMT',
      :old.AMORTCDCAMT,:new.AMORTCDCAMT);
    histlog_col_if_changed('AMORTSBAAMT',
      :old.AMORTSBAAMT,:new.AMORTSBAAMT);
    histlog_col_if_changed('AMORTCSAAMT',
      :old.AMORTCSAAMT,:new.AMORTCSAAMT);
    histlog_col_if_changed('CDCPCT',
      :old.CDCPCT,:new.CDCPCT);
    histlog_col_if_changed('PREPAYPCT',
      :old.PREPAYPCT,:new.PREPAYPCT);
    histlog_col_if_changed('FEECALCAMT',
      :old.FEECALCAMT,:new.FEECALCAMT);
    histlog_col_if_changed('CSAFEECALCAMT',
      :old.CSAFEECALCAMT,:new.CSAFEECALCAMT);
    histlog_col_if_changed('CSAFEECALCONLYAMT',
      :old.CSAFEECALCONLYAMT,:new.CSAFEECALCONLYAMT);
    histlog_col_if_changed('CDCFEECALCONLYAMT',
      :old.CDCFEECALCONLYAMT,:new.CDCFEECALCONLYAMT);
    histlog_col_if_changed('FEECALCONLYAMT',
      :old.FEECALCONLYAMT,:new.FEECALCONLYAMT);
    histlog_col_if_changed('FIVEYRADJ',
      :old.FIVEYRADJ,:new.FIVEYRADJ);
    histlog_col_if_changed('FIVEYRADJCSAAMT',
      :old.FIVEYRADJCSAAMT,:new.FIVEYRADJCSAAMT);
    histlog_col_if_changed('FIVEYRADJCDCAMT',
      :old.FIVEYRADJCDCAMT,:new.FIVEYRADJCDCAMT);
    histlog_col_if_changed('FIVEYRADJSBAAMT',
      :old.FIVEYRADJSBAAMT,:new.FIVEYRADJSBAAMT);
    histlog_col_if_changed('BALCALC1AMT',
      :old.BALCALC1AMT,:new.BALCALC1AMT);
    histlog_col_if_changed('BALCALC2AMT',
      :old.BALCALC2AMT,:new.BALCALC2AMT);
    histlog_col_if_changed('BALCALC3AMT',
      :old.BALCALC3AMT,:new.BALCALC3AMT);
    histlog_col_if_changed('BALCALC4AMT',
      :old.BALCALC4AMT,:new.BALCALC4AMT);
    histlog_col_if_changed('BALCALC5AMT',
      :old.BALCALC5AMT,:new.BALCALC5AMT);
    histlog_col_if_changed('BALCALC6AMT',
      :old.BALCALC6AMT,:new.BALCALC6AMT);
    histlog_col_if_changed('PRINAMT',
      :old.PRINAMT,:new.PRINAMT);
    histlog_col_if_changed('PRIN2AMT',
      :old.PRIN2AMT,:new.PRIN2AMT);
    histlog_col_if_changed('PRIN3AMT',
      :old.PRIN3AMT,:new.PRIN3AMT);
    histlog_col_if_changed('PRIN4AMT',
      :old.PRIN4AMT,:new.PRIN4AMT);
    histlog_col_if_changed('PRIN5AMT',
      :old.PRIN5AMT,:new.PRIN5AMT);
    histlog_col_if_changed('PRIN6AMT',
      :old.PRIN6AMT,:new.PRIN6AMT);
    histlog_col_if_changed('PRINPROJAMT',
      :old.PRINPROJAMT,:new.PRINPROJAMT);
    histlog_col_if_changed('PIPROJAMT',
      :old.PIPROJAMT,:new.PIPROJAMT);
    histlog_col_if_changed('INTAMT',
      :old.INTAMT,:new.INTAMT);
    histlog_col_if_changed('INT2AMT',
      :old.INT2AMT,:new.INT2AMT);
    histlog_col_if_changed('INT3AMT',
      :old.INT3AMT,:new.INT3AMT);
    histlog_col_if_changed('INT4AMT',
      :old.INT4AMT,:new.INT4AMT);
    histlog_col_if_changed('INT5AMT',
      :old.INT5AMT,:new.INT5AMT);
    histlog_col_if_changed('INT6AMT',
      :old.INT6AMT,:new.INT6AMT);
    histlog_col_if_changed('ESCROWAMT',
      :old.ESCROWAMT,:new.ESCROWAMT);
    histlog_col_if_changed('PI6MOAMT',
      :old.PI6MOAMT,:new.PI6MOAMT);
    histlog_col_if_changed('GFDAMT',
      :old.GFDAMT,:new.GFDAMT);
    histlog_col_if_changed('OPENGNTYAMT',
      :old.OPENGNTYAMT,:new.OPENGNTYAMT);
    histlog_col_if_changed('OPENGNTYERRAMT',
      :old.OPENGNTYERRAMT,:new.OPENGNTYERRAMT);
    histlog_col_if_changed('OPENGNTYREGAMT',
      :old.OPENGNTYREGAMT,:new.OPENGNTYREGAMT);
    histlog_col_if_changed('OPENGNTYUNALLOCAMT',
      :old.OPENGNTYUNALLOCAMT,:new.OPENGNTYUNALLOCAMT);
    histlog_col_if_changed('UNALLOCAMT',
      :old.UNALLOCAMT,:new.UNALLOCAMT);
    histlog_col_if_changed('USERLCK',
      :old.USERLCK,:new.USERLCK);
    histlog_col_if_changed('TIMESTAMPFLD',
      to_char(:old.TIMESTAMPFLD,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.TIMESTAMPFLD,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CREATUSERID',
      :old.CREATUSERID,:new.CREATUSERID);
    histlog_col_if_changed('CREATDT',
      to_char(:old.CREATDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CREATDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('LASTUPDTUSERID',
      :old.LASTUPDTUSERID,:new.LASTUPDTUSERID);
    histlog_col_if_changed('LASTUPDTDT',
      to_char(:old.LASTUPDTDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.LASTUPDTDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CDCSBAFEEDISBAMT',
      :old.CDCSBAFEEDISBAMT,:new.CDCSBAFEEDISBAMT);
    -- write out any accumulated logdtl recs with bulk insert
    if logdtl_ctr>1 then
      forall r in 2..logdtl_ctr insert into stgcsa.stgupdtlogdtltbl values logdtl(r);
    end if;
  end if;  
  -- end of history logging section
end;

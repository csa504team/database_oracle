create or replace trigger STGCSA.PRPMFWIREPOSTINGTRG
    after insert or update or delete ON STGCSA.PRPMFWIREPOSTINGTBL
    REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE
 /*
  Created on: 2019-05-04 08:25:13
  Created by: GENR
  Crerated from template csatrig_template v3.4 3 May 2019 on 2019-05-04 08:25:13
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
    Trigger handles errors by not updating and raising error -20100.
    Caller should handle failing SQL statement, including recording.
*/
  calling_module varchar2(80);
  tabowner varchar2(32):='STGCSA';
  tabname varchar2(32):='PRPMFWIREPOSTINGTBL';
  -- vars for history logging
  hst stgupdtlogtbl%rowtype;
  TYPE logdtl_typ is table of STGCSA.stgupdtlogdtltbl%rowtype;
  logdtl logdtl_typ:=logdtl_typ();
  logdtl_ctr number:=0;
procedure histlog_col_if_changed(colnam varchar2, oldval varchar2, newval varchar2) is
begin
  If (inserting and newval is not null)
    or (deleting and oldval is not null)
    or (updating and 
         (oldval<>newval 
          or (oldval is null and newval is not null)
          or (oldval is not null and newval is null)
         )
       )                  
  then 
    logdtl.extend;
    logdtl_ctr:=logdtl_ctr+1;
    logdtl(logdtl_ctr):=logdtl(1);
    logdtl(logdtl_ctr).colname:=colnam;
    if inserting or updating then 
      logdtl(logdtl_ctr).newvalue:=newval; 
    else
      logdtl(logdtl_ctr).newvalue:=null;       
    end if;  
    if updating or deleting then 
      logdtl(logdtl_ctr).oldvalue:=oldval;
    else
      logdtl(logdtl_ctr).oldvalue:=null;      
    end if;
  end if;
end;
--
--
begin
  select SYS_CONTEXT ('USERENV', 'MODULE') into calling_module from dual;
  if calling_module='Data Pump Worker' then return; end if;
  --
  -- History logging (HST)... initialize 
  -- Skip if datahist logging is explicitly turned fo via switch in runtime
  if runtime.datahist_logging_on_YN='Y' then
    SELECT LOGENTRYIDSEQ.NEXTVAL, sysdate 
      into hst.updtlogid, hst.updtdt FROM DUAL;
    -- set aside table pk, set managed variables
    hst.csa_sid:=runtime.csa_session_id;
    hst.loannmb:=null;
    hst.tablenm:='PRPMFWIREPOSTINGTBL';
    hst.loannmb:=nvl(:old.loannmb,:new.loannmb);
      
    hst.updtuserid:=:new.lastupdtuserid;
    if deleting=true then hst.updtuserid:=runtime.csa_userid; end if;
    if hst.updtuserid is null then hst.updtuserid:=user; end if;
    case
      when inserting=true then hst.upd_action:='I'; 
      when updating=true then hst.upd_action:='U';
      when deleting=true then hst.upd_action:='D';
    end case; 
    -- now set composit PK for row being updated  
    hst.pk:=null         
      ||rpad(to_char(nvl(:new.WIREPOSTINGID,:old.WIREPOSTINGID)),22)
;  
    -- insert log main row
    INSERT INTO stgupdtlogtbl values HST;
    --
    -- Now column value logging for changed columns
    --   Initialize logdtl collection.
    logdtl.extend;
    logdtl_ctr:=1;
    logdtl(logdtl_ctr).updtlogfk:=hst.updtlogid;
    logdtl(logdtl_ctr).oldvalue:=null;
    logdtl(logdtl_ctr).newvalue:=null;
    histlog_col_if_changed('LOANNMB',
      :old.LOANNMB,:new.LOANNMB);
    histlog_col_if_changed('BORRFEEAMT',
      :old.BORRFEEAMT,:new.BORRFEEAMT);
    histlog_col_if_changed('BORRPAIDTHRUDT',
      :old.BORRPAIDTHRUDT,:new.BORRPAIDTHRUDT);
    histlog_col_if_changed('CSAFEEAMT',
      :old.CSAFEEAMT,:new.CSAFEEAMT);
    histlog_col_if_changed('CSAPAIDTHRUDT',
      :old.CSAPAIDTHRUDT,:new.CSAPAIDTHRUDT);
    histlog_col_if_changed('CDCFEEAPPLAMT',
      :old.CDCFEEAPPLAMT,:new.CDCFEEAPPLAMT);
    histlog_col_if_changed('CDCFEEDISBAMT',
      :old.CDCFEEDISBAMT,:new.CDCFEEDISBAMT);
    histlog_col_if_changed('CDCDISBAMT',
      :old.CDCDISBAMT,:new.CDCDISBAMT);
    histlog_col_if_changed('CDCPAIDTHRUDT',
      :old.CDCPAIDTHRUDT,:new.CDCPAIDTHRUDT);
    histlog_col_if_changed('INTPAIDTHRUDT',
      :old.INTPAIDTHRUDT,:new.INTPAIDTHRUDT);
    histlog_col_if_changed('ADJTOAPPLINTAMT',
      :old.ADJTOAPPLINTAMT,:new.ADJTOAPPLINTAMT);
    histlog_col_if_changed('ADJTOAPPLPRINAMT',
      :old.ADJTOAPPLPRINAMT,:new.ADJTOAPPLPRINAMT);
    histlog_col_if_changed('LATEFEEADJAMT',
      :old.LATEFEEADJAMT,:new.LATEFEEADJAMT);
    histlog_col_if_changed('UNALLOCAMT',
      :old.UNALLOCAMT,:new.UNALLOCAMT);
    histlog_col_if_changed('CURBALAMT',
      :old.CURBALAMT,:new.CURBALAMT);
    histlog_col_if_changed('PREPAYAMT',
      :old.PREPAYAMT,:new.PREPAYAMT);
    histlog_col_if_changed('WIRECMNT',
      :old.WIRECMNT,:new.WIRECMNT);
    histlog_col_if_changed('ADJTYP',
      :old.ADJTYP,:new.ADJTYP);
    histlog_col_if_changed('OVRWRITECD',
      :old.OVRWRITECD,:new.OVRWRITECD);
    histlog_col_if_changed('POSTINGTYP',
      :old.POSTINGTYP,:new.POSTINGTYP);
    histlog_col_if_changed('POSTINGSCHDLDT',
      to_char(:old.POSTINGSCHDLDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.POSTINGSCHDLDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('POSTINGACTUALDT',
      to_char(:old.POSTINGACTUALDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.POSTINGACTUALDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('WIREPOSTINGID',
      :old.WIREPOSTINGID,:new.WIREPOSTINGID);
    histlog_col_if_changed('TRANSID',
      :old.TRANSID,:new.TRANSID);
    histlog_col_if_changed('MFSTATCD',
      :old.MFSTATCD,:new.MFSTATCD);
    histlog_col_if_changed('ACHDEL',
      :old.ACHDEL,:new.ACHDEL);
    histlog_col_if_changed('OVRPAYAMT',
      :old.OVRPAYAMT,:new.OVRPAYAMT);
    histlog_col_if_changed('CDCREPDAMT',
      :old.CDCREPDAMT,:new.CDCREPDAMT);
    histlog_col_if_changed('CSAREPDAMT',
      :old.CSAREPDAMT,:new.CSAREPDAMT);
    histlog_col_if_changed('REPDAMT',
      :old.REPDAMT,:new.REPDAMT);
    histlog_col_if_changed('PREPAYDT',
      to_char(:old.PREPAYDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.PREPAYDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('SEMIANDT',
      to_char(:old.SEMIANDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.SEMIANDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('ZEROBALAMT',
      :old.ZEROBALAMT,:new.ZEROBALAMT);
    histlog_col_if_changed('SEMIANCDCNMB',
      :old.SEMIANCDCNMB,:new.SEMIANCDCNMB);
    histlog_col_if_changed('SEMIANNOTEBALAMT',
      :old.SEMIANNOTEBALAMT,:new.SEMIANNOTEBALAMT);
    histlog_col_if_changed('SEMIANWIREAMT',
      :old.SEMIANWIREAMT,:new.SEMIANWIREAMT);
    histlog_col_if_changed('SEMIANPRINACCRAMT',
      :old.SEMIANPRINACCRAMT,:new.SEMIANPRINACCRAMT);
    histlog_col_if_changed('SEMIANINTACCRAMT',
      :old.SEMIANINTACCRAMT,:new.SEMIANINTACCRAMT);
    histlog_col_if_changed('SEMIANOPENGNTYAMT',
      :old.SEMIANOPENGNTYAMT,:new.SEMIANOPENGNTYAMT);
    histlog_col_if_changed('SEMIANOPENGNTYERRAMT',
      :old.SEMIANOPENGNTYERRAMT,:new.SEMIANOPENGNTYERRAMT);
    histlog_col_if_changed('SEMIANOPENGNTYREMAINAMT',
      :old.SEMIANOPENGNTYREMAINAMT,:new.SEMIANOPENGNTYREMAINAMT);
    histlog_col_if_changed('SEMIANPIPYMTAFT6MOAMT',
      :old.SEMIANPIPYMTAFT6MOAMT,:new.SEMIANPIPYMTAFT6MOAMT);
    histlog_col_if_changed('SEMIANPISHORTOVRAMT',
      :old.SEMIANPISHORTOVRAMT,:new.SEMIANPISHORTOVRAMT);
    histlog_col_if_changed('SEMIANESCROWPRIORPYMTAMT',
      :old.SEMIANESCROWPRIORPYMTAMT,:new.SEMIANESCROWPRIORPYMTAMT);
    histlog_col_if_changed('SEMIANDBENTRBALAMT',
      :old.SEMIANDBENTRBALAMT,:new.SEMIANDBENTRBALAMT);
    histlog_col_if_changed('SEMIANPREPAYPREMAMT',
      :old.SEMIANPREPAYPREMAMT,:new.SEMIANPREPAYPREMAMT);
    histlog_col_if_changed('SEMIANAMT',
      :old.SEMIANAMT,:new.SEMIANAMT);
    histlog_col_if_changed('SEMIANSBADUEFROMBORRAMT',
      :old.SEMIANSBADUEFROMBORRAMT,:new.SEMIANSBADUEFROMBORRAMT);
    histlog_col_if_changed('SEMIANSBABEHINDAMT',
      :old.SEMIANSBABEHINDAMT,:new.SEMIANSBABEHINDAMT);
    histlog_col_if_changed('SEMIANSBAACCRAMT',
      :old.SEMIANSBAACCRAMT,:new.SEMIANSBAACCRAMT);
    histlog_col_if_changed('SEMIANSBAPRJCTDAMT',
      :old.SEMIANSBAPRJCTDAMT,:new.SEMIANSBAPRJCTDAMT);
    histlog_col_if_changed('SEMIANCSADUEFROMBORRAMT',
      :old.SEMIANCSADUEFROMBORRAMT,:new.SEMIANCSADUEFROMBORRAMT);
    histlog_col_if_changed('SEMIANCSABEHINDAMT',
      :old.SEMIANCSABEHINDAMT,:new.SEMIANCSABEHINDAMT);
    histlog_col_if_changed('SEMIANCSAACCRAMT',
      :old.SEMIANCSAACCRAMT,:new.SEMIANCSAACCRAMT);
    histlog_col_if_changed('SEMIANCSAPRJCTDAMT',
      :old.SEMIANCSAPRJCTDAMT,:new.SEMIANCSAPRJCTDAMT);
    histlog_col_if_changed('SEMIANCDCDUEFROMBORRAMT',
      :old.SEMIANCDCDUEFROMBORRAMT,:new.SEMIANCDCDUEFROMBORRAMT);
    histlog_col_if_changed('SEMIANCDCBEHINDAMT',
      :old.SEMIANCDCBEHINDAMT,:new.SEMIANCDCBEHINDAMT);
    histlog_col_if_changed('SEMIANCDCACCRAMT',
      :old.SEMIANCDCACCRAMT,:new.SEMIANCDCACCRAMT);
    histlog_col_if_changed('SEMIANACCRINTONDELAMT',
      :old.SEMIANACCRINTONDELAMT,:new.SEMIANACCRINTONDELAMT);
    histlog_col_if_changed('SEMIANLATEFEEAMT',
      :old.SEMIANLATEFEEAMT,:new.SEMIANLATEFEEAMT);
    histlog_col_if_changed('SEMIANPROOFAMT',
      :old.SEMIANPROOFAMT,:new.SEMIANPROOFAMT);
    histlog_col_if_changed('TIMESTAMPFLD',
      to_char(:old.TIMESTAMPFLD,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.TIMESTAMPFLD,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CREATUSERID',
      :old.CREATUSERID,:new.CREATUSERID);
    histlog_col_if_changed('CREATDT',
      to_char(:old.CREATDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CREATDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('LASTUPDTUSERID',
      :old.LASTUPDTUSERID,:new.LASTUPDTUSERID);
    histlog_col_if_changed('LASTUPDTDT',
      to_char(:old.LASTUPDTDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.LASTUPDTDT,'YYYY-MM-DD HH24:MI:SS'));
    -- write out any accumulated logdtl recs with bulk insert
    if logdtl_ctr>1 then
      forall r in 2..logdtl_ctr insert into stgcsa.stgupdtlogdtltbl values logdtl(r);
    end if;
  end if;  
  -- end of history logging section
end;

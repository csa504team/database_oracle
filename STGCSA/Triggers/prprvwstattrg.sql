create or replace trigger STGCSA.PRPRVWSTATTRG
    after insert or update or delete ON STGCSA.PRPRVWSTATTBL
    REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE
 /*
  Created on: 2019-05-04 08:25:15
  Created by: GENR
  Crerated from template csatrig_template v3.4 3 May 2019 on 2019-05-04 08:25:15
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
    Trigger handles errors by not updating and raising error -20100.
    Caller should handle failing SQL statement, including recording.
*/
  calling_module varchar2(80);
  tabowner varchar2(32):='STGCSA';
  tabname varchar2(32):='PRPRVWSTATTBL';
  -- vars for history logging
  hst stgupdtlogtbl%rowtype;
  TYPE logdtl_typ is table of STGCSA.stgupdtlogdtltbl%rowtype;
  logdtl logdtl_typ:=logdtl_typ();
  logdtl_ctr number:=0;
procedure histlog_col_if_changed(colnam varchar2, oldval varchar2, newval varchar2) is
begin
  If (inserting and newval is not null)
    or (deleting and oldval is not null)
    or (updating and 
         (oldval<>newval 
          or (oldval is null and newval is not null)
          or (oldval is not null and newval is null)
         )
       )                  
  then 
    logdtl.extend;
    logdtl_ctr:=logdtl_ctr+1;
    logdtl(logdtl_ctr):=logdtl(1);
    logdtl(logdtl_ctr).colname:=colnam;
    if inserting or updating then 
      logdtl(logdtl_ctr).newvalue:=newval; 
    else
      logdtl(logdtl_ctr).newvalue:=null;       
    end if;  
    if updating or deleting then 
      logdtl(logdtl_ctr).oldvalue:=oldval;
    else
      logdtl(logdtl_ctr).oldvalue:=null;      
    end if;
  end if;
end;
--
--
begin
  select SYS_CONTEXT ('USERENV', 'MODULE') into calling_module from dual;
  if calling_module='Data Pump Worker' then return; end if;
  --
  -- History logging (HST)... initialize 
  -- Skip if datahist logging is explicitly turned fo via switch in runtime
  if runtime.datahist_logging_on_YN='Y' then
    SELECT LOGENTRYIDSEQ.NEXTVAL, sysdate 
      into hst.updtlogid, hst.updtdt FROM DUAL;
    -- set aside table pk, set managed variables
    hst.csa_sid:=runtime.csa_session_id;
    hst.loannmb:=null;
    hst.tablenm:='PRPRVWSTATTBL';
    hst.loannmb:=nvl(:old.loannmb,:new.loannmb);
      
    hst.updtuserid:=:new.lastupdtuserid;
    if deleting=true then hst.updtuserid:=runtime.csa_userid; end if;
    if hst.updtuserid is null then hst.updtuserid:=user; end if;
    case
      when inserting=true then hst.upd_action:='I'; 
      when updating=true then hst.upd_action:='U';
      when deleting=true then hst.upd_action:='D';
    end case; 
    -- now set composit PK for row being updated  
    hst.pk:=null         
      ||rpad(to_char(nvl(:new.PRPRVWSTATID,:old.PRPRVWSTATID)),22)
;  
    -- insert log main row
    INSERT INTO stgupdtlogtbl values HST;
    --
    -- Now column value logging for changed columns
    --   Initialize logdtl collection.
    logdtl.extend;
    logdtl_ctr:=1;
    logdtl(logdtl_ctr).updtlogfk:=hst.updtlogid;
    logdtl(logdtl_ctr).oldvalue:=null;
    logdtl(logdtl_ctr).newvalue:=null;
    histlog_col_if_changed('PRPRVWSTATID',
      :old.PRPRVWSTATID,:new.PRPRVWSTATID);
    histlog_col_if_changed('LOANNMB',
      :old.LOANNMB,:new.LOANNMB);
    histlog_col_if_changed('TRANSID',
      :old.TRANSID,:new.TRANSID);
    histlog_col_if_changed('TRANSTYPID',
      :old.TRANSTYPID,:new.TRANSTYPID);
    histlog_col_if_changed('INITIALPOSTRVW',
      :old.INITIALPOSTRVW,:new.INITIALPOSTRVW);
    histlog_col_if_changed('INITIALPOSTRVWBY',
      :old.INITIALPOSTRVWBY,:new.INITIALPOSTRVWBY);
    histlog_col_if_changed('SCNDPOSTRVW',
      :old.SCNDPOSTRVW,:new.SCNDPOSTRVW);
    histlog_col_if_changed('SCNDPOSTRVWBY',
      :old.SCNDPOSTRVWBY,:new.SCNDPOSTRVWBY);
    histlog_col_if_changed('THIRDPOSTRVW',
      :old.THIRDPOSTRVW,:new.THIRDPOSTRVW);
    histlog_col_if_changed('THIRDPOSTRVWBY',
      :old.THIRDPOSTRVWBY,:new.THIRDPOSTRVWBY);
    histlog_col_if_changed('FOURTHPOSTRVW',
      :old.FOURTHPOSTRVW,:new.FOURTHPOSTRVW);
    histlog_col_if_changed('FOURTHPOSTRVWBY',
      :old.FOURTHPOSTRVWBY,:new.FOURTHPOSTRVWBY);
    histlog_col_if_changed('FIFTHPOSTRVW',
      :old.FIFTHPOSTRVW,:new.FIFTHPOSTRVW);
    histlog_col_if_changed('FIFTHPOSTRVWBY',
      :old.FIFTHPOSTRVWBY,:new.FIFTHPOSTRVWBY);
    histlog_col_if_changed('SIXTHPOSTRVW',
      :old.SIXTHPOSTRVW,:new.SIXTHPOSTRVW);
    histlog_col_if_changed('SIXTHPOSTRVWBY',
      :old.SIXTHPOSTRVWBY,:new.SIXTHPOSTRVWBY);
    histlog_col_if_changed('SIXTHMOPOSTRVW',
      :old.SIXTHMOPOSTRVW,:new.SIXTHMOPOSTRVW);
    histlog_col_if_changed('SIXTHMOPOSTRVWBY',
      :old.SIXTHMOPOSTRVWBY,:new.SIXTHMOPOSTRVWBY);
    histlog_col_if_changed('SIXTHMOPOST2RVW',
      :old.SIXTHMOPOST2RVW,:new.SIXTHMOPOST2RVW);
    histlog_col_if_changed('SIXTHMOPOST2RVWBY',
      :old.SIXTHMOPOST2RVWBY,:new.SIXTHMOPOST2RVWBY);
    histlog_col_if_changed('SIXTHMOPOST3RVW',
      :old.SIXTHMOPOST3RVW,:new.SIXTHMOPOST3RVW);
    histlog_col_if_changed('SIXTHMOPOST3RVWBY',
      :old.SIXTHMOPOST3RVWBY,:new.SIXTHMOPOST3RVWBY);
    histlog_col_if_changed('SIXTHMOPOST4RVW',
      :old.SIXTHMOPOST4RVW,:new.SIXTHMOPOST4RVW);
    histlog_col_if_changed('SIXTHMOPOST4RVWBY',
      :old.SIXTHMOPOST4RVWBY,:new.SIXTHMOPOST4RVWBY);
    histlog_col_if_changed('SIXTHMOPOST5RVW',
      :old.SIXTHMOPOST5RVW,:new.SIXTHMOPOST5RVW);
    histlog_col_if_changed('SIXTHMOPOST5RVWBY',
      :old.SIXTHMOPOST5RVWBY,:new.SIXTHMOPOST5RVWBY);
    histlog_col_if_changed('THISTRVW',
      :old.THISTRVW,:new.THISTRVW);
    histlog_col_if_changed('THISTRVWBY',
      :old.THISTRVWBY,:new.THISTRVWBY);
    histlog_col_if_changed('FEEVAL',
      :old.FEEVAL,:new.FEEVAL);
    histlog_col_if_changed('FEEVALBY',
      :old.FEEVALBY,:new.FEEVALBY);
    histlog_col_if_changed('RVWSBMT',
      :old.RVWSBMT,:new.RVWSBMT);
    histlog_col_if_changed('RVWSBMTBY',
      :old.RVWSBMTBY,:new.RVWSBMTBY);
    histlog_col_if_changed('RVWSBMTDT',
      to_char(:old.RVWSBMTDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.RVWSBMTDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('WORKUPVAL',
      :old.WORKUPVAL,:new.WORKUPVAL);
    histlog_col_if_changed('WORKUPVALBY',
      :old.WORKUPVALBY,:new.WORKUPVALBY);
    histlog_col_if_changed('THISTVAL',
      :old.THISTVAL,:new.THISTVAL);
    histlog_col_if_changed('THISTVALBY',
      :old.THISTVALBY,:new.THISTVALBY);
    histlog_col_if_changed('VALSBMT',
      :old.VALSBMT,:new.VALSBMT);
    histlog_col_if_changed('VALSBMTBY',
      :old.VALSBMTBY,:new.VALSBMTBY);
    histlog_col_if_changed('VALSBMTDT',
      to_char(:old.VALSBMTDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.VALSBMTDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('PROOFVAL',
      :old.PROOFVAL,:new.PROOFVAL);
    histlog_col_if_changed('PROOFVALBY',
      :old.PROOFVALBY,:new.PROOFVALBY);
    histlog_col_if_changed('TIMESTAMPFLD',
      to_char(:old.TIMESTAMPFLD,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.TIMESTAMPFLD,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CREATUSERID',
      :old.CREATUSERID,:new.CREATUSERID);
    histlog_col_if_changed('CREATDT',
      to_char(:old.CREATDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CREATDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('LASTUPDTUSERID',
      :old.LASTUPDTUSERID,:new.LASTUPDTUSERID);
    histlog_col_if_changed('LASTUPDTDT',
      to_char(:old.LASTUPDTDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.LASTUPDTDT,'YYYY-MM-DD HH24:MI:SS'));
    -- write out any accumulated logdtl recs with bulk insert
    if logdtl_ctr>1 then
      forall r in 2..logdtl_ctr insert into stgcsa.stgupdtlogdtltbl values logdtl(r);
    end if;
  end if;  
  -- end of history logging section
end;

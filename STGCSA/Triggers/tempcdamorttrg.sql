create or replace trigger STGCSA.TEMPCDAMORTTRG
    after insert or update or delete ON STGCSA.TEMPCDAMORTTBL
    REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE
 /*
  Created on: 2019-05-04 08:25:43
  Created by: GENR
  Crerated from template csatrig_template v3.4 3 May 2019 on 2019-05-04 08:25:43
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
    Trigger handles errors by not updating and raising error -20100.
    Caller should handle failing SQL statement, including recording.
*/
  calling_module varchar2(80);
  tabowner varchar2(32):='STGCSA';
  tabname varchar2(32):='TEMPCDAMORTTBL';
  -- vars for history logging
  hst stgupdtlogtbl%rowtype;
  TYPE logdtl_typ is table of STGCSA.stgupdtlogdtltbl%rowtype;
  logdtl logdtl_typ:=logdtl_typ();
  logdtl_ctr number:=0;
procedure histlog_col_if_changed(colnam varchar2, oldval varchar2, newval varchar2) is
begin
  If (inserting and newval is not null)
    or (deleting and oldval is not null)
    or (updating and 
         (oldval<>newval 
          or (oldval is null and newval is not null)
          or (oldval is not null and newval is null)
         )
       )                  
  then 
    logdtl.extend;
    logdtl_ctr:=logdtl_ctr+1;
    logdtl(logdtl_ctr):=logdtl(1);
    logdtl(logdtl_ctr).colname:=colnam;
    if inserting or updating then 
      logdtl(logdtl_ctr).newvalue:=newval; 
    else
      logdtl(logdtl_ctr).newvalue:=null;       
    end if;  
    if updating or deleting then 
      logdtl(logdtl_ctr).oldvalue:=oldval;
    else
      logdtl(logdtl_ctr).oldvalue:=null;      
    end if;
  end if;
end;
--
--
begin
  select SYS_CONTEXT ('USERENV', 'MODULE') into calling_module from dual;
  if calling_module='Data Pump Worker' then return; end if;
  --
  -- History logging (HST)... initialize 
  -- Skip if datahist logging is explicitly turned fo via switch in runtime
  if runtime.datahist_logging_on_YN='Y' then
    SELECT LOGENTRYIDSEQ.NEXTVAL, sysdate 
      into hst.updtlogid, hst.updtdt FROM DUAL;
    -- set aside table pk, set managed variables
    hst.csa_sid:=runtime.csa_session_id;
    hst.loannmb:=null;
    hst.tablenm:='TEMPCDAMORTTBL';
    hst.loannmb:=nvl(:old.loannmb,:new.loannmb);
      
    hst.updtuserid:=:new.lastupdtuserid;
    if deleting=true then hst.updtuserid:=runtime.csa_userid; end if;
    if hst.updtuserid is null then hst.updtuserid:=user; end if;
    case
      when inserting=true then hst.upd_action:='I'; 
      when updating=true then hst.upd_action:='U';
      when deleting=true then hst.upd_action:='D';
    end case; 
    -- now set composit PK for row being updated  
    hst.pk:=null         
      ||rpad(to_char(nvl(:new.CDAMORTID,:old.CDAMORTID)),22)
;  
    -- insert log main row
    INSERT INTO stgupdtlogtbl values HST;
    --
    -- Now column value logging for changed columns
    --   Initialize logdtl collection.
    logdtl.extend;
    logdtl_ctr:=1;
    logdtl(logdtl_ctr).updtlogfk:=hst.updtlogid;
    logdtl(logdtl_ctr).oldvalue:=null;
    logdtl(logdtl_ctr).newvalue:=null;
    histlog_col_if_changed('CDAMORTID',
      :old.CDAMORTID,:new.CDAMORTID);
    histlog_col_if_changed('LOANNMB',
      :old.LOANNMB,:new.LOANNMB);
    histlog_col_if_changed('PD',
      to_char(:old.PD,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.PD,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('PD_BEGIN_PRIN',
      :old.PD_BEGIN_PRIN,:new.PD_BEGIN_PRIN);
    histlog_col_if_changed('PYMT_AMT',
      :old.PYMT_AMT,:new.PYMT_AMT);
    histlog_col_if_changed('PRIN_AMT',
      :old.PRIN_AMT,:new.PRIN_AMT);
    histlog_col_if_changed('INT_AMT',
      :old.INT_AMT,:new.INT_AMT);
    histlog_col_if_changed('CDC_AMT',
      :old.CDC_AMT,:new.CDC_AMT);
    histlog_col_if_changed('SBA_AMT',
      :old.SBA_AMT,:new.SBA_AMT);
    histlog_col_if_changed('CSA_AMT',
      :old.CSA_AMT,:new.CSA_AMT);
    histlog_col_if_changed('LATE_AMT',
      :old.LATE_AMT,:new.LATE_AMT);
    histlog_col_if_changed('PRIN_BALANCE',
      :old.PRIN_BALANCE,:new.PRIN_BALANCE);
    histlog_col_if_changed('PRIN_NEEDED',
      :old.PRIN_NEEDED,:new.PRIN_NEEDED);
    histlog_col_if_changed('INT_NEEDED',
      :old.INT_NEEDED,:new.INT_NEEDED);
    histlog_col_if_changed('CDC_FEE_NEEDED',
      :old.CDC_FEE_NEEDED,:new.CDC_FEE_NEEDED);
    histlog_col_if_changed('SBA_FEE_NEEDED',
      :old.SBA_FEE_NEEDED,:new.SBA_FEE_NEEDED);
    histlog_col_if_changed('CSA_FEE_NEEDED',
      :old.CSA_FEE_NEEDED,:new.CSA_FEE_NEEDED);
    histlog_col_if_changed('LATE_FEE_NEEDED',
      :old.LATE_FEE_NEEDED,:new.LATE_FEE_NEEDED);
    histlog_col_if_changed('UNALLOCATED_AMT',
      :old.UNALLOCATED_AMT,:new.UNALLOCATED_AMT);
    histlog_col_if_changed('PRIN_PYMT',
      :old.PRIN_PYMT,:new.PRIN_PYMT);
    histlog_col_if_changed('INT_PYMT',
      :old.INT_PYMT,:new.INT_PYMT);
    histlog_col_if_changed('TOTAL_PRIN',
      :old.TOTAL_PRIN,:new.TOTAL_PRIN);
    histlog_col_if_changed('STATUS',
      :old.STATUS,:new.STATUS);
    histlog_col_if_changed('TYPENM',
      :old.TYPENM,:new.TYPENM);
    histlog_col_if_changed('FIRSTPAYMENTDT',
      to_char(:old.FIRSTPAYMENTDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.FIRSTPAYMENTDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('LASTPAYMENTDT',
      to_char(:old.LASTPAYMENTDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.LASTPAYMENTDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CREATUSERID',
      :old.CREATUSERID,:new.CREATUSERID);
    histlog_col_if_changed('CREATDT',
      to_char(:old.CREATDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CREATDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('LASTUPDTUSERID',
      :old.LASTUPDTUSERID,:new.LASTUPDTUSERID);
    histlog_col_if_changed('LASTUPDTDT',
      to_char(:old.LASTUPDTDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.LASTUPDTDT,'YYYY-MM-DD HH24:MI:SS'));
    -- write out any accumulated logdtl recs with bulk insert
    if logdtl_ctr>1 then
      forall r in 2..logdtl_ctr insert into stgcsa.stgupdtlogdtltbl values logdtl(r);
    end if;
  end if;  
  -- end of history logging section
end;

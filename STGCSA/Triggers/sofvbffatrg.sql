create or replace trigger STGCSA.SOFVBFFATRG
    after insert or update or delete ON STGCSA.SOFVBFFATBL
    REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE
 /*
  Created on: 2019-05-04 08:25:20
  Created by: GENR
  Crerated from template csatrig_template v3.4 3 May 2019 on 2019-05-04 08:25:20
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
    Trigger handles errors by not updating and raising error -20100.
    Caller should handle failing SQL statement, including recording.
*/
  calling_module varchar2(80);
  tabowner varchar2(32):='STGCSA';
  tabname varchar2(32):='SOFVBFFATBL';
  -- vars for sofvaudt support
  TYPE audrecs_typ is table of STGCSA.SOFVAUDTTBL%rowtype;
  audrecs audrecs_typ:=audrecs_typ();
  audrecs_ctr number:=0;
  -- vars for history logging
  hst stgupdtlogtbl%rowtype;
  TYPE logdtl_typ is table of STGCSA.stgupdtlogdtltbl%rowtype;
  logdtl logdtl_typ:=logdtl_typ();
  logdtl_ctr number:=0;
procedure histlog_col_if_changed(colnam varchar2, oldval varchar2, newval varchar2) is
begin
  If (inserting and newval is not null)
    or (deleting and oldval is not null)
    or (updating and 
         (oldval<>newval 
          or (oldval is null and newval is not null)
          or (oldval is not null and newval is null)
         )
       )                  
  then 
    logdtl.extend;
    logdtl_ctr:=logdtl_ctr+1;
    logdtl(logdtl_ctr):=logdtl(1);
    logdtl(logdtl_ctr).colname:=colnam;
    if inserting or updating then 
      logdtl(logdtl_ctr).newvalue:=newval; 
    else
      logdtl(logdtl_ctr).newvalue:=null;       
    end if;  
    if updating or deleting then 
      logdtl(logdtl_ctr).oldvalue:=oldval;
    else
      logdtl(logdtl_ctr).oldvalue:=null;      
    end if;
  end if;
end;
--
--
begin
  select SYS_CONTEXT ('USERENV', 'MODULE') into calling_module from dual;
  if calling_module='Data Pump Worker' then return; end if;
    --
  -- code to record changes to SOFVAUDTTBL
  -- initialize audrecs collection and first rec 
  -- first rec holds defaults and is not inserted
  -- Skip if audit logging is explicitly turned fo via switch in runtime
  if runtime.audit_logging_on_YN='Y' then
    audrecs.extend;
    select systimestamp,sysdate,sys_context('userenv','session_user')
      into audrecs(1).audttransdt, audrecs(1).CREATDT, audrecs(1).creatuserid from dual;
    audrecs_ctr:=1;   
    audrecs(1).LASTUPDTDT:=audrecs(audrecs_ctr).CREATDT;
    audrecs(1).lastupdtuserid:=audrecs(audrecs_ctr).creatuserid;
    
     
    if deleting then
      audrecs(1).audtrecrdkey:=:old.ASSUMPKEYLOANNMB;
    else 
      audrecs(1).audtrecrdkey:=:new.ASSUMPKEYLOANNMB;
    end if;     
    audrecs(1).audtseqnmb:=0;
    audrecs(1).AUDTOPRTRINITIAL:='  ';
    audrecs(1).AUDTTRMNLID:='    ';
    audrecs(1).AUDTEDTCD:='?';
    audrecs(1).AUDTUSERID:=nvl(:new.lastupdtuserid,:old.lastupdtuserid);
    if audrecs(1).AUDTUSERID is null then audrecs(audrecs_ctr).AUDTUSERID:=runtime.csa_userid; end if;
    if audrecs(1).AUDTUSERID is null then audrecs(audrecs_ctr).AUDTUSERID:=user; end if;  
    if inserting or updating then
      --
      -- record specific changed values to sofvaudttbl

      if inserting then  
        audrecs(audrecs_ctr).AUDTACTNCD:='A';
      else  
        audrecs(audrecs_ctr).AUDTACTNCD:='C';
      end if;
    if not (:new.ASSUMPNM=:old.ASSUMPNM 
                or (:new.ASSUMPNM is null and :old.ASSUMPNM is null)) 
      then  
        audrecs.extend;
        audrecs_ctr:=audrecs_ctr+1;
        audrecs(audrecs_ctr):=audrecs(1);
        audrecs(audrecs_ctr).audtseqnmb:=audrecs(audrecs_ctr-1).audtseqnmb+1;
        audrecs(audrecs_ctr).AUDTFLDNM:='ASNAME1 ';
        audrecs(audrecs_ctr).audtchngfile:='BGW9ASSU';
        if updating then
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:=:old.ASSUMPNM;
        else  
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:='$NULL$';
        end if;  
        audrecs(audrecs_ctr).AUDTAFTCNTNTS:=:new.ASSUMPNM;
      end if;
    if not (:new.ASSUMPMAILADRSTR1NM=:old.ASSUMPMAILADRSTR1NM 
                or (:new.ASSUMPMAILADRSTR1NM is null and :old.ASSUMPMAILADRSTR1NM is null)) 
      then  
        audrecs.extend;
        audrecs_ctr:=audrecs_ctr+1;
        audrecs(audrecs_ctr):=audrecs(1);
        audrecs(audrecs_ctr).audtseqnmb:=audrecs(audrecs_ctr-1).audtseqnmb+1;
        audrecs(audrecs_ctr).AUDTFLDNM:='ASSTREET';
        audrecs(audrecs_ctr).audtchngfile:='BGW9ASSU';
        if updating then
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:=:old.ASSUMPMAILADRSTR1NM;
        else  
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:='$NULL$';
        end if;  
        audrecs(audrecs_ctr).AUDTAFTCNTNTS:=:new.ASSUMPMAILADRSTR1NM;
      end if;
    if not (:new.ASSUMPMAILADRCTYNM=:old.ASSUMPMAILADRCTYNM 
                or (:new.ASSUMPMAILADRCTYNM is null and :old.ASSUMPMAILADRCTYNM is null)) 
      then  
        audrecs.extend;
        audrecs_ctr:=audrecs_ctr+1;
        audrecs(audrecs_ctr):=audrecs(1);
        audrecs(audrecs_ctr).audtseqnmb:=audrecs(audrecs_ctr-1).audtseqnmb+1;
        audrecs(audrecs_ctr).AUDTFLDNM:='ASCITY  ';
        audrecs(audrecs_ctr).audtchngfile:='BGW9ASSU';
        if updating then
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:=:old.ASSUMPMAILADRCTYNM;
        else  
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:='$NULL$';
        end if;  
        audrecs(audrecs_ctr).AUDTAFTCNTNTS:=:new.ASSUMPMAILADRCTYNM;
      end if;
    if not (:new.ASSUMPMAILADRSTCD=:old.ASSUMPMAILADRSTCD 
                or (:new.ASSUMPMAILADRSTCD is null and :old.ASSUMPMAILADRSTCD is null)) 
      then  
        audrecs.extend;
        audrecs_ctr:=audrecs_ctr+1;
        audrecs(audrecs_ctr):=audrecs(1);
        audrecs(audrecs_ctr).audtseqnmb:=audrecs(audrecs_ctr-1).audtseqnmb+1;
        audrecs(audrecs_ctr).AUDTFLDNM:='ASSTATE ';
        audrecs(audrecs_ctr).audtchngfile:='BGW9ASSU';
        if updating then
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:=:old.ASSUMPMAILADRSTCD;
        else  
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:='$NULL$';
        end if;  
        audrecs(audrecs_ctr).AUDTAFTCNTNTS:=:new.ASSUMPMAILADRSTCD;
      end if;
    if not (:new.ASSUMPMAILADRZIPCD=:old.ASSUMPMAILADRZIPCD 
                or (:new.ASSUMPMAILADRZIPCD is null and :old.ASSUMPMAILADRZIPCD is null)) 
      then  
        audrecs.extend;
        audrecs_ctr:=audrecs_ctr+1;
        audrecs(audrecs_ctr):=audrecs(1);
        audrecs(audrecs_ctr).audtseqnmb:=audrecs(audrecs_ctr-1).audtseqnmb+1;
        audrecs(audrecs_ctr).AUDTFLDNM:='ASZIP1  ';
        audrecs(audrecs_ctr).audtchngfile:='BGW9ASSU';
        if updating then
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:=:old.ASSUMPMAILADRZIPCD;
        else  
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:='$NULL$';
        end if;  
        audrecs(audrecs_ctr).AUDTAFTCNTNTS:=:new.ASSUMPMAILADRZIPCD;
      end if;
    if not (:new.ASSUMPMAILADRZIP4CD=:old.ASSUMPMAILADRZIP4CD 
                or (:new.ASSUMPMAILADRZIP4CD is null and :old.ASSUMPMAILADRZIP4CD is null)) 
      then  
        audrecs.extend;
        audrecs_ctr:=audrecs_ctr+1;
        audrecs(audrecs_ctr):=audrecs(1);
        audrecs(audrecs_ctr).audtseqnmb:=audrecs(audrecs_ctr-1).audtseqnmb+1;
        audrecs(audrecs_ctr).AUDTFLDNM:='ASZIP2  ';
        audrecs(audrecs_ctr).audtchngfile:='BGW9ASSU';
        if updating then
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:=:old.ASSUMPMAILADRZIP4CD;
        else  
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:='$NULL$';
        end if;  
        audrecs(audrecs_ctr).AUDTAFTCNTNTS:=:new.ASSUMPMAILADRZIP4CD;
      end if;
  -- write out any accumulated audit recs with bulk insert
      if audrecs_ctr>1 then
        forall r in 2..audrecs_ctr insert into stgcsa.sofvaudttbl values audrecs(r);
      end if;  
    end if;
  end if;
  -- end of logging to sofvaudttbl
  --
  -- History logging (HST)... initialize 
  -- Skip if datahist logging is explicitly turned fo via switch in runtime
  if runtime.datahist_logging_on_YN='Y' then
    SELECT LOGENTRYIDSEQ.NEXTVAL, sysdate 
      into hst.updtlogid, hst.updtdt FROM DUAL;
    -- set aside table pk, set managed variables
    hst.csa_sid:=runtime.csa_session_id;
    hst.loannmb:=null;
    hst.tablenm:='SOFVBFFATBL';
    hst.loannmb:=nvl(:old.loannmb,:new.loannmb);
      
    hst.updtuserid:=:new.lastupdtuserid;
    if deleting=true then hst.updtuserid:=runtime.csa_userid; end if;
    if hst.updtuserid is null then hst.updtuserid:=user; end if;
    case
      when inserting=true then hst.upd_action:='I'; 
      when updating=true then hst.upd_action:='U';
      when deleting=true then hst.upd_action:='D';
    end case; 
    -- now set composit PK for row being updated  
    hst.pk:=null         
      ||nvl(:new.ASSUMPKEYLOANNMB,:old.ASSUMPKEYLOANNMB)
      ||to_char(nvl(:new.ASSUMPLASTEFFDT,:old.ASSUMPLASTEFFDT), 'YYYY-MM-DD HH24:MI:SS')
;  
    -- insert log main row
    INSERT INTO stgupdtlogtbl values HST;
    --
    -- Now column value logging for changed columns
    --   Initialize logdtl collection.
    logdtl.extend;
    logdtl_ctr:=1;
    logdtl(logdtl_ctr).updtlogfk:=hst.updtlogid;
    logdtl(logdtl_ctr).oldvalue:=null;
    logdtl(logdtl_ctr).newvalue:=null;
    histlog_col_if_changed('LOANNMB',
      :old.LOANNMB,:new.LOANNMB);
    histlog_col_if_changed('ASSUMPTAXID',
      :old.ASSUMPTAXID,:new.ASSUMPTAXID);
    histlog_col_if_changed('ASSUMPNM',
      :old.ASSUMPNM,:new.ASSUMPNM);
    histlog_col_if_changed('ASSUMPMAILADRSTR1NM',
      :old.ASSUMPMAILADRSTR1NM,:new.ASSUMPMAILADRSTR1NM);
    histlog_col_if_changed('ASSUMPMAILADRSTR2NM',
      :old.ASSUMPMAILADRSTR2NM,:new.ASSUMPMAILADRSTR2NM);
    histlog_col_if_changed('ASSUMPMAILADRCTYNM',
      :old.ASSUMPMAILADRCTYNM,:new.ASSUMPMAILADRCTYNM);
    histlog_col_if_changed('ASSUMPMAILADRSTCD',
      :old.ASSUMPMAILADRSTCD,:new.ASSUMPMAILADRSTCD);
    histlog_col_if_changed('ASSUMPMAILADRZIPCD',
      :old.ASSUMPMAILADRZIPCD,:new.ASSUMPMAILADRZIPCD);
    histlog_col_if_changed('ASSUMPMAILADRZIP4CD',
      :old.ASSUMPMAILADRZIP4CD,:new.ASSUMPMAILADRZIP4CD);
    histlog_col_if_changed('ASSUMPOFC',
      :old.ASSUMPOFC,:new.ASSUMPOFC);
    histlog_col_if_changed('ASSUMPPRGRM',
      :old.ASSUMPPRGRM,:new.ASSUMPPRGRM);
    histlog_col_if_changed('ASSUMPSTATCDOFLOAN',
      :old.ASSUMPSTATCDOFLOAN,:new.ASSUMPSTATCDOFLOAN);
    histlog_col_if_changed('ASSUMPVERIFICATIONIND',
      :old.ASSUMPVERIFICATIONIND,:new.ASSUMPVERIFICATIONIND);
    histlog_col_if_changed('ASSUMPVERIFICATIONDT',
      to_char(:old.ASSUMPVERIFICATIONDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.ASSUMPVERIFICATIONDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('ASSUMPNMMAILADRCHNGIND',
      :old.ASSUMPNMMAILADRCHNGIND,:new.ASSUMPNMMAILADRCHNGIND);
    histlog_col_if_changed('ASSUMPNMADRCHNGDT',
      to_char(:old.ASSUMPNMADRCHNGDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.ASSUMPNMADRCHNGDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('ASSUMPTAXFORMFLD',
      :old.ASSUMPTAXFORMFLD,:new.ASSUMPTAXFORMFLD);
    histlog_col_if_changed('ASSUMPTAXMAINTNDT',
      to_char(:old.ASSUMPTAXMAINTNDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.ASSUMPTAXMAINTNDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('ASSUMPKEYLOANNMB',
      :old.ASSUMPKEYLOANNMB,:new.ASSUMPKEYLOANNMB);
    histlog_col_if_changed('ASSUMPLASTEFFDT',
      to_char(:old.ASSUMPLASTEFFDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.ASSUMPLASTEFFDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CREATUSERID',
      :old.CREATUSERID,:new.CREATUSERID);
    histlog_col_if_changed('CREATDT',
      to_char(:old.CREATDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CREATDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('LASTUPDTUSERID',
      :old.LASTUPDTUSERID,:new.LASTUPDTUSERID);
    histlog_col_if_changed('LASTUPDTDT',
      to_char(:old.LASTUPDTDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.LASTUPDTDT,'YYYY-MM-DD HH24:MI:SS'));
    -- write out any accumulated logdtl recs with bulk insert
    if logdtl_ctr>1 then
      forall r in 2..logdtl_ctr insert into stgcsa.stgupdtlogdtltbl values logdtl(r);
    end if;
  end if;  
  -- end of history logging section
end;

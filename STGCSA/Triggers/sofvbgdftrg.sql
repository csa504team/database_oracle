create or replace trigger STGCSA.SOFVBGDFTRG
    after insert or update or delete ON STGCSA.SOFVBGDFTBL
    REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE
 /*
  Created on: 2019-05-04 08:25:21
  Created by: GENR
  Crerated from template csatrig_template v3.4 3 May 2019 on 2019-05-04 08:25:22
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
    Trigger handles errors by not updating and raising error -20100.
    Caller should handle failing SQL statement, including recording.
*/
  calling_module varchar2(80);
  tabowner varchar2(32):='STGCSA';
  tabname varchar2(32):='SOFVBGDFTBL';
  -- vars for history logging
  hst stgupdtlogtbl%rowtype;
  TYPE logdtl_typ is table of STGCSA.stgupdtlogdtltbl%rowtype;
  logdtl logdtl_typ:=logdtl_typ();
  logdtl_ctr number:=0;
procedure histlog_col_if_changed(colnam varchar2, oldval varchar2, newval varchar2) is
begin
  If (inserting and newval is not null)
    or (deleting and oldval is not null)
    or (updating and 
         (oldval<>newval 
          or (oldval is null and newval is not null)
          or (oldval is not null and newval is null)
         )
       )                  
  then 
    logdtl.extend;
    logdtl_ctr:=logdtl_ctr+1;
    logdtl(logdtl_ctr):=logdtl(1);
    logdtl(logdtl_ctr).colname:=colnam;
    if inserting or updating then 
      logdtl(logdtl_ctr).newvalue:=newval; 
    else
      logdtl(logdtl_ctr).newvalue:=null;       
    end if;  
    if updating or deleting then 
      logdtl(logdtl_ctr).oldvalue:=oldval;
    else
      logdtl(logdtl_ctr).oldvalue:=null;      
    end if;
  end if;
end;
--
--
begin
  select SYS_CONTEXT ('USERENV', 'MODULE') into calling_module from dual;
  if calling_module='Data Pump Worker' then return; end if;
  --
  -- History logging (HST)... initialize 
  -- Skip if datahist logging is explicitly turned fo via switch in runtime
  if runtime.datahist_logging_on_YN='Y' then
    SELECT LOGENTRYIDSEQ.NEXTVAL, sysdate 
      into hst.updtlogid, hst.updtdt FROM DUAL;
    -- set aside table pk, set managed variables
    hst.csa_sid:=runtime.csa_session_id;
    hst.loannmb:=null;
    hst.tablenm:='SOFVBGDFTBL';
    hst.loannmb:=null;      
    hst.updtuserid:=:new.lastupdtuserid;
    if deleting=true then hst.updtuserid:=runtime.csa_userid; end if;
    if hst.updtuserid is null then hst.updtuserid:=user; end if;
    case
      when inserting=true then hst.upd_action:='I'; 
      when updating=true then hst.upd_action:='U';
      when deleting=true then hst.upd_action:='D';
    end case; 
    -- now set composit PK for row being updated  
    hst.pk:=null         
      ||nvl(:new.DEFRMNTGPKEY,:old.DEFRMNTGPKEY)
;  
    -- insert log main row
    INSERT INTO stgupdtlogtbl values HST;
    --
    -- Now column value logging for changed columns
    --   Initialize logdtl collection.
    logdtl.extend;
    logdtl_ctr:=1;
    logdtl(logdtl_ctr).updtlogfk:=hst.updtlogid;
    logdtl(logdtl_ctr).oldvalue:=null;
    logdtl(logdtl_ctr).newvalue:=null;
    histlog_col_if_changed('DEFRMNTGPKEY',
      :old.DEFRMNTGPKEY,:new.DEFRMNTGPKEY);
    histlog_col_if_changed('DEFRMNTSTRTDT1',
      to_char(:old.DEFRMNTSTRTDT1,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.DEFRMNTSTRTDT1,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('DEFRMNTENDDT1',
      to_char(:old.DEFRMNTENDDT1,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.DEFRMNTENDDT1,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('DEFRMNTREQ1AMT',
      :old.DEFRMNTREQ1AMT,:new.DEFRMNTREQ1AMT);
    histlog_col_if_changed('DEFRMNTSTRTDT2',
      to_char(:old.DEFRMNTSTRTDT2,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.DEFRMNTSTRTDT2,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('DEFRMNTENDDT2',
      to_char(:old.DEFRMNTENDDT2,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.DEFRMNTENDDT2,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('DEFRMNTREQ2AMT',
      :old.DEFRMNTREQ2AMT,:new.DEFRMNTREQ2AMT);
    histlog_col_if_changed('DEFRMNTSTRTDT3',
      to_char(:old.DEFRMNTSTRTDT3,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.DEFRMNTSTRTDT3,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('DEFRMNTENDDT3',
      to_char(:old.DEFRMNTENDDT3,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.DEFRMNTENDDT3,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('DEFRMNTREQ3AMT',
      :old.DEFRMNTREQ3AMT,:new.DEFRMNTREQ3AMT);
    histlog_col_if_changed('DEFRMNTSTRTDT4',
      to_char(:old.DEFRMNTSTRTDT4,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.DEFRMNTSTRTDT4,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('DEFRMNTENDDT4',
      to_char(:old.DEFRMNTENDDT4,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.DEFRMNTENDDT4,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('DEFRMNTREQ4AMT',
      :old.DEFRMNTREQ4AMT,:new.DEFRMNTREQ4AMT);
    histlog_col_if_changed('DEFRMNTSTRTDT5',
      to_char(:old.DEFRMNTSTRTDT5,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.DEFRMNTSTRTDT5,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('DEFRMNTENDDT5',
      to_char(:old.DEFRMNTENDDT5,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.DEFRMNTENDDT5,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('DEFRMNTREQ5AMT',
      :old.DEFRMNTREQ5AMT,:new.DEFRMNTREQ5AMT);
    histlog_col_if_changed('DEFRMNTSTRTDT6',
      to_char(:old.DEFRMNTSTRTDT6,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.DEFRMNTSTRTDT6,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('DEFRMNTENDDT6',
      to_char(:old.DEFRMNTENDDT6,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.DEFRMNTENDDT6,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('DEFRMNTREQ6AMT',
      :old.DEFRMNTREQ6AMT,:new.DEFRMNTREQ6AMT);
    histlog_col_if_changed('DEFRMNTSTRTDT7',
      to_char(:old.DEFRMNTSTRTDT7,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.DEFRMNTSTRTDT7,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('DEFRMNTENDDT7',
      to_char(:old.DEFRMNTENDDT7,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.DEFRMNTENDDT7,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('DEFRMNTREQ7AMT',
      :old.DEFRMNTREQ7AMT,:new.DEFRMNTREQ7AMT);
    histlog_col_if_changed('DEFRMNTSTRTDT8',
      to_char(:old.DEFRMNTSTRTDT8,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.DEFRMNTSTRTDT8,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('DEFRMNTENDDT8',
      to_char(:old.DEFRMNTENDDT8,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.DEFRMNTENDDT8,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('DEFRMNTREQ8AMT',
      :old.DEFRMNTREQ8AMT,:new.DEFRMNTREQ8AMT);
    histlog_col_if_changed('DEFRMNTSTRTDT9',
      to_char(:old.DEFRMNTSTRTDT9,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.DEFRMNTSTRTDT9,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('DEFRMNTENDDT9',
      to_char(:old.DEFRMNTENDDT9,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.DEFRMNTENDDT9,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('DEFRMNTREQ9AMT',
      :old.DEFRMNTREQ9AMT,:new.DEFRMNTREQ9AMT);
    histlog_col_if_changed('DEFRMNTSTRTDT10',
      to_char(:old.DEFRMNTSTRTDT10,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.DEFRMNTSTRTDT10,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('DEFRMNTENDDT10',
      to_char(:old.DEFRMNTENDDT10,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.DEFRMNTENDDT10,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('DEFRMNTREQ10AMT',
      :old.DEFRMNTREQ10AMT,:new.DEFRMNTREQ10AMT);
    histlog_col_if_changed('DEFRMNTSTRTDT11',
      to_char(:old.DEFRMNTSTRTDT11,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.DEFRMNTSTRTDT11,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('DEFRMNTENDDT11',
      to_char(:old.DEFRMNTENDDT11,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.DEFRMNTENDDT11,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('DEFRMNTREQ11AMT',
      :old.DEFRMNTREQ11AMT,:new.DEFRMNTREQ11AMT);
    histlog_col_if_changed('DEFRMNTSTRTDT12',
      to_char(:old.DEFRMNTSTRTDT12,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.DEFRMNTSTRTDT12,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('DEFRMNTENDDT12',
      to_char(:old.DEFRMNTENDDT12,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.DEFRMNTENDDT12,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('DEFRMNTREQ12AMT',
      :old.DEFRMNTREQ12AMT,:new.DEFRMNTREQ12AMT);
    histlog_col_if_changed('DEFRMNTSTRTDT13',
      to_char(:old.DEFRMNTSTRTDT13,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.DEFRMNTSTRTDT13,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('DEFRMNTENDDT13',
      to_char(:old.DEFRMNTENDDT13,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.DEFRMNTENDDT13,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('DEFRMNTREQ13AMT',
      :old.DEFRMNTREQ13AMT,:new.DEFRMNTREQ13AMT);
    histlog_col_if_changed('DEFRMNTSTRTDT14',
      to_char(:old.DEFRMNTSTRTDT14,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.DEFRMNTSTRTDT14,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('DEFRMNTENDDT14',
      to_char(:old.DEFRMNTENDDT14,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.DEFRMNTENDDT14,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('DEFRMNTREQ14AMT',
      :old.DEFRMNTREQ14AMT,:new.DEFRMNTREQ14AMT);
    histlog_col_if_changed('DEFRMNTSTRTDT15',
      to_char(:old.DEFRMNTSTRTDT15,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.DEFRMNTSTRTDT15,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('DEFRMNTENDDT15',
      to_char(:old.DEFRMNTENDDT15,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.DEFRMNTENDDT15,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('DEFRMNTREQ15AMT',
      :old.DEFRMNTREQ15AMT,:new.DEFRMNTREQ15AMT);
    histlog_col_if_changed('DEFRMNTDTCREAT',
      to_char(:old.DEFRMNTDTCREAT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.DEFRMNTDTCREAT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('DEFRMNTDTMOD',
      to_char(:old.DEFRMNTDTMOD,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.DEFRMNTDTMOD,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CREATUSERID',
      :old.CREATUSERID,:new.CREATUSERID);
    histlog_col_if_changed('CREATDT',
      to_char(:old.CREATDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CREATDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('LASTUPDTUSERID',
      :old.LASTUPDTUSERID,:new.LASTUPDTUSERID);
    histlog_col_if_changed('LASTUPDTDT',
      to_char(:old.LASTUPDTDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.LASTUPDTDT,'YYYY-MM-DD HH24:MI:SS'));
    -- write out any accumulated logdtl recs with bulk insert
    if logdtl_ctr>1 then
      forall r in 2..logdtl_ctr insert into stgcsa.stgupdtlogdtltbl values logdtl(r);
    end if;
  end if;  
  -- end of history logging section
end;

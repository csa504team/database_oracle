create or replace trigger STGCSA.GENPACSIMPTDTLTRG
    after insert or update or delete ON STGCSA.GENPACSIMPTDTLTBL
    REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE
 /*
  Created on: 2019-05-04 08:25:01
  Created by: GENR
  Crerated from template csatrig_template v3.4 3 May 2019 on 2019-05-04 08:25:01
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
    Trigger handles errors by not updating and raising error -20100.
    Caller should handle failing SQL statement, including recording.
*/
  calling_module varchar2(80);
  tabowner varchar2(32):='STGCSA';
  tabname varchar2(32):='GENPACSIMPTDTLTBL';
  -- vars for history logging
  hst stgupdtlogtbl%rowtype;
  TYPE logdtl_typ is table of STGCSA.stgupdtlogdtltbl%rowtype;
  logdtl logdtl_typ:=logdtl_typ();
  logdtl_ctr number:=0;
procedure histlog_col_if_changed(colnam varchar2, oldval varchar2, newval varchar2) is
begin
  If (inserting and newval is not null)
    or (deleting and oldval is not null)
    or (updating and 
         (oldval<>newval 
          or (oldval is null and newval is not null)
          or (oldval is not null and newval is null)
         )
       )                  
  then 
    logdtl.extend;
    logdtl_ctr:=logdtl_ctr+1;
    logdtl(logdtl_ctr):=logdtl(1);
    logdtl(logdtl_ctr).colname:=colnam;
    if inserting or updating then 
      logdtl(logdtl_ctr).newvalue:=newval; 
    else
      logdtl(logdtl_ctr).newvalue:=null;       
    end if;  
    if updating or deleting then 
      logdtl(logdtl_ctr).oldvalue:=oldval;
    else
      logdtl(logdtl_ctr).oldvalue:=null;      
    end if;
  end if;
end;
--
--
begin
  select SYS_CONTEXT ('USERENV', 'MODULE') into calling_module from dual;
  if calling_module='Data Pump Worker' then return; end if;
  --
  -- History logging (HST)... initialize 
  -- Skip if datahist logging is explicitly turned fo via switch in runtime
  if runtime.datahist_logging_on_YN='Y' then
    SELECT LOGENTRYIDSEQ.NEXTVAL, sysdate 
      into hst.updtlogid, hst.updtdt FROM DUAL;
    -- set aside table pk, set managed variables
    hst.csa_sid:=runtime.csa_session_id;
    hst.loannmb:=null;
    hst.tablenm:='GENPACSIMPTDTLTBL';
    hst.loannmb:=null;      
    hst.updtuserid:=:new.lastupdtuserid;
    if deleting=true then hst.updtuserid:=runtime.csa_userid; end if;
    if hst.updtuserid is null then hst.updtuserid:=user; end if;
    case
      when inserting=true then hst.upd_action:='I'; 
      when updating=true then hst.upd_action:='U';
      when deleting=true then hst.upd_action:='D';
    end case; 
    -- now set composit PK for row being updated  
    hst.pk:=null         
      ||rpad(to_char(nvl(:new.PACSIMPTDTLID,:old.PACSIMPTDTLID)),22)
;  
    -- insert log main row
    INSERT INTO stgupdtlogtbl values HST;
    --
    -- Now column value logging for changed columns
    --   Initialize logdtl collection.
    logdtl.extend;
    logdtl_ctr:=1;
    logdtl(logdtl_ctr).updtlogfk:=hst.updtlogid;
    logdtl(logdtl_ctr).oldvalue:=null;
    logdtl(logdtl_ctr).newvalue:=null;
    histlog_col_if_changed('PACSIMPTDTLID',
      :old.PACSIMPTDTLID,:new.PACSIMPTDTLID);
    histlog_col_if_changed('PACSIMPTID',
      :old.PACSIMPTID,:new.PACSIMPTID);
    histlog_col_if_changed('RQSTID',
      :old.RQSTID,:new.RQSTID);
    histlog_col_if_changed('RELTDTBLNM',
      :old.RELTDTBLNM,:new.RELTDTBLNM);
    histlog_col_if_changed('RELTDTBLREF',
      :old.RELTDTBLREF,:new.RELTDTBLREF);
    histlog_col_if_changed('RQSTTYP',
      :old.RQSTTYP,:new.RQSTTYP);
    histlog_col_if_changed('PYMTTYP',
      :old.PYMTTYP,:new.PYMTTYP);
    histlog_col_if_changed('TRANSID',
      :old.TRANSID,:new.TRANSID);
    histlog_col_if_changed('TRANSAMT',
      :old.TRANSAMT,:new.TRANSAMT);
    histlog_col_if_changed('TRANSTYP',
      :old.TRANSTYP,:new.TRANSTYP);
    histlog_col_if_changed('PACSACCT',
      :old.PACSACCT,:new.PACSACCT);
    histlog_col_if_changed('ACCTNM',
      :old.ACCTNM,:new.ACCTNM);
    histlog_col_if_changed('INTRNLEXTRNLACCT',
      :old.INTRNLEXTRNLACCT,:new.INTRNLEXTRNLACCT);
    histlog_col_if_changed('TRANAU',
      :old.TRANAU,:new.TRANAU);
    histlog_col_if_changed('ABABNKNMB',
      :old.ABABNKNMB,:new.ABABNKNMB);
    histlog_col_if_changed('ABABNKNM',
      :old.ABABNKNM,:new.ABABNKNM);
    histlog_col_if_changed('SWIFTBNK',
      :old.SWIFTBNK,:new.SWIFTBNK);
    histlog_col_if_changed('INTRMEDRYABANMB',
      :old.INTRMEDRYABANMB,:new.INTRMEDRYABANMB);
    histlog_col_if_changed('INTRMEDRYSWIFT',
      :old.INTRMEDRYSWIFT,:new.INTRMEDRYSWIFT);
    histlog_col_if_changed('BNKIRC',
      :old.BNKIRC,:new.BNKIRC);
    histlog_col_if_changed('BNKIBAN',
      :old.BNKIBAN,:new.BNKIBAN);
    histlog_col_if_changed('TRANSINFO',
      :old.TRANSINFO,:new.TRANSINFO);
    histlog_col_if_changed('STATCD',
      :old.STATCD,:new.STATCD);
    histlog_col_if_changed('TRANSDT',
      to_char(:old.TRANSDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.TRANSDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('POSTEDDT',
      to_char(:old.POSTEDDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.POSTEDDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('POSTERLOC',
      :old.POSTERLOC,:new.POSTERLOC);
    histlog_col_if_changed('PACSIMPTPTIC',
      :old.PACSIMPTPTIC,:new.PACSIMPTPTIC);
    histlog_col_if_changed('RCPTCD',
      :old.RCPTCD,:new.RCPTCD);
    histlog_col_if_changed('DISBCD',
      :old.DISBCD,:new.DISBCD);
    histlog_col_if_changed('PRININC',
      :old.PRININC,:new.PRININC);
    histlog_col_if_changed('SEIBTCHID',
      :old.SEIBTCHID,:new.SEIBTCHID);
    histlog_col_if_changed('SEITRANTYP',
      :old.SEITRANTYP,:new.SEITRANTYP);
    histlog_col_if_changed('RQSTAU',
      :old.RQSTAU,:new.RQSTAU);
    histlog_col_if_changed('CREATUSERID',
      :old.CREATUSERID,:new.CREATUSERID);
    histlog_col_if_changed('CREATDT',
      to_char(:old.CREATDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CREATDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('INITBY',
      :old.INITBY,:new.INITBY);
    histlog_col_if_changed('APPVBY',
      :old.APPVBY,:new.APPVBY);
    histlog_col_if_changed('ACHADDENDA',
      :old.ACHADDENDA,:new.ACHADDENDA);
    histlog_col_if_changed('TIMESTAMPFLD',
      to_char(:old.TIMESTAMPFLD,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.TIMESTAMPFLD,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('LASTUPDTUSERID',
      :old.LASTUPDTUSERID,:new.LASTUPDTUSERID);
    histlog_col_if_changed('LASTUPDTDT',
      to_char(:old.LASTUPDTDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.LASTUPDTDT,'YYYY-MM-DD HH24:MI:SS'));
    -- write out any accumulated logdtl recs with bulk insert
    if logdtl_ctr>1 then
      forall r in 2..logdtl_ctr insert into stgcsa.stgupdtlogdtltbl values logdtl(r);
    end if;
  end if;  
  -- end of history logging section
end;

create or replace trigger STGCSA.SOFVLND1TRG
    after insert or update or delete ON STGCSA.SOFVLND1TBL
    REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE
 /*
  Created on: 2019-05-04 08:25:34
  Created by: GENR
  Crerated from template csatrig_template v3.4 3 May 2019 on 2019-05-04 08:25:34
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
    Trigger handles errors by not updating and raising error -20100.
    Caller should handle failing SQL statement, including recording.
*/
  calling_module varchar2(80);
  tabowner varchar2(32):='STGCSA';
  tabname varchar2(32):='SOFVLND1TBL';
  -- vars for sofvaudt support
  TYPE audrecs_typ is table of STGCSA.SOFVAUDTTBL%rowtype;
  audrecs audrecs_typ:=audrecs_typ();
  audrecs_ctr number:=0;
  -- vars for history logging
  hst stgupdtlogtbl%rowtype;
  TYPE logdtl_typ is table of STGCSA.stgupdtlogdtltbl%rowtype;
  logdtl logdtl_typ:=logdtl_typ();
  logdtl_ctr number:=0;
procedure histlog_col_if_changed(colnam varchar2, oldval varchar2, newval varchar2) is
begin
  If (inserting and newval is not null)
    or (deleting and oldval is not null)
    or (updating and 
         (oldval<>newval 
          or (oldval is null and newval is not null)
          or (oldval is not null and newval is null)
         )
       )                  
  then 
    logdtl.extend;
    logdtl_ctr:=logdtl_ctr+1;
    logdtl(logdtl_ctr):=logdtl(1);
    logdtl(logdtl_ctr).colname:=colnam;
    if inserting or updating then 
      logdtl(logdtl_ctr).newvalue:=newval; 
    else
      logdtl(logdtl_ctr).newvalue:=null;       
    end if;  
    if updating or deleting then 
      logdtl(logdtl_ctr).oldvalue:=oldval;
    else
      logdtl(logdtl_ctr).oldvalue:=null;      
    end if;
  end if;
end;
--
--
begin
  select SYS_CONTEXT ('USERENV', 'MODULE') into calling_module from dual;
  if calling_module='Data Pump Worker' then return; end if;
    --
  -- code to record changes to SOFVAUDTTBL
  -- initialize audrecs collection and first rec 
  -- first rec holds defaults and is not inserted
  -- Skip if audit logging is explicitly turned fo via switch in runtime
  if runtime.audit_logging_on_YN='Y' then
    audrecs.extend;
    select systimestamp,sysdate,sys_context('userenv','session_user')
      into audrecs(1).audttransdt, audrecs(1).CREATDT, audrecs(1).creatuserid from dual;
    audrecs_ctr:=1;   
    audrecs(1).LASTUPDTDT:=audrecs(audrecs_ctr).CREATDT;
    audrecs(1).lastupdtuserid:=audrecs(audrecs_ctr).creatuserid;
    
     
    if deleting then
      audrecs(1).audtrecrdkey:=:old.LOANNMB;
    else 
      audrecs(1).audtrecrdkey:=:new.LOANNMB;
    end if;     
    audrecs(1).audtseqnmb:=0;
    audrecs(1).AUDTOPRTRINITIAL:='  ';
    audrecs(1).AUDTTRMNLID:='    ';
    audrecs(1).AUDTEDTCD:='?';
    audrecs(1).AUDTUSERID:=nvl(:new.lastupdtuserid,:old.lastupdtuserid);
    if audrecs(1).AUDTUSERID is null then audrecs(audrecs_ctr).AUDTUSERID:=runtime.csa_userid; end if;
    if audrecs(1).AUDTUSERID is null then audrecs(audrecs_ctr).AUDTUSERID:=user; end if;  
    if inserting or updating then
      --
      -- record specific changed values to sofvaudttbl
    -- special lnd1 thingee where VSAM field LDCDCNUM got split... ask jill
      audrecs(audrecs_ctr):=audrecs(1);
      if inserting then
        if :new.LoanDtlCDCRegnCd||:new.LoanDtlCDCCert is not null then 
          audrecs.extend;
          audrecs_ctr:=audrecs_ctr+1;
          audrecs(audrecs_ctr):=audrecs(1);
          audrecs(audrecs_ctr).audtseqnmb:=audrecs(audrecs_ctr-1).audtseqnmb+1;
          audrecs(audrecs_ctr).AUDTACTNCD:='A';
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:='$NULL$';
          audrecs(audrecs_ctr).AUDTAFTCNTNTS:=:new.LoanDtlCDCRegnCd||:new.LoanDtlCDCCert;      
          audrecs(audrecs_ctr).AUDTFLDNM:='LDCDCNUM';
          audrecs(audrecs_ctr).audtchngfile:='BGLOANDB';
        end if;
      else
        if not 
          (:old.LoanDtlCDCRegnCd||:old.LoanDtlCDCCert
            =:new.LoanDtlCDCRegnCd||:new.LoanDtlCDCCert
          or (:old.LoanDtlCDCRegnCd||:old.LoanDtlCDCCert is null 
            and :new.LoanDtlCDCRegnCd||:new.LoanDtlCDCCert is null)) 
        then          
          audrecs.extend;
          audrecs_ctr:=audrecs_ctr+1;
          audrecs(audrecs_ctr):=audrecs(1);
          audrecs(audrecs_ctr).audtseqnmb:=audrecs(audrecs_ctr-1).audtseqnmb+1;
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:=:old.LoanDtlCDCRegnCd||:old.LoanDtlCDCCert;        
          audrecs(audrecs_ctr).AUDTAFTCNTNTS:=:new.LoanDtlCDCRegnCd||:new.LoanDtlCDCCert;      
          audrecs(audrecs_ctr).audtseqnmb:=audrecs(audrecs_ctr).audtseqnmb+1;
          audrecs(audrecs_ctr).AUDTACTNCD:='C';
          audrecs(audrecs_ctr).AUDTFLDNM:='LDCDCNUM';
          audrecs(audrecs_ctr).audtchngfile:='BGLOANDB';
        end if;
      end if;
      -- end of special case for split VSAM field LDCDCNUM
      if inserting then  
        audrecs(audrecs_ctr).AUDTACTNCD:='A';
      else  
        audrecs(audrecs_ctr).AUDTACTNCD:='C';
      end if;
    if not (:new.LOANDTLBORRMAILADRSTR1NM=:old.LOANDTLBORRMAILADRSTR1NM 
                or (:new.LOANDTLBORRMAILADRSTR1NM is null and :old.LOANDTLBORRMAILADRSTR1NM is null)) 
      then  
        audrecs.extend;
        audrecs_ctr:=audrecs_ctr+1;
        audrecs(audrecs_ctr):=audrecs(1);
        audrecs(audrecs_ctr).audtseqnmb:=audrecs(audrecs_ctr-1).audtseqnmb+1;
        audrecs(audrecs_ctr).AUDTFLDNM:='LDSTRET1';
        audrecs(audrecs_ctr).audtchngfile:='BGLOANDB';
        if updating then
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:=:old.LOANDTLBORRMAILADRSTR1NM;
        else  
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:='$NULL$';
        end if;  
        audrecs(audrecs_ctr).AUDTAFTCNTNTS:=:new.LOANDTLBORRMAILADRSTR1NM;
      end if;
    if not (:new.LOANDTLBORRMAILADRCTYNM=:old.LOANDTLBORRMAILADRCTYNM 
                or (:new.LOANDTLBORRMAILADRCTYNM is null and :old.LOANDTLBORRMAILADRCTYNM is null)) 
      then  
        audrecs.extend;
        audrecs_ctr:=audrecs_ctr+1;
        audrecs(audrecs_ctr):=audrecs(1);
        audrecs(audrecs_ctr).audtseqnmb:=audrecs(audrecs_ctr-1).audtseqnmb+1;
        audrecs(audrecs_ctr).AUDTFLDNM:='LDCITY1 ';
        audrecs(audrecs_ctr).audtchngfile:='BGLOANDB';
        if updating then
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:=:old.LOANDTLBORRMAILADRCTYNM;
        else  
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:='$NULL$';
        end if;  
        audrecs(audrecs_ctr).AUDTAFTCNTNTS:=:new.LOANDTLBORRMAILADRCTYNM;
      end if;
    if not (:new.LOANDTLBORRMAILADRSTCD=:old.LOANDTLBORRMAILADRSTCD 
                or (:new.LOANDTLBORRMAILADRSTCD is null and :old.LOANDTLBORRMAILADRSTCD is null)) 
      then  
        audrecs.extend;
        audrecs_ctr:=audrecs_ctr+1;
        audrecs(audrecs_ctr):=audrecs(1);
        audrecs(audrecs_ctr).audtseqnmb:=audrecs(audrecs_ctr-1).audtseqnmb+1;
        audrecs(audrecs_ctr).AUDTFLDNM:='LDSTATE1';
        audrecs(audrecs_ctr).audtchngfile:='BGLOANDB';
        if updating then
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:=:old.LOANDTLBORRMAILADRSTCD;
        else  
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:='$NULL$';
        end if;  
        audrecs(audrecs_ctr).AUDTAFTCNTNTS:=:new.LOANDTLBORRMAILADRSTCD;
      end if;
    if not (:new.LOANDTLBORRMAILADRZIPCD=:old.LOANDTLBORRMAILADRZIPCD 
                or (:new.LOANDTLBORRMAILADRZIPCD is null and :old.LOANDTLBORRMAILADRZIPCD is null)) 
      then  
        audrecs.extend;
        audrecs_ctr:=audrecs_ctr+1;
        audrecs(audrecs_ctr):=audrecs(1);
        audrecs(audrecs_ctr).audtseqnmb:=audrecs(audrecs_ctr-1).audtseqnmb+1;
        audrecs(audrecs_ctr).AUDTFLDNM:='LDZIP1  ';
        audrecs(audrecs_ctr).audtchngfile:='BGLOANDB';
        if updating then
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:=:old.LOANDTLBORRMAILADRZIPCD;
        else  
          audrecs(audrecs_ctr).AUDTBEFCNTNTS:='$NULL$';
        end if;  
        audrecs(audrecs_ctr).AUDTAFTCNTNTS:=:new.LOANDTLBORRMAILADRZIPCD;
      end if;
  -- write out any accumulated audit recs with bulk insert
      if audrecs_ctr>1 then
        forall r in 2..audrecs_ctr insert into stgcsa.sofvaudttbl values audrecs(r);
      end if;  
    end if;
  end if;
  -- end of logging to sofvaudttbl
  --
  -- History logging (HST)... initialize 
  -- Skip if datahist logging is explicitly turned fo via switch in runtime
  if runtime.datahist_logging_on_YN='Y' then
    SELECT LOGENTRYIDSEQ.NEXTVAL, sysdate 
      into hst.updtlogid, hst.updtdt FROM DUAL;
    -- set aside table pk, set managed variables
    hst.csa_sid:=runtime.csa_session_id;
    hst.loannmb:=null;
    hst.tablenm:='SOFVLND1TBL';
    hst.loannmb:=nvl(:old.loannmb,:new.loannmb);
      
    hst.updtuserid:=:new.lastupdtuserid;
    if deleting=true then hst.updtuserid:=runtime.csa_userid; end if;
    if hst.updtuserid is null then hst.updtuserid:=user; end if;
    case
      when inserting=true then hst.upd_action:='I'; 
      when updating=true then hst.upd_action:='U';
      when deleting=true then hst.upd_action:='D';
    end case; 
    -- now set composit PK for row being updated  
    hst.pk:=null         
      ||nvl(:new.LOANNMB,:old.LOANNMB)
;  
    -- insert log main row
    INSERT INTO stgupdtlogtbl values HST;
    --
    -- Now column value logging for changed columns
    --   Initialize logdtl collection.
    logdtl.extend;
    logdtl_ctr:=1;
    logdtl(logdtl_ctr).updtlogfk:=hst.updtlogid;
    logdtl(logdtl_ctr).oldvalue:=null;
    logdtl(logdtl_ctr).newvalue:=null;
    histlog_col_if_changed('LOANNMB',
      :old.LOANNMB,:new.LOANNMB);
    histlog_col_if_changed('LOANDTLSBAOFC',
      :old.LOANDTLSBAOFC,:new.LOANDTLSBAOFC);
    histlog_col_if_changed('LOANDTLBORRNM',
      :old.LOANDTLBORRNM,:new.LOANDTLBORRNM);
    histlog_col_if_changed('LOANDTLBORRMAILADRSTR1NM',
      :old.LOANDTLBORRMAILADRSTR1NM,:new.LOANDTLBORRMAILADRSTR1NM);
    histlog_col_if_changed('LOANDTLBORRMAILADRSTR2NM',
      :old.LOANDTLBORRMAILADRSTR2NM,:new.LOANDTLBORRMAILADRSTR2NM);
    histlog_col_if_changed('LOANDTLBORRMAILADRCTYNM',
      :old.LOANDTLBORRMAILADRCTYNM,:new.LOANDTLBORRMAILADRCTYNM);
    histlog_col_if_changed('LOANDTLBORRMAILADRSTCD',
      :old.LOANDTLBORRMAILADRSTCD,:new.LOANDTLBORRMAILADRSTCD);
    histlog_col_if_changed('LOANDTLBORRMAILADRZIPCD',
      :old.LOANDTLBORRMAILADRZIPCD,:new.LOANDTLBORRMAILADRZIPCD);
    histlog_col_if_changed('LOANDTLBORRMAILADRZIP4CD',
      :old.LOANDTLBORRMAILADRZIP4CD,:new.LOANDTLBORRMAILADRZIP4CD);
    histlog_col_if_changed('LOANDTLSBCNM',
      :old.LOANDTLSBCNM,:new.LOANDTLSBCNM);
    histlog_col_if_changed('LOANDTLSBCMAILADRSTR1NM',
      :old.LOANDTLSBCMAILADRSTR1NM,:new.LOANDTLSBCMAILADRSTR1NM);
    histlog_col_if_changed('LOANDTLSBCMAILADRSTR2NM',
      :old.LOANDTLSBCMAILADRSTR2NM,:new.LOANDTLSBCMAILADRSTR2NM);
    histlog_col_if_changed('LOANDTLSBCMAILADRCTYNM',
      :old.LOANDTLSBCMAILADRCTYNM,:new.LOANDTLSBCMAILADRCTYNM);
    histlog_col_if_changed('LOANDTLSBCMAILADRSTCD',
      :old.LOANDTLSBCMAILADRSTCD,:new.LOANDTLSBCMAILADRSTCD);
    histlog_col_if_changed('LOANDTLSBCMAILADRZIPCD',
      :old.LOANDTLSBCMAILADRZIPCD,:new.LOANDTLSBCMAILADRZIPCD);
    histlog_col_if_changed('LOANDTLSBCMAILADRZIP4CD',
      :old.LOANDTLSBCMAILADRZIP4CD,:new.LOANDTLSBCMAILADRZIP4CD);
    histlog_col_if_changed('LOANDTLTAXID',
      :old.LOANDTLTAXID,:new.LOANDTLTAXID);
    histlog_col_if_changed('LOANDTLCDCREGNCD',
      :old.LOANDTLCDCREGNCD,:new.LOANDTLCDCREGNCD);
    histlog_col_if_changed('LOANDTLCDCCERT',
      :old.LOANDTLCDCCERT,:new.LOANDTLCDCCERT);
    histlog_col_if_changed('LOANDTLDEFPYMT',
      :old.LOANDTLDEFPYMT,:new.LOANDTLDEFPYMT);
    histlog_col_if_changed('LOANDTLDBENTRPRINAMT',
      :old.LOANDTLDBENTRPRINAMT,:new.LOANDTLDBENTRPRINAMT);
    histlog_col_if_changed('LOANDTLDBENTRCURBALAMT',
      :old.LOANDTLDBENTRCURBALAMT,:new.LOANDTLDBENTRCURBALAMT);
    histlog_col_if_changed('LOANDTLDBENTRINTRTPCT',
      :old.LOANDTLDBENTRINTRTPCT,:new.LOANDTLDBENTRINTRTPCT);
    histlog_col_if_changed('LOANDTLISSDT',
      to_char(:old.LOANDTLISSDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.LOANDTLISSDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('LOANDTLDEFPYMTDTX',
      :old.LOANDTLDEFPYMTDTX,:new.LOANDTLDEFPYMTDTX);
    histlog_col_if_changed('LOANDTLDBENTRPYMTDTTRM',
      :old.LOANDTLDBENTRPYMTDTTRM,:new.LOANDTLDBENTRPYMTDTTRM);
    histlog_col_if_changed('LOANDTLDBENTRPROCDSAMT',
      :old.LOANDTLDBENTRPROCDSAMT,:new.LOANDTLDBENTRPROCDSAMT);
    histlog_col_if_changed('LOANDTLDBENTRPROCDSESCROWAMT',
      :old.LOANDTLDBENTRPROCDSESCROWAMT,:new.LOANDTLDBENTRPROCDSESCROWAMT);
    histlog_col_if_changed('LOANDTLSEMIANNPYMTAMT',
      :old.LOANDTLSEMIANNPYMTAMT,:new.LOANDTLSEMIANNPYMTAMT);
    histlog_col_if_changed('LOANDTLNOTEPRINAMT',
      :old.LOANDTLNOTEPRINAMT,:new.LOANDTLNOTEPRINAMT);
    histlog_col_if_changed('LOANDTLNOTECURBALAMT',
      :old.LOANDTLNOTECURBALAMT,:new.LOANDTLNOTECURBALAMT);
    histlog_col_if_changed('LOANDTLNOTEINTRTPCT',
      :old.LOANDTLNOTEINTRTPCT,:new.LOANDTLNOTEINTRTPCT);
    histlog_col_if_changed('LOANDTLNOTEDT',
      to_char(:old.LOANDTLNOTEDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.LOANDTLNOTEDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('LOANDTLACCTCD',
      :old.LOANDTLACCTCD,:new.LOANDTLACCTCD);
    histlog_col_if_changed('LOANDTLACCTCHNGCD',
      :old.LOANDTLACCTCHNGCD,:new.LOANDTLACCTCHNGCD);
    histlog_col_if_changed('LOANDTLREAMIND',
      :old.LOANDTLREAMIND,:new.LOANDTLREAMIND);
    histlog_col_if_changed('LOANDTLNOTEPYMTDTTRM',
      :old.LOANDTLNOTEPYMTDTTRM,:new.LOANDTLNOTEPYMTDTTRM);
    histlog_col_if_changed('LOANDTLRESRVDEPAMT',
      :old.LOANDTLRESRVDEPAMT,:new.LOANDTLRESRVDEPAMT);
    histlog_col_if_changed('LOANDTLUNDRWTRFEEPCT',
      :old.LOANDTLUNDRWTRFEEPCT,:new.LOANDTLUNDRWTRFEEPCT);
    histlog_col_if_changed('LOANDTLUNDRWTRFEEMTDAMT',
      :old.LOANDTLUNDRWTRFEEMTDAMT,:new.LOANDTLUNDRWTRFEEMTDAMT);
    histlog_col_if_changed('LOANDTLUNDRWTRFEEYTDAMT',
      :old.LOANDTLUNDRWTRFEEYTDAMT,:new.LOANDTLUNDRWTRFEEYTDAMT);
    histlog_col_if_changed('LOANDTLUNDRWTRFEEPTDAMT',
      :old.LOANDTLUNDRWTRFEEPTDAMT,:new.LOANDTLUNDRWTRFEEPTDAMT);
    histlog_col_if_changed('LOANDTLCDCFEEPCT',
      :old.LOANDTLCDCFEEPCT,:new.LOANDTLCDCFEEPCT);
    histlog_col_if_changed('LOANDTLCDCFEEMTDAMT',
      :old.LOANDTLCDCFEEMTDAMT,:new.LOANDTLCDCFEEMTDAMT);
    histlog_col_if_changed('LOANDTLCDCFEEYTDAMT',
      :old.LOANDTLCDCFEEYTDAMT,:new.LOANDTLCDCFEEYTDAMT);
    histlog_col_if_changed('LOANDTLCDCFEEPTDAMT',
      :old.LOANDTLCDCFEEPTDAMT,:new.LOANDTLCDCFEEPTDAMT);
    histlog_col_if_changed('LOANDTLCSAFEEPCT',
      :old.LOANDTLCSAFEEPCT,:new.LOANDTLCSAFEEPCT);
    histlog_col_if_changed('LOANDTLCSAFEEMTDAMT',
      :old.LOANDTLCSAFEEMTDAMT,:new.LOANDTLCSAFEEMTDAMT);
    histlog_col_if_changed('LOANDTLCSAFEEYTDAMT',
      :old.LOANDTLCSAFEEYTDAMT,:new.LOANDTLCSAFEEYTDAMT);
    histlog_col_if_changed('LOANDTLCSAFEEPTDAMT',
      :old.LOANDTLCSAFEEPTDAMT,:new.LOANDTLCSAFEEPTDAMT);
    histlog_col_if_changed('LOANDTLATTORNEYFEEAMT',
      :old.LOANDTLATTORNEYFEEAMT,:new.LOANDTLATTORNEYFEEAMT);
    histlog_col_if_changed('LOANDTLINITFEEPCT',
      :old.LOANDTLINITFEEPCT,:new.LOANDTLINITFEEPCT);
    histlog_col_if_changed('LOANDTLINITFEEDOLLRAMT',
      :old.LOANDTLINITFEEDOLLRAMT,:new.LOANDTLINITFEEDOLLRAMT);
    histlog_col_if_changed('LOANDTLFUNDFEEPCT',
      :old.LOANDTLFUNDFEEPCT,:new.LOANDTLFUNDFEEPCT);
    histlog_col_if_changed('LOANDTLFUNDFEEDOLLRAMT',
      :old.LOANDTLFUNDFEEDOLLRAMT,:new.LOANDTLFUNDFEEDOLLRAMT);
    histlog_col_if_changed('LOANDTLCBNKNM',
      :old.LOANDTLCBNKNM,:new.LOANDTLCBNKNM);
    histlog_col_if_changed('LOANDTLCBNKMAILADRSTR1NM',
      :old.LOANDTLCBNKMAILADRSTR1NM,:new.LOANDTLCBNKMAILADRSTR1NM);
    histlog_col_if_changed('LOANDTLCBNKMAILADRSTR2NM',
      :old.LOANDTLCBNKMAILADRSTR2NM,:new.LOANDTLCBNKMAILADRSTR2NM);
    histlog_col_if_changed('LOANDTLCBNKMAILADRCTYNM',
      :old.LOANDTLCBNKMAILADRCTYNM,:new.LOANDTLCBNKMAILADRCTYNM);
    histlog_col_if_changed('LOANDTLCBNKMAILADRSTCD',
      :old.LOANDTLCBNKMAILADRSTCD,:new.LOANDTLCBNKMAILADRSTCD);
    histlog_col_if_changed('LOANDTLCBNKMAILADRZIPCD',
      :old.LOANDTLCBNKMAILADRZIPCD,:new.LOANDTLCBNKMAILADRZIPCD);
    histlog_col_if_changed('LOANDTLCBNKMAILADRZIP4CD',
      :old.LOANDTLCBNKMAILADRZIP4CD,:new.LOANDTLCBNKMAILADRZIP4CD);
    histlog_col_if_changed('LOANDTLCACCTNM',
      :old.LOANDTLCACCTNM,:new.LOANDTLCACCTNM);
    histlog_col_if_changed('LOANDTLCOLOANDTLACCT',
      :old.LOANDTLCOLOANDTLACCT,:new.LOANDTLCOLOANDTLACCT);
    histlog_col_if_changed('LOANDTLCROUTSYM',
      :old.LOANDTLCROUTSYM,:new.LOANDTLCROUTSYM);
    histlog_col_if_changed('LOANDTLCTRANSCD',
      :old.LOANDTLCTRANSCD,:new.LOANDTLCTRANSCD);
    histlog_col_if_changed('LOANDTLCATTEN',
      :old.LOANDTLCATTEN,:new.LOANDTLCATTEN);
    histlog_col_if_changed('LOANDTLRBNKNM',
      :old.LOANDTLRBNKNM,:new.LOANDTLRBNKNM);
    histlog_col_if_changed('LOANDTLRBNKMAILADRSTR1NM',
      :old.LOANDTLRBNKMAILADRSTR1NM,:new.LOANDTLRBNKMAILADRSTR1NM);
    histlog_col_if_changed('LOANDTLRBNKMAILADRSTR2NM',
      :old.LOANDTLRBNKMAILADRSTR2NM,:new.LOANDTLRBNKMAILADRSTR2NM);
    histlog_col_if_changed('LOANDTLRBNKMAILADRCTYNM',
      :old.LOANDTLRBNKMAILADRCTYNM,:new.LOANDTLRBNKMAILADRCTYNM);
    histlog_col_if_changed('LOANDTLRBNKMAILADRSTCD',
      :old.LOANDTLRBNKMAILADRSTCD,:new.LOANDTLRBNKMAILADRSTCD);
    histlog_col_if_changed('LOANDTLRBNKMAILADRZIPCD',
      :old.LOANDTLRBNKMAILADRZIPCD,:new.LOANDTLRBNKMAILADRZIPCD);
    histlog_col_if_changed('LOANDTLRBNKMAILADRZIP4CD',
      :old.LOANDTLRBNKMAILADRZIP4CD,:new.LOANDTLRBNKMAILADRZIP4CD);
    histlog_col_if_changed('LOANDTLRACCTNM',
      :old.LOANDTLRACCTNM,:new.LOANDTLRACCTNM);
    histlog_col_if_changed('LOANDTLROLOANDTLACCT',
      :old.LOANDTLROLOANDTLACCT,:new.LOANDTLROLOANDTLACCT);
    histlog_col_if_changed('LOANDTLRROUTSYM',
      :old.LOANDTLRROUTSYM,:new.LOANDTLRROUTSYM);
    histlog_col_if_changed('LOANDTLTRANSCD',
      :old.LOANDTLTRANSCD,:new.LOANDTLTRANSCD);
    histlog_col_if_changed('LOANDTLRATTEN',
      :old.LOANDTLRATTEN,:new.LOANDTLRATTEN);
    histlog_col_if_changed('LOANDTLPYMT1AMT',
      :old.LOANDTLPYMT1AMT,:new.LOANDTLPYMT1AMT);
    histlog_col_if_changed('LOANDTLPYMTDT1X',
      :old.LOANDTLPYMTDT1X,:new.LOANDTLPYMTDT1X);
    histlog_col_if_changed('LOANDTLPRIN1AMT',
      :old.LOANDTLPRIN1AMT,:new.LOANDTLPRIN1AMT);
    histlog_col_if_changed('LOANDTLINT1AMT',
      :old.LOANDTLINT1AMT,:new.LOANDTLINT1AMT);
    histlog_col_if_changed('LOANDTLCSA1PCT',
      :old.LOANDTLCSA1PCT,:new.LOANDTLCSA1PCT);
    histlog_col_if_changed('LOANDTLCSADOLLR1AMT',
      :old.LOANDTLCSADOLLR1AMT,:new.LOANDTLCSADOLLR1AMT);
    histlog_col_if_changed('LOANDTLCDC1PCT',
      :old.LOANDTLCDC1PCT,:new.LOANDTLCDC1PCT);
    histlog_col_if_changed('LOANDTLCDCDOLLR1AMT',
      :old.LOANDTLCDCDOLLR1AMT,:new.LOANDTLCDCDOLLR1AMT);
    histlog_col_if_changed('LOANDTLSTMTNM',
      :old.LOANDTLSTMTNM,:new.LOANDTLSTMTNM);
    histlog_col_if_changed('LOANDTLCURCSAFEEAMT',
      :old.LOANDTLCURCSAFEEAMT,:new.LOANDTLCURCSAFEEAMT);
    histlog_col_if_changed('LOANDTLCURCDCFEEAMT',
      :old.LOANDTLCURCDCFEEAMT,:new.LOANDTLCURCDCFEEAMT);
    histlog_col_if_changed('LOANDTLCURRESRVAMT',
      :old.LOANDTLCURRESRVAMT,:new.LOANDTLCURRESRVAMT);
    histlog_col_if_changed('LOANDTLCURFEEBALAMT',
      :old.LOANDTLCURFEEBALAMT,:new.LOANDTLCURFEEBALAMT);
    histlog_col_if_changed('LOANDTLAMPRINLEFTAMT',
      :old.LOANDTLAMPRINLEFTAMT,:new.LOANDTLAMPRINLEFTAMT);
    histlog_col_if_changed('LOANDTLAMTHRUDTX',
      :old.LOANDTLAMTHRUDTX,:new.LOANDTLAMTHRUDTX);
    histlog_col_if_changed('LOANDTLAPPIND',
      :old.LOANDTLAPPIND,:new.LOANDTLAPPIND);
    histlog_col_if_changed('LOANDTLSPPIND',
      :old.LOANDTLSPPIND,:new.LOANDTLSPPIND);
    histlog_col_if_changed('LOANDTLSPPLQDAMT',
      :old.LOANDTLSPPLQDAMT,:new.LOANDTLSPPLQDAMT);
    histlog_col_if_changed('LOANDTLSPPAUTOPYMTAMT',
      :old.LOANDTLSPPAUTOPYMTAMT,:new.LOANDTLSPPAUTOPYMTAMT);
    histlog_col_if_changed('LOANDTLSPPAUTOPCT',
      :old.LOANDTLSPPAUTOPCT,:new.LOANDTLSPPAUTOPCT);
    histlog_col_if_changed('LOANDTLSTMTINT1AMT',
      :old.LOANDTLSTMTINT1AMT,:new.LOANDTLSTMTINT1AMT);
    histlog_col_if_changed('LOANDTLSTMTINT2AMT',
      :old.LOANDTLSTMTINT2AMT,:new.LOANDTLSTMTINT2AMT);
    histlog_col_if_changed('LOANDTLSTMTINT3AMT',
      :old.LOANDTLSTMTINT3AMT,:new.LOANDTLSTMTINT3AMT);
    histlog_col_if_changed('LOANDTLSTMTINT4AMT',
      :old.LOANDTLSTMTINT4AMT,:new.LOANDTLSTMTINT4AMT);
    histlog_col_if_changed('LOANDTLSTMTINT5AMT',
      :old.LOANDTLSTMTINT5AMT,:new.LOANDTLSTMTINT5AMT);
    histlog_col_if_changed('LOANDTLSTMTINT6AMT',
      :old.LOANDTLSTMTINT6AMT,:new.LOANDTLSTMTINT6AMT);
    histlog_col_if_changed('LOANDTLSTMTINT7AMT',
      :old.LOANDTLSTMTINT7AMT,:new.LOANDTLSTMTINT7AMT);
    histlog_col_if_changed('LOANDTLSTMTINT8AMT',
      :old.LOANDTLSTMTINT8AMT,:new.LOANDTLSTMTINT8AMT);
    histlog_col_if_changed('LOANDTLSTMTINT9AMT',
      :old.LOANDTLSTMTINT9AMT,:new.LOANDTLSTMTINT9AMT);
    histlog_col_if_changed('LOANDTLSTMTINT10AMT',
      :old.LOANDTLSTMTINT10AMT,:new.LOANDTLSTMTINT10AMT);
    histlog_col_if_changed('LOANDTLSTMTINT11AMT',
      :old.LOANDTLSTMTINT11AMT,:new.LOANDTLSTMTINT11AMT);
    histlog_col_if_changed('LOANDTLSTMTINT12AMT',
      :old.LOANDTLSTMTINT12AMT,:new.LOANDTLSTMTINT12AMT);
    histlog_col_if_changed('LOANDTLSTMTINT13AMT',
      :old.LOANDTLSTMTINT13AMT,:new.LOANDTLSTMTINT13AMT);
    histlog_col_if_changed('LOANDTLSTMTAVGMOBALAMT',
      :old.LOANDTLSTMTAVGMOBALAMT,:new.LOANDTLSTMTAVGMOBALAMT);
    histlog_col_if_changed('LOANDTLAMPRINAMT',
      :old.LOANDTLAMPRINAMT,:new.LOANDTLAMPRINAMT);
    histlog_col_if_changed('LOANDTLAMINTAMT',
      :old.LOANDTLAMINTAMT,:new.LOANDTLAMINTAMT);
    histlog_col_if_changed('LOANDTLREFIIND',
      :old.LOANDTLREFIIND,:new.LOANDTLREFIIND);
    histlog_col_if_changed('LOANDTLSETUPIND',
      :old.LOANDTLSETUPIND,:new.LOANDTLSETUPIND);
    histlog_col_if_changed('LOANDTLCREATDT',
      to_char(:old.LOANDTLCREATDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.LOANDTLCREATDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('LOANDTLLMAINTNDT',
      to_char(:old.LOANDTLLMAINTNDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.LOANDTLLMAINTNDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('LOANDTLCONVDT',
      to_char(:old.LOANDTLCONVDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.LOANDTLCONVDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('LOANDTLPRGRM',
      :old.LOANDTLPRGRM,:new.LOANDTLPRGRM);
    histlog_col_if_changed('LOANDTLNOTEMOPYMTAMT',
      :old.LOANDTLNOTEMOPYMTAMT,:new.LOANDTLNOTEMOPYMTAMT);
    histlog_col_if_changed('LOANDTLDBENTRMATDT',
      to_char(:old.LOANDTLDBENTRMATDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.LOANDTLDBENTRMATDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('LOANDTLNOTEMATDT',
      to_char(:old.LOANDTLNOTEMATDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.LOANDTLNOTEMATDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('LOANDTLRESRVDEPPCT',
      :old.LOANDTLRESRVDEPPCT,:new.LOANDTLRESRVDEPPCT);
    histlog_col_if_changed('LOANDTLCDCPFEEPCT',
      :old.LOANDTLCDCPFEEPCT,:new.LOANDTLCDCPFEEPCT);
    histlog_col_if_changed('LOANDTLCDCPFEEDOLLRAMT',
      :old.LOANDTLCDCPFEEDOLLRAMT,:new.LOANDTLCDCPFEEDOLLRAMT);
    histlog_col_if_changed('LOANDTLDUEBORRAMT',
      :old.LOANDTLDUEBORRAMT,:new.LOANDTLDUEBORRAMT);
    histlog_col_if_changed('LOANDTLAPPVDT',
      to_char(:old.LOANDTLAPPVDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.LOANDTLAPPVDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('LOANDTLLTFEEIND',
      :old.LOANDTLLTFEEIND,:new.LOANDTLLTFEEIND);
    histlog_col_if_changed('LOANDTLACHPRENTIND',
      :old.LOANDTLACHPRENTIND,:new.LOANDTLACHPRENTIND);
    histlog_col_if_changed('LOANDTLAMSCHDLTYP',
      :old.LOANDTLAMSCHDLTYP,:new.LOANDTLAMSCHDLTYP);
    histlog_col_if_changed('LOANDTLACHBNKNM',
      :old.LOANDTLACHBNKNM,:new.LOANDTLACHBNKNM);
    histlog_col_if_changed('LOANDTLACHBNKMAILADRSTR1NM',
      :old.LOANDTLACHBNKMAILADRSTR1NM,:new.LOANDTLACHBNKMAILADRSTR1NM);
    histlog_col_if_changed('LOANDTLACHBNKMAILADRSTR2NM',
      :old.LOANDTLACHBNKMAILADRSTR2NM,:new.LOANDTLACHBNKMAILADRSTR2NM);
    histlog_col_if_changed('LOANDTLACHBNKMAILADRCTYNM',
      :old.LOANDTLACHBNKMAILADRCTYNM,:new.LOANDTLACHBNKMAILADRCTYNM);
    histlog_col_if_changed('LOANDTLACHBNKMAILADRSTCD',
      :old.LOANDTLACHBNKMAILADRSTCD,:new.LOANDTLACHBNKMAILADRSTCD);
    histlog_col_if_changed('LOANDTLACHBNKMAILADRZIPCD',
      :old.LOANDTLACHBNKMAILADRZIPCD,:new.LOANDTLACHBNKMAILADRZIPCD);
    histlog_col_if_changed('LOANDTLACHBNKMAILADRZIP4CD',
      :old.LOANDTLACHBNKMAILADRZIP4CD,:new.LOANDTLACHBNKMAILADRZIP4CD);
    histlog_col_if_changed('LOANDTLACHBNKBR',
      :old.LOANDTLACHBNKBR,:new.LOANDTLACHBNKBR);
    histlog_col_if_changed('LOANDTLACHBNKACTYP',
      :old.LOANDTLACHBNKACTYP,:new.LOANDTLACHBNKACTYP);
    histlog_col_if_changed('LOANDTLACHBNKACCT',
      :old.LOANDTLACHBNKACCT,:new.LOANDTLACHBNKACCT);
    histlog_col_if_changed('LOANDTLACHBNKROUTNMB',
      :old.LOANDTLACHBNKROUTNMB,:new.LOANDTLACHBNKROUTNMB);
    histlog_col_if_changed('LOANDTLACHBNKTRANS',
      :old.LOANDTLACHBNKTRANS,:new.LOANDTLACHBNKTRANS);
    histlog_col_if_changed('LOANDTLACHBNKIDNOORATTEN',
      :old.LOANDTLACHBNKIDNOORATTEN,:new.LOANDTLACHBNKIDNOORATTEN);
    histlog_col_if_changed('LOANDTLACHDEPNM',
      :old.LOANDTLACHDEPNM,:new.LOANDTLACHDEPNM);
    histlog_col_if_changed('LOANDTLACHSIGNDT',
      to_char(:old.LOANDTLACHSIGNDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.LOANDTLACHSIGNDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('LOANDTLBORRNM2',
      :old.LOANDTLBORRNM2,:new.LOANDTLBORRNM2);
    histlog_col_if_changed('LOANDTLSBCNM2',
      :old.LOANDTLSBCNM2,:new.LOANDTLSBCNM2);
    histlog_col_if_changed('LOANDTLCACCTNMB',
      :old.LOANDTLCACCTNMB,:new.LOANDTLCACCTNMB);
    histlog_col_if_changed('LOANDTLRACCTNMB',
      :old.LOANDTLRACCTNMB,:new.LOANDTLRACCTNMB);
    histlog_col_if_changed('LOANDTLINTLENDRIND',
      :old.LOANDTLINTLENDRIND,:new.LOANDTLINTLENDRIND);
    histlog_col_if_changed('LOANDTLINTRMLENDR',
      :old.LOANDTLINTRMLENDR,:new.LOANDTLINTRMLENDR);
    histlog_col_if_changed('LOANDTLPYMTRECV',
      :old.LOANDTLPYMTRECV,:new.LOANDTLPYMTRECV);
    histlog_col_if_changed('LOANDTLTRMYR',
      :old.LOANDTLTRMYR,:new.LOANDTLTRMYR);
    histlog_col_if_changed('LOANDTLLOANTYP',
      :old.LOANDTLLOANTYP,:new.LOANDTLLOANTYP);
    histlog_col_if_changed('LOANDTLMLACCTNMB',
      :old.LOANDTLMLACCTNMB,:new.LOANDTLMLACCTNMB);
    histlog_col_if_changed('LOANDTLEXCESSDUEBORRAMT',
      :old.LOANDTLEXCESSDUEBORRAMT,:new.LOANDTLEXCESSDUEBORRAMT);
    histlog_col_if_changed('LOANDTL503SUBTYP',
      :old.LOANDTL503SUBTYP,:new.LOANDTL503SUBTYP);
    histlog_col_if_changed('LOANDTLLPYMTDT',
      to_char(:old.LOANDTLLPYMTDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.LOANDTLLPYMTDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('LOANDTLPOOLSERS',
      :old.LOANDTLPOOLSERS,:new.LOANDTLPOOLSERS);
    histlog_col_if_changed('LOANDTLSBAFEEPCT',
      :old.LOANDTLSBAFEEPCT,:new.LOANDTLSBAFEEPCT);
    histlog_col_if_changed('LOANDTLSBAPAIDTHRUDTX',
      :old.LOANDTLSBAPAIDTHRUDTX,:new.LOANDTLSBAPAIDTHRUDTX);
    histlog_col_if_changed('LOANDTLSBAFEEAMT',
      :old.LOANDTLSBAFEEAMT,:new.LOANDTLSBAFEEAMT);
    histlog_col_if_changed('LOANDTLPRMAMT',
      :old.LOANDTLPRMAMT,:new.LOANDTLPRMAMT);
    histlog_col_if_changed('LOANDTLLENDRSBAFEEAMT',
      :old.LOANDTLLENDRSBAFEEAMT,:new.LOANDTLLENDRSBAFEEAMT);
    histlog_col_if_changed('LOANDTLWITHHELOANDTLIND',
      :old.LOANDTLWITHHELOANDTLIND,:new.LOANDTLWITHHELOANDTLIND);
    histlog_col_if_changed('LOANDTLPRGRMTYP',
      :old.LOANDTLPRGRMTYP,:new.LOANDTLPRGRMTYP);
    histlog_col_if_changed('LOANDTLLATEIND',
      :old.LOANDTLLATEIND,:new.LOANDTLLATEIND);
    histlog_col_if_changed('LOANDTLCMNT1',
      :old.LOANDTLCMNT1,:new.LOANDTLCMNT1);
    histlog_col_if_changed('LOANDTLCMNT2',
      :old.LOANDTLCMNT2,:new.LOANDTLCMNT2);
    histlog_col_if_changed('LOANDTLSOD',
      :old.LOANDTLSOD,:new.LOANDTLSOD);
    histlog_col_if_changed('LOANDTLTOBECURAMT',
      :old.LOANDTLTOBECURAMT,:new.LOANDTLTOBECURAMT);
    histlog_col_if_changed('LOANDTLGNTYREPAYPTDAMT',
      :old.LOANDTLGNTYREPAYPTDAMT,:new.LOANDTLGNTYREPAYPTDAMT);
    histlog_col_if_changed('LOANDTLCDCPAIDTHRUDTX',
      :old.LOANDTLCDCPAIDTHRUDTX,:new.LOANDTLCDCPAIDTHRUDTX);
    histlog_col_if_changed('LOANDTLCSAPAIDTHRUDTX',
      :old.LOANDTLCSAPAIDTHRUDTX,:new.LOANDTLCSAPAIDTHRUDTX);
    histlog_col_if_changed('LOANDTLINTPAIDTHRUDTX',
      :old.LOANDTLINTPAIDTHRUDTX,:new.LOANDTLINTPAIDTHRUDTX);
    histlog_col_if_changed('LOANDTLSTRTBAL1AMT',
      :old.LOANDTLSTRTBAL1AMT,:new.LOANDTLSTRTBAL1AMT);
    histlog_col_if_changed('LOANDTLSTRTBAL2AMT',
      :old.LOANDTLSTRTBAL2AMT,:new.LOANDTLSTRTBAL2AMT);
    histlog_col_if_changed('LOANDTLSTRTBAL3AMT',
      :old.LOANDTLSTRTBAL3AMT,:new.LOANDTLSTRTBAL3AMT);
    histlog_col_if_changed('LOANDTLSTRTBAL4AMT',
      :old.LOANDTLSTRTBAL4AMT,:new.LOANDTLSTRTBAL4AMT);
    histlog_col_if_changed('LOANDTLSTRTBAL5AMT',
      :old.LOANDTLSTRTBAL5AMT,:new.LOANDTLSTRTBAL5AMT);
    histlog_col_if_changed('LOANDTLSBCIDNMB',
      :old.LOANDTLSBCIDNMB,:new.LOANDTLSBCIDNMB);
    histlog_col_if_changed('LOANDTLACHLASTCHNGDT',
      to_char(:old.LOANDTLACHLASTCHNGDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.LOANDTLACHLASTCHNGDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('LOANDTLPRENOTETESTDT',
      to_char(:old.LOANDTLPRENOTETESTDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.LOANDTLPRENOTETESTDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('LOANDTLPYMTNMBDUE',
      :old.LOANDTLPYMTNMBDUE,:new.LOANDTLPYMTNMBDUE);
    histlog_col_if_changed('LOANDTLRESRVAMT',
      :old.LOANDTLRESRVAMT,:new.LOANDTLRESRVAMT);
    histlog_col_if_changed('LOANDTLFEEBASEAMT',
      :old.LOANDTLFEEBASEAMT,:new.LOANDTLFEEBASEAMT);
    histlog_col_if_changed('LOANDTLSTATCDOFLOAN',
      :old.LOANDTLSTATCDOFLOAN,:new.LOANDTLSTATCDOFLOAN);
    histlog_col_if_changed('LOANDTLACTUALCSAINITFEEAMT',
      :old.LOANDTLACTUALCSAINITFEEAMT,:new.LOANDTLACTUALCSAINITFEEAMT);
    histlog_col_if_changed('LOANDTLSTATDT',
      to_char(:old.LOANDTLSTATDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.LOANDTLSTATDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('LOANDTLSTMTCLSBALAMT',
      :old.LOANDTLSTMTCLSBALAMT,:new.LOANDTLSTMTCLSBALAMT);
    histlog_col_if_changed('LOANDTLSTMTCLSDT',
      to_char(:old.LOANDTLSTMTCLSDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.LOANDTLSTMTCLSDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('LOANDTLLASTDBPAYOUTAMT',
      :old.LOANDTLLASTDBPAYOUTAMT,:new.LOANDTLLASTDBPAYOUTAMT);
    histlog_col_if_changed('LOANDTLCHEMICALBASISDAYS',
      :old.LOANDTLCHEMICALBASISDAYS,:new.LOANDTLCHEMICALBASISDAYS);
    histlog_col_if_changed('CREATUSERID',
      :old.CREATUSERID,:new.CREATUSERID);
    histlog_col_if_changed('CREATDT',
      to_char(:old.CREATDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CREATDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('LASTUPDTUSERID',
      :old.LASTUPDTUSERID,:new.LASTUPDTUSERID);
    histlog_col_if_changed('LASTUPDTDT',
      to_char(:old.LASTUPDTDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.LASTUPDTDT,'YYYY-MM-DD HH24:MI:SS'));
    -- write out any accumulated logdtl recs with bulk insert
    if logdtl_ctr>1 then
      forall r in 2..logdtl_ctr insert into stgcsa.stgupdtlogdtltbl values logdtl(r);
    end if;
  end if;  
  -- end of history logging section
end;

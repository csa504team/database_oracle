create or replace trigger STGCSA.ACCPRCSCYCSTATTRG
    after insert or update or delete ON STGCSA.ACCPRCSCYCSTATTBL
    REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE
 /*
  Created on: 2019-05-04 08:24:33
  Created by: GENR
  Crerated from template csatrig_template v3.4 3 May 2019 on 2019-05-04 08:24:33
    Using SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
    Trigger handles errors by not updating and raising error -20100.
    Caller should handle failing SQL statement, including recording.
*/
  calling_module varchar2(80);
  tabowner varchar2(32):='STGCSA';
  tabname varchar2(32):='ACCPRCSCYCSTATTBL';
  -- vars for history logging
  hst stgupdtlogtbl%rowtype;
  TYPE logdtl_typ is table of STGCSA.stgupdtlogdtltbl%rowtype;
  logdtl logdtl_typ:=logdtl_typ();
  logdtl_ctr number:=0;
procedure histlog_col_if_changed(colnam varchar2, oldval varchar2, newval varchar2) is
begin
  If (inserting and newval is not null)
    or (deleting and oldval is not null)
    or (updating and 
         (oldval<>newval 
          or (oldval is null and newval is not null)
          or (oldval is not null and newval is null)
         )
       )                  
  then 
    logdtl.extend;
    logdtl_ctr:=logdtl_ctr+1;
    logdtl(logdtl_ctr):=logdtl(1);
    logdtl(logdtl_ctr).colname:=colnam;
    if inserting or updating then 
      logdtl(logdtl_ctr).newvalue:=newval; 
    else
      logdtl(logdtl_ctr).newvalue:=null;       
    end if;  
    if updating or deleting then 
      logdtl(logdtl_ctr).oldvalue:=oldval;
    else
      logdtl(logdtl_ctr).oldvalue:=null;      
    end if;
  end if;
end;
--
--
begin
  select SYS_CONTEXT ('USERENV', 'MODULE') into calling_module from dual;
  if calling_module='Data Pump Worker' then return; end if;
  --
  -- History logging (HST)... initialize 
  -- Skip if datahist logging is explicitly turned fo via switch in runtime
  if runtime.datahist_logging_on_YN='Y' then
    SELECT LOGENTRYIDSEQ.NEXTVAL, sysdate 
      into hst.updtlogid, hst.updtdt FROM DUAL;
    -- set aside table pk, set managed variables
    hst.csa_sid:=runtime.csa_session_id;
    hst.loannmb:=null;
    hst.tablenm:='ACCPRCSCYCSTATTBL';
    hst.loannmb:=null;      
    hst.updtuserid:=:new.lastupdtuserid;
    if deleting=true then hst.updtuserid:=runtime.csa_userid; end if;
    if hst.updtuserid is null then hst.updtuserid:=user; end if;
    case
      when inserting=true then hst.upd_action:='I'; 
      when updating=true then hst.upd_action:='U';
      when deleting=true then hst.upd_action:='D';
    end case; 
    -- now set composit PK for row being updated  
    hst.pk:=null         
      ||rpad(to_char(nvl(:new.CYCPRCSID,:old.CYCPRCSID)),22)
;  
    -- insert log main row
    INSERT INTO stgupdtlogtbl values HST;
    --
    -- Now column value logging for changed columns
    --   Initialize logdtl collection.
    logdtl.extend;
    logdtl_ctr:=1;
    logdtl(logdtl_ctr).updtlogfk:=hst.updtlogid;
    logdtl(logdtl_ctr).oldvalue:=null;
    logdtl(logdtl_ctr).newvalue:=null;
    histlog_col_if_changed('CYCPRCSID',
      :old.CYCPRCSID,:new.CYCPRCSID);
    histlog_col_if_changed('CYCID',
      :old.CYCID,:new.CYCID);
    histlog_col_if_changed('PRCSID',
      :old.PRCSID,:new.PRCSID);
    histlog_col_if_changed('SCHDLDT',
      to_char(:old.SCHDLDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.SCHDLDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('SCHDLUSER',
      :old.SCHDLUSER,:new.SCHDLUSER);
    histlog_col_if_changed('SETUPDT',
      to_char(:old.SETUPDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.SETUPDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('LASTREFRESHDT',
      to_char(:old.LASTREFRESHDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.LASTREFRESHDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('INITIALRVWCMPLT',
      :old.INITIALRVWCMPLT,:new.INITIALRVWCMPLT);
    histlog_col_if_changed('INITIALRVWUSER',
      :old.INITIALRVWUSER,:new.INITIALRVWUSER);
    histlog_col_if_changed('INITIALRVWDT',
      to_char(:old.INITIALRVWDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.INITIALRVWDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('PEERRVWCMPLT',
      :old.PEERRVWCMPLT,:new.PEERRVWCMPLT);
    histlog_col_if_changed('PEERRVWUSER',
      :old.PEERRVWUSER,:new.PEERRVWUSER);
    histlog_col_if_changed('PEERRVWDT',
      to_char(:old.PEERRVWDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.PEERRVWDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('UPDTCMPLT',
      :old.UPDTCMPLT,:new.UPDTCMPLT);
    histlog_col_if_changed('UPDTCMPLTUSER',
      :old.UPDTCMPLTUSER,:new.UPDTCMPLTUSER);
    histlog_col_if_changed('UPDTCMPLTDT',
      to_char(:old.UPDTCMPLTDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.UPDTCMPLTDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('MGRRVWCMPLT',
      :old.MGRRVWCMPLT,:new.MGRRVWCMPLT);
    histlog_col_if_changed('MGRRVWUSER',
      :old.MGRRVWUSER,:new.MGRRVWUSER);
    histlog_col_if_changed('MGRRVWDT',
      to_char(:old.MGRRVWDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.MGRRVWDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('UPDTRUNNING',
      :old.UPDTRUNNING,:new.UPDTRUNNING);
    histlog_col_if_changed('UPDTRUNNINGUSER',
      :old.UPDTRUNNINGUSER,:new.UPDTRUNNINGUSER);
    histlog_col_if_changed('PRCSCYCCMNT',
      :old.PRCSCYCCMNT,:new.PRCSCYCCMNT);
    histlog_col_if_changed('PROPOSEDDT',
      to_char(:old.PROPOSEDDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.PROPOSEDDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('TIMESTAMPFLD',
      to_char(:old.TIMESTAMPFLD,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.TIMESTAMPFLD,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('CREATUSERID',
      :old.CREATUSERID,:new.CREATUSERID);
    histlog_col_if_changed('CREATDT',
      to_char(:old.CREATDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.CREATDT,'YYYY-MM-DD HH24:MI:SS'));
    histlog_col_if_changed('LASTUPDTUSERID',
      :old.LASTUPDTUSERID,:new.LASTUPDTUSERID);
    histlog_col_if_changed('LASTUPDTDT',
      to_char(:old.LASTUPDTDT,'YYYY-MM-DD HH24:MI:SS'),to_char(:new.LASTUPDTDT,'YYYY-MM-DD HH24:MI:SS'));
    -- write out any accumulated logdtl recs with bulk insert
    if logdtl_ctr>1 then
      forall r in 2..logdtl_ctr insert into stgcsa.stgupdtlogdtltbl values logdtl(r);
    end if;
  end if;  
  -- end of history logging section
end;

GRANT EXECUTE ON cdconline.ltrrlseaudtseltsp TO cdconlinereadallrole;
GRANT EXECUTE ON cdconline.ltrrlseaudtseltsp TO cdconlineprtread;
GRANT EXECUTE ON cdconline.ltrrlseaudtseltsp TO cdconlinegovread;

GRANT SELECT ON CDCONLINE.LtrPPWorkUpAudtTbl TO CDCONLINEREADALLROLE;
GRANT SELECT ON CDCONLINE.LtrPPWorkUpAudtTbl TO cdconlineprtread;
GRANT SELECT ON CDCONLINE.LtrPPWorkUpAudtTbl TO cdconlinegovread;

GRANT SELECT ON CDCONLINE.LtrTransctHistAudtTbl TO CDCONLINEREADALLROLE;
GRANT SELECT ON CDCONLINE.LtrTransctHistAudtTbl TO cdconlineprtread;
GRANT SELECT ON CDCONLINE.LtrTransctHistAudtTbl TO cdconlinegovread;

GRANT EXECUTE ON cdconline.ltrtranscthistaudtseltsp TO cdconlinereadallrole;
GRANT EXECUTE ON cdconline.ltrtranscthistaudtseltsp TO cdconlineprtread;
GRANT EXECUTE ON cdconline.ltrtranscthistaudtseltsp TO cdconlinegovread;

GRANT EXECUTE ON cdconline.ltrppworkupaudtinstsp TO cdconlinereadallrole;
GRANT EXECUTE ON cdconline.ltrppworkupaudtinstsp TO cdconlineprtread;
GRANT EXECUTE ON cdconline.ltrppworkupaudtinstsp TO cdconlinegovread;

GRANT EXECUTE ON cdconline.ltrtranscthistaudtinstsp TO cdconlinereadallrole;
GRANT EXECUTE ON cdconline.ltrtranscthistaudtinstsp TO cdconlineprtread;
GRANT EXECUTE ON cdconline.ltrtranscthistaudtinstsp TO cdconlinegovread;

GRANT SELECT ON CDCONLINE.LtrRlseAudtTbl TO CDCONLINEREADALLROLE;
GRANT SELECT ON CDCONLINE.LtrRlseAudtTbl TO cdconlineprtread;
GRANT SELECT ON CDCONLINE.LtrRlseAudtTbl TO cdconlinegovread;

GRANT EXECUTE ON cdconline.ltrppworkupaudtseltsp TO cdconlinereadallrole;
GRANT EXECUTE ON cdconline.ltrppworkupaudtseltsp TO cdconlineprtread;
GRANT EXECUTE ON cdconline.ltrppworkupaudtseltsp TO cdconlinegovread;

GRANT EXECUTE ON cdconline.ltrrlseaudtinstsp TO cdconlinereadallrole;
GRANT EXECUTE ON cdconline.ltrrlseaudtinstsp TO cdconlineprtread;
GRANT EXECUTE ON cdconline.ltrrlseaudtinstsp TO cdconlinegovread;

CREATE OR REPLACE PROCEDURE CDCONLINE.SYSHEALTHCHKCSP
/*******************************************************************
* PROCEDURE : CDCONLINE.SYSHEALTHCHKCSP
* PURPOSE   : Stored Procedure to run System Health Check calculations and populate the report data
* CREATED   : 12/20/2020
* MODIFIED  : 03/08/2021
* AUTHOR    : Siva Maniam
* HISTORY   : Initial script

*-------------------------------------------------------------------
*******************************************************************/
(
  p_RunDt   IN  DATE,
  p_SelCur1 OUT SYS_REFCURSOR
)
AS
   v_run_date       DATE            := p_RunDt                              ;   /* RUNDATE - Date of this PROC is RUN */
   v_run_id         NUMBER(10,0)    := 0                                    ;   /* RUNID - One run per Day. First RUN -> INSERT Data. Subsequent runs in the day -> RETRIEVE the RUNID and USE the same data  */
   v_json_str       VARCHAR2(4000)  := NULL                                 ;   /* JSON String that holds the run data. Used by Coldfusion application to display / send email the status report */
   v_next_runid     NUMBER(10,0)    := 0                                    ;   /* BEFORE INsert the JSON String, GET last RunID and ADD 1 to obtain new Run ID */ 
   v_dummy_date     DATE            := to_date('1900/01/01','YYYY/MM/DD')   ;   /* Dummy Date to hold date format with Zero data = 1900/01/01  */
   v_dummy_TBLID    NUMBER          := 0                                    ;   /* Dummy TBLID - Set value to 0 */
   v_dummy_RPTID    NUMBER          := 0                                    ;   /* Dummy RPTID - Set value to 0 */
   v_thisYear       Number(4,0)     := to_char(sysdate, 'YYYY')             ;   /* YEAR Part of Run Date */
   v_thisMon        Number(4,0)     := to_char(sysdate, 'MM')               ;   /* MON Part of Run Date */
   v_thisDay        CHAR(3)         := to_char(sysdate, 'DY')               ;   /* DAY Part of Run Date 3 Char Abbr */
   v_tmp            NUMBER          := 0                                    ;


   /*  [TBLID-4: CDCONLINE.ANNSTMTLOANINFOTBL]    */
   v_4_TBLID        NUMBER          ;   /* TBLID    */
   v_4_TBLNAME      VARCHAR2(100)   ;   /* TBLNAME */
   v_4_RPTFREQNUM   NUMBER          ;   /* REPORT FREQ NUMBER VALUE */
   v_4_RPTFREQMDY   CHAR(1)         ;   /* REPORT FREQ MONTH/DAY/YEAR */
   v_4_RPTID        NUMBER          ;   /* RPTID   */
   v_4_RPTNAME      VARCHAR2(100)   ;   /* RPTNAME  */
   v_4_EXPECTEDNEXTRUNDT    DATE    ;   /* EXPECTED NEXT RUN DATE */
   v_4_STATUS       VARCHAR2(20)    ;   /* STATUS OF THE TABLE BASED ON LAST POPULATED DATE COMPARED TO TODAY */
   v_4_LASTUPDTDT   DATE            ;   /* TABLR LAST UPDATED DATE */
   v_4_FACTOR       NUMBER          ;   /* FACTOR TO MULTIPLY RPTFREQNUM BASED ON RPTFREQMDY. IF YEAR, MULTIPLY BY 12 FOR MONTH*/


   /* [TBLID-8: CDCONLINE.CNFRMCOPYTBL]  */
   v_8_TBLID        NUMBER          ;   /* TBLID    */
   v_8_TBLNAME      VARCHAR2(100)   ;   /* TBLNAME */
   v_8_RPTFREQNUM   NUMBER          ;   /* REPORT FREQ NUMBER VALUE */
   v_8_RPTFREQMDY   CHAR(1)         ;   /* REPORT FREQ MONTH/DAY/YEAR */
   v_8_RPTID        NUMBER          ;   /* RPTID   */
   v_8_RPTNAME      VARCHAR2(100)   ;   /* RPTNAME  */
   v_8_EXPECTEDNEXTRUNDT    DATE    ;   /* EXPECTED NEXT RUN DATE */
   v_8_STATUS       VARCHAR2(20)    ;   /* STATUS OF THE TABLE BASED ON LAST POPULATED DATE COMPARED TO TODAY */
   v_8_LASTUPDTDT   DATE            ;   /* TABLR LAST UPDATED DATE */
   v_8_FACTOR       NUMBER          ;   /* FACTOR TO MULTIPLY RPTFREQNUM BASED ON RPTFREQMDY. IF YEAR, MULTIPLY BY 12 FOR MONTH*/
   v_8_RUNDAY       VARCHAR2(32)    ;   /* DAYS the TABLE is UPDATED; So Montor have to ALERT next day; Grace period 1 day */

   /* [TBLID-11: CDCONLINE.FUNDINGSTMTTBL]  */
   v_11_TBLID        NUMBER          ;   /* TBLID    */
   v_11_TBLNAME      VARCHAR2(100)   ;   /* TBLNAME */
   v_11_RPTFREQNUM   NUMBER          ;   /* REPORT FREQ NUMBER VALUE */
   v_11_RPTFREQMDY   CHAR(1)         ;   /* REPORT FREQ MONTH/DAY/YEAR */
   v_11a_RPTID        NUMBER          ;   /* RPTID   */
   v_11b_RPTID        NUMBER          ;   /* RPTID   */
   v_11a_RPTNAME      VARCHAR2(100)   ;   /* RPTNAME  */
   v_11b_RPTNAME      VARCHAR2(100)   ;   /* RPTNAME  */
   v_11_EXPECTEDNEXTRUNDT    DATE    ;   /* EXPECTED NEXT RUN DATE */
   v_11_STATUS       VARCHAR2(20)    ;   /* STATUS OF THE TABLE BASED ON LAST POPULATED DATE COMPARED TO TODAY */
   v_11_LASTUPDTDT   DATE            ;   /* TABLR LAST UPDATED DATE */
   v_11_FACTOR       NUMBER          ;   /* FACTOR TO MULTIPLY RPTFREQNUM BASED ON RPTFREQMDY. IF YEAR, MULTIPLY BY 12 FOR MONTH*/
   v_11_RUNDAY       VARCHAR2(32)    ;   /* DAYS the TABLE is UPDATED; So Montor have to ALERT next day; Grace period 1 day */


  /* [TBLID-12: CDCONLINE.LATEFEEDISBTBL]  */
   v_12_TBLID        NUMBER          ;   /* TBLID    */
   v_12_TBLNAME      VARCHAR2(100)   ;   /* TBLNAME */
   v_12_RPTFREQNUM   NUMBER          ;   /* REPORT FREQ NUMBER VALUE */
   v_12_RPTFREQMDY   CHAR(1)         ;   /* REPORT FREQ MONTH/DAY/YEAR */
   v_12_RPTID        NUMBER          ;   /* RPTID   */
   v_12_RPTNAME      VARCHAR2(100)   ;   /* RPTNAME  */
   v_12_EXPECTEDNEXTRUNDT    DATE    ;   /* EXPECTED NEXT RUN DATE */
   v_12_STATUS       VARCHAR2(20)    ;   /* STATUS OF THE TABLE BASED ON LAST POPULATED DATE COMPARED TO TODAY */
   v_12_LASTUPDTDT   DATE            ;   /* TABLR LAST UPDATED DATE */
   v_12_FACTOR       NUMBER          ;   /* FACTOR TO MULTIPLY RPTFREQNUM BASED ON RPTFREQMDY. IF YEAR, MULTIPLY BY 12 FOR MONTH*/
   v_12_RUNDAY       VARCHAR2(32)    ;   /* DAYS the TABLE is UPDATED; So Montor have to ALERT next day; Grace period 1 day */


   /* [TBLID-13: CDCONLINE.LATESTMTTBL] */
   v_13_TBLID        NUMBER          ;   /* TBLID    */
   v_13_TBLNAME      VARCHAR2(100)   ;   /* TBLNAME */
   v_13_RPTFREQNUM   NUMBER          ;   /* REPORT FREQ NUMBER VALUE */
   v_13_RPTFREQMDY   CHAR(1)         ;   /* REPORT FREQ MONTH/DAY/YEAR */
   v_13_RPTID        NUMBER          ;   /* RPTID   */
   v_13_RPTNAME      VARCHAR2(100)   ;   /* RPTNAME  */
   v_13_EXPECTEDNEXTRUNDT    DATE    ;   /* EXPECTED NEXT RUN DATE */
   v_13_STATUS       VARCHAR2(20)    ;   /* STATUS OF THE TABLE BASED ON LAST POPULATED DATE COMPARED TO TODAY */
   v_13_LASTUPDTDT   DATE            ;   /* TABLR LAST UPDATED DATE */
   v_13_FACTOR       NUMBER          ;   /* FACTOR TO MULTIPLY RPTFREQNUM BASED ON RPTFREQMDY. IF YEAR, MULTIPLY BY 12 FOR MONTH*/
   v_13_RUNDAY       VARCHAR2(32)    ;   /* DAYS the TABLE is UPDATED; So Montor have to ALERT next day; Grace period 1 day */


   /*  [TBLID-15	CDCONLINE.OUTGNTYTBL	]    */
   v_15_TBLID        NUMBER          ;   /* TBLID    */
   v_15_TBLNAME      VARCHAR2(100)   ;   /* TBLNAME */
   v_15_RPTFREQNUM   NUMBER          ;   /* REPORT FREQ NUMBER VALUE */
   v_15_RPTFREQMDY   CHAR(1)         ;   /* REPORT FREQ MONTH/DAY/YEAR */
   v_15_RPTID        NUMBER          ;   /* RPTID   */
   v_15_RPTNAME      VARCHAR2(100)   ;   /* RPTNAME  */
   v_15_EXPECTEDNEXTRUNDT    DATE    ;   /* EXPECTED NEXT RUN DATE */
   v_15_STATUS       VARCHAR2(20)    ;   /* STATUS OF THE TABLE BASED ON LAST POPULATED DATE COMPARED TO TODAY */
   v_15_LASTUPDTDT   DATE            ;   /* TABLR LAST UPDATED DATE */
   v_15_FACTOR       NUMBER          ;   /* FACTOR TO MULTIPLY RPTFREQNUM BASED ON RPTFREQMDY. IF YEAR, MULTIPLY BY 12 FOR MONTH*/
   v_15_RUNDAY       VARCHAR2(32)    ;   /* DAYS the TABLE is UPDATED; So Montor have to ALERT next day; Grace period 1 day */

    /*  [TBLID-16	CDCONLINE.PAYOUTFLTSTMTTBL	]    */
   v_16_TBLID        NUMBER          ;   /* TBLID    */
   v_16_TBLNAME      VARCHAR2(100)   ;   /* TBLNAME */
   v_16_RPTFREQNUM   NUMBER          ;   /* REPORT FREQ NUMBER VALUE */
   v_16_RPTFREQMDY   CHAR(1)         ;   /* REPORT FREQ MONTH/DAY/YEAR */
   v_16_RPTID        NUMBER          ;   /* RPTID   */
   v_16_RPTNAME      VARCHAR2(100)   ;   /* RPTNAME  */
   v_16_EXPECTEDNEXTRUNDT    DATE    ;   /* EXPECTED NEXT RUN DATE */
   v_16_STATUS       VARCHAR2(20)    ;   /* STATUS OF THE TABLE BASED ON LAST POPULATED DATE COMPARED TO TODAY */
   v_16_LASTUPDTDT   DATE            ;   /* TABLR LAST UPDATED DATE */
   v_16_FACTOR       NUMBER          ;   /* FACTOR TO MULTIPLY RPTFREQNUM BASED ON RPTFREQMDY. IF YEAR, MULTIPLY BY 12 FOR MONTH*/
   v_16_RUNDAY       VARCHAR2(32)    ;   /* DAYS the TABLE is UPDATED; So Montor have to ALERT next day; Grace period 1 day */


   /*  [TBLID-17	CDCONLINE.PAYOUTSTMTTBL	]    */
   v_17_TBLID        NUMBER          ;   /* TBLID    */
   v_17_TBLNAME      VARCHAR2(100)   ;   /* TBLNAME */
   v_17_RPTFREQNUM   NUMBER          ;   /* REPORT FREQ NUMBER VALUE */
   v_17_RPTFREQMDY   CHAR(1)         ;   /* REPORT FREQ MONTH/DAY/YEAR */
   v_17_RPTID        NUMBER          ;   /* RPTID   */
   v_17_RPTNAME      VARCHAR2(100)   ;   /* RPTNAME  */
   v_17_EXPECTEDNEXTRUNDT    DATE    ;   /* EXPECTED NEXT RUN DATE */
   v_17_STATUS       VARCHAR2(20)    ;   /* STATUS OF THE TABLE BASED ON LAST POPULATED DATE COMPARED TO TODAY */
   v_17_LASTUPDTDT   DATE            ;   /* TABLR LAST UPDATED DATE */
   v_17_FACTOR       NUMBER          ;   /* FACTOR TO MULTIPLY RPTFREQNUM BASED ON RPTFREQMDY. IF YEAR, MULTIPLY BY 12 FOR MONTH*/
   v_17_RUNDAY       VARCHAR2(32)    ;   /* DAYS the TABLE is UPDATED; So Montor have to ALERT next day; Grace period 1 day */

BEGIN
      /*  CHECK for RUNID for v_run_date Passed to PROC  */
      BEGIN
            SELECT  RUNID INTO  v_run_id 
            FROM    CDCONLINE.SHRUNHEALTHCHK
            WHERE   RUNDATE = v_run_date;
        EXCEPTION
          WHEN  NO_DATA_FOUND THEN
                v_run_id := 0;
      END;

    /* NO RUNID Exists. So run the calculations and Create the RunData  */
    IF v_run_id = 0
    THEN
        BEGIN
            /* START v_run_id = 0; Not Existing. Process the report for the day */

            /* * QUERY through CDCONLINE.SHTABLETBL items and perform calculations */

            /* ################################### START CALCULATIONS #########################################  */
            /*  [TBLID-4: CDCONLINE.ANNSTMTLOANINFOTBL] [RPTID-24: View Annual Statement]  */
            SELECT  t.TBLID, t.TBLNAME, t.RPTFREQNUM, t.RPTFREQMDY, j.RPTID , r.RPTNAME
            INTO    v_4_TBLID, v_4_TBLNAME, v_4_RPTFREQNUM, v_4_RPTFREQMDY, v_4_RPTID, v_4_RPTNAME
            FROM    CDCONLINE.SHTABLETBL t, CDCONLINE.SHJOINRPTTABTBL j, CDCONLINE.SHREPORTSTBL r
            WHERE   t.ISACTIVE='Y'
                    AND t.TBLID=4
                    AND j.TBLID=t.TBLID
                    AND j.RPTID=r.RPTID
                    AND r.ISACTIVE='Y' ;

            /* ANNUAL FREQ - CONVERT TO MONTH */
           IF v_4_RPTFREQMDY = 'Y' THEN
                v_4_FACTOR := 12;
           ELSE 
                v_4_FACTOR := 1;
           END IF;

            /* CALCULATE THE STATUS VALUE and DETERMINE the Reports affected in CDCONLINE.SHREPORTSTBL */
            SELECT  EXPECTEDNEXTRUNDT, STATUS, ADD_MONTHS( EXPECTEDNEXTRUNDT, - v_4_RPTFREQNUM * v_4_FACTOR) as LASTUPDTDT
            INTO    v_4_EXPECTEDNEXTRUNDT, v_4_STATUS, v_4_LASTUPDTDT
            FROM (
                SELECT  ADD_MONTHS( MAX(DISTINCT LASTUPDTDT), v_4_RPTFREQNUM * v_4_FACTOR) AS EXPECTEDNEXTRUNDT
                        ,(ADD_MONTHS( MAX(DISTINCT LASTUPDTDT), v_4_RPTFREQNUM * v_4_FACTOR) - v_run_date) AS NUMDAYS
                        ,CASE WHEN (ADD_MONTHS( MAX(DISTINCT LASTUPDTDT), v_4_RPTFREQNUM * v_4_FACTOR) - v_run_date) > 0
                            THEN 'NORMAL'
                            ELSE 'ELAPSED'
                            END AS STATUS
                FROM CDCONLINE.ANNSTMTLOANINFOTBL
                ORDER BY LASTUPDTDT DESC
            ) 
            WHERE ROWNUM <= 1;


           /* ##############################################################################################  */
            /*  START:: [TBLID-8: CDCONLINE.CNFRMCOPYTBL] [RPTID-13: Funding > Conformed Copy ] 31	D */

                SELECT  t.TBLID     , t.TBLNAME     , t.RPTFREQNUM      , t.RPTFREQMDY      , j.RPTID   , r.RPTNAME     , t.FREQRUNDAY
                INTO    v_8_TBLID   , v_8_TBLNAME   , v_8_RPTFREQNUM    , v_8_RPTFREQMDY    , v_8_RPTID , v_8_RPTNAME   , v_8_RUNDAY
                FROM    CDCONLINE.SHTABLETBL t, CDCONLINE.SHJOINRPTTABTBL j, CDCONLINE.SHREPORTSTBL r
                WHERE   t.ISACTIVE='Y'
                        AND t.TBLID=8
                        AND j.TBLID=t.TBLID
                        AND j.RPTID=r.RPTID
                        AND r.ISACTIVE='Y' ; 

                SELECT   MAX(DISTINCT LASTUPDTDT) + v_8_RPTFREQNUM AS EXPECTEDNEXTRUNDT,
                        CASE WHEN (v_run_date - MAX(DISTINCT LASTUPDTDT)) < v_8_RPTFREQNUM+1 THEN 'NORMAL'  ELSE 'ELAPSED'  END AS STATUS,
                        MAX(DISTINCT LASTUPDTDT) as LASTUPDTDT                    
                INTO    v_8_EXPECTEDNEXTRUNDT, v_8_STATUS, v_8_LASTUPDTDT
                FROM    CDCONLINE.LATEFEEDISBTBL
                ORDER BY LASTUPDTDT DESC; 

            -- CDCONLINE.FUNDINGSTMTTBL [11] > Funding > Funding Statements [14] & SBA Reports > Funding [20]
           /* ##############################################################################################  */

            /*  START:: [TBLID-11: CDCONLINE.FUNDINGSTMTTBL] [RPTID-14: Funding > Funding Statements ] 31	D */
                SELECT  t.TBLID     , t.TBLNAME     , t.RPTFREQNUM      , t.RPTFREQMDY      , j.RPTID   , r.RPTNAME     , t.FREQRUNDAY
                INTO    v_11_TBLID  , v_11_TBLNAME  , v_11_RPTFREQNUM   , v_11_RPTFREQMDY   , v_11a_RPTID, v_11a_RPTNAME  , v_11_RUNDAY
                FROM    CDCONLINE.SHTABLETBL t, CDCONLINE.SHJOINRPTTABTBL j, CDCONLINE.SHREPORTSTBL r
                WHERE   t.ISACTIVE='Y'
                        AND t.TBLID=11
                        AND j.RPTID=14
                        AND j.TBLID=t.TBLID
                        AND j.RPTID=r.RPTID
                        AND r.ISACTIVE='Y' ;  

                 /*  START:: [TBLID-11: CDCONLINE.FUNDINGSTMTTBL] [RPTID-20: SBA Reports > Funding ] 31	D */      
                SELECT  t.TBLID     , t.TBLNAME     , t.RPTFREQNUM      , t.RPTFREQMDY      , j.RPTID   , r.RPTNAME     , t.FREQRUNDAY
                INTO    v_11_TBLID  , v_11_TBLNAME  , v_11_RPTFREQNUM   , v_11_RPTFREQMDY   , v_11b_RPTID, v_11b_RPTNAME  , v_11_RUNDAY
                FROM    CDCONLINE.SHTABLETBL t, CDCONLINE.SHJOINRPTTABTBL j, CDCONLINE.SHREPORTSTBL r
                WHERE   t.ISACTIVE='Y'
                        AND t.TBLID=11
                        AND j.RPTID=20
                        AND j.TBLID=t.TBLID
                        AND j.RPTID=r.RPTID
                        AND r.ISACTIVE='Y' ; 

                /* Grace Period Added : 5 days; v_11_RPTFREQNUM+5 */
                SELECT   MAX(DISTINCT LASTUPDTDT) + v_11_RPTFREQNUM AS EXPECTEDNEXTRUNDT,
                        CASE WHEN (v_run_date - MAX(DISTINCT LASTUPDTDT)) < v_11_RPTFREQNUM+5 THEN 'NORMAL'  ELSE 'ELAPSED'  END AS STATUS,
                        MAX(DISTINCT LASTUPDTDT) as LASTUPDTDT                    
                INTO    v_11_EXPECTEDNEXTRUNDT, v_11_STATUS, v_11_LASTUPDTDT
                FROM    CDCONLINE.FUNDINGSTMTTBL
                ORDER BY LASTUPDTDT DESC;  





            /* ##############################################################################################  */
            /*  START:: [TBLID-12: CDCONLINE.LATEFEEDISBTBL] [RPTID-18: Other Reports > Late Fee Disbursed] 31	D */
                SELECT  t.TBLID     , t.TBLNAME     , t.RPTFREQNUM      , t.RPTFREQMDY      , j.RPTID   , r.RPTNAME     , t.FREQRUNDAY
                INTO    v_12_TBLID  , v_12_TBLNAME  , v_12_RPTFREQNUM   , v_12_RPTFREQMDY   , v_12_RPTID, v_12_RPTNAME  , v_12_RUNDAY
                FROM    CDCONLINE.SHTABLETBL t, CDCONLINE.SHJOINRPTTABTBL j, CDCONLINE.SHREPORTSTBL r
                WHERE   t.ISACTIVE='Y'
                        AND t.TBLID=12
                        AND j.TBLID=t.TBLID
                        AND j.RPTID=r.RPTID
                        AND r.ISACTIVE='Y' ;  

                SELECT   MAX(DISTINCT LASTUPDTDT) + v_12_RPTFREQNUM AS EXPECTEDNEXTRUNDT,
                        CASE WHEN (v_run_date - MAX(DISTINCT LASTUPDTDT)) < v_12_RPTFREQNUM+1 THEN 'NORMAL'  ELSE 'ELAPSED'  END AS STATUS,
                        MAX(DISTINCT LASTUPDTDT) as LASTUPDTDT                    
                INTO    v_12_EXPECTEDNEXTRUNDT, v_12_STATUS, v_12_LASTUPDTDT
                FROM    CDCONLINE.LATEFEEDISBTBL
                ORDER BY LASTUPDTDT DESC;  



             /* ##############################################################################################  */
            /*  START:: [TBLID-13: CDCONLINE.LATESTMTTBL] [RPTID-17: Other Reports > Late Fee Assessed] 31	D */
                SELECT  t.TBLID     , t.TBLNAME     , t.RPTFREQNUM      , t.RPTFREQMDY      , j.RPTID   , r.RPTNAME     , t.FREQRUNDAY
                INTO    v_13_TBLID  , v_13_TBLNAME  , v_13_RPTFREQNUM   , v_13_RPTFREQMDY   , v_13_RPTID, v_13_RPTNAME  , v_13_RUNDAY
                FROM    CDCONLINE.SHTABLETBL t, CDCONLINE.SHJOINRPTTABTBL j, CDCONLINE.SHREPORTSTBL r
                WHERE   t.ISACTIVE='Y'
                        AND t.TBLID=13
                        AND j.TBLID=t.TBLID
                        AND j.RPTID=r.RPTID
                        AND r.ISACTIVE='Y' ;  

                SELECT   MAX(DISTINCT LASTUPDTDT) + v_13_RPTFREQNUM AS EXPECTEDNEXTRUNDT,
                        CASE WHEN (v_run_date - MAX(DISTINCT LASTUPDTDT)) < v_13_RPTFREQNUM+1 THEN 'NORMAL'  ELSE 'ELAPSED'  END AS STATUS,
                        MAX(DISTINCT LASTUPDTDT) as LASTUPDTDT                    
                INTO    v_13_EXPECTEDNEXTRUNDT, v_13_STATUS, v_13_LASTUPDTDT
                FROM    CDCONLINE.LATEFEEDISBTBL
                ORDER BY LASTUPDTDT DESC;  

            /* ##############################################################################################  */
            /*  START:: [TBLID-15: CDCONLINE.OUTGNTYTBL] [RPTID-21: SBA Reports > Outstanding Guarantee] 30	D */

                SELECT  t.TBLID     , t.TBLNAME     , t.RPTFREQNUM      , t.RPTFREQMDY      , j.RPTID   , r.RPTNAME     , t.FREQRUNDAY
                INTO    v_15_TBLID  , v_15_TBLNAME  , v_15_RPTFREQNUM   , v_15_RPTFREQMDY   , v_15_RPTID, v_15_RPTNAME  , v_15_RUNDAY
                FROM    CDCONLINE.SHTABLETBL t, CDCONLINE.SHJOINRPTTABTBL j, CDCONLINE.SHREPORTSTBL r
                WHERE   t.ISACTIVE='Y'
                        AND t.TBLID=15
                        AND j.TBLID=t.TBLID
                        AND j.RPTID=r.RPTID
                        AND r.ISACTIVE='Y' ;  

                SELECT   MAX(DISTINCT LASTUPDTDT) + v_15_RPTFREQNUM AS EXPECTEDNEXTRUNDT,
                        CASE WHEN (v_run_date - MAX(DISTINCT LASTUPDTDT)) < v_15_RPTFREQNUM+1 THEN 'NORMAL'  ELSE 'ELAPSED'  END AS STATUS,
                        MAX(DISTINCT LASTUPDTDT) as LASTUPDTDT                    
                INTO    v_15_EXPECTEDNEXTRUNDT, v_15_STATUS, v_15_LASTUPDTDT
                FROM    CDCONLINE.OUTGNTYTBL
                ORDER BY LASTUPDTDT DESC;   


             /* ##############################################################################################  */
            /*  START:: [TBLID-16: CDCONLINE.PAYOUTFLTSTMTTBL] [RPTID-16: Other Reports > Float Report] */

            SELECT  t.TBLID, t.TBLNAME, t.RPTFREQNUM, t.RPTFREQMDY, j.RPTID , r.RPTNAME, t.FREQRUNDAY
            INTO    v_16_TBLID, v_16_TBLNAME, v_16_RPTFREQNUM, v_16_RPTFREQMDY, v_16_RPTID, v_16_RPTNAME, v_16_RUNDAY
            FROM    CDCONLINE.SHTABLETBL t, CDCONLINE.SHJOINRPTTABTBL j, CDCONLINE.SHREPORTSTBL r
            WHERE   t.ISACTIVE='Y'
                    AND t.TBLID=16
                    AND j.TBLID=t.TBLID
                    AND j.RPTID=r.RPTID
                    AND r.ISACTIVE='Y' ;                    

            SELECT   MAX(DISTINCT LASTUPDTDT) + v_16_RPTFREQNUM AS EXPECTEDNEXTRUNDT,
                    CASE WHEN (v_run_date - MAX(DISTINCT LASTUPDTDT)) < v_16_RPTFREQNUM+5 THEN 'NORMAL'  ELSE 'ELAPSED'  END AS STATUS,
                    MAX(DISTINCT LASTUPDTDT) as LASTUPDTDT                    
            INTO    v_16_EXPECTEDNEXTRUNDT, v_16_STATUS, v_16_LASTUPDTDT
            FROM    CDCONLINE.PAYOUTSTMTTBL
            ORDER BY LASTUPDTDT DESC;    

            /* ##############################################################################################  */

             /*  START:: [TBLID-17: CDCONLINE.PAYOUTSTMTTBL] [RPTID-10: CDCONLINE.PAYOUTSTMTTBL] Query for TBLNAME/RPTFREQNUM/RPTFREQMDY/RPTNAME and assign to Variables; TABLE populated Weekly on FRI; RAISE Alert on SAT  */ 

            SELECT  t.TBLID, t.TBLNAME, t.RPTFREQNUM, t.RPTFREQMDY, j.RPTID , r.RPTNAME, t.FREQRUNDAY
            INTO    v_17_TBLID, v_17_TBLNAME, v_17_RPTFREQNUM, v_17_RPTFREQMDY, v_17_RPTID, v_17_RPTNAME, v_17_RUNDAY
            FROM    CDCONLINE.SHTABLETBL t, CDCONLINE.SHJOINRPTTABTBL j, CDCONLINE.SHREPORTSTBL r
            WHERE   t.ISACTIVE='Y'
                    AND t.TBLID=17
                    AND j.TBLID=t.TBLID
                    AND j.RPTID=r.RPTID
                    AND r.ISACTIVE='Y' ;

            SELECT   MAX(DISTINCT LASTUPDTDT) + v_17_RPTFREQNUM AS EXPECTEDNEXTRUNDT,
                    CASE WHEN (v_run_date - MAX(DISTINCT LASTUPDTDT)) < v_17_RPTFREQNUM+1 THEN 'NORMAL'  ELSE 'ELAPSED'  END AS STATUS,
                    MAX(DISTINCT LASTUPDTDT) as LASTUPDTDT                    
            INTO    v_17_EXPECTEDNEXTRUNDT, v_17_STATUS, v_17_LASTUPDTDT
            FROM    CDCONLINE.PAYOUTSTMTTBL
            ORDER BY LASTUPDTDT DESC;            
            /* ##############################################################################################  */

            /* ** MODIFY TO INCLUDE IN JSON OUTPUT ** CONSTRUCT JSON string and populate the CDCONLINE.SHRUNHEALTHCHK Table  */
            BEGIN

                SELECT JSON_ARRAYAGG (
                            JSON_OBJECT(
                        'RPTID' is r.RPTID,                     
                        'RPTSTATUS' is  CASE 
                                        WHEN r.RPTID=v_4_RPTID  THEN  v_4_STATUS  
                                        WHEN r.RPTID=v_8_RPTID  THEN  v_8_STATUS 
                                        WHEN r.RPTID=v_11a_RPTID THEN  v_11_STATUS 
                                        WHEN r.RPTID=v_11b_RPTID THEN  v_11_STATUS 
                                        WHEN r.RPTID=v_12_RPTID THEN  v_12_STATUS 
                                        WHEN r.RPTID=v_13_RPTID THEN  v_13_STATUS 
                                        WHEN r.RPTID=v_15_RPTID THEN  v_15_STATUS  
                                        WHEN r.RPTID=v_16_RPTID THEN  v_16_STATUS  
                                        WHEN r.RPTID=v_17_RPTID THEN  v_17_STATUS  
                                        ELSE  'IN PROGRESS'  END,
                        'LASTRUNDT' is  CASE 
                                        WHEN r.RPTID=v_4_RPTID  THEN  v_4_LASTUPDTDT 
                                        WHEN r.RPTID=v_8_RPTID  THEN  v_8_LASTUPDTDT
                                        WHEN r.RPTID=v_11a_RPTID THEN  v_11_LASTUPDTDT
                                        WHEN r.RPTID=v_11b_RPTID THEN  v_11_LASTUPDTDT
                                        WHEN r.RPTID=v_12_RPTID THEN  v_12_LASTUPDTDT
                                        WHEN r.RPTID=v_13_RPTID THEN  v_13_LASTUPDTDT
                                        WHEN r.RPTID=v_15_RPTID THEN  v_15_LASTUPDTDT
                                        WHEN r.RPTID=v_16_RPTID THEN  v_16_LASTUPDTDT
                                        WHEN r.RPTID=v_17_RPTID THEN  v_17_LASTUPDTDT
                                        ELSE  v_dummy_date  END,
                        'EXPECTEDRUNDT' is CASE 
                                        WHEN r.RPTID=v_4_RPTID  THEN  v_4_EXPECTEDNEXTRUNDT 
                                        WHEN r.RPTID=v_8_RPTID  THEN  v_8_EXPECTEDNEXTRUNDT 
                                        WHEN r.RPTID=v_11a_RPTID THEN  v_11_EXPECTEDNEXTRUNDT
                                        WHEN r.RPTID=v_11b_RPTID THEN  v_11_EXPECTEDNEXTRUNDT
                                        WHEN r.RPTID=v_12_RPTID THEN  v_12_EXPECTEDNEXTRUNDT 
                                        WHEN r.RPTID=v_13_RPTID THEN  v_13_EXPECTEDNEXTRUNDT
                                        WHEN r.RPTID=v_15_RPTID THEN  v_15_EXPECTEDNEXTRUNDT 
                                        WHEN r.RPTID=v_16_RPTID THEN  v_16_EXPECTEDNEXTRUNDT  
                                        WHEN r.RPTID=v_17_RPTID THEN  v_17_EXPECTEDNEXTRUNDT  
                                        ELSE  v_dummy_date  END,
                        'RELATEDTBL' is CASE 
                                        WHEN r.RPTID=v_4_RPTID  THEN  v_4_TBLID 
                                        WHEN r.RPTID=v_8_RPTID  THEN  v_8_TBLID
                                        WHEN r.RPTID=v_11a_RPTID THEN  v_11_TBLID 
                                        WHEN r.RPTID=v_11b_RPTID THEN  v_11_TBLID 
                                        WHEN r.RPTID=v_12_RPTID THEN  v_12_TBLID 
                                        WHEN r.RPTID=v_13_RPTID THEN  v_13_TBLID 
                                        WHEN r.RPTID=v_15_RPTID THEN  v_15_TBLID 
                                        WHEN r.RPTID=v_16_RPTID THEN  v_16_TBLID  
                                        WHEN r.RPTID=v_17_RPTID THEN  v_17_TBLID  
                                        ELSE  v_dummy_TBLID  END
                                ) 
                            ) 
                INTO    v_json_str
                FROM    CDCONLINE.SHREPORTSTBL r
                WHERE   r.ISACTIVE='Y';

            END;


            BEGIN
                /* GET the Next RUNID  */
                SELECT NVL(max(RUNID),0)+1 INTO v_next_runid FROM CDCONLINE.SHRUNHEALTHCHK;

                /* POPULATE THE HEALTHCHECK RECORD */
                INSERT INTO CDCONLINE.SHRUNHEALTHCHK (RUNID, RUNDATE, ISRUNCOMPLETE, RUNCONTENT, LASTUPDTDT , CREATDT )
                VALUES  (v_next_runid, v_run_date, 'Y', v_json_str, sysdate, sysdate);

                COMMIT;
            END;


            /* GET the RunId in to variable v_run_id */
             BEGIN
                    SELECT  RUNID INTO  v_run_id 
                    FROM    CDCONLINE.SHRUNHEALTHCHK
                    WHERE   RUNDATE = v_run_date;
                EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                    v_run_id := 0;
              END;

            /* END v_run_id = 0 */
        END;   
    END IF;


    /* POPULATE OUTPUT CURSOR with output */
    OPEN  p_SelCur1 FOR        
        SELECT 	RUNID, RUNDATE, ISRUNCOMPLETE, RUNCONTENT, LASTUPDTDT , CREATDT 
        FROM 	CDCONLINE.SHRUNHEALTHCHK
        WHERE 	RUNID=v_run_id ;


EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
END;
/

GRANT EXECUTE ON CDCONLINE.SYSHEALTHCHKCSP TO CDCONLINECORPGOVPRTUPDATE;
GRANT EXECUTE ON CDCONLINE.SYSHEALTHCHKCSP TO CDCONLINEDEVROLE;
GRANT EXECUTE ON CDCONLINE.SYSHEALTHCHKCSP TO CDCONLINEREADALLROLE;
GRANT EXECUTE ON CDCONLINE.SYSHEALTHCHKCSP TO CSAUPDTROLE;


COMMIT;
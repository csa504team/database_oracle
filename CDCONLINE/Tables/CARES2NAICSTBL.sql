CREATE TABLE CDCONLINE.CARES2NAICSTBL
(
  LOAN_NUMBER          VARCHAR2(10 BYTE),
  PROGRAM_CODE         VARCHAR2(5 BYTE),
  PROCESS_METHOD_CODE  VARCHAR2(5 BYTE),
  STATUS_CODE          VARCHAR2(5 BYTE),
  NAICS_CODE           VARCHAR2(10 BYTE),
  CDCREGNCD            VARCHAR2(5 BYTE),
  CDCNMB               VARCHAR2(5 BYTE),
  PROCESSED_DATE       DATE,
  CREATEUSERID         VARCHAR2(32 BYTE),
  CREATDT              DATE                     DEFAULT sysdate,
  LASTUPDTUSERID       VARCHAR2(32 BYTE),
  LASTUPDTDT           DATE                     DEFAULT sysdate,
  APPROVAL_DATE        DATE
)
TABLESPACE CDCONLINEDATATBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
MONITORING;


ALTER TABLE CDCONLINE.CARES2NAICSTBL ADD (
  CONSTRAINT SYS_C00210838
  CHECK ("LOAN_NUMBER" IS NOT NULL)
  ENABLE VALIDATE);

GRANT SELECT ON CDCONLINE.CARES2NAICSTBL TO CDCONLINEREADALLROLE;

CREATE TABLE cdconline.rptschdlpodollrtbl
(
	loannmb CHAR(10 CHAR) NOT NULL
	, prepaydt DATE NOT NULL
	, borrnm VARCHAR2(80 BYTE)
	, cdcregncd CHAR(2 BYTE) NOT NULL 
	, cdcnmb CHAR(4 BYTE) NOT NULL 
	, wkprepayamt NUMBER(19, 4)
	, isarchived CHAR(1 CHAR) DEFAULT 'N'
	, creatuserid VARCHAR2(32 BYTE) NOT NULL 
	, creatdt DATE DEFAULT SYSDATE NOT NULL 
	, lastupdtuserid VARCHAR2(32 BYTE) NOT NULL 
	, lastupdtdt DATE DEFAULT SYSDATE NOT NULL
);


GRANT SELECT ON cdconline.rptschdlpodollrtbl TO cdconlinereadallrole;
GRANT SELECT ON cdconline.rptschdlpodollrtbl TO cdconlineprtread;
GRANT SELECT ON cdconline.rptschdlpodollrtbl TO cdconlinegovread;
GRANT DELETE, INSERT, SELECT, UPDATE ON CDCONLINE.rptschdlpodollrtbl TO CDCONLINEDEVROLE;
create or replace PROCEDURE cdconline.HistSaveInsCSP
(
  p_req_ref IN NUMBER,
  p_sba_loan_no IN CHAR,
  p_posting_date IN DATE,
  p_prepay_date IN DATE
)
AS
   /***get next semi annual Bal from debenture file ***/
   v_sa_date DATE;

BEGIN

   INSERT INTO LoanSaveTbl(RqstREF,PrgrmNmb,UserLevelRoleID,CDCRegnCd,CDCNmb,LoanNmb,LoanStatCd,NotePrinAmt,NoteRtPct,IssDt,MatDt,BorrNm,BorrMailAdrStr1Nm,BorrMailAdrStr2Nm,BorrMailAdrCtyNm,BorrMailAdrStCd,BorrMailAdrZipCd,BorrMailAdrZip4Cd,SmllBusCONS,StmtNm,ACHRoutingNmb,ACHAcct,LateFeeStatCd,PrinLeftAmt,AmortDtx,AmortPymtAmt,AmortIntAmt,AmortPrinAmt,AmortCDCAmt,AmortSBAAmt,AmortCSAAmt,CDCPct,SBAPct,NoteBalAmt,FeePaidDtx,IntDtx,CurLoanDtx,CurBalNeededAmt,PrinNeededAmt,IntNeededAmt,CDCFeeNeededAmt,SBAFeeNeededAmt,CSAFeeNeededAmt,LateFeeNeededAmt,DueFromBorrNeededAmt,CDCNm,LoanAmt,MoPymtAmt,SemiAnnPymtAmt,PrepayRtPct,PrepayPremAmt,DbentrBalAmt,WKPrepayAmt, CreatUserID, LastUpdtUserID)
     ( SELECT p_req_ref RqstREF  ,
              l.PrgrmNmb ,
              l.UserLevelRoleID ,
              l.CDCRegnCd ,
              l.CDCNmb ,
              l.LoanNmb ,
              l.LoanStatCd ,
              l.NotePrinAmt ,
              l.NoteRtPct ,
              l.IssDt ,
              l.MatDt ,
              l.BorrNm ,
              l.BorrMailAdrStr1Nm ,
  l.BorrMailAdrStr2Nm ,
              l.BorrMailAdrCtyNm ,
              l.BorrMailAdrStCd ,
              l.BorrMailAdrZipCd ,
  l.BorrMailAdrZip4Cd ,
              l.SmllBusCons ,
              l.StmtNm ,
              l.ACHRoutingNmb ,
              l.ACHAcct ,
              l.LateFeeStatCd ,
              l.PrinLeftAmt ,
              l.AmortDtx ,
              l.AmortPymtAmt ,
              l.AmortIntAmt ,
              l.AmortPrinAmt ,
              l.AmortCDCAmt ,
              l.AmortSBAAmt ,
              l.AmortCSAAmt ,
              l.CDCPct ,
              l.SBAPct ,
              l.NoteBalAmt ,
              l.FeePaidDtx ,
              l.IntDtx ,
              l.CurLoanDtx ,
              l.CurBalNeededAmt ,
              l.PrinNeededAmt ,
              l.IntNeededAmt ,
              l.CDCFeeNeededAmt ,
              l.SBAFeeNeededAmt ,
              l.CSAFeeNeededAmt ,
              l.LateFeeNeededAmt ,
              l.DueFromBorrNeededAmt ,
              p.CDCNm ,
              p.LoanAmt ,
              p.MoPymtAmt ,
              NULL ,--d.SemiAnnAmt,

              NULL ,--d.PrepayRt,

              NULL ,--d.PremAmt,

              NULL ,--d.Bal,

              0.00 WKPrepayAmt  ,
  USER ,
  USER
       FROM LoanTbl l
              LEFT JOIN PortflTbl p   ON l.LoanNmb = p.LoanNmb

       ---left outer join DbentrTbl d

       ---on l.LoanNmb=d.LoanNmb
       WHERE  l.LoanNmb = p_sba_loan_no );
   SELECT MIN(SemiAnnDt)

     INTO v_sa_date
     FROM DbentrTbl
    WHERE  LoanNmb = p_sba_loan_no
             AND MONTHS_BETWEEN(LAST_DAY(SemiAnnDt), LAST_DAY(p_prepay_date)) > 0
     GROUP BY LoanNmb;
   IF v_sa_date IS NOT NULL THEN

   BEGIN
      MERGE INTO LoanSaveTbl
      USING (SELECT LoanSaveTbl.ROWID row_id, D.SemiAnnAmt, D.PrepayRtPct, D.PremAmt, D.BalAmt
      FROM LoanSaveTbl ,DbentrTbl D
       WHERE RqstREF = p_req_ref
        AND D.LoanNmb = p_sba_loan_no
        AND TRUNC(v_sa_date) - TRUNC(D.SemiAnnDt) = 0) src
      ON ( LoanSaveTbl.ROWID = src.row_id )
      WHEN MATCHED THEN UPDATE SET SemiAnnPymtAmt = src.SemiAnnAmt,
                                   PrepayRtPct = src.PrepayRtPct,
                                   PrepayPremAmt = src.PremAmt,
                                   DbentrBalAmt = src.BalAmt;

   END;
   END IF;
   /*********/
   INSERT INTO PymtSaveTbl
     ( SELECT p_req_ref RqstREF  ,
              p.*
       FROM PymtHistryTbl p
        WHERE  p.LoanNmb = p_sba_loan_no
        /* HC: Needs fix due to DATE to CHAR conversion; ZM: Fixed to CHAR(10) */
                 AND p.PostingDt = p_posting_date );

--  2019-07-19 ZM Undoing the "AND NOT EXISTS (SELECT 1 FROM CDCONLINE.PYMTSAVETBL WHERE RQSTREF = p_req_ref AND TRIM(PYMTTYP) = 'X' AND LOANNMB = p_sba_loan_no)"
--  2019-06-13 ZM Updating this query so that it only updates the balance amount if the number of records does not contain an adjustment
--  2018-05-01 RAJ Set Correct current balance in PYMTSAVETBL to avoid adjustments causing discrepancies in CSA Prepayment validations
UPDATE CDCONLINE.PYMTSAVETBL
SET BALAMT = 
  (
    SELECT NOTEBALAMT
    FROM CDCONLINE.LOANTBL
    WHERE LOANNMB = p_sba_loan_no
  )
WHERE RQSTREF = p_req_ref;
-- 2018-05-01 RAJ End of update to current balance in PYMTSAVETBL

EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
END;

/
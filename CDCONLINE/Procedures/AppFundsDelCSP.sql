create or replace 
PROCEDURE            CDCONLINE.AppFundsDelCSP
(
  p_SOD IN CHAR DEFAULT NULL ,
  p_RegionNum IN CHAR DEFAULT NULL ,
  p_CDC_Cert IN CHAR DEFAULT NULL ,
  p_SBA IN NUMBER DEFAULT 0 ,
  p_SelCur1 OUT SYS_REFCURSOR
)
AS

BEGIN

   DELETE FROM CDCONLINE.TTTempAppFundsTbl;
   -- CDC_App_Funds_R null, '10','468 ',0
   IF p_SBA = 1 THEN
    INSERT INTO CDCONLINE.TTTempAppFundsTbl (UserLevelRoleID,DistNm,CDCRegnCd,CDCNmb,CDCNm,BorrNm,StmtNm,LoanNmb,PymtRecvDt,SBAFeeAmt,CSAFeeAmt,CDCFeeAmt,LateFeeAmt,IntApplAmt,PrinApplAmt,DiffToEscrowAmt,RefiLoan,CreatUserID,LastUpdtUserID)
     SELECT sp.UserLevelRoleID UserLevelRoleID  ,
            UPPER(sp.DistNm) DistNm  ,
            sp.CDCRegnCd CDCRegnCd  ,
            sp.CDCNmb CDCNmb  ,
            UPPER(sp.CDCNm) CDCNm  ,
            UPPER(sp.BorrNm) BorrNm  ,
            UPPER(sp.StmtNm) StmtNm  ,
            ap.LoanNmb LoanNmb  ,
            ap.PymtRecvDDtx PymtRecvDDtx  ,
            ap.SBAFeeAmt SBAFeeAmt  ,
            ap.CSAFeeAmt CSAFeeAmt  ,
            ap.CDCFeeAmt CDCFeeAmt  ,
            ap.LateFeeAmt LateFeeAmt  ,
            ap.IntApplAmt IntApplAmt  ,
            ap.PrinApplAmt PrinApplAmt  ,
            ap.DiffToEscrowAmt DiffToEscrowAmt  ,
            NULL RefiLoan  ,
			USER ,
			USER
       FROM PortflTbl sp
              JOIN FundsTbl ap   ON sp.LoanNmb = ap.LoanNmb
      WHERE  sp.CDCRegnCd = p_RegionNum
               AND sp.CDCNmb = p_CDC_Cert
       ORDER BY UPPER(sp.StmtNm);

   --select * from CDCPortflTbl
   ELSE
      IF LTRIM(RTRIM(p_SOD)) > 0 THEN
       INSERT INTO TTTempAppFundsTbl (UserLevelRoleID,DistNm,CDCRegnCd,CDCNmb,CDCNm,BorrNm,StmtNm,LoanNmb,PymtRecvDt,SBAFeeAmt,CSAFeeAmt,CDCFeeAmt,LateFeeAmt,IntApplAmt,PrinApplAmt,DiffToEscrowAmt,RefiLoan,CreatUserID,LastUpdtUserID)
        SELECT sp.UserLevelRoleID UserLevelRoleID  ,
               UPPER(sp.DistNm) DistNm  ,
               sp.CDCRegnCd CDCRegnCd  ,
               sp.CDCNmb CDCNmb  ,
               UPPER(sp.CDCNm) CDCNm  ,
               UPPER(sp.BorrNm) BorrNm  ,
               UPPER(sp.StmtNm) StmtNm  ,
               ap.LoanNmb LoanNmb  ,
               ap.PymtRecvDDtx PymtRecvDDtx  ,
               ap.SBAFeeAmt SBAFeeAmt  ,
               ap.CSAFeeAmt CSAFeeAmt  ,
               ap.CDCFeeAmt CDCFeeAmt  ,
               ap.LateFeeAmt LateFeeAmt  ,
               ap.IntApplAmt IntApplAmt  ,
               ap.PrinApplAmt PrinApplAmt  ,
               ap.DiffToEscrowAmt DiffToEscrowAmt  ,
               NULL RefiLoan  ,
			   USER ,
			   USER
          FROM PortflTbl sp
                 JOIN FundsTbl ap   ON sp.LoanNmb = ap.LoanNmb

        --WHERE		UserLevelRoleID = @SOD
        WHERE  sp.CDCRegnCd = p_RegionNum
                 AND sp.CDCNmb = p_CDC_Cert
                 AND SUBSTR(sp.UserLevelRoleID, 1, 2) = SUBSTR(p_SOD, 1, 2)
          ORDER BY UPPER(sp.StmtNm);
      ELSE
         INSERT INTO CDCONLINE.TTTempAppFundsTbl (UserLevelRoleID,DistNm,CDCRegnCd,CDCNmb,CDCNm,BorrNm,StmtNm,LoanNmb,PymtRecvDt,SBAFeeAmt,CSAFeeAmt,CDCFeeAmt,LateFeeAmt,IntApplAmt,PrinApplAmt,DiffToEscrowAmt,RefiLoan,CreatUserID,LastUpdtUserID)
           SELECT sp.UserLevelRoleID UserLevelRoleID  ,
                  UPPER(sp.DistNm) DistNm  ,
                  sp.CDCRegnCd CDCRegnCd  ,
                  sp.CDCNmb CDCNmb  ,
                  UPPER(sp.CDCNm) CDCNm  ,
                  UPPER(sp.BorrNm) BorrNm  ,
                  UPPER(sp.StmtNm) StmtNm  ,
                  ap.LoanNmb LoanNmb  ,
                  ap.PymtRecvDDtx PymtRecvDDtx  ,
                  ap.SBAFeeAmt SBAFeeAmt  ,
                  ap.CSAFeeAmt CSAFeeAmt  ,
                  ap.CDCFeeAmt CDCFeeAmt  ,
                  ap.LateFeeAmt LateFeeAmt  ,
                  ap.IntApplAmt IntApplAmt  ,
                  ap.PrinApplAmt PrinApplAmt  ,
                  ap.DiffToEscrowAmt DiffToEscrowAmt  ,
                  NULL RefiLoan  ,
				  USER ,
				  USER
             FROM CDCONLINE.PortflTbl sp
                    JOIN FundsTbl ap   ON sp.LoanNmb = ap.LoanNmb
            WHERE  sp.CDCRegnCd = p_RegionNum
                     AND sp.CDCNmb = p_CDC_Cert
             ORDER BY UPPER(sp.StmtNm);
      END IF;
   END IF;
   MERGE INTO CDCONLINE.TTTempAppFundsTbl p
   USING (SELECT p.ROWID row_id, ln.RefiLoan
   FROM CDCONLINE.TTTempAppFundsTbl p
          JOIN LoanTbl ln   ON p.LoanNmb = ln.LoanNmb ) src
   ON ( p.ROWID = src.row_id )
   WHEN MATCHED THEN UPDATE SET RefiLoan = src.RefiLoan;
   OPEN  p_SelCur1 FOR
      SELECT UserLevelRoleID UserLevelRoleID  ,
             UPPER(DistNm) DistNm  ,
             CDCRegnCd CDCRegnCd  ,
             CDCNmb CDCNmb  ,
             UPPER(CDCNm) CDCNm  ,
             UPPER(BorrNm) BorrNm  ,
             UPPER(StmtNm) StmtNm  ,
             LoanNmb LoanNmb  ,
             PymtRecvDt PymtRecvDt  ,
             SBAFeeAmt SBAFeeAmt  ,
             CSAFeeAmt CSAFeeAmt  ,
             CDCFeeAmt CDCFeeAmt  ,
             LateFeeAmt LateFeeAmt  ,
             IntApplAmt IntApplAmt  ,
             PrinApplAmt PrinApplAmt  ,
             DiffToEscrowAmt DiffToEscrowAmt  ,
             RefiLoan RefiLoan
        FROM CDCONLINE.TTTempAppFundsTbl  
		ORDER BY UPPER(StmtNm);
EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
END;
/
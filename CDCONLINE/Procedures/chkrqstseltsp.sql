create or replace PROCEDURE           cdconline.ChkRqstSelTSP
(
	p_Identifier IN number := 0,
	p_CDCRgnCD IN char default null,
	p_CDCNmb IN char default null,
	p_RetVal OUT number,
	p_ErrVal OUT number,
	p_ErrMsg OUT varchar2,
	p_SelCur OUT SYS_REFCURSOR
)AS
 -- variable declaration
 BEGIN
	 SAVEPOINT ChkRqstSelTSP;
	 
-- CDC's will see all records
	IF p_Identifier = 0
		THEN
		/* select from CHKRQSTTBL Table */
	 BEGIN
		 OPEN p_SelCur FOR
			SELECT
				CHKNMB,
				ISSDT,
				CHKAMT,
				LOANNMB,
				RCPNTNM,
				MAILADRSTRNM,
				MAILADRCTYNM,
				MAILADRSTCD,
				MAILADRZIPCD,
				ESCHTMNTDT,
				LASTARPUPDDT
			FROM CHKRQSTTBL;
		 
		 p_RetVal := SQL%ROWCOUNT;
		 p_ErrVal := SQLCODE;
		 p_ErrMsg := SQLERRM;
	 END;
	 
-- CDC's will see their own records
	 ELSIF p_Identifier = 1
		THEN
		/* select from CHKRQSTTBL Table */
	BEGIN
		 OPEN p_SelCur FOR
			SELECT
				CHKNMB,
				ISSDT,
				CHKAMT,
				LOANNMB,
				RCPNTNM,
				MAILADRSTRNM,
				MAILADRCTYNM,
				MAILADRSTCD,
				MAILADRZIPCD,
				ESCHTMNTDT,
				LASTARPUPDDT
			FROM CHKRQSTTBL
			WHERE TRIM(CDCREGNCD) = TRIM(p_CDCRgnCD)
			AND TRIM(CDCNMB) = TRIM(p_CDCNmb)
			;
		 
		 p_RetVal := SQL%ROWCOUNT;
		 p_ErrVal := SQLCODE;
		 p_ErrMsg := SQLERRM;
	 END;
	 
	 END IF;
	 EXCEPTION
		 WHEN OTHERS THEN
		 BEGIN
			 p_RetVal := 0;
			 p_ErrVal := SQLCODE;
			 p_ErrMsg := SQLERRM;
		 
		 ROLLBACK TO ChkRqstSelTSP;
		 RAISE;
	 END;
 END ChkRqstSelTSP;
 /
 
GRANT EXECUTE ON CDCONLINE.ChkRqstSelTSP TO CDCONLINEREADALLROLE;
GRANT EXECUTE ON CDCONLINE.ChkRqstSelTSP TO cdconlineprtread;
GRANT EXECUTE ON CDCONLINE.ChkRqstSelTSP TO cdconlinegovread;
GRANT EXECUTE ON CDCONLINE.ChkRqstSelTSP TO CDCONLINEDEVROLE;
CREATE OR REPLACE PROCEDURE CDCONLINE.CaresActPortflLastPmtInfoTsp
(
  p_CDCREGNCD IN CHAR,
  p_CDCNMB IN CHAR,
  p_SelCur1 OUT SYS_REFCURSOR
)
AS

BEGIN
	OPEN  p_SelCur1 FOR
		SELECT H.LoanNmb ,
               C.APPVDT,
               H.PymtTyp ,
               H.PostingDt ,
			   CASE
               WHEN H.PymtTyp = 'X' THEN NULL
               ELSE H.SBAFeeAmt + H.CSAFeeAmt + H.CDCFeeAmt + H.IntAmt + H.PrinAmt + H.UnallocAmt + H.LateFeeAmt + H.DueFromBorrAmt
                END total_pymt  ,
				L.DUEFROMBORRNEEDEDAMT,
				H.SBAFeeAmt ,
                H.CSAFeeAmt ,
                H.CDCFeeAmt ,
			    H.IntAmt ,
                H.PrinAmt ,
                H.LateFeeAmt ,
                H.UnallocAmt ,
                H.PrepayAmt ,
                H.BalAmt ,
				H.CreatUserId,
			    L.CURBALNEEDEDAMT 
			        FROM CDCONLINE.PymtHistryTbl H
					LEFT JOIN CDCONLINE.CnfrmCopyTbl C on C.Loannmb = H.Loannmb
					LEFT JOIN CDCONLINE.LoanTbl L on C.Loannmb = L.Loannmb
       WHERE C.CDCREGNCD = p_CDCREGNCD and
			  C.CDCNMB = p_CDCNMB
			  and H.PostingDt = (select max(PostingDt) from CDCONLINE.PymtHistryTbl where  Loannmb = H.Loannmb)
        ORDER BY H.Loannmb DESC ;

EXCEPTION WHEN OTHERS THEN raise_application_error(-20584,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
END;
/

GRANT EXECUTE ON CDCONLINE.CaresActPortflLastPmtInfoTsp TO CDCONLINEREADALLROLE;
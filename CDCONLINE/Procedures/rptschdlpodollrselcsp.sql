CREATE OR replace PROCEDURE cdconline.rptschdlpodollrselcsp ( 
p_identifier  NUMBER := NULL,  
p_prepaydt    DATE := SYSDATE, 
p_cdcregncd   CHAR := NULL, 
p_cdcnmb      CHAR := NULL, 
p_SelCur out sys_refcursor) 
AS 
BEGIN
	-- TO_NUMBER(REPLACE(SUBSTR(PPWORKUPLTR, INSTR(PPWORKUPLTR, 'Less Net Amount Of Unallocated Funds')+122, INSTR(SUBSTR(PPWORKUPLTR, INSTR(PPWORKUPLTR, 'Less Net Amount Of Unallocated Funds')+122, 20), '<')-1), ',', '')) is used to extract the unallocated funds from the archived workup cover letter
	
	-- PER Vincent's direction on 10/10/2019, the wkprepayamt should always be from the archived workup cover letter
	-- returns report data irrespective of cdc number and cdc region for current data
	IF p_Identifier = 0 THEN
		BEGIN
			OPEN p_SelCur FOR
			SELECT 
				rq.loannmb
				, ls.cdcregncd
				, ls.cdcnmb
				, NVL(LTRIM(RTRIM(ls.smllbuscons)), ls.borrnm) BorrNm
				, rq.prepaydt
				,	(SELECT NVL(audt.duetopploanamt, 0) FROM cdconline.ltrppworkupaudttbl audt WHERE audt.loannmb = rq.loannmb AND audt.prepaydt = rq.prepaydt AND audt.rqstcd = 4  ORDER BY creatdt DESC FETCH FIRST 1 ROW ONLY) wkprepayamt
			FROM
				cdconline.rqsttbl rq
				, cdconline.loansavetbl ls
			WHERE  
				rq.rqstref = ls.rqstref
			AND TRUNC(MONTHS_BETWEEN(LAST_DAY(SYSDATE), LAST_DAY(rq.prepaydt))) = 0
		-- the month of the prepay date must be in the current month
			AND rq.cdcportflstatcdcd = 'CL'
			-- suggests that the user already generated the transcript and/or prepayment work-up
			---and(rq.GFDRecv = 'Y' or datediff(dd, ls.IssDt, '01/01/1992') >= 0)
			AND rq.rqstcd NOT IN ( 1, 6, 7, 9 )
			ORDER  BY ls.wkprepayamt ASC;
		END;
	-- returns report data filtered by cdc number, and cdc region for the current month
	ELSIF p_Identifier = 1 THEN
		BEGIN
			OPEN p_SelCur FOR
			SELECT 
				rq.loannmb
				, ls.cdcregncd
				, ls.cdcnmb
				, NVL(LTRIM(RTRIM(ls.smllbuscons)), ls.borrnm) BorrNm
				, rq.prepaydt
				,	(SELECT NVL(audt.duetopploanamt, 0) FROM cdconline.ltrppworkupaudttbl audt WHERE audt.loannmb = rq.loannmb AND audt.prepaydt = rq.prepaydt AND audt.rqstcd = 4  ORDER BY creatdt DESC FETCH FIRST 1 ROW ONLY) wkprepayamt
			FROM
				cdconline.rqsttbl rq
				, cdconline.loansavetbl ls
			WHERE  
				rq.rqstref = ls.rqstref
			AND TRUNC(MONTHS_BETWEEN(LAST_DAY(SYSDATE), LAST_DAY(rq.prepaydt))) = 0
			AND rq.cdcportflstatcdcd = 'CL'
			AND rq.rqstcd NOT IN ( 1, 6, 7, 9 )
			AND TRIM(ls.cdcnmb) = TRIM(p_cdcnmb)
			AND TRIM(ls.cdcregncd) = TRIM(p_cdcregncd)
			ORDER  BY ls.wkprepayamt ASC;
		END;
	-- archives data at the end of the month for given prepay date; this should always be the current date unless testing for previous versions
	ELSIF p_Identifier = 2 THEN
		BEGIN
			DELETE FROM cdconline.rptschdlpodollrtbl WHERE TRUNC(LAST_DAY(prepaydt)) = TRUNC(LAST_DAY(p_prepaydt));
			
			INSERT INTO cdconline.rptschdlpodollrtbl(loannmb, prepaydt, borrnm, cdcregncd, cdcnmb, wkprepayamt, isarchived, creatuserid, creatdt, lastupdtuserid, lastupdtdt)
			SELECT 
				rq.loannmb
				, rq.prepaydt
				, NVL(LTRIM(RTRIM(ls.smllbuscons)), ls.borrnm) BorrNm
				, ls.cdcregncd
				, ls.cdcnmb
				,	(SELECT NVL(audt.duetopploanamt, 0) FROM cdconline.ltrppworkupaudttbl audt WHERE audt.loannmb = rq.loannmb AND audt.prepaydt = rq.prepaydt AND audt.rqstcd = 4  ORDER BY creatdt DESC FETCH FIRST 1 ROW ONLY) wkprepayamt
				, 'Y' AS isarchived
				, USER
				, SYSDATE
				, USER
				, SYSDATE
			FROM
				cdconline.rqsttbl rq
				, cdconline.loansavetbl ls
			WHERE  
				rq.rqstref = ls.rqstref
			AND TRUNC(LAST_DAY(rq.prepaydt)) = TRUNC(LAST_DAY(p_prepaydt))
			AND rq.cdcportflstatcdcd = 'CL'
			AND rq.rqstcd NOT IN ( 1, 6, 7, 9 );
			
			OPEN p_SelCur FOR
			SELECT 
				rq.loannmb
				, ls.cdcregncd
				, ls.cdcnmb
				, NVL(LTRIM(RTRIM(ls.smllbuscons)), ls.borrnm) BorrNm
				, rq.prepaydt
				,	(SELECT NVL(audt.duetopploanamt, 0) FROM cdconline.ltrppworkupaudttbl audt WHERE audt.loannmb = rq.loannmb AND audt.prepaydt = rq.prepaydt AND audt.rqstcd = 4  ORDER BY creatdt DESC FETCH FIRST 1 ROW ONLY) wkprepayamt
			FROM
				cdconline.rqsttbl rq
				, cdconline.loansavetbl ls
			WHERE  
				rq.rqstref = ls.rqstref
			AND TRUNC(LAST_DAY(rq.prepaydt)) = TRUNC(LAST_DAY(p_prepaydt))
		-- the month of the prepay date must be in the current month
			AND rq.cdcportflstatcdcd = 'CL'
			-- suggests that the user already generated the transcript and/or prepayment work-up
			---and(rq.GFDRecv = 'Y' or datediff(dd, ls.IssDt, '01/01/1992') >= 0)
			AND rq.rqstcd NOT IN ( 1, 6, 7, 9 )
			ORDER  BY ls.wkprepayamt ASC;
		END;
	-- retrieves data for given prepay date month for non-cdc
	ELSIF p_Identifier = 4 THEN
		BEGIN			
			OPEN p_SelCur FOR
			SELECT 
				loannmb
				, prepaydt
				, borrnm
				, cdcregncd
				, cdcnmb
				, wkprepayamt
				, isarchived
				, creatuserid
				, creatdt
				, lastupdtuserid
				, lastupdtdt
			FROM
				cdconline.rptschdlpodollrtbl
			WHERE  
				TRUNC(LAST_DAY(prepaydt)) = TRUNC(LAST_DAY(p_prepaydt))
			ORDER  BY
				cdcregncd ASC, cdcnmb ASC, loannmb ASC;
		END;
	-- retrieves data for given prepay date month for non-cdc
	ELSIF p_Identifier = 5 THEN
		BEGIN			
			OPEN p_SelCur FOR
			SELECT 
				loannmb
				, prepaydt
				, borrnm
				, cdcregncd
				, cdcnmb
				, wkprepayamt
				, isarchived
				, creatuserid
				, creatdt
				, lastupdtuserid
				, lastupdtdt
			FROM
				cdconline.rptschdlpodollrtbl
			WHERE  
				TRUNC(LAST_DAY(prepaydt)) = TRUNC(LAST_DAY(p_prepaydt))
			AND
				TRIM(cdcnmb) = TRIM(p_cdcnmb)
			AND
				TRIM(cdcregncd) = TRIM(p_cdcregncd)
			ORDER  BY
				cdcregncd ASC, cdcnmb ASC, loannmb ASC;
		END;
	END IF;
END;
/

GRANT EXECUTE ON cdconline.rptschdlpodollrselcsp TO cdconlinereadallrole;
GRANT EXECUTE ON cdconline.rptschdlpodollrselcsp TO cdconlineprtread;
GRANT EXECUTE ON cdconline.rptschdlpodollrselcsp TO cdconlinedevrole;
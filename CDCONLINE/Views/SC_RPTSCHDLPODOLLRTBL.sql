create or replace view CDCONLINE.SC_RPTSCHDLPODOLLRTBL 
  (
LOANNMB
,PREPAYDT
,BORRNM
,CDCREGNCD
,CDCNMB
,WKPREPAYAMT
,ISARCHIVED
,CREATUSERID
,CREATDT
,LASTUPDTUSERID
,LASTUPDTDT
) as select
  -- Scrubbing View - Definition generated 2019-07-11 16:10:54
  -- Using SNAP ver SNAP V3.09 3 Oct 2018 J. Low Binary Frond, Select Computing
  --   and template SCRUB_VIEWS V1.2 25 Mar 2019, J. Low, J. Gorowada, Select Computing
LOANNMB
,PREPAYDT
/* BORRNM */ ,'BWR: '||loannmb
,CDCREGNCD
,CDCNMB
,WKPREPAYAMT
,ISARCHIVED
,CREATUSERID
,CREATDT
,LASTUPDTUSERID
,LASTUPDTDT
from CDCONLINE.RPTSCHDLPODOLLRTBL ;